from bisect import bisect_left, bisect_right
from functools import cache
from heapq import heappop, heappush
from math import factorial, inf
import random
from typing import List, Optional

# GRAPHS
def dijkstra(graph: dict, distances: dict, source) -> dict:
    """Dijkstra algorithm to find shortest paths."""
    shortest_paths = dict.fromkeys(graph, inf)
    shortest_paths[source] = 0
    to_visit = [(0, source)]
    while to_visit:
        parent_dist, root = heappop(to_visit)
        for neib in graph[root]:
            cur_dist = distances[frozenset((root, neib))]
            if parent_dist + cur_dist < shortest_paths[neib]:
                shortest_paths[neib] = parent_dist + cur_dist
                heappush(to_visit, (parent_dist + cur_dist, neib))
    return shortest_paths

def topological_sort_dfs(graph: dict, cur, visited: list, ret: list) -> bool:
    # https://en.wikipedia.org/wiki/Topological_sorting#Depth-first_search
    if visited[cur] == "temporary":
        return False    # directed cycle graph, can't be topologically sorted
    elif not visited[cur]:
        visited[cur] = "temporary"
        for course in graph[cur]:
            if not topological_sort_dfs(graph, course, visited, ret):
                return False
        visited[cur] = "permanent"
        ret.append(cur)
    return True

def hierholzer(graph: dict, start) -> list:
    """Find an Eulerian path of the graph."""
    circuit = []
    stack = [start]
    while stack:
        while graph[stack[-1]]:
            stack.append(graph[stack[-1]].pop())
        circuit.append(stack.pop())
    return circuit

def hierholzer_dfs(graph: dict, cur, path: list):
    """Find an Eulerian path of the graph."""
    while graph[cur] and graph[cur]:
        hierholzer_dfs(graph, graph[cur].pop(), path)
    path.append(cur)

# BINARY TREES
def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

@cache
def diameter(r):
    return 0 if r is None else max(height(r.left) + height(r.right) + 1,
                                   max(diameter(r.left), diameter(r.right)))

def height(r):
    """Returns the longest path from root node to any leaf node."""
    return 0 if r is None else 1 + max(height(r.left), height(r.right))

def width(root, level):
    """Returns tree width at level's depth."""
    if root is None:
        return 0
    if level == 1:
        return 1
    elif level > 1:
        return width(root.left, level - 1) + width(root.right, level - 1)
    return 0

def max_width(root):
    """Returns max width of the tree."""
    return max(width(root, i) for i in range(height(root) + 1))

def is_cousins(root, x, y):
    return node_depth(root, x) == node_depth(root, y) and not is_siblings(root, x, y)

def node_depth(root, val):
    if root:
        if root.val == val:
            return 0
        elif (depth := node_depth(root.left, val)) is not None:
            return depth + 1
        elif (depth := node_depth(root.right, val)) is not None:
            return depth + 1

def is_siblings(root, x, y):
    if root:
        if root.left and root.right and {root.left.val, root.right.val} == {x, y}:
            return True
        if is_siblings(root.left, x, y) or is_siblings(root.right, x, y):
            return True
    return False

def traverse_inorder(root):
    if root is None:
        return
    for node in traverse_inorder(root.left):
        yield node
    yield root
    for node in traverse_inorder(root.right):
        yield node

def print_tree_inorder_2d(root, level=0):
    if root is not None:
        print_tree_inorder_2d(root.right, level + 1)
        print(f"{root.val: >{level * 4}}")
        print_tree_inorder_2d(root.left, level + 1)

def print_tree_inorder(root):
    if root is not None:
        print_tree_inorder(root.left)
        print(root.val, end=' ')
        print_tree_inorder(root.right)

def print_tree_preorder(root):
    if root is not None:
        print(root.val, end=' ')
        print_tree_preorder(root.left)
        print_tree_preorder(root.right)

def print_tree_postorder(root):
    if root is not None:
        print_tree_postorder(root.left)
        print_tree_postorder(root.right)
        print(root.val, end=' ')

def del_node(node, key):
    if node is not None:
        if node.val == key:
            if node.left is node.right is None:
                return None
            elif node.left is None:
                return node.right
            elif node.right is None:
                return node.left
            else:
                return add_node(node.right, node.left)
        elif node.val < key:
            node.right = del_node(node.right, key)
        else:
            node.left = del_node(node.left, key)
    return node

def add_node(parent, child):
    if not parent:
        return child
    if child.val < parent.val:
        parent.left = add_node(parent.left, child)
    else:
        parent.right = add_node(parent.right, child)
    return parent

def sortedArrayToBST(nums: List[int]) -> Optional[TreeNode]:
    if not nums:
        return None
    median = len(nums) // 2
    return TreeNode(nums[median],
                    sortedArrayToBST(nums[:median]),
                    sortedArrayToBST(nums[median+1:]))

# LINKED LISTS
class ListNode:
    def __init__(self, val: int = 0, next: "ListNode" = None) -> None:
        self.val = val
        self.next = next

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head: Optional[ListNode]) -> List[int]:
    return [head.val] + linked_list_to_list(head.next) if head else []

def count_nodes(head: ListNode) -> int:
    count = 0
    while head:
        head = head.next
        count += 1
    return count

def get_random(head: Optional[ListNode]) -> int:
    # https://en.wikipedia.org/wiki/Reservoir_sampling
    scope = 1
    chosen = 0
    while head:
        # decide whether to include the element in reservoir
        if random.random() < 1 / scope:
            chosen = head.val
        head = head.next
        scope += 1
    return chosen

def middle_node(head: Optional[ListNode]) -> Optional[ListNode]:
    end = mid = head
    while end.next and (end := end.next.next):
        mid = mid.next
    return mid if end else mid.next

def reverse_iter(head: Optional[ListNode]) -> Optional[ListNode]:
    prev = None
    cur = head
    while cur:
        prev = ListNode(cur.val, prev)
        cur = cur.next
    return prev

def reverse_recur(head, prev=None):
    return reverse_recur(head.next, ListNode(head.val, prev)) if head else prev


# BINARY OPERATIONS
def bsf(x):
    """Returns position of lo bit (rightmost) counting from the right."""
    lo = x & -x
    lo_bit = -1
    while lo:
        lo >>= 1
        lo_bit += 1
    return lo_bit

def bsr(x):
    """Returns position of hi bit (leftmost) counting from the right."""
    hi_bit = -1
    while x:
        x >>= 1
        hi_bit += 1
    return hi_bit

def bit_count32(n):
    """Returns number of 1 bits in 32 bit integer."""
    k = 0b11111111111111111111111111111111
    n = n - ((n >> 1) & 0x55555555)
    n = (n & 0x33333333) + ((n >> 2) & 0x33333333)
    return (((n + (n >> 4) & 0xF0F0F0F) * 0x1010101) & k) >> 24

# MATH
def count_combinations(n, k):
    return factorial(n) / (factorial(k) * factorial(n - k))

def math_infix_to_rpn(expr):
    """
    Transforms expr from infix notation to reverse polish notation.
    This is classical shunting-yard algorithm by Edsger Dijkstra.
    """
    PRECEDENCE = {
        '*': 2,
        '/': 2,
        '+': 1,
        '-': 1,
        '(': 0,
        ')': 0,
    }
    ret = [0, ' ']
    stack = []
    for c in expr:
        if '0' <= c <= '9':
            if isinstance(ret[-1], int):
                ret.append(10*ret.pop() + int(c))
            else:
                ret.append(int(c))
        else:
            if ret[-1] != ' ':
                ret.append(' ')
            if c in '*/+-':
                while stack and PRECEDENCE[c] <= PRECEDENCE[stack[-1]]:
                    ret.append(stack.pop())
                stack.append(c)
            elif c == '(':
                stack.append('(')
            elif c == ')':
                while (tmp := stack.pop()) != '(':
                    ret.append(tmp)
    while stack:
        ret.append(stack.pop())
    return tuple(element for element in ret if element != ' ')

# MISC
def is_square(x):
    return x**0.5 % 1 == 0

def flood_fill_dfs(matrix, row, col, old, new):
    if matrix[row][col] == old:
        matrix[row][col] = new
        if row > 0:
            flood_fill_dfs(matrix, row - 1, col, old, new)
        if row < len(matrix) - 1:
            flood_fill_dfs(matrix, row + 1, col, old, new)
        if col > 0:
            flood_fill_dfs(matrix, row, col - 1, old, new)
        if col < len(matrix[row]) - 1:
            flood_fill_dfs(matrix, row, col + 1, old, new)

def flood_fill_bfs(matrix, row, col, old, new):
    n = len(matrix)
    m = len(matrix[0])
    to_visit = [(row, col)]
    while to_visit:
        row, col = to_visit.pop()
        if 0 <= row < n and 0 <= col < m and matrix[row][col] == old:
            matrix[row][col] = new
            to_visit += (row, col-1), (row, col+1), (row-1, col), (row+1, col)

def manacher_odd(s):
    """
    Return array of max palindrome radiuses for every point in s.
    """
    s = '^' + s + '$'
    len_s = len(s)
    radii = [0] * len_s
    l = 0
    r = 0
    for i in range(1, len_s - 1):
        radii[i] = max(0, min(r - i, radii[l + (r - i)]))
        while s[i - radii[i]] == s[i + radii[i]]:
            radii[i] += 1
        if i + radii[i] > r:
            l = i - radii[i]
            r = i + radii[i]
    return radii[1:-1]

def length_of_lis(nums: List[int]) -> int:
    """
    Length of longest increasing subsequence.
    https://en.wikipedia.org/wiki/Longest_increasing_subsequence
    """
    ret = [nums[0]]
    for n in nums:
        if n > ret[-1]:
            ret.append(n)
        else:
            ret[bisect_left(ret, n)] = n
    return len(ret)

def length_of_lnds(nums: List[int]) -> int:
    """Length of longest non decreasing subsequence."""
    seq = [inf]
    for n in nums:
        if n >= seq[-1]:
            seq.append(n)
        else:
            seq[bisect_right(seq, n)] = n
    return len(seq)

def lis(nums: List[int]):
    """
    Longest increasing subsequence in nums.
    https://en.wikipedia.org/wiki/Longest_increasing_subsequence
    """
    sub = [nums[0]]
    sub_index = [0]
    path = [-1] * len(nums)
    for i, n in enumerate(nums):
        if n > sub[-1]:
            path[i] = sub_index[-1]
            sub.append(n)
            sub_index.append(i)
        else:
            idx = bisect_left(sub, n)
            sub[idx] = n
            sub_index[idx] = i
            if idx > 0:
                path[i] = sub_index[idx-1]

    ret = []
    t = sub_index[-1]
    while t >= 0:
        ret.append(nums[t])
        t = path[t]
    return ret[::-1]

class Trie:
    """Prefix Tree"""
    def __init__(self):
        self.prefixes = {}

    def insert(self, word: str) -> None:
        p = self.prefixes
        for l in word:
            p = p.setdefault(l, {})
        p["\0"] = word

    def remove(self, word):
        def remove_word(prefs, word):
            if word:
                return (word[0] in prefs and
                        remove_word(prefs[word[0]], word[1:]) and
                        prefs.pop(word[0]) and not prefs)
            else:
                return prefs.pop('\0', None) and not prefs

        remove_word(self.prefixes, word)

    def search(self, word: str) -> bool:
        p = self.prefixes
        for l in word:
            if l not in p:
                return False
            p = p[l]
        return "\0" in p

    def startsWith(self, prefix: str) -> bool:
        p = self.prefixes
        for l in prefix:
            if l not in p:
                return False
            p = p[l]
        return True

class DisjointSet:
    """Optimized for add and union operations."""
    def __init__(self):
        self.parent = {}

    def make_set(self, x):
        self.parent.setdefault(x, x)

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def find_set(self, x):
        x_parent = self.find(x)
        return {i for i in self.parent if self.find(i) == x_parent}

    def union(self, x, y):
        if (px := self.find(x)) != (py := self.find(y)):
            self.parent[px] = py

class DisjointSet2:
    """Optimized for set find operation."""
    def __init__(self):
        self.parent = {}
        self.sets = {}

    def make_set(self, x):
        self.parent.setdefault(x, x)
        self.sets.setdefault(x, frozenset([x]))

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def find_set(self, x):
        return self.sets[self.find(x)]

    def union(self, x, y):
        if (px := self.find(x)) != (py := self.find(y)):
            self.parent[px] = py
            self.sets[py] |= self.sets.pop(px)  # merge sets in new parent

class BIT:
    """Binary Indexed Tree implementation."""
    def __init__(self, n: int) -> None:
        self.sums = [0] * (n + 1)

    def update(self, i: int, delta: int) -> None:
        while i < len(self.sums):
            self.sums[i] += delta
            i += i & -i

    def query(self, i: int) -> int:
        ret = 0
        while i > 0:
            ret += self.sums[i]
            i -= i & -i
        return ret
