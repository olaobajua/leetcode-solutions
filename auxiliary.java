// BINARY TREES
public TreeNode sortedArrayToBST(int[] nums) {
    int n = nums.length;
    if (n == 0)
        return null;
    int m = n / 2;
    return new TreeNode(nums[m],
                        sortedArrayToBST(Arrays.copyOfRange(nums, 0, m)),
                        sortedArrayToBST(Arrays.copyOfRange(nums, m+1, n)));
}


// LINKED LISTS
public class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

int countNodes(ListNode head) {
    int count = 0;
    while (head != null) {
        head = head.next;
        ++count;
    }
    return count;
}


// BISECT
int bisectLeft(List<Integer> a, int x, int lo, int hi) {
    if (a.size() == 0)
        return 0;
    while (lo + 1 < hi) {
        int mid = (hi + lo) / 2;
        int t = x <= a.get(mid) ? (hi = mid) : (lo = mid);
    }
    return a.get(lo) >= x ? lo : hi;
}


// MISC
private void floodFill(char[][] matrix, int row, int col, char o, char n) {
    if (matrix[row][col] == o) {
        matrix[row][col] = n;
        if (row > 0)
            floodFill(matrix, row - 1, col, o, n);
        if (row < matrix.length - 1)
            floodFill(matrix, row + 1, col, o, n);
        if (col > 0)
            floodFill(matrix, row, col - 1, o, n);
        if (col < matrix[row].length - 1)
            floodFill(matrix, row, col + 1, o, n);
    }
}

public int lengthOfLIS(int[] nums) {
    // https://en.wikipedia.org/wiki/Longest_increasing_subsequence#Efficient_algorithms
    List<Integer> ret = new ArrayList(Arrays.asList(nums[0]));
    for (int x: nums) {
        if (x > ret.get(ret.size() - 1)) {
            ret.add(x);
        } else {
            ret.set(bisectLeft(ret, x, 0, ret.size()), x);
        }
    }
    return ret.size();
}

/* Binary Indexed Tree implementation. */
class BIT {
    private int[] sums;
    public BIT(int n) {
        sums = new int[n+1];
    }

    public void update(int i, int delta) {
        while (i < sums.length) {
            sums[i] += delta;
            i += i & -i;
        }
    }

    public int query(int i) {
        int ret = 0;
        while (i > 0) {
            ret += sums[i];
            i -= i & -i;
        }
        return ret;
    }
}

class DisjointSet {
    int[] parent;
    public DisjointSet(int n) {
        parent = new int[n];
        for (int i = 0; i < n; ++i)
            parent[i] = i;
    }

    int find(int x) {
        if (x != parent[x])
            parent[x] = find(parent[x]);
        return parent[x];
    }

    void union(int x, int y) {
        int px = find(x), py = find(y);
        if (px != py)
            parent[px] = py;
    }
}
