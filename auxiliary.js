// FUNCTIONAL TOOLS
function cache(fn) {
    const cache = {};
    return function(...args) {
        const argsKey = JSON.stringify(args);
        if (cache[argsKey] === undefined) {
            cache[argsKey] = fn.apply(this, args);
        }
        return cache[argsKey];
    };
}

function zip(...arrays) {
    let minLength = Math.min(...arrays.map(arr => arr.length));
    return [...Array(minLength)].map((_, i) => arrays.map(row => row[i]));
}

function zipLongest(fillvalue=0, ...arrays) {
    let maxLen = Math.max(...arrays.map(arr => arr.length));
    return [...Array(maxLen)].map((_, i) => arrays.map(r => r[i] || fillvalue));
}

// COLLECTIONS
class Counter extends Map {
    constructor(iterable) {
        super();
        [...iterable].forEach(x => this.set(x, this.get(x) + 1 || 1));
    }
    mostCommon() {
        return [...this.entries()].sort((a, b) => b[1] - a[1]);
    }
}

// BISECT
function bisectLeft(a, x, cmp=(a, b) => a - b, lo=0, hi=a.length) {
    if (!a.length)
        return 0;
    while (lo + 1 < hi) {
        const mid = Math.floor((hi + lo) / 2);
        cmp(x, a[mid]) <= 0 ? hi = mid : lo = mid;
    }
    return cmp(a[lo], x) >= 0 ? lo : hi;
}

function bisectRight(a, x, cmp=(a, b) => a - b, lo=0, hi=a.length) {
    if (!a.length)
        return 0;
    while (lo + 1 < hi) {
        const mid = Math.floor((hi + lo) / 2);
        cmp(x, a[mid]) >= 0 ? lo = mid : hi = mid;
    }
    return cmp(a[hi], x) == 0 ? hi + 1 : cmp(a[lo], x) == 0 ? lo + 1 : lo;
}

// LINKED LISTS
function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

function getRandom(head) {
    // https://en.wikipedia.org/wiki/Reservoir_sampling
    let scope = 1, chosen = 0;
    while (head) {
        // decide whether to include the element in reservoir
        if (Math.random() < 1 / scope) {
            chosen = head.val;
        }
        head = head.next;
        ++scope;
    }
    return chosen;
};

function middleNode(head) {
    let end = head, mid = head;
    while (end.next && (end = end.next.next)) {
        mid = mid.next;
    }
    return end ? mid : mid.next;
};

function reverseRecur(head, prev=null) {
    return head ? reverseRecur(head.next, new ListNode(head.val, prev)) : prev;
}

function reverseIter(head) {
    let prev = null, cur = head;
    while (cur) {
        prev = new ListNode(cur.val, prev);
        cur = cur.next;
    }
    return prev;
}

// GRAPHS
function dijkstra(graph, distances, source) {
    /* Dijkstra algorithm to find shortest paths. */
    const shortestPaths = new Map();
    [...graph.keys()].forEach(n => shortestPaths.set(n, Infinity));
    shortestPaths.set(source, 0);
    const toVisit = [[0, source]];
    while (toVisit.length) {
        const [parentDist, root] = toVisit.pop();
        for (const neib of graph.get(root)) {
            const dist = parentDist + distances.get([root, neib].toString());
            if (dist < shortestPaths.get(neib)) {
                shortestPaths.set(neib, dist);
                toVisit.splice(
                    bisectLeft(toVisit, [dist, neib], (a, b) => b[0] - a[0]),
                    0, [dist, neib]
                );
            }
        }
    }
    return shortestPaths;
}
function topologicalSortDfs(graph, cur, visited, ret) {
    // https://en.wikipedia.org/wiki/Topological_sorting#Depth-first_search
    if (visited[cur] == 'temporary') {
        return false;   // directed cycle graph, can't be topologically sorted
    } else if (!visited[cur]) {
        visited[cur] = 'temporary';
        for (const course of graph[cur] || []) {
            if (!topologicalSortDfs(graph, course, visited, ret)) {
                return false;
            }
        }
        visited[cur] = 'permanent';
        ret.push(cur);
    }
    return true;
}

function hierholzerDfs(graph, cur, path) {
    /* Find an Eulerian path of the graph. */
    while (graph[cur] && graph[cur].length) {
        hierholzerDfs(graph, graph[cur].pop(), path);
    }
    path.push(cur);
}

// BINARY TREES
function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

function* treeToList(root) {
    let nodes = [root];
    for (const n of nodes) {
        yield n ? n.val : null;
        if (n) {
            nodes.push(n.left, n.right);
        }
    }
}

function diameter(root) {
    if (root === null) {
        return 0
    }
    return Math.max(height(root.left) + height(root.right) + 1,
                    Math.max(diameter(root.left), diameter(root.right)));
}

function height(root) {
    if (root === null) {
        return 0;
    }
    return 1 + Math.max(height(root.left), height(root.right));
}

function isCousins(root, x, y) {
    return nodeDepth(root, x) == nodeDepth(root, y) && !isSiblings(root, x, y);
};

function nodeDepth(root, val) {
    if (root) {
        if (root.val == val) { return 0; }
        return nodeDepth(root.left, val) + 1 || nodeDepth(root.right, val) + 1;
    }
}

function isSiblings(root, x, y) {
    if (root) {
        if (root.left && root.right && [x, y].includes(root.left.val) &&
                [x, y].includes(root.right.val)) {
            return true;
        }
        if (isSiblings(root.left, x, y) || isSiblings(root.right, x, y)) {
            return true;
        }
    }
    return false;
}

function delNode(root, key) {
    if (root !== null) {
        if (root.val == key) {
            if (root.left === root.right) {
                return null;
            } else if (root.left === null) {
                return root.right;
            } else if (root.right === null) {
                return root.left;
            } else {
                return addNode(root.right, root.left);
            }
        } else if (root.val < key) {
            root.right = delNode(root.right, key);
        } else {
            root.left = delNode(root.left, key);
        }
    }
    return root;
};

function addNode(root, child) {
    if (root === null) {
        return child;
    } else if (child.val < root.val) {
        root.left = addNode(root.left, child);
    } else {
        root.right = addNode(root.right, child);
    }
    return root;
}

function printTreeInorder2D(root, level) {
    /* Print tree inorder rotated to left and in 2D. */
    if (level === undefined) { level = 0; }
    if (root === null) { return; }
    printTreeInorder2D(root.right, level + 1);
    if (root) { console.log(root.val.toString().padStart(level * 4)) }
    printTreeInorder2D(root.left, level + 1);
}

function printTreeInorder(root) {
    if (root) {
        printTreeInorder(root.left);
        process.stdout.write(root.val + ' ');
        printTreeInorder(root.right);
    }
}

function printTreePreorder(root) {
    if (root) {
        process.stdout.write(root.val + ' ');
        printTreePreorder(root.left);
        printTreePreorder(root.right);
    }
}

function printTreePostorder(root) {
    if (root) {
        printTreePostorder(root.left);
        printTreePostorder(root.right);
        process.stdout.write(root.val + ' ');
    }
}

// MATH
function sum(...iterable) {
    return iterable.reduce((acc, x) => acc + x);
}

function mul(...iterable) {
    return iterable.reduce((acc, x) => acc * x);
}

// greatest common divisor
function gcd(a, b) {
    return !b ? a : gcd(b, a % b);
}

// least common multiple
function lcm(a, b) {
    return (a * b) / gcd(a, b);
}

function accumulate(iterable, func, initial=0) {
    return iterable.map((acc => x => acc = func(acc, x))(initial));
}

function* combinations(iter, r=iter.length, prefix='') {
    if (prefix.length < r) {
        for (let i = 0; i < iter.length; ++i) {
            yield* combinations(iter.slice(i + 1), r, [...prefix, iter[i]]);
        }
    } else {
        yield prefix;
    }
}

function* permutations(iterable, r=iterable.length, prefix='') {
    if (prefix.length < r) {
        for (let i = 0; i < iterable.length; ++i) {
            const rem = [...iterable.slice(0, i), ...iterable.slice(i + 1)];
            yield* permutations(rem, r, [...prefix, iterable[i]]);
        }
    } else {
        yield prefix;
    }
}

function countCombinations(n, k) {
    return factorial(n) / (factorial(k) * factorial(n - k));
}

var factorial = cache((a) => {
    return 0 == a ? 1 : (a * factorial(a - 1));
})

function isSquare(x) {
    /* Check if x has integer square root. */
    return x**0.5 % 1 === 0;
}

function mathInfixToRpn(expr) {
    /**
     * Transforms expr from infix notation to reverse polish notation.
     * This is classical shunting-yard algorithm by Edsger Dijkstra.
     */
    const PRECEDENCE = {
        '*': 2,
        '/': 2,
        '+': 1,
        '-': 1,
        '(': 0,
        ')': 0,
    };
    const ret = [0, ' '];
    const stack = [];
    for (const c of expr) {
        if ('0' <= c && c <= '9') {
            if (Number.isInteger(ret[ret.length-1])) {
                ret.push(10*ret.pop() + parseInt(c));
            } else {
                ret.push(parseInt(c));
            }
        } else {
            if (ret[ret.length-1] !== ' ') {
                ret.push(' ');
            }
            if ('*/+-'.includes(c)) {
                while (PRECEDENCE[c] <= PRECEDENCE[stack[stack.length-1]]) {
                    ret.push(stack.pop());
                }
                stack.push(c);
            } else if (c == '(') {
                stack.push('(');
            } else if (c == ')') {
                let tmp;
                while ((tmp = stack.pop()) != '(') {
                    ret.push(tmp);
                }
            }
        }
    }
    while (stack.length) {
        ret.push(stack.pop());
    }
    return ret.filter(element => element !== ' ');
}

// RANDOM
function randint(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function choice(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
};

// SET
Set.prototype.sub = function(set) {
    return new Set([...this.keys()].filter(x => !set.has(x)));
}

// BINARY OPERATIONS
/* Returns position of lo bit (rightmost) counting from the right. */
function bsf(num) {
    let lo = num & -num;
    let loBit = -1;
    while (lo) {
        lo >>>= 1;
        loBit += 1;
    }
    return loBit;
}

/* Returns position of hi bit (leftmost) counting from the right. */
function bsr(num) {
    let hi = num;
    let hiBit = -1;
    while (hi) {
        hi >>>= 1;
        hiBit += 1;
    }
    return hiBit;
}

function bitCount(n) {
    n = n - ((n >> 1) & 0x55555555);
    n = (n & 0x33333333) + ((n >> 2) & 0x33333333);
    return ((n + (n >> 4) & 0xF0F0F0F) * 0x1010101) >> 24;
}

// MISC
function floodFillDfs(matrix, row, col, o, n) {
    if (matrix[row][col] == o) {
        matrix[row][col] = n;
        if (row > 0) {
            floodFillDfs(matrix, row - 1, col, o, n);
        }
        if (row < matrix.length - 1) {
            floodFillDfs(matrix, row + 1, col, o, n);
        }
        if (col > 0) {
            floodFillDfs(matrix, row, col - 1, o, n);
        }
        if (col < matrix[row].length - 1) {
            floodFillDfs(matrix, row, col + 1, o, n);
        }
    }
}

function lengthOfLIS(nums) {
    // https://en.wikipedia.org/wiki/Longest_increasing_subsequence#Efficient_algorithms
    const ret = [nums[0]];
    nums.forEach(num => {
        if (num > ret[ret.length-1]) {
            ret.push(num);
        } else {
            ret[ret.findIndex(x => num <= x)] = num;
        }
    });
    return ret.length;
};

function lenghtOfLNDS(nums) {
    // https://en.wikipedia.org/wiki/Longest_increasing_subsequence#Efficient_algorithms
    const seq = [Number.MAX_SAFE_INTEGER];
    nums.forEach(n => {
        if (n >= seq[seq.length-1]) {
            seq.push(n);
        } else {
            seq[seq.findIndex(x => n < x)] = n;
        }
    });
    return seq.length;
}

function mergeSort(arr) {
    const mid = Math.floor(arr.length / 2);
    if (mid) {
        const lo = mergeSort(arr.slice(0, mid)), m = lo.length;
        const hi = mergeSort(arr.slice(mid)), n = hi.length;
        for (let i = 0, j = 0; i < m || j < n;)
            arr[i+j] = (j == n || i < m && lo[i] <= hi[j]) ? lo[i++] : hi[j++];
    }
    return arr;
}

function quickSelect(points, k) {
    function partition(points, left, right) {
        function squaredDistance([x,y]) {
            return x**2 + y**2;
        }

        let pivot = points[left + ((right - left) >> 1)];
        let pivotDist = squaredDistance(pivot);
        while (left < right) {
            if (squaredDistance(points[left]) >= pivotDist) {
                [points[left], points[right]] = [points[right], points[left]];
                --right;
            } else {
                ++left;
            }
        }
        left += squaredDistance(points[left]) < pivotDist;
        return left;
    };

    let left = 0, right = points.length - 1;
    let pivotIndex = points.length;
    while (pivotIndex !== k) {
        pivotIndex = partition(points, left, right);
        if (pivotIndex < k) {
            left = pivotIndex;
        } else {
            right = pivotIndex - 1;
        }
    }
    return points.slice(0, k);
};

class Trie {
    constructor(name) {
        Object.defineProperty(this, 'prefixes', { value: {} });
    }
    insert(word) {
        let p = this.prefixes;
        for (let l of word) {
            if (!(l in p)) {
                p[l] = {};
            }
            p = p[l];
        }
        p['\0'] = null;
    };
    search(word) {
        let p = this.prefixes;
        for (let l of word) {
            if (!(l in p)) {
                return false;
            }
            p = p[l];
        }
        return "\0" in p;
    };
    startsWith(prefix) {
        let p = this.prefixes;
        for (l of prefix) {
            if (!(l in p)) {
                return false;
            }
            p = p[l];
        }
        return true;
    };
};

/* Optimized for add and union operations. */
class DisjointSet {
    constructor(name) {
        Object.defineProperty(this, 'parent', { value: {} });
    }
    makeSet(x) {
        if (this.parent[x] === undefined) {
            this.parent[x] = x;
        }
    }
    find(x) {
        if (x != this.parent[x]) {
            this.parent[x] = this.find(this.parent[x]);
        }
        return this.parent[x];
    }
    findSet(x) {
        const xParent = this.find(x);
        return Object.keys(this.parent).map(i => parseInt(i))
                                       .filter(i => this.find(i) == xParent);
    }
    union(x, y) {
        const px = this.find(x);
        const py = this.find(y);
        if (px !== undefined && py !== undefined && px != py) {
            this.parent[px] = py;
        }
    }
}

/* Optimized fo set find operation. */
class DisjointSet2 {
    constructor(name) {
        Object.defineProperties(this, {
            'parent': { value: {} },
            'sets': { value: {} },
        });
    }
    makeSet(x) {
        if (this.parent[x] === undefined) {
            this.parent[x] = x;
            this.sets[x] = new Set([x]);
        }
    }
    find(x) {
        if (x != this.parent[x]) {
            this.parent[x] = this.find(this.parent[x]);
        }
        return this.parent[x];
    }
    findSet(x) {
        return this.sets[this.find(x)];
    }
    union(x, y) {
        const px = this.find(x);
        const py = this.find(y);
        if (px != py) {
            this.parent[px] = py;
            this.sets[px].forEach(x => this.sets[py].add(x));
            delete this.sets[px];
        }
    }
}

/* Binary Indexed Tree implementation. */
class BIT {
    constructor(n) {
        this.sums = Array(n + 1).fill(0);
    }

    update(i, delta) {
        while (i < this.sums.length) {
            this.sums[i] += delta;
            i += i & -i;
        }
    }

    query(i) {
        let ret = 0;
        while (i > 0) {
            ret += this.sums[i];
            i -= i & -i;
        }
        return ret;
    }
}
