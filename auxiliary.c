#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// BISECT
int compare(const int *a, const int *b) {
    return *a - *b;
}

int bisectLeft(void a[], int aSize, void x,
               int (*comp)(const void *, const void *), int lo, int hi) {
    if (!aSize)
        return 0;
    while (lo + 1 < hi) {
        const int mid = (lo + hi) / 2;
        comp(&x, &a[mid]) <= 0 ? (hi = mid) : (lo = mid);
    }
    return cmp(a[lo], x) >= 0 ? lo : hi;
}

int bisectRight(void a[], int aSize, void x,
               int (*comp)(const void *, const void *), int lo, int hi) {
    if (!aSize)
        return 0;
    while (lo + 1 < hi) {
        const int mid = (lo + hi) / 2;
        comp(&x, &a[mid]) >= 0 ? (lo = mid) : (hi = mid);
    }
    if (hi >= aSize) return lo;
    return cmp(a[hi], x) == 0 ? hi + 1 : cmp(a[lo], x) == 0 ? lo + 1 : lo;
}

// LINKED LISTS
struct ListNode {
    int val;
    struct ListNode *next;
};

int countNodes(struct ListNode *head) {
    int count = 0;
    while (head) {
        head = head->next;
        ++count;
    }
    return count;
}

int getRandom(struct ListNode *head) {
    // https://en.wikipedia.org/wiki/Reservoir_sampling
    int scope = 1, chosen = 0;
    struct ListNode *head = obj->head;
    while (head) {
        // decide whether to include the element in reservoir
        if ((double)rand() / (double)((unsigned)RAND_MAX + 1) < 1.0 / scope) {
            chosen = head->val;
        }
        head = head->next;
        ++scope;
    }
    return chosen;
}

// BINARY TREES
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

int * treeToList(struct TreeNode *root, int arr[], int *cur) {
    /* arr should be preallocated. */
    if (root) {
        treeToList(root->left, arr, cur);
        arr[(*cur)++] = root->val;
        treeToList(root->right, arr, cur);
    } else {
        // we need something other than NULL for int type here
        arr[(*cur)++] = NULL;
    }
    return arr;
}

struct TreeNode * add_node(struct TreeNode *parent, struct TreeNode *child) {
    /* Creates sorted tree. */
    if (!parent)
        return child;
    if (child->val < parent->val)
        parent->left = add_node(parent->left, child);
    else
        parent->right = add_node(parent->right, child);
    return parent;
}

int height(struct TreeNode* root) {
    if (root == NULL) {
        return 0;
    }
    return 1 + fmax(height(root->left), height(root->right));
}

// MATH
int max(int iterable[], int iterableSize) {
    int ret = 0;
    for (int i = 0; i < iterableSize; ++i) {
        if (ret < iterable[i]) {
            ret = iterable[i];
        }
    }
    return ret;
}

long long int sum(int iterable[], int iterableSize) {
    long long int ret = 0LL;
    for (int i = 0; i < iterableSize; ++i) {
        ret += iterable[i];
    }
    return ret;
}

/* ================================== TRIE ================================= */
#define TRIE_CHARS_COUNT 26
#define TRIE_FIRST_CHAR 'a'

typedef struct Trie {
    struct Trie *children[TRIE_CHARS_COUNT];
    bool end;
} Trie;

Trie* trieCreate() {
    return (Trie*)calloc(1, sizeof(Trie));
}

void trieInsert(Trie *trie, char *word) {
    for (int i = 0; word[i] != '\0'; ++i) {
        if (!trie->children[word[i]-TRIE_FIRST_CHAR])
            trie->children[word[i]-TRIE_FIRST_CHAR] =
                (Trie*)calloc(1, sizeof(Trie));
        trie = trie->children[word[i]-TRIE_FIRST_CHAR];
    }
    trie->end = true;
}

bool trieSearch(Trie *trie, char *word) {
    for (int i = 0; word[i] != '\0'; ++i) {
        if (trie->children[word[i]-TRIE_FIRST_CHAR] == NULL) {
            return false;
        }
        trie = trie->children[word[i]-TRIE_FIRST_CHAR];
    }
    return trie->end;
}

bool trieStartsWith(Trie *trie, char *prefix) {
    for (int i = 0; prefix[i] != '\0'; ++i) {
        if (trie->children[prefix[i]-TRIE_FIRST_CHAR] == NULL) {
            return false;
        }
        trie = trie->children[prefix[i]-TRIE_FIRST_CHAR];
    }
    return true;
}

void trieFree(Trie *trie) {
    if (trie) {
        for (int i = 0; i < TRIE_CHARS_COUNT; ++i) {
            trieFree(trie->children[i]);
        }
        free(trie);
    }
}
/* ========================================================================= */

/* ================================ HASH SET =============================== */
#define SIZE 1 << 15

typedef struct HashSet {
    int key;
    struct HashSet *next;
} HashSet;

int hash(long key) {
    return ((key * 1031237) & ((1 << 20) - 1)) >> 5;
}

HashSet* hashSetCreate() {
    return calloc(SIZE, sizeof(HashSet));
}

void hashSetAdd(HashSet *obj, int key) {
    int index = hash(key);
    for (HashSet *cur = obj[index].next; cur; cur = cur->next) {
        if (cur->key == key) {
            return;
        }
    }
    HashSet *new_node = calloc(1, sizeof(HashSet));
    new_node->key = key;
    new_node->next = obj[index].next;
    obj[index].next = new_node;
}

void hashSetRemove(HashSet* obj, int key) {
    for (HashSet **cur = &obj[hash(key)].next; *cur; cur = &(*cur)->next) {
        if ((*cur)->key == key) {
            HashSet *del = *cur;
            *cur = (*cur)->next;
            free(del);
            return;
        }
    }
}

bool hashSetContains(HashSet* obj, int key) {
    for (HashSet *cur = obj[hash(key)].next; cur; cur = cur->next) {
        if (cur->key == key) {
            return true;
        }
    }
    return false;
}

void hashSetFree(HashSet* obj) {
    for (int i = 0; i < SIZE; ++i) {
        HashSet *cur = obj[i].next;
        while (cur) {
            HashSet *del = cur;
            cur = cur->next;
            free(del);
        }
    }
    free(obj);
}
/* ========================================================================= */

/* ================================ HASH MAP =============================== */
#define SIZE 1 << 15

typedef struct HashMap {
    int key;
    int value;
    struct HashMap *next;
} HashMap;

int hash(long key) {
    return ((key * 1031237) & ((1 << 20) - 1)) >> 5;
}

HashMap* hashMapCreate() {
    return calloc(SIZE, sizeof(HashMap));
}

void hashMapPut(HashMap* obj, int key, int value) {
    int index = hash(key);
    for (HashMap *cur = obj[index].next; cur; cur = cur->next) {
        if (cur->key == key) {
            cur->value = value;
            return;
        }
    }
    HashMap *new_node = calloc(1, sizeof(HashMap));
    new_node->key = key;
    new_node->value = value;
    new_node->next = obj[index].next;
    obj[index].next = new_node;
}

int hashMapGet(HashMap* obj, int key) {
    for (HashMap *cur = obj[hash(key)].next; cur; cur = cur->next) {
        if (cur->key == key) {
            return cur->value;
        }
    }
    return -1;
}

void hashMapRemove(HashMap* obj, int key) {
    for (HashMap **cur = &obj[hash(key)].next; *cur; cur = &(*cur)->next) {
        if ((*cur)->key == key) {
            HashMap *del = *cur;
            *cur = (*cur)->next;
            free(del);
            return;
        }
    }
}

void hashMapFree(HashMap* obj) {
    for (int i = 0; i < SIZE; ++i) {
        HashMap *cur = obj[i].next;
        while (cur) {
            HashMap *del = cur;
            cur = cur->next;
            free(del);
        }
    }
    free(obj);
}
/* ========================================================================= */
