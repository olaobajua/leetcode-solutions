// MISC

/* Binary Indexed Tree implementation. */
class BIT {
private:
    vector<int> sums;

public:
    BIT(int n) {
        sums = vector<int>(n + 1, 0);
    }

    void update(int i, int delta) {
        while (i < sums.size()) {
            sums[i] += delta;
            i += i & -i;
        }
    }

    int query(int i) {
        int ret = 0;
        while (i > 0) {
            ret += sums[i];
            i -= i & -i;
        }
        return ret;
    }
};
