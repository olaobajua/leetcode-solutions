/**
 * 876. Middle of the Linked List (Easy)
 * Given the head of a singly linked list, return the middle node of the linked
 * list.
 *
 * If there are two middle nodes, return the second middle node.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5]
 * Output: [3,4,5]
 * Explanation: The middle node of the list is node 3.
 *
 * Example 2:
 * Input: head = [1,2,3,4,5,6]
 * Output: [4,5,6]
 * Explanation: Since the list has two middle nodes with values 3 and 4, we
 * return the second one.
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [1, 100].
 *     ∙ 1 <= Node.val <= 100
 */

// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var middleNode = function(head) {
    let end = head, mid = head;
    while (end.next && (end = end.next.next)) {
        mid = mid.next;
    }
    return end ? mid : mid.next;
};

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    [[1,2,3,4,5], [3,4,5]],
    [[1,2,3,4,5,6], [4,5,6]],
];

for (const [nums, expected] of tests) {
    const head = arrayToLinkedList(nums);
    console.log(linkedListToArray(middleNode(head)).toString() ==
                expected.toString());
}
