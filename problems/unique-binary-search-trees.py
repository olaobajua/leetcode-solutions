"""
 * 96. Unique Binary Search Trees (Medium)
 * Given an integer n, return the number of structurally unique BST's (binary
 * search trees) which has exactly n nodes of unique values from 1 to n.
 *
 * Example 1:
 * Input: n = 3
 * Output: 5
 *
 * Example 2:
 * Input: n = 1
 * Output: 1
 *
 * Constraints:
 *     1 <= n <= 19
"""
from math import factorial

class Solution:
    def numTrees(self, n: int) -> int:
        # nth Catalan number is Cₙ = (2n)!/((n + 1)! x n!)
        return int(factorial(2 * n) / (factorial(n + 1) * factorial(n)))

if __name__ == "__main__":
    tests = (
        (1, 1),
        (2, 2),
        (3, 5),
        (4, 14),
        (5, 42),
        (6, 132),
        (7, 429),
        (8, 1430),
        (9, 4862),
        (10, 16796),
        (11, 58786),
        (12, 208012),
        (13, 742900),
        (14, 2674440),
        (15, 9694845),
        (16, 35357670),
        (17, 129644790),
        (18, 477638700),
        (19, 1767263190),
    )
    for n, expected in tests:
        print(Solution().numTrees(n) == expected)
