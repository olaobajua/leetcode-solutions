"""
 * 834. Sum of Distances in Tree [Hard]
 * There is an undirected connected tree with n nodes labeled from 0 to n - 1
 * and n - 1 edges.
 *
 * You are given the integer n and the array edges where edges[i] = [aᵢ, bᵢ]
 * indicates that there is an edge between nodes aᵢ and bᵢ in the tree.
 *
 * Return an array answer of length n where answer[i] is the sum of the
 * distances between the iᵗʰ node in the tree and all other nodes.
 *
 * Example 1:
 * Input: n = 6, edges = [[0,1],[0,2],[2,3],[2,4],[2,5]]
 * Output: [8,12,6,10,10,10]
 * Explanation: The tree is shown above.
 * We can see that dist(0,1) + dist(0,2) + dist(0,3) + dist(0,4) + dist(0,5)
 * equals 1 + 1 + 2 + 2 + 2 = 8.
 * Hence, answer[0] = 8, and so on.
 *
 * Example 2:
 * Input: n = 1, edges = []
 * Output: [0]
 *
 * Example 3:
 * Input: n = 2, edges = [[1,0]]
 * Output: [1,1]
 *
 * Constraints:
 *     ∙ 1 <= n <= 3 * 10⁴
 *     ∙ edges.length == n - 1
 *     ∙ edges[i].length == 2
 *     ∙ 0 <= aᵢ, bᵢ < n
 *     ∙ aᵢ != bᵢ
 *     ∙ The given input represents a valid tree.
"""
from collections import defaultdict
from functools import cache
from typing import List

class Solution:
    def sumOfDistancesInTree(self, n: int, edges: List[List[int]]) -> List[int]:
        @cache
        def count_descendants(i):
            ret = len(graph[i])
            for child in graph[i]:
                graph[child].remove(i)
                ret += count_descendants(child)
                graph[child].add(i)
            return ret

        graph = defaultdict(set)
        for a, b in edges:
            graph[a].add(b)
            graph[b].add(a)

        ROOT = 0
        sum_of_distances_to_root = 0
        visited = set()
        for node in (to_visit := [ROOT]):
            if node not in visited:
                visited.add(node)
                sum_of_distances_to_root += count_descendants(node)
                to_visit.extend(graph[node])

        ret = [sum_of_distances_to_root] + [0] * (n - 1)
        visited = {ROOT}
        to_visit = [(child, ROOT) for child in graph[ROOT]]
        for node, parent in to_visit:
            if node not in visited:
                visited.add(node)
                ret[node] += ret[parent] - count_descendants(node) + n
                ret[node] -= count_descendants(node) + 2
                to_visit.extend([(child, node) for child in graph[node]])

        return ret

if __name__ == "__main__":
    tests = (
        (6, [[0,1],[0,2],[2,3],[2,4],[2,5]], [8,12,6,10,10,10]),
        (1, [], [0]),
        (2, [[1,0]], [1,1]),
        (3, [[2,1],[0,2]], [3,3,2]),
        (4, [[1,2],[2,0],[0,3]], [4,6,4,6]),
        (4, [[1,2],[3,2],[3,0]], [6,6,4,4]),
    )
    for n, edges, expected in tests:
        print(Solution().sumOfDistancesInTree(n, edges) == expected)
