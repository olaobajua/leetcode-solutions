/**
 * 219. Contains Duplicate II [Easy]
 * Given an integer array nums and an integer k, return true if there are two
 * distinct indices i and j in the array such that nums[i] == nums[j] and
 * abs(i - j) <= k.
 *
 * Example 1:
 * Input: nums = [1,2,3,1], k = 3
 * Output: true
 *
 * Example 2:
 * Input: nums = [1,0,1,1], k = 1
 * Output: true
 *
 * Example 3:
 * Input: nums = [1,2,3,1,2,3], k = 2
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ -10⁹ <= nums[i] <= 10⁹
 *     ∙ 0 <= k <= 10⁵
 */
class Solution {
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> indices = new HashMap(nums.length);
        Integer index;
        for (int i = 0; i < nums.length; ++i) {
            index = indices.put(nums[i], i);
            if (index != null && i - index <= k)
                return true;
        }
        return false;
    }
}
