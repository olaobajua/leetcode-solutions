"""
 * 1970. Last Day Where You Can Still Cross (Hard)
 * There is a 1-based binary matrix where 0 represents land and 1 represents
 * water. You are given integers row and col representing the number of rows
 * and columns in the matrix, respectively.
 *
 * Initially on day 0, the entire matrix is land. However, each day a new cell
 * becomes flooded with water. You are given a 1-based 2D array cells, where
 * cells[i] = [ri, ci] represents that on the ith day, the cell on the rith row
 * and cith column (1-based coordinates) will be covered with water
 * (i.e., changed to 1).
 *
 * You want to find the last day that it is possible to walk from the top to
 * the bottom by only walking on land cells. You can start from any cell in the
 * top row and end at any cell in the bottom row. You can only travel in the
 * four cardinal directions (left, right, up, and down).
 *
 * Return the last day where it is possible to walk from the top to the bottom
 * by only walking on land cells.
 *
 * Example 1:
 * Input: row = 2, col = 2, cells = [[1,1],[2,1],[1,2],[2,2]]
 * Output: 2
 * Explanation: The above image depicts how the matrix changes each day
 * starting from day 0.
 * The last day where it is possible to cross from top to bottom is on day 2.
 *
 * Example 2:
 * Input: row = 2, col = 2, cells = [[1,1],[1,2],[2,1],[2,2]]
 * Output: 1
 * Explanation: The above image depicts how the matrix changes each day
 * starting from day 0.
 * The last day where it is possible to cross from top to bottom is on day 1.
 *
 * Example 3:
 * Input: row = 3, col = 3, cells = [[1,2],[2,1],[3,3],[2,2],[1,1],[1,3],[2,3],
                                     [3,2],[3,1]]
 * Output: 3
 * Explanation: The above image depicts how the matrix changes each day
 * starting from day 0.
 * The last day where it is possible to cross from top to bottom is on day 3.
 *
 * Constraints:
 *     2 <= row, col <= 2 * 10⁴
 *     4 <= row * col <= 2 * 10⁴
 *     cells.length == row * col
 *     1 <= ri <= row
 *     1 <= ci <= col
 *     All the values of cells are unique.
"""
from bisect import bisect_right
from itertools import repeat
from operator import eq
from typing import List

class Solution:
    def latestDayToCross(self, row: int, col: int, cells: List[List[int]]) -> int:
        class CantCross:
            "Return True if we CAN'T cross the matrix and False otherwise."""
            def __getitem__(self, day):
                matrix = [[0] * col for r in range(row)]
                for (r, c) in cells[:day]:
                    matrix[r - 1][c - 1] = 1
                LAND = 0
                PATH = 2
                for c in range(col):
                    if matrix[0][c] == LAND:
                        flood_fill_bfs(matrix, 0, c, LAND, PATH)
                        if any(map(eq, matrix[row-1], repeat(PATH))):
                            return False
                return True

        return bisect_right(CantCross(), False, 0, len(cells)) - 1

def flood_fill_bfs(matrix, row, col, old, new):
    n = len(matrix)
    m = len(matrix[0])
    to_visit = [(row, col)]
    while to_visit:
        row, col = to_visit.pop()
        if 0 <= row < n and 0 <= col < m and matrix[row][col] == old:
            matrix[row][col] = new
            to_visit += (row, col-1), (row, col+1), (row-1, col), (row+1, col)


if __name__ == "__main__":
    tests = (
        (2, 2, [[1,1],[2,1],[1,2],[2,2]], 2),
        (2, 2, [[1,1],[1,2],[2,1],[2,2]], 1),
        (3, 3, [[1,2],[2,1],[3,3],[2,2],[1,1],[1,3],[2,3],[3,2],[3,1]], 3),
    )
    for row, col, cells, expected in tests:
        print(Solution().latestDayToCross(row, col, cells) == expected)
