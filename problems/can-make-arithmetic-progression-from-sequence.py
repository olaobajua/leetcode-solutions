"""
 * 1502. Can Make Arithmetic Progression From Sequence [Easy]
 * A sequence of numbers is called an arithmetic progression if the difference
 * between any two consecutive elements is the same.
 *
 * Given an array of numbers arr, return true if the array can be rearranged
 * to form an arithmetic progression. Otherwise, return false.
 *
 * Example 1:
 * Input: arr = [3,5,1]
 * Output: true
 * Explanation: We can reorder the elements as [1,3,5] or [5,3,1] with
 * differences 2 and -2 respectively, between each consecutive elements.
 *
 * Example 2:
 * Input: arr = [1,2,4]
 * Output: false
 * Explanation: There is no way to reorder the elements to obtain an
 * arithmetic progression.
 *
 * Constraints:
 *     ∙ 2 <= arr.length <= 1000
 *     ∙ -10⁶ <= arr[i] <= 10⁶
"""
from typing import List

class Solution:
    def canMakeArithmeticProgression(self, arr: List[int]) -> bool:
        arr.sort()
        d = arr[1] - arr[0]
        return all(b - a == d for a, b in zip(arr, arr[1:]))

if __name__ == "__main__":
    tests = (
        ([3,5,1], True),
        ([1,2,4], False),
    )
    for arr, expected in tests:
        print(Solution().canMakeArithmeticProgression(arr) == expected)
