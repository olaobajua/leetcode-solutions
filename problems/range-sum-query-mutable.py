"""
 * 307. Range Sum Query - Mutable [Medium]
 * Given an integer array nums, handle multiple queries of the following types:
 *     ∙ Update the value of an element in nums.
 *     ∙ Calculate the sum of the elements of nums between indices left and
 *       right inclusive where left <= right.
 *
 * Implement the NumArray class:
 *     ∙ NumArray(int[] nums) Initializes the object with the integer array
 *       nums.
 *     ∙ void update(int index, int val) Updates the value of nums[index] to be
 *       val.
 *     ∙ int sumRange(int left, int right) Returns the sum of the elements of
 *       nums between indices left and right inclusive
 *       (i.e. nums[left] + nums[left + 1] + ... + nums[right]).
 *
 * Example 1:
 * Input
 * ["NumArray", "sumRange", "update", "sumRange"]
 * [[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]
 * Output
 * [null, 9, null, 8]
 *
 * Explanation
 * NumArray numArray = new NumArray([1, 3, 5]);
 * numArray.sumRange(0, 2); // return 1 + 3 + 5 = 9
 * numArray.update(1, 2);   // nums = [1, 2, 5]
 * numArray.sumRange(0, 2); // return 1 + 2 + 5 = 8
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 3 * 10⁴
 *     ∙ -100 <= nums[i] <= 100
 *     ∙ 0 <= index < nums.length
 *     ∙ -100 <= val <= 100
 *     ∙ 0 <= left <= right < nums.length
 *     ∙ At most 3 * 10⁴ calls will be made to update and sumRange.
"""
from typing import List

class NumArray:
    def __init__(self, nums: List[int]):
        self.bit = BIT(len(nums))
        for i, x in enumerate(nums):
            self.bit.update(i + 1, x)
        self.nums = [0] + nums

    def update(self, index: int, val: int) -> None:
        self.bit.update(index + 1, val - self.nums[index+1])
        self.nums[index+1] = val

    def sumRange(self, left: int, right: int) -> int:
        return self.bit.query(right + 1) - self.bit.query(left)

class BIT:
    """Binary Indexed Tree implementation."""
    def __init__(self, n: int) -> None:
        self.sums = [0] * (n + 1)

    def update(self, i: int, delta: int) -> None:
        while i < len(self.sums):
            self.sums[i] += delta
            i += i & -i

    def query(self, i: int) -> int:
        ret = 0
        while i > 0:
            ret += self.sums[i]
            i -= i & -i
        return ret


obj = NumArray([1, 3, 5])
print(9 == obj.sumRange(0, 2))
obj.update(1, 2)
print(8 == obj.sumRange(0, 2))

obj = NumArray([-1])
print(-1 == obj.sumRange(0, 0))
obj.update(0, 1)
print(1 == obj.sumRange(0, 0))
