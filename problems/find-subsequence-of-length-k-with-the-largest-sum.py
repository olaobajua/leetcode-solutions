"""
 * 2099. Find Subsequence of Length K With the Largest Sum (Easy)
 * You are given an integer array nums and an integer k. You want to find a
 * subsequence of nums of length k that has the largest sum.
 *
 * Return any such subsequence as an integer array of length k.
 *
 * A subsequence is an array that can be derived from another array by deleting
 * some or no elements without changing the order of the remaining elements.
 *
 * Example 1:
 * Input: nums = [2,1,3,3], k = 2
 * Output: [3,3]
 * Explanation:
 * The subsequence has the largest sum of 3 + 3 = 6.
 *
 * Example 2:
 * Input: nums = [-1,-2,3,4], k = 3
 * Output: [-1,3,4]
 * Explanation:
 * The subsequence has the largest sum of -1 + 3 + 4 = 6.
 *
 * Example 3:
 * Input: nums = [3,4,3,3], k = 2
 * Output: [3,4]
 * Explanation:
 * The subsequence has the largest sum of 3 + 4 = 7.
 * Another possible subsequence is [4, 3].
 *
 * Constraints:
 *     1 <= nums.length <= 1000
 *     -10⁵ <= nums[i] <= 10⁵
 *     1 <= k <= nums.length
"""
from operator import itemgetter
from typing import List

class Solution:
    def maxSubsequence(self, nums: List[int], k: int) -> List[int]:
        top_k = sorted([(n, i) for i, n in enumerate(nums)])[-k:]
        return [n for n, _ in sorted(top_k, key=itemgetter(1))]

if __name__ == "__main__":
    tests = (
        ([2,1,3,3], 2, [3,3]),
        ([-1,-2,3,4], 3, [-1,3,4]),
        ([3,4,3,3], 2, [4,3]),
        ([5,4,3,2,1], 2, [5,4]),
    )
    for nums, k, expected in tests:
        print(Solution().maxSubsequence(nums, k) == expected)
