"""
 * 113. Path Sum II [Medium]
 * Given the root of a binary tree and an integer targetSum, return all
 * root-to-leaf paths where the sum of the node values in the path equals
 * targetSum. Each path should be returned as a list of the node values, not
 * node references.
 *
 * A root-to-leaf path is a path starting from the root and ending at any leaf
 * node. A leaf is a node with no children.
 *
 * Example 1:
 * Input: root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
 * Output: [[5,4,11,2],[5,8,4,5]]
 * Explanation: There are two paths whose sum equals targetSum:
 * 5 + 4 + 11 + 2 = 22
 * 5 + 8 + 4 + 5 = 22
 *
 * Example 2:
 * Input: root = [1,2,3], targetSum = 5
 * Output: []
 *
 * Example 3:
 * Input: root = [1,2], targetSum = 0
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 5000].
 *     ∙ -1000 <= Node.val <= 1000
 *     ∙ -1000 <= targetSum <= 1000
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def pathSum(self, root: Optional[TreeNode], targetSum: int) -> List[List[int]]:
        def dfs(node, target, path):
            if node:
                target -= node.val
                path += (node.val,)
                if not target and not node.left and not node.right:
                    yield path
                yield from dfs(node.left, target, path)
                yield from dfs(node.right, target, path)
        return list(map(list, dfs(root, targetSum, ())))

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([5,4,8,11,None,13,4,7,2,None,None,5,1], 22, [[5,4,11,2],[5,8,4,5]]),
        ([1,2,3], 5, []),
        ([1,2], 0, []),
        ([], 0, []),
        ([1,2], 1, []),
        ([0,1,1], 1, [[0,1],[0,1]]),
    )
    for root, targetSum, expected in tests:
        print(Solution().pathSum(build_tree(root), targetSum) == expected)
