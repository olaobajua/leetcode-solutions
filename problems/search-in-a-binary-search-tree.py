"""
 * 700. Search in a Binary Search Tree (Easy)
 * You are given the root of a binary search tree (BST) and an integer val.
 *
 * Find the node in the BST that the node's value equals val and return the
 * subtree rooted with that node. If such a node does not exist, return null.
 *
 * Example 1:
 * Input: root = [4,2,7,1,3], val = 2
 * Output: [2,1,3]
 *
 * Example 2:
 * Input: root = [4,2,7,1,3], val = 5
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 5000].
 *     ∙ 1 <= Node.val <= 10⁷
 *     ∙ root is a binary search tree.
 *     ∙ 1 <= val <= 10⁷
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def searchBST(self, root: Optional[TreeNode], val: int) -> Optional[TreeNode]:
        if root is None or val == root.val:
            return root
        return self.searchBST(root.left if val < root.val else root.right, val)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([4,2,7,1,3], 2, [2,1,3]),
        ([4,2,7,1,3], 5, []),
    )
    for nums, val, expected in tests:
        root = build_tree(nums)
        ret = [*tree_to_list(Solution().searchBST(root, val))]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
