/**
 * 1288. Remove Covered Intervals (Medium)
 * Given an array intervals where intervals[i] = [lᵢ, rᵢ] represent the
 * interval [lᵢ, rᵢ), remove all intervals that are covered by another interval
 * in the list.
 *
 * The interval [a, b) is covered by the interval [c, d) if and only if
 * c <= a and b <= d.
 *
 * Return the number of remaining intervals.
 *
 * Example 1:
 * Input: intervals = [[1,4],[3,6],[2,8]]
 * Output: 2
 * Explanation: Interval [3,6] is covered by [2,8], therefore it is removed.
 *
 * Example 2:
 * Input: intervals = [[1,4],[2,3]]
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= intervals.length <= 1000
 *     ∙ intervals[i].length == 2
 *     ∙ 0 <= lᵢ <= rᵢ <= 10⁵
 *     ∙ All the given intervals are unique.
 */
int compare(const int **a, const int **b) {
    return (*a)[0] != (*b)[0] ? (*a)[0] - (*b)[0] : (*b)[1] - (*a)[1];
}
int removeCoveredIntervals(int **intervals, int intervalsSize, int *intervalsColSize) {
    int ret = 0, right = 0;
    qsort(intervals, intervalsSize, sizeof(intervals[0]), compare);
    for (int i = 0; i < intervalsSize; ++i) {
        ret += right < intervals[i][1];
        right = fmax(right, intervals[i][1]);
    }
    return ret;
}
