"""
 * 2589. Minimum Time to Complete All Tasks [Hard]
 * There is a computer that can run an unlimited number of tasks at the same
 * time. You are given a 2D integer array tasks where tasks[i] = [startᵢ, endᵢ,
 * durationᵢ] indicates that the iᵗʰ task should run for a total of durationᵢ
 * seconds (not necessarily continuous) within the inclusive time range
 * [startᵢ, endᵢ].
 *
 * You may turn on the computer only when it needs to run a task. You can also
 * turn it off if it is idle.
 *
 * Return the minimum time during which the computer should be turned on to
 * complete all tasks.
 *
 * Example 1:
 * Input: tasks = [[2,3,1],[4,5,1],[1,5,2]]
 * Output: 2
 * Explanation:
 * - The first task can be run in the inclusive time range [2, 2].
 * - The second task can be run in the inclusive time range [5, 5].
 * - The third task can be run in the two inclusive time ranges [2, 2] and [5,
 * 5].
 * The computer will be on for a total of 2 seconds.
 *
 * Example 2:
 * Input: tasks = [[1,3,2],[2,5,3],[5,6,2]]
 * Output: 4
 * Explanation:
 * - The first task can be run in the inclusive time range [2, 3].
 * - The second task can be run in the inclusive time ranges [2, 3] and [5,
 * 5].
 * - The third task can be run in the two inclusive time range [5, 6].
 * The computer will be on for a total of 4 seconds.
 *
 * Constraints:
 *     ∙ 1 <= tasks.length <= 2000
 *     ∙ tasks[i].length == 3
 *     ∙ 1 <= startᵢ, endᵢ <= 2000
 *     ∙ 1 <= durationᵢ <= endᵢ - startᵢ + 1
"""
from operator import itemgetter
from typing import List

class Solution:
    def findMinimumTime(self, tasks: List[List[int]]) -> int:
        timepoints = [0] * 2001
        for start, end, duration in sorted(tasks, key=itemgetter(1)):
            for i in range(start, end + 1):
                if duration == 0:
                    break
                if timepoints[i]:
                    timepoints[i] += 1
                    duration -=1

            for i in range(end, start - 1, -1):
                if duration == 0:
                    break
                if timepoints[i] == 0:
                    timepoints[i] += 1
                    duration -=1
        return sum(t > 0 for t in timepoints)

if __name__ == "__main__":
    tests = (
        ([[2,3,1],[4,5,1],[1,5,2]], 2),
        ([[1,3,2],[2,5,3],[5,6,2]], 4),
    )
    for tasks, expected in tests:
        print(Solution().findMinimumTime(tasks) == expected)
