/**
 * 576. Out of Boundary Paths [Medium]
 * There is an m x n grid with a ball. The ball is initially at the position
 * [startRow, startColumn]. You are allowed to move the ball to one of the four
 * adjacent cells in the grid (possibly out of the grid crossing the grid
 * boundary). You can apply at most maxMove moves to the ball.
 *
 * Given the five integers m, n, maxMove, startRow, startColumn, return the
 * number of paths to move the ball out of the grid boundary. Since the answer
 * can be very large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: m = 2, n = 2, maxMove = 2, startRow = 0, startColumn = 0
 * Output: 6
 *
 * Example 2:
 * Input: m = 1, n = 3, maxMove = 3, startRow = 0, startColumn = 1
 * Output: 12
 *
 * Constraints:
 *     ∙ 1 <= m, n <= 50
 *     ∙ 0 <= maxMove <= 50
 *     ∙ 0 <= startRow < m
 *     ∙ 0 <= startColumn < n
 */

/**
 * @param {number} m
 * @param {number} n
 * @param {number} maxMove
 * @param {number} startRow
 * @param {number} startColumn
 * @return {number}
 */
var findPaths = function(m, n, maxMove, startRow, startColumn) {
    const MOD = 1000000007;
    let dp = [...Array(m)].map(_ => Array(n).fill(0));
    dp[startRow][startColumn] = 1;
    let ret = 0;
    for (let moves = 1; moves <= maxMove; ++moves) {
        const temp = [...Array(m)].map(_ => Array(n));
        for (let row = 0; row < m; ++row) {
            for (let col = 0; col < n; ++col) {
                ret += dp[row][col] * (row == m - 1);
                ret += dp[row][col] * (col == n - 1);
                ret += dp[row][col] * (row == 0);
                ret += dp[row][col] * (col == 0);
                ret %= MOD;
                temp[row][col] = 0;
                temp[row][col] += (row > 0) && dp[row - 1][col];
                temp[row][col] += (row < m - 1) && dp[row + 1][col];
                temp[row][col] += (col > 0) && dp[row][col - 1];
                temp[row][col] += (col < n - 1) && dp[row][col + 1];
                temp[row][col] %= MOD;
            }
        }
        dp = temp;
    }
    return ret;
};

const tests = [
    [2, 2, 2, 0, 0, 6],
    [1, 3, 3, 0, 1, 12],
    [8, 50, 23, 5, 26, 914783380],
    [36, 5, 50, 15, 3, 390153306],
];

for (const [m, n, maxMove, startRow, startColumn, expected] of tests) {
    console.log(findPaths(m, n, maxMove, startRow, startColumn) == expected);
}
