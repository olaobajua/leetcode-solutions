"""
 * 1162. As Far from Land as Possible [Medium]
 * Given an n x n grid containing only values 0 and 1, where 0 represents
 * water and 1 represents land, find a water cell such that its distance to the
 * nearest land cell is maximized, and return the distance. If no land or water
 * exists in the grid, return -1.
 *
 * The distance used in this problem is the Manhattan distance: the distance
 * between two cells (x0, y0) and (x1, y1) is |x0 - x1| + |y0 - y1|.
 *
 * Example 1:
 * Input: grid = [[1,0,1],
 *                [0,0,0],
 *                [1,0,1]]
 * Output: 2
 * Explanation: The cell (1, 1) is as far as possible from all the land with
 * distance 2.
 *
 * Example 2:
 * Input: grid = [[1,0,0],
 *                [0,0,0],
 *                [0,0,0]]
 * Output: 4
 * Explanation: The cell (2, 2) is as far as possible from all the land with
 * distance 4.
 *
 * Constraints:
 *     ∙ n == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= n <= 100
 *     ∙ grid[i][j] is 0 or 1
"""
from typing import List

class Solution:
    def maxDistance(self, grid: List[List[int]]) -> int:
        n = len(grid)
        land = [(r, c, 0) for r in range(n) for c in range(n) if grid[r][c]]
        if len(land) == 1:
            r, c, _ = land[0]
            n -= 1
            return max(r + c, n - r + c, r + n - c, n - r + n - c)
        visited = set()
        ret = -1
        for r, c, d in land:
            if grid[r][c] == 0 and (r, c) not in visited:
                ret = max(ret, d)
            visited.add((r, c))
            for nr, nc in (r+1, c), (r-1, c), (r, c+1), (r, c-1):
                if 0 <= nr < n and 0 <= nc < n and (nr, nc) not in visited:
                    land.append((nr, nc, d + 1))
        return ret

if __name__ == "__main__":
    tests = (
        ([[1,0,1],
          [0,0,0],
          [1,0,1]], 2),
        ([[1,0,0],
          [0,0,0],
          [0,0,0]], 4),
        ([[0,0,0,0],
          [0,0,0,0],
          [0,0,0,0],
          [0,0,0,0]], -1),
        ([[1,1,1,1,1],
          [1,1,1,1,1],
          [1,1,1,1,1],
          [1,1,1,1,1],
          [1,1,1,1,1]], -1),
        ([[1,0,0,0,0,1,0,0,0,1],
          [1,1,0,1,1,1,0,1,1,0],
          [0,1,1,0,1,0,0,1,0,0],
          [1,0,1,0,1,0,0,0,0,0],
          [0,1,0,0,0,1,1,0,1,1],
          [0,0,1,0,0,1,0,1,0,1],
          [0,0,0,1,1,1,1,0,0,1],
          [0,1,0,0,1,0,0,1,0,0],
          [0,0,0,0,0,1,1,1,0,0],
          [1,1,0,1,1,1,1,1,0,0]], 2),
    )
    for grid, expected in tests:
        print(Solution().maxDistance(grid) == expected)
