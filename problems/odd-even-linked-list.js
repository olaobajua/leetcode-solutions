/**
 * 328. Odd Even Linked List (Medium)
 * Given the head of a singly linked list, group all the nodes with odd indices
 * together followed by the nodes with even indices, and return the reordered
 * list.
 *
 * The first node is considered odd, and the second node is even, and so on.
 *
 * Note that the relative order inside both the even and odd groups should
 * remain as it was in the input.
 *
 * You must solve the problem in O(1) extra space complexity and O(n) time
 * complexity.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5]
 * Output: [1,3,5,2,4]
 *
 * Example 2:
 * Input: head = [2,1,3,5,6,4,7]
 * Output: [2,3,6,7,1,5,4]
 *
 * Constraints:
 *     n == number of nodes in the linked list
 *     0 <= n <= 10⁴
 *     -10⁶ <= Node.val <= 10⁶
 */

// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var oddEvenList = function(head) {
    if (!head || !head.next) {
        return head;
    }
    let odd = head;
    let even = head.next;
    let evenHead = even;
    for (let cur = head.next.next, i = 3; cur; cur = cur.next, ++i) {
        if (i % 2) {
            odd.next = cur;
            odd = odd.next;
        } else {
            even.next = cur;
            even = even.next;
        }
    }
    even.next = null;
    odd.next = evenHead;
    return head;
};

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    ([1,2,3,4,5], [1,3,5,2,4]),
    ([2,1,3,5,6,4,7], [2,3,6,7,1,5,4]),
];

for (const [nums, expected] of tests) {
    const head = arrayToLinkedList(nums);
    console.log(linkedListToArray(oddEvenList(head))
                .every((n, i) => n == expected[i]));
}
