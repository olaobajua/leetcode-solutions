"""
 * 782. Transform to Chessboard (Hard)
 * You are given an n x n binary grid board. In each move, you can swap any two
 * rows with each other, or any two columns with each other.
 *
 * Return the minimum number of moves to transform the board into a chessboard
 * board. If the task is impossible, return -1.
 *
 * A chessboard board is a board where no 0's and no 1's are 4-directionally
 * adjacent.
 *
 * Example 1:
 * Input: board = [[0,1,1,0],[0,1,1,0],[1,0,0,1],[1,0,0,1]]
 * Output: 2
 * Explanation: One potential sequence of moves is shown.
 * The first move swaps the first and second column.
 * The second move swaps the second and third row.
 *
 * Example 2:
 * Input: board = [[0,1],[1,0]]
 * Output: 0
 * Explanation: Also note that the board with 0 in the top left corner, is also
 * a valid chessboard.
 *
 * Example 3:
 * Input: board = [[1,0],[1,0]]
 * Output: -1
 * Explanation: No matter what sequence of moves you make, you cannot end with
 * a valid chessboard.
 *
 * Constraints:
 *     n == board.length
 *     n == board[i].length
 *     2 <= n <= 30
 *     board[i][j] is either 0 or 1.
"""
from collections import Counter
from functools import reduce
from operator import sub
from typing import List

class Solution:
    def movesToChessboard(self, board: List[List[int]]) -> int:
        board = tuple(tuple(row) for row in board)
        rows = Counter(board)
        if len(rows) > 2 or abs(reduce(sub, rows.values())) > 1:
            return -1
        rotated_board = [*zip(*board)]
        cols = Counter(rotated_board)
        if len(cols) > 2 or abs(reduce(sub, cols.values())) > 1:
            return -1
        odd, even = rows.keys()
        if rows[odd] == rows[even]:
            to_swap = Counter(board[::2])
            swaps = min(to_swap[odd], to_swap[even])
        else:
            first = odd if rows[odd] > rows[even] else even
            swaps = sum(row != first for row in board[::2])
        odd, even = cols.keys()
        if cols[odd] == cols[even]:
            to_swap = Counter(rotated_board[::2])
            swaps += min(to_swap[odd], to_swap[even])
        else:
            first = odd if cols[odd] > cols[even] else even
            swaps += sum(col != first for col in rotated_board[::2])
        return swaps

if __name__ == "__main__":
    tests = (
        ([[1,0,0,1,1],
          [0,1,1,0,0],
          [1,0,0,1,1],
          [0,1,1,0,0],
          [0,1,1,0,0]], 3),
        ([[0,1],
          [1,0]], 0),
        ([[0,0,1,1],
          [1,1,0,0],
          [0,1,0,1],
          [1,0,1,0]], -1),
        ([[1,1,0],
          [0,0,1],
          [0,0,1]], 2),
        ([[0,1,1,0],
          [0,1,1,0],
          [1,0,0,1],
          [1,0,0,1]], 2),
        ([[1,0],
          [1,0]], -1),
        ([[0,1,1,0,0,1,1,1,0],
          [0,1,1,1,0,0,1,0,1],
          [0,1,1,0,0,1,1,1,0],
          [0,1,1,0,0,1,1,1,0],
          [0,1,1,1,0,0,1,0,1],
          [0,1,1,0,0,1,1,1,0],
          [0,1,1,1,0,0,1,0,1],
          [0,1,1,0,0,1,1,1,0],
          [0,1,1,1,0,0,1,0,1]], -1),
        ([[0,1,1,1,0,0,0,1,1,0],
          [0,1,1,1,0,0,0,1,1,0],
          [1,0,0,0,1,1,1,0,0,1],
          [1,0,0,0,1,1,1,0,0,1],
          [0,1,1,1,0,0,0,1,1,0],
          [1,0,0,0,1,1,1,0,0,1],
          [1,0,0,0,1,1,1,0,0,1],
          [0,1,1,1,0,0,0,1,1,0],
          [1,0,0,0,1,1,1,0,0,1],
          [0,1,1,1,0,0,0,1,1,0]], 4),
        ([[0,1,0,0,1,1,1,0,0,1],
          [1,0,1,1,0,0,0,1,1,0],
          [0,1,0,0,1,1,1,0,0,1],
          [0,1,0,0,1,1,1,0,0,1],
          [1,0,1,1,0,0,0,1,1,0],
          [1,0,1,1,0,0,0,1,1,0],
          [1,0,1,1,0,0,0,1,1,0],
          [1,0,1,1,0,0,0,1,1,0],
          [0,1,0,0,1,1,1,0,0,1],
          [0,1,0,0,1,1,1,0,0,1]], 4),
        ([[1,0,1,0,1,0,0,0,1,1,0],
          [1,0,1,0,1,0,0,0,1,1,0],
          [0,1,0,1,0,1,1,1,0,0,1],
          [0,1,0,1,0,1,1,1,0,0,1],
          [1,0,1,0,1,0,0,0,1,1,0],
          [0,1,0,1,0,1,1,1,0,0,1],
          [0,1,0,1,0,1,1,1,0,0,1],
          [1,0,1,0,1,0,0,0,1,1,0],
          [1,0,1,0,1,0,0,0,1,1,0],
          [1,0,1,0,1,0,0,0,1,1,0],
          [0,1,0,1,0,1,1,1,0,0,1]], 7),
    )
    from time import time
    start = time()
    for board, expected in tests:
        print(Solution().movesToChessboard(board) == expected)
    print(f"Time elapsed {time() - start} sec")
