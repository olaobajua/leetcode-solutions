"""
 * 121. Best Time to Buy and Sell Stock (Easy)
 * You are given an array prices where prices[i] is the price of a given stock
 * on the iᵗʰ day.
 *
 * You want to maximize your profit by choosing a single day to buy one stock
 * and choosing a different day in the future to sell that stock.
 *
 * Return the maximum profit you can achieve from this transaction. If you
 * cannot achieve any profit, return 0.
 *
 * Example 1:
 * Input: prices = [7,1,5,3,6,4]
 * Output: 5
 * Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6),
 * profit = 6-1 = 5.
 * Note that buying on day 2 and selling on day 1 is not allowed because you
 * must buy before you sell.
 *
 * Example 2:
 * Input: prices = [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transactions are done and the max profit = 0.
 *
 * Constraints:
 *     ∙ 1 <= prices.length <= 10⁵
 *     ∙ 0 <= prices[i] <= 10⁴
"""
from itertools import accumulate
from typing import List

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        min_before = accumulate(prices, min)
        max_after = reversed([*accumulate(reversed(prices), max)])
        return max(a - b for a, b in zip(max_after, min_before))

if __name__ == "__main__":
    tests = (
        ([7,1,5,3,6,4], 5),
        ([7,6,4,3,1], 0),
        ([7,2,4,1], 2),
    )
    for prices, expected in tests:
        print(Solution().maxProfit(prices) == expected)
