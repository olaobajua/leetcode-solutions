"""
 * 114. Flatten Binary Tree to Linked List [Medium]
 * Given the root of a binary tree, flatten the tree into a "linked list":
 *     ∙ The "linked list" should use the same TreeNode class where the right
 * child pointer points to the next node in the list and the left child pointer
 * is always null.
 *     ∙ The "linked list" should be in the same order as a pre-order traversal
 *       (https://en.wikipedia.org/wiki/Tree_traversal#Pre-order,_NLR) of the
 *       binary tree.
 *
 * Example 1:
 *     1        1
 *  2     5      2
 * 3 4   6        3
 *                 4
 *                  5
 *                   6
 * Input: root = [1,2,5,3,4,null,6]
 * Output: [1,null,2,null,3,null,4,null,5,null,6]
 *
 * Example 2:
 * Input: root = []
 * Output: []
 *
 * Example 3:
 * Input: root = [0]
 * Output: [0]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 2000].
 *     ∙ -100 <= Node.val <= 100
 *
 * Follow up: Can you flatten the tree in-place (with O(1) extra space)?
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def flatten(self, root: Optional[TreeNode]) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        if root:
            self.flatten(root.left)
            self.flatten(root.right)
            tmp, root.right, root.left = root.right, root.left, None
            while root.right:
                root = root.right
            root.right = tmp

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([1,2,5,3,4,None,6], [1,None,2,None,3,None,4,None,5,None,6]),
        ([], []),
        ([0], [0]),
    )
    for nums, expected in tests:
        root = build_tree(nums)
        Solution().flatten(root)
        ret = [*tree_to_list(root)]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
