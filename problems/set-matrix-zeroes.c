/*
 * 73. Set Matrix Zeroes (Medium)
 * Given an m x n integer matrix matrix, if an element is 0, set its entire row
 * and column to 0's, and return the matrix.
 *
 * You must do it in place.
 *
 * Example 1:
 * Input: matrix = [[1,1,1],
 *                  [1,0,1],
 *                  [1,1,1]]
 * Output: [[1,0,1],
 *          [0,0,0],
 *          [1,0,1]]
 *
 * Example 2:
 * Input: matrix = [[0,1,2,0],
 *                  [3,4,5,2],
 *                  [1,3,1,5]]
 * Output: [[0,0,0,0],
 *          [0,4,5,0],
 *          [0,3,1,0]]
 *
 * Constraints:
 *     m == matrix.length
 *     n == matrix[0].length
 *     1 <= m, n <= 200
 *     -2³¹ <= matrix[i][j] <= 2³¹ - 1
 *
 * Follow up:
 *     A straightforward solution using O(mn) space is probably a bad idea.
 *     A simple improvement uses O(m + n) space, but still not the best solution.
 *     Could you devise a constant space solution?
*/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool isZeroCol(int **matrix, int colNum, int rowSize) {
    for (int row = 0; row < rowSize; ++row) {
        if (matrix[row][colNum] == 0) {
            return true;
        }
    }
    return false;
}

bool isZeroRow(int **matrix, int rowNum, int colSize) {
    for (int col = 0; col < colSize; ++col) {
        if (matrix[rowNum][col] == 0) {
            return true;
        }
    }
    return false;
}

void setColToZeroes(int **matrix, int colNum, int rowSize) {
    for (int row = 0; row < rowSize; ++row) {
        matrix[row][colNum] = 0;
    }
}

void setColsToZeroes(int **matrix, int matrixSize, int *matrixColSize) {
    for (int col = 1; col < matrixColSize[0]; ++col) {
        if (matrix[0][col] == 0) {
            setColToZeroes(matrix, col, matrixSize);
        }
    }
}

void setRowToZeroes(int **matrix, int rowNum, int colSize) {
    for (int col = 0; col < colSize; ++col) {
        matrix[rowNum][col] = 0;
    }
}

void setRowsToZeroes(int **matrix, int matrixSize, int *matrixColSize) {
    for (int row = 1; row < matrixSize; ++row) {
        if (matrix[row][0] == 0) {
            setRowToZeroes(matrix, row, matrixColSize[row]);
        }
    }
}

void checkMatrix(int **matrix, int matrixSize, int *matrixColSize) {
    for (int row = 1; row < matrixSize; ++row) {
        for (int col = 1; col < matrixColSize[row]; ++col) {
            if (matrix[row][col] == 0) {
                matrix[0][col] = matrix[row][0] = 0;
            }
        }
    }
}

void checkAndSetMatrixToZeroes(int **matrix, int matrixSize, int *matrixColSize) {
    checkMatrix(matrix, matrixSize, matrixColSize);
    setColsToZeroes(matrix, matrixSize, matrixColSize);
    setRowsToZeroes(matrix, matrixSize, matrixColSize);
}

void setZeroes(int **matrix, int matrixSize, int *matrixColSize) {
    if (isZeroCol(matrix, 0, matrixSize)) {
        if (isZeroRow(matrix, 0, matrixColSize[0])) {
            checkAndSetMatrixToZeroes(matrix, matrixSize, matrixColSize);
            setRowToZeroes(matrix, 0, matrixColSize[0]);
        } else {
            checkAndSetMatrixToZeroes(matrix, matrixSize, matrixColSize);
        }
        setColToZeroes(matrix, 0, matrixSize);
    } else {
        if (isZeroRow(matrix, 0, matrixColSize[0])) {
            checkAndSetMatrixToZeroes(matrix, matrixSize, matrixColSize);
            setRowToZeroes(matrix, 0, matrixColSize[0]);
        } else {
            checkAndSetMatrixToZeroes(matrix, matrixSize, matrixColSize);
        }
    }
}

#define ROW_COUNT 4
#define COL_COUNT 4

int main() {
    // int m[ROW_COUNT][COL_COUNT] = {{1,1,1},{1,0,1},{1,1,1}};
    // int m[ROW_COUNT][COL_COUNT] = {{0,1,2,0},{3,4,5,2},{1,3,1,5}};
    // int m[ROW_COUNT][COL_COUNT] = {{1,0}};
    // int m[ROW_COUNT][COL_COUNT] = {{1},{0}};
    int m[ROW_COUNT][COL_COUNT] = {{1,2,3,4},{5,0,7,8},{0,10,11,12},{13,14,15,0}};
    // int m[ROW_COUNT][COL_COUNT] = {{8,3,1,4,6,4,0,3,4},{9,1,3,0,1,5,7,4,1},{2,2,5,2147483647,5,4,4,3,8},{4,9,7,0,3,6,9,5,9},{4,1,8,8,4,1,5,7,6}};
    int **mat = (int**)malloc(ROW_COUNT * sizeof(int*));
    int *mcs = (int*)malloc(ROW_COUNT * sizeof(int));
    for (int row = 0; row < ROW_COUNT; ++row) {
        mat[row] = (int*)malloc(COL_COUNT * sizeof(int));
        mcs[row] = COL_COUNT;
        for (int col = 0; col < COL_COUNT; ++col) {
            mat[row][col] = m[row][col];
        }
    }
    for (int row = 0; row < ROW_COUNT; ++row) {
        for (int col = 0; col < mcs[row]; ++col) {
            printf("%*d ", 2, mat[row][col]);
        }
        printf("\n");
    }
    printf("\n");
    setZeroes(mat, ROW_COUNT, mcs);
    for (int row = 0; row < ROW_COUNT; ++row) {
        for (int col = 0; col < mcs[row]; ++col) {
            printf("%d ", mat[row][col]);
        }
        printf("\n");
        free(mat[row]);
    }
    free(mat);
    free(mcs);
    return EXIT_SUCCESS;
}
