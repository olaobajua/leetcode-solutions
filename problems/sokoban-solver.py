"""
 * Shortest Path to Move a Box to Their Target Location
 * Sokoban is a game in which the player pushes boxes around in a warehouse
 * trying to get them to target locations.
 *
 * The game is represented by a grid of size m x n, where each element is a
 * wall, floor, or a box.
 *
 * Your task is move the box 'B' to the target position 'T' under the following
 * rules:
 *     ∙ Player is represented by character 'S' and can move up, down, left,
 *       right in the grid if it is a floor (empy cell).
 *     ∙ Floor is represented by character '.' that means free cell to walk.
 *     ∙ Wall is represented by character '#' that means obstacle (impossible
 *       to walk there).
 *     ∙ There is only one box 'B' and one target cell 'T' in the grid.
 *     ∙ The box can be moved to an adjacent free cell by standing next to the
 *       box and then moving in the direction of the box. This is a push.
 *     ∙ The player cannot walk through the box.
 *
 * Return the shortest path to move the box to the target.
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m <= 20
 *     1 <= n <= 20
 *     grid contains only characters '.', '#',  'S' , 'T', or 'B'.
 *     There is only one character 'S', 'B' and 'T' in the grid.
"""
def solve(grid):
    # state = hero_x, hero_y, box_x, box_y, path
    initial_state = *location(grid, 'S'), *location(grid, 'B'), ()
    target = location(grid, 'T')
    visited = {initial_state[:4]}
    states = (initial_state,)
    while states:
        for s in states:
            if s[2:4] == target:
                return s[4] + (s[:2],)
        states = tuple(ns for s in states for ns in next_states(grid, s)
                       if ns[:4] not in visited)
        visited.update([s[:4] for s in states])
    return None

def location(grid, symbol):
    width = len(grid[0])
    grid = [cell for row in grid for cell in row]
    s = grid.index(symbol)
    return (s // width, s % width)

def next_states(grid, state):
    nstates = ()
    width = len(grid)
    height = len(grid[0])
    sx, sy, bx, by, path = state
    for dx, dy in ((0, 1), (0, -1), (1, 0), (-1, 0)):
        if 0 <= sx + dx < width and 0 <= sy + dy < height and grid[sx + dx][sy + dy] != '#':
            if sx + dx == bx and sy + dy == by:
                if 0 <= sx + 2*dx < width and 0 <= sy + 2*dy < height and grid[sx + 2*dx][sy + 2*dy] != '#':
                    nstates += ((sx + dx, sy + dy, bx + dx, by + dy, path + ((sx, sy),)),)
            else:
                nstates += ((sx + dx, sy + dy, bx, by, path + ((sx, sy),)),)
    return nstates

if __name__ == "__main__":
    tests = (
        [["#","#","#","#","#","#"],
         ["#","T","#","#","#","#"],
         ["#",".",".","B",".","#"],
         ["#",".","#","#",".","#"],
         ["#",".",".",".","S","#"],
         ["#","#","#","#","#","#"]],
        [["#","#","#","#","#","#"],
         ["#","T","#","#","#","#"],
         ["#",".",".","B",".","#"],
         ["#","#","#","#",".","#"],
         ["#",".",".",".","S","#"],
         ["#","#","#","#","#","#"]],
        [["#","#","#","#","#","#"],
         ["#","T",".",".","#","#"],
         ["#",".","#","B",".","#"],
         ["#",".",".",".",".","#"],
         ["#",".",".",".","S","#"],
         ["#","#","#","#","#","#"]],
        [["#","#","#","#","#","#","#"],
         ["#","S","#",".","B","T","#"],
         ["#","#","#","#","#","#","#"]],
        [["#",".",".","#","#","#","#","#"],
         ["#",".",".","T","#",".",".","#"],
         ["#",".",".",".","#","B",".","#"],
         ["#",".",".",".",".",".",".","#"],
         ["#",".",".",".","#",".","S","#"],
         ["#",".",".","#","#","#","#","#"]],
        [["#",".",".",".",".",".",".",".",".","."],
         [".",".",".",".",".","#",".",".",".","#"],
         ["#",".","#",".",".","T",".",".",".","."],
         [".","#",".",".",".",".",".",".",".","."],
         [".",".",".",".",".",".","#",".",".","."],
         [".",".",".","#","#","S",".","B",".","."],
         ["#",".",".",".",".",".",".","#",".","."],
         [".","#",".",".",".",".",".",".",".","."],
         [".",".",".",".",".",".",".",".",".","."],
         [".",".",".",".",".","#",".",".",".","."]],
        [[".",".","#",".",".",".",".",".",".","."],
         [".","#",".","#","B","#",".","#",".","."],
         [".","#",".",".",".",".",".",".","T","."],
         ["#",".",".",".",".",".",".",".",".","."],
         [".",".",".",".",".",".",".",".",".","#"],
         [".",".",".",".",".",".",".",".","#","."],
         [".",".",".","#",".",".","#","#",".","."],
         [".",".",".",".","#",".",".","#",".","."],
         [".","#",".","S",".",".",".",".",".","."],
         ["#",".",".","#",".",".",".",".",".","#"]],
    )
    for grid in tests:
        print(solve(grid))
