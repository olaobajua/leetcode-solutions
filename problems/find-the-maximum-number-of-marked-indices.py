"""
 * 2576. Find the Maximum Number of Marked Indices [Medium]
 * You are given a 0-indexed integer array nums.
 *
 * Initially, all of the indices are unmarked. You are allowed to make this
 * operation any number of times:
 *     ∙ Pick two different unmarked indices i and j such that 2 * nums[i] <=
 *       nums[j], then mark i and j.
 *
 * Return the maximum possible number of marked indices in nums using the
 * above operation any number of times.
 *
 * Example 1:
 * Input: nums = [3,5,2,4]
 * Output: 2
 * Explanation: In the first operation: pick i = 2 and j = 1, the operation is
 * allowed because 2 * nums[2] <= nums[1]. Then mark index 2 and 1.
 * It can be shown that there's no other valid operation so the answer is 2.
 *
 * Example 2:
 * Input: nums = [9,2,5,4]
 * Output: 4
 * Explanation: In the first operation: pick i = 3 and j = 0, the operation is
 * allowed because 2 * nums[3] <= nums[0]. Then mark index 3 and 0.
 * In the second operation: pick i = 1 and j = 2, the operation is allowed
 * because 2 * nums[1] <= nums[2]. Then mark index 1 and 2.
 * Since there is no other operation, the answer is 4.
 *
 * Example 3:
 * Input: nums = [7,6,8]
 * Output: 0
 * Explanation: There is no valid operation to do, so the answer is 0.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i] <= 10⁹
"""
from typing import List

class Solution:
    def maxNumOfMarkedIndices(self, nums: List[int]) -> int:
        def can_mark(k):
            return all(2 * nums[k-i-1] <= nums[n-i-1] for i in range(k))

        nums = sorted(nums)
        n = len(nums)
        lo = 0
        hi = n // 2
        while lo < hi:
            mid = (lo + hi) // 2
            if can_mark(mid):
                lo = mid + 1
            else:
                hi = mid
        return (2 * lo) if can_mark(lo) else 2 * (lo - 1)

if __name__ == "__main__":
    tests = (
        ([3,5,2,4], 2),
        ([9,2,5,4], 4),
        ([7,6,8], 0),
        ([42,83,48,10,24,55,9,100,10,17,17,99,51,32,16,98,99,31,28,68,71,14,64,29,15,40], 26),
        ([1,78,27,48,14,86,79,68,77,20,57,21,18,67,5,51,70,85,47,56,22,79,41,8,39,81,59,74,14,45,49,15,10,28,16,77,22,65,8,36,79,94,44,80,72,8,96,78,39,92,69,55,9,44,26,76,40,77,16,69,40,64,12,48,66,7,59,10], 64),
    )
    for nums, expected in tests:
        print(Solution().maxNumOfMarkedIndices(nums) == expected)
