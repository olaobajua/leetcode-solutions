"""
 * 2580. Count Ways to Group Overlapping Ranges [Medium]
 * You are given a 2D integer array ranges where ranges[i] = [startᵢ, endᵢ]
 * denotes that all integers between startᵢ and endᵢ (both inclusive) are
 * contained in the iᵗʰ range.
 *
 * You are to split ranges into two (possibly empty) groups such that:
 *     ∙ Each range belongs to exactly one group.
 *     ∙ Any two overlapping ranges must belong to the same group.
 *
 * Two ranges are said to be overlapping if there exists at least one integer
 * that is present in both ranges.
 *
 *     ∙ For example, [1, 3] and [2, 5] are overlapping because 2 and 3 occur
 *       in both ranges.
 *
 * Return the total number of ways to split ranges into two groups. Since the
 * answer may be very large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: ranges = [[6,10],[5,15]]
 * Output: 2
 * Explanation:
 * The two ranges are overlapping, so they must be in the same group.
 * Thus, there are two possible ways:
 * - Put both the ranges together in group 1.
 * - Put both the ranges together in group 2.
 *
 * Example 2:
 * Input: ranges = [[1,3],[10,20],[2,5],[4,8]]
 * Output: 4
 * Explanation:
 * Ranges [1,3], and [2,5] are overlapping. So, they must be in the same
 * group.
 * Again, ranges [2,5] and [4,8] are also overlapping. So, they must also be
 * in the same group.
 * Thus, there are four possible ways to group them:
 * - All the ranges in group 1.
 * - All the ranges in group 2.
 * - Ranges [1,3], [2,5], and [4,8] in group 1 and [10,20] in group 2.
 * - Ranges [1,3], [2,5], and [4,8] in group 2 and [10,20] in group 1.
 *
 * Constraints:
 *     ∙ 1 <= ranges.length <= 10⁵
 *     ∙ ranges[i].length == 2
 *     ∙ 0 <= startᵢ <= endᵢ <= 10⁹
"""
from typing import List

class Solution:
    def countWays(self, ranges: List[List[int]]) -> int:
        ranges.sort()
        merged = [ranges[0]]
        for start, end in ranges:
            if start <= merged[-1][1]:
                merged[-1][1] = max(merged[-1][1], end)
            else:
                merged.append([start, end])
        return 2 ** len(merged) % 1000000007

if __name__ == "__main__":
    tests = (
        ([[6,10],[5,15]], 2),
        ([[1,3],[10,20],[2,5],[4,8]], 4),
        ([[0,2],[2,3]], 2),
        ([[0,0],[8,9],[12,13],[1,3]], 16),
        ([[5,11],[20,22],[1,3],[21,22],[11,11]], 8),
        ([[34,56],[28,29],[12,16],[11,48],[28,54],[22,55],[28,41],[41,44]], 2),
        ([[57,92],[139,210],[306,345],[411,442],[533,589],[672,676],[801,831],[937,940],[996,1052],[1113,1156],[1214,1258],[1440,1441],[1507,1529],[1613,1659],[1773,1814],[1826,1859],[2002,2019],[2117,2173],[2223,2296],[2335,2348],[2429,2532],[2640,2644],[2669,2676],[2786,2885],[2923,2942],[3035,3102],[3177,3249],[3310,3339],[3450,3454],[3587,3620],[3725,3744],[3847,3858],[3901,3993],[4100,4112],[4206,4217],[4250,4289],[4374,4446],[4510,4591],[4675,4706],[4732,4768],[4905,4906],[5005,5073],[5133,5142],[5245,5309],[5352,5377],[5460,5517],[5569,5602],[5740,5791],[5823,5888],[6036,6042],[6096,6114],[6217,6262],[6374,6394],[6420,6511],[6564,6587],[6742,6743],[6797,6877],[6909,6985],[7042,7117],[7141,7144],[7276,7323],[7400,7456],[7505,7557],[7690,7720],[7787,7800],[7870,7880],[8013,8031],[8114,8224],[8272,8328],[8418,8435],[8493,8537],[8600,8704],[8766,8812],[8839,8853],[9032,9036],[9108,9189],[9222,9291],[9344,9361],[9448,9502],[9615,9673],[9690,9800],[9837,9868],[85,96],[145,202],[254,304],[372,411],[534,551],[629,692],[727,787],[861,944],[1041,1084],[1133,1174],[1260,1307],[1339,1358],[1478,1548],[1580,1618],[1694,1814],[1848,1891],[1936,1990],[2058,2130]], 570065479),
    )
    for ranges, expected in tests:
        print(Solution().countWays(ranges) == expected)
