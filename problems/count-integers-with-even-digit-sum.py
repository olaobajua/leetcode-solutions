"""
 * 2180. Count Integers With Even Digit Sum (Easy)
 * Given a positive integer num, return the number of positive integers less
 * than or equal to num whose digit sums are even.
 *
 * The digit sum of a positive integer is the sum of all its digits.
 *
 * Example 1:
 * Input: num = 4
 * Output: 2
 * Explanation:
 * The only integers less than or equal to 4 whose digit sums are even are
 * 2 and 4.
 *
 * Example 2:
 * Input: num = 30
 * Output: 14
 * Explanation:
 * The 14 integers less than or equal to 30 whose digit sums are even are
 * 2, 4, 6, 8, 11, 13, 15, 17, 19, 20, 22, 24, 26, and 28.
 *
 * Constraints:
 *     ∙ 1 <= num <= 1000
"""
from typing import List

class Solution:
    def countEven(self, num: int) -> int:
        return (num - sum(map(int, str(num))) % 2) // 2

if __name__ == "__main__":
    tests = (
        (4, 2),
        (30, 14),
    )
    for num, expected in tests:
        print(Solution().countEven(num) == expected)
