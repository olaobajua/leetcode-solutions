"""
 * 1318. Minimum Flips to Make a OR b Equal to c [Medium]
 * Given 3 positives numbers a, b and c. Return the minimum flips required in
 * some bits of a and b to make ( a OR b == c ). (bitwise OR operation).
 * Flip operation consists of change any single bit 1 to 0 or change the bit 0
 * to 1 in their binary representation.
 *
 * Example 1:
 * Input: a = 2, b = 6, c = 5
 * Output: 3
 * Explanation: After flips a = 1 , b = 4 , c = 5 such that (a OR b == c)
 *
 * Example 2:
 * Input: a = 4, b = 2, c = 7
 * Output: 1
 *
 * Example 3:
 * Input: a = 1, b = 2, c = 3
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= a <= 10⁹
 *     ∙ 1 <= b <= 10⁹
 *     ∙ 1 <= c <= 10⁹
"""
class Solution:
    def minFlips(self, a: int, b: int, c: int) -> int:
        ret = 0
        m = max(a, b, c)
        d = 1
        while d <= m:
            if d & c == 0:
                ret += (a & d > 0) + (b & d > 0)
            else:
                ret += (a & d == 0) and (b & d == 0)
            d <<= 1
        return ret

if __name__ == "__main__":
    tests = (
        (2, 6, 5, 3),
        (4, 2, 7, 1),
        (1, 2, 3, 0),
    )
    for a, b, c, expected in tests:
        print(Solution().minFlips(a, b, c) == expected)
