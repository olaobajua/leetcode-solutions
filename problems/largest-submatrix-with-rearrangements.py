"""
 * 1727. Largest Submatrix With Rearrangements [Medium]
 * You are given a binary matrix matrix of size m x n, and you are allowed to
 * rearrange the columns of the matrix in any order.
 *
 * Return the area of the largest submatrix within matrix where every element
 * of the submatrix is 1 after reordering the columns optimally.
 *
 * Example 1:
 * Input: matrix = [[0,0,1],
 *                  [1,1,1],
 *                  [1,0,1]]
 * Output: 4
 * Explanation: You can rearrange the columns as shown above.
 * The largest submatrix of 1s, in bold, has an area of 4.
 *
 * Example 2:
 * Input: matrix = [[1,0,1,0,1]]
 * Output: 3
 * Explanation: You can rearrange the columns as shown above.
 * The largest submatrix of 1s, in bold, has an area of 3.
 *
 * Example 3:
 * Input: matrix = [[1,1,0],
 *                  [1,0,1]]
 * Output: 2
 * Explanation: Notice that you must rearrange entire columns, and there is no
 * way to make a submatrix of 1s larger than an area of 2.
 *
 * Constraints:
 *     ∙ m == matrix.length
 *     ∙ n == matrix[i].length
 *     ∙ 1 <= m * n <= 10⁵
 *     ∙ matrix[i][j] is either 0 or 1.
"""
from typing import List

class Solution:
    def largestSubmatrix(self, matrix: List[List[int]]) -> int:
        m = len(matrix)
        n = len(matrix[0])

        columns = []
        for col in range(n):
            cur = []
            count = 0
            for row in range(m):
                if matrix[row][col] == 1:
                    count += 1
                else:
                    count = 0
                cur.append(count)
            columns.append(cur)

        rows = (sorted(r) for r in zip(*columns))
        return max(h * (n - i) for row in rows for i, h in enumerate(row))

if __name__ == "__main__":
    tests = (
        ([[0,0,1],[1,1,1],[1,0,1]], 4),
        ([[1,0,1,0,1]], 3),
        ([[1,1,0],[1,0,1]], 2),
    )
    for matrix, expected in tests:
        print(Solution().largestSubmatrix(matrix) == expected)
