"""
 * 1379. Find a Corresponding Node of a Binary Tree in a Clone of That
 * Tree (Medium)
 * Given two binary trees original and cloned and given a reference to a node
 * target in the original tree.
 *
 * The cloned tree is a copy of the original tree.
 *
 * Return a reference to the same node in the cloned tree.
 *
 * Note that you are not allowed to change any of the two trees or the target
 * node and the answer must be a reference to a node in the cloned tree.
 *
 * Example 1:
 * Input: tree = [7,4,3,null,null,6,19], target = 3
 * Output: 3
 * Explanation: In all examples the original and cloned trees are shown.
 * The target node is a green node from the original tree. The answer is
 * the yellow node from the cloned tree.
 *
 * Example 2:
 * Input: tree = [7], target =  7
 * Output: 7
 *
 * Example 3:
 * Input: tree = [8,null,6,null,5,null,4,null,3,null,2,null,1], target = 4
 * Output: 4
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ The values of the nodes of the tree are unique.
 *     ∙ target node is a node from the original tree and is not null.
 *
 * Follow up: Could you solve the problem if repeated values on the tree are
 * allowed?
"""
from typing import List

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def getTargetCopy(self, original: TreeNode, cloned: TreeNode, target: TreeNode) -> TreeNode:
        def dfs(node):
            if not node:
                return False
            if node == target:
                return True
            left = dfs(node.left)
            right = dfs(node.right)
            if left or right:
                path.append(not left)
            return left or right

        path = []
        dfs(original)
        for i in reversed(path):
            cloned = cloned.left if i == 0 else cloned.right

        return cloned

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def find_node(root, val):
    if root is None:
        yield root
    elif root.val == val:
        yield root
    else:
        yield from find_node(root.left, val)
        yield from find_node(root.right, val)

if __name__ == "__main__":
    tests = (
        ([7,4,3,None,None,6,19], 3, 3),
        ([7], 7, 7),
        ([8,None,6,None,5,None,4,None,3,None,2,None,1], 4, 4),
    )
    for nums, target, expected in tests:
        original = build_tree(nums.copy())
        cloned = build_tree(nums)
        target = [node for node in find_node(original, target) if node][0]
        ret = Solution().getTargetCopy(original, cloned, target)
        print(ret != target and ret.val == expected)
