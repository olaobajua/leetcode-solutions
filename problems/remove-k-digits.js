/**
 * 402. Remove K Digits (Medium)
 * Given string num representing a non-negative integer num, and an integer k,
 * return the smallest possible integer after removing k digits from num.
 *
 * Example 1:
 * Input: num = "1432219", k = 3
 * Output: "1219"
 * Explanation: Remove the three digits 4, 3, and 2 to form the new number 1219
 * which is the smallest.
 *
 * Example 2:
 * Input: num = "10200", k = 1
 * Output: "200"
 * Explanation: Remove the leading 1 and the number is 200. Note that the
 * output must not contain leading zeroes.
 *
 * Example 3:
 * Input: num = "10", k = 2
 * Output: "0"
 * Explanation: Remove all the digits from the number and it is left with
 * nothing which is 0.
 *
 * Constraints:
 *     ∙ 1 <= k <= num.length <= 10⁵
 *     ∙ num consists of only digits.
 *     ∙ num does not have any leading zeros except for the zero itself.
 */

/**
 * @param {string} num
 * @param {number} k
 * @return {string}
 */
var removeKdigits = function(num, k) {
    const stack = [];
    for (const digit of Array.from(num)) {
        while (k > 0 && stack.length && stack[stack.length - 1] > digit) {
            --k;
            stack.pop();
        }
        stack.push(digit);
    }
    return stack.slice(0, stack.length - k).join('').replace(/^0+/, '') || '0';
};

const tests = [
    ["1432219", 3, "1219"],
    ["10200", 1, "200"],
    ["10", 2, "0"],
    ["9", 1, "0"],
    ["112", 1, "11"],
    ["487416100161", 6, "100161"],
];

for (const [num, k, expected] of tests) {
    console.log(removeKdigits(num, k) === expected);
}
