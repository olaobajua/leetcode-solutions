"""
 * 1992. Find All Groups of Farmland (Medium)
 * You are given a 0-indexed m x n binary matrix land where a 0 represents a
 * hectare of forested land and a 1 represents a hectare of farmland.
 *
 * To keep the land organized, there are designated rectangular areas of
 * hectares that consist entirely of farmland. These rectangular areas are
 * called groups. No two groups are adjacent, meaning farmland in one group is
 * not four-directionally adjacent to another farmland in a different group.
 *
 * land can be represented by a coordinate system where the top left corner of
 * land is (0, 0) and the bottom right corner of land is (m-1, n-1). Find the
 * coordinates of the top left and bottom right corner of each group of
 * farmland. A group of farmland with a top left corner at (r₁, c₁) and a
 * bottom right corner at (r₂, c₂) is represented by the 4-length array
 * [r₁, c₁, r₂, c₂].
 *
 * Return a 2D array containing the 4-length arrays described above for each
 * group of farmland in land. If there are no groups of farmland, return an
 * empty array. You may return the answer in any order.
 *
 * Example 1:
 * Input: land = [[1,0,0],[0,1,1],[0,1,1]]
 * Output: [[0,0,0,0],[1,1,2,2]]
 * Explanation:
 * The first group has a top left corner at land[0][0] and a bottom right
 * corner at land[0][0].
 * The second group has a top left corner at land[1][1] and a bottom right
 * corner at land[2][2].
 *
 * Example 2:
 * Input: land = [[1,1],[1,1]]
 * Output: [[0,0,1,1]]
 * Explanation:
 * The first group has a top left corner at land[0][0] and a bottom right
 * corner at land[1][1].
 *
 * Example 3:
 * Input: land = [[0]]
 * Output: []
 * Explanation:
 * There are no groups of farmland.
 *
 * Constraints:
 *     m == land.length
 *     n == land[i].length
 *     1 <= m, n <= 300
 *     land consists of only 0's and 1's.
 *     Groups of farmland are rectangular in shape.
"""
from typing import List

class Solution:
    def findFarmland(self, land: List[List[int]]) -> List[List[int]]:
        groups = []
        cur_groups = []
        for row in range(len(land)):
            for col in range(len(land[0])):
                if land[row][col] == 1:
                    for i, (r1, c1, r2, c2) in enumerate(cur_groups):
                        if r2 == row and c1 <= col <= c2 + 1:
                            cur_groups[i] = [r1, c1, r2, col]
                            break
                        if r2 + 1 == row and c1 <= col <= c2:
                            cur_groups[i] = [r1, c1, row, c2]
                            break
                    else:
                        cur_groups.append([row, col, row, col])
            for r1, c1, r2, c2 in reversed(cur_groups):
                if r2 < row:
                    cur_groups.remove([r1, c1, r2, c2])
                    groups.append([r1, c1, r2, c2])
        return groups + cur_groups

if __name__ == "__main__":
    tests = (
        ([[1,0,0],[0,1,1],[0,1,1]], [[0,0,0,0],[1,1,2,2]]),
        ([[1,1],[1,1]], [[0,0,1,1]]),
        ([[0]], []),
    )
    for land, expected in tests:
        try:
            for farm_land in Solution().findFarmland(land):
                expected.remove(farm_land)
            print(not expected)
        except ValueError:
            print(False)
