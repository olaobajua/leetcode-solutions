"""
 * 976. Largest Perimeter Triangle [Easy]
 * Given an integer array nums, return the largest perimeter of a triangle
 * with a non-zero area, formed from three of these lengths. If it is
 * impossible to form any triangle of a non-zero area, return 0.
 *
 * Example 1:
 * Input: nums = [2,1,2]
 * Output: 5
 *
 * Example 2:
 * Input: nums = [1,2,1]
 * Output: 0
 *
 * Constraints:
 *     ∙ 3 <= nums.length <= 10⁴
 *     ∙ 1 <= nums[i] <= 10⁶
"""
from typing import List

class Solution:
    def largestPerimeter(self, nums: List[int]) -> int:
        nums.sort()
        for i in reversed(range(3, len(nums) + 1)):
            a, b, c = nums[i-3:i]
            if a + b > c:
                return a + b + c
        return 0

if __name__ == "__main__":
    tests = (
        ([2,1,2], 5),
        ([1,2,1], 0),
        ([3,2,3,4], 10),
    )
    for nums, expected in tests:
        print(Solution().largestPerimeter(nums) == expected)
