"""
 * 417. Pacific Atlantic Water Flow [Medium]
 * There is an m x n rectangular island that borders both the Pacific Ocean
 * and Atlantic Ocean. The Pacific Ocean touches the island's left and top
 * edges, and the Atlantic Ocean touches the island's right and bottom edges.
 *
 * The island is partitioned into a grid of square cells. You are given an m x
 * n integer matrix heights where heights[r][c] represents the height above sea
 * level of the cell at coordinate (r, c).
 *
 * The island receives a lot of rain, and the rain water can flow to
 * neighboring cells directly north, south, east, and west if the neighboring
 * cell's height is less than or equal to the current cell's height. Water can
 * flow from any cell adjacent to an ocean into the ocean.
 *
 * Return a 2D list of grid coordinates result where result[i] = [rᵢ, cᵢ]
 * denotes that rain water can flow from cell (rᵢ, cᵢ) to both the Pacific and
 * Atlantic oceans.
 *
 * Example 1:
 * Input: heights = [[1,2,2,3,5],
 *                   [3,2,3,4,4],
 *                   [2,4,5,3,1],
 *                   [6,7,1,4,5],
 *                   [5,1,1,2,4]]
 * Output: [[0,4],[1,3],[1,4],[2,2],[3,0],[3,1],[4,0]]
 * Explanation: The following cells can flow to the Pacific and Atlantic
 * oceans, as shown below:
 * [0,4]: [0,4] -> Pacific Ocean
 *        [0,4] -> Atlantic Ocean
 * [1,3]: [1,3] -> [0,3] -> Pacific Ocean
 *        [1,3] -> [1,4] -> Atlantic Ocean
 * [1,4]: [1,4] -> [1,3] -> [0,3] -> Pacific Ocean
 *        [1,4] -> Atlantic Ocean
 * [2,2]: [2,2] -> [1,2] -> [0,2] -> Pacific Ocean
 *        [2,2] -> [2,3] -> [2,4] -> Atlantic Ocean
 * [3,0]: [3,0] -> Pacific Ocean
 *        [3,0] -> [4,0] -> Atlantic Ocean
 * [3,1]: [3,1] -> [3,0] -> Pacific Ocean
 *        [3,1] -> [4,1] -> Atlantic Ocean
 * [4,0]: [4,0] -> Pacific Ocean
 *        [4,0] -> Atlantic Ocean
 * Note that there are other possible paths for these cells to flow to the
 * Pacific and Atlantic oceans.
 *
 * Example 2:
 * Input: heights = [[1]]
 * Output: [[0,0]]
 * Explanation: The water can flow from the only cell to the Pacific and
 * Atlantic oceans.
 *
 * Constraints:
 *     ∙ m == heights.length
 *     ∙ n == heights[r].length
 *     ∙ 1 <= m, n <= 200
 *     ∙ 0 <= heights[r][c] <= 10⁵
"""
from collections import defaultdict
from functools import cache
from itertools import product
from typing import List

class Solution:
    def pacificAtlantic(self, heights: List[List[int]]) -> List[List[int]]:
        @cache
        def dfs(row, col):
            ret = set()
            if row == 0 or col == 0:
                ret.add("Pacific")
            if row == m - 1 or col == n - 1:
                ret.add("Atlantic")
            for r, c in list(graph[row, col]):
                graph[row, col].discard((r, c))
                ret |= dfs(r, c)
                graph[row, col].add((r, c))
            return frozenset(ret)

        m = len(heights)
        n = len(heights[0])
        graph = defaultdict(set)
        for r in range(m):
            for c in range(n):
                if r > 0 and heights[r][c] >= heights[r-1][c]:
                    graph[r, c].add((r - 1, c))
                if c > 0 and heights[r][c] >= heights[r][c-1]:
                    graph[r, c].add((r, c - 1))
                if r < m - 1 and heights[r][c] >= heights[r+1][c]:
                    graph[r, c].add((r + 1, c))
                if c < n - 1 and heights[r][c] >= heights[r][c+1]:
                    graph[r, c].add((r, c + 1))
        return [[r, c] for r, c in product(range(m), range(n))
                if dfs(r, c) == {"Pacific", "Atlantic"}]

if __name__ == "__main__":
    tests = (
        ([[1,2,2,3,5],
          [3,2,3,4,4],
          [2,4,5,3,1],
          [6,7,1,4,5],
          [5,1,1,2,4]], [[0,4],[1,3],[1,4],[2,2],[3,0],[3,1],[4,0]]),
        ([[1]], [[0,0]]),
        ([[1,1],
          [1,1]], [[0,0],[0,1],[1,0],[1,1]]),
        ([[10,10,1,11,2,15,17,6,17,10,0,10,18,9,16,13,8,9,12,6,3,2,5,19,4,14],
          [12,19,13,2,7,2,3,8,17,4,2,1,8,13,7,2,10,16,12,3,4,12,4,16,0,12],
          [1,12,9,18,12,16,17,5,13,0,7,13,12,13,6,2,11,19,7,2,6,11,11,0,17,6],
          [1,12,2,0,11,7,7,3,7,13,11,1,11,15,5,13,14,12,4,10,5,16,3,17,18,12],
          [9,16,11,5,9,13,7,18,18,14,19,10,9,4,8,14,4,19,13,1,14,8,0,3,12,10],
          [10,19,9,11,1,18,1,2,1,8,1,5,2,15,14,13,18,18,11,4,15,3,15,12,12,2],
          [0,10,9,2,15,9,12,7,0,0,12,18,9,12,18,4,18,10,3,1,17,14,13,18,9,4],
          [3,19,9,16,16,19,11,19,6,9,18,0,12,11,13,1,15,6,9,18,9,6,3,12,12,2],
          [0,16,15,12,3,19,18,9,5,1,4,3,19,15,1,0,7,10,14,4,8,10,15,16,14,8],
          [15,9,3,15,19,17,17,10,9,8,16,11,3,15,15,9,1,14,11,13,16,7,1,7,13,5],
          [9,19,6,7,19,14,4,18,6,10,19,13,12,7,7,15,6,10,7,8,8,8,19,13,17,14],
          [14,7,1,15,2,6,12,4,2,19,17,17,8,9,19,10,0,12,4,12,4,16,15,18,8,0],
          [4,8,5,8,0,2,19,18,1,7,13,9,13,16,6,15,15,12,18,5,8,11,6,17,5,11],
          [17,16,9,19,12,6,13,19,0,6,7,9,7,13,9,18,5,15,16,8,18,9,6,0,11,14],
          [11,5,13,3,12,19,5,15,2,15,9,16,6,12,8,0,19,19,11,0,16,8,15,15,1,12],
          [15,16,16,19,14,1,2,11,14,8,16,13,2,0,3,8,1,5,4,15,12,5,13,3,5,3]],
         [[0,25],[15,0],[15,1],[15,2],[15,3]]),
         ([[11,3,2,4,14,6,13,18,1,4,12,2,4,1,16],
           [5,11,18,0,15,14,6,17,2,17,19,15,12,3,14],
           [10,2,5,13,11,11,13,19,11,17,14,18,14,3,11],
           [14,2,10,7,5,11,6,11,15,11,6,11,12,3,11],
           [13,1,16,15,8,2,16,10,9,9,10,14,7,15,13],
           [17,12,4,17,16,5,0,4,10,15,15,15,14,5,18],
           [9,13,18,4,14,6,7,8,5,5,6,16,13,7,2],
           [19,9,16,19,16,6,1,11,7,2,12,10,9,18,19],
           [19,5,19,10,7,18,6,10,7,12,14,8,4,11,16],
           [13,3,18,9,16,12,1,0,1,14,2,6,1,16,6],
           [14,1,12,16,7,15,9,19,14,4,16,6,11,15,7],
           [6,15,19,13,3,2,13,7,19,11,13,16,0,16,16],
           [1,5,9,7,12,9,2,18,6,12,1,8,1,10,19],
           [10,11,10,11,3,5,12,0,0,8,15,7,5,13,19],
           [8,1,17,18,3,6,8,15,0,9,8,8,12,5,18],
           [8,3,6,12,18,15,10,10,12,19,16,7,17,17,1],
           [12,13,6,4,12,18,18,9,4,9,13,11,5,3,14],
           [8,4,12,11,2,2,10,3,11,17,14,2,17,4,7],
           [8,0,14,0,13,17,11,0,16,13,15,17,4,8,3],
           [18,15,8,11,18,3,10,18,3,3,15,9,11,15,15]],
          [[0,14],[1,14],[2,14],[3,14],[4,13],[4,14],[5,14],[19,0]]),
    )
    for heights, expected in tests:
        print(set(map(tuple, Solution().pacificAtlantic(heights))) ==
              set(map(tuple, expected)))
