/**
 * 147. Insertion Sort List (Medium)
 * Given the head of a singly linked list, sort the list using insertion sort,
 * and return the sorted list's head.
 *
 * The steps of the insertion sort algorithm:
 *     ∙ Insertion sort iterates, consuming one input element each repetition
 *       and growing a sorted output list.
 *     ∙ At each iteration, insertion sort removes one element from the input
 *       data, finds the location it belongs within the sorted list and inserts
 *       it there.
 *     ∙ It repeats until no input elements remain.
 *
 * The following is a graphical example of the insertion sort algorithm. The
 * partially sorted list (black) initially contains only the first element in
 * the list. One element (red) is removed from the input data and inserted
 * in-place into the sorted list with each iteration.
 *
 * Example 1:
 * Input: head = [4,2,1,3]
 * Output: [1,2,3,4]
 *
 * Example 2:
 * Input: head = [-1,5,3,4,0]
 * Output: [-1,0,3,4,5]
 *
 * Constraints:
 *     The number of nodes in the list is in the range [1, 5000].
 *     -5000 <= Node.val <= 5000
 */

// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var insertionSortList = function(head) {
    const start = new ListNode();
    while (head) {
        const nxt = head.next;
        let p = start, n = start.next;
        while (n && n.val < head.val) {
            [p, n] = [n, n.next];
        }
        [p.next, head.next, head] = [head, n, nxt];
    }
    return start.next;
};

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    [[1], [1]],
    [[1,2,3,4], [1,2,3,4]],
    [[7,6,5,4,3,2,1], [1,2,3,4,5,6,7]],
    [[4,2,1,3], [1,2,3,4]],
    [[-1,5,3,4,0], [-1,0,3,4,5]],
    [[4,19,14,5,-3,1,8,5,11,15], [-3,1,4,5,5,8,11,14,15,19]],
];

for (const [nums, expected] of tests) {
    const root = arrayToLinkedList(nums);
    console.log(linkedListToArray(insertionSortList(root))
                .every((x, i) => x == expected[i]));
}
