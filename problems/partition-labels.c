/**
 * 763. Partition Labels (Medium)
 * You are given a string s. We want to partition the string into as many parts
 * as possible so that each letter appears in at most one part.
 *
 * Note that the partition is done so that after concatenating all the parts in
 * order, the resultant string should be s.
 *
 * Return a list of integers representing the size of these parts.
 *
 * Example 1:
 * Input: s = "ababcbacadefegdehijhklij"
 * Output: [9,7,8]
 * Explanation:
 * The partition is "ababcbaca", "defegde", "hijhklij".
 * This is a partition so that each letter appears in at most one part.
 * A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it
 * splits s into less parts.
 *
 * Example 2:
 * Input: s = "eccbbbbdec"
 * Output: [10]
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 500
 *     ∙ s consists of lowercase English letters.
 */
int* partitionLabels(char *s, int *returnSize) {
    int last[26] = {0};
    for (int i = 0; s[i] != '\0'; ++i) {
        last[s[i]-'a'] = i;
    }
    int start = -1;
    int end = last[s[0]-'a'];
    static int ret[500];
    *returnSize = 0;
    for (int i = 0; s[i] != '\0'; ++i) {
        end = fmax(end, last[s[i]-'a']);
        if (end == i) {
            ret[(*returnSize)++] = end - start;
            start = i;
        }
    };
    return ret;
}
