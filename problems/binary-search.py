"""
 * 704. Binary Search [Easy]
 * Given an array of integers nums which is sorted in ascending order, and an
 * integer target, write a function to search target in nums. If target exists,
 * then return its index. Otherwise, return -1.
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 * Example 1:
 * Input: nums = [-1,0,3,5,9,12], target = 9
 * Output: 4
 * Explanation: 9 exists in nums and its index is 4
 *
 * Example 2:
 * Input: nums = [-1,0,3,5,9,12], target = 2
 * Output: -1
 * Explanation: 2 does not exist in nums so return -1
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁴
 *     ∙ -10⁴ < nums[i], target < 10⁴
 *     ∙ All the integers in nums are unique.
 *     ∙ nums is sorted in ascending order.
"""
from typing import List

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        lo = 0
        hi = len(nums)
        while lo < hi:
            mid = (lo + hi) // 2
            if nums[mid] < target:
                lo = mid + 1
            elif nums[mid] > target:
                hi = mid
            else:
                return mid
        return -1

if __name__ == "__main__":
    tests = (
        ([-1,0,3,5,9,12], 9, 4),
        ([-1,0,3,5,9,12], 2, -1),
        ([-1,0,3,5,9,12], 13, -1),
    )
    for nums, target, expected in tests:
        print(Solution().search(nums, target) == expected)
