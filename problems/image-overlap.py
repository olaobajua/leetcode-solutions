"""
 * 835. Image Overlap [Medium]
 * You are given two images, img1 and img2, represented as binary, square
 * matrices of size n x n. A binary matrix has only 0s and 1s as values.
 *
 * We translate one image however we choose by sliding all the 1 bits left,
 * right, up, and/or down any number of units. We then place it on top of the
 * other image. We can then calculate the overlap by counting the number of
 * positions that have a 1 in both images.
 *
 * Note also that a translation does not include any kind of rotation. Any 1
 * bits that are translated outside of the matrix borders are erased.
 *
 * Return the largest possible overlap.
 *
 * Example 1:
 * Input: img1 = [[1,1,0],[0,1,0],[0,1,0]], img2 = [[0,0,0],[0,1,1],[0,0,1]]
 * Output: 3
 * Explanation: We translate img1 to right by 1 unit and down by 1 unit.
 *
 * Example 2:
 * Input: img1 = [[1]], img2 = [[1]]
 * Output: 1
 *
 * Example 3:
 * Input: img1 = [[0]], img2 = [[0]]
 * Output: 0
 *
 * Constraints:
 *     ∙ n == img1.length == img1[i].length
 *     ∙ n == img2.length == img2[i].length
 *     ∙ 1 <= n <= 30
 *     ∙ img1[i][j] is either 0 or 1.
 *     ∙ img2[i][j] is either 0 or 1.
"""
import numpy as np
from scipy.ndimage import convolve
from typing import List

class Solution:
    def largestOverlap(self, img1: List[List[int]], img2: List[List[int]]) -> int:
        img2 = np.pad(img2, len(img1), mode='constant', constant_values=(0, 0))
        return np.amax(convolve(img2, np.flip(np.flip(img1, 1), 0), mode='constant'))

if __name__ == "__main__":
    tests = (
        ([[1,1,0],
          [0,1,0],
          [0,1,0]],
         [[0,0,0],
          [0,1,1],
          [0,0,1]], 3),
        ([[1]], [[1]], 1),
        ([[0]], [[0]], 0),
        ([[0,0,1],
          [0,0,0],
          [0,0,0]],
         [[0,0,0],
          [0,0,0],
          [1,0,0]], 1),
    )
    for img1, img2, expected in tests:
        print(Solution().largestOverlap(img1, img2) == expected)
