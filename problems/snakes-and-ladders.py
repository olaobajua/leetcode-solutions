"""
 * 909. Snakes and Ladders [Medium]
 * You are given an n x n integer matrix board where the cells are labeled
 * from 1 to n² in a Boustrophedon style
 * (https://en.wikipedia.org/wiki/Boustrophedon) starting from the bottom left
 * of the board (i.e. board[n - 1][0]) and alternating direction each row.
 *
 * You start on square 1 of the board. In each move, starting from square
 * curr, do the following:
 *     ∙ Choose a destination square next with a label in the range
 *       [curr + 1, min(curr + 6, n²)].
 * 	    ∙ This choice simulates the result of a standard 6-sided die roll:
 *        i.e., there are always at most 6 destinations, regardless of the size
 *        of the board.
 *     ∙ If next has a snake or ladder, you must move to the destination of
 *       that snake or ladder. Otherwise, you move to next.
 *     ∙ The game ends when you reach the square n².
 *
 * A board square on row r and column c has a snake or ladder if
 * board[r][c] != -1. The destination of that snake or ladder is board[r][c].
 * Squares 1 and n² do not have a snake or ladder.
 *
 * Note that you only take a snake or ladder at most once per move. If the
 * destination to a snake or ladder is the start of another snake or ladder,
 * you do not follow the subsequent snake or ladder.
 *     ∙ For example, suppose the board is [[-1,4],[-1,3]], and on the first
 *       move, your destination square is 2. You follow the ladder to square 3,
 *       but do not follow the subsequent ladder to 4.
 *
 * Return the least number of moves required to reach the square n². If it is
 * not possible to reach the square, return -1.
 *
 * Example 1:
 * Input: board = [[-1,-1,-1,-1,-1,-1],
 *                 [-1,-1,-1,-1,-1,-1],
 *                 [-1,-1,-1,-1,-1,-1],
 *                 [-1,35,-1,-1,13,-1],
 *                 [-1,-1,-1,-1,-1,-1],
 *                 [-1,15,-1,-1,-1,-1]]
 * Output: 4
 * Explanation:
 * In the beginning, you start at square 1 (at row 5, column 0).
 * You decide to move to square 2 and must take the ladder to square 15.
 * You then decide to move to square 17 and must take the snake to square 13.
 * You then decide to move to square 14 and must take the ladder to square 35.
 * You then decide to move to square 36, ending the game.
 * This is the lowest possible number of moves to reach the last square, so
 * return 4.
 *
 * Example 2:
 * Input: board = [[-1,-1],
 *                 [-1,3]]
 * Output: 1
 *
 * Constraints:
 *     ∙ n == board.length == board[i].length
 *     ∙ 2 <= n <= 20
 *     ∙ grid[i][j] is either -1 or in the range [1, n²].
 *     ∙ The squares labeled 1 and n² do not have any ladders or snakes.
"""
from typing import List

class Solution:
    def snakesAndLadders(self, board: List[List[int]]) -> int:
        road = [None]
        for i, row in enumerate(reversed(board)):
            road.extend(reversed(row) if i & 1 else row)
        n = len(road) - 1
        to_visit = [(1, 0)]
        visited = {1}
        for cur, moves in to_visit:
            if cur == n:
                return moves
            for i in range(cur + 1, min(cur + 6, n) + 1):
                nxt = i if road[i] == -1 else road[i]
                if nxt not in visited:
                    visited.add(nxt)
                    to_visit.append((nxt, moves + 1))
        return -1

if __name__ == "__main__":
    tests = (
        ([[-1,-1,-1,-1,-1,-1],
          [-1,-1,-1,-1,-1,-1],
          [-1,-1,-1,-1,-1,-1],
          [-1,35,-1,-1,13,-1],
          [-1,-1,-1,-1,-1,-1],
          [-1,15,-1,-1,-1,-1]], 4),
        ([[-1,-1],
          [-1,3]], 1),
        ([[1,1,-1],
          [1,1,1],
          [-1,1,1]], -1),
        ([[-1,1,2,-1],
          [2,13,15,-1],
          [-1,10,-1,-1],
          [-1,6,2,8]], 2),
    )
    for board, expected in tests:
        print(Solution().snakesAndLadders(board) == expected)
