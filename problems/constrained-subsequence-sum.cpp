/**
 * 1425. Constrained Subsequence Sum (Hard)
 * Given an integer array nums and an integer k, return the maximum sum of a
 * non-empty subsequence of that array such that for every two consecutive
 * integers in the subsequence, nums[i] and nums[j], where i < j,
 * the condition j - i <= k is satisfied.
 *
 * A subsequence of an array is obtained by deleting some number of elements
 * (can be zero) from the array, leaving the remaining elements in their
 * original order.
 *
 * Example 1:
 * Input: nums = [10,2,-10,5,20], k = 2
 * Output: 37
 * Explanation: The subsequence is [10, 2, 5, 20].
 *
 * Example 2:
 * Input: nums = [-1,-2,-3], k = 1
 * Output: -1
 * Explanation: The subsequence must be non-empty, so we choose the largest
 * number.
 *
 * Example 3:
 * Input: nums = [10,-2,-10,-5,20], k = 2
 * Output: 23
 * Explanation: The subsequence is [10, -2, -5, 20].
 *
 * Constraints:
 *     ∙ 1 <= k <= nums.length <= 10⁵
 *     ∙ -10⁴ <= nums[i] <= 10⁴
 */
class Solution {
public:
    int constrainedSubsetSum(vector<int>& nums, int k) {
        int n = nums.size();
        vector<pair<int, int>> heap;
        int ret = nums[0];
        heap.push_back(pair<int, int>(nums[0], 0));
        push_heap(heap.begin(), heap.end());
        for (int i = 1; i < n; ++i) {
            while (heap[0].second < i - k) {
                ret = max(ret, heap[0].first);
                pop_heap(heap.begin(), heap.end());
                heap.pop_back();
            }
            heap.push_back(pair<int, int>(nums[i] + max(0, heap[0].first), i));
            push_heap(heap.begin(), heap.end());
        }
        while (heap.size()) {
            ret = max(ret, heap[0].first);
            pop_heap(heap.begin(), heap.end());
            heap.pop_back();
        }
        return ret;
    }
};
