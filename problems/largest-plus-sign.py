"""
764. Largest Plus Sign (Medium)
You are given an integer n. You have an n x n binary grid grid with all values initially 1's except for some indices given in the array mines. The ith element of the array mines is defined as mines[i] = [xᵢ, yᵢ] where grid[xᵢ][yᵢ] == 0.

Return the order of the largest axis-aligned plus sign of 1's contained in grid. If there is none, return 0.

An axis-aligned plus sign of 1's of order k has some center grid[r][c] == 1 along with four arms of length k - 1 going up, down, left, and right, and made of 1's. Note that there could be 0's or 1's beyond the arms of the plus sign, only the relevant area of the plus sign is checked for 1's.

Example 1:
Input: n = 5, mines = [[4,2]]
Output: 2
Explanation: In the above grid, the largest plus sign can only be of order 2. One of them is shown.

Example 2:
Input: n = 1, mines = [[0,0]]
Output: 0
Explanation: There is no plus sign, so return 0.

Constraints:
    1 <= n <= 500
    1 <= mines.length <= 5000
    0 <= xᵢ, yᵢ < n
    All the pairs (xᵢ, yᵢ) are unique.
"""
from typing import List

class Solution:
    def orderOfLargestPlusSign(self, n: int, mines: List[List[int]]) -> int:
        grid = [[1] * n for _ in range(n)]
        for r, c in mines:
            grid[r][c] = 0
        left = [[0] * n for _ in range(n)]
        right = [[0] * n for _ in range(n)]
        up = [[0] * n for _ in range(n)]
        down = [[0] * n for _ in range(n)]
        for r in range(n):
            for c in range(n):
                if grid[r][c]:
                    left[r][c] = left[r][c - 1] + 1
                    up[r][c] = up[r - 1][c] + 1
                if grid[r][n - c - 1]:
                    right[r][n - c - 1] = right[r][(n - c) % n] + 1
                if grid[n - r - 1][c]:
                    down[n - r - 1][c] = down[(n - r) % n][c] + 1
        return max((min(left[r][c], right[r][c], up[r][c], down[r][c])
                    for r in range(n) for c in range(n)))

if __name__ == "__main__":
    tests = (
        (5, [[4,2]], 2),
        (1, [[0,0]], 0),
    )
    for n, mines, expected in tests:
        print(Solution().orderOfLargestPlusSign(n, mines) == expected)
