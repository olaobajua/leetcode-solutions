"""
 * 954. Array of Doubled Pairs (Medium)
 * Given an array of integers arr of even length, return true if and only if it
 * is possible to reorder it such that arr[2 * i + 1] = 2 * arr[2 * i] for
 * every 0 <= i < len(arr) / 2.
 *
 * Example 1:
 * Input: arr = [3,1,3,6]
 * Output: false
 *
 * Example 2:
 * Input: arr = [2,1,2,6]
 * Output: false
 *
 * Example 3:
 * Input: arr = [4,-2,2,-4]
 * Output: true
 * Explanation: We can take two groups, [-2,-4] and [2,4] to
 * form [-2,-4,2,4] or [2,4,-2,-4].
 *
 * Example 4:
 * Input: arr = [1,2,4,16,8,4]
 * Output: false
 *
 * Constraints:
 *
 *     0 <= arr.length <= 3 * 10⁴
 *     arr.length is even.
 *     -10⁵ <= arr[i] <= 10⁵
"""
from typing import List
from collections import Counter

class Solution:
    def canReorderDoubled(self, arr: List[int]) -> bool:
        counter = Counter(arr)
        for x in sorted(arr, key=abs):
            if counter[x] > 0:
                if x * 2 in counter and counter[x * 2] > 0:
                    counter[x] -= 1
                    counter[x * 2] -= 1
                else:
                    return False
        return True

if __name__ == "__main__":
    tests = (
        ([3,1,3,6], False),
        ([2,1,2,6], False),
        ([4,-2,2,-4], True),
        ([1,2,4,16,8,4], False),
        ([1,2,1,-8,8,-4,4,-4,2,-2], True),
        ([-4,-6,-1,-2,-1,-1,-3,-8], False),
    )
    for arr, expected in tests:
        print(Solution().canReorderDoubled(arr) == expected)
