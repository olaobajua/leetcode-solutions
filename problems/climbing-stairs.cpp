/**
 * 70. Climbing Stairs [Easy]
 * You are climbing a staircase. It takes n steps to reach the top.
 *
 * Each time you can either climb 1 or 2 steps. In how many distinct ways can
 * you climb to the top?
 *
 * Example 1:
 * Input: n = 2
 * Output: 2
 * Explanation: There are two ways to climb to the top.
 * 1. 1 step + 1 step
 * 2. 2 steps
 *
 * Example 2:
 * Input: n = 3
 * Output: 3
 * Explanation: There are three ways to climb to the top.
 * 1. 1 step + 1 step + 1 step
 * 2. 1 step + 2 steps
 * 3. 2 steps + 1 step
 *
 * Constraints:
 *     ∙ 1 <= n <= 45
 */
#include <iostream>
#include <vector>

class Solution {
public:
    int climbStairs(int n) {
        cache = std::vector(n, -1);
        return dp(0, n);
    }
private:
    std::vector<int> cache;

    int dp(int i, int n) {
        if (i >= n - 1) {
            return 1;
        }

        if (cache[i] > -1) {
            return cache[i];
        }

        cache[i] = dp(i + 1, n) + dp(i + 2, n);

        return cache[i];
    }
};
