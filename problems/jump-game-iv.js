/**
 * 1345. Jump Game IV (Hard)
 * Given an array of integers arr, you are initially positioned at the first
 * index of the array.
 *
 * In one step you can jump from index i to index:
 *     ∙ i + 1 where: i + 1 < arr.length.
 *     ∙ i - 1 where: i - 1 >= 0.
 *     ∙ j where: arr[i] == arr[j] and i != j.
 *
 * Return the minimum number of steps to reach the last index of the array.
 *
 * Notice that you can not jump outside of the array at any time.
 *
 * Example 1:
 * Input: arr = [100,-23,-23,404,100,23,23,23,3,404]
 * Output: 3
 * Explanation: You need three jumps from index 0 --> 4 --> 3 --> 9. Note that
 * index 9 is the last index of the array.
 *
 * Example 2:
 * Input: arr = [7]
 * Output: 0
 * Explanation: Start index is the last index. You do not need to jump.
 *
 * Example 3:
 * Input: arr = [7,6,9,6,9,6,9,7]
 * Output: 1
 * Explanation: You can jump directly from index 0 to index 7 which is last
 * index of the array.
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 5 * 10⁴
 *     ∙ -10⁸ <= arr[i] <= 10⁸
 */

/**
 * @param {number[]} arr
 * @return {number}
 */
var minJumps = function(arr) {
    const n = arr.length;
    const byValue = {};
    arr.forEach((x, i) => {
        byValue[x] ? byValue[x].push(i) : byValue[x] = [i];
    });
    let toVisit = [0];
    let steps = 0;
    while (true) {
        const nextVisit = [];
        for (const i of toVisit) {
            if (i === n - 1) {
                return steps;
            }
            const num = arr[i];
            arr[i] = null;  // mark visited
            if (i - 1 > 0 && arr[i-1] !== null) {
                nextVisit.push(i - 1);
            }
            if (arr[i+1] !== null) {
                nextVisit.push(i + 1);
            }
            (byValue[num] || []).forEach(i => {
                if (arr[i] !== null) {
                    nextVisit.push(i);
                }
            });
            delete byValue[num];  // remove visited
        }
        toVisit = nextVisit;
        ++steps;
    }
};

const tests = [
    [[100,-23,-23,404,100,23,23,23,3,404], 3],
    [[7], 0],
    [[7,6,9,6,9,6,9,7], 1],
    [[6,1,9], 2],
];

for (const [arr, expected] of tests) {
    console.log(minJumps(arr) == expected);
}
