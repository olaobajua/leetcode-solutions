/**
 * 1239. Maximum Length of a Concatenated String with Unique Characters
 * [Medium]
 * You are given an array of strings arr. A string s is formed by the
 * concatenation of a subsequence of arr that has unique characters.
 *
 * Return the maximum possible length of s.
 *
 * A subsequence is an array that can be derived from another array by
 * deleting some or no elements without changing the order of the remaining
 * elements.
 *
 * Example 1:
 * Input: arr = ["un","iq","ue"]
 * Output: 4
 * Explanation: All the valid concatenations are:
 * - ""
 * - "un"
 * - "iq"
 * - "ue"
 * - "uniq" ("un" + "iq")
 * - "ique" ("iq" + "ue")
 * Maximum length is 4.
 *
 * Example 2:
 * Input: arr = ["cha","r","act","ers"]
 * Output: 6
 * Explanation: Possible longest valid concatenations are "chaers" ("cha" +
 * "ers") and "acters" ("act" + "ers").
 *
 * Example 3:
 * Input: arr = ["abcdefghijklmnopqrstuvwxyz"]
 * Output: 26
 * Explanation: The only string in arr has all 26 characters.
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 16
 *     ∙ 1 <= arr[i].length <= 26
 *     ∙ arr[i] contains only lowercase English letters.
 */
#include <vector>
#include <string>
#include <unordered_set>
#include <algorithm>

class Solution {
public:
    int maxLength(vector<string>& arr) {
        arr.erase(std::remove_if(arr.begin(), arr.end(),
            [](const std::string& s) { return s.size() != std::unordered_set<char>(s.begin(), s.end()).size(); }),
            arr.end());

        int n = arr.size();
        int ret = 0;
        for (int mask = 0; mask < (1 << n); ++mask) {
            std::vector<std::string> selectedStrings;
            for (int i = 0; i < n; ++i) {
                if (mask & (1 << i)) {
                    selectedStrings.push_back(arr[i]);
                }
            }

            std::vector<int> count(26, 0);
            bool hasDups = false;
            for (const std::string& s : selectedStrings) {
                for (char c : s) {
                    ++count[c-'a'];
                    if (count[c-'a'] > 1) {
                        hasDups = true;
                        break;
                    }
                }
                if (hasDups) {
                    break;
                }
            }

            if (!hasDups) {
                ret = std::max(ret, std::reduce(count.begin(), count.end()));
            }
        }

        return ret;
    }
};
