"""
 * 1975. Maximum Matrix Sum (Medium)
 * You are given an n x n integer matrix. You can do the following operation
 * any number of times:
 *     Choose any two adjacent elements of matrix and multiply each of them by -1.
 *
 * Two elements are considered adjacent if and only if they share a border.
 *
 * Your goal is to maximize the summation of the matrix's elements. Return the
 * maximum sum of the matrix's elements using the operation mentioned above.
 *
 * Example 1:
 * Input: matrix = [[1,-1],[-1,1]]
 * Output: 4
 * Explanation: We can follow the following steps to reach sum equals 4:
 * - Multiply the 2 elements in the first row by -1.
 * - Multiply the 2 elements in the first column by -1.
 *
 * Example 2:
 * Input: matrix = [[1,2,3],[-1,-2,-3],[1,2,3]]
 * Output: 16
 * Explanation: We can follow the following step to reach sum equals 16:
 * - Multiply the 2 last elements in the second row by -1.
 *
 * Constraints:
 *     n == matrix.length == matrix[i].length
 *     2 <= n <= 250
 *     -10⁵ <= matrix[i][j] <= 10⁵
"""
from typing import List

class Solution:
    def maxMatrixSum(self, matrix: List[List[int]]) -> int:
        flat_matrix = [cell for row in matrix for cell in row]
        negative_count = len([x for x in flat_matrix if x < 0])
        flat_matrix = tuple(map(abs, flat_matrix))
        n = min(flat_matrix) if negative_count % 2 else 0
        return sum(flat_matrix) - 2 * n

if __name__ == "__main__":
    tests = (
        ([[1,-1],[-1,1]], 4),
        ([[1,2,3],[-1,-2,-3],[1,2,3]], 16),
    )
    for matrix, expected in tests:
        print(Solution().maxMatrixSum(matrix) == expected)
