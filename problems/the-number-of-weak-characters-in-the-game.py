"""
 * 1996. The Number of Weak Characters in the Game [Medium]
 * You are playing a game that contains multiple characters, and each of the
 * characters has two main properties: attack and defense. You are given a 2D
 * integer array properties where properties[i] = [attackᵢ, defenseᵢ]
 * represents the properties of the iᵗʰ character in the game.
 *
 * A character is said to be weak if any other character has both attack and
 * defense levels strictly greater than this character's attack and defense
 * levels. More formally, a character i is said to be weak if there exists
 * another character j where attackⱼ > attackᵢ and defenseⱼ > defenseᵢ.
 *
 * Return the number of weak characters.
 *
 * Example 1:
 * Input: properties = [[5,5],[6,3],[3,6]]
 * Output: 0
 * Explanation: No character has strictly greater attack and defense than the
 * other.
 *
 * Example 2:
 * Input: properties = [[2,2],[3,3]]
 * Output: 1
 * Explanation: The first character is weak because the second character has a
 * strictly greater attack and defense.
 *
 * Example 3:
 * Input: properties = [[1,5],[10,4],[4,3]]
 * Output: 1
 * Explanation: The third character is weak because the second character has a
 * strictly greater attack and defense.
 *
 * Constraints:
 *     ∙ 2 <= properties.length <= 10⁵
 *     ∙ properties[i].length == 2
 *     ∙ 1 <= attackᵢ, defenseᵢ <= 10⁵
"""
from typing import List

class Solution:
    def numberOfWeakCharacters(self, properties: List[List[int]]) -> int:
        prev_max_defense = counter = 0
        for _, defense in sorted(properties, key=lambda x: (-x[0], x[1])):
            if defense < prev_max_defense:
                counter += 1
            else:
                prev_max_defense = defense

        return counter

if __name__ == "__main__":
    tests = (
        ([[5,5],[6,3],[3,6]], 0),
        ([[2,2],[3,3]], 1),
        ([[1,5],[10,4],[4,3]], 1),
        ([[1,1],[2,2],[3,3],[3,4]], 2),
        ([[7,9],[10,7],[6,9],[10,4],[7,5],[7,10]], 2),
        ([[10,1],[5,1],[7,10],[4,1],[5,9],[6,9],[7,2],[1,10]], 4),
    )
    for properties, expected in tests:
        print(Solution().numberOfWeakCharacters(properties) == expected)
