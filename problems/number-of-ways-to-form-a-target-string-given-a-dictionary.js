/*
 * 1639. Number of Ways to Form a Target String Given a Dictionary [Hard]
 * You are given a list of strings of the same length words and a string
 * target.
 *
 * Your task is to form target using the given words under the following
 * rules:
 *     ∙ target should be formed from left to right.
 *     ∙ To form the iᵗʰ character (0-indexed) of target, you can choose the
 *       kᵗʰ character of the jᵗʰ string in words if target[i] = words[j][k].
 *     ∙ Once you use the kᵗʰ character of the jᵗʰ string of words, you can no
 *       longer use the xᵗʰ character of any string in words where x <= k. In
 *       other words, all characters to the left of or at index k become
 *       unusuable for every string.
 *     ∙ Repeat the process until you form the string target.
 *
 * Notice that you can use multiple characters from the same string in words
 * provided the conditions above are met.
 *
 * Return the number of ways to form target from words. Since the answer may
 * be too large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: words = ["acca","bbbb","caca"], target = "aba"
 * Output: 6
 * Explanation: There are 6 ways to form target.
 * "aba" -> index 0 ("acca"), index 1 ("bbbb"), index 3 ("caca")
 * "aba" -> index 0 ("acca"), index 2 ("bbbb"), index 3 ("caca")
 * "aba" -> index 0 ("acca"), index 1 ("bbbb"), index 3 ("acca")
 * "aba" -> index 0 ("acca"), index 2 ("bbbb"), index 3 ("acca")
 * "aba" -> index 1 ("caca"), index 2 ("bbbb"), index 3 ("acca")
 * "aba" -> index 1 ("caca"), index 2 ("bbbb"), index 3 ("caca")
 *
 * Example 2:
 * Input: words = ["abba","baab"], target = "bab"
 * Output: 4
 * Explanation: There are 4 ways to form target.
 * "bab" -> index 0 ("baab"), index 1 ("baab"), index 2 ("abba")
 * "bab" -> index 0 ("baab"), index 1 ("baab"), index 3 ("baab")
 * "bab" -> index 0 ("baab"), index 2 ("baab"), index 3 ("baab")
 * "bab" -> index 1 ("abba"), index 2 ("baab"), index 3 ("baab")
 *
 * Constraints:
 *     ∙ 1 <= words.length <= 1000
 *     ∙ 1 <= words[i].length <= 1000
 *     ∙ All strings in words have the same length.
 *     ∙ 1 <= target.length <= 1000
 *     ∙ words[i] and target contain only lowercase English letters.
 */
/**
 * @param {string[]} words
 * @param {string} target
 * @return {number}
 */
var numWays = function(words, target) {
    const n = target.length;
    const m = words[0].length;
    const MOD = 1000000007;
    const count = Array.from({ length: m }, () => ({}));

    for (let j = 0; j < m; j++) {
      for (const word of words) {
        const char = word[j];
        count[j][char] = (count[j][char] || 0) + 1;
      }
    }

    const cache = new Map();
    const dfs = (i, j) => {
      if (i === n) {
        return 1;
      }
      if (j === m) {
        return 0;
      }
      const key = `${i}-${j}`;
      if (cache.has(key)) {
        return cache.get(key);
      }
      const char = target[i];
      const cur = count[j][char] || 0;
      const result = ((cur * dfs(i + 1, j + 1)) % MOD + dfs(i, j + 1)) % MOD;
      cache.set(key, result);
      return result;
    };

    return dfs(0, 0);
};

const tests = [
    [["acca","bbbb","caca"], "aba", 6],
    [["abba","baab"], "bab", 4],
    [["cbabddddbc","addbaacbbd","cccbacdccd","cdcaccacac","dddbacabbd","bdbdadbccb","ddadbacddd","bbccdddadd","dcabaccbbd","ddddcddadc","bdcaaaabdd","adacdcdcdd","cbaaadbdbb","bccbabcbab","accbdccadd","dcccaaddbc","cccccacabd","acacdbcbbc","dbbdbaccca","bdbddbddda","daabadbacb","baccdbaada","ccbabaabcb","dcaabccbbb","bcadddaacc","acddbbdccb","adbddbadab","dbbcdcbcdd","ddbabbadbb","bccbcbbbab","dabbbdbbcb","dacdabadbb","addcbbabab","bcbbccadda","abbcacadac","ccdadcaada","bcacdbccdb"], "bcbbcccc", 677452090],
];

for (const [words, target, expected] of tests) {
    console.log(numWays(words, target) === expected);
}
