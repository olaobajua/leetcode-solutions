"""
 * 1976. Number of Ways to Arrive at Destination (Medium)
 * You are in a city that consists of n intersections numbered from 0 to n - 1
 * with bi-directional roads between some intersections. The inputs are
 * generated such that you can reach any intersection from any other
 * intersection and that there is at most one road between any two
 * intersections.
 *
 * You are given an integer n and a 2D integer array roads where
 * roads[i] = [uᵢ, vᵢ, timeᵢ] means that there is a road between intersections
 * uᵢ and vᵢ that takes timei minutes to travel. You want to know in how many
 * ways you can travel from intersection 0 to intersection n - 1 in the
 * shortest amount of time.
 *
 * Return the number of ways you can arrive at your destination in the shortest
 * amount of time. Since the answer may be large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 7, roads = [[0,6,7],[0,1,2],[1,2,3],[1,3,3],[6,3,3],[3,5,1],
 *                        [6,5,1],[2,5,1],[0,4,5],[4,6,2]]
 * Output: 4
 * Explanation: The shortest amount of time it takes to go from intersection 0
 * to intersection 6 is 7 minutes.
 * The four ways to get there in 7 minutes are:
 * - 0 ➝ 6
 * - 0 ➝ 4 ➝ 6
 * - 0 ➝ 1 ➝ 2 ➝ 5 ➝ 6
 * - 0 ➝ 1 ➝ 3 ➝ 5 ➝ 6
 *
 * Example 2:
 * Input: n = 2, roads = [[1,0,10]]
 * Output: 1
 * Explanation: There is only one way to go from intersection 0 to intersection
 * 1, and it takes 10 minutes.
 *
 * Constraints:
 *     1 <= n <= 200
 *     n - 1 <= roads.length <= n * (n - 1) / 2
 *     roads[i].length == 3
 *     0 <= uᵢ, vᵢ <= n - 1
 *     1 <= timeᵢ <= 10⁹
 *     uᵢ != vᵢ
 *     There is at most one road connecting any two intersections.
 *     You can reach any intersection from any other intersection.
"""
from collections import defaultdict
from functools import cache
from heapq import heappop, heappush
from math import inf
from typing import List

class Solution:
    def countPaths(self, n: int, roads: List[List[int]]) -> int:
        neighbours = defaultdict(set)
        roads_lengths = {}
        for i1, i2, time in roads:
            neighbours[i1].add(i2)
            neighbours[i2].add(i1)
            roads_lengths[frozenset((i1, i2))] = time

        shortest_paths = dijkstra(neighbours, roads_lengths, 0)

        @cache
        def count_shortest_paths(i):
            if i > 0:
                counter = 0
                for neib in neighbours[i]:
                    cur_dist = roads_lengths[frozenset((i, neib))]
                    if shortest_paths[neib] + cur_dist == shortest_paths[i]:
                        counter += count_shortest_paths(neib)
                return counter
            else:
                return 1

        return count_shortest_paths(n - 1) % (10**9 + 7)

def dijkstra(graph, distances, source):
    shortest_paths = dict.fromkeys(graph, inf)
    shortest_paths[source] = 0
    to_visit = [(0, source)]
    while to_visit:
        parent_dist, root = heappop(to_visit)
        for neib in graph[root]:
            cur_dist = distances[frozenset((root, neib))]
            if parent_dist + cur_dist < shortest_paths[neib]:
                shortest_paths[neib] = parent_dist + cur_dist
                heappush(to_visit, (parent_dist + cur_dist, neib))
    return shortest_paths

if __name__ == "__main__":
    tests = (
        (7, [[0,6,7],[0,1,2],[1,2,3],[1,3,3],[6,3,3],[3,5,1],[6,5,1],[2,5,1],[0,4,5],[4,6,2]], 4),
        (2, [[1,0,10]], 1),
        (6, [[3,0,4],[0,2,3],[1,2,2],[4,1,3],[2,5,5],[2,3,1],[0,4,1],[2,4,6],[4,3,1]], 2),
    )
    from time import time
    start = time()
    for n, roads, expected in tests:
        print(Solution().countPaths(n, roads) == expected, flush=True)
    print(f"Time elapsed: {time() - start} sec")
