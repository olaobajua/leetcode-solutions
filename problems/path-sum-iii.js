/**
 * 437. Path Sum III (Medium)
 * Given the root of a binary tree and an integer targetSum, return the number
 * of paths where the sum of the values along the path equals targetSum.
 *
 * The path does not need to start or end at the root or a leaf, but it must go
 * downwards (i.e., traveling only from parent nodes to child nodes).
 *
 * Example 1:
 * Input: root = [10,5,-3,3,2,null,11,3,-2,null,1], targetSum = 8
 * Output: 3
 * Explanation: The paths that sum to 8 are shown.
 *
 * Example 2:
 * Input: root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
 * Output: 3
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [0, 1000].
 *     -10⁹ <= Node.val <= 10⁹
 *     -1000 <= targetSum <= 1000
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @param {number} targetSum
 * @return {number}
 */
var pathSum = function(root, targetSum) {
    function countPaths(root, sums) {
        if (root === null) {
            return 0;
        }
        sums = sums.concat(0).map(s => s + root.val);
        return (sum(...sums.map(s => s == targetSum)) +
                countPaths(root.left, sums) +
                countPaths(root.right, sums))
    }
    return countPaths(root, [])
};

function sum(...iterable) {
    return iterable.reduce((acc, x) => acc + x);
}

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}
tests = [
    [[10,5,-3,3,2,null,11,3,-2,null,1], 8, 3],
    [[5,4,8,11,null,13,4,7,2,null,null,5,1], 22, 3],
];
for (let [nums, targetSum, expected] of tests) {
    console.log(pathSum(buildTree(nums), targetSum) == expected);
}
