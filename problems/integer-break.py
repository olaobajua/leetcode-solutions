"""
 * 343. Integer Break [Medium]
 * Given an integer n, break it into the sum of k positive integers, where
 * k >= 2, and maximize the product of those integers.
 *
 * Return the maximum product you can get.
 *
 * Example 1:
 * Input: n = 2
 * Output: 1
 * Explanation: 2 = 1 + 1, 1 × 1 = 1.
 *
 * Example 2:
 * Input: n = 10
 * Output: 36
 * Explanation: 10 = 3 + 3 + 4, 3 × 3 × 4 = 36.
 *
 * Constraints:
 *     ∙ 2 <= n <= 58
"""
from functools import cache

class Solution:
    def integerBreak(self, n: int) -> int:
        @cache
        def dp(n):
            ret = 1
            for x in range(1, n):
                ret = max(ret, x * (n - x), x * dp(n - x))
            return ret

        return dp(n)

if __name__ == "__main__":
    tests = (
        (2, 1),
        (10, 36),
    )
    for n, expected in tests:
        print(Solution().integerBreak(n) == expected)
