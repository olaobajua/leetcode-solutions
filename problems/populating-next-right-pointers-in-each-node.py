"""
 * 116. Populating Next Right Pointers in Each Node (Medium)
 * You are given a perfect binary tree where all leaves are on the same level,
 * and every parent has two children. The binary tree has the following
 * definition:
 *     struct Node {
 *       int val;
 *       Node *left;
 *       Node *right;
 *       Node *next;
 *     }
 *
 * Populate each next pointer to point to its next right node. If there is no
 * next right node, the next pointer should be set to NULL.
 *
 * Initially, all next pointers are set to NULL.
 *
 * Example 1:
 * Input: root = [1,2,3,4,5,6,7]
 * Output: [1,#,2,3,#,4,5,6,7,#]
 * Explanation: Given the above perfect binary tree (Figure A), your function
 * should populate each next pointer to point to its next right node, just like
 * in Figure B. The serialized output is in level order as connected by the
 * next pointers, with '#' signifying the end of each level.
 *
 * Example 2:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 2¹² - 1].
 *     ∙ -1000 <= Node.val <= 1000
 *
 * Follow-up:
 *     ∙ You may only use constant extra space.
 *     ∙ The recursive approach is fine. You may assume implicit stack space
 *       does not count as extra space for this problem.
"""
from typing import List

# Definition for a Node.
class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next

class Solution:
    def connect(self, root: 'Optional[Node]') -> 'Optional[Node]':
        cur = root
        while cur and cur.left:
            nxt = cur.left
            while cur:
                cur.left.next = cur.right
                cur.right.next = cur.next and cur.next.left
                cur = cur.next
            cur = nxt
        return root

def build_tree(vals):
    root = None
    try:
        root = Node(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = Node(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = Node(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    ret = []
    while root:
        nxt = root.left
        while root:
            ret.append(root.val)
            root = root.next
        ret.append('#')
        root = nxt
    return ret

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5,6,7], [1,'#',2,3,'#',4,5,6,7,'#']),
        ([], []),
        ([0], [0, '#']),
    )
    for nums, expected in tests:
        print(list(tree_to_list(Solution().connect(build_tree(nums)))) ==
              expected)
