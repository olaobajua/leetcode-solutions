"""
 * 155. Min Stack (Easy)
 * Design a stack that supports push, pop, top, and retrieving the minimum
 * element in constant time.
 *
 * Implement the MinStack class:
 *     MinStack() initializes the stack object.
 *     void push(int val) pushes the element val onto the stack.
 *     void pop() removes the element on the top of the stack.
 *     int top() gets the top element of the stack.
 *     int getMin() retrieves the minimum element in the stack.
 *
 * Example 1:
 * Input
 * ["MinStack","push","push","push","getMin","pop","top","getMin"]
 * [[],[-2],[0],[-3],[],[],[],[]]
 *
 * Output
 * [null,null,null,null,-3,null,0,-2]
 *
 * Explanation
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.getMin(); // return -3
 * minStack.pop();
 * minStack.top();    // return 0
 * minStack.getMin(); // return -2
 *
 * Constraints:
 *     ∙ -2³¹ <= val <= 2³¹ - 1
 *     ∙ Methods pop, top and getMin operations will always be called on
 *       non-empty stacks.
 *     ∙ At most 3 * 10⁴ calls will be made to push, pop, top, and getMin.
"""
from sortedcontainers import SortedList

class MinStack:
    def __init__(self):
        self.__list = []
        self.__sorted = SortedList()

    def push(self, val: int) -> None:
        self.__list.append(val)
        self.__sorted.add(val)

    def pop(self) -> None:
        self.__sorted.remove(self.__list.pop())

    def top(self) -> int:
        return self.__list[-1]

    def getMin(self) -> int:
        return self.__sorted[0]

if __name__ == "__main__":
    obj = MinStack()
    obj.push(-2)
    obj.push(0)
    obj.push(-3)
    print(obj.getMin() == -3)
    obj.pop()
    print(obj.top() == 0)
    print(obj.getMin() == -2)
