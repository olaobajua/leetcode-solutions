"""
 * 332. Reconstruct Itinerary [Hard]
 * You are given a list of airline tickets where tickets[i] = [fromᵢ, toᵢ]
 * represent the departure and the arrival airports of one flight. Reconstruct
 * the itinerary in order and return it.
 *
 * All of the tickets belong to a man who departs from "JFK", thus, the
 * itinerary must begin with "JFK". If there are multiple valid itineraries,
 * you should return the itinerary that has the smallest lexical order when
 * read as a single string.
 *
 *     ∙ For example, the itinerary ["JFK", "LGA"] has a smaller lexical order
 *       than ["JFK", "LGB"].
 *
 * You may assume all tickets form at least one valid itinerary. You must use
 * all the tickets once and only once.
 *
 * Example 1:
 * Input: tickets = [["MUC","LHR"],["JFK","MUC"],["SFO","SJC"],["LHR","SFO"]]
 * Output: ["JFK","MUC","LHR","SFO","SJC"]
 *
 * Example 2:
 * Input: tickets =
 * [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]
 * Output: ["JFK","ATL","JFK","SFO","ATL","SFO"]
 * Explanation: Another possible reconstruction is
 * ["JFK","SFO","ATL","JFK","ATL","SFO"] but it is larger in lexical order.
 *
 * Constraints:
 *     ∙ 1 <= tickets.length <= 300
 *     ∙ tickets[i].length == 2
 *     ∙ fromᵢ.length == 3
 *     ∙ toᵢ.length == 3
 *     ∙ fromᵢ and toᵢ consist of uppercase English letters.
 *     ∙ fromᵢ != toᵢ
"""
from collections import defaultdict
from typing import List

class Solution:
    def findItinerary(self, tickets: List[List[str]]) -> List[str]:
        def dfs(node):
            if node in graph:
                while graph[node]:
                    dfs(graph[node].pop())
            itinerary.append(node)

        graph = defaultdict(list)
        for src, dst in tickets:
            graph[src].append(dst)
        
        for src in graph:
            graph[src].sort(reverse=True)
        
        itinerary = []
        dfs("JFK")
        
        return reversed(itinerary)


if __name__ == "__main__":
    tests = (
        ([["MUC","LHR"],["JFK","MUC"],["SFO","SJC"],["LHR","SFO"]],
         ["JFK","MUC","LHR","SFO","SJC"]),
        ([["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]],
         ["JFK","ATL","JFK","SFO","ATL","SFO"]),
        ([["JFK","KUL"],["JFK","NRT"],["NRT","JFK"]],
         ["JFK","NRT","JFK","KUL"]),
        ([["AXA","EZE"],["EZE","AUA"],["ADL","JFK"],["ADL","TIA"],["AUA","AXA"],["EZE","TIA"],["EZE","TIA"],["AXA","EZE"],["EZE","ADL"],["ANU","EZE"],["TIA","EZE"],["JFK","ADL"],["AUA","JFK"],["JFK","EZE"],["EZE","ANU"],["ADL","AUA"],["ANU","AXA"],["AXA","ADL"],["AUA","JFK"],["EZE","ADL"],["ANU","TIA"],["AUA","JFK"],["TIA","JFK"],["EZE","AUA"],["AXA","EZE"],["AUA","ANU"],["ADL","AXA"],["EZE","ADL"],["AUA","ANU"],["AXA","EZE"],["TIA","AUA"],["AXA","EZE"],["AUA","SYD"],["ADL","JFK"],["EZE","AUA"],["ADL","ANU"],["AUA","TIA"],["ADL","EZE"],["TIA","JFK"],["AXA","ANU"],["JFK","AXA"],["JFK","ADL"],["ADL","EZE"],["AXA","TIA"],["JFK","AUA"],["ADL","EZE"],["JFK","ADL"],["ADL","AXA"],["TIA","AUA"],["AXA","JFK"],["ADL","AUA"],["TIA","JFK"],["JFK","ADL"],["JFK","ADL"],["ANU","AXA"],["TIA","AXA"],["EZE","JFK"],["EZE","AXA"],["ADL","TIA"],["JFK","AUA"],["TIA","EZE"],["EZE","ADL"],["JFK","ANU"],["TIA","AUA"],["EZE","ADL"],["ADL","JFK"],["ANU","AXA"],["AUA","AXA"],["ANU","EZE"],["ADL","AXA"],["ANU","AXA"],["TIA","ADL"],["JFK","ADL"],["JFK","TIA"],["AUA","ADL"],["AUA","TIA"],["TIA","JFK"],["EZE","JFK"],["AUA","ADL"],["ADL","AUA"],["EZE","ANU"],["ADL","ANU"],["AUA","AXA"],["AXA","TIA"],["AXA","TIA"],["ADL","AXA"],["EZE","AXA"],["AXA","JFK"],["JFK","AUA"],["ANU","ADL"],["AXA","TIA"],["ANU","AUA"],["JFK","EZE"],["AXA","ADL"],["TIA","EZE"],["JFK","AXA"],["AXA","ADL"],["EZE","AUA"],["AXA","ANU"],["ADL","EZE"],["AUA","EZE"]],
        ["JFK","ADL","ANU","ADL","ANU","AUA","ADL","AUA","ADL","AUA","ANU","AXA","ADL","AUA","ANU","AXA","ADL","AXA","ADL","AXA","ANU","AXA","ANU","AXA","EZE","ADL","AXA","EZE","ADL","AXA","EZE","ADL","EZE","ADL","EZE","ADL","EZE","ANU","EZE","ANU","EZE","AUA","AXA","EZE","AUA","AXA","EZE","AUA","AXA","JFK","ADL","EZE","AUA","EZE","AXA","JFK","ADL","JFK","ADL","JFK","ADL","JFK","ADL","TIA","ADL","TIA","AUA","JFK","ANU","TIA","AUA","JFK","AUA","JFK","AUA","TIA","AUA","TIA","AXA","TIA","EZE","AXA","TIA","EZE","JFK","AXA","TIA","EZE","JFK","AXA","TIA","JFK","EZE","TIA","JFK","EZE","TIA","JFK","TIA","JFK","AUA","SYD"]),
    )
    for tickets, expected in tests:
        print(list(Solution().findItinerary(tickets)) == expected)
