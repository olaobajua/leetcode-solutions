"""
 * 11. Container With Most Water (Medium)
 * You are given an integer array height of length n. There are n vertical
 * lines drawn such that the two endpoints of the iᵗʰ line are (i, 0) and
 * (i, height[i]).
 *
 * Find two lines that together with the x-axis form a container, such that the
 * container contains the most water.
 *
 * Return the maximum amount of water a container can store.
 *
 * Notice that you may not slant the container.
 *
 * Example 1:
 *
 *   |_________|___
 *   |         |   |
 *   | |       |   |
 *   | |   |   |   |
 *   | |   | | |   |
 *   | |   | | | | |
 *   | | | | | | | |
 * |_|_|_|_|_|_|_|_|
 * Input: height = [1,8,6,2,5,4,8,3,7]
 * Output: 49
 * Explanation: The above vertical lines are represented by array
 * [1,8,6,2,5,4,8,3,7]. In this case, the max area of water the container can
 * contain is 49.
 *
 * Example 2:
 * Input: height = [1,1]
 * Output: 1
 *
 * Constraints:
 *     ∙ n == height.length
 *     ∙ 2 <= n <= 10⁵
 *     ∙ 0 <= height[i] <= 10⁴
"""
from typing import List

class Solution:
    def maxArea(self, height: List[int]) -> int:
        left = 0
        right = len(height) - 1
        area = 0
        while left < right:
            area = max(area, min(height[left], height[right]) * (right - left))
            if height[right] > height[left]:
                left += 1
            else:
                right -= 1
        return area

if __name__ == "__main__":
    tests = (
        ([1,8,6,2,5,4,8,3,7], 49),
        ([1,1], 1),
    )
    for height, expected in tests:
        print(Solution().maxArea(height) == expected)
