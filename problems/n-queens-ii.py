"""
 * 52. N-Queens II (Hard)
 * The n-queens puzzle is the problem of placing n queens on an n x n
 * chessboard such that no two queens attack each other.
 *
 * Given an integer n, return the number of distinct solutions to the n-queens
 * puzzle.
 *
 * Example 1:
 * . Q . .  . . Q .
 * . . . Q  Q . . .
 * Q . . .  . . . Q
 * . . Q .  . Q . .
 *
 * Input: n = 4
 * Output: 2
 * Explanation: There are two distinct solutions to the 4-queens puzzle
 * as shown.
 *
 * Example 2:
 * Input: n = 1
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= n <= 9
"""
from typing import List

class Solution:
    def totalNQueens(self, n: int) -> int:
        def dfs(i, c1, c2, c3):
            nonlocal ret
            if i == n:
                ret += 1

            for j in range(n):
                if c1 & 1<<j or c2 & 1<<i-j+n or c3 & 1<<i+j:
                    continue
                dfs(i + 1, c1 ^ 1<<j, c2 ^ 1<<i-j+n, c3 ^ 1<<i+j)

        ret = 0
        dfs(0, 0, 0, 0)
        return ret

if __name__ == "__main__":
    tests = (
        (4, 2),
        (1, 1),
    )
    for n, expected in tests:
        print(Solution().totalNQueens(n) == expected)
