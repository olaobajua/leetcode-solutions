"""
 * 904. Fruit Into Baskets [Medium]
 * You are visiting a farm that has a single row of fruit trees arranged from
 * left to right. The trees are represented by an integer array fruits where
 * fruits[i] is the type of fruit the iᵗʰ tree produces.
 *
 * You want to collect as much fruit as possible. However, the owner has some
 * strict rules that you must follow:
 *     ∙ You only have two baskets, and each basket can only hold a single
 *       type of fruit. There is no limit on the amount of fruit each basket
 *       can hold.
 *     ∙ Starting from any tree of your choice, you must pick exactly one fruit
 *       from every tree (including the start tree) while moving to the right.
 *       The picked fruits must fit in one of your baskets.
 *     ∙ Once you reach a tree with fruit that cannot fit in your baskets, you
 *       must stop.
 *
 * Given the integer array fruits, return the maximum number of fruits you can
 * pick.
 *
 * Example 1:
 * Input: fruits = [1,2,1]
 * Output: 3
 * Explanation: We can pick from all 3 trees.
 *
 * Example 2:
 * Input: fruits = [0,1,2,2]
 * Output: 3
 * Explanation: We can pick from trees [1,2,2].
 * If we had started at the first tree, we would only pick from trees [0,1].
 *
 * Example 3:
 * Input: fruits = [1,2,3,2,2]
 * Output: 4
 * Explanation: We can pick from trees [2,3,2,2].
 * If we had started at the first tree, we would only pick from trees [1,2].
 *
 * Constraints:
 *     ∙ 1 <= fruits.length <= 10⁵
 *     ∙ 0 <= fruits[i] < fruits.length
"""
from typing import List

class Solution:
    def totalFruit(self, fruits: List[int]) -> int:
        basket1 = basket2 = None
        count1 = count2 = count_last = 0
        ret = 0
        for fruit in fruits:
            if basket1 is None:
                basket1 = fruit
                count1 = 1
            elif basket1 == fruit:
                count1 += 1
                if basket2 is not None:
                    basket1, basket2 = basket2, basket1
                    count1, count2 = count2, count1
                    count_last = 1
            elif basket2 is None:
                basket2 = fruit
                count2 = count_last = 1
            elif basket2 == fruit:
                count2 += 1
                count_last += 1
            else:
                basket1 = basket2
                count1 = count_last
                basket2 = fruit
                count2 = count_last = 1
            ret = max(ret, count1 + count2)
        return ret

if __name__ == "__main__":
    tests = (
        ([1,2,1], 3),
        ([0,1,2,2], 3),
        ([1,2,3,2,2], 4),
        ([1,0,1,4,1,4,1,2,3], 5),
    )
    for fruits, expected in tests:
        print(Solution().totalFruit(fruits) == expected)
