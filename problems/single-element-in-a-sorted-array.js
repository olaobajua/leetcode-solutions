/**
 * 540. Single Element in a Sorted Array (Medium)
 * You are given a sorted array consisting of only integers where every element
 * appears exactly twice, except for one element which appears exactly once.
 *
 * Return the single element that appears only once.
 *
 * Your solution must run in O(log n) time and O(1) space.
 *
 * Example 1:
 * Input: nums = [1,1,2,3,3,4,4,8,8]
 * Output: 2
 *
 * Example 2:
 * Input: nums = [3,3,7,7,10,11,11]
 * Output: 10
 *
 * Constraints:
 *     1 <= nums.length <= 10⁵
 *     0 <= nums[i] <= 10⁵
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var singleNonDuplicate = function(nums) {
    let lo = 0;
    let hi = nums.length - 1;
    while (lo < hi) {
        const mid = Math.floor((lo + hi) / 2);
        if ((mid % 2 === 1 && nums[mid] === nums[mid-1]) ||
            (mid % 2 === 0 && nums[mid] === nums[mid+1])) {
            lo = mid + 1;
        } else {
            hi = mid;
        }
    }
    return nums[lo];
};

const tests = [
    [[1], 1],
    [[1,1,2], 2],
    [[1,1,2,3,3], 2],
    [[1,1,2,3,3,4,4,8,8], 2],
    [[3,3,7,7,10,11,11], 10],
];

for (const [nums, expected] of tests) {
    console.log(singleNonDuplicate(nums) == expected);
}
