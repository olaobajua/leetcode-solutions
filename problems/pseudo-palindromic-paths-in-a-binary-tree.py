"""
 * 1457. Pseudo-Palindromic Paths in a Binary Tree [Medium]
 * Given a binary tree where node values are digits from 1 to 9. A path in the
 * binary tree is said to be pseudo-palindromic if at least one permutation of
 * the node values in the path is a palindrome.
 *
 * Return the number of pseudo-palindromic paths going from the root node to
 * leaf nodes.
 *
 * Example 1:
 *    2
 *  3   1
 * 3 1   1
 * Input: root = [2,3,1,3,1,null,1]
 * Output: 2
 * Explanation: The figure above represents the given binary tree. There are
 * three paths going from the root node to leaf nodes: the red path [2,3,3],
 * the green path [2,1,1], and the path [2,3,1]. Among these paths only red
 * path and green path are pseudo-palindromic paths since the red path [2,3,3]
 * can be rearranged in [3,2,3] (palindrome) and the green path [2,1,1] can be
 * rearranged in [1,2,1] (palindrome).
 *
 * Example 2:
 *    2
 *  1   1
 * 1 3
 *    1
 * Input: root = [2,1,1,1,3,null,null,null,null,null,1]
 * Output: 1
 * Explanation: The figure above represents the given binary tree. There are
 * three paths going from the root node to leaf nodes: the green path [2,1,1],
 * the path [2,1,3,1], and the path [2,1]. Among these paths only the green
 * path is pseudo-palindromic since [2,1,1] can be rearranged in [1,2,1]
 * (palindrome).
 *
 * Example 3:
 * Input: root = [9]
 * Output: 1
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁵].
 *     ∙ 1 <= Node.val <= 9
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def pseudoPalindromicPaths(self, root: Optional[TreeNode]) -> int:
        def dfs(node):
            nodes[node.val] += 1
            if not node.left and not node.right:
                if sum(x % 2 for x in nodes) <= 1:
                    nonlocal ret
                    ret += 1
            if node.left:
                dfs(node.left)
            if node.right:
                dfs(node.right)
            nodes[node.val] -= 1
        nodes = [0] * 10
        ret = 0
        dfs(root)
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([2,3,1,3,1,None,1], 2),
        ([2,1,1,1,3,None,None,None,None,None,1], 1),
        ([9], 1),
        ([1,9,1,None,1,None,1,None,None,7,None,None,4], 1),
    )
    for root, expected in tests:
        print(Solution().pseudoPalindromicPaths(build_tree(root)) == expected)
