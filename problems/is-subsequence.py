"""
 * 392. Is Subsequence (Easy)
 * Given two strings s and t, return true if s is a subsequence of t, or false
 * otherwise.
 *
 * A subsequence of a string is a new string that is formed from the original
 * string by deleting some (can be none) of the characters without disturbing
 * the relative positions of the remaining characters. (i.e., "ace" is a
 * subsequence of "abcde" while "aec" is not).
 *
 * Example 1:
 * Input: s = "abc", t = "ahbgdc"
 * Output: true
 *
 * Example 2:
 * Input: s = "axc", t = "ahbgdc"
 * Output: false
 *
 * Constraints:
 *     ∙ 0 <= s.length <= 100
 *     ∙ 0 <= t.length <= 10⁴
 *     ∙ s and t consist only of lowercase English letters.
 *
 * Follow up: Suppose there are lots of incoming s, say s₁, s₂, ..., sₖ where
 * k >= 10⁹, and you want to check one by one to see if t has its subsequence.
 * In this scenario, how would you change your code?
"""
class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        t = iter(t)
        return all(c in t for c in s)

if __name__ == "__main__":
    tests = (
        ("abc", "ahbgdc", True),
        ("axc", "ahbgdc", False),
        ("acb", "ahbgdc", False),
        ("", "ahbgdc", True),
    )
    for s, t, expected in tests:
        print(Solution().isSubsequence(s, t) == expected)
