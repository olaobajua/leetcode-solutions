"""
 * 589. N-ary Tree Preorder Traversal [Easy]
 * Given the root of an n-ary tree, return the preorder traversal of its
 * nodes' values.
 *
 * Nary-Tree input serialization is represented in their level order
 * traversal. Each group of children is separated by the null value (See
 * examples)
 *
 * Example 1:
 * Input: root = [1,null,3,2,4,null,5,6]
 * Output: [1,3,5,6,2,4]
 *
 * Example 2:
 * Input: root =
 * [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
 * Output: [1,2,3,6,7,11,14,4,8,12,5,9,13,10]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 10⁴].
 *     ∙ 0 <= Node.val <= 10⁴
 *     ∙ The height of the n-ary tree is less than or equal to 1000.
 *
 * Follow up: Recursive solution is trivial, could you do it iteratively?
"""
from typing import List

# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children

class Solution:
    def preorder(self, root: 'Node') -> List[int]:
        ret = []
        stack = [root]

        while stack:
            node = stack.pop()
            if node is not None:
                ret.append(node.val)
                stack.extend(reversed(node.children))

        return ret

if __name__ == "__main__":
    tests = (
        ([1,None,3,2,4,None,5,6], [1,3,5,6,2,4]),
        ([1,None,2,3,4,5,None,None,6,7,None,8,None,9,10,None,None,11,None,12,None,13,None,None,14],
         [1,2,3,6,7,11,14,4,8,12,5,9,13,10]),
    )
    for nums, expected in tests:
        root = Node(nums[0], [])
        to_visit = [root]
        i = 2
        for node in to_visit:
            while i < len(nums) and nums[i] is not None:
                child = Node(nums[i], [])
                node.children.append(child)
                to_visit.append(child)
                i += 1
            i += 1

        print(Solution().preorder(root) == expected)
