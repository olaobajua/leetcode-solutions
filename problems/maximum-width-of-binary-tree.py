"""
 * 662. Maximum Width of Binary Tree [Medium]
 * Given the root of a binary tree, return the maximum width of the given
 * tree.
 *
 * The maximum width of a tree is the maximum width among all levels.
 *
 * The width of one level is defined as the length between the end-nodes (the
 * leftmost and rightmost non-null nodes), where the null nodes between the
 * end-nodes that would be present in a complete binary tree extending down to
 * that level are also counted into the length calculation.
 *
 * It is guaranteed that the answer will in the range of a 32-bit signed
 * integer.
 *
 * Example 1:
 *    1
 *  3   2
 * 5 3   9
 * Input: root = [1,3,2,5,3,null,9]
 * Output: 4
 * Explanation: The maximum width exists in the third level with length 4
 * (5,3,null,9).
 *
 * Example 2:
 *    1
 *   3 2
 *  5   9
 * 6   7
 * Input: root = [1,3,2,5,null,null,9,6,null,7]
 * Output: 7
 * Explanation: The maximum width exists in the fourth level with length 7
 * (6,null,null,null,null,null,7).
 *
 * Example 3:
 *   1
 *  3 2
 * 5
 * Input: root = [1,3,2,5]
 * Output: 2
 * Explanation: The maximum width exists in the second level with length 2
 * (3,2).
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 3000].
 *     ∙ -100 <= Node.val <= 100
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def widthOfBinaryTree(self, root: Optional[TreeNode]) -> int:
        ret = 0
        level = [(root, 0)]
        while level:
            left = level[0][1]
            next_level = []
            for node, pos in level:
                if node:
                    next_level.append((node.left, 2*pos))
                    next_level.append((node.right, 2*pos + 1))
                    ret = max(ret, pos - left + 1)
            level = next_level
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,3,2,5], 2),
        ([1,3,None,5,3], 2),
        ([1,3,2,5,3,None,9], 4),
        ([1,3,2,5,None,None,9,6,None,7], 7),
        ([1,3,2,5,None,None,9,6,None,None,7], 8),
        ([1,1,1,1,1,1,1,None,None,None,1,None,None,None,None,2,2,2,2,2,2,2,None,2,None,None,2,None,2], 8),
        ([1,1,1,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,1,None,None,1,1,None,1,None,1,None,1,None,1,None], 2147483645),
    )
    for nums, expected in tests:
        print(Solution().widthOfBinaryTree(build_tree(nums)) == expected)
