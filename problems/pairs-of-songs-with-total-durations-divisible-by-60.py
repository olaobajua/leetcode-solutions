"""
 * 1010. Pairs of Songs With Total Durations Divisible by 60 (Medium)
 * You are given a list of songs where the iᵗʰ song has a duration of time[i]
 * seconds.
 *
 * Return the number of pairs of songs for which their total duration in
 * seconds is divisible by 60. Formally, we want the number of indices i, j
 * such that i < j with (time[i] + time[j]) % 60 == 0.
 *
 * Example 1:
 * Input: time = [30,20,150,100,40]
 * Output: 3
 * Explanation: Three pairs have a total duration divisible by 60:
 * (time[0] = 30, time[2] = 150): total duration 180
 * (time[1] = 20, time[3] = 100): total duration 120
 * (time[1] = 20, time[4] = 40): total duration 60
 *
 * Example 2:
 * Input: time = [60,60,60]
 * Output: 3
 * Explanation: All three pairs have a total duration of 120, which is
 * divisible by 60.
 *
 * Constraints:
 *     1 <= time.length <= 6 * 10⁴
 *     1 <= time[i] <= 500
"""
from typing import List

class Solution:
    def numPairsDivisibleBy60(self, time: List[int]) -> int:
        time_by_mod = [0]*60
        pairs = 0;
        for t in time:
            pairs += time_by_mod[-t % 60]
            time_by_mod[t % 60] += 1
        return pairs

if __name__ == "__main__":
    tests = (
        ([30,20,150,100,40], 3),
        ([60,60,60], 3),
    )
    for time, expected in tests:
        print(Solution().numPairsDivisibleBy60(time) == expected)
