/**
 * 315. Count of Smaller Numbers After Self [Hard]
 * You are given an integer array nums and you have to return a new counts
 * array. The counts array has the property where counts[i] is the number of
 * smaller elements to the right of nums[i].
 *
 * Example 1:
 * Input: nums = [5,2,6,1]
 * Output: [2,1,1,0]
 * Explanation:
 * To the right of 5 there are 2 smaller elements (2 and 1).
 * To the right of 2 there is only 1 smaller element (1).
 * To the right of 6 there is 1 smaller element (1).
 * To the right of 1 there is 0 smaller element.
 *
 * Example 2:
 * Input: nums = [-1]
 * Output: [0]
 *
 * Example 3:
 * Input: nums = [-1,-1]
 * Output: [0,0]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ -10⁴ <= nums[i] <= 10⁴
 */
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
template<class T> using ordered_set = tree<
    T,
    null_type,
    less_equal<T>,
    rb_tree_tag,
    tree_order_statistics_node_update
>;

class Solution {
public:
    vector<int> countSmaller(vector<int>& nums) {
        ordered_set<int> sorted_nums;
        vector<int> ret;
        for(int i = nums.size() - 1; i >= 0; --i) {
            ret.push_back(sorted_nums.order_of_key(nums[i]));
            sorted_nums.insert(nums[i]);
        }
        reverse(ret.begin(), ret.end());
        return ret;
    }
};
