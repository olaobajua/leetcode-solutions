"""
 * 485. Max Consecutive Ones (Easy)
 * Given a binary array nums, return the maximum number of consecutive 1's in
 * the array.
 *
 * Example 1:
 * Input: nums = [1,1,0,1,1,1]
 * Output: 3
 * Explanation: The first two digits or the last three digits are
 * consecutive 1s. The maximum number of consecutive 1s is 3.
 *
 * Example 2:
 * Input: nums = [1,0,1,1,0,1]
 * Output: 2
 *
 * Constraints:
 *     1 <= nums.length <= 10⁵
 *     nums[i] is either 0 or 1.
"""
from itertools import groupby
from typing import List

class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        return max(sum(n) for _, n in groupby(nums))

if __name__ == "__main__":
    tests = (
        ([1,1,0,1,1,1], 3),
        ([1,0,1,1,0,1], 2),
    )
    for nums, expected in tests:
        print(Solution().findMaxConsecutiveOnes(nums) == expected)
