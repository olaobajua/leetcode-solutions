"""
 * 32. Longest Valid Parentheses (Hard)
 * Given a string containing just the characters '(' and ')', find the length
 * of the longest valid (well-formed) parentheses substring.
 *
 * Example 1:
 * Input: s = "(()"
 * Output: 2
 * Explanation: The longest valid parentheses substring is "()".
 *
 * Example 2:
 * Input: s = ")()())"
 * Output: 4
 * Explanation: The longest valid parentheses substring is "()()".
 *
 * Example 3:
 * Input: s = ""
 * Output: 0
 *
 * Constraints:
 *     ∙ 0 <= s.length <= 3 * 10⁴
 *     ∙ s[i] is '(', or ')'.
"""
from functools import cache
from typing import List

class Solution:
    def longestValidParentheses(self, s: str) -> int:
        @cache
        def dp(i):
            if i == -1 or s[i] == "(":
                return 0
            if i >= 1 and s[i-1:i+1] == "()":
                return dp(i-2) + 2
            j = i - dp(i-1) - 1
            if j >= 0 and s[j] == "(":
                return dp(i - 1) + dp(j - 1) + 2
            return 0

        return max(dp(i) for i in range(len(s))) if s else 0

if __name__ == "__main__":
    tests = (
        ("(()", 2),
        (")()())", 4),
        ("", 0),
    )
    for s, expected in tests:
        print(Solution().longestValidParentheses(s) == expected)
