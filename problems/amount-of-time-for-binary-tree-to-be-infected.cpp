/**
 * 2385. Amount of Time for Binary Tree to Be Infected [Medium]
 * You are given the root of a binary tree with unique values, and an integer
 * start. At minute 0, an infection starts from the node with value start.
 *
 * Each minute, a node becomes infected if:
 *     ∙ The node is currently uninfected.
 *     ∙ The node is adjacent to an infected node.
 *
 * Return the number of minutes needed for the entire tree to be infected.
 *
 * Example 1:
 *      1
 *   5     3
 *    4  10 6
 *   9 2
 * Input: root = [1,5,3,null,4,10,6,9,2], start = 3
 * Output: 4
 * Explanation: The following nodes are infected during:
 * - Minute 0: Node 3
 * - Minute 1: Nodes 1, 10 and 6
 * - Minute 2: Node 5
 * - Minute 3: Node 4
 * - Minute 4: Nodes 9 and 2
 * It takes 4 minutes for the whole tree to be infected so we return 4.
 *
 * Example 2:
 * Input: root = [1], start = 1
 * Output: 0
 * Explanation: At minute 0, the only node in the tree is infected so we
 * return 0.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁵].
 *     ∙ 1 <= Node.val <= 10⁵
 *     ∙ Each node has a unique value.
 *     ∙ A node with a value of start exists in the tree.
 */
#include <iostream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <queue>

// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    int amountOfTime(TreeNode* root, int start) {
        std::unordered_map<int, std::vector<int>> graph;
        std::queue<TreeNode*> to_visit;
        to_visit.push(root);

        while (!to_visit.empty()) {
            TreeNode* node = to_visit.front();
            to_visit.pop();
            if (node->left != nullptr) {
                to_visit.push(node->left);
                graph[node->left->val].push_back(node->val);
                graph[node->val].push_back(node->left->val);
            }
            if (node->right != nullptr) {
                to_visit.push(node->right);
                graph[node->right->val].push_back(node->val);
                graph[node->val].push_back(node->right->val);
            }
        }

        std::unordered_set<int> seen = {start};
        std::queue<std::pair<int, int>> to_visit2;
        to_visit2.push({start, 0});
        int ret = 0;

        while (!to_visit2.empty()) {
            auto [node, steps] = to_visit2.front();
            to_visit2.pop();
            ret = std::max(ret, steps);

            for (int n : graph[node]) {
                if (seen.find(n) == seen.end()) {
                    to_visit2.push({n, steps + 1});
                    seen.insert(n);
                }
            }
        }

        return ret;
    }
};
