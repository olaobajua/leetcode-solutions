/**
 * 1015. Smallest Integer Divisible by K (Medium)
 * Given a positive integer k, you need to find the length of the smallest
 * positive integer n such that n is divisible by k, and n only contains the
 * digit 1.
 *
 * Return the length of n. If there is no such n, return -1.
 *
 * Note: n may not fit in a 64-bit signed integer.
 *
 * Example 1:
 * Input: k = 1
 * Output: 1
 * Explanation: The smallest answer is n = 1, which has length 1.
 *
 * Example 2:
 * Input: k = 2
 * Output: -1
 * Explanation: There is no such positive integer n divisible by 2.
 *
 * Example 3:
 * Input: k = 3
 * Output: 3
 * Explanation: The smallest answer is n = 111, which has length 3.
 *
 * Constraints:
 *     1 <= k <= 10⁵
 */
#include <stdio.h>
#include <stdlib.h>

int smallestRepunitDivByK(int k) {
    int rem = 0;
    for (int i = 1; i <= k; ++i) {
        // if x % k = y % k, then (x * a + b) % k = (y * a + b) % k
        // so (10 * i + 1) % k == (10 * (i % k) + 1) % k
        // https://en.wikipedia.org/wiki/Modular_arithmetic#Properties
        if (!(rem = (rem * 10 + 1) % k)) {
            return i;
        }
    }
    return -1;
}

int main() {
    const int tests[] = {1, 2, 3, 19};
    const int expected[] = {1, -1, 3, 18};
    const size_t n = sizeof(tests) / sizeof(tests[0]);
    for (int i = 0; i < n; ++i) {
        printf(smallestRepunitDivByK(tests[i]) == expected[i] ? "true\n"
                                                              : "false\n");
    }
    return EXIT_SUCCESS;
}
