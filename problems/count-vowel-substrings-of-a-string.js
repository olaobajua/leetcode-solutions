/**
 * 2062. Count Vowel Substrings of a String (Easy)
 * A substring is a contiguous (non-empty) sequence of characters within a
 * string.
 *
 * A vowel substring is a substring that only consists of vowels
 * ('a', 'e', 'i', 'o', and 'u') and has all five vowels present in it.
 *
 * Given a string word, return the number of vowel substrings in word.
 *
 * Example 1:
 * Input: word = "aeiouu"
 * Output: 2
 * Explanation: The vowel substrings of word are as follows (underlined):
 * - "aeiouu"
 * - "aeiouu"
 *
 * Example 2:
 * Input: word = "unicornarihan"
 * Output: 0
 * Explanation: Not all 5 vowels are present, so there are no vowel substrings.
 *
 * Example 3:
 * Input: word = "cuaieuouac"
 * Output: 7
 * Explanation: The vowel substrings of word are as follows (underlined):
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 *
 * Example 4:
 * Input: word = "bbaeixoubb"
 * Output: 0
 * Explanation: The only substrings that contain all five vowels also contain
 * consonants, so there are no vowel substrings.
 *
 * Constraints:
 *     1 <= word.length <= 100
 *     word consists of lowercase English letters only.
 */

/**
 * @param {string} word
 * @return {number}
 */
var countVowelSubstrings = function(word) {
    const n = word.length;
    const VOWELS = new Set("aeiou");
    let counter = 0;
    for (let i = 0; i < n - 1; ++i) {
        let curVowels = new Set();
        for (let j = i; j < n; ++j) {
            if (!VOWELS.has(word[j])) {
                break;
            }
            curVowels.add(word[j]);
            counter += curVowels.size == 5;
        }
    }
    return counter;
};

const tests = [
    ["aeiouu", 2],
    ["unicornarihan", 0],
    ["cuaieuouac", 7],
    ["bbaeixoubb", 0],
];

for (let [word, expected] of tests) {
    console.log(countVowelSubstrings(word) == expected);
}
