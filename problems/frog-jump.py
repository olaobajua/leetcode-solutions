"""
 * 403. Frog Jump [Hard]
 * A frog is crossing a river. The river is divided into some number of units,
 * and at each unit, there may or may not exist a stone. The frog can jump on a
 * stone, but it must not jump into the water.
 *
 * Given a list of stones' positions (in units) in sorted ascending order,
 * determine if the frog can cross the river by landing on the last stone.
 * Initially, the frog is on the first stone and assumes the first jump must be
 * 1 unit.
 *
 * If the frog's last jump was k units, its next jump must be either k - 1, k,
 * or k + 1 units. The frog can only jump in the forward direction.
 *
 * Example 1:
 * Input: stones = [0,1,3,5,6,8,12,17]
 * Output: true
 * Explanation: The frog can jump to the last stone by jumping 1 unit to the
 * 2nd stone, then 2 units to the 3rd stone, then 2 units to the 4th stone,
 * then 3 units to the 6th stone, 4 units to the 7th stone, and 5 units to the
 * 8th stone.
 *
 * Example 2:
 * Input: stones = [0,1,2,3,4,8,9,11]
 * Output: false
 * Explanation: There is no way to jump to the last stone as the gap between
 * the 5th and 6th stone is too large.
 *
 * Constraints:
 *     ∙ 2 <= stones.length <= 2000
 *     ∙ 0 <= stones[i] <= 2³¹ - 1
 *     ∙ stones[0] == 0
 *     ∙ stones is sorted in a strictly increasing order.
"""
from bisect import bisect_left
from functools import cache
from typing import List

class Solution:
    def canCross(self, stones: List[int]) -> bool:
        @cache
        def dp(i, d):
            if i == n - 1:
                return True

            cur = stones[i]
            if d > 1:
                nxt = bisect_left(stones, cur + d - 1)
                if nxt < n and stones[nxt] == cur + d - 1 and dp(nxt, d - 1):
                    return True

            nxt = bisect_left(stones, cur + d)
            if nxt < n and stones[nxt] == cur + d and dp(nxt, d):
                return True

            nxt = bisect_left(stones, cur + d + 1)
            if nxt < n and stones[nxt] == cur + d + 1 and dp(nxt, d + 1):
                return True

            return False

        if stones[1] != 1:
            return False

        n = len(stones)
        return dp(1, 1)

if __name__ == "__main__":
    tests = (
        ([0,1,3,5,6,8,12,17], True),
        ([0,1,2,3,4,8,9,11], False),
        ([0,1,3,6,7], False),
    )
    for stones, expected in tests:
        print(Solution().canCross(stones) == expected)
