/**
 * 383. Ransom Note [Easy]
 * Given two strings ransomNote and magazine, return true if ransomNote can be
 * constructed by using the letters from magazine and false otherwise.
 *
 * Each letter in magazine can only be used once in ransomNote.
 *
 * Example 1:
 * Input: ransomNote = "a", magazine = "b"
 * Output: false
 * Example 2:
 * Input: ransomNote = "aa", magazine = "ab"
 * Output: false
 * Example 3:
 * Input: ransomNote = "aa", magazine = "aab"
 * Output: true
 *
 * Constraints:
 *     ∙ 1 <= ransomNote.length, magazine.length <= 10⁵
 *     ∙ ransomNote and magazine consist of lowercase English letters.
 */
class Solution {
    public boolean canConstruct(String ransomNote, String magazine) {
        int[] countRN = new int[123];
        int[] countMagazine = new int[123];
        for (char c : ransomNote.toCharArray())
            ++countRN[c];
        for (char c : magazine.toCharArray())
            ++countMagazine[c];
        for (int i = (int)'a'; i <= (int)'z'; ++i)
            if (countMagazine[i] < countRN[i])
                return false;
        return true;
    }
}
