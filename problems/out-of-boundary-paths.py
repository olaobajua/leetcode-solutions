"""
 * 576. Out of Boundary Paths [Medium]
 * There is an m x n grid with a ball. The ball is initially at the position
 * [startRow, startColumn]. You are allowed to move the ball to one of the four
 * adjacent cells in the grid (possibly out of the grid crossing the grid
 * boundary). You can apply at most maxMove moves to the ball.
 *
 * Given the five integers m, n, maxMove, startRow, startColumn, return the
 * number of paths to move the ball out of the grid boundary. Since the answer
 * can be very large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: m = 2, n = 2, maxMove = 2, startRow = 0, startColumn = 0
 * Output: 6
 *
 * Example 2:
 * Input: m = 1, n = 3, maxMove = 3, startRow = 0, startColumn = 1
 * Output: 12
 *
 * Constraints:
 *     ∙ 1 <= m, n <= 50
 *     ∙ 0 <= maxMove <= 50
 *     ∙ 0 <= startRow < m
 *     ∙ 0 <= startColumn < n
"""
from functools import cache

class Solution:
    def findPaths(self, m: int, n: int, maxMove: int, startRow: int, startColumn: int) -> int:
        @cache
        def dfs(r, c, moves_left):
            if r == m or r < 0 or c == n or c < 0:
                return 1
            if moves_left == 0:
                return 0
            return sum(dfs(nr, nc, moves_left - 1) % MOD
                       for nr, nc in ((r, c+1), (r+1, c), (r, c-1), (r-1, c)))

        MOD = 10**9 + 7
        return dfs(startRow, startColumn, maxMove) % MOD

if __name__ == "__main__":
    tests = (
        (2, 2, 2, 0, 0, 6),
        (1, 3, 3, 0, 1, 12),
        (8, 50, 23, 5, 26, 914783380),
        (36, 5, 50, 15, 3, 390153306),
    )
    for m, n, maxMove, startRow, startColumn, expected in tests:
        print(Solution().findPaths(m, n, maxMove, startRow, startColumn) ==
              expected)
