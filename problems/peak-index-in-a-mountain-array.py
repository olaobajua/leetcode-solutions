"""
 * 852. Peak Index in a Mountain Array [Medium]
 * An array arr a mountain if the following properties hold:
 *     ∙ arr.length >= 3
 *     ∙ There exists some i with 0 < i < arr.length - 1 such that:
 * 	    ∙ arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
 * 	    ∙ arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
 *
 * Given a mountain array arr, return the index i such that arr[0] < arr[1] <
 * ... < arr[i - 1] < arr[i] > arr[i + 1] > ... > arr[arr.length - 1].
 *
 * You must solve it in O(log(arr.length)) time complexity.
 *
 * Example 1:
 * Input: arr = [0,1,0]
 * Output: 1
 *
 * Example 2:
 * Input: arr = [0,2,1,0]
 * Output: 1
 *
 * Example 3:
 * Input: arr = [0,10,5,2]
 * Output: 1
 *
 * Constraints:
 *     ∙ 3 <= arr.length <= 10⁵
 *     ∙ 0 <= arr[i] <= 10⁶
 *     ∙ arr is guaranteed to be a mountain array.
"""
from typing import List

class Solution:
    def peakIndexInMountainArray(self, arr: List[int]) -> int:
        lo = 0
        hi = len(arr) - 1
        while lo < hi:
            mid = (lo + hi) // 2
            if arr[mid-1] < arr[mid] < arr[mid+1]:
                lo = mid
            elif arr[mid-1] > arr[mid] > arr[mid+1]:
                hi = mid
            elif arr[mid-1] < arr[mid] > arr[mid+1]:
                return mid

        return lo

if __name__ == "__main__":
    tests = (
        ([0,1,0], 1),
        ([0,2,1,0], 1),
        ([0,10,5,2], 1),
        ([3,4,5,1], 2),
    )
    for arr, expected in tests:
        print(Solution().peakIndexInMountainArray(arr) == expected)
