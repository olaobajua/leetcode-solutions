/**
 * 1239. Maximum Length of a Concatenated String with Unique Characters (Medium)
 * Given an array of strings arr. String s is a concatenation of a sub-sequence
 * of arr which have unique characters.
 *
 * Return the maximum possible length of s.
 *
 * Example 1:
 * Input: arr = ["un","iq","ue"]
 * Output: 4
 * Explanation: All possible concatenations are "","un","iq","ue","uniq"
 * and "ique".
 * Maximum length is 4.
 *
 * Example 2:
 * Input: arr = ["cha","r","act","ers"]
 * Output: 6
 * Explanation: Possible solutions are "chaers" and "acters".
 *
 * Example 3:
 * Input: arr = ["abcdefghijklmnopqrstuvwxyz"]
 * Output: 26
 *
 * Constraints:
 *     1 <= arr.length <= 16
 *     1 <= arr[i].length <= 26
 *     arr[i] contains only lower case English letters.
 */

/**
 * @param {string[]} arr
 * @return {number}
 */
var maxLength = function(arr) {
    function* concat(s, i) {
        for (let j = i; j < arr.length; ++j) {
            if (Array.from(s).every(l => arr[j].indexOf(l) == -1)) {
                yield* concat(s + arr[j], j + 1);
            }
        }
        yield (s.length == new Set(s).size) ? s : ""
    }
    return Math.max(...[...concat("", 0)].map(s => s.length));
};

tests = [
    [["un","iq","ue"], 4],
    [["cha","r","act","ers"], 6],
    [["abcdefghijklmnopqrstuvwxyz"], 26],
    [["yy","bkhwmpbiisbldzknpm"], 0],
    [["cha","r","act","ers"], 6],
]
for (let [arr, expected] of tests) {
    console.log(maxLength(arr) == expected);
}
