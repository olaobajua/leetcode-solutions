"""
 * 1032. Stream of Characters (Hard)
 * Design an algorithm that accepts a stream of characters and checks if a
 * suffix of these characters is a string of a given array of strings words.
 *
 * For example, if words = ["abc", "xyz"] and the stream added the four
 * characters (one by one) 'a', 'x', 'y', and 'z', your algorithm should detect
 * that the suffix "xyz" of the characters "axyz" matches "xyz" from words.
 *
 * Implement the StreamChecker class:
 *     ∙ StreamChecker(String[] words) Initializes the object with the strings
 *       array words.
 *     ∙ boolean query(char letter) Accepts a new character from the stream and
 *       returns true if any non-empty suffix from the stream forms a word that
 *       is in words.
 *
 * Example 1:
 * Input
 * ["StreamChecker", "query", "query", "query", "query", "query", "query",
 *  "query", "query", "query", "query", "query", "query"]
 * [[["cd", "f", "kl"]], ["a"], ["b"], ["c"], ["d"], ["e"], ["f"], ["g"],
 *  ["h"], ["i"], ["j"], ["k"], ["l"]]
 * Output
 * [null, false, false, false, true, false, true, false, false, false, false,
 *  false, true]
 *
 * Explanation
 * StreamChecker streamChecker = new StreamChecker(["cd", "f", "kl"]);
 * streamChecker.query("a"); // return False
 * streamChecker.query("b"); // return False
 * streamChecker.query("c"); // return False
 * streamChecker.query("d"); // return True, because 'cd' is in the wordlist
 * streamChecker.query("e"); // return False
 * streamChecker.query("f"); // return True, because 'f' is in the wordlist
 * streamChecker.query("g"); // return False
 * streamChecker.query("h"); // return False
 * streamChecker.query("i"); // return False
 * streamChecker.query("j"); // return False
 * streamChecker.query("k"); // return False
 * streamChecker.query("l"); // return True, because 'kl' is in the wordlist
 *
 * Constraints:
 *     1 <= words.length <= 2000
 *     1 <= words[i].length <= 2000
 *     words[i] consists of lowercase English letters.
 *     letter is a lowercase English letter.
 *     At most 4 * 10⁴ calls will be made to query.
"""
from collections import deque
from typing import List

class StreamChecker:
    def __init__(self, words: List[str]):
        self.chars = deque([])
        self.trie = Trie()
        for word in words:
            self.trie.insert(word[::-1])

    def query(self, letter: str) -> bool:
        self.chars.appendleft(letter)
        return self.trie.search(self.chars)

class Trie:
    """Prefix Tree"""
    def __init__(self):
        self.prefixes = {}

    def insert(self, word: str) -> None:
        p = self.prefixes
        for l in word:
            p = p.setdefault(l, {})
        p["\0"] = word

    def search(self, word: str) -> bool:
        p = self.prefixes
        for l in word:
            if l not in p:
                break
            p = p[l]
            if "\0" in p:
                return True
        return False

obj = StreamChecker(["cd","f","kl"])
print(obj.query('a') == False)
print(obj.query('b') == False)
print(obj.query('c') == False)
print(obj.query('d') == True)
print(obj.query('e') == False)
print(obj.query('f') == True)
print(obj.query('g') == False)
print(obj.query('h') == False)
print(obj.query('i') == False)
print(obj.query('j') == False)
print(obj.query('k') == False)
print(obj.query('l') == True)
