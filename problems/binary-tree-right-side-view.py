"""
 * 199. Binary Tree Right Side View (Medium)
 * Given the root of a binary tree, imagine yourself standing on the right side
 * of it, return the values of the nodes you can see ordered from top to
 * bottom.
 *
 * Example 1:
 * Input: root = [1,2,3,null,5,null,4]
 * Output: [1,3,4]
 *
 * Example 2:
 * Input: root = [1,null,3]
 * Output: [1,3]
 *
 * Example 3:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 100].
 *     ∙ -100 <= Node.val <= 100
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def rightSideView(self, root: Optional[TreeNode]) -> List[int]:
        ret = []
        level = [root] if root else []
        while level:
            ret.append(level[0].val)
            next_level = []
            for node in level:
                if node.right:
                    next_level.append(node.right)
                if node.left:
                    next_level.append(node.left)
            level = next_level
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3,None,5,None,4], [1,3,4]),
        ([1,None,3], [1,3]),
        ([], []),
        ([1,2], [1,2]),
    )
    for nums, expected in tests:
        root = build_tree(nums)
        print(Solution().rightSideView(root) == expected)
