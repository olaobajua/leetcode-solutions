"""
 * 2605. Form Smallest Number From Two Digit Arrays [Easy]
 * Given two arrays of unique digits nums1 and nums2, return the smallest
 * number that contains at least one digit from each array.
 * Example 1:
 * Input: nums1 = [4,1,3], nums2 = [5,7]
 * Output: 15
 * Explanation: The number 15 contains the digit 1 from nums1 and the digit 5
 * from nums2. It can be proven that 15 is the smallest number we can have.
 *
 * Example 2:
 * Input: nums1 = [3,5,2,6], nums2 = [3,1,7]
 * Output: 3
 * Explanation: The number 3 contains the digit 3 which exists in both arrays.
 *
 * Constraints:
 *     ∙ 1 <= nums1.length, nums2.length <= 9
 *     ∙ 1 <= nums1[i], nums2[i] <= 9
 *     ∙ All digits in each array are unique.
"""
from typing import List

class Solution:
    def minNumber(self, nums1: List[int], nums2: List[int]) -> int:
        ret = []
        for x in range(1, 10):
            if x in nums1 and x in nums2:
                ret.append(x)
        ret.append(10*min(nums1) + min(nums2))
        ret.append(10*min(nums2) + min(nums1))
        return min(ret)

if __name__ == "__main__":
    tests = (
        ([4,1,3], [5,7], 15),
        ([3,5,2,6], [3,1,7], 3),
    )
    for nums1, nums2, expected in tests:
        print(Solution().minNumber(nums1, nums2) == expected)
