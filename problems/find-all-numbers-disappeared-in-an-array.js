/**
 * 448. Find All Numbers Disappeared in an Array (Easy)
 * Given an array nums of n integers where nums[i] is in the range [1, n],
 * return an array of all the integers in the range [1, n] that do not appear
 * in nums.
 *
 * Example 1:
 * Input: nums = [4,3,2,7,8,2,3,1]
 * Output: [5,6]
 *
 * Example 2:
 * Input: nums = [1,1]
 * Output: [2]
 *
 * Constraints:
 *     n == nums.length
 *     1 <= n <= 10⁵
 *     1 <= nums[i] <= n
 *
 * Follow up: Could you do it without extra space and in O(n) runtime?
 * You may assume the returned list does not count as extra space.
 */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var findDisappearedNumbers = function(nums) {
    for (let i = 0; i < nums.length; ++i) {
        while (i != nums[i] - 1 && nums[i] != nums[nums[i]-1]) {
            [nums[nums[i]-1], nums[i]] = [nums[i], nums[nums[i]-1]];
        }
    }
    return nums.map((n, i) => (i + 1 == n) ? 0 : i + 1).filter(n => n != 0);
};

const tests = [
    [[4,3,2,7,8,2,3,1], [5,6]],
    [[1,1], [2]],
];

for (const [nums, expected] of tests) {
    test: {
        for (const n of findDisappearedNumbers(nums)) {
            let i = expected.indexOf(n);
            if (i > -1) {
                expected.splice(i, 1);
            } else {
                console.log(false);
                break test;
            }
        }
        console.log(expected.length == 0);
    }
}
