/**
 * 138. Copy List with Random Pointer (Medium)
 * A linked list of length n is given such that each node contains an
 * additional random pointer, which could point to any node in the list, or
 * null.
 *
 * Construct a deep copy of the list. The deep copy should consist of exactly n
 * brand new nodes, where each new node has its value set to the value of its
 * corresponding original node. Both the next and random pointer of the new
 * nodes should point to new nodes in the copied list such that the pointers in
 * the original list and copied list represent the same list state. None of the
 * pointers in the new list should point to nodes in the original list.
 *
 * For example, if there are two nodes X and Y in the original list, where
 * X.random --> Y, then for the corresponding two nodes x and y in the copied
 * list, x.random --> y.
 *
 * Return the head of the copied linked list.
 *
 * The linked list is represented in the input/output as a list of n nodes.
 * Each node is represented as a pair of [val, random_index] where:
 *     ∙ val: an integer representing Node.val
 *     ∙ random_index: the index of the node (range from 0 to n-1) that the
 *       random pointer points to, or null if it does not point to any node.
 *
 * Your code will only be given the head of the original linked list.
 *
 * Example 1:
 * Input: head = [[7,null],[13,0],[11,4],[10,2],[1,0]]
 * Output: [[7,null],[13,0],[11,4],[10,2],[1,0]]
 *
 * Example 2:
 * Input: head = [[1,1],[2,1]]
 * Output: [[1,1],[2,1]]
 *
 * Example 3:
 * Input: head = [[3,null],[3,0],[3,null]]
 * Output: [[3,null],[3,0],[3,null]]
 *
 * Constraints:
 *     ∙ 0 <= n <= 1000
 *     ∙ -10⁴ <= Node.val <= 10⁴
 *     ∙ Node.random is null or is pointing to some node in the linked list.
 */

// Definition for a Node.
function Node(val, next, random) {
   this.val = val;
   this.next = next;
   this.random = random;
};

/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function(head) {
    return arrayToLinkedList(linkedListToArray(head));
};

function arrayToLinkedList(nums) {
    if (nums.length == 0) {
        return null;
    }
    const l = nums.map(([val, _]) => new Node(val, null, null));
    nums.forEach(([_, random], i) => {
        l[i].next = l[i+1];
        if (random !== null) {
            l[i].random = l[random];
        }
    });
    l[l.length-1].next = null;
    return l[0];
}

function linkedListToArray(head) {
    const l = [];
    while (head) {
        l.push(head);
        head = head.next;
    }
    return l.map(n => [n.val, l.includes(n.random) ? l.indexOf(n.random) : null]);
}

const tests = [
    [[[7,null],[13,0],[11,4],[10,2],[1,0]], [[7,null],[13,0],[11,4],[10,2],[1,0]]],
    [[[1,1],[2,1]], [[1,1],[2,1]]],
    [[[3,null],[3,0],[3,null]], [[3,null],[3,0],[3,null]]],
    [[], []],
];

for (const [nums, expected] of tests) {
    const root = arrayToLinkedList(nums)
    const ret = copyRandomList(root)
    console.log(JSON.stringify(linkedListToArray(ret)) ==
                JSON.stringify(expected) &&
                root ? ret !== root : true);
}
