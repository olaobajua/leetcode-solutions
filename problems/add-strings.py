"""
 * 415. Add Strings (Easy)
 * Given two non-negative integers, num1 and num2 represented as string, return
 * the sum of num1 and num2 as a string.
 *
 * You must solve the problem without using any built-in library for handling
 * large integers (such as BigInteger). You must also not convert the inputs to
 * integers directly.
 *
 * Example 1:
 * Input: num1 = "11", num2 = "123"
 * Output: "134"
 *
 * Example 2:
 * Input: num1 = "456", num2 = "77"
 * Output: "533"
 *
 * Example 3:
 * Input: num1 = "0", num2 = "0"
 * Output: "0"
 *
 * Constraints:
 *     1 <= num1.length, num2.length <= 10⁴
 *     num1 and num2 consist of only digits.
 *     num1 and num2 don't have any leading zeros except for the zero itself.
"""
from itertools import zip_longest

class Solution:
    def addStrings(self, num1: str, num2: str) -> str:
        reversed_num1 = [*map(int, reversed(num1))] + [0]
        reversed_num2 = [*map(int, reversed(num2))] + [0]
        reg = 0
        ret = []
        for n1, n2 in zip_longest(reversed_num1, reversed_num2, fillvalue=0):
            add = n1 + n2 + reg
            reg = add // 10
            ret.append(add % 10)
        if ret[-1] == 0:
            ret.pop()
        return ''.join(map(str, reversed(ret)))

if __name__ == "__main__":
    tests = (
        ("11", "123", "134"),
        ("456", "77", "533"),
        ("0", "0", "0"),
        ("983", "49", "1032"),
        ("99", "99", "198"),
    )
    for num1, num2, expected in tests:
        print(Solution().addStrings(num1, num2) == expected)
