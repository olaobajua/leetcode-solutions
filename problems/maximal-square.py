"""
 * 221. Maximal Square (Medium)
 * Given an m x n binary matrix filled with 0's and 1's, find the largest
 * square containing only 1's and return its area.
 *
 * Example 1:
 * Input: matrix = [["1","0","1","0","0"],
 *                  ["1","0","1","1","1"],
 *                  ["1","1","1","1","1"],
 *                  ["1","0","0","1","0"]]
 * Output: 4
 *
 * Example 2:
 * Input: matrix = [["0","1"],
 *                  ["1","0"]]
 * Output: 1
 *
 * Example 3:
 * Input: matrix = [["0"]]
 * Output: 0
 *
 * Constraints:
 *     m == matrix.length
 *     n == matrix[i].length
 *     1 <= m, n <= 300
 *     matrix[i][j] is '0' or '1'.
"""
from itertools import chain
from typing import List

class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        M = [[int(cell) for cell in row] for row in matrix]
        R = len(M)
        C = len(M[0])
        for r in range(1, R):
            for c in range(1, C):
                M[r][c] += min(M[r-1][c], M[r][c-1], M[r-1][c-1]) * M[r][c]
        return max(chain(*M))**2

if __name__ == "__main__":
    tests = (
        ([["1","0","1","0","0"],
          ["1","0","1","1","1"],
          ["1","1","1","1","1"],
          ["1","0","0","1","0"]], 4),
        ([["1","1","1","0","0"],
          ["1","0","1","1","1"],
          ["1","1","1","0","1"],
          ["1","0","0","1","0"]], 1),
        ([["0","1"],
          ["1","0"]], 1),
        ([["0"]], 0),
    )
    for matrix, expected in tests:
        print(Solution().maximalSquare(matrix) == expected)
