"""
 * 1958. Check if Move is Legal (Medium)
 * You are given a 0-indexed 8 x 8 grid board, where board[r][c] represents the
 * cell (r, c) on a game board. On the board, free cells are represented by '.',
 * white cells are represented by 'W', and black cells are represented by 'B'.
 *
 * Each move in this game consists of choosing a free cell and changing it to
 * the color you are playing as (either white or black). However, a move is
 * only legal if, after changing it, the cell becomes the endpoint of a good
 * line (horizontal, vertical, or diagonal).
 *
 * A good line is a line of three or more cells (including the endpoints) where
 * the endpoints of the line are one color, and the remaining cells in the
 * middle are the opposite color (no cells in the line are free).
 *
 * Given two integers rMove and cMove and a character color representing the
 * color you are playing as (white or black), return true if changing cell
 * (rMove, cMove) to color color is a legal move, or false if it is not legal.
 *
 * Example 1:
 * Input: board = [[".",".",".","B",".",".",".","."],
 *                 [".",".",".","W",".",".",".","."],
 *                 [".",".",".","W",".",".",".","."],
 *                 [".",".",".","W",".",".",".","."],
 *                 ["W","B","B",".","W","W","W","B"],
 *                 [".",".",".","B",".",".",".","."],
 *                 [".",".",".","B",".",".",".","."],
 *                 [".",".",".","W",".",".",".","."]], rMove = 4, cMove = 3, color = "B"
 * Output: true
 * Explanation: '.', 'W', and 'B' are represented by the colors blue, white,
 * and black respectively, and cell (rMove, cMove) is marked with an 'X'.
 *
 * Example 2:
 * Input: board = [[".",".",".",".",".",".",".","."],
 *                 [".","B",".",".","W",".",".","."],
 *                 [".",".","W",".",".",".",".","."],
 *                 [".",".",".","W","B",".",".","."],
 *                 [".",".",".",".",".",".",".","."],
 *                 [".",".",".",".","B","W",".","."],
 *                 [".",".",".",".",".",".","W","."],
 *                 [".",".",".",".",".",".",".","B"]], rMove = 4, cMove = 4, color = "W"
 * Output: false
 * Explanation: While there are good lines with the chosen cell as a middle
 * cell, there are no good lines with the chosen cell as an endpoint.
 *
 * Constraints:
 *
 *     board.length == board[r].length == 8
 *     0 <= rMove, cMove < 8
 *     board[rMove][cMove] == '.'
 *     color is either 'B' or 'W'.
"""
from typing import List

class Solution:
    def checkMove(self, board: List[List[str]], rMove: int, cMove: int, color: str) -> bool:
        if board[rMove][cMove] != '.':
            return False
        width = len(board)
        height = len(board[0])
        for dx, dy in ((0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (1, -1), (-1, 1), (-1, -1)):
            counter = 0
            x, y = rMove + dx, cMove + dy
            opposite = 'W' if color == 'B' else 'B'
            while 0 <= x < width and 0 <= y < height and board[x][y] == opposite:
                counter += 1
                x += dx
                y += dy
            if counter > 0 and 0 <= x < width and 0 <= y < height and board[x][y] == color:
                return True
        return False

if __name__ == "__main__":
    tests = (
        ([[".",".",".","B",".",".",".","."],
          [".",".",".","W",".",".",".","."],
          [".",".",".","W",".",".",".","."],
          [".",".",".","W",".",".",".","."],
          ["W","B","B",".","W","W","W","B"],
          [".",".",".","B",".",".",".","."],
          [".",".",".","B",".",".",".","."],
          [".",".",".","W",".",".",".","."]], 4, 3, "B", True),
        ([[".",".",".",".",".",".",".","."],
          [".","B",".",".","W",".",".","."],
          [".",".","W",".",".",".",".","."],
          [".",".",".","W","B",".",".","."],
          [".",".",".",".",".",".",".","."],
          [".",".",".",".","B","W",".","."],
          [".",".",".",".",".",".","W","."],
          [".",".",".",".",".",".",".","B"]], 4, 4, "W", False),
        ([["W","W",".","B",".","B","B","."],
          ["W","B",".",".","W","B",".","."],
          ["B","B","B","B","W","W","B","."],
          ["W","B",".",".","B","B","B","."],
          ["W","W","B",".","W",".","B","B"],
          ["B",".","B","W",".","B",".","."],
          [".","B","B","W","B","B",".","."],
          ["B","B","W",".",".","B",".","."]], 7, 4, "B", True),
        ([["W","B","B","W","W","B",".","B"],
          ["W",".",".","W","B",".",".","B"],
          [".","B","B","B","W",".","W","."],
          ["B","W","B",".","B","W",".","."],
          ["W","W",".","W","B","B","B","."],
          [".",".",".",".","W",".","B","."],
          [".",".","B","W","W","B","B","."],
          ["B","B",".","W","B",".","B","."]], 3, 7, "W", False),
        ([["W","B","W",".",".","W","W","."],
          ["W","B",".","W","W","W","B","W"],
          [".","B","B",".","B","W","B","W"],
          [".",".","B","B","B","W","W","."],
          ["B","B",".","B",".",".",".","B"],
          [".","W","B",".",".","B",".","B"],
          [".","W",".","W",".","W","B","W"],
          ["W",".","B",".","W","W","B","."]], 4, 6, "W", False),
    )
    for board, rMove, cMove, color, expected in tests:
        print(Solution().checkMove(board, rMove, cMove, color) == expected)
