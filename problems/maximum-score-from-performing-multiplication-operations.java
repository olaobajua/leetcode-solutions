/**
 * 1770. Maximum Score from Performing Multiplication Operations [Medium]
 * You are given two integer arrays nums and multipliers of size n and m
 * respectively, where n >= m. The arrays are 1-indexed.
 *
 * You begin with a score of 0. You want to perform exactly m operations. On
 * the iᵗʰ operation (1-indexed), you will:
 *     ∙ Choose one integer x from either the start or the end of the array
 *       nums.
 *     ∙ Add multipliers[i] * x to your score.
 *     ∙ Remove x from the array nums.
 *
 * Return the maximum score after performing m operations.
 *
 * Example 1:
 * Input: nums = [1,2,3], multipliers = [3,2,1]
 * Output: 14
 * Explanation: An optimal solution is as follows:
 * - Choose from the end, [1,2,3], adding 3 * 3 = 9 to the score.
 * - Choose from the end, [1,2], adding 2 * 2 = 4 to the score.
 * - Choose from the end, [1], adding 1 * 1 = 1 to the score.
 * The total score is 9 + 4 + 1 = 14.
 *
 * Example 2:
 * Input: nums = [-5,-3,-3,-2,7,1], multipliers = [-10,-5,3,4,6]
 * Output: 102
 * Explanation: An optimal solution is as follows:
 * - Choose from the start, [-5,-3,-3,-2,7,1], adding -5 * -10 = 50 to the
 * score.
 * - Choose from the start, [-3,-3,-2,7,1], adding -3 * -5 = 15 to the score.
 * - Choose from the start, [-3,-2,7,1], adding -3 * 3 = -9 to the score.
 * - Choose from the end, [-2,7,1], adding 1 * 4 = 4 to the score.
 * - Choose from the end, [-2,7], adding 7 * 6 = 42 to the score.
 * The total score is 50 + 15 - 9 + 4 + 42 = 102.
 *
 * Constraints:
 *     ∙ n == nums.length
 *     ∙ m == multipliers.length
 *     ∙ 1 <= m <= 10³
 *     ∙ m <= n <= 10⁵
 *     ∙ -1000 <= nums[i], multipliers[i] <= 1000
 */
class Solution {
    int n, m;
    int[][] cache;
    int[] nums;
    int[] multipliers;
    public int maximumScore(int[] nums, int[] multipliers) {
        this.nums = nums;
        this.multipliers = multipliers;
        n = nums.length;
        m = multipliers.length;
        cache = new int[m+1][m+1];
        for (int i = 0; i <= m; ++i)
            Arrays.fill(cache[i], -1001);
        return dp(0, 0);
    }
    private int dp(int i, int start) {
        if (cache[i][start] == -1001) {
            cache[i][start] = 0;
            if (i < m) {
                int end = n - (i - start) - 1;
                cache[i][start] = Math.max(
                    nums[start] * multipliers[i] + dp(i + 1, start + 1),
                    nums[end] * multipliers[i] + dp(i + 1, start)
                );
            }
        }
        return cache[i][start];
    };
}
