/**
 * 1022. Sum of Root To Leaf Binary Numbers (Easy)
 * You are given the root of a binary tree where each node has a value 0 or 1.
 * Each root-to-leaf path represents a binary number starting with the most
 * significant bit.
 *     ∙ For example, if the path is 0 -> 1 -> 1 -> 0 -> 1, then this could
 *       represent 01101 in binary, which is 13.
 *
 * For all leaves in the tree, consider the numbers represented by the path
 * from the root to that leaf. Return the sum of these numbers.
 *
 * The test cases are generated so that the answer fits in a 32-bits integer.
 *
 * Example 1:
 * Input: root = [1,0,1,0,1,0,1]
 * Output: 22
 * Explanation: (100) + (101) + (110) + (111) = 4 + 5 + 6 + 7 = 22
 *
 * Example 2:
 * Input: root = [0]
 * Output: 0
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 1000].
 *     ∙ Node.val is 0 or 1.
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @return {number}
 */
var sumRootToLeaf = function(root) {
    function dfs(root, prev=0) {
        if (root) {
            prev = (prev << 1) + root.val;
            dfs(root.left, prev);
            dfs(root.right, prev);
            if (!root.left && !root.right) {
                ret += prev;
            }
        }
    }
    let ret = 0;
    dfs(root);
    return ret;
};

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

const tests = [
    [[1,0,1,0,1,0,1], 22],
    [[0], 0],
];
for (const [nums, expected] of tests) {
    console.log(sumRootToLeaf(buildTree(nums)) == expected);
}
