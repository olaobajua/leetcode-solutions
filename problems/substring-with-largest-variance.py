"""
 * 2272. Substring With Largest Variance [Hard]
 * The variance of a string is defined as the largest difference between the
 * number of occurrences of any 2 characters present in the string. Note the
 * two characters may or may not be the same.
 *
 * Given a string s consisting of lowercase English letters only, return the
 * largest variance possible among all substrings of s.
 *
 * A substring is a contiguous sequence of characters within a string.
 *
 * Example 1:
 * Input: s = "aababbb"
 * Output: 3
 * Explanation:
 * All possible variances along with their respective substrings are listed
 * below:
 * - Variance 0 for substrings "a", "aa", "ab", "abab", "aababb", "ba", "b",
 * "bb", and "bbb".
 * - Variance 1 for substrings "aab", "aba", "abb", "aabab", "ababb",
 * "aababbb", and "bab".
 * - Variance 2 for substrings "aaba", "ababbb", "abbb", and "babb".
 * - Variance 3 for substring "babbb".
 * Since the largest possible variance is 3, we return it.
 *
 * Example 2:
 * Input: s = "abcde"
 * Output: 0
 * Explanation:
 * No letter occurs more than once in s, so the variance of every substring is
 * 0.
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁴
 *     ∙ s consists of lowercase English letters.
"""
from itertools import permutations

class Solution:
    def largestVariance(self, s: str) -> int:
        ret = 0
        unique_letters = set(s)
        for a, b in permutations(unique_letters, 2):
            # iterate over all possible pairs of characters and use modified
            # Kadane's algorithm where subarray must contain both characters
            # https://en.wikipedia.org/wiki/Maximum_subarray_problem
            variance = 0
            has_b = False
            subarray_starts_with_b = False
            for char in s:
                variance += char == a
                if char == b:
                    has_b = True
                    if subarray_starts_with_b and variance >= 0:
                        subarray_starts_with_b = False
                    elif variance < 1:
                        subarray_starts_with_b = True
                        variance = -1
                    else:
                        variance -= 1
                ret = max(ret, variance if has_b else 0)
        return ret

if __name__ == "__main__":
    tests = (
        ("aababbb", 3),
        ("abcde", 0),
        ("abbabaaba", 2),
    )
    for s, expected in tests:
        print(Solution().largestVariance(s) == expected)
