/**
 * 438. Find All Anagrams in a String (Medium)
 * Given two strings s and p, return an array of all the start indices of p's
 * anagrams in s. You may return the answer in any order.
 *
 * An Anagram is a word or phrase formed by rearranging the letters of a
 * different word or phrase, typically using all the original letters exactly
 * once.
 *
 * Example 1:
 * Input: s = "cbaebabacd", p = "abc"
 * Output: [0,6]
 * Explanation:
 * The substring with start index = 0 is "cba", which is an anagram of "abc".
 * The substring with start index = 6 is "bac", which is an anagram of "abc".
 *
 * Example 2:
 * Input: s = "abab", p = "ab"
 * Output: [0,1,2]
 * Explanation:
 * The substring with start index = 0 is "ab", which is an anagram of "ab".
 * The substring with start index = 1 is "ba", which is an anagram of "ab".
 * The substring with start index = 2 is "ab", which is an anagram of "ab".
 *
 * Constraints:
 *     ∙ 1 <= s.length, p.length <= 3 * 10⁴
 *     ∙ s and p consist of lowercase English letters.
 */

/**
 * @param {string} s
 * @param {string} p
 * @return {number[]}
 */
var findAnagrams = function(s, p) {
    function isEqual() {
        for (let i = 0; i < 26; ++i) {
            if (sc[i] != pc[i]) {
                return false;
            }
        }
        return true;
    }
    const a = 'a'.charCodeAt();
    const pc = new Array(26).fill(0);
    Array.from(p).forEach(char => ++pc[char.charCodeAt() - a]);
    const m = p.length;
    const sc = new Array(26).fill(0);
    Array.from(s.slice(0, m)).forEach(char => ++sc[char.charCodeAt() - a]);
    const ret = [];
    if (isEqual()) {
        ret.push(0);
    }
    for (let i = 0; i < s.length - m; ++i) {
        --sc[s[i].charCodeAt() - a];
        ++sc[s[i+m].charCodeAt() - a];
        if (isEqual()) {
            ret.push(i + 1);
        }
    }
    return ret;
};

const tests = [
    ["cbaebabacd", "abc", [0,6]],
    ["abab", "ab", [0,1,2]],
];

for (const [s, p, expected] of tests) {
    console.log(findAnagrams(s, p).toString() == expected.toString());
}
