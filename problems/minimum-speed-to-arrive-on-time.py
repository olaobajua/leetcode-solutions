"""
 * 1870. Minimum Speed to Arrive on Time [Medium]
 * You are given a floating-point number hour, representing the amount of time
 * you have to reach the office. To commute to the office, you must take n
 * trains in sequential order. You are also given an integer array dist of
 * length n, where dist[i] describes the distance (in kilometers) of the iᵗʰ
 * train ride.
 *
 * Each train can only depart at an integer hour, so you may need to wait in
 * between each train ride.
 *
 *     ∙ For example, if the 1ˢᵗ train ride takes 1.5 hours, you must wait for
 *       an additional 0.5 hours before you can depart on the 2ⁿᵈ train ride at
 *       the 2 hour mark.
 *
 * Return the minimum positive integer speed (in kilometers per hour) that all
 * the trains must travel at for you to reach the office on time, or -1 if it
 * is impossible to be on time.
 *
 * Tests are generated such that the answer will not exceed 10⁷ and hour will
 * have at most two digits after the decimal point.
 *
 * Example 1:
 * Input: dist = [1,3,2], hour = 6
 * Output: 1
 * Explanation: At speed 1:
 * - The first train ride takes 1/1 = 1 hour.
 * - Since we are already at an integer hour, we depart immediately at the 1
 * hour mark. The second train takes 3/1 = 3 hours.
 * - Since we are already at an integer hour, we depart immediately at the 4
 * hour mark. The third train takes 2/1 = 2 hours.
 * - You will arrive at exactly the 6 hour mark.
 *
 * Example 2:
 * Input: dist = [1,3,2], hour = 2.7
 * Output: 3
 * Explanation: At speed 3:
 * - The first train ride takes 1/3 = 0.33333 hours.
 * - Since we are not at an integer hour, we wait until the 1 hour mark to
 * depart. The second train ride takes 3/3 = 1 hour.
 * - Since we are already at an integer hour, we depart immediately at the 2
 * hour mark. The third train takes 2/3 = 0.66667 hours.
 * - You will arrive at the 2.66667 hour mark.
 *
 * Example 3:
 * Input: dist = [1,3,2], hour = 1.9
 * Output: -1
 * Explanation: It is impossible because the earliest the third train can
 * depart is at the 2 hour mark.
 *
 * Constraints:
 *     ∙ n == dist.length
 *     ∙ 1 <= n <= 10⁵
 *     ∙ 1 <= dist[i] <= 10⁵
 *     ∙ 1 <= hour <= 10⁹
 *     ∙ There will be at most two digits after the decimal point in hour.
"""
from math import ceil
from typing import List

class Solution:
    def minSpeedOnTime(self, dist: List[int], hour: float) -> int:
        def count_time(speed):
            return sum(ceil(dist[i]/speed) for i in range(n - 1)) + last/speed

        if len(dist) >= hour + 1:
            return -1

        n = len(dist)
        last = dist[-1]
        lo = max(1, int(sum(dist) / hour))
        hi = 10 * lo
        if int(hour) < hour:
            hi = max(hi, ceil(last / ((hour - int(hour)))))
        else:
            hi = max(hi, last)
        while lo < hi:
            mid = (lo + hi) // 2
            arrival_time = count_time(mid)
            if arrival_time < hour:
                hi = mid
            elif arrival_time > hour:
                lo = mid + 1
            else:
                return mid

        return lo

if __name__ == "__main__":
    tests = (
        ([1,3,2], 6, 1),
        ([1,3,2], 2.7, 3),
        ([1,3,2], 1.9, -1),
        ([1,1,100000], 2.01, 10000000),
        ([1,1], 1, -1),
        ([69], 4.6, 15),
        ([8,4,1,2], 3.9, 8),
        ([2,1,5,4,4,3,2,9,2,10], 75.12, 1),
        ([47,40,31,8,31,73,11,11,94,63,9,98,69,99,17,17,85,61,71,22,34,68,78,55,28,70,97,94,89,26,92,40,52,86,84,48,57,67,58,16,32,29,9,44,3,76,71,30,76,29,1,10,91,81,8,30,9], 73.58, 71),
    )
    for dist, hour, expected in tests:
        print(Solution().minSpeedOnTime(dist, hour) == expected)
