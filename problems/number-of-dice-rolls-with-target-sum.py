"""
 * 1155. Number of Dice Rolls With Target Sum [Medium]
 * You have n dice and each die has k faces numbered from 1 to k.
 *
 * Given three integers n, k, and target, return the number of possible ways
 * (out of the kⁿ total ways) to roll the dice so the sum of the face-up
 * numbers equals target. Since the answer may be too large, return it modulo
 * 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 1, k = 6, target = 3
 * Output: 1
 * Explanation: You throw one die with 6 faces.
 * There is only one way to get a sum of 3.
 *
 * Example 2:
 * Input: n = 2, k = 6, target = 7
 * Output: 6
 * Explanation: You throw two dice, each with 6 faces.
 * There are 6 ways to get a sum of 7: 1+6, 2+5, 3+4, 4+3, 5+2, 6+1.
 *
 * Example 3:
 * Input: n = 30, k = 30, target = 500
 * Output: 222616187
 * Explanation: The answer must be returned modulo 10⁹ + 7.
 *
 * Constraints:
 *     ∙ 1 <= n, k <= 30
 *     ∙ 1 <= target <= 1000
"""
from functools import cache

class Solution:
    @cache
    def numRollsToTarget(self, n: int, k: int, target: int) -> int:
        if n == 0 or target <= 0:
            return n == target
        return sum(self.numRollsToTarget(n - 1, k, target - i)
                   for i in range(1, k + 1)) % 1000000007

if __name__ == "__main__":
    tests = (
        (1, 6, 3, 1),
        (2, 6, 7, 6),
        (6, 6, 20, 4221),
        (5, 28, 116, 20475),
        (5, 29, 116, 40915),
        (30, 30, 500, 222616187),
    )
    for n, k, target, expected in tests:
        print(Solution().numRollsToTarget(n, k, target) == expected)
