/**
 * 224. Basic Calculator (Hard)
 * Given a string s representing a valid expression, implement a basic
 * calculator to evaluate it, and return the result of the evaluation.
 *
 * Note: You are not allowed to use any built-in function which evaluates
 * strings as mathematical expressions, such as eval().
 *
 * Example 1:
 * Input: s = "1 + 1"
 * Output: 2
 *
 * Example 2:
 * Input: s = " 2-1 + 2 "
 * Output: 3
 *
 * Example 3:
 * Input: s = "(1+(4+5+2)-3)+(6+8)"
 * Output: 23
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 3 * 10⁵
 *     ∙ s consists of digits, '+', '-', '(', ')', and ' '.
 *     ∙ s represents a valid expression.
 *     ∙ '+' is not used as a unary operation.
 *     ∙ '-' could be used as a unary operation but it has to be inside
 *       parentheses.
 *     ∙ There will be no two consecutive operators in the input.
 *     ∙ Every number and running calculation will fit in a signed 32-bit
 *       integer.
 */

/**
 * @param {string} s
 * @return {number}
 */
var calculate = function(s) {
    const stack = [];
    for (const element of mathInfixToRpn(s.replace(/\(-/g ,"(0-"))) {
        if (Number.isInteger(element)) {
            stack.push(element);
        } else {
            if (element == "+") {
                stack.push(stack.pop() + stack.pop());
            } else if (element == "-") {
                stack.push(-stack.pop() + stack.pop());
            }
        }
    }
    return stack.pop();
};

function mathInfixToRpn(expr) {
    /**
     * Transforms expr from infix notation to reverse polish notation.
     * This is classical shunting-yard algorithm by Edsger Dijkstra.
     */
    const PRECEDENCE = {
        '*': 2,
        '/': 2,
        '+': 1,
        '-': 1,
        '(': 0,
        ')': 0,
    };
    const ret = [0, ' '];
    const stack = [];
    for (const c of expr) {
        if ('0' <= c && c <= '9') {
            if (Number.isInteger(ret[ret.length-1])) {
                ret.push(10*ret.pop() + parseInt(c));
            } else {
                ret.push(parseInt(c));
            }
        } else {
            if (ret[ret.length-1] !== ' ') {
                ret.push(' ');
            }
            if ('*/+-'.includes(c)) {
                while (PRECEDENCE[c] <= PRECEDENCE[stack[stack.length-1]]) {
                    ret.push(stack.pop());
                }
                stack.push(c);
            } else if (c == '(') {
                stack.push('(');
            } else if (c == ')') {
                let tmp;
                while ((tmp = stack.pop()) != '(') {
                    ret.push(tmp);
                }
            }
        }
    }
    while (stack.length) {
        ret.push(stack.pop());
    }
    return ret.filter(element => element !== ' ');
}

const tests = [
    ["1 + 1", 2],
    [" 2-1 + 2 ", 3],
    ["(1+(4+5+2)-3)+(6+8)", 23],
    ["0", 0],
    ["-2+ 1", -1],
    ["1 + (-1)", 0],
    ["1-(-2)", 3],
    ["1-(1-2)", 2],
];

for (const [s, expected] of tests) {
    console.log(calculate(s) == expected);
}
