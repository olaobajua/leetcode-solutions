/**
 * 234. Palindrome Linked List [Easy]
 * Given the head of a singly linked list, return true if it is a palindrome.
 *
 * Example 1:
 * Input: head = [1,2,2,1]
 * Output: true
 *
 * Example 2:
 * Input: head = [1,2]
 * Output: false
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [1, 10⁵].
 *     ∙ 0 <= Node.val <= 9
 *
 * Follow up: Could you do it in O(n) time and O(1) space?
 */

// Definition for singly-linked list.
public class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

class Solution {
    public boolean isPalindrome(ListNode head) {
        ListNode slow = head, fast = head, rev = null;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            ListNode tmp = rev;
            rev = slow;
            slow = slow.next;
            rev.next = tmp;
        }
        if (fast != null)
            slow = slow.next;
        while (rev != null && rev.val == slow.val) {
            slow = slow.next;
            rev = rev.next;
        }
        return rev == null;
    }
}
