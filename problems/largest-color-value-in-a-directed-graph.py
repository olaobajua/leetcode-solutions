"""
 * 1857. Largest Color Value in a Directed Graph [Hard]
 * There is a directed graph of n colored nodes and m edges. The nodes are
 * numbered from 0 to n - 1.
 *
 * You are given a string colors where colors[i] is a lowercase English letter
 * representing the color of the iᵗʰ node in this graph (0-indexed). You are
 * also given a 2D array edges where edges[j] = [aⱼ, bⱼ] indicates that there
 * is a directed edge from node aⱼ to node bⱼ.
 *
 * A valid path in the graph is a sequence of nodes x₁ -> x₂ -> x₃ -> ... ->
 * xₖ such that there is a directed edge from xᵢ to xᵢ₊₁ for every 1 <= i < k.
 * The color value of the path is the number of nodes that are colored the most
 * frequently occurring color along that path.
 *
 * Return the largest color value of any valid path in the given graph, or -1
 * if the graph contains a cycle.
 *
 * Example 1:
 * Input: colors = "abaca", edges = [[0,1],[0,2],[2,3],[3,4]]
 * Output: 3
 * Explanation: The path 0 -> 2 -> 3 -> 4 contains 3 nodes that are colored
 * "a" (red in the above image).
 *
 * Example 2:
 * Input: colors = "a", edges = [[0,0]]
 * Output: -1
 * Explanation: There is a cycle from 0 to 0.
 *
 * Constraints:
 *     ∙ n == colors.length
 *     ∙ m == edges.length
 *     ∙ 1 <= n <= 10⁵
 *     ∙ 0 <= m <= 10⁵
 *     ∙ colors consists of lowercase English letters.
 *     ∙ 0 <= aⱼ, bⱼ < n
"""
from collections import Counter
from functools import cache
from typing import List

class Solution:
    def largestPathValue(self, colors: str, edges: List[List[int]]) -> int:
        @cache
        def dfs(node):
            if node in seen:
                return -1
            seen.add(node)
            ret = Counter()
            for neib in graph[node]:
                cur = dfs(neib)
                if cur == -1:
                    return -1
                for c in cur:
                    ret[c] = max(ret[c], cur[c])
            ret[colors[node]] += 1
            return ret

        n = len(colors)
        graph = [[] for _ in range(n)]
        for a, b in edges:
            graph[a].append(b)
        ret = 0
        for i in range(n):
            seen = set()
            cur = dfs(i)
            if cur == -1:
                return -1
            ret = max(ret, max(cur.values(), default=0))
        return ret

if __name__ == "__main__":
    tests = (
        ("abaca", [[0,1],[0,2],[2,3],[3,4]], 3),
        ("a", [[0,0]], -1),
        ("hhqhuqhqff", [[0,1],[0,2],[2,3],[3,4],[3,5],[5,6],[2,7],[6,7],[7,8],[3,8],[5,8],[8,9],[3,9],[6,9]], 3),
        ("g", [], 1),
    )
    for colors, edges, expected in tests:
        print(Solution().largestPathValue(colors, edges) == expected)
