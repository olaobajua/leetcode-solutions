/**
 * 1235. Maximum Profit in Job Scheduling [Hard]
 * We have n jobs, where every job is scheduled to be done from startTime[i]
 * to endTime[i], obtaining a profit of profit[i].
 *
 * You're given the startTime, endTime and profit arrays, return the maximum
 * profit you can take such that there are no two jobs in the subset with
 * overlapping time range.
 *
 * If you choose a job that ends at time X you will be able to start another
 * job that starts at time X.
 *
 * Example 1:
 * Input: startTime = [1,2,3,3], endTime = [3,4,5,6], profit = [50,10,40,70]
 * Output: 120
 * Explanation: The subset chosen is the first and fourth job.
 * Time range [1-3]+[3-6] , we get profit of 120 = 50 + 70.
 *
 * Example 2:
 *
 * Input: startTime = [1,2,3,4,6], endTime = [3,5,10,6,9], profit =
 * [20,20,100,70,60]
 * Output: 150
 * Explanation: The subset chosen is the first, fourth and fifth job.
 * Profit obtained 150 = 20 + 70 + 60.
 *
 * Example 3:
 * Input: startTime = [1,1,1], endTime = [2,3,4], profit = [5,6,4]
 * Output: 6
 *
 * Constraints:
 *     ∙ 1 <= startTime.length == endTime.length == profit.length <= 5 * 10⁴
 *     ∙ 1 <= startTime[i] < endTime[i] <= 10⁹
 *     ∙ 1 <= profit[i] <= 10⁴
 */
#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>

class Solution {
public:
    int jobScheduling(vector<int>& startTime, vector<int>& endTime, vector<int>& profit) {
        int n = startTime.size();
        std::vector<std::tuple<int, int, int>> jobs;

        for (int i = 0; i < n; ++i) {
            jobs.emplace_back(startTime[i], endTime[i], profit[i]);
        }

        std::sort(jobs.begin(), jobs.end());
        for (int i = 0; i < n; ++i) {
            startTime[i] = std::get<0>(jobs[i]);
        }

        std::vector<int> memo(n, -1);

        std::function<int(int)> dp = [&](int i) -> int {
            if (i == n) {
                return 0;
            }

            if (memo[i] != -1) {
                return memo[i];
            }

            int s, e, p;
            std::tie(s, e, p) = jobs[i];

            int j = std::lower_bound(startTime.begin(), startTime.end(), e) -
                    startTime.begin();

            int ret = std::max(dp(i + 1), dp(j) + p);

            return memo[i] = ret;
        };

        return dp(0);
    }
};
