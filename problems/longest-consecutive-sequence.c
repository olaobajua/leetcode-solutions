/**
 * 128. Longest Consecutive Sequence (Medium)
 * Given an unsorted array of integers nums, return the length of the longest
 * consecutive elements sequence.
 *
 * You must write an algorithm that runs in O(n) time.
 *
 * Example 1:
 * Input: nums = [100,4,200,1,3,2]
 * Output: 4
 * Explanation: The longest consecutive elements sequence is [1, 2, 3, 4].
 * Therefore its length is 4.
 *
 * Example 2:
 * Input: nums = [0,3,7,2,5,8,4,6,0,1]
 * Output: 9
 *
 * Constraints:
 *     ∙ 0 <= nums.length <= 10⁵
 *     ∙ -10⁹ <= nums[i] <= 10⁹
 */
int compare(const int *a, const int *b) {
  return *a - *b;
}
int longestConsecutive(int *nums, int numsSize) {
    if (!numsSize) {
        return 0;
    }
    int ret = 1, count = 1;
    qsort(nums, numsSize, sizeof(int), compare);
    for (int i = 0; i < numsSize - 1; ++i) {
        if (nums[i] == nums[i+1] - 1) {
            ++count;
        } else if (nums[i] != nums[i+1]) {
            count = 1;
        }
        ret = fmax(ret, count);
    }
    return ret;
}
