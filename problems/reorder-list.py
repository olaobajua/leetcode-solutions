"""
 * 143. Reorder List (Medium)
 * You are given the head of a singly linked-list. The list can be represented
 * as:
 * L0 → L1 → … → Ln - 1 → Ln
 *
 * Reorder the list to be on the following form:
 * L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
 *
 * You may not modify the values in the list's nodes. Only nodes themselves may
 * be changed.
 *
 * Example 1:
 * Input: head = [1,2,3,4]
 * Output: [1,4,2,3]
 *
 * Example 2:
 * Input: head = [1,2,3,4,5]
 * Output: [1,5,2,4,3]
 *
 * Constraints:
 *     The number of nodes in the list is in the range [1, 5 * 10⁴].
 *     1 <= Node.val <= 1000
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def reorderList(self, head: Optional[ListNode]) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        end = mid = head
        while end.next and (end := end.next.next):
            mid = mid.next
        mid.next, mid = None, mid.next
        mid = reverse_recur(mid)
        while mid:
            mid.next, head.next, head, mid = head.next, mid, head.next, mid.next

def reverse_recur(head, prev=None):
    return reverse_recur(head.next, ListNode(head.val, prev)) if head else prev

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3,4], [1,4,2,3]),
        ([1,2,3,4,5], [1,5,2,4,3]),
    )
    for nums, expected in tests:
        root = list_to_linked_list(nums)
        Solution().reorderList(root)
        print(linked_list_to_list(root) == expected)
