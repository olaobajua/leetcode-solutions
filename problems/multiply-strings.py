"""
 * 43. Multiply Strings (Medium)
 * Given two non-negative integers num1 and num2 represented as strings, return
 * the product of num1 and num2, also represented as a string.
 *
 * Note: You must not use any built-in BigInteger library or convert the inputs
 * to integer directly.
 *
 * Example 1:
 * Input: num1 = "2", num2 = "3"
 * Output: "6"
 *
 * Example 2:
 * Input: num1 = "123", num2 = "456"
 * Output: "56088"
 *
 * Constraints:
 *     ∙ 1 <= num1.length, num2.length <= 200
 *     ∙ num1 and num2 consist of digits only.
 *     ∙ Both num1 and num2 do not contain any leading zero, except the number
 *       0 itself.
"""
from itertools import zip_longest
from typing import List

class Solution:
    def multiply(self, num1: str, num2: str) -> str:
        reversed_num1 = [*map(int, reversed(num1))] + [0]
        reversed_num2 = [*map(int, reversed(num2))] + [0]
        reg = 0
        ret = [0] * (len(num1) + len(num2))
        for i, n1 in enumerate(reversed_num1):
            cur = []
            for n2 in reversed_num2:
                mul = n1 * n2 + reg
                reg = mul // 10
                cur.append(mul % 10)
            ret = ret[:i] + self.__add(ret[i:], cur)
        while ret and ret[-1] == 0:
            ret.pop()
        return ''.join(map(str, reversed(ret or [0])))

    def __add(self, num1: List[int], num2: List[int]) -> List[int]:
        reg = 0
        ret = []
        for n1, n2 in zip_longest(num1, num2, fillvalue=0):
            add = n1 + n2 + reg
            reg = add // 10
            ret.append(add % 10)
        return ret

if __name__ == "__main__":
    tests = (
        ("2", "3", "6"),
        ("123", "456", "56088"),
        ("3", "456", "1368"),
        ("0", "0", "0"),

    )
    for num1, num2, expected in tests:
        print(Solution().multiply(num1, num2) == expected)
