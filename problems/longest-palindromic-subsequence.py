"""
 * 516. Longest Palindromic Subsequence [Medium]
 * Given a string s, find the longest palindromic subsequence's length in s.
 *
 * A subsequence is a sequence that can be derived from another sequence by
 * deleting some or no elements without changing the order of the remaining
 * elements.
 *
 * Example 1:
 * Input: s = "bbbab"
 * Output: 4
 * Explanation: One possible longest palindromic subsequence is "bbbb".
 *
 * Example 2:
 * Input: s = "cbbd"
 * Output: 2
 * Explanation: One possible longest palindromic subsequence is "bb".
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 1000
 *     ∙ s consists only of lowercase English letters.
"""
from functools import cache

class Solution:
    def longestPalindromeSubseq(self, s: str) -> int:
        @cache
        def dp(left, right):
            if left > right:
                return 0
            if left == right:
                return 1
            if s[left] == s[right]:
                return dp(left + 1, right - 1) + 2
            return max(dp(left, right - 1), dp(left + 1, right))

        n = len(s)
        return dp(0, n - 1)

if __name__ == "__main__":
    tests = (
        ("bbbab", 4),
        ("cbbd", 2),
    )
    for s, expected in tests:
        print(Solution().longestPalindromeSubseq(s) == expected)
