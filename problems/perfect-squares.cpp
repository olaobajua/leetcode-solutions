/**
 * 279. Perfect Squares [Medium]
 * Given an integer n, return the least number of perfect square numbers that
 * sum to n.
 *
 * A perfect square is an integer that is the square of an integer; in other
 * words, it is the product of some integer with itself. For example, 1, 4, 9,
 * and 16 are perfect squares while 3 and 11 are not.
 *
 * Example 1:
 * Input: n = 12
 * Output: 3
 * Explanation: 12 = 4 + 4 + 4.
 *
 * Example 2:
 * Input: n = 13
 * Output: 2
 * Explanation: 13 = 4 + 9.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁴
 */
#include <vector>
#include <algorithm>
#include <cmath>

class Solution {
public:
    Solution() {
        for (int x = 1; x <= 100; ++x) {
            PERFECT_SQUARES[x-1] = x * x;
        }
    }
    int numSquares(int n) {
        return dp(n);
    }

private:
    std::vector<int> memo = std::vector<int>(10001, -1);
    std::vector<int> PERFECT_SQUARES = std::vector<int>(100, 0);

    int dp(int x) {
        if (x == 0) {
            return 0;
        }
        if (x < 0) {
            return numeric_limits<int>::max();
        }

        if (memo[x] > -1) {
            return memo[x];
        }

        memo[x] = numeric_limits<int>::max();
        for (size_t i = 0; PERFECT_SQUARES[i] <= x; ++i) {
            memo[x] = std::min(memo[x], dp(x - PERFECT_SQUARES[i]) + 1);
        }

        return memo[x];
    }
};
