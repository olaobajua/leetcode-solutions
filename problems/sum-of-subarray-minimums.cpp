/**
 * 907. Sum of Subarray Minimums [Medium]
 * Given an array of integers arr, find the sum of min(b), where b ranges over
 * every (contiguous) subarray of arr. Since the answer may be large, return
 * the answer modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: arr = [3,1,2,4]
 * Output: 17
 * Explanation:
 * Subarrays are [3], [1], [2], [4], [3,1], [1,2], [2,4], [3,1,2], [1,2,4],
 * [3,1,2,4].
 * Minimums are 3, 1, 2, 4, 1, 1, 2, 1, 1, 1.
 * Sum is 17.
 *
 * Example 2:
 * Input: arr = [11,81,94,43,3]
 * Output: 444
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 3 * 10⁴
 *     ∙ 1 <= arr[i] <= 3 * 10⁴
 */
#include <iostream>
#include <vector>

class Solution {
public:
    int sumSubarrayMins(vector<int>& arr) {
        int ret = 0;
        vector<int>stack(arr.size() + 1, 0);
        int j = 0;
        const int MOD = 1000000007;
        stack[0] = -1;

        for (int i = 0; i < arr.size() + 1; ++i) {
            int x = i < arr.size() ? arr[i] : 0;
            while (stack[j] != -1 && x < arr[stack[j]]) {
                int mid = stack[j--];
                int right = i - mid;
                long left = mid - stack[j];
                ret = (ret + left * right * arr[mid]) % MOD;
            }
            stack[++j] = i;
        }
        return ret;
    }
};
