"""
 * 1351. Count Negative Numbers in a Sorted Matrix [Easy]
 * Given a m x n matrix grid which is sorted in non-increasing order both
 * row-wise and column-wise, return the number of negative numbers in grid.
 *
 * Example 1:
 * Input: grid = [[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]]
 * Output: 8
 * Explanation: There are 8 negatives number in the matrix.
 *
 * Example 2:
 * Input: grid = [[3,2],[1,0]]
 * Output: 0
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 100
 *     ∙ -100 <= grid[i][j] <= 100
 *
 * Follow up: Could you find an O(n + m) solution?
"""
from typing import List

class Solution:
    def countNegatives(self, grid: List[List[int]]) -> int:
        m = len(grid)
        n = len(grid[0])
        ret = col = 0
        for row in range(m - 1, -1, -1):
            for col in range(col, n):
                if grid[row][col] < 0:
                    ret += n - col
                    break
        return ret

if __name__ == "__main__":
    tests = (
        ([[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]], 8),
        ([[3,2],[1,0]], 0),
    )
    for grid, expected in tests:
        print(Solution().countNegatives(grid) == expected)
