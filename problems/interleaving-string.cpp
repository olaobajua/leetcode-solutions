/**
 * 97. Interleaving String (Medium)
 * Given strings s1, s2, and s3, find whether s3 is formed by an interleaving
 * of s1 and s2.
 *
 * An interleaving of two strings s and t is a configuration where they are
 * divided into non-empty substrings such that:
 *     ∙ s = s1 + s2 + ... + sn
 *     ∙ t = t1 + t2 + ... + tm
 *     ∙ |n - m| <= 1
 *     ∙ The interleaving is s1 + t1 + s2 + t2 + s3 + t3 + ... or
 *       t1 + s1 + t2 + s2 + t3 + s3 + ...
 *
 * Note: a + b is the concatenation of strings a and b.
 *
 * Example 1:
 * Input: s1 = "aabcc", s2 = "dbbca", s3 = "aadbbcbcac"
 * Output: true
 *
 * Example 2:
 * Input: s1 = "aabcc", s2 = "dbbca", s3 = "aadbbbaccc"
 * Output: false
 *
 * Example 3:
 * Input: s1 = "", s2 = "", s3 = ""
 * Output: true
 *
 * Constraints:
 *     ∙ 0 <= s1.length, s2.length <= 100
 *     ∙ 0 <= s3.length <= 200
 *     ∙ s1, s2, and s3 consist of lowercase English letters.
 *
 * Follow up: Could you solve it using only O(s2.length) additional memory
 * space?
 */
class Solution {
private:
    int n1, n2, n3;
    string s1, s2, s3;
    vector<vector<char>> cache;
public:
    bool isInterleave(string s1, string s2, string s3) {
        n1 = s1.length();
        n2 = s2.length();
        n3 = s3.length();
        this->s1 = s1;
        this->s2 = s2;
        this->s3 = s3;
        cache = vector<vector<char>>(n1 + 1, vector<char>(n2 + 1));
        return dp(0, 0);
    }
    int dp(int i1, int i2) {
        if (cache[i1][i2] == 0) {
            if (i1 + i2 == n3) {
                cache[i1][i2] = i1 == n1 && i2 == n2 ? 2 : 1;
            } else if (i1 < n1 && s1[i1] == s3[i1+i2] && dp(i1 + 1, i2)) {
                cache[i1][i2] = 2;
            } else if (i2 < n2 && s2[i2] == s3[i1+i2] && dp(i1, i2 + 1)) {
                cache[i1][i2] = 2;
            } else {
                cache[i1][i2] = 1;
            }
        }
        return cache[i1][i2] == 2;
    }
};
