/**
 * 231. Power of Two (Easy)
 * Given an integer n, return true if it is a power of two. Otherwise, return
 * false.
 *
 * An integer n is a power of two, if there exists an integer x such that
 * n == 2x.
 *
 * Example 1:
 * Input: n = 1
 * Output: true
 * Explanation: 20 = 1
 *
 * Example 2:
 * Input: n = 16
 * Output: true
 * Explanation: 24 = 16
 *
 * Example 3:
 * Input: n = 3
 * Output: false
 *
 * Constraints:
 *     -2³¹ <= n <= 2³¹ - 1
 *
 * Follow up: Could you solve it without loops/recursion?
 */
#include <stdbool.h>
#include <stdio.h>

bool isPowerOfTwo(int n){
    return n > 0 && (1L << 32) % n == 0;
}

int main() {
    const int tests[] = {1, 16, 3, 0};
    const bool expected[] = {true, true, false, false};
    const size_t n = sizeof(tests) / sizeof(tests[0]);
    for (int i = 0; i < n; ++i) {
        printf(isPowerOfTwo(tests[i]) == expected[i] ? "true\n" : "false\n");
    }
}
