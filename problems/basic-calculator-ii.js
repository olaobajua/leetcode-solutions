/**
 * 227. Basic Calculator II (Medium)
 * Given a string s which represents an expression, evaluate this expression
 * and return its value.
 *
 * The integer division should truncate toward zero.
 *
 * You may assume that the given expression is always valid. All intermediate
 * results will be in the range of [-2³¹, 2³¹ - 1].
 *
 * Note: You are not allowed to use any built-in function which evaluates
 * strings as mathematical expressions, such as eval().
 *
 * Example 1:
 * Input: s = "3+2*2"
 * Output: 7
 *
 * Example 2:
 * Input: s = " 3/2 "
 * Output: 1
 *
 * Example 3:
 * Input: s = " 3+5 / 2 "
 * Output: 5
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 3 * 10⁵
 *     ∙ s consists of integers and operators ('+', '-', '*', '/') separated by
 *       some number of spaces.
 *     ∙ s represents a valid expression.
 *     ∙ All the integers in the expression are non-negative integers in the
 *       range [0, 2³¹ - 1].
 *     ∙ The answer is guaranteed to fit in a 32-bit integer.
 */

/**
 * @param {string} s
 * @return {number}
 */
var calculate = function(s) {
    const stack = [];
    for (const element of mathInfixToRpn(s)) {
        if (Number.isInteger(element)) {
            stack.push(element);
        } else {
            switch (element) {
                case '+':
                    stack.push(stack.pop() + stack.pop());
                    break;
                case '-':
                    stack.push(-stack.pop() + stack.pop());
                    break;
                case '/':
                    const prev = stack.pop();
                    stack.push(Math.floor(stack.pop() / prev));
                    break;
                case '*':
                    stack.push(stack.pop() * stack.pop());
            }
        }
    }
    return stack.pop();
};

function mathInfixToRpn(expr) {
    /**
     * Transforms expr from infix notation to reverse polish notation.
     * This is classical shunting-yard algorithm by Edsger Dijkstra.
     */
    const PRECEDENCE = {
        '*': 2,
        '/': 2,
        '+': 1,
        '-': 1,
        '(': 0,
        ')': 0,
    };
    const ret = [0, ' '];
    const stack = [];
    for (const c of expr) {
        if ('0' <= c && c <= '9') {
            if (Number.isInteger(ret[ret.length-1])) {
                ret.push(10*ret.pop() + parseInt(c));
            } else {
                ret.push(parseInt(c));
            }
        } else {
            if (ret[ret.length-1] !== ' ') {
                ret.push(' ');
            }
            if ('*/+-'.includes(c)) {
                while (PRECEDENCE[c] <= PRECEDENCE[stack[stack.length-1]]) {
                    ret.push(stack.pop());
                }
                stack.push(c);
            } else if (c == '(') {
                stack.push('(');
            } else if (c == ')') {
                let tmp;
                while ((tmp = stack.pop()) != '(') {
                    ret.push(tmp);
                }
            }
        }
    }
    while (stack.length) {
        ret.push(stack.pop());
    }
    return ret.filter(element => element !== ' ');
}

const tests = [
    ["3+2*2", 7],
    [" 3/2 ", 1],
    [" 3+5 / 2 ", 5],
    ["0", 0],
];

for (const [s, expected] of tests) {
    console.log(calculate(s) == expected);
}
