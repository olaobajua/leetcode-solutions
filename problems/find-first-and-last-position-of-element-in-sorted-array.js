/**
 * 34. Find First and Last Position of Element in Sorted Array [Medium]
 * Given an array of integers nums sorted in non-decreasing order, find the
 * starting and ending position of a given target value.
 *
 * If target is not found in the array, return [-1, -1].
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 * Example 1:
 * Input: nums = [5,7,7,8,8,10], target = 8
 * Output: [3,4]
 * Example 2:
 * Input: nums = [5,7,7,8,8,10], target = 6
 * Output: [-1,-1]
 * Example 3:
 * Input: nums = [], target = 0
 * Output: [-1,-1]
 *
 * Constraints:
 *     ∙ 0 <= nums.length <= 10⁵
 *     ∙ -10⁹ <= nums[i] <= 10⁹
 *     ∙ nums is a non-decreasing array.
 *     ∙ -10⁹ <= target <= 10⁹
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var searchRange = function(nums, target) {
    const lo = bisectLeft(nums, target);
    const hi = bisectRight(nums, target);
    return lo == hi || lo == nums.length ? [-1, -1] : [lo, hi - 1];
};

function bisectLeft(a, x, cmp=(a, b) => a - b, lo=0, hi=a.length) {
    if (!a.length)
        return 0;
    while (lo + 1 < hi) {
        const mid = Math.floor((hi + lo) / 2);
        cmp(x, a[mid]) <= 0 ? hi = mid : lo = mid;
    }
    return a[lo] == x || a[hi] > x ? lo : hi;
}

function bisectRight(a, x, cmp=(a, b) => a - b, lo=0, hi=a.length) {
    if (!a.length)
        return 0;
    while (lo + 1 < hi) {
        const mid = Math.floor((hi + lo) / 2);
        cmp(x, a[mid]) >= 0 ? lo = mid : hi = mid;
    }
    return a[hi] == x ? hi + 1 : a[lo] == x ? lo + 1 : lo;
}

const tests = [
    [[5,7,7,8,8,10], 8, [3,4]],
    [[5,7,7,8,8,10], 6, [-1,-1]],
    [[], 0, [-1,-1]],
    [[2,2], 2, [0,1]],
    [[2,2], 3, [-1,-1]],
];

for (const [nums, target, expected] of tests) {
    console.log(searchRange(nums, target).toString() == expected.toString());
}
