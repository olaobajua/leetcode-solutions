"""
 * 560. Subarray Sum Equals K [Medium]
 * Given an array of integers nums and an integer k, return the total number of
 * subarrays whose sum equals to k.
 *
 * A subarray is a contiguous non-empty sequence of elements within an array.
 *
 * Example 1:
 * Input: nums = [1,1,1], k = 2
 * Output: 2
 * Example 2:
 * Input: nums = [1,2,3], k = 3
 * Output: 2
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 2 * 10⁴
 *     ∙ -1000 <= nums[i] <= 1000
 *     ∙ -10⁷ <= k <= 10⁷
"""
from collections import defaultdict
from typing import List

class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        sums = defaultdict(int)
        ret = cur = 0
        for x in [0] + nums:
            cur += x
            ret += sums[cur-k]
            sums[cur] += 1
        return ret

if __name__ == "__main__":
    tests = (
        ([1,1,1], 2, 2),
        ([1,2,3], 3, 2),
        ([-1,-1,1], 0, 1),
    )
    for nums, k, expected in tests:
        print(Solution().subarraySum(nums, k) == expected)
