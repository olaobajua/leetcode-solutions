"""
 * 433. Minimum Genetic Mutation [Medium]
 * A gene string can be represented by an 8-character long string, with
 * choices from 'A', 'C', 'G', and 'T'.
 *
 * Suppose we need to investigate a mutation from a gene string start to a
 * gene string end where one mutation is defined as one single character
 * changed in the gene string.
 *
 *     ∙ For example, "AACCGGTT" --> "AACCGGTA" is one mutation.
 *
 * There is also a gene bank bank that records all the valid gene mutations. A
 * gene must be in bank to make it a valid gene string.
 *
 * Given the two gene strings start and end and the gene bank bank, return the
 * minimum number of mutations needed to mutate from start to end. If there is
 * no such a mutation, return -1.
 *
 * Note that the starting point is assumed to be valid, so it might not be
 * included in the bank.
 *
 * Example 1:
 * Input: start = "AACCGGTT", end = "AACCGGTA", bank = ["AACCGGTA"]
 * Output: 1
 *
 * Example 2:
 * Input: start = "AACCGGTT", end = "AAACGGTA", bank =
 * ["AACCGGTA","AACCGCTA","AAACGGTA"]
 * Output: 2
 *
 * Example 3:
 * Input: start = "AAAAACCC", end = "AACCCCCC", bank =
 * ["AAAACCCC","AAACCCCC","AACCCCCC"]
 * Output: 3
 *
 * Constraints:
 *     ∙ start.length == 8
 *     ∙ end.length == 8
 *     ∙ 0 <= bank.length <= 10
 *     ∙ bank[i].length == 8
 *     ∙ start, end, and bank[i] consist of only the characters
 *       ['A', 'C', 'G', 'T'].
"""
from typing import List

class Solution:
    def minMutation(self, start: str, end: str, bank: List[str]) -> int:
        bank = set(bank)
        bank.add(start)
        if end not in bank:
            return -1
        n = len(end)
        to_visit = [(end, 0)]
        for e, count in to_visit:
            if e == start:
                return count
            for i in range(n):
                for c in "ACGT":
                    if c != e[i]:
                        t = e[:i] + c + e[i+1:]
                        if t in bank:
                            to_visit.append((t, count + 1))
                            bank.remove(t)
        return -1


if __name__ == "__main__":
    tests = (
        ("AACCGGTT", "AACCGGTA", ["AACCGGTA"], 1),
        ("AACCGGTT", "AAACGGTA", ["AACCGGTA","AACCGCTA","AAACGGTA"], 2),
        ("AAAAACCC", "AACCCCCC", ["AAAACCCC","AAACCCCC","AACCCCCC"], 3),
        ("AACCTTGG", "AATTCCGG", ["AATTCCGG","AACCTGGG","AACCCCGG","AACCTACC"], -1),
        ("AACCGGTT", "AACCGGTA", [], -1),
        ("AACCGGTT", "AAACGGTA", ["AACCGATT","AACCGATA","AAACGATA","AAACGGTA"], 4),
    )
    for start, end, bank, expected in tests:
        print(Solution().minMutation(start, end, bank) == expected)
