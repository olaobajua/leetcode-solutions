/**
 * 515. Find Largest Value in Each Tree Row [Medium]
 * Given the root of a binary tree, return an array of the largest value in
 * each row of the tree (0-indexed).
 *
 * Example 1:
 *    1
 *  3   2
 * 5 3   9
 * Input: root = [1,3,2,5,3,null,9]
 * Output: [1,3,9]
 *
 * Example 2:
 *  1
 * 2 3
 * Input: root = [1,2,3]
 * Output: [1,3]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree will be in the range [0, 10⁴].
 *     ∙ -2³¹ <= Node.val <= 2³¹ - 1
 */
#include <vector>
#include <queue>
#include <limits> // For numeric_limits

using namespace std;

// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    vector<int> largestValues(TreeNode* root) {
        vector<int> ret;
        if (!root) return ret;

        vector<TreeNode*> level;
        level.push_back(root);
        while (!level.empty()) {
            vector<TreeNode*> next_level;
            int cur = numeric_limits<int>::min();
            for (auto node : level) {
                cur = max(cur, node->val);
                if (node->left) next_level.push_back(node->left);
                if (node->right) next_level.push_back(node->right);
            }
            level.clear();
            ret.push_back(cur);
            level = next_level;
        }
        return ret;
    }
};
