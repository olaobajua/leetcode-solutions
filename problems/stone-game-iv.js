/**
 * 1510. Stone Game IV (Hard)
 * Alice and Bob take turns playing a game, with Alice starting first.
 *
 * Initially, there are n stones in a pile. On each player's turn, that player
 * makes a move consisting of removing any non-zero square number of stones in
 * the pile.
 *
 * Also, if a player cannot make a move, he/she loses the game.
 *
 * Given a positive integer n, return true if and only if Alice wins the game
 * otherwise return false, assuming both players play optimally.
 *
 * Example 1:
 * Input: n = 1
 * Output: true
 * Explanation: Alice can remove 1 stone winning the game because Bob doesn't
 * have any moves.
 *
 * Example 2:
 * Input: n = 2
 * Output: false
 * Explanation: Alice can only remove 1 stone, after that Bob removes the last
 * one winning the game (2 -> 1 -> 0).
 *
 * Example 3:
 * Input: n = 4
 * Output: true
 * Explanation: n is already a perfect square, Alice can win with one move,
 * removing 4 stones (4 -> 0).
 *
 * Constraints:
 *     1 <= n <= 10⁵
 */

/**
 * @param {number} n
 * @return {boolean}
 */
var winnerSquareGame = function(n) {
    if (n > precalculated) {
        for (let i = 0; i <= n; ++i) {
            if (!winningPosition[i]) {
                // if i is loosing state then
                // any other states that can go to i is winning
                for (let j = 1; i + j*j <= n; ++j) {
                    winningPosition[i + j*j] = true;
                }
            }
        }
    }
    precalculated = n;
    return winningPosition[n];
};

const winningPosition = new Array(10**5).fill(false);
let precalculated = 0;

const tests = [
    [1, true],  // 1 -> 0
    [2, false], // 1 -> 1 -> 0
    [4, true],  // 4 -> 0
    [5, false], // 1 -> 4 -> 0; 4 -> 1 -> 0
    [8, true],  // 1 -> 4 -> 1 -> 1 -> 1 -> 0; 1 -> 1 -> 4 -> 1 -> 1 -> 0
    [47, true],
    [9520, true],
];

for (const [n, expected] of tests) {
    console.log(winnerSquareGame(n) == expected);
}
