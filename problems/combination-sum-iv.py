"""
 * 377. Combination Sum IV [Medium]
 * Given an array of distinct integers nums and a target integer target,
 * return the number of possible combinations that add up to target.
 *
 * The test cases are generated so that the answer can fit in a 32-bit
 * integer.
 *
 * Example 1:
 * Input: nums = [1,2,3], target = 4
 * Output: 7
 * Explanation:
 * The possible combination ways are:
 * (1, 1, 1, 1)
 * (1, 1, 2)
 * (1, 2, 1)
 * (1, 3)
 * (2, 1, 1)
 * (2, 2)
 * (3, 1)
 * Note that different sequences are counted as different combinations.
 *
 * Example 2:
 * Input: nums = [9], target = 3
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 200
 *     ∙ 1 <= nums[i] <= 1000
 *     ∙ All the elements of nums are unique.
 *     ∙ 1 <= target <= 1000
 *
 * Follow up: What if negative numbers are allowed in the given array? How
 * does it change the problem? What limitation we need to add to the question
 * to allow negative numbers?
"""
from functools import cache
from typing import List

class Solution:
    def combinationSum4(self, nums: List[int], target: int) -> int:
        @cache
        def dp(left):
            return left == 0 or sum(dp(left - x) for x in nums if left >= x)
        return dp(target)

if __name__ == "__main__":
    tests = (
        ([1,2,3], 4, 7),
        ([9], 3, 0),
        ([3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25], 10, 9),
    )
    for nums, target, expected in tests:
        print(Solution().combinationSum4(nums, target) == expected)
