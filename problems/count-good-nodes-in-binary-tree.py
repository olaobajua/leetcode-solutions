"""
 * 1448. Count Good Nodes in Binary Tree [Medium]
 * Given a binary tree root, a node X in the tree is named good if in the path
 * from root to X there are no nodes with a value greater than X.
 *
 * Return the number of good nodes in the binary tree.
 *
 * Example 1:
 * Input: root = [3,1,4,3,null,1,5]
 * Output: 4
 * Explanation: Nodes in blue are good.
 * Root Node (3) is always a good node.
 * Node 4 -> (3,4) is the maximum value in the path starting from the root.
 * Node 5 -> (3,4,5) is the maximum value in the path
 * Node 3 -> (3,1,3) is the maximum value in the path.
 *
 * Example 2:
 * Input: root = [3,3,null,4,2]
 * Output: 3
 * Explanation: Node 2 -> (3, 3, 2) is not good, because "3" is higher than
 * it.
 *
 * Example 3:
 * Input: root = [1]
 * Output: 1
 * Explanation: Root is considered as good.
 *
 * Constraints:
 *     ∙ The number of nodes in the binary tree is in the range [1, 10⁵].
 *     ∙ Each node's value is between [-10⁴, 10⁴].
"""
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def goodNodes(self, root: TreeNode) -> int:
        self.good_nodes = 0
        self.count_nodes(root, -10000)
        return self.good_nodes

    def count_nodes(self, root, max_val):
        if root:
            self.good_nodes += root.val >= max_val
            max_val = max(max_val, root.val)
            self.count_nodes(root.left, max_val)
            self.count_nodes(root.right, max_val)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([3,1,4,3,None,1,5], 4),
        ([3,3,None,4,2], 3),
        ([1], 1),
    )
    for nums, expected in tests:
        print(Solution().goodNodes(build_tree(nums)) == expected)
