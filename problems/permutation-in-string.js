/**
 * 567. Permutation in String (Medium)
 * Given two strings s1 and s2, return true if s2 contains a permutation of s1,
 * or false otherwise.
 *
 * In other words, return true if one of s1's permutations is the substring of
 * s2.
 *
 * Example 1:
 * Input: s1 = "ab", s2 = "eidbaooo"
 * Output: true
 * Explanation: s2 contains one permutation of s1 ("ba").
 *
 * Example 2:
 * Input: s1 = "ab", s2 = "eidboaoo"
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= s1.length, s2.length <= 10⁴
 *     ∙ s1 and s2 consist of lowercase English letters.
 */

/**
 * @param {string} s1
 * @param {string} s2
 * @return {boolean}
 */
var checkInclusion = function(s1, s2) {
    function cmp(c1, c2) { return c1.every((char, i) => char == c2[i]) }
    const n1 = s1.length;
    const n2 = s2.length;
    const c1 = Array(26).fill(0);
    const c2 = Array(26).fill(0);
    const a = 'a'.charCodeAt();
    [...s1].forEach(char => ++c1[char.charCodeAt()-a]);
    [...s2.slice(0, n1)].forEach(char => ++c2[char.charCodeAt()-a]);
    for (let i = n1; i < n2; ++i) {
        if (cmp(c1, c2)) {
            return true;
        }
        ++c2[s2.charCodeAt(i)-a];
        --c2[s2.charCodeAt(i-n1)-a];
    }
    return cmp(c1, c2);
};

const tests = [
    ["ab", "eidbaooo", true],
    ["ab", "eidboaoo", false],
    ["ab", "a", false],
];

for (const [s1, s2, expected] of tests) {
    console.log(checkInclusion(s1, s2) == expected);
}
