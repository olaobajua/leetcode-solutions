"""
 * 2360. Longest Cycle in a Graph [Hard]
 * You are given a directed graph of n nodes numbered from 0 to n - 1, where
 * each node has at most one outgoing edge.
 *
 * The graph is represented with a given 0-indexed array edges of size n,
 * indicating that there is a directed edge from node i to node edges[i]. If
 * there is no outgoing edge from node i, then edges[i] == -1.
 *
 * Return the length of the longest cycle in the graph. If no cycle exists,
 * return -1.
 *
 * A cycle is a path that starts and ends at the same node.
 *
 * Example 1:
 * Input: edges = [3,3,4,2,3]
 * Output: 3
 * Explanation: The longest cycle in the graph is the cycle: 2 -> 4 -> 3 -> 2.
 * The length of this cycle is 3, so 3 is returned.
 *
 * Example 2:
 * Input: edges = [2,-1,3,1]
 * Output: -1
 * Explanation: There are no cycles in this graph.
 *
 * Constraints:
 *     ∙ n == edges.length
 *     ∙ 2 <= n <= 10⁵
 *     ∙ -1 <= edges[i] < n
 *     ∙ edges[i] != i
"""
from typing import List

class Solution:
    def longestCycle(self, edges: List[int]) -> int:
        def dfs(node):
            next_node = edges[node]
            if next_node == -1:
                cycles[node] = -1
                return
            if cycles[next_node] > 0:
                cycles[node] = cycles[node] - cycles[next_node] + 1
                return
            cycles[next_node] = cycles[node] + 1
            dfs(next_node)
            cycles[node] = cycles[next_node]

        n = len(edges)
        cycles = [0] * n
        for i in range(n):
            if cycles[i] == 0:
                cycles[i] = 1
                dfs(i)
        return max(cycles)

if __name__ == "__main__":
    tests = (
        ([3,3,4,2,3], 3),
        ([2,-1,3,1], -1),
        ([3,4,0,2,-1,2], 3),
    )
    for edges, expected in tests:
        print(Solution().longestCycle(edges) == expected)
