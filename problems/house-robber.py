"""
 * 198. House Robber [Medium]
 * You are a professional robber planning to rob houses along a street. Each
 * house has a certain amount of money stashed, the only constraint stopping
 * you from robbing each of them is that adjacent houses have security systems
 * connected and it will automatically contact the police if two adjacent
 * houses were broken into on the same night.
 *
 * Given an integer array nums representing the amount of money of each house,
 * return the maximum amount of money you can rob tonight without alerting the
 * police.
 *
 * Example 1:
 * Input: nums = [1,2,3,1]
 * Output: 4
 * Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
 * Total amount you can rob = 1 + 3 = 4.
 *
 * Example 2:
 * Input: nums = [2,7,9,3,1]
 * Output: 12
 * Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house
 * 5 (money = 1).
 * Total amount you can rob = 2 + 9 + 1 = 12.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 100
 *     ∙ 0 <= nums[i] <= 400
"""
from functools import cache
from typing import List

class Solution:
    def rob(self, nums: List[int]) -> int:
        @cache
        def dp(i):
            return max(nums[i] + dp(i + 2), dp(i + 1)) if i < n else 0
        n = len(nums)
        return dp(0)

if __name__ == "__main__":
    tests = (
        ([1,2,3,1], 4),
        ([2,7,9,3,1], 12),
        ([2,7,9,3,1,9], 20),
        ([183,219,57,193,94,233,202,154,65,240,97,234,100,249,186,66,90,238,168,128,177,235,50,81,185,165,217,207,88,80,112,78,135,62,228,247,211], 3365),
    )
    for nums, expected in tests:
        print(Solution().rob(nums) == expected)
