"""
 * 587. Erect the Fence [Hard]
 * You are given an array trees where trees[i] = [xᵢ, yᵢ] represents the
 * location of a tree in the garden.
 *
 * You are asked to fence the entire garden using the minimum length of rope
 * as it is expensive. The garden is well fenced only if all the trees are
 * enclosed.
 *
 * Return the coordinates of trees that are exactly located on the fence
 * perimeter.
 *
 * Example 1:
 * Input: points = [[1,1],[2,2],[2,0],[2,4],[3,3],[4,2]]
 * Output: [[1,1],[2,0],[3,3],[2,4],[4,2]]
 *
 * Example 2:
 * Input: points = [[1,2],[2,2],[4,2]]
 * Output: [[4,2],[2,2],[1,2]]
 *
 * Constraints:
 *     ∙ 1 <= points.length <= 3000
 *     ∙ points[i].length == 2
 *     ∙ 0 <= xᵢ, yᵢ <= 100
 *     ∙ All the given points are unique.
"""
from typing import List

class Solution:
    def outerTrees(self, trees: List[List[int]]) -> List[List[int]]:
        def pos(point, vector):
            px1, py1 = point
            vx1, vy1, vx2, vy2 = vector
            pos = (px1 - vx2) * (vy1 - vy2) - (vx1 - vx2) * (py1 - vy2)
            return "on vector" if pos == 0 else "left" if pos > 0 else "right"

        trees = tuple(map(tuple, sorted(trees)))
        top = [trees[0]]
        bottom = [trees[0]]
        for tree in trees[1:]:
            while len(top) > 1 and pos(tree, (*top[-2], *top[-1])) == "left":
                top.pop()
            top.append(tree)
            while len(bottom) > 1 and pos(tree, (*bottom[-2], *bottom[-1])) == "right":
                bottom.pop()
            bottom.append(tree)
        return map(list, set(top) | set(bottom))

if __name__ == "__main__":
    tests = (
        ([[1,1],[2,2],[2,0],[2,4],[3,3],[4,2]], [[1,1],[2,0],[3,3],[2,4],[4,2]]),
        ([[1,2],[2,2],[4,2]], [[4,2],[2,2],[1,2]]),
    )
    for trees, expected in tests:
        print(set(map(tuple, Solution().outerTrees(trees))) ==
              set(map(tuple, expected)))
