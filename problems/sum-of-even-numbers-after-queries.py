"""
 * 985. Sum of Even Numbers After Queries [Medium]
 * You are given an integer array nums and an array queries where queries[i] =
 * [valᵢ, indexᵢ].
 *
 * For each query i, first, apply nums[indexᵢ] = nums[indexᵢ] + valᵢ, then
 * print the sum of the even values of nums.
 *
 * Return an integer array answer where answer[i] is the answer to the iᵗʰ
 * query.
 *
 * Example 1:
 * Input: nums = [1,2,3,4], queries = [[1,0],[-3,1],[-4,0],[2,3]]
 * Output: [8,6,2,4]
 * Explanation: At the beginning, the array is [1,2,3,4].
 * After adding 1 to nums[0], the array is [2,2,3,4], and the sum of even
 * values is 2 + 2 + 4 = 8.
 * After adding -3 to nums[1], the array is [2,-1,3,4], and the sum of even
 * values is 2 + 4 = 6.
 * After adding -4 to nums[0], the array is [-2,-1,3,4], and the sum of even
 * values is -2 + 4 = 2.
 * After adding 2 to nums[3], the array is [-2,-1,3,6], and the sum of even
 * values is -2 + 6 = 4.
 *
 * Example 2:
 * Input: nums = [1], queries = [[4,0]]
 * Output: [0]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁴
 *     ∙ -10⁴ <= nums[i] <= 10⁴
 *     ∙ 1 <= queries.length <= 10⁴
 *     ∙ -10⁴ <= valᵢ <= 10⁴
 *     ∙ 0 <= indexᵢ < nums.length
"""
from typing import List

class Solution:
    def sumEvenAfterQueries(self, nums: List[int], queries: List[List[int]]) -> List[int]:
        even_sum = sum(x for x in nums if x % 2 == 0)
        ret = []
        for val, index in queries:
            even_sum -= nums[index] if nums[index] % 2 == 0 else 0
            nums[index] += val
            even_sum += nums[index] if nums[index] % 2 == 0 else 0
            ret.append(even_sum)
        return ret

if __name__ == "__main__":
    tests = (
        ([1,2,3,4], [[1,0],[-3,1],[-4,0],[2,3]], [8,6,2,4]),
        ([1], [[4,0]], [0]),
    )
    for nums, queries, expected in tests:
        print(Solution().sumEvenAfterQueries(nums, queries) == expected)
