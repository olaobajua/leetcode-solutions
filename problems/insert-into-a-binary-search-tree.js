/**
 * 701. Insert into a Binary Search Tree (Medium)
 * You are given the root node of a binary search tree (BST) and a value to
 * insert into the tree. Return the root node of the BST after the insertion.
 * It is guaranteed that the new value does not exist in the original BST.
 *
 * Notice that there may exist multiple valid ways for the insertion, as long
 * as the tree remains a BST after insertion. You can return any of them.
 *
 * Example 1:
 *     4              4
 *  2     7         2     7
 * 1 3             1 3   5
 * Input: root = [4,2,7,1,3], val = 5
 * Output: [4,2,7,1,3,5]
 * Explanation: Another accepted tree is:
 *     5
 *  2     7
 * 1 3
 *    4
 *
 * Example 2:
 * Input: root = [40,20,60,10,30,50,70], val = 25
 * Output: [40,20,60,10,30,50,70,null,null,25]
 *
 * Example 3:
 * Input: root = [4,2,7,1,3,null,null,null,null,null,null], val = 5
 * Output: [4,2,7,1,3,5]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree will be in the range [0, 10⁴].
 *     ∙ -10⁸ <= Node.val <= 10⁸
 *     ∙ All the values Node.val are unique.
 *     ∙ -10⁸ <= val <= 10⁸
 *     ∙ It's guaranteed that val does not exist in the original BST.
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @param {number} val
 * @return {TreeNode}
 */
var insertIntoBST = function(root, val) {
    return addNode(root, new TreeNode(val));
};

function addNode(root, child) {
    if (root === null) {
        return child;
    } else if (child.val < root.val) {
        root.left = addNode(root.left, child);
    } else {
        root.right = addNode(root.right, child);
    }
    return root;
}

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

function* treeToList(root) {
    let nodes = [root];
    for (let n of nodes) {
        yield n ? n.val : null;
        if (n) {
            nodes.push(n.left, n.right);
        }
    }
}

const tests = [
    [[4,2,7,1,3], 5, [4,2,7,1,3,5]],
    [[40,20,60,10,30,50,70], 25, [40,20,60,10,30,50,70,null,null,25]],
    [[4,2,7,1,3,null,null,null,null,null,null], 5, [4,2,7,1,3,5]],
];

for (const [nums, val, expected] of tests) {
    while (nums.length && nums[nums.length-1] === null) {
        nums.pop();
    }
    const tree = [...treeToList(insertIntoBST(buildTree(nums), val))]
    while (tree.length && tree[tree.length-1] === null) {
        tree.pop();
    }
    console.log(tree.toString() == expected.toString());
}
