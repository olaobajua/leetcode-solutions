"""
 * 132. Palindrome Partitioning II (Hard)
 *
 * Given a string s, partition s such that every substring of the partition is a palindrome.
 *
 * Return the minimum cuts needed for a palindrome partitioning of s.
 *
 * Example 1:
 * Input: s = "aab"
 * Output: 1
 * Explanation: The palindrome partitioning ["aa","b"] could be produced using 1 cut.
 *
 * Example 2:
 * Input: s = "a"
 * Output: 0
 *
 * Example 3:
 * Input: s = "ab"
 * Output: 1
 *
 * Constraints:
 *     1 <= s.length <= 2000
 *     s consists of lower-case English letters only.
"""
from functools import cache
import sys

def is_palindrome(s):
    return s == s[::-1]

@cache
def min_cut(s, start=0):
    if start < len(s):
        count = len(s)
        for i in range(start + 1, len(s) + 1):
            if is_palindrome(s[start:i]):
                count = min(count, min_cut(s, i) + 1)
        return count
    else:
        return 0

class Solution:
    def minCut(self, s: str) -> int:
        return min_cut(s) - 1

if __name__ == "__main__":
    sys.setrecursionlimit(1500)
    tests = (
        ("aab", 1),
        ("a", 0),
        ("ab", 1),
        ("aaabaa", 1),
        ("ababbbabbaba", 3),
        ("abaaaaabba", 2),  # a:baaaaab:b:a, aba:aaa:abba
        ("apjesgpsxoeiokmqmfgvjslcjukbqxpsobyhjpbgdfruqdkeiszrlmtwgfxyfostpqczidfljwfbbrflkgdvtytbgqalguewnhvvmcgxboycffopmtmhtfizxkmeftcucxpobxmelmjtuzigsxnncxpaibgpuijwhankxbplpyejxmrrjgeoevqozwdtgospohznkoyzocjlracchjqnggbfeebmuvbicbvmpuleywrpzwsihivnrwtxcukwplgtobhgxukwrdlszfaiqxwjvrgxnsveedxseeyeykarqnjrtlaliyudpacctzizcftjlunlgnfwcqqxcqikocqffsjyurzwysfjmswvhbrmshjuzsgpwyubtfbnwajuvrfhlccvfwhxfqthkcwhatktymgxostjlztwdxritygbrbibdgkezvzajizxasjnrcjwzdfvdnwwqeyumkamhzoqhnqjfzwzbixclcxqrtniznemxeahfozp", 452),
    )
    for s, exp in tests:
        r = Solution().minCut(s)
        print(r == exp, f"{r} {'==' if r == exp else '!='} {exp}")
