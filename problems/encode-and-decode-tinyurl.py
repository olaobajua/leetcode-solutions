"""
 * 535. Encode and Decode TinyURL (Medium)
 * Note: This is a companion problem to the System Design problem:
 * Design TinyURL.
 *
 * TinyURL is a URL shortening service where you enter a URL such as
 * https://leetcode.com/problems/design-tinyurl and it returns a short URL such
 * as http://tinyurl.com/4e9iAk. Design a class to encode a URL and decode
 * a tiny URL.
 *
 * There is no restriction on how your encode/decode algorithm should work.
 * You just need to ensure that a URL can be encoded to a tiny URL and the tiny
 * URL can be decoded to the original URL.
 *
 * Implement the Solution class:
 *     ∙ Solution() Initializes the object of the system
 *     ∙ String encode(String longUrl) Returns a tiny URL for the given longUrl
 *     ∙ String decode(String shortUrl) Returns the original long URL
 *       for the given shortUrl. It is guaranteed that the given shortUrl
 *       was encoded by the same object.
 *
 * Example 1:
 * Input: url = "https://leetcode.com/problems/design-tinyurl"
 * Output: "https://leetcode.com/problems/design-tinyurl"
 *
 * Explanation:
 * Solution obj = new Solution();
 * string tiny = obj.encode(url); // returns the encoded tiny url.
 * string ans = obj.decode(tiny); // returns the original url after deconding
 * it.
 *
 * Constraints:
 *     ∙ 1 <= url.length <= 10⁴
 *     ∙ url is guranteed to be a valid URL.
"""
from random import choice
from string import ascii_letters

class Codec:
    chars = ascii_letters + '0123456789'
    base = "http://tinyurl.com/"

    def __init__(self, n=6):
        self.url2code = {}
        self.code2url = {}
        self.__len = n

    def encode(self, longUrl: str) -> str:
        """Encodes a URL to a shortened URL.
        """
        while longUrl not in self.url2code:
            code = ''.join(choice(self.chars) for _ in range(self.__len))
            if code not in self.code2url:
                self.code2url[code] = longUrl
                self.url2code[longUrl] = code
        return self.base + self.url2code[longUrl]

    def decode(self, shortUrl: str) -> str:
        """Decodes a shortened URL to its original URL.
        """
        return self.code2url[shortUrl[-self.__len:]]

# Your Codec object will be instantiated and called as such:
codec = Codec()
url = "https://leetcode.com/problems/design-tinyurl"
print(url == codec.decode(codec.encode(url)))
