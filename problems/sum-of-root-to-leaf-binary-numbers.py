"""
 * 1022. Sum of Root To Leaf Binary Numbers (Easy)
 * You are given the root of a binary tree where each node has a value 0 or 1.
 * Each root-to-leaf path represents a binary number starting with the most
 * significant bit.
 *     ∙ For example, if the path is 0 -> 1 -> 1 -> 0 -> 1, then this could
 *       represent 01101 in binary, which is 13.
 *
 * For all leaves in the tree, consider the numbers represented by the path
 * from the root to that leaf. Return the sum of these numbers.
 *
 * The test cases are generated so that the answer fits in a 32-bits integer.
 *
 * Example 1:
 * Input: root = [1,0,1,0,1,0,1]
 * Output: 22
 * Explanation: (100) + (101) + (110) + (111) = 4 + 5 + 6 + 7 = 22
 *
 * Example 2:
 * Input: root = [0]
 * Output: 0
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 1000].
 *     ∙ Node.val is 0 or 1.
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def sumRootToLeaf(self, root: Optional[TreeNode]) -> int:
        def dfs(root, prev=0):
            if root:
                prev = (prev << 1) + root.val
                dfs(root.left, prev)
                dfs(root.right, prev)
                if not root.left and not root.right:
                    nonlocal ret
                    ret += prev
        ret = 0
        dfs(root)
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,0,1,0,1,0,1], 22),
        ([0], 0),
    )
    for nums, expected in tests:
        print(Solution().sumRootToLeaf(build_tree(nums)) == expected)
