/**
 * 374. Guess Number Higher or Lower (Easy)
 * We are playing the Guess Game. The game is as follows:
 *
 * I pick a number from 1 to n. You have to guess which number I picked.
 *
 * Every time you guess wrong, I will tell you whether the number I picked is
 * higher or lower than your guess.
 *
 * You call a pre-defined API int guess(int num), which returns 3 possible
 * results:
 *     -1: The number I picked is lower than your guess (i.e. pick < num).
 *     1: The number I picked is higher than your guess (i.e. pick > num).
 *     0: The number I picked is equal to your guess (i.e. pick == num).
 *
 * Return the number that I picked.
 *
 * Example 1:
 * Input: n = 10, pick = 6
 * Output: 6
 *
 * Example 2:
 * Input: n = 1, pick = 1
 * Output: 1
 *
 * Example 3:
 * Input: n = 2, pick = 1
 * Output: 1
 *
 * Example 4:
 * Input: n = 2, pick = 2
 * Output: 2
 *
 * Constraints:
 *     1 <= n <= 2³¹ - 1
 *     1 <= pick <= n
 */

/** 
 * Forward declaration of guess API.
 * @param {number} num   your guess
 * @return 	            -1 if num is lower than the guess number
 *			             1 if num is higher than the guess number
 *                       otherwise return 0
 * var guess = function(num) {}
 */

/**
 * @param {number} n
 * @return {number}
 */
var guessNumber = function(n) {
    let cmp, num, start = 1, end = n;
    while (start <= end) {
        num = Math.floor((end + start) / 2);
        cmp = guess(num);
        if (cmp == 0) {
            return num;
        }
        if (cmp < 0) {
            end = num - 1;
        } else {
            start = num + 1;
        }
    }
};

tests = [
    [10, 6],
    [10, 4],
    [1, 1],
    [2, 1],
    [2, 2],
];

for (let [n, expected] of tests) {
    var guess = function(num) { return expected - num; }
    console.log(guessNumber(n) == expected);
}
