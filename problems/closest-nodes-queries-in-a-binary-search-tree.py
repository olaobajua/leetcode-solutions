"""
 * 2476. Closest Nodes Queries in a Binary Search Tree [Medium]
 * You are given the root of a binary search tree and an array queries of size
 * n consisting of positive integers.
 *
 * Find a 2D array answer of size n where answer[i] = [minᵢ, maxᵢ]:
 *     ∙ minᵢ is the largest value in the tree that is smaller than or equal
 *       to queries[i]. If a such value does not exist, add -1 instead.
 *     ∙ maxᵢ is the smallest value in the tree that is greater than or equal
 *       to queries[i]. If a such value does not exist, add -1 instead.
 *
 * Return the array answer.
 *
 * Example 1:
 * Input: root = [6,2,13,1,4,9,15,null,null,null,null,null,null,14], queries =
 * [2,5,16]
 * Output: [[2,2],[4,6],[15,-1]]
 * Explanation: We answer the queries in the following way:
 * - The largest number that is smaller or equal than 2 in the tree is 2, and
 * the smallest number that is greater or equal than 2 is still 2. So the
 * answer for the first query is [2,2].
 * - The largest number that is smaller or equal than 5 in the tree is 4, and
 * the smallest number that is greater or equal than 5 is 6. So the answer for
 * the second query is [4,6].
 * - The largest number that is smaller or equal than 16 in the tree is 15,
 * and the smallest number that is greater or equal than 16 does not exist. So
 * the answer for the third query is [15,-1].
 *
 * Example 2:
 * Input: root = [4,null,9], queries = [3]
 * Output: [[-1,4]]
 * Explanation: The largest number that is smaller or equal to 3 in the tree
 * does not exist, and the smallest number that is greater or equal to 3 is 4.
 * So the answer for the query is [-1,4].
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [2, 10⁵].
 *     ∙ 1 <= Node.val <= 10⁶
 *     ∙ n == queries.length
 *     ∙ 1 <= n <= 10⁵
 *     ∙ 1 <= queries[i] <= 10⁶
"""
from bisect import bisect_left
from functools import cache
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def closestNodes(self, root: Optional[TreeNode], queries: List[int]) -> List[List[int]]:
        def traverse_inorder(root):
            if root:
                traverse_inorder(root.left)
                nums.append(root.val)
                traverse_inorder(root.right)

        @cache
        def query(x):
            i = bisect_left(nums, x) % n
            return [nums[i-(nums[i] != x)], nums[i]]

        nums = [-1]
        traverse_inorder(root)
        n = len(nums)
        return [query(x) for x in queries]

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([6,2,13,1,4,9,15,None,None,None,None,None,None,14], [2,5,16], [[2,2],[4,6],[15,-1]]),
        ([4,None,9], [3], [[-1,4]]),
        ([9,6,14,None,None,13,20,12], [19,10,9,17,19,6,10,19,13,6],
         [[14,20],[9,12],[9,9],[14,20],[14,20],[6,6],[9,12],[14,20],[13,13],[6,6]])
    )
    for nums, queries, expected in tests:
        print(Solution().closestNodes(build_tree(nums), queries) == expected)
