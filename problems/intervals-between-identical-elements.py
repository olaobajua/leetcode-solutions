"""
 * 2121. Intervals Between Identical Elements (Medium)
 * You are given a 0-indexed array of n integers arr.
 *
 * The interval between two elements in arr is defined as the absolute
 * difference between their indices. More formally, the interval between arr[i]
 * and arr[j] is |i - j|.
 *
 * Return an array intervals of length n where intervals[i] is the sum of
 * intervals between arr[i] and each element in arr with the same value as
 * arr[i].
 *
 * Note: |x| is the absolute value of x.
 *
 * Example 1:
 * Input: arr = [2,1,3,1,2,3,3]
 * Output: [4,2,7,2,4,4,5]
 * Explanation:
 * - Index 0: Another 2 is found at index 4. |0 - 4| = 4
 * - Index 1: Another 1 is found at index 3. |1 - 3| = 2
 * - Index 2: Two more 3s are found at indices 5 and 6. |2 - 5| + |2 - 6| = 7
 * - Index 3: Another 1 is found at index 1. |3 - 1| = 2
 * - Index 4: Another 2 is found at index 0. |4 - 0| = 4
 * - Index 5: Two more 3s are found at indices 2 and 6. |5 - 2| + |5 - 6| = 4
 * - Index 6: Two more 3s are found at indices 2 and 5. |6 - 2| + |6 - 5| = 5
 *
 * Example 2:
 * Input: arr = [10,5,10,10]
 * Output: [5,0,3,4]
 * Explanation:
 * - Index 0: Two more 10s are found at indices 2 and 3. |0 - 2| + |0 - 3| = 5
 * - Index 1: There is only one 5 in the array, so its sum of intervals to
 *            identical elements is 0.
 * - Index 2: Two more 10s are found at indices 0 and 3. |2 - 0| + |2 - 3| = 3
 * - Index 3: Two more 10s are found at indices 0 and 2. |3 - 0| + |3 - 2| = 4
 *
 * Constraints:
 *     n == arr.length
 *     1 <= n <= 10⁵
 *     1 <= arr[i] <= 10⁵
"""
from collections import defaultdict
from itertools import accumulate
from typing import List

class Solution:
    def getDistances(self, arr: List[int]) -> List[int]:
        indices = defaultdict(list)
        for i, x in enumerate(arr):
            indices[x].append(i)
        ret = [0] * len(arr)
        for idxs in indices.values():
            prefix = list(accumulate(idxs, initial=0))
            for i, x in enumerate(idxs):
                # If for the list [1, 3, 5, 7, 9, 11] i = 3 and idxs[i] = 7,
                # then abs difference for numbers smaller than equal to 7 is
                # 7 - 1 + 7 - 3 + 7 - 5 + 7 - 7 = 7 * 4 - (1 + 3 + 5 + 7) =
                #                                 x*(i + 1) - prefix[i+1]
                # Similarly abs difference for numbers greater than idxs[i] is:
                # 7 - 7 + 9 - 7 + 11 - 7 = (7 + 9 + 11) - 7 * 3
                # (prefix[len(idxs)] - prefix[i]) - x*(len(idxs) - i)
                left_sum = x*(i + 1) - prefix[i+1]
                right_sum = (prefix[len(idxs)] - prefix[i]) - x*(len(idxs) - i)
                ret[x] = left_sum + right_sum
        return ret

if __name__ == "__main__":
    tests = (
        ([2,1,3,1,2,3,3], [4,2,7,2,4,4,5]),
        ([10,5,10,10], [5,0,3,4]),
    )
    for arr, expected in tests:
        print(Solution().getDistances(arr) == expected)
