"""
 * 1977. Number of Ways to Separate Numbers (Hard)
 * You wrote down many positive integers in a string called num. However, you
 * realized that you forgot to add commas to seperate the different numbers.
 * You remember that the list of integers was non-decreasing and that no
 * integer had leading zeros.
 *
 * Return the number of possible lists of integers that you could have written
 * down to get the string num. Since the answer may be large, return it modulo
 * 10⁹ + 7.
 *
 * Example 1:
 * Input: num = "327"
 * Output: 2
 * Explanation: You could have written down the numbers:
 * 3, 27
 * 327
 *
 * Example 2:
 * Input: num = "094"
 * Output: 0
 * Explanation: No numbers can have leading zeros and all numbers must be
 * positive.
 *
 * Example 3:
 * Input: num = "0"
 * Output: 0
 * Explanation: No numbers can have leading zeros and all numbers must be
 * positive.
 *
 * Example 4:
 * Input: num = "9999999999999"
 * Output: 101
 *
 * Constraints:
 *     1 <= num.length <= 3500
 *     num consists of digits '0' through '9'.
"""
class Solution:
    def numberOfCombinations(self, num: str) -> int:
        n = len(num)
        comboes = [[0] * (n + 1) for _ in range(n)]
        for i in range(n):
            comboes[i][n] = num[i] != '0'

        total_comboes = comboes[0][n]
        for m in reversed(range(n)):  # middle boundary between last and prev
            e = n  # last num end
            counter = 0
            for s in range(m):  # prev num start
                if num[s] != '0':
                    while e-m > m-s or (e-m == m-s and (num[s:m] <= num[m:e])):
                        counter += comboes[m][e]
                        e -= 1
                    comboes[s][m] = counter
            total_comboes += comboes[0][m]
        return total_comboes % (10**9 + 7)

if __name__ == "__main__":
    tests = (
        ("12", 2),  # 1 2, 12
        ("327", 2),  # 327, 3 27
        ("094", 0),
        ("0", 0),
        ("12345", 7),  # 12345, 1 2345, 12 345, 1 2 345, 1 23 45, 1 2 3 45, 1 2 3 4 5
        ("54321", 3),  # 54321, 5 4321, 54 321
        ("9999999999999", 101),
        ("2423", 3),  # 2423, 2 423, 2 4 23
        ("57366096569998808177038860868034764472649082771812982665702793714521117518689268602592222064474212309407778097339776719903849830220", 14022729),
    )
    import sys
    sys.setrecursionlimit(10**5)
    from time import time
    start = time()
    for num, expected in tests:
        print(Solution().numberOfCombinations(num) == expected)
    print(f"Time elapesd: {time() - start} sec")
