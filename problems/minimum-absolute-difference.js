/**
 * 1200. Minimum Absolute Difference (Easy)
 * Given an array of distinct integers arr, find all pairs of elements with the
 * minimum absolute difference of any two elements.
 *
 * Return a list of pairs in ascending order(with respect to pairs), each pair
 * [a, b] follows
 *     ∙ a, b are from arr
 *     ∙ a < b
 *     ∙ b - a equals to the minimum absolute difference of any two elements in
 *       arr
 *
 * Example 1:
 * Input: arr = [4,2,1,3]
 * Output: [[1,2],[2,3],[3,4]]
 * Explanation: The minimum absolute difference is 1. List all pairs with
 * difference equal to 1 in ascending order.
 *
 * Example 2:
 * Input: arr = [1,3,6,10,15]
 * Output: [[1,3]]
 *
 * Example 3:
 * Input: arr = [3,8,-10,23,19,-4,-14,27]
 * Output: [[-14,-10],[19,23],[23,27]]
 *
 * Constraints:
 *     2 <= arr.length <= 10⁵
 *     -10⁶ <= arr[i] <= 10⁶
 */

/**
 * @param {number[]} arr
 * @return {number[][]}
 */
var minimumAbsDifference = function(arr) {
    arr.sort((a, b) => a - b);
    const minDif = Math.min(...arr.slice(1).map((x, i) => [x - arr[i]]));
    return arr.slice(1).map((x, i) => x - arr[i] == minDif && [arr[i], x])
                       .filter((x) => x);
};

const tests = [
    [[4,2,1,3], [[1,2],[2,3],[3,4]]],
    [[1,3,6,10,15], [[1,3]]],
    [[3,8,-10,23,19,-4,-14,27], [[-14,-10],[19,23],[23,27]]],
];

for (let [arr, expected] of tests) {
    expected = expected.map(pair => pair.toString());
    test: {
        for (const pair of minimumAbsDifference(arr)) {
            const i = expected.indexOf(pair.toString());
            if (i > -1) {
                expected.splice(i, 1);
            } else {
                console.log(false);
                break test;
            }
        }
        console.log(expected.length == 0);
    }
}
