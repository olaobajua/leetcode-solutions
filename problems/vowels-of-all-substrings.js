/**
 * 2063. Vowels of All Substrings (Medium)
 * Given a string word, return the sum of the number of vowels
 * ('a', 'e', 'i', 'o', and 'u') in every substring of word.
 *
 * A substring is a contiguous (non-empty) sequence of characters within a
 * string.
 *
 * Note: Due to the large constraints, the answer may not fit in a signed
 * 32-bit integer. Please be careful during the calculations.
 *
 * Example 1:
 * Input: word = "aba"
 * Output: 6
 * Explanation:
 * All possible substrings are: "a", "ab", "aba", "b", "ba", and "a".
 * - "b" has 0 vowels in it
 * - "a", "ab", "ba", and "a" have 1 vowel each
 * - "aba" has 2 vowels in it
 * Hence, the total sum of vowels = 0 + 1 + 1 + 1 + 1 + 2 = 6.
 *
 * Example 2:
 * Input: word = "abc"
 * Output: 3
 * Explanation:
 * All possible substrings are: "a", "ab", "abc", "b", "bc", and "c".
 * - "a", "ab", and "abc" have 1 vowel each
 * - "b", "bc", and "c" have 0 vowels each
 * Hence, the total sum of vowels = 1 + 1 + 1 + 0 + 0 + 0 = 3.
 *
 * Example 3:
 * Input: word = "ltcd"
 * Output: 0
 * Explanation: There are no vowels in any substring of "ltcd".
 *
 * Example 4:
 * Input: word = "noosabasboosa"
 * Output: 237
 * Explanation: There are a total of 237 vowels in all the substrings.
 *
 * Constraints:
 *     1 <= word.length <= 10⁵
 *     word consists of lowercase English letters.
 */

/**
 * @param {string} word
 * @return {number}
 */
var countVowels = function(word) {
    const VOWELS = new Set("aeiou");
    let counter = 0, prev = 0;
    Array.from(word).forEach((l, i) => {
        prev += VOWELS.has(l) * (i + 1);
        counter += prev;
    });
    return counter;
};

const tests = [
    ["aba", 6],    // a /1/ b, ab /1/ a, ba, aba /4/
    ["aab", 7],    // a /1/ a, aa /3/ b, ab, aab /3/
    ["baa", 7],    // b /0/ a, ba /2/ a, aa, baa /5/
    ["abc", 3],    // a /1/ b, ab /1/ c, bc, abc /1/
    ["aaa", 10],   // a /1/ a, aa /3/ a, aa, aaa /6/
    ["aaaa", 20],  // a /1/ a, aa /3/ a, aa, aaa /6/ a, aa, aaa, aaaa /10/
    ["aaab", 16],  // a /1/ a, aa /3/ a, aa, aaa /6/ b, ab, aab, aaab /6/
    ["ltcd", 0],
    ["noosabasboosa", 237],
]

for (let [word, expected] of tests) {
    console.log(countVowels(word) == expected);
}
