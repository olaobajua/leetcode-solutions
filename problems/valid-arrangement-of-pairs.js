/**
 * 2097. Valid Arrangement of Pairs (Hard)
 * You are given a 0-indexed 2D integer array pairs where
 * pairs[i] = [startᵢ, endᵢ]. An arrangement of pairs is valid if for every
 * index i where 1 <= i < pairs.length, we have endᵢ-1 == startᵢ.
 *
 * Return any valid arrangement of pairs.
 *
 * Note: The inputs will be generated such that there exists a valid
 * arrangement of pairs.
 *
 * Example 1:
 * Input: pairs = [[5,1],[4,5],[11,9],[9,4]]
 * Output: [[11,9],[9,4],[4,5],[5,1]]
 * Explanation:
 * This is a valid arrangement since endᵢ-1 always equals startᵢ.
 * end0 = 9 == 9 = start1
 * end1 = 4 == 4 = start2
 * end2 = 5 == 5 = start3
 *
 * Example 2:
 * Input: pairs = [[1,3],[3,2],[2,1]]
 * Output: [[1,3],[3,2],[2,1]]
 * Explanation:
 * This is a valid arrangement since endᵢ-1 always equals startᵢ.
 * end0 = 3 == 3 = start1
 * end1 = 2 == 2 = start2
 * The arrangements [[2,1],[1,3],[3,2]] and [[3,2],[2,1],[1,3]] are also valid.
 *
 * Example 3:
 * Input: pairs = [[1,2],[1,3],[2,1]]
 * Output: [[1,2],[2,1],[1,3]]
 * Explanation:
 * This is a valid arrangement since endᵢ-1 always equals startᵢ.
 * end0 = 2 == 2 = start1
 * end1 = 1 == 1 = start2
 *
 * Constraints:
 *     1 <= pairs.length <= 10⁵
 *     pairs[i].length == 2
 *     0 <= startᵢ, endᵢ <= 10⁹
 *     startᵢ != endᵢ
 *     No two pairs are exactly the same.
 *     There exists a valid arrangement of pairs.
 */

/**
 * @param {number[][]} pairs
 * @return {number[][]}
 */
var validArrangement = function(pairs) {
    const graph = {};
    const outDegree = {};
    for (const [x, y] of pairs) {
        graph[x] ? graph[x].push(y) : graph[x] = [y];
        outDegree[x] = (outDegree[x] || 0) + 1;
        outDegree[y] = (outDegree[y] || 0) - 1;
    }

    let start;
    for (const [node, degree] of Object.entries(outDegree)) {
        start = parseInt(node);
        if (degree == 1) {
            break;
        }
    }

    const path = [];
    hierholzerDfs(graph, start, path);
    path.reverse();
    return path.slice(1).map((n, i) => [path[i], n]);
};

function hierholzerDfs(graph, cur, path) {
    /* Find an Eulerian path of the graph. */
    while (graph[cur] && graph[cur].length) {
        hierholzerDfs(graph, graph[cur].pop(), path);
    }
    path.push(cur);
}

const tests = [
    [[5,1],[4,5],[11,9],[9,4]],
    [[1,3],[3,2],[2,1]],
    [[1,2],[1,3],[2,1]],
    [[17,18],[18,10],[10,18]],
    [[8,5],[8,7],[0,8],[0,5],[7,0],[5,0],[0,7],[8,0],[7,8]],
    [[5,3],[2,3],[0,1],[1,4],[0,5],[3,2],[4,3],[3,0]],
]
for (let pairs of tests) {
    const path = validArrangement(pairs);
    pairs = pairs.map(p => p.toString());
    test: {
        for (const pair of path) {
            const i = pairs.indexOf(pair.toString());
            if (i > -1) {
                pairs.splice(i, 1);
            } else {
                break test;
            }
        }
        console.log(pairs.length == 0 &&
                    path.slice(1).every((n, i) => path[i][1] == n[0]));
    }
}
