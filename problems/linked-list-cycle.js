/**
 * 141. Linked List Cycle (Easy)
 * Given head, the head of a linked list, determine if the linked list has a
 * cycle in it.
 *
 * There is a cycle in a linked list if there is some node in the list that can
 * be reached again by continuously following the next pointer. Internally, pos
 * is used to denote the index of the node that tail's next pointer is
 * connected to. Note that pos is not passed as a parameter.
 *
 * Return true if there is a cycle in the linked list. Otherwise, return false.
 *
 * Example 1:
 * Input: head = [3,2,0,-4], pos = 1
 * Output: true
 * Explanation: There is a cycle in the linked list, where the tail connects to
 * the 1ˢᵗ node (0-indexed).
 *
 * Example 2:
 * Input: head = [1,2], pos = 0
 * Output: true
 * Explanation: There is a cycle in the linked list, where the tail connects to
 * the 0ᵗʰ node.
 *
 * Example 3:
 * Input: head = [1], pos = -1
 * Output: false
 * Explanation: There is no cycle in the linked list.
 *
 * Constraints:
 *     ∙ The number of the nodes in the list is in the range [0, 10⁴].
 *     ∙ -10⁵ <= Node.val <= 10⁵
 *     ∙ pos is -1 or a valid index in the linked-list.
 *
 * Follow up: Can you solve it using O(1) (i.e. constant) memory?
 */


// Definition for singly-linked list.
function ListNode(val, next=null) {
    this.val = val;
    this.next = next;
}

/**
 * @param {ListNode} head
 * @return {boolean}
 */
var hasCycle = function(head) {
    let end = head;
    while (end && end.next) {
        end = end.next.next;
        head = head.next;
        if (end == head) {
            return true;
        }
    }
    return false;
};

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    [[3,2,0,-4], 1, true],
    [[1,2], 0, true],
    [[1], -1, false],
];

for (const [nums, pos, expected] of tests) {
    const root = arrayToLinkedList(nums);
    let end = root, mid = root;
    while (end && end.next) {
        end = end.next;
    }
    if (pos >= 0) {
        for (let i = 0; i < pos; ++i) {
            mid = mid.next;
        }
        end.next = mid;
    }
    console.log(hasCycle(root) == expected);
}
