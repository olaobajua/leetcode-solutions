/**
 * 2054. Two Best Non-Overlapping Events (Medium)
 * You are given a 0-indexed 2D integer array of events where
 * events[i] = [startTimeᵢ, endTimeᵢ, valueᵢ]. The ith event starts at
 * startTimeᵢ and ends at endTimeᵢ, and if you attend this event, you will
 * receive a value of valueᵢ. You can choose at most two non-overlapping events
 * to attend such that the sum of their values is maximized.
 *
 * Return this maximum sum.
 *
 * Note that the start time and end time is inclusive: that is, you cannot
 * attend two events where one of them starts and the other ends at the same
 * time. More specifically, if you attend an event with end time t, the next
 * event must start at or after t + 1.
 *
 * Example 1:
 * Input: events = [[1,3,2],[4,5,2],[2,4,3]]
 * Output: 4
 * Explanation: Choose the green events, 0 and 1 for a sum of 2 + 2 = 4.
 *
 * Example 2:
 * Input: events = [[1,3,2],[4,5,2],[1,5,5]]
 * Output: 5
 * Explanation: Choose event 2 for a sum of 5.
 *
 * Example 3:
 * Input: events = [[1,5,3],[1,5,1],[6,6,5]]
 * Output: 8
 * Explanation: Choose events 0 and 2 for a sum of 3 + 5 = 8.
 *
 * Constraints:
 *     2 <= events.length <= 10⁵
 *     events[i].length == 3
 *     1 <= startTimeᵢ <= endTimeᵢ <= 10⁹
 *     1 <= valueᵢ <= 10⁶
 */

/**
 * @param {number[][]} events
 * @return {number}
 */
var maxTwoEvents = function(events) {
    let points = events.map(([start, end, _]) => [start, end]).flat()
    points = [...new Set(points).values()].sort((a, b) => a - b);
    const n = points.length;
    let maxBefore = Array(n + 1).fill(0);
    let maxAfter = Array(n + 1).fill(0);
    let sortedByEnd = events.map(a => a.slice()).sort((a, b) => b[1] - a[1]);
    let sortedByStart = events.map(a => a.slice()).sort((a, b) => a[0] - b[0]);
    let el = sortedByEnd.length - 1;  // pop works much faster than shift
    let sl = sortedByStart.length - 1;
    for (let i = 0; i < n; ++i) {
        maxBefore[i+1] = maxBefore[i];
        while (el >= 0 && sortedByEnd[el][1] <= points[i]) {
            const value = sortedByEnd.pop()[2];
            --el;
            maxBefore[i+1] = Math.max(maxBefore[i+1], value);
        }
        maxAfter[i+1] = maxAfter[i];
        while (sl >= 0 && sortedByStart[sl][0] >= points[n-i-1]) {
            const value = sortedByStart.pop()[2];
            --sl;
            maxAfter[i+1] = Math.max(maxAfter[i+1], value);
        }
    }
    maxAfter = maxAfter.reverse();
    return Math.max(...maxBefore.map((b, i) => maxBefore[i] + maxAfter[i]));
};

const tests = [
    [[[1,3,2],[4,5,2],[2,4,3]], 4],
    [[[1,3,2],[4,5,2],[1,5,5]], 5],
    [[[1,5,3],[1,5,1],[6,6,5]], 8],
    [[[66,97,90],[98,98,68],[38,49,63],[91,100,42],[92,100,22],[1,77,50],[64,72,97]], 165],
    [[[1,5,5],[1,3,2],[4,5,2]], 5],
];

for (let [events, expected] of tests) {
    console.log(maxTwoEvents(events) == expected);
}
