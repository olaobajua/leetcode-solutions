"""
 * 363. Max Sum of Rectangle No Larger Than K [Hard]
 * Given an m x n matrix matrix and an integer k, return the max sum of a
 * rectangle in the matrix such that its sum is no larger than k.
 *
 * It is guaranteed that there will be a rectangle with a sum no larger than
 * k.
 *
 * Example 1:
 * Input: matrix = [[1,0,1],[0,-2,3]], k = 2
 * Output: 2
 * Explanation: Because the sum of the blue rectangle [[0, 1], [-2, 3]] is 2,
 * and 2 is the max number no larger than k (k = 2).
 *
 * Example 2:
 * Input: matrix = [[2,2,-1]], k = 3
 * Output: 3
 *
 * Constraints:
 *     ∙ m == matrix.length
 *     ∙ n == matrix[i].length
 *     ∙ 1 <= m, n <= 100
 *     ∙ -100 <= matrix[i][j] <= 100
 *     ∙ -10⁵ <= k <= 10⁵
 *
 * Follow up: What if the number of rows is much larger than the number of
 * columns?
"""
from sortedcontainers import SortedList
from itertools import accumulate
from typing import List

class Solution:
    def maxSumSubmatrix(self, matrix: List[List[int]], k: int) -> int:
        def find_sum_no_more_than(nums, limit):
            sums = SortedList([0])
            ret = -100000
            for s in accumulate(nums):
                i = sums.bisect_left(s - limit)
                if i < len(sums):
                    ret = max(ret, s - sums[i])
                sums.add(s)
            return ret

        m = len(matrix)
        n = len(matrix[0])
        for i in range(1, m):
            for j in range(n):
                matrix[i][j] += matrix[i-1][j]
        matrix = [[0] * n] + matrix

        ret = -100000
        for r1 in range(m):
            for r2 in range(r1 + 1, m + 1):
                row = [j - i for i, j in zip(matrix[r1], matrix[r2])]
                ret = max(ret, find_sum_no_more_than(row, k))
        return ret

if __name__ == "__main__":
    tests = (
        ([[1,0,1],[0,-2,3]], 2, 2),
        ([[2,2,-1]], 3, 3),
        ([[2,2,-1]], 0, -1),
        ([[5,-4,-3,4],[-3,-4,4,5],[5,1,5,-4]], 10, 10),
    )
    for matrix, k, expected in tests:
        print(Solution().maxSumSubmatrix(matrix, k) == expected)
