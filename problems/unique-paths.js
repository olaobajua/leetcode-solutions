/**
 * 62. Unique Paths (Medium)
 * A robot is located at the top-left corner of a m x n grid.
 *
 * The robot can only move either down or right at any point in time. The robot
 * is trying to reach the bottom-right corner of the grid.
 *
 * How many possible unique paths are there?
 *
 * Example 1:
 * Input: m = 3, n = 7
 * Output: 28
 *
 * Example 2:
 * Input: m = 3, n = 2
 * Output: 3
 * Explanation:
 * From the top-left corner, there are a total of 3 ways to reach the
 * bottom-right corner:
 * 1. Right -> Down -> Down
 * 2. Down -> Down -> Right
 * 3. Down -> Right -> Down
 *
 * Example 3:
 * Input: m = 7, n = 3
 * Output: 28
 *
 * Example 4:
 * Input: m = 3, n = 3
 * Output: 6
 *
 * Constraints:
 *     1 <= m, n <= 100
 *     It's guaranteed that the answer will be less than or equal to 2 * 10⁹.
 */

/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
var uniquePaths = function(m, n) {
    const dp = [...Array(m)].map(r => Array(n).fill(1));
    for (let i = 1; i < dp.length; ++i) {
        for (let j = 1; j < dp[0].length; ++j) {
            dp[i][j] = dp[i-1][j] + dp[i][j-1];
        };
    };
    return dp[dp.length-1][dp[0].length-1];
};

tests = [
    [1, 1, 1],
    [3, 7, 28],
    [3, 2, 3],
    [7, 3, 28],
    [3, 3, 6],
];
for (let [m, n, expected] of tests) {
    console.log(uniquePaths(m, n) == expected);
}
