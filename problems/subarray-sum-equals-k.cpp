/**
 * 560. Subarray Sum Equals K [Medium]
 * Given an array of integers nums and an integer k, return the total number of
 * subarrays whose sum equals to k.
 *
 * A subarray is a contiguous non-empty sequence of elements within an array.
 *
 * Example 1:
 * Input: nums = [1,1,1], k = 2
 * Output: 2
 * Example 2:
 * Input: nums = [1,2,3], k = 3
 * Output: 2
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 2 * 10⁴
 *     ∙ -1000 <= nums[i] <= 1000
 *     ∙ -10⁷ <= k <= 10⁷
 */
class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        unordered_map<int, int> sums;
        sums[0] = 1;
        int ret = 0, cur = 0;
        for (auto x : nums) {
            cur += x;
            ret += sums[cur-k];
            ++sums[cur];
        }
        return ret;
    }
};
