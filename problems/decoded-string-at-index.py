"""
 * 880. Decoded String at Index [Medium]
 * You are given an encoded string s. To decode the string to a tape, the
 * encoded string is read one character at a time and the following steps are
 * taken:
 *     ∙ If the character read is a letter, that letter is written onto the
 *       tape.
 *     ∙ If the character read is a digit d, the entire current tape is
 *       repeatedly written d - 1 more times in total.
 *
 * Given an integer k, return the kᵗʰ letter (1-indexed) in the decoded
 * string.
 *
 * Example 1:
 * Input: s = "leet2code3", k = 10
 * Output: "o"
 * Explanation: The decoded string is "leetleetcodeleetleetcodeleetleetcode".
 * The 10ᵗʰ letter in the string is "o".
 *
 * Example 2:
 * Input: s = "ha22", k = 5
 * Output: "h"
 * Explanation: The decoded string is "hahahaha".
 * The 5ᵗʰ letter is "h".
 *
 * Example 3:
 * Input: s = "a2345678999999999999999", k = 1
 * Output: "a"
 * Explanation: The decoded string is "a" repeated 8301530446056247680 times.
 * The 1ˢᵗ letter is "a".
 *
 * Constraints:
 *     ∙ 2 <= s.length <= 100
 *     ∙ s consists of lowercase English letters and digits 2 through 9.
 *     ∙ s starts with a letter.
 *     ∙ 1 <= k <= 10⁹
 *     ∙ It is guaranteed that k is less than or equal to the length of the
 *       decoded string.
 *     ∙ The decoded string is guaranteed to have less than 2⁶³ letters.
"""
class Solution:
    def decodeAtIndex(self, s: str, k: int) -> str:
        lens = [0]
        for char in s:
            if char.isdigit():
                lens.append(lens[-1] * int(char))
            else:
                lens.append(lens[-1] + 1)
                
        for i in range(len(s), 0, -1):
            k %= lens[i]
            if k == 0 and s[i-1].isalpha():
                return s[i-1]

if __name__ == "__main__":
    tests = (
        ("leet2code3", 10, "o"),
        ("ha22", 5, "h"),
        ("a2345678999999999999999", 1, "a"),
    )
    for s, k, expected in tests:
        print(Solution().decodeAtIndex(s, k) == expected)
