"""
 * 543. Diameter of Binary Tree (Easy)
 * Given the root of a binary tree, return the length of the diameter of the
 * tree.
 *
 * The diameter of a binary tree is the length of the longest path between any
 * two nodes in a tree. This path may or may not pass through the root.
 *
 * The length of a path between two nodes is represented by the number of edges
 * between them.
 *
 * Example 1:
 * Input: root = [1,2,3,4,5]
 * Output: 3
 * Explanation: 3 is the length of the path [4,2,1,3] or [5,2,1,3].
 *
 * Example 2:
 * Input: root = [1,2]
 * Output: 1
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [1, 10⁴].
 *     -100 <= Node.val <= 100
"""
from functools import cache
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def diameterOfBinaryTree(self, root: Optional[TreeNode]) -> int:
        return diameter(root) - 1

@cache
def diameter(r):
    return 0 if r is None else max(height(r.left) + height(r.right) + 1,
                                   max(diameter(r.left), diameter(r.right)))

@cache
def height(r):
    return 0 if r is None else 1 + max(height(r.left), height(r.right))

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5], 3),
        ([1,2], 1),
    )
    for vals, expected in tests:
        print(Solution().diameterOfBinaryTree(build_tree(vals)) == expected)
