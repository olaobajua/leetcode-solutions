/**
 * 629. K Inverse Pairs Array [Hard]
 * For an integer array nums, an inverse pair is a pair of integers [i, j]
 * where 0 <= i < j < nums.length and nums[i] > nums[j].
 *
 * Given two integers n and k, return the number of different arrays consist of
 * numbers from 1 to n such that there are exactly k inverse pairs. Since the
 * answer can be huge, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 3, k = 0
 * Output: 1
 * Explanation: Only the array [1,2,3] which consists of numbers from 1 to 3
 * has exactly 0 inverse pairs.
 *
 * Example 2:
 * Input: n = 3, k = 1
 * Output: 2
 * Explanation: The array [1,3,2] and [2,1,3] have exactly 1 inverse pair.
 *
 * Constraints:
 *     ∙ 1 <= n <= 1000
 *     ∙ 0 <= k <= 1000
 */
class Solution {
    public int kInversePairs(int n, int k) {
        final int MOD = 1000000007;
        int[] dp = new int[k + 2];
        Arrays.fill(dp, 1);
        dp[0] = 0;
        for (int i = 2; i <= n; ++i) {
            long prev = 0;
            int[] ds = new int[k + 2];
            for (int j = 0; j <= k; ++j) {
                prev = (prev + MOD + dp[j+1] - (j >= i ? dp[j-i+1] : 0)) % MOD;
                ds[j+1] = (int)prev;
            }
            dp = ds;
        }
        return (dp[k+1] - dp[k] + MOD) % MOD;
    }
}
