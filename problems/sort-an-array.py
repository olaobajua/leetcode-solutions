"""
 * 912. Sort an Array [Medium]
 * Given an array of integers nums, sort the array in ascending order and
 * return it.
 *
 * You must solve the problem without using any built-in functions in
 * O(nlog(n)) time complexity and with the smallest space complexity possible.
 *
 * Example 1:
 * Input: nums = [5,2,3,1]
 * Output: [1,2,3,5]
 * Explanation: After sorting the array, the positions of some numbers are not
 * changed (for example, 2 and 3), while the positions of other numbers are
 * changed (for example, 1 and 5).
 *
 * Example 2:
 * Input: nums = [5,1,1,2,0,0]
 * Output: [0,0,1,1,2,5]
 * Explanation: Note that the values of nums are not necessairly unique.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 5 * 10⁴
 *     ∙ -5 * 10⁴ <= nums[i] <= 5 * 10⁴
"""
from typing import List

class Solution:
    def sortArray(self, nums: List[int]) -> List[int]:
        def heapify(nums, n, i):
            left = 2 * i + 1
            right = left + 1
            largest = i
            if left < n and nums[largest] < nums[left]:
                largest = left
            if right < n and nums[largest] < nums[right]:
                largest = right
            if largest != i:
                nums[i], nums[largest] = nums[largest], nums[i]
                heapify(nums, n, largest)

        n = len(nums)
        for i in range(n//2, -1, -1):
            heapify(nums, n, i)
        for i in range(n - 1, -1, -1):
            nums[i], nums[0] = nums[0], nums[i]
            heapify(nums, i, 0)
        return nums

if __name__ == "__main__":
    tests = (
        ([5,2,3,1], [1,2,3,5]),
        ([5,1,1,2,0,0], [0,0,1,1,2,5]),
    )
    for nums, expected in tests:
        print(Solution().sortArray(nums) == expected)
