"""
 * 952. Largest Component Size by Common Factor (Hard)
 * You are given an integer array of unique positive integers nums. Consider
 * the following graph:
 *     ∙ There are nums.length nodes, labeled nums[0] to nums[nums.length - 1],
 *     ∙ There is an undirected edge between nums[i] and nums[j] if nums[i] and
 *       nums[j] share a common factor greater than 1.
 *
 * Return the size of the largest connected component in the graph.
 *
 * Example 1:
 * Input: nums = [4,6,15,35]
 * Output: 4
 *
 * Example 2:
 * Input: nums = [20,50,9,63]
 * Output: 2
 *
 * Example 3:
 * Input: nums = [2,3,6,7,4,12,21,39]
 * Output: 8
 *
 * Constraints:
 *     1 <= nums.length <= 2 * 10⁴
 *     1 <= nums[i] <= 10⁵
 *     All the values of nums are unique.
"""
from collections import Counter
from functools import cache
from typing import List

class Solution:
    def largestComponentSize(self, nums: List[int]) -> int:
        groups = DisjointSet()
        for n in nums:
            groups.make_set(n)
            for f in factors(n):
                groups.make_set(f)
                groups.union(f, n)
        return max(Counter(groups.find(n) for n in nums).values())

@cache
def factors(n):
    d = n
    for i in range(2, n):
        if i * i > n:
            break
        if n % i == 0:
            d = i
            break
    return [d] + factors(n // d) if n > 1 else []

class DisjointSet:
    def __init__(self):
        self.parent = {}

    def make_set(self, x):
        self.parent.setdefault(x, x)

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        if (px := self.find(x)) != (py := self.find(y)):
            self.parent[px] = py

if __name__ == "__main__":
    tests = (
        ([5, 25], 2),
        ([4,6,15,35], 4),
        ([20,50,9,63], 2),
        ([2,3,6,7,4,12,21,39], 8),
        ([1,73,51,84,21,55,92,93,85], 7),
        ([36314,20275,20056,86959,30023,8908,92707,34259,65423,65311,6548,73417,17285,88729,6057,54573,23909,12955,61298,6361,22911,23669,30187,37829,6988,47053,63847,77156,69911,75689,13573,33407,22084,21851,18377,99907,563,12249,467,27329,26001,25583,18703,30869,46051,32359,34641,52329,23531,39521,27699,19361,38281,39807,86998,25743,80296,61546,21050,61091,97423,63092,65138,25231,1097,66763,415,63961,94143,41221,6131,85941,90935,9293,97329,12703], 23),
    )
    for nums, expected in tests:
        print(Solution().largestComponentSize(nums) == expected)
