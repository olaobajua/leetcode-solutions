"""
 * 1960. Maximum Product of the Length of Two Palindromic Substrings (Hard)
 *
 * You are given a 0-indexed string s and are tasked with finding two
 * non-intersecting palindromic substrings of odd length such that the product
 * of their lengths is maximized.
 *
 * More formally, you want to choose four integers i, j, k, l such that
 * 0 <= i <= j < k <= l < s.length and both the substrings s[i...j] and
 * s[k...l] are palindromes and have odd lengths. s[i...j] denotes a substring
 * from index i to index j inclusive.
 *
 * Return the maximum possible product of the lengths of the two
 * non-intersecting palindromic substrings.
 *
 * A palindrome is a string that is the same forward and backward.
 * A substring is a contiguous sequence of characters in a string.
 *
 * Example 1:
 * Input: s = "ababbb"
 * Output: 9
 * Explanation: Substrings "aba" and "bbb" are palindromes with odd length.
 * product = 3 * 3 = 9.
 *
 * Example 2:
 * Input: s = "zaaaxbbby"
 * Output: 9
 * Explanation: Substrings "aaa" and "bbb" are palindromes with odd length.
 * product = 3 * 3 = 9.
 *
 * Constraints:
 *
 *     2 <= s.length <= 10⁵
 *     s consists of lowercase English letters.
"""
from itertools import accumulate

def manacher_odd(s):
    """
    Return array of max palindrome radiuses for every point in s.
    """
    s = '^' + s + '$'
    len_s = len(s)
    radii = [0] * len_s
    l = 0
    r = 0
    for i in range(1, len_s - 1):
        radii[i] = max(0, min(r - i, radii[l + (r - i)]))
        while s[i - radii[i]] == s[i + radii[i]]:
            radii[i] += 1
        if i + radii[i] > r:
            l = i - radii[i]
            r = i + radii[i]
    return radii[1:-1]

def pal_lengths_before(radii):
    """
    Find max pal length ending in current point.
    Return array of pals lengths for every point in initial string.
    """
    pals_before = [0]
    radii_queue = []
    last_center = 0
    last_radius = 0
    for i, r in enumerate(radii):
        radii_queue.append((i, r))
        while i - last_center >= last_radius:
            # find the center of last pal not ended before this point
            last_center, last_radius = radii_queue.pop(0)
        left_pal_length = 2 * (i - last_center + 1) - 1
        pals_before.append(left_pal_length)
    return pals_before

class Solution:
    def maxProduct(self, s: str) -> int:
        pal_radii = manacher_odd(s)
        max_pals_before = list(accumulate(pal_lengths_before(pal_radii), max))
        max_pals_after = reversed(list(accumulate(pal_lengths_before(reversed(pal_radii)), max)))
        return max(l * r for l, r in zip(max_pals_before, max_pals_after))

if __name__ == "__main__":
    tests = (
        ("ababbb", 9),
        ("zaaaxbbby", 9),
        ("ggbswiymmlevedhkbdhntnhdbkhdevelmmyiwsbgg", 45),
        # ggbswiymml eve dhkbdhntnhdbkhd eve lmmyiwsbgg
        ("rofcjxfkbzcvvlbkgcwtcjctwcgkblvvczbkfxjcfor", 41),
    )
    from time import time
    start = time()
    for s, expected in tests:
        print(Solution().maxProduct(s) == expected, flush=True)
    print(f"Total time: {time() - start} sec")
