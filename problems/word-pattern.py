"""
 * 290. Word Pattern [Easy]
 * Given a pattern and a string s, find if s follows the same pattern.
 *
 * Here follow means a full match, such that there is a bijection between a
 * letter in pattern and a non-empty word in s.
 *
 * Example 1:
 * Input: pattern = "abba", s = "dog cat cat dog"
 * Output: true
 *
 * Example 2:
 * Input: pattern = "abba", s = "dog cat cat fish"
 * Output: false
 *
 * Example 3:
 * Input: pattern = "aaaa", s = "dog cat cat dog"
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= pattern.length <= 300
 *     ∙ pattern contains only lower-case English letters.
 *     ∙ 1 <= s.length <= 3000
 *     ∙ s contains only lowercase English letters and spaces ' '.
 *     ∙ s does not contain any leading or trailing spaces.
 *     ∙ All the words in s are separated by a single space.
"""
from typing import List

class Solution:
    def wordPattern(self, pattern: str, s: str) -> bool:
        s = s.split()
        return (len(pattern) == len(s) and
                len(set(zip(pattern, s))) == len(set(pattern)) == len(set(s)))

if __name__ == "__main__":
    tests = (
        ("abba", "dog cat cat dog", True),
        ("abba", "dog cat cat fish", False),
        ("aaaa", "dog cat cat dog", False),
        ("abba", "dog dog dog dog", False),
        ("aaa", "aa aa aa aa", False),
        ("he", "unit", False),
    )
    for pattern, s, expected in tests:
        print(Solution().wordPattern(pattern, s) == expected)
