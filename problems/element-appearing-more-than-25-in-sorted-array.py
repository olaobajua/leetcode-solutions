"""
 * 1287. Element Appearing More Than 25% In Sorted Array [Easy]
 * Given an integer array sorted in non-decreasing order, there is exactly one
 * integer in the array that occurs more than 25% of the time, return that
 * integer.
 *
 * Example 1:
 * Input: arr = [1,2,2,6,6,6,6,7,10]
 * Output: 6
 *
 * Example 2:
 * Input: arr = [1,1]
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 10⁴
 *     ∙ 0 <= arr[i] <= 10⁵
"""
from typing import List

class Solution:
    def findSpecialInteger(self, arr: List[int]) -> int:
        n = len(arr)
        for i in range(n):
            j = min(i + n//4, n)
            if arr[i] == arr[j]:
                return arr[i]

if __name__ == "__main__":
    tests = (
        ([1,2,2,6,6,6,6,7,10], 6),
        ([1,1], 1),
    )
    for arr, expected in tests:
        print(Solution().findSpecialInteger(arr) == expected)
