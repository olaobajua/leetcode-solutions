/**
 * 878. Nth Magical Number (Hard)
 * A positive integer is magical if it is divisible by either a or b.
 *
 * Given the three integers n, a, and b, return the nᵗʰ magical number. Since
 * the answer may be very large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 1, a = 2, b = 3
 * Output: 2
 *
 * Example 2:
 * Input: n = 4, a = 2, b = 3
 * Output: 6
 *
 * Example 3:
 * Input: n = 5, a = 2, b = 4
 * Output: 10
 *
 * Example 4:
 * Input: n = 3, a = 6, b = 4
 * Output: 8
 *
 * Constraints:
 *     1 <= n <= 10⁹
 *     2 <= a, b <= 4 * 10⁴
 */

/**
 * @param {number} n
 * @param {number} a
 * @param {number} b
 * @return {number}
 */
var nthMagicalNumber = function(n, a, b) {
    const lcm = a * b / gcd(a, b);
    const magicNumbersPerLcm = lcm / a + lcm / b - 1;
    const lcmBlocks = Math.floor(n / magicNumbersPerLcm);
    const magicNumbersRemain = n % magicNumbersPerLcm;
    const nearest = magicNumbersRemain / (1/a + 1/b);
    const magicNumberInLcm = Math.min(Math.ceil(nearest / a) * a,
                                      Math.ceil(nearest / b) * b);
    return (lcmBlocks * lcm + magicNumberInLcm) % (1e9 + 7);
};

function gcd(a, b) {
    return !b ? a : gcd(b, a % b);
}

const tests = [
    [1, 2, 3, 2],
    [4, 2, 3, 6],
    [2, 7, 3, 6],
    [5, 2, 4, 10],
    [3, 6, 4, 8],
    [3, 8, 3, 8],
    [9, 10, 2, 18],
    [7, 5, 8, 24],
    [859, 759, 30, 24900],
    [1000000000, 40000, 40000, 999720007],
];

for (const [n, a, b, expected] of tests) {
    console.log(nthMagicalNumber(n, a, b) == expected);
}
