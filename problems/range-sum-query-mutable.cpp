/**
 * 307. Range Sum Query - Mutable [Medium]
 * Given an integer array nums, handle multiple queries of the following types:
 *     ∙ Update the value of an element in nums.
 *     ∙ Calculate the sum of the elements of nums between indices left and
 *       right inclusive where left <= right.
 *
 * Implement the NumArray class:
 *     ∙ NumArray(int[] nums) Initializes the object with the integer array
 *       nums.
 *     ∙ void update(int index, int val) Updates the value of nums[index] to be
 *       val.
 *     ∙ int sumRange(int left, int right) Returns the sum of the elements of
 *       nums between indices left and right inclusive
 *       (i.e. nums[left] + nums[left + 1] + ... + nums[right]).
 *
 * Example 1:
 * Input
 * ["NumArray", "sumRange", "update", "sumRange"]
 * [[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]
 * Output
 * [null, 9, null, 8]
 *
 * Explanation
 * NumArray numArray = new NumArray([1, 3, 5]);
 * numArray.sumRange(0, 2); // return 1 + 3 + 5 = 9
 * numArray.update(1, 2);   // nums = [1, 2, 5]
 * numArray.sumRange(0, 2); // return 1 + 2 + 5 = 8
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 3 * 10⁴
 *     ∙ -100 <= nums[i] <= 100
 *     ∙ 0 <= index < nums.length
 *     ∙ -100 <= val <= 100
 *     ∙ 0 <= left <= right < nums.length
 *     ∙ At most 3 * 10⁴ calls will be made to update and sumRange.
 */

/* Binary Indexed Tree implementation. */
class BIT {
private:
    vector<int> sums;

public:
    BIT(int n) {
        sums = vector<int>(n + 1, 0);
    }

    void update(int i, int delta) {
        while (i < sums.size()) {
            sums[i] += delta;
            i += i & -i;
        }
    }

    int query(int i) {
        int ret = 0;
        while (i > 0) {
            ret += sums[i];
            i -= i & -i;
        }
        return ret;
    }
};

class NumArray {
private:
    BIT bit;
    vector<int> nums;

public:
    NumArray(vector<int>& nums) : bit(nums.size()) {
        for (int i = 0; i < nums.size(); ++i)
            bit.update(i + 1, nums[i]);
        this->nums = vector<int>(nums.size() + 1, 0);
        copy(nums.begin(), nums.end(), this->nums.begin() + 1);
    }

    void update(int index, int val) {
        bit.update(index + 1, val - nums[index+1]);
        nums[index+1] = val;
    }

    int sumRange(int left, int right) {
        return bit.query(right + 1) - bit.query(left);
    }
};
