/**
 * 430. Flatten a Multilevel Doubly Linked List (Medium)
 * You are given a doubly linked list which in addition to the next and
 * previous pointers, it could have a child pointer, which may or may not point
 * to a separate doubly linked list. These child lists may have one or more
 * children of their own, and so on, to produce a multilevel data structure, as
 * shown in the example below.
 *
 * Flatten the list so that all the nodes appear in a single-level, doubly
 * linked list. You are given the head of the first level of the list.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
 * Output: [1,2,3,7,8,11,12,9,10,4,5,6]
 * Explanation:
 * The multilevel linked list in the input is as follows:
 *  1---2---3---4---5---6--NULL
 *          |
 *          7---8---9---10--NULL
 *              |
 *              11--12--NULL
 *
 * After flattening the multilevel linked list it becomes:
 *  1---2---3---7---8---11---12---9---10---4---5---6
 *
 * Example 2:
 * Input: head = [1,2,null,3]
 * Output: [1,3,2]
 * Explanation:
 * The input multilevel linked list is as follows:
 *
 *   1---2---NULL
 *   |
 *   3---NULL
 *
 * Example 3:
 * Input: head = []
 * Output: []
 *
 * How multilevel linked list is represented in test case:
 *
 * We use the multilevel linked list from Example 1 above:
 *  1---2---3---4---5---6--NULL
 *          |
 *          7---8---9---10--NULL
 *              |
 *              11--12--NULL
 *
 * The serialization of each level is as follows:
 * [1,2,3,4,5,6,null]
 * [7,8,9,10,null]
 * [11,12,null]
 *
 * To serialize all levels together we will add nulls in each level to signify
 * no node connects to the upper node of the previous level. The serialization
 * becomes:
 * [1,2,3,4,5,6,null]
 * [null,null,7,8,9,10,null]
 * [null,11,12,null]
 *
 * Merging the serialization of each level and removing trailing nulls we
 * obtain:
 * [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
 *
 * Constraints:
 *     The number of Nodes will not exceed 1000.
 *     1 <= Node.val <= 10⁵
 */

// Definition for a Node.
function Node(val,prev,next,child) {
   this.val = val;
   this.prev = prev;
   this.next = next;
   this.child = child;
};

/**
 * @param {Node} head
 * @return {Node}
 */
var flatten = function(head) {
    if (head) {
        let root = new Node(0, null, head, null);
        let prev = root;
        let stack = [head];
        while (stack.length) {
            let cur = stack.pop();
            prev.next = cur;
            cur.prev = prev;
            if (cur.next) {
                stack.push(cur.next);
            }
            if (cur.child) {
                stack.push(cur.child);
                cur.child = null;
            }
            prev = cur;
        }
        root = root.next;
        root.prev = null;
        return root;
    }
};

function listToDoublyLinkedList(nums) {
    const root = nums.length ? new Node(nums.shift(), null, null, null) : null;
    let cur = root;
    let nodes = [cur];
    let insert = 1;
    while (nums.length) {
        let num = nums.shift();
        if (num !== null) {
            if (cur.next) {
                cur.child = new Node(num, cur, null, null);
                cur = cur.child;
                nodes.unshift(cur);
                insert = 1;
            } else {
                cur.next = new Node(num, cur, null, null);
                cur = cur.next;
                nodes.splice(insert, 0, cur);
                ++insert;
            }
        } else {
            cur = nodes.shift();
        }
    }
    return root;
}

function doublyLinkedListToList(root) {
    let ret = [];
    let nodes = [root];
    let insert = 1;
    while (nodes.length) {
        let root = nodes.shift();
        while (root) {
            ret.push(root.val);
            nodes.splice(insert, 0, root.child);
            ++insert;
            root = root.next;
        }
        insert = 0;
        ret.push(null);
    }
    while (ret.length && ret[ret.length-1] === null) {
        ret.pop();
    }
    return ret;
}

const tests = [
    [[1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12], [1,2,3,7,8,11,12,9,10,4,5,6]],
    [[1,2,null,3], [1,3,2]],
    [[], []],
];

for (let [nums, expected] of tests) {
    const root = listToDoublyLinkedList(nums);
    console.log(doublyLinkedListToList(flatten(root)).every((x, i) => x == expected[i]));
}
