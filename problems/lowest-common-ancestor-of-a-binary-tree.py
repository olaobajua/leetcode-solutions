"""
 * 236. Lowest Common Ancestor of a Binary Tree [Medium]
 * Given a binary tree, find the lowest common ancestor (LCA) of two given
 * nodes in the tree.
 *
 * According to the definition of LCA on Wikipedia
 * (https://en.wikipedia.org/wiki/Lowest_common_ancestor): «The lowest common
 * ancestor is defined between two nodes p and q as the lowest node in T that
 * has both p and q as descendants (where we allow a node to be a descendant of
 * itself)».

 * Example 1:
 *     3
 *  5     1
 * 6 2   0 8
 *  7 4
 *
 * Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
 * Output: 3
 * Explanation: The LCA of nodes 5 and 1 is 3.
 *
 * Example 2:
 *     3
 *  5     1
 * 6 2   0 8
 *  7 4
 *
 * Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
 * Output: 5
 * Explanation: The LCA of nodes 5 and 4 is 5, since a node can be a descendant
 * of itself according to the LCA definition.
 *
 * Example 3:
 *  1
 * 2
 * Input: root = [1,2], p = 1, q = 2
 * Output: 1
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [2, 10⁵].
 *     ∙ -10⁹ <= Node.val <= 10⁹
 *     ∙ All Node.val are unique.
 *     ∙ p != q
 *     ∙ p and q will exist in the tree.
"""
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        if root in (None, p, q):
            return root
        left, right = (self.lowestCommonAncestor(child, p, q)
                       for child in (root.left, root.right))
        return root if left and right else left or right

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([3,5,1,6,2,0,8,None,None,7,4], 5, 1, 3),
        ([3,5,1,6,2,0,8,None,None,7,4], 5, 4, 5),
        ([1,2], 1, 2, 1),
    )
    for nums, p, q, expected in tests:
        root = build_tree(nums)
        nodes = [root]
        pnode = qnode = None
        for node in nodes:
            if node is None:
                continue
            if node.val == p:
                pnode = node
            if node.val == q:
                qnode = node
            if pnode and qnode:
                break
            nodes.extend([node.left, node.right])

        print(Solution().lowestCommonAncestor(root, pnode, qnode).val ==
              expected)
