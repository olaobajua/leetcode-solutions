/**
 * 3. Longest Substring Without Repeating Characters (Medium)
 * Given a string s, find the length of the longest substring without repeating
 * characters.
 *
 * Example 1:
 * Input: s = "abcabcbb"
 * Output: 3
 * Explanation: The answer is "abc", with the length of 3.
 *
 * Example 2:
 * Input: s = "bbbbb"
 * Output: 1
 * Explanation: The answer is "b", with the length of 1.
 *
 * Example 3:
 * Input: s = "pwwkew"
 * Output: 3
 * Explanation: The answer is "wke", with the length of 3.
 * Notice that the answer must be a substring, "pwke" is a subsequence and not
 * a substring.
 *
 * Constraints:
 *     ∙ 0 <= s.length <= 5 * 10⁴
 *     ∙ s consists of English letters, digits, symbols and spaces.
 */
int lengthOfLongestSubstring(char *s) {
    int ret = 0, start = 0;
    int lastPos[128] = {0};
    for (int end = 0; s[end] != '\0'; ++end) {
        start = fmax(lastPos[s[end]], start);
        lastPos[s[end]] = end + 1;
        ret = fmax(ret, end - start + 1);
    }
    return ret;
}
