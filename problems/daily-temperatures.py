"""
 * 739. Daily Temperatures [Medium]
 * Given an array of integers temperatures represents the daily temperatures,
 * return an array answer such that answer[i] is the number of days you have to
 * wait after the iᵗʰ day to get a warmer temperature. If there is no future
 * day for which this is possible, keep answer[i] == 0 instead.
 *
 * Example 1:
 * Input: temperatures = [73,74,75,71,69,72,76,73]
 * Output: [1,1,4,2,1,1,0,0]
 * Example 2:
 * Input: temperatures = [30,40,50,60]
 * Output: [1,1,1,0]
 * Example 3:
 * Input: temperatures = [30,60,90]
 * Output: [1,1,0]
 *
 * Constraints:
 *     ∙ 1 <= temperatures.length <= 10⁵
 *     ∙ 30 <= temperatures[i] <= 100
"""
from typing import List

class Solution:
    def dailyTemperatures(self, temperatures: List[int]) -> List[int]:
        ret = [0] * len(temperatures)
        stack = []
        for cur_day, temp in enumerate(temperatures):
            while stack and stack[-1][0] < temp:
                _, day = stack.pop()
                ret[day] = cur_day - day
            stack.append([temp, cur_day])
        return ret

if __name__ == "__main__":
    tests = (
        ([73,74,75,71,69,72,76,73], [1,1,4,2,1,1,0,0]),
        ([30,40,50,60], [1,1,1,0]),
        ([30,60,90], [1,1,0]),
    )
    for temperatures, expected in tests:
        print(Solution().dailyTemperatures(temperatures) == expected)
