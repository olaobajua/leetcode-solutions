"""
 * 623. Add One Row to Tree [Medium]
 * Given the root of a binary tree and two integers val and depth, add a row
 * of nodes with value val at the given depth depth.
 *
 * Note that the root node is at depth 1.
 *
 * The adding rule is:
 *     ∙ Given the integer depth, for each not null tree node cur at the depth
 *       depth - 1, create two tree nodes with value val as cur's left subtree
 *       root and right subtree root.
 *     ∙ cur's original left subtree should be the left subtree of the new
 *       left subtree root.
 *     ∙ cur's original right subtree should be the right subtree of the new
 *       right subtree root.
 *     ∙ If depth == 1 that means there is no depth depth - 1 at all, then
 *       create a tree node with value val as the new root of the whole
 *       original tree, and the original tree is the new root's left subtree.
 *
 * Example 1:
 *     4              4
 *  2     6   ->   1     1
 * 3 1   5        2       6
 *               3 1     5
 * Input: root = [4,2,6,3,1,5], val = 1, depth = 2
 * Output: [4,1,1,2,null,null,6,3,1,5]
 *
 * Example 2:
 *     4              4
 *  2         ->   2
 * 3 1            1 1
 *               3   1
 * Input: root = [4,2,null,3,1], val = 1, depth = 3
 * Output: [4,2,null,1,1,3,null,null,1]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ The depth of the tree is in the range [1, 10⁴].
 *     ∙ -100 <= Node.val <= 100
 *     ∙ -10⁵ <= val <= 10⁵
 *     ∙ 1 <= depth <= the depth of tree + 1
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def addOneRow(self, root: Optional[TreeNode], val: int, depth: int) -> Optional[TreeNode]:
        def dfs(node, d):
            if node:
                if d > 1:
                    dfs(node.left, d - 1)
                    dfs(node.right, d - 1)
                else:
                    node.left = TreeNode(val, left=node.left)
                    node.right = TreeNode(val, right=node.right)
        if depth == 1:
            return TreeNode(val, root)
        dfs(root, depth - 1)
        return root

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([4,2,6,3,1,5], 1, 2, [4,1,1,2,None,None,6,3,1,5]),
        ([4,2,None,3,1], 1, 3, [4,2,None,1,1,3,None,None,1]),
    )
    for nums, val, depth, expected in tests:
        ret = Solution().addOneRow(build_tree(nums), val, depth)
        ret = [*tree_to_list(ret)]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
