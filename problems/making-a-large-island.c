/*
 * 827. Making A Large Island (Hard)
 * You are given an n x n binary matrix grid. You are allowed to change at most one 0 to be 1.
 * Return the size of the largest island in grid after applying this operation.
 * An island is a 4-directionally connected group of 1s.
 *
 * Example 1:
 * Input: grid = [[1,0],[0,1]]
 * Output: 3
 * Explanation: Change one 0 to 1 and connect two 1s, then we get an island with area = 3.
 *
 * Example 2:
 * Input: grid = [[1,1],[1,0]]
 * Output: 4
 * Explanation: Change the 0 to 1 and make the island bigger, only one island with area = 4.
 *
 * Example 3:
 * Input: grid = [[1,1],[1,1]]
 * Output: 4
 * Explanation: Can't change any 0 to 1, only one island with area = 4.
 *
 * Constraints:
 *     n == grid.length
 *     n == grid[i].length
 *     1 <= n <= 500
 *     grid[i][j] is either 0 or 1.
*/
#define max(a,b) ((a)>(b)?(a):(b))
#define MAXGRID 500*500

int isles[MAXGRID];

void floodFill(int** matrix, int matrixSize, int x, int y, int old, int new) {
    if (matrix[x][y] == old) {
        matrix[x][y] = new;
        if (x > 0) {
            floodFill(matrix, matrixSize, x - 1, y, old, new);
        }
        if (x < matrixSize - 1) {
            floodFill(matrix, matrixSize, x + 1, y, old, new);
        }
        if (y > 0) {
            floodFill(matrix, matrixSize, x, y - 1, old, new);
        }
        if (y < matrixSize - 1) {
            floodFill(matrix, matrixSize, x, y + 1, old, new);
        }
    }
}
int in(int n, int *arr, int arrLen) {
    for (int i = 0; i < arrLen; ++i) {
        if (n == arr[i]) {
            return 1;
        }
    }
    return 0;
}

int newArea(int** matrix, int matrixSize, int* areas, int x, int y) {
    int area = 1;
    int united[5] = {0};
    int uc = 1;
    if (matrix[x][y] == 0) {
        if (x > 0 && !in(matrix[x - 1][y], united, uc)) {
            united[uc++] = matrix[x - 1][y];
            area += areas[matrix[x - 1][y]];
        }
        if (x < matrixSize - 1 && !in(matrix[x + 1][y], united, uc)) {
            united[uc++] = matrix[x + 1][y];
            area += areas[matrix[x + 1][y]];
        }
        if (y > 0 && !in(matrix[x][y - 1], united, uc)) {
            united[uc++] = matrix[x][y - 1];
            area += areas[matrix[x][y - 1]];
        }
        if (y < matrixSize - 1 && !in(matrix[x][y + 1], united, uc)) {
            united[uc++] = matrix[x][y + 1];
            area += areas[matrix[x][y + 1]];
        }
    }
    return area;
}

int largestIsland(int** grid, int gridSize, int* gridColSize){
    int isleN = 2;
    for (int x = 0; x < gridSize; ++x) {
        for (int y = 0; y < gridSize; ++y) {
            if (grid[x][y] == 1) {
                floodFill(grid, gridSize, x, y, 1, isleN++);
            }
        }
    }
    memset(isles, 0, MAXGRID * sizeof(int));
    for (int x = 0; x < gridSize; ++x) {
        for (int y = 0; y < gridSize; ++y) {
            ++isles[grid[x][y]];
        }
    }
    int maxIsle = 0;
    for (int i = 2; i < isleN; ++i) {
        maxIsle = max(maxIsle, isles[i]);
    }
    for (int x = 0; x < gridSize; ++x) {
        for (int y = 0; y < gridSize; ++y) {
            maxIsle = max(maxIsle, newArea(grid, gridSize, isles, x, y));
        }
    }
    return maxIsle;
}
