"""
 * 1192. Critical Connections in a Network (Hard)
 * There are n servers numbered from 0 to n - 1 connected by undirected
 * server-to-server connections forming a network where
 * connections[i] = [aᵢ, bᵢ] represents a connection between servers aᵢ and bᵢ.
 * Any server can reach other servers directly or indirectly through
 * the network.
 *
 * A critical connection is a connection that, if removed, will make some
 * servers unable to reach some other server.
 *
 * Return all critical connections in the network in any order.
 *
 * Example 1:
 * Input: n = 4, connections = [[0,1],[1,2],[2,0],[1,3]]
 * Output: [[1,3]]
 * Explanation: [[3,1]] is also accepted.
 *
 * Example 2:
 * Input: n = 2, connections = [[0,1]]
 * Output: [[0,1]]
 *
 * Constraints:
 *     ∙ 2 <= n <= 10⁵
 *     ∙ n - 1 <= connections.length <= 10⁵
 *     ∙ 0 <= aᵢ, bᵢ <= n - 1
 *     ∙ aᵢ != bᵢ
 *     ∙ There are no repeated connections.
"""
from typing import List

class Solution:
    def criticalConnections(self, n: int, connections: List[List[int]]) -> List[List[int]]:
        def dfs(node, parent=-1):
            nonlocal timer
            used[node] = True
            time_in[node] = min_time[node] = timer + 1
            timer += 1
            for child in graph[node]:
                if child == parent:
                    continue
                if used[child]:
                    min_time[node] = min(min_time[node], time_in[child])
                else:
                    dfs(child, node)
                    min_time[node] = min(min_time[node], min_time[child])
                    if min_time[child] > time_in[node]:
                        ret.append([node, child])

        used = [False]*n
        time_in = [-1]*n
        min_time = [-1]*n
        timer = 0
        ret = []
        graph = [[] for _ in range(n)]

        for i, j in connections:
            graph[i].append(j)
            graph[j].append(i)

        for i in range(n):
            if not used[i]:
                dfs(i)

        return ret


if __name__ == "__main__":
    tests = (
        (4, [[0,1],[1,2],[2,0],[1,3]], [[1,3]]),
        (2, [[0,1]], [[0,1]]),
        (5, [[1,0],[2,0],[3,2],[4,2],[4,3],[3,0],[4,0]], [[0,1]]),
    )
    for n, connections, expected in tests:
        print(Solution().criticalConnections(n, connections) == expected)
