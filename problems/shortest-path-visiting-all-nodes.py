"""
 * 847. Shortest Path Visiting All Nodes (Hard)
 * You have an undirected, connected graph of n nodes labeled from 0 to n - 1.
 * You are given an array graph where graph[i] is a list of all the nodes
 * connected with node i by an edge.
 *
 * Return the length of the shortest path that visits every node. You may start
 * and stop at any node, you may revisit nodes multiple times, and you may
 * reuse edges.
 *
 * Example 1:
 * Input: graph = [[1,2,3],[0],[0],[0]]
 * Output: 4
 * Explanation: One possible path is [1,0,2,0,3]
 *
 * Example 2:
 * Input: graph = [[1],[0,2,4],[1,3,4],[2],[1,2]]
 * Output: 4
 * Explanation: One possible path is [0,1,4,2,3]
 *
 * Constraints:
 *     ∙ n == graph.length
 *     ∙ 1 <= n <= 12
 *     ∙ 0 <= graph[i].length < n
 *     ∙ graph[i] does not contain i.
 *     ∙ If graph[a] contains b, then graph[b] contains a.
 *     ∙ The input graph is always connected.
"""
from typing import List

class Solution:
    def shortestPathLength(self, graph: List[List[int]]) -> int:
        if (n := len(graph)) <= 3:
            return n - 1
        mask = (1 << n) - 1
        to_visit = [(node, 0, 1 << node) for node in range(n)]
        seen = set()
        for cur, distance, visited in to_visit:
            if (cur, visited) not in seen:
                seen.add((cur, visited))
                visited |= 1 << cur
                if visited == mask:
                    return distance
                for child in graph[cur]:
                    to_visit.append((child, distance + 1, visited))

if __name__ == "__main__":
    tests = (
        ([[]], 0),
        ([[1,2,3],[0],[0],[0]], 4),
        ([[1],[0,2,4],[1,3,4],[2],[1,2]], 4),
        ([[6,8],[2,9],[1,3,5],[2,6],[5],[2,6,4],[5,3,0,7],[6],[0],[1]], 12),
        ([[1,4,6,8,9],[0,6],[9],[5],[0],[7,3],[0,1],[9,5],[0],[0,2,7]], 12),
        ([[2,5,7],[2,4],[0,1],[5],[5,6,1],[4,10,8,0,3],[4,9],[0],[5],[6],[5]], 13),
        ([[2,3,5,7],[2,3,7],[0,1],[0,1],[7],[0],[10],[9,10,0,1,4],[9],[7,8],[7,6]], 14),
    )
    for graph, expected in tests:
        print(Solution().shortestPathLength(graph) == expected, flush=True)
