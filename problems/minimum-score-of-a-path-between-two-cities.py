"""
 * 2492. Minimum Score of a Path Between Two Cities [Medium]
 * You are given a positive integer n representing n cities numbered from 1 to
 * n. You are also given a 2D array roads where roads[i] = [aᵢ, bᵢ, distanceᵢ]
 * indicates that there is a bidirectional road between cities aᵢ and bᵢ with a
 * distance equal to distanceᵢ. The cities graph is not necessarily connected.
 *
 * The score of a path between two cities is defined as the minimum distance
 * of a road in this path.
 *
 * Return the minimum possible score of a path between cities 1 and n.
 *
 * Note:
 *     ∙ A path is a sequence of roads between two cities.
 *     ∙ It is allowed for a path to contain the same road multiple times, and
 *       you can visit cities 1 and n multiple times along the path.
 *     ∙ The test cases are generated such that there is at least one path
 *       between 1 and n.
 *
 * Example 1:
 * Input: n = 4, roads = [[1,2,9],[2,3,6],[2,4,5],[1,4,7]]
 * Output: 5
 * Explanation: The path from city 1 to 4 with the minimum score is: 1 -> 2 ->
 * 4. The score of this path is min(9,5) = 5.
 * It can be shown that no other path has less score.
 *
 * Example 2:
 * Input: n = 4, roads = [[1,2,2],[1,3,4],[3,4,7]]
 * Output: 2
 * Explanation: The path from city 1 to 4 with the minimum score is: 1 -> 2 ->
 * 1 -> 3 -> 4. The score of this path is min(2,2,4,7) = 2.
 *
 * Constraints:
 *     ∙ 2 <= n <= 10⁵
 *     ∙ 1 <= roads.length <= 10⁵
 *     ∙ roads[i].length == 3
 *     ∙ 1 <= aᵢ, bᵢ <= n
 *     ∙ aᵢ != bᵢ
 *     ∙ 1 <= distanceᵢ <= 10⁴
 *     ∙ There are no repeated edges.
 *     ∙ There is at least one path between 1 and n.
"""
from math import inf
from typing import List

class Solution:
    def minScore(self, n: int, roads: List[List[int]]) -> int:
        def dfs(node):
            nonlocal ret
            visited.add(node)
            for neib, distance in graph[node]:
                if neib not in visited:
                    dfs(neib)
                ret = min(ret, distance)

        graph = [set() for _ in range(n + 1)]
        for a, b, distance in roads:
            graph[a].add((b, distance))
            graph[b].add((a, distance))
        ret = inf
        visited = set()
        dfs(1)
        return ret

if __name__ == "__main__":
    tests = (
        (4, [[1,2,9],[2,3,6],[2,4,5],[1,4,7]], 5),
        (4, [[1,2,2],[1,3,4],[3,4,7]], 2),
        (6, [[4,5,7468],[6,2,7173],[6,3,8365],[2,3,7674],[5,6,7852],[1,2,8547],[2,4,1885],[2,5,5192],[1,3,4065],[1,4,7357]], 1885),
        (83, [[37,77,7994],[77,79,2857],[79,28,1678],[16,21,7223],[44,2,1872],[15,26,383],[54,15,9033],[38,79,9479],[27,69,6813],[60,10,5810],[17,20,9118],[38,72,8826],[7,64,249],[2,65,4937],[79,44,6593],[26,78,3208],[3,18,5008],[33,54,3534],[79,65,3363],[64,34,1483],[52,79,6717],[75,16,5245],[72,7,5468],[67,3,2706],[52,33,4443],[61,75,3034],[24,6,6406],[26,79,9039],[18,61,1265],[62,52,2962],[79,15,2084],[6,17,5652],[10,44,393],[79,61,8296],[20,37,7689],[79,54,4069],[58,79,5651],[78,58,8112],[1,62,1686],[58,67,143],[79,33,4041],[79,1,655],[65,28,8294],[28,38,2799],[34,24,8936],[16,60,8422],[59,83,2174],[79,78,5522],[37,79,6107],[77,59,223]], 143),
    )
    for n, roads, expected in tests:
        print(Solution().minScore(n, roads) == expected)
