"""
 * 128. Longest Consecutive Sequence (Medium)
 * Given an unsorted array of integers nums, return the length of the longest
 * consecutive elements sequence.
 *
 * You must write an algorithm that runs in O(n) time.
 *
 * Example 1:
 * Input: nums = [100,4,200,1,3,2]
 * Output: 4
 * Explanation: The longest consecutive elements sequence is [1, 2, 3, 4].
 * Therefore its length is 4.
 *
 * Example 2:
 * Input: nums = [0,3,7,2,5,8,4,6,0,1]
 * Output: 9
 *
 * Constraints:
 *     ∙ 0 <= nums.length <= 10⁵
 *     ∙ -10⁹ <= nums[i] <= 10⁹
"""
from typing import List

class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        nums = set(nums)
        ret = 0
        for x in nums:
            if x - 1 not in nums:
                y = x + 1
                while y in nums:
                    y += 1
                ret = max(ret, y - x)
        return ret

if __name__ == "__main__":
    tests = (
        ([100,4,200,1,3,2], 4),
        ([0,3,7,2,5,8,4,6,0,1], 9),
        ([], 0),
        ([1,2,0,1], 3),
    )
    for nums, expected in tests:
        print(Solution().longestConsecutive(nums) == expected)
