"""
 * 405. Convert a Number to Hexadecimal [Easy]
 * Given an integer num, return a string representing its hexadecimal
 * representation. For negative integers, two's complement
 * (https://en.wikipedia.org/wiki/Two%27s_complement) method is used.
 *
 * All the letters in the answer string should be lowercase characters, and
 * there should not be any leading zeros in the answer except for the zero
 * itself.
 *
 * Note: You are not allowed to use any built-in library method to directly
 * solve this problem.
 *
 * Example 1:
 * Input: num = 26
 * Output: "1a"
 * Example 2:
 * Input: num = -1
 * Output: "ffffffff"
 *
 * Constraints:
 *     ∙ -2³¹ <= num <= 2³¹ - 1
"""
class Solution:
    def toHex(self, num: int) -> str:
        if num == 0:
            return "0"

        D = {10: "a", 11: "b", 12: "c", 13: "d", 14: "e", 15: "f"}
        ret = []
        if num < 0:
            num = 2**32 + num
        while num:
            ret.append(D.get(num % 16, str(num % 16)))
            num //= 16

        return "".join(reversed(ret))

if __name__ == "__main__":
    tests = (
        (26, "1a"),
        (-1, "ffffffff"),
        (0, "0"),
    )
    for num, expected in tests:
        print(Solution().toHex(num) == expected)
