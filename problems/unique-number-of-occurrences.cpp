/*
 * 1207. Unique Number of Occurrences [Easy]
 * Given an array of integers arr, return true if the number of occurrences of
 * each value in the array is unique or false otherwise.
 *
 * Example 1:
 * Input: arr = [1,2,2,1,1,3]
 * Output: true
 * Explanation: The value 1 has 3 occurrences, 2 has 2 and 3 has 1. No two
 * values have the same number of occurrences.
 *
 * Example 2:
 * Input: arr = [1,2]
 * Output: false
 *
 * Example 3:
 * Input: arr = [-3,0,1,-3,1,1,1,-3,10,0]
 * Output: true
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 1000
 *     ∙ -1000 <= arr[i] <= 1000
 */
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>

class Solution {
public:
    bool uniqueOccurrences(vector<int>& arr) {
        std::unordered_map<int, int> count;
        for (int x : arr) {
            ++count[x];
        }

        int prev = -1;
        std::unordered_set<int> occurrences;
        for (const auto& kv : count) {
            occurrences.insert(kv.second);
        }

        return count.size() == occurrences.size();
    }
};
