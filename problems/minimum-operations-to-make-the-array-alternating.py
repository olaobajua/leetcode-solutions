"""
 * 2170. Minimum Operations to Make the Array Alternating (Medium)
 * You are given a 0-indexed array nums consisting of n positive integers.
 *
 * The array nums is called alternating if:
 *     ∙ nums[i - 2] == nums[i], where 2 <= i <= n - 1.
 *     ∙ nums[i - 1] != nums[i], where 1 <= i <= n - 1.
 *
 * In one operation, you can choose an index i and change nums[i] into any
 * positive integer.
 *
 * Return the minimum number of operations required to make the array
 * alternating.
 *
 * Example 1:
 * Input: nums = [3,1,3,2,4,3]
 * Output: 3
 * Explanation:
 * One way to make the array alternating is by converting it to [3,1,3,1,3,1].
 * The number of operations required in this case is 3.
 * It can be proven that it is not possible to make the array alternating in
 * less than 3 operations.
 *
 * Example 2:
 * Input: nums = [1,2,2,2,2]
 * Output: 2
 * Explanation:
 * One way to make the array alternating is by converting it to [1,2,1,2,1].
 * The number of operations required in this case is 2.
 * Note that the array cannot be converted to [2,2,2,2,2] because in this case
 * nums[0] == nums[1] which violates the conditions of an alternating array.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i] <= 10⁵
"""
from collections import Counter
from typing import List

class Solution:
    def minimumOperations(self, nums: List[int]) -> int:
        if (n := len(nums)) == 1:
            return 0
        odd_count = n // 2
        even_count = n - odd_count
        even = Counter(nums[::2]).most_common(2) + [(0, 0)]
        odd = Counter(nums[1::2]).most_common(2) + [(0, 0)]
        (even1, even1_count), (_, even2_count), *_ = even
        (odd1, odd1_count), (_, odd2_count), *_ = odd
        if even1 != odd1:
            return even_count - even1_count + odd_count - odd1_count
        else:
            return min(even_count - even1_count + odd_count - odd2_count,
                       even_count - even2_count + odd_count - odd1_count)

if __name__ == "__main__":
    tests = (
        ([3,1,3,2,4,3], 3),
        ([1,2,2,2,2], 2),
    )
    for nums, expected in tests:
        print(Solution().minimumOperations(nums) == expected)
