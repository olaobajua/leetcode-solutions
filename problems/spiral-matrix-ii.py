"""
 * 59. Spiral Matrix II (Medium)
 * Given a positive integer n, generate an n x n matrix filled with elements
 * from 1 to n² in spiral order.
 *
 * Example 1:
 * Input: n = 3
 * Output: [[1,2,3],
 *          [8,9,4],
 *          [7,6,5]]
 *
 * Example 2:
 * Input: n = 1
 * Output: [[1]]
 *
 * Constraints:
 *     1 <= n <= 20
"""
from typing import List

class Solution:
    def generateMatrix(self, n: int) -> List[List[int]]:
        ret = [[0] * n for _ in range(n)]
        row = col = dr = dc = 0
        dc += 1
        for i in range(1, n*n + 1):
            ret[row][col] = i
            if ret[(row+dr)%n][(col+dc)%n]:
                dr, dc = dc, -dr  # rotate 90° clockwise
            row += dr
            col += dc
        return ret

if __name__ == "__main__":
    tests = (
        (1, [[1]]),
        (3, [[1,2,3],
             [8,9,4],
             [7,6,5]]),
        (4, [[ 1, 2, 3,4],
             [12,13,14,5],
             [11,16,15,6],
             [10, 9, 8,7]]),
        (5, [[1, 2, 3, 4, 5],
             [16,17,18,19,6],
             [15,24,25,20,7],
             [14,23,22,21,8],
             [13,12,11,10,9]])
    )
    for n, expected in tests:
        print(Solution().generateMatrix(n) == expected)
