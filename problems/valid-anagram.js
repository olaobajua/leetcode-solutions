/**
 * 242. Valid Anagram [Easy]
 * Given two strings s and t, return true if t is an anagram of s, and false
 * otherwise.
 *
 * An Anagram is a word or phrase formed by rearranging the letters of a
 * different word or phrase, typically using all the original letters exactly
 * once.
 *
 * Example 1:
 * Input: s = "anagram", t = "nagaram"
 * Output: true
 * Example 2:
 * Input: s = "rat", t = "car"
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= s.length, t.length <= 5 * 10⁴
 *     ∙ s and t consist of lowercase English letters.
 *
 * Follow up: What if the inputs contain Unicode characters? How would you
 * adapt your solution to such a case?
 */

/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function(s, t) {
    const sc = Array(26).fill(0); st = Array(26).fill(0), a = 'a'.charCodeAt();
    for (const char of s)
        ++sc[char.charCodeAt()-a];
    for (const char of t)
        ++st[char.charCodeAt()-a];
    return sc.every((val, i) => val == st[i]);
};

const tests = [
    ["anagram", "nagaram", true],
    ["rat", "car", false],
    ["aa", "a", false],
];

for (const [s, t, expected] of tests) {
    console.log(isAnagram(s, t) == expected);
}
