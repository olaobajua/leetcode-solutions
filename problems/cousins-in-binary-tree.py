"""
 * 993. Cousins in Binary Tree (Easy)
 * Given the root of a binary tree with unique values and the values of two
 * different nodes of the tree x and y, return true if the nodes corresponding
 * to the values x and y in the tree are cousins, or false otherwise.
 *
 * Two nodes of a binary tree are cousins if they have the same depth with
 * different parents.
 *
 * Note that in a binary tree, the root node is at the depth 0, and children of
 * each depth k node are at the depth k + 1.
 *
 * Example 1:
 * Input: root = [1,2,3,4], x = 4, y = 3
 * Output: false
 *
 * Example 2:
 * Input: root = [1,2,3,null,4,null,5], x = 5, y = 4
 * Output: true
 *
 * Example 3:
 * Input: root = [1,2,3,null,4], x = 2, y = 3
 * Output: false
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [2, 100].
 *     1 <= Node.val <= 100
 *     Each node has a unique value.
 *     x != y
 *     x and y are exist in the tree.
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def isCousins(self, root: Optional[TreeNode], x: int, y: int) -> bool:
        return node_depth(root, x) == node_depth(root, y) and not is_siblings(root, x, y)

def node_depth(root, val):
    if root:
        if root.val == val:
            return 0
        elif (depth := node_depth(root.left, val)) is not None:
            return depth + 1
        elif (depth := node_depth(root.right, val)) is not None:
            return depth + 1

def is_siblings(root, x, y):
    if root:
        if root.left and root.right and {root.left.val, root.right.val} == {x, y}:
            return True
        if is_siblings(root.left, x, y) or is_siblings(root.right, x, y):
            return True
    return False

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3,4], 4, 3, False),
        ([1,2,3,None,4,None,5], 5, 4, True),
        ([1,2,3,None,4], 2, 3, False),
    )
    for nums, x, y, expected in tests:
        print(Solution().isCousins(build_tree(nums), x, y) == expected)
