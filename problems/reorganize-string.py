"""
 * 767. Reorganize String [Medium]
 * Given a string s, rearrange the characters of s so that any two adjacent
 * characters are not the same.
 *
 * Return any possible rearrangement of s or return "" if not possible.
 *
 * Example 1:
 * Input: s = "aab"
 * Output: "aba"
 * Example 2:
 * Input: s = "aaab"
 * Output: ""
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 500
 *     ∙ s consists of lowercase English letters.
"""
from collections import Counter

class Solution:
    def reorganizeString(self, s: str) -> str:
        char_count = Counter(s)
        sorted_chars = char_count.most_common()
        
        h = (len(s) + 1) // 2
        if sorted_chars[0][1] > h:
            return ""
        
        ret = [""] * len(s)
        
        i = 0
        for char, _ in sorted_chars:
            while char_count[char] > 0:
                ret[i] = char
                i += 2
                if i >= len(s):
                    i = 1
                char_count[char] -= 1
        
        return "".join(ret)

if __name__ == "__main__":
    tests = (
        ("aab", "aba"),
        ("aaab", ""),
        ("baaba", "ababa"),
        ("cxmwmmm", "mcmxmwm"),
    )
    for s, expected in tests:
        print(Solution().reorganizeString(s) == expected)
