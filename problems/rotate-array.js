/**
 * 189. Rotate Array (Medium)
 * Given an array, rotate the array to the right by k steps, where k is
 * non-negative.
 *
 * Example 1:
 * Input: nums = [1,2,3,4,5,6,7], k = 3
 * Output: [5,6,7,1,2,3,4]
 * Explanation:
 * rotate 1 steps to the right: [7,1,2,3,4,5,6]
 * rotate 2 steps to the right: [6,7,1,2,3,4,5]
 * rotate 3 steps to the right: [5,6,7,1,2,3,4]
 *
 * Example 2:
 * Input: nums = [-1,-100,3,99], k = 2
 * Output: [3,99,-1,-100]
 * Explanation:
 * rotate 1 steps to the right: [99,-1,-100,3]
 * rotate 2 steps to the right: [3,99,-1,-100]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ -2³¹ <= nums[i] <= 2³¹ - 1
 *     ∙ 0 <= k <= 10⁵
 *
 * Follow up:
 *     ∙ Try to come up with as many solutions as you can. There are at least
 *       three different ways to solve this problem.
 *     ∙ Could you do it in-place with O(1) extra space?
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var rotate = function(nums, k) {
    k = k % nums.length;
    for (const [i, x] of [...nums.slice(-k), ...nums.slice(0, -k)].entries()) {
        nums[i] = x;
    }
};

const tests = [
    [[1,2,3,4,5,6,7], 3, [5,6,7,1,2,3,4]],
    [[-1,-100,3,99], 2, [3,99,-1,-100]],
    [[1,2], 3, [2,1]],
];

for (const [nums, k, expected] of tests) {
    rotate(nums, k);
    console.log(nums.toString() == expected.toString());
}
