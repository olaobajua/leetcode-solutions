"""
 * 2096. Step-By-Step Directions From a Binary Tree Node to Another (Medium)
 * You are given the root of a binary tree with n nodes. Each node is uniquely
 * assigned a value from 1 to n. You are also given an integer startValue
 * representing the value of the start node s, and a different integer
 * destValue representing the value of the destination node t.
 *
 * Find the shortest path starting from node s and ending at node t. Generate
 * step-by-step directions of such path as a string consisting of only the
 * uppercase letters 'L', 'R', and 'U'. Each letter indicates a specific
 * direction:
 *     'L' means to go from a node to its left child node.
 *     'R' means to go from a node to its right child node.
 *     'U' means to go from a node to its parent node.
 *
 * Return the step-by-step directions of the shortest path from node s to
 * node t.
 *
 * Example 1:
 * Input: root = [5,1,2,3,null,6,4], startValue = 3, destValue = 6
 * Output: "UURL"
 * Explanation: The shortest path is: 3 → 1 → 5 → 2 → 6.
 *
 * Example 2:
 * Input: root = [2,1], startValue = 2, destValue = 1
 * Output: "L"
 * Explanation: The shortest path is: 2 → 1.
 *
 * Constraints:
 *     The number of nodes in the tree is n.
 *     2 <= n <= 10⁵
 *     1 <= Node.val <= n
 *     All the values in the tree are unique.
 *     1 <= startValue, destValue <= n
 *     startValue != destValue
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def getDirections(self, root: Optional[TreeNode], startValue: int, destValue: int) -> str:
        def find_path(root, val):
            if root:
                if root.val == val:
                    return [[root.val, 'N']]
                else:
                    if (l := find_path(root.left, val)):
                        l.append([root.val, 'L'])
                        return l
                    elif (r := find_path(root.right, val)):
                        r.append([root.val, 'R'])
                        return r

        spath = find_path(root, startValue)
        dpath = find_path(root, destValue)
        while len(spath) > 1 and len(dpath) > 1 and spath[-2][0] == dpath[-2][0]:
            spath.pop()
            dpath.pop()
        if spath:
            spath.pop(0)
        if dpath:
            dpath.pop(0)
        return 'U'*len(spath) + ''.join([d for _, d in reversed(dpath)])

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([5,1,2,3,None,6,4], 3, 6, "UURL"),
        ([2,1], 2, 1, "L"),
    )
    for nums, startValue, destValue, expected in tests:
        root = build_tree(nums)
        print(Solution().getDirections(root, startValue, destValue) ==
              expected, flush=True)
