"""
 * 442. Find All Duplicates in an Array (Medium)
 * Given an integer array nums of length n where all the integers of nums are
 * in the range [1, n] and each integer appears once or twice, return an array
 * of all the integers that appears twice.
 *
 * You must write an algorithm that runs in O(n) time and uses only constant
 * extra space.
 *
 * Example 1:
 * Input: nums = [4,3,2,7,8,2,3,1]
 * Output: [2,3]
 *
 * Example 2:
 * Input: nums = [1,1,2]
 * Output: [1]
 *
 * Example 3:
 * Input: nums = [1]
 * Output: []
 *
 * Constraints:
 *     n == nums.length
 *     1 <= n <= 10⁵
 *     1 <= nums[i] <= n
 *     Each element in nums appears once or twice.
"""
from typing import List

class Solution:
    def findDuplicates(self, nums: List[int]) -> List[int]:
        nums.sort()
        return [n1 for n1, n2 in zip(nums, nums[1:]) if n1 == n2]

if __name__ == "__main__":
    tests = (
        ([4,3,2,7,8,2,3,1], [2,3]),
        ([4,3,2,7,8,2,3,8], [2,3,8]),
        ([1,1,2], [1]),
        ([1], []),
    )
    for nums, expected in tests:
        print(Solution().findDuplicates(nums) == expected)
