/**
 * 1. Two Sum [Easy]
 * Given an array of integers nums and an integer target, return indices of the
 * two numbers such that they add up to target.
 *
 * You may assume that each input would have exactly one solution, and you may
 * not use the same element twice.
 *
 * You can return the answer in any order.
 *
 * Example 1:
 * Input: nums = [2,7,11,15], target = 9
 * Output: [0,1]
 * Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
 *
 * Example 2:
 * Input: nums = [3,2,4], target = 6
 * Output: [1,2]
 *
 * Example 3:
 * Input: nums = [3,3], target = 6
 * Output: [0,1]
 *
 * Constraints:
 *     ∙ 2 <= nums.length <= 10⁴
 *     ∙ -10⁹ <= nums[i] <= 10⁹
 *     ∙ -10⁹ <= target <= 10⁹
 *     ∙ Only one valid answer exists.
 *
 * Follow-up: Can you come up with an algorithm that is less than O(n²) time
 * complexity?
 */
#define SIZE 1 << 15

typedef struct HashMap {
    int key;
    int value;
    struct HashMap *next;
} HashMap;

int hash(long key) {
    return ((key * 1031237) & ((1 << 20) - 1)) >> 5;
}

HashMap* hashMapCreate() {
    return calloc(SIZE, sizeof(HashMap));
}

void hashMapPut(HashMap* obj, int key, int value) {
    int index = hash(key);
    for (HashMap *cur = obj[index].next; cur; cur = cur->next) {
        if (cur->key == key) {
            cur->value = value;
            return;
        }
    }
    HashMap *new_node = calloc(1, sizeof(HashMap));
    new_node->key = key;
    new_node->value = value;
    new_node->next = obj[index].next;
    obj[index].next = new_node;
}

int hashMapGet(HashMap* obj, int key) {
    for (HashMap *cur = obj[hash(key)].next; cur; cur = cur->next) {
        if (cur->key == key) {
            return cur->value;
        }
    }
    return -1;
}

void hashMapRemove(HashMap* obj, int key) {
    for (HashMap **cur = &obj[hash(key)].next; *cur; cur = &(*cur)->next) {
        if ((*cur)->key == key) {
            HashMap *del = *cur;
            *cur = (*cur)->next;
            free(del);
            return;
        }
    }
}

void hashMapFree(HashMap* obj) {
    for (int i = 0; i < SIZE; ++i) {
        HashMap *cur = obj[i].next;
        while (cur) {
            HashMap *del = cur;
            cur = cur->next;
            free(del);
        }
    }
    free(obj);
}

int* twoSum(int *nums, int numsSize, int target, int *returnSize) {
    static int ret[2] = {0, 0};
    *returnSize = 2;
    HashMap *indices = hashMapCreate();
    for (int i = 0; i < numsSize; ++i) {
        if (hashMapGet(indices, target - nums[i]) != -1) {
            ret[0] = hashMapGet(indices, target - nums[i]);
            ret[1] = i;
            break;
        }
        hashMapPut(indices, nums[i], i);
    }
    return ret;
}
