/**
 * 104. Maximum Depth of Binary Tree (Easy)
 * Given the root of a binary tree, return its maximum depth.
 *
 * A binary tree's maximum depth is the number of nodes along the longest path
 * from the root node down to the farthest leaf node.
 *
 * Example 1:
 * Input: root = [3,9,20,null,null,15,7]
 * Output: 3
 *
 * Example 2:
 * Input: root = [1,null,2]
 * Output: 2
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 10⁴].
 *     ∙ -100 <= Node.val <= 100
 */

// Definition for a binary tree node.
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

int height(struct TreeNode* root) {
    if (root == NULL) {
        return 0;
    }
    return 1 + fmax(height(root->left), height(root->right));
}

int maxDepth(struct TreeNode* root) {
    return height(root);
}
