"""
 * 902. Numbers At Most N Given Digit Set (Hard)
 * Given an array of digits which is sorted in non-decreasing order. You can
 * write numbers using each digits[i] as many times as we want. For example, if
 * digits = ['1','3','5'], we may write numbers such as '13', '551', and
 * '1351315'.
 *
 * Return the number of positive integers that can be generated that are less
 * than or equal to a given integer n.
 *
 * Example 1:
 * Input: digits = ["1","3","5","7"], n = 100
 * Output: 20
 * Explanation:
 * The 20 numbers that can be written are:
 * 1, 3, 5, 7, 11, 13, 15, 17, 31, 33, 35, 37, 51, 53, 55, 57, 71, 73, 75, 77.
 *
 * Example 2:
 * Input: digits = ["1","4","9"], n = 1000000000
 * Output: 29523
 * Explanation:
 * We can write 3 one digit numbers, 9 two digit numbers, 27 three digit
 * numbers, 81 four digit numbers, 243 five digit numbers, 729 six digit
 * numbers, 2187 seven digit numbers, 6561 eight digit numbers, and 19683 nine
 * digit numbers. In total, this is 29523 integers that can be written using
 * the digits array.
 *
 * Example 3:
 * Input: digits = ["7"], n = 8
 * Output: 1
 *
 * Constraints:
 *     1 <= digits.length <= 9
 *     digits[i].length == 1
 *     digits[i] is a digit from '1' to '9'.
 *     All the values in digits are unique.
 *     digits is sorted in non-decreasing order.
 *     1 <= n <= 10⁹
"""
from bisect import bisect_left
from typing import List

class Solution:
    def atMostNGivenDigitSet(self, digits: List[str], n: int) -> int:
        n = str(n)
        counter = sum(len(digits)**i for i in range(1, len(n)))
        for i, d in enumerate(n):
            counter += bisect_left(digits, d) * len(digits) ** (len(n) - i - 1)
            if d not in digits:
                break
        else:
            counter += 1
        return counter

if __name__ == "__main__":
    tests = (
        (["7"], 8, 1),
        (["1"], 834, 3),                # 1,11,111
        (["1","2"], 834, 14),           # 1,2,11,12,21,22,111,112,121,122,211,212,221,222
        (["1","5"], 834, 14),           # 1,5,11,15,51,55,111,115,151,155,511,515,551,555
        (["1","3","5","7"], 834, 84),
        (["1","3","5","7"], 30, 8),     # 1,3,5,7,11,13,15,17
        (["1","3","5","7"], 33, 10),    # 1,3,5,7,11,13,15,17,31,33
        (["1","3","5","7"], 76, 19),    # 1,3,5,7,11,13,15,17,31,33,35,37,51,53,55,57,71,73,75
        (["1","3","5","7"], 100, 20),   # 1,3,5,7,11,13,15,17,31,33,35,37,51,53,55,57,71,73,75,77
        (["1","3","5","7"], 103, 20),   # 1,3,5,7,11,13,15,17,31,33,35,37,51,53,55,57,71,73,75,77
        (["1","3","5","7"], 113, 22),   # 1,3,5,7,11,13,15,17,31,33,35,37,51,53,55,57,71,73,75,77,111,113
        (["1","3","5","7"], 117, 24),   # 1,3,5,7,11,13,15,17,31,33,35,37,51,53,55,57,71,73,75,77,111,113,115,117
        (["1","3","5","7"], 130, 24),   # 1,3,5,7,11,13,15,17,31,33,35,37,51,53,55,57,71,73,75,77,111,113,115,117
        (["1","3","5","7"], 1000, 84),
        (["3","4","8"], 4, 2),
        (["3","4","8"], 100, 12),
        (["3","4","8"], 280, 12),
        (["1","2","4","5","6","9"], 7440, 1338),
        (["3","5","6","7","8","9"], 917791, 48210),
        (["1","4","9"], 1000000000, 29523),
        (["1","2","4","5","6","7","8","9"], 674990, 190528),
        (["1","2","3","4","5","6","7","8","9"], 539756, 322107),
        (["3","4","5","6"], 893148953, 349524),
        (["1","3","5","6","7","8"], 62774961, 1222386),
    )
    for digits, n, expected in tests:
        print(Solution().atMostNGivenDigitSet(digits, n) == expected)
