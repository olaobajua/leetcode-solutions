"""
 * 1074. Number of Submatrices That Sum to Target [Hard]
 * Given a matrix and a target, return the number of non-empty submatrices that
 * sum to target.
 *
 * A submatrix x1, y1, x2, y2 is the set of all cells matrix[x][y] with x1 <= x
 * <= x2 and y1 <= y <= y2.
 *
 * Two submatrices (x1, y1, x2, y2) and (x1', y1', x2', y2') are different if
 * they have some coordinate that is different: for example, if x1 != x1'.
 *
 * Example 1:
 * Input: matrix = [[0,1,0],[1,1,1],[0,1,0]], target = 0
 * Output: 4
 * Explanation: The four 1x1 submatrices that only contain 0.
 *
 * Example 2:
 * Input: matrix = [[1,-1],[-1,1]], target = 0
 * Output: 5
 * Explanation: The two 1x2 submatrices, plus the two 2x1 submatrices, plus the
 * 2x2 submatrix.
 *
 * Example 3:
 * Input: matrix = [[904]], target = 0
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= matrix.length <= 100
 *     ∙ 1 <= matrix[0].length <= 100
 *     ∙ -1000 <= matrix[i] <= 1000
 *     ∙ -10⁸ <= target <= 10⁸
"""
from collections import Counter
from itertools import combinations
import numpy as np
from typing import List

class Solution:
    def numSubmatrixSumTarget(self, matrix: List[List[int]], target: int) -> int:
        height = len(matrix)
        width = len(matrix[0])
        matrix = np.vstack((np.zeros(width, dtype=int), matrix)).cumsum(axis=0)
        #           0 0 0   add row of zeroes
        # 0 1 0     0 1 0   cumsum along axis 0
        # 1 1 1  -> 1 2 1     ↓
        # 0 1 0     1 3 1
        ret = 0
        for row1, row2 in combinations(range(height + 1), 2):
            sum_count = Counter([0])
            cur_sum = 0
            for col in range(width):
                # accumulate sum of elements between row1 and row2 in col
                cur_sum += matrix[row2][col] - matrix[row1][col]
                # if cur_sum - sum = target, then sum = cur_sum - target
                ret += sum_count[cur_sum-target]
                sum_count[cur_sum] += 1
        return ret

if __name__ == "__main__":
    tests = (
        ([[0,1,0],[1,1,1],[0,1,0]], 0, 4),
        ([[1,-1],[-1,1]], 0, 5),
        ([[904]], 0, 0),
    )
    for matrix, target, expected in tests:
        print(Solution().numSubmatrixSumTarget(matrix, target) == expected)
