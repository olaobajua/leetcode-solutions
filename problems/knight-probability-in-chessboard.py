"""
 * 688. Knight Probability in Chessboard [Medium]
 * On an n x n chessboard, a knight starts at the cell (row, column) and
 * attempts to make exactly k moves. The rows and columns are 0-indexed, so the
 * top-left cell is (0, 0), and the bottom-right cell is (n - 1, n - 1).
 *
 * A chess knight has eight possible moves it can make, as illustrated below.
 * Each move is two cells in a cardinal direction, then one cell in an
 * orthogonal direction.
 *
 * Each time the knight is to move, it chooses one of eight possible moves
 * uniformly at random (even if the piece would go off the chessboard) and
 * moves there.
 *
 * The knight continues moving until it has made exactly k moves or has moved
 * off the chessboard.
 *
 * Return the probability that the knight remains on the board after it has
 * stopped moving.
 *
 * Example 1:
 * Input: n = 3, k = 2, row = 0, column = 0
 * Output: 0.06250
 * Explanation: There are two moves (to (1,2), (2,1)) that will keep the
 * knight on the board.
 * From each of those positions, there are also two moves that will keep the
 * knight on the board.
 * The total probability the knight stays on the board is 0.0625.
 *
 * Example 2:
 * Input: n = 1, k = 0, row = 0, column = 0
 * Output: 1.00000
 *
 * Constraints:
 *     ∙ 1 <= n <= 25
 *     ∙ 0 <= k <= 100
 *     ∙ 0 <= row, column <= n - 1
"""
from functools import cache

class Solution:
    def knightProbability(self, n: int, k: int, row: int, column: int) -> float:
        @cache
        def dfs(r, c, moves_left):
            if 0 <= r < n and 0 <= c < n:
                if moves_left == 0:
                    return 1

                ret = 0
                moves_left -= 1
                for nr, nc in ((r+2, c+1), (r+2, c-1), (r-2, c+1), (r-2, c-1),
                               (r+1, c+2), (r+1, c-2), (r-1, c+2), (r-1, c-2)):
                    ret += dfs(nr, nc, moves_left)

                return ret

            return 0

        on_board = dfs(row, column, k)
        return on_board / 8**k

if __name__ == "__main__":
    tests = (
        (3, 2, 0, 0, 0.06250),
        (1, 0, 0, 0, 1.00000),
    )
    for n, k, row, column, expected in tests:
        print(Solution().knightProbability(n, k, row, column) == expected)
