"""
 * 546. Remove Boxes (Hard)
 * You are given several boxes with different colors represented by different
 * positive numbers.
 *
 * You may experience several rounds to remove boxes until there is no box
 * left. Each time you can choose some continuous boxes with the same color
 * (i.e., composed of k boxes, k >= 1), remove them and get k * k points.
 *
 * Return the maximum points you can get.
 *
 * Example 1:
 * Input: boxes = [1,3,2,2,2,3,4,3,1]
 * Output: 23
 * Explanation:
 * [1, 3, 2, 2, 2, 3, 4, 3, 1]
 * ----> [1, 3, 3, 4, 3, 1] (3*3=9 points)
 * ----> [1, 3, 3, 3, 1] (1*1=1 points)
 * ----> [1, 1] (3*3=9 points)
 * ----> [] (2*2=4 points)
 *
 * Example 2:
 * Input: boxes = [1,1,1]
 * Output: 9
 *
 * Example 3:
 * Input: boxes = [1]
 * Output: 1
 *
 * Constraints:
 *     1 <= boxes.length <= 100
 *     1 <= boxes[i] <= 100
"""
from typing import List
from functools import cache
import sys

def find_border(boxes, start, end):
    border = start + 1
    while border < end and boxes[start] == boxes[border]:
        border += 1
    return border

@cache
def max_score(boxes, same_before, start, end):
    if start < end:
        same_after = find_border(boxes, start, end) - start
        same_before += same_after
        score = max_score(boxes, 0, start + same_after, end) + same_before ** 2
        for i in range(start + same_after + 1, end):
            if boxes[i] == boxes[start]:
                score = max(score,
                            max_score(boxes, 0, start + same_after, i) + 
                            max_score(boxes, same_before, i, end))
        return score
    else:
        return 0

class Solution:
    def removeBoxes(self, boxes: List[int]) -> int:
        return max_score(tuple(boxes), 0, 0, len(boxes))


if __name__ == "__main__":
    sys.setrecursionlimit(1500)
    tests = (
        ([1,3,2,2,2,3,4,3,1], 23),
        ([1,1,1], 9),
        ([1], 1),
        ([1,1,2,1], 10),
    )
    for boxes, expected in tests:
        print(Solution().removeBoxes(boxes) == expected)
