"""
 * 1443. Minimum Time to Collect All Apples in a Tree [Medium]
 * Given an undirected tree consisting of n vertices numbered from 0 to n-1,
 * which has some apples in their vertices. You spend 1 second to walk over one
 * edge of the tree. Return the minimum time in seconds you have to spend to
 * collect all apples in the tree, starting at vertex 0 and coming back to this
 * vertex.
 *
 * The edges of the undirected tree are given in the array edges, where
 * edges[i] = [aᵢ, bᵢ] means that exists an edge connecting the vertices aᵢ and
 * bᵢ. Additionally, there is a boolean array hasApple, where hasApple[i] =
 * true means that vertex i has an apple; otherwise, it does not have any
 * apple.
 *
 * Example 1:
 *      0
 *   1     2*
 * 4* 5*  3 6
 * Input: n = 7,
 *        edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]],
 *        hasApple = [false,false,true,false,true,true,false]
 * Output: 8
 *
 * Example 2:
 *      0
 *   1     2*
 * 4  5*  3 6
 * Input: n = 7,
 *        edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]],
 *        hasApple = [false,false,true,false,false,true,false]
 * Output: 6
 *
 * Example 3:
 *     0
 *  1     2
 * 4 5   3 6
 * Input: n = 7,
 *        edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]],
 *        hasApple = [false,false,false,false,false,false,false]
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁵
 *     ∙ edges.length == n - 1
 *     ∙ edges[i].length == 2
 *     ∙ 0 <= aᵢ < bᵢ <= n - 1
 *     ∙ fromᵢ < toᵢ
 *     ∙ hasApple.length == n
"""
from collections import defaultdict
from typing import List

class Solution:
    def minTime(self, n: int, edges: List[List[int]], hasApple: List[bool]) -> int:
        def dfs(i):
            ret = 0
            for child in graph[i].copy():
                graph[i].remove(child)
                graph[child].remove(i)
                ret += dfs(child)
                graph[i].add(child)
                graph[child].add(i)
            if i and (ret or hasApple[i]):
                ret += 2
            return ret

        graph = defaultdict(set)
        for a, b in edges:
            graph[a].add(b)
            graph[b].add(a)
        return dfs(0)

if __name__ == "__main__":
    tests = (
        (7, [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]],
         [False,False,True,False,True,True,False], 8),
        (7, [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]],
         [False,False,True,False,False,True,False], 6),
        (7, [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]],
         [False,False,False,False,False,False,False], 0),
        (40, [[0,1],[0,2],[0,3],[3,4],[1,5],[5,6],[1,7],[2,8],[1,9],[6,10],[6,11],[8,12],[4,13],[8,14],[4,15],[15,16],[10,17],[3,18],[6,19],[10,20],[1,21],[0,22],[0,23],[9,24],[23,25],[19,26],[10,27],[14,28],[12,29],[10,30],[12,31],[4,32],[4,33],[20,34],[29,35],[7,36],[16,37],[13,38],[34,39]],
         [True,True,True,False,False,True,False,False,True,False,True,False,True,False,True,False,True,True,False,True,False,True,True,False,True,False,False,False,False,False,False,True,False,True,False,False,False,False,True,True], 50),
         (7, [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]],
          [True,False,False,False,False,False,False], 0),
    )
    for n, edges, hasApple, expected in tests:
        print(Solution().minTime(n, edges, hasApple) == expected)
