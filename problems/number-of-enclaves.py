"""
 * 1020. Number of Enclaves [Medium]
 * You are given an m x n binary matrix grid, where 0 represents a sea cell
 * and 1 represents a land cell.
 *
 * A move consists of walking from one land cell to another adjacent
 * (4-directionally) land cell or walking off the boundary of the grid.
 *
 * Return the number of land cells in grid for which we cannot walk off the
 * boundary of the grid in any number of moves.
 *
 * Example 1:
 * Input: grid = [[0,0,0,0],
 *                [1,0,1,0],
 *                [0,1,1,0],
 *                [0,0,0,0]]
 * Output: 3
 * Explanation: There are three 1s that are enclosed by 0s, and one 1 that is
 * not enclosed because its on the boundary.
 *
 * Example 2:
 * Input: grid = [[0,1,1,0],
 *                [0,0,1,0],
 *                [0,0,1,0],
 *                [0,0,0,0]]
 * Output: 0
 * Explanation: All 1s are either on the boundary or can reach the boundary.
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 500
 *     ∙ grid[i][j] is either 0 or 1.
"""
from typing import List

class Solution:
    def numEnclaves(self, grid: List[List[int]]) -> int:
        def flood_fill(r, c):
            grid[r][c] = 0
            for nr, nc in (r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1):
                if m > nr >= 0 <= nc < n and grid[nr][nc]:
                    flood_fill(nr, nc)

        m = len(grid)
        n = len(grid[0])
        for row in range(m):
            if grid[row][0] == 1:
                flood_fill(row, 0)
            if grid[row][n-1] == 1:
                flood_fill(row, n - 1)
        for col in range(n):
            if grid[0][col] == 1:
                flood_fill(0, col)
            if grid[m-1][col] == 1:
                flood_fill(m - 1, col)
        return sum(map(sum, grid))

if __name__ == "__main__":
    tests = (
        ([[0,0,0,0],
          [1,0,1,0],
          [0,1,1,0],
          [0,0,0,0]], 3),
        ([[0,1,1,0],
          [0,0,1,0],
          [0,0,1,0],
          [0,0,0,0]], 0),
        ([[0,0,0,1,1,1,0,1,0,0],
          [1,1,0,0,0,1,0,1,1,1],
          [0,0,0,1,1,1,0,1,0,0],
          [0,1,1,0,0,0,1,0,1,0],
          [0,1,1,1,1,1,0,0,1,0],
          [0,0,1,0,1,1,1,1,0,1],
          [0,1,1,0,0,0,1,1,1,1],
          [0,0,1,0,0,1,0,1,0,1],
          [1,0,1,0,1,1,0,0,0,0],
          [0,0,0,0,1,1,0,0,0,1]], 3),
    )
    for grid, expected in tests:
        print(Solution().numEnclaves(grid) == expected)
