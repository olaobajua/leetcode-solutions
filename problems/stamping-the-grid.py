"""
 * 2132. Stamping the Grid (Hard)
 * You are given an m x n binary matrix grid where each cell is either 0
 * (empty) or 1 (occupied).
 *
 * You are then given stamps of size stampHeight x stampWidth. We want to fit
 * the stamps such that they follow the given restrictions and requirements:
 *     ∙ Cover all the empty cells.
 *     ∙ Do not cover any of the occupied cells.
 *     ∙ We can put as many stamps as we want.
 *     ∙ Stamps can overlap with each other.
 *     ∙ Stamps are not allowed to be rotated.
 *     ∙ Stamps must stay completely inside the grid.
 *
 * Return true if it is possible to fit the stamps while following the given
 * restrictions and requirements. Otherwise, return false.
 *
 * Example 1:
 * Input: grid = [[1,0,0,0],
 *                [1,0,0,0],
 *                [1,0,0,0],
 *                [1,0,0,0],
 *                [1,0,0,0]],
 *        stampHeight = 4, stampWidth = 3
 * Output: true
 * Explanation: We have two overlapping stamps that are able to cover all the
 * empty cells.
 *
 * Example 2:
 * Input: grid = [[1,0,0,0],
 *                [0,1,0,0],
 *                [0,0,1,0],
 *                [0,0,0,1]],
 *        stampHeight = 2, stampWidth = 2
 * Output: false
 * Explanation: There is no way to fit the stamps onto all the empty cells
 * without the stamps going outside the grid.
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[r].length
 *     ∙ 1 <= m, n <= 10⁵
 *     ∙ 1 <= m * n <= 2 * 10⁵
 *     ∙ grid[r][c] is either 0 or 1.
 *     ∙ 1 <= stampHeight, stampWidth <= 10⁵
"""
from itertools import product
from typing import List

class Solution:
    def possibleToStamp(self, grid: List[List[int]], stampHeight: int, stampWidth: int) -> bool:
        def acc_sum_2d(grid):
            s = [[0] * (n+1) for _ in range(m+1)]
            for c, r in product(range(n), range(m)):
                s[r+1][c+1] = s[r+1][c] + s[r][c+1] - s[r][c] + grid[r][c]
            return s

        def sum_region(r1, c1, r2, c2):
            return s[r2+1][c2+1] - s[r1][c2+1] - s[r2+1][c1] + s[r1][c1]

        m = len(grid)
        n = len(grid[0])
        s = acc_sum_2d(grid)

        diff = [[0] * (n+1) for _ in range(m+1)]
        for c in range(n - stampWidth + 1):
            for r in range(m - stampHeight + 1):
                if sum_region(r, c, r+stampHeight-1, c+stampWidth-1) == 0:
                    diff[r][c] += 1
                    diff[r][c+stampWidth] -= 1
                    diff[r+stampHeight][c] -= 1
                    diff[r+stampHeight][c+stampWidth] += 1

        can_stamp = acc_sum_2d(diff)
        for c, r in product(range(n), range(m)):
            if can_stamp[r+1][c+1] == 0 and grid[r][c] == 0:
                return False

        return True

if __name__ == "__main__":
    tests = (
        ([[1,0],
          [0,0]], 2, 2, False),
        ([[1,0,0,0],
          [1,0,0,0],
          [1,0,0,0],
          [1,0,0,0],
          [1,0,0,0]], 4, 3, True),
        ([[1,0,0,0],
          [0,1,0,0],
          [0,0,1,0],
          [0,0,0,1]], 2, 2, False),
        ([[0,0,1,1],
          [0,0,1,1]], 2, 1, True),
        ([[1,1,1],
          [1,0,1],
          [1,1,1]], 1, 2, False),
        ([[1,1,1],
          [1,0,1],
          [1,1,1]], 2, 1, False),
        ([[0,0,0,0],
          [0,0,0,0],
          [0,0,0,0],
          [0,0,0,0]], 5, 5, False),
        ([[1,1,1],
          [1,1,1],
          [1,1,1],
          [1,1,1],
          [1,1,1],
          [1,1,1]], 9, 4, True),
        ([[0,0,0,0,0],
          [0,0,0,0,0],
          [0,0,1,0,0],
          [0,0,0,0,1],
          [0,0,0,1,1]], 2, 2, False),
        ([[0,0,0,0,0],
          [0,0,0,0,0],
          [0,0,1,0,0],
          [0,0,0,0,1],
          [0,0,0,0,1]], 2, 2, True),
        ([[0,0,0,0,0],
          [0,0,0,0,0],
          [0,0,0,0,0],
          [1,0,0,0,0],
          [1,1,0,0,0]], 2, 2, True),
    )
    for grid, stampHeight, stampWidth, expected in tests:
        print(Solution().possibleToStamp(grid, stampHeight, stampWidth) ==
              expected)
