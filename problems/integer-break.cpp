/**
 * 343. Integer Break [Medium]
 * Given an integer n, break it into the sum of k positive integers, where
 * k >= 2, and maximize the product of those integers.
 *
 * Return the maximum product you can get.
 *
 * Example 1:
 * Input: n = 2
 * Output: 1
 * Explanation: 2 = 1 + 1, 1 × 1 = 1.
 *
 * Example 2:
 * Input: n = 10
 * Output: 36
 * Explanation: 10 = 3 + 3 + 4, 3 × 3 × 4 = 36.
 *
 * Constraints:
 *     ∙ 2 <= n <= 58
 */
#include <iostream>
#include <vector>
#include <algorithm>

class Solution {
public:
    int memo[58];

    int integerBreak(int n) {
        if (n == 1) {
            return 1;
        }

        if (memo[n] != 0) {
            return memo[n];
        }

        int ret = 1;
        for (int x = 1; x < n; ++x) {
            ret = std::max(ret, std::max(x * (n - x), x * integerBreak(n - x)));
        }

        memo[n] = ret;
        return ret;
    }
};

int main() {
    Solution solution;
    int n = 10; // You can replace 10 with any desired value
    std::cout << "Result: " << solution.integerBreak(n) << std::endl;
    return 0;
}

