/**
 * 2477. Minimum Fuel Cost to Report to the Capital [Medium]
 * There is a tree (i.e., a connected, undirected graph with no cycles)
 * structure country network consisting of n cities numbered from 0 to n - 1
 * and exactly n - 1 roads. The capital city is city 0. You are given a 2D
 * integer array roads where roads[i] = [aᵢ, bᵢ] denotes that there exists a
 * bidirectional road connecting cities aᵢ and bᵢ.
 *
 * There is a meeting for the representatives of each city. The meeting is in
 * the capital city.
 *
 * There is a car in each city. You are given an integer seats that indicates
 * the number of seats in each car.
 *
 * A representative can use the car in their city to travel or change the car
 * and ride with another representative. The cost of traveling between two
 * cities is one liter of fuel.
 *
 * Return the minimum number of liters of fuel to reach the capital city.
 *
 * Example 1:
 * Input: roads = [[0,1],[0,2],[0,3]], seats = 5
 * Output: 3
 * Explanation:
 * - Representative₁ goes directly to the capital with 1 liter of fuel.
 * - Representative₂ goes directly to the capital with 1 liter of fuel.
 * - Representative₃ goes directly to the capital with 1 liter of fuel.
 * It costs 3 liters of fuel at minimum.
 * It can be proven that 3 is the minimum number of liters of fuel needed.
 *
 * Example 2:
 * Input: roads = [[3,1],[3,2],[1,0],[0,4],[0,5],[4,6]], seats = 2
 * Output: 7
 * Explanation:
 * - Representative₂ goes directly to city 3 with 1 liter of fuel.
 * - Representative₂ and representative₃ go together to city 1 with 1 liter of
 * fuel.
 * - Representative₂ and representative₃ go together to the capital with 1
 * liter of fuel.
 * - Representative₁ goes directly to the capital with 1 liter of fuel.
 * - Representative₅ goes directly to the capital with 1 liter of fuel.
 * - Representative₆ goes directly to city 4 with 1 liter of fuel.
 * - Representative₄ and representative₆ go together to the capital with 1
 * liter of fuel.
 * It costs 7 liters of fuel at minimum.
 * It can be proven that 7 is the minimum number of liters of fuel needed.
 *
 * Example 3:
 * Input: roads = [], seats = 1
 * Output: 0
 * Explanation: No representatives need to travel to the capital city.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁵
 *     ∙ roads.length == n - 1
 *     ∙ roads[i].length == 2
 *     ∙ 0 <= aᵢ, bᵢ < n
 *     ∙ aᵢ != bᵢ
 *     ∙ roads represents a valid tree.
 *     ∙ 1 <= seats <= 10⁵
 */
class Solution {
    long fuel = 0;
    int seats;
    Map<Integer, List<Integer>> cities = new HashMap();
    public long minimumFuelCost(int[][] roads, int seats) {
        this.seats = seats;
        cities.put(0, new ArrayList());
        for (int[] r : roads) {
            cities.putIfAbsent(r[0], new ArrayList());
            cities.putIfAbsent(r[1], new ArrayList());
            cities.get(r[0]).add(r[1]);
            cities.get(r[1]).add(r[0]);
        }
        dfs(0, 0);
        return fuel;
    }

    private int dfs(int city, int distance) {
        int passengers = 1;
        for (int neib : cities.remove(city)) {
            if (cities.containsKey(neib)) {
                int new_passengers = dfs(neib, distance + 1);
                passengers += new_passengers;
                fuel += new_passengers > 0 ? 1 : 0;
            }
        }
        if (city > 0) {
            fuel += distance * (passengers / seats);
            passengers %= seats;
        }
        return passengers;
    }
}
