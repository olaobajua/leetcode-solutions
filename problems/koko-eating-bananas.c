/**
 * 875. Koko Eating Bananas (Medium)
 * Koko loves to eat bananas. There are n piles of bananas, the iᵗʰ pile has
 * piles[i] bananas. The guards have gone and will come back in h hours.
 *
 * Koko can decide her bananas-per-hour eating speed of k. Each hour, she
 * chooses some pile of bananas and eats k bananas from that pile. If the pile
 * has less than k bananas, she eats all of them instead and will not eat any
 * more bananas during this hour.
 *
 * Koko likes to eat slowly but still wants to finish eating all the bananas
 * before the guards return.
 *
 * Return the minimum integer k such that she can eat all the bananas within h
 * hours.
 *
 * Example 1:
 * Input: piles = [3,6,7,11], h = 8
 * Output: 4
 *
 * Example 2:
 * Input: piles = [30,11,23,4,20], h = 5
 * Output: 30
 *
 * Example 3:
 * Input: piles = [30,11,23,4,20], h = 6
 * Output: 23
 *
 * Constraints:
 *     ∙ 1 <= piles.length <= 10⁴
 *     ∙ piles.length <= h <= 10⁹
 *     ∙ 1 <= piles[i] <= 10⁹
 */
int max(int iterable[], int iterableSize) {
    int ret = 0;
    for (int i = 0; i < iterableSize; ++i) {
        if (ret < iterable[i]) {
            ret = iterable[i];
        }
    }
    return ret;
}
long long int sum(int iterable[], int iterableSize) {
    long long int ret = 0LL;
    for (int i = 0; i < iterableSize; ++i) {
        ret += iterable[i];
    }
    return ret;
}
bool canEatAllBananas(int piles[], int pilesSize, int speed, int h) {
    long long int spent = 0;
    for (int i = 0; i < pilesSize; ++i) {
        spent += ceil((double)piles[i] / speed);
    }
    return spent <= h;
}
int minEatingSpeed(int *piles, int pilesSize, int h) {
    if (h == pilesSize) {
        return max(piles, pilesSize);
    }
    int lo = ceil((double)sum(piles, pilesSize) / h);
    int hi = max(piles, pilesSize);
    int mid = 0;
    while (lo <= hi) {
        mid = floor((double)(lo + hi) / 2);
        if (canEatAllBananas(piles, pilesSize, mid, h)) {
            hi = mid - 1;
        } else {
            lo = mid + 1;
        }
    }
    return lo;
}
