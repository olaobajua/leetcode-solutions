/**
 * 1305. All Elements in Two Binary Search Trees (Medium)
 * Given two binary search trees root1 and root2, return a list containing all
 * the integers from both trees sorted in ascending order.
 *
 * Example 1:
 * Input: root1 = [2,1,4], root2 = [1,0,3]
 * Output: [0,1,1,2,3,4]
 *
 * Example 2:
 * Input: root1 = [1,null,8], root2 = [8,1]
 * Output: [1,1,8,8]
 *
 * Constraints:
 *     ∙ The number of nodes in each tree is in the range [0, 5000].
 *     ∙ -10⁵ <= Node.val <= 10⁵
 */

// Definition for a binary tree node.
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

void inorder(struct TreeNode *root, int lst[], int *cur) {
    if (root) {
        inorder(root->left, lst, cur);
        lst[(*cur)++] = root->val;
        inorder(root->right, lst, cur);
    }
}

int * getAllElements(struct TreeNode *root1, struct TreeNode *root2, int *returnSize) {
    static int lst1[5000];
    static int lst2[5000];
    int s1 = 0, s2 = 0;
    inorder(root1, lst1, &s1);
    inorder(root2, lst2, &s2);
    int i1 = 0, i2 = 0;
    static int ret[10000];
    *returnSize = 0;
    while (i1 < s1 && i2 < s2) {
        if (lst1[i1] < lst2[i2]) {
            ret[(*returnSize)++] = lst1[i1++];
        } else {
            ret[(*returnSize)++] = lst2[i2++];
        }
    }
    while (i1 < s1) {
        ret[(*returnSize)++] = lst1[i1++];
    }
    while (i2 < s2) {
        ret[(*returnSize)++] = lst2[i2++];
    }
    return ret;
}
