/*
 * 279. Perfect Squares [Medium]
 * Given an integer n, return the least number of perfect square numbers that
 * sum to n.
 *
 * A perfect square is an integer that is the square of an integer; in other
 * words, it is the product of some integer with itself. For example, 1, 4, 9,
 * and 16 are perfect squares while 3 and 11 are not.
 *
 * Example 1:
 * Input: n = 12
 * Output: 3
 * Explanation: 12 = 4 + 4 + 4.
 *
 * Example 2:
 * Input: n = 13
 * Output: 2
 * Explanation: 13 = 4 + 9.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁴
 */

/**
 * @param {number} n
 * @return {number}
 */
var numSquares = function(n) {
    if (is_square(n)) {
        return 1;
    }
    let nums = [n];
    let minSquares = 0;
    while (nums.length) {
        minSquares += 1;
        for (let i = nums.length; i; --i) {
            let num = nums.shift();
            for (let j = Math.floor(num**0.5) + 1; j; --j) {
                let nextNum = num - j**2;
                if (is_square(nextNum)) {
                    return minSquares + 1;
                }
                nums.push(nextNum);
            }
        }
    }
};

function is_square(x) {
    return x**0.5 % 1 === 0;
}


tests = [
    [1, 1],
    [12, 3],
    [13, 2],
    [4869, 2],
    [8609, 2],
    [8935, 4],
];
for (let [n, expected] of tests) {
    console.log(numSquares(n) == expected);
}
