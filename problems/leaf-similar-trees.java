/**
 * 872. Leaf-Similar Trees [Easy]
 * Consider all the leaves of a binary tree, from left to right order, the
 * values of those leaves form a leaf value sequence.
 *
 * For example, in the given tree above, the leaf value sequence is (6, 7, 4,
 * 9, 8).
 *       3
 *   5       1
 * 6   2   9   8
 *    7 4
 *
 * Two binary trees are considered leaf-similar if their leaf value sequence
 * is the same.
 *
 * Return true if and only if the two given trees with head nodes root1 and
 * root2 are leaf-similar.
 *
 * Example 1:
 * Input: root1 = [3,5,1,6,2,9,8,null,null,7,4], root2 =
 * [3,5,1,6,7,4,2,null,null,null,null,null,null,9,8]
 * Output: true
 *
 * Example 2:
 * Input: root1 = [1,2,3], root2 = [1,3,2]
 * Output: false
 *
 * Constraints:
 *     ∙ The number of nodes in each tree will be in the range [1, 200].
 *     ∙ Both of the given trees will have values in the range [0, 200].
 */

// Definition for a binary tree node.
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class Solution {
    public boolean leafSimilar(TreeNode root1, TreeNode root2) {
        List<Integer> seq1 = new ArrayList();
        List<Integer> seq2 = new ArrayList();
        leafSequence(root1, seq1);
        leafSequence(root2, seq2);
        return seq1.equals(seq2);
    }

    private void leafSequence(TreeNode root, List out) {
        if (root.left == null && root.right == null)
            out.add(root.val);
        if (root.left != null)
            leafSequence(root.left, out);
        if (root.right != null)
            leafSequence(root.right, out);
    }
}
