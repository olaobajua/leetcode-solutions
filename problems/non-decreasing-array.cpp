/**
 * 665. Non-decreasing Array (Medium)
 * Given an array nums with n integers, your task is to check if it could
 * become non-decreasing by modifying at most one element.
 *
 * We define an array is non-decreasing if nums[i] <= nums[i + 1] holds for
 * every i (0-based) such that (0 <= i <= n - 2).
 *
 * Example 1:
 * Input: nums = [4,2,3]
 * Output: true
 * Explanation: You could modify the first 4 to 1 to get a non-decreasing
 * array.
 *
 * Example 2:
 * Input: nums = [4,2,1]
 * Output: false
 * Explanation: You can't get a non-decreasing array by modify at most one
 * element.
 *
 * Constraints:
 *     ∙ n == nums.length
 *     ∙ 1 <= n <= 10⁴
 *     ∙ -10⁵ <= nums[i] <= 10⁵
 */
class Solution {
public:
    bool checkPossibility(vector<int>& nums) {
        if (nums.size() <= 1)
            return true;
        unordered_set<int> candidates;
        const int n = nums.size();
        for (int i = 0; i < n - 1; ++i)
            if (nums[i] > nums[i+1])
                candidates.insert(i);
        if (candidates.size() > 1)
            return false;
        if (candidates.size() == 0)
            return true;
        const int i = *candidates.begin();
        if (i == 0 || i >= n - 2)
            return true;
        if (nums[i-1] < nums[i+1] || nums[i] <= nums[i+2])
            return true;
        return i > 0 && nums[i-1] <= nums[i+1];
    }
};
