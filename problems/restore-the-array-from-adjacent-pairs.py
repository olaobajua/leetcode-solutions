"""
 * 1743. Restore the Array From Adjacent Pairs [Medium]
 * There is an integer array nums that consists of n unique elements, but you
 * have forgotten it. However, you do remember every pair of adjacent elements
 * in nums.
 *
 * You are given a 2D integer array adjacentPairs of size n - 1 where each
 * adjacentPairs[i] = [uᵢ, vᵢ] indicates that the elements uᵢ and vᵢ are
 * adjacent in nums.
 *
 * It is guaranteed that every adjacent pair of elements nums[i] and nums[i+1]
 * will exist in adjacentPairs, either as [nums[i], nums[i+1]] or [nums[i+1],
 * nums[i]]. The pairs can appear in any order.
 *
 * Return the original array nums. If there are multiple solutions, return any
 * of them.
 *
 * Example 1:
 * Input: adjacentPairs = [[2,1],[3,4],[3,2]]
 * Output: [1,2,3,4]
 * Explanation: This array has all its adjacent pairs in adjacentPairs.
 * Notice that adjacentPairs[i] may not be in left-to-right order.
 *
 * Example 2:
 * Input: adjacentPairs = [[4,-2],[1,4],[-3,1]]
 * Output: [-2,4,1,-3]
 * Explanation: There can be negative numbers.
 * Another solution is [-3,1,4,-2], which would also be accepted.
 *
 * Example 3:
 * Input: adjacentPairs = [[100000,-100000]]
 * Output: [100000,-100000]
 *
 * Constraints:
 *     ∙ nums.length == n
 *     ∙ adjacentPairs.length == n - 1
 *     ∙ adjacentPairs[i].length == 2
 *     ∙ 2 <= n <= 10⁵
 *     ∙ -10⁵ <= nums[i], uᵢ, vᵢ <= 10⁵
 *     ∙ There exists some nums that has adjacentPairs as its pairs.
"""
from collections import defaultdict
from typing import List

class Solution:
    def restoreArray(self, adjacentPairs: List[List[int]]) -> List[int]:
        graph = defaultdict(list)
        for a, b in adjacentPairs:
            graph[a].append(b)
            graph[b].append(a)

        end1, end2 = [node for node in graph if len(graph[node]) == 1]
        ret = []
        to_visit = [end1]
        prev = None
        for node in to_visit:
            ret.append(node)
            for neib in graph[node]:
                if neib != prev:
                    to_visit.append(neib)
            prev = node
        return ret

if __name__ == "__main__":
    tests = (
        ([[2,1],[3,4],[3,2]], [1,2,3,4]),
        ([[4,-2],[1,4],[-3,1]], [-2,4,1,-3]),
        ([[100000,-100000]], [100000,-100000]),
    )
    for adjacentPairs, expected in tests:
        print(Solution().restoreArray(adjacentPairs) == expected)
