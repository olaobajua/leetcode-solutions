"""
 * 743. Network Delay Time (Medium)
 * You are given a network of n nodes, labeled from 1 to n. You are also given
 * times, a list of travel times as directed edges times[i] = (uᵢ, vᵢ, wᵢ),
 * where uᵢ is the source node, vᵢ is the target node, and wᵢ is the time it
 * takes for a signal to travel from source to target.
 *
 * We will send a signal from a given node k. Return the time it takes for all
 * the n nodes to receive the signal. If it is impossible for all the n nodes
 * to receive the signal, return -1.
 *
 * Example 1:
 * Input: times = [[2,1,1],[2,3,1],[3,4,1]], n = 4, k = 2
 * Output: 2
 *
 * Example 2:
 * Input: times = [[1,2,1]], n = 2, k = 1
 * Output: 1
 *
 * Example 3:
 * Input: times = [[1,2,1]], n = 2, k = 2
 * Output: -1
 *
 * Constraints:
 *     ∙ 1 <= k <= n <= 100
 *     ∙ 1 <= times.length <= 6000
 *     ∙ times[i].length == 3
 *     ∙ 1 <= uᵢ, vᵢ <= n
 *     ∙ uᵢ != vᵢ
 *     ∙ 0 <= wᵢ <= 100
 *     ∙ All the pairs (uᵢ, vᵢ) are unique. (i.e., no multiple edges.)
"""
from heapq import heappop, heappush
from math import inf
from typing import List

class Solution:
    def networkDelayTime(self, times: List[List[int]], n: int, k: int) -> int:
        neighbours = [set() for _ in range(n + 1)]
        distances = {}
        for u, v, w in times:
            neighbours[u].add(v)
            distances[(u, v)] = w
        time = max(dijkstra(neighbours, distances, k)[1:])
        return time if time < inf else -1

def dijkstra(graph: dict, distances: dict, source) -> dict:
    """Dijkstra algorithm to find shortest paths."""
    shortest_paths = [inf] * len(graph)
    shortest_paths[source] = 0
    to_visit = [(0, source)]
    while to_visit:
        parent_dist, root = heappop(to_visit)
        for neib in graph[root]:
            cur_dist = distances.get((root, neib), inf)
            if parent_dist + cur_dist < shortest_paths[neib]:
                shortest_paths[neib] = parent_dist + cur_dist
                heappush(to_visit, (parent_dist + cur_dist, neib))
    return shortest_paths

if __name__ == "__main__":
    tests = (
        ([[2,1,1],[2,3,1],[3,4,1]], 4, 2, 2),
        ([[1,2,1]], 2, 1, 1),
        ([[1,2,1]], 2, 2, -1),
    )
    for times, n, k, expected in tests:
        print(Solution().networkDelayTime(times, n, k) == expected)
