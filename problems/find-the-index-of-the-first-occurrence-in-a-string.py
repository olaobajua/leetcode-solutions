"""
 * 28. Find the Index of the First Occurrence in a String [Medium]
 * Given two strings needle and haystack, return the index of the first
 * occurrence of needle in haystack, or -1 if needle is not part of haystack.
 *
 * Example 1:
 * Input: haystack = "sadbutsad", needle = "sad"
 * Output: 0
 * Explanation: "sad" occurs at index 0 and 6.
 * The first occurrence is at index 0, so we return 0.
 *
 * Example 2:
 * Input: haystack = "leetcode", needle = "leeto"
 * Output: -1
 * Explanation: "leeto" did not occur in "leetcode", so we return -1.
 *
 * Constraints:
 *     ∙ 1 <= haystack.length, needle.length <= 10⁴
 *     ∙ haystack and needle consist of only lowercase English characters.
"""
class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        return haystack.find(needle)

if __name__ == "__main__":
    tests = (
        ("sadbutsad", "sad", 0),
        ("leetcode", "leeto", -1),
    )
    for haystack, needle, expected in tests:
        print(Solution().strStr(haystack, needle) == expected)
