"""
 * 133. Clone Graph (Medium)
 * Given a reference of a node in a connected undirected graph.
 *
 * Return a deep copy (clone) of the graph.
 *
 * Each node in the graph contains a value (int) and a list (List[Node]) of its
 * neighbors.
 * class Node {
 *     public int val;
 *     public List<Node> neighbors;
 * }
 *
 * Test case format:
 *
 * For simplicity, each node's value is the same as the node's index
 * (1-indexed). For example, the first node with val == 1, the second node with
 * val == 2, and so on. The graph is represented in the test case using an
 * adjacency list.
 *
 * An adjacency list is a collection of unordered lists used to represent a
 * finite graph. Each list describes the set of neighbors of a node in the
 * graph.
 *
 * The given node will always be the first node with val = 1. You must return
 * the copy of the given node as a reference to the cloned graph.
 *
 * Example 1:
 * Input: adjList = [[2,4],[1,3],[2,4],[1,3]]
 * Output: [[2,4],[1,3],[2,4],[1,3]]
 * Explanation: There are 4 nodes in the graph.
 * 1st node (val = 1)'s neighbors are 2nd node (val = 2) and 4th node (val = 4).
 * 2nd node (val = 2)'s neighbors are 1st node (val = 1) and 3rd node (val = 3).
 * 3rd node (val = 3)'s neighbors are 2nd node (val = 2) and 4th node (val = 4).
 * 4th node (val = 4)'s neighbors are 1st node (val = 1) and 3rd node (val = 3).
 *
 * Example 2:
 * Input: adjList = [[]]
 * Output: [[]]
 * Explanation: Note that the input contains one empty list. The graph consists
 * of only one node with val = 1 and it does not have any neighbors.
 *
 * Example 3:
 * Input: adjList = []
 * Output: []
 * Explanation: This an empty graph, it does not have any nodes.
 *
 * Constraints:
 *     ∙ The number of nodes in the graph is in the range [0, 100].
 *     ∙ 1 <= Node.val <= 100
 *     ∙ Node.val is unique for each node.
 *     ∙ There are no repeated edges and no self-loops in the graph.
 *     ∙ The Graph is connected and all nodes can be visited starting from the
 *       given node.
"""
from typing import List

# Definition for a Node.
class Node:
    def __init__(self, val=0, neighbors=None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []

class Solution:
    def cloneGraph(self, node: 'Node') -> 'Node':
        return list_to_graph(graph_to_list(node))

def list_to_graph(nums):
    n = len(nums)
    graph = [Node(i) for i in range(1, n + 1)]
    for node, neighbors in zip(graph, nums):
        for neib in neighbors:
            node.neighbors.append(graph[neib-1])
    return graph[0] if graph else None

def graph_to_list(node):
    ret = {}
    to_visit = [node]
    for n in to_visit:
        if n and n.val not in ret:
            ret[n.val] = [neib.val for neib in n.neighbors]
            to_visit.extend(n.neighbors)
    return [ret[n] for n in sorted(ret)]

if __name__ == "__main__":
    tests = (
        ([[2,4],[1,3],[2,4],[1,3]], [[2,4],[1,3],[2,4],[1,3]]),
        ([[]], [[]]),
        ([], []),
        ([[2,3,4,5,6,7,8,9,10],
          [1,3,4,5,6,7,8,9,10],
          [1,2,4,5,6,7,8,9,10],
          [1,2,3,5,6,7,8,9,10],
          [1,2,3,4,6,7,8,9,10],
          [1,2,3,4,5,7,8,9,10],
          [1,2,3,4,5,6,8,9,10],
          [1,2,3,4,5,6,7,9,10],
          [1,2,3,4,5,6,7,8,10],
          [1,2,3,4,5,6,7,8,9]],
         [[2,3,4,5,6,7,8,9,10],
          [1,3,4,5,6,7,8,9,10],
          [1,2,4,5,6,7,8,9,10],
          [1,2,3,5,6,7,8,9,10],
          [1,2,3,4,6,7,8,9,10],
          [1,2,3,4,5,7,8,9,10],
          [1,2,3,4,5,6,8,9,10],
          [1,2,3,4,5,6,7,9,10],
          [1,2,3,4,5,6,7,8,10],
          [1,2,3,4,5,6,7,8,9]]),
    )
    for nums, expected in tests:
        print(graph_to_list(Solution().cloneGraph(list_to_graph(nums))) ==
              expected)
