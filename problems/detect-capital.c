/**
 * 520. Detect Capital (Easy)
 * We define the usage of capitals in a word to be right when one of the
 * following cases holds:
 *     ∙ All letters in this word are capitals, like "USA".
 *     ∙ All letters in this word are not capitals, like "leetcode".
 *     ∙ Only the first letter in this word is capital, like "Google".
 *
 * Given a string word, return true if the usage of capitals in it is right.
 *
 * Example 1:
 * Input: word = "USA"
 * Output: true
 *
 * Example 2:
 * Input: word = "FlaG"
 * Output: false
 *
 * Constraints:
 *     1 <= word.length <= 100
 *     word consists of lowercase and uppercase English letters.
 */
#define isLower(letter) ((letter >= 'a') ? (true) : (false))
#define isUpper(letter) ((letter < 'a') ? (true) : (false))

bool detectCapitalUse(char *word) {
    if (word[1] == '\0') {
        return true;
    }
    if (isLower(word[0]) && isUpper(word[1])) {
        return false;
    }
    char letter, secondIsLower = isLower(*++word);
    while (letter = *word++) {
        if (isLower(letter) != secondIsLower) {
            return false;
        }
    }
    return true;
}
