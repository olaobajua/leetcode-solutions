"""
 * 79. Word Search [Medium]
 * Given an m x n grid of characters board and a string word, return true if
 * word exists in the grid.
 *
 * The word can be constructed from letters of sequentially adjacent cells,
 * where adjacent cells are horizontally or vertically neighboring. The same
 * letter cell may not be used more than once.
 *
 * Example 1:
 * Input: board = [["A","B","C","E"],
 *                 ["S","F","C","S"],
 *                 ["A","D","E","E"]],
 * word = "ABCCED"
 * Output: true
 *
 * Example 2:
 * Input: board = [["A","B","C","E"],
 *                 ["S","F","C","S"],
 *                 ["A","D","E","E"]],
 * word = "SEE"
 * Output: true
 *
 * Example 3:
 * Input: board = [["A","B","C","E"],
 *                 ["S","F","C","S"],
 *                 ["A","D","E","E"]],
 * word = "ABCB"
 * Output: false
 *
 * Constraints:
 *     ∙ m == board.length
 *     ∙ n = board[i].length
 *     ∙ 1 <= m, n <= 6
 *     ∙ 1 <= word.length <= 15
 *     ∙ board and word consists of only lowercase and uppercase English
 *       letters.
 *
 * Follow up: Could you use search pruning to make your solution faster with a
 * larger board?
"""
from collections import Counter
from typing import List

class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        def dfs(row, col, word):
            if board.get((row, col)) == word:
                return True
            if board.get((row, col)) != word[0]:
                return False
            char, board[(row, col)] = board[(row, col)], None
            for nr, nc in (row-1,col), (row+1,col), (row,col-1), (row,col+1):
                if dfs(nr, nc, word[1:]):
                    return True
            board[(row, col)] = char
            return False

        board = {(r, c): l for r, row in enumerate(board)
                 for c, l in enumerate(row)}
        bcount = Counter(board.values())
        return (all(wcnt <= bcount[c] for c, wcnt in Counter(word).items()) and
                any(dfs(row, col, word) for row, col in board))


if __name__ == "__main__":
    tests = (
        ([["A","B","C","E"],
          ["S","F","C","S"],
          ["A","D","E","E"]], "ABCCED", True),
        ([["A","B","C","E"],
          ["S","F","C","S"],
          ["A","D","E","E"]], "SEE", True),
        ([["A","B","C","E"],
          ["S","F","C","S"],
          ["A","D","E","E"]], "ABCB", False),
        ([["a"]], "a", True),
    )
    for board, word, expected in tests:
        print(Solution().exist(board, word) == expected)
