/**
 * 383. Ransom Note [Easy]
 * Given two strings ransomNote and magazine, return true if ransomNote can be
 * constructed by using the letters from magazine and false otherwise.
 *
 * Each letter in magazine can only be used once in ransomNote.
 *
 * Example 1:
 * Input: ransomNote = "a", magazine = "b"
 * Output: false
 * Example 2:
 * Input: ransomNote = "aa", magazine = "ab"
 * Output: false
 * Example 3:
 * Input: ransomNote = "aa", magazine = "aab"
 * Output: true
 *
 * Constraints:
 *     ∙ 1 <= ransomNote.length, magazine.length <= 10⁵
 *     ∙ ransomNote and magazine consist of lowercase English letters.
 */

/**
 * @param {string} ransomNote
 * @param {string} magazine
 * @return {boolean}
 */
var canConstruct = function(ransomNote, magazine) {
    const countRN = Array(123).fill(0);
    const countMagazine = Array(123).fill(0);
    Array.from(ransomNote).forEach(c => ++countRN[c.charCodeAt()]);
    Array.from(magazine).forEach(c => ++countMagazine[c.charCodeAt()]);
    for (let i = 'a'.charCodeAt(); i < 123; ++i)
        if (countMagazine[i] < countRN[i])
            return false;
    return true;
};

const tests = [
    ["a", "b", false],
    ["aa", "ab", false],
    ["aa", "aab", true],
];

for (const [ransomNote, magazine, expected] of tests) {
    console.log(canConstruct(ransomNote, magazine) == expected);
}
