/**
 * 463. Island Perimeter (Easy)
 * You are given row x col grid representing a map where grid[i][j] = 1
 * represents land and grid[i][j] = 0 represents water.
 *
 * Grid cells are connected horizontally/vertically (not diagonally). The grid
 * is completely surrounded by water, and there is exactly one island
 * (i.e., one or more connected land cells).
 *
 * The island doesn't have "lakes", meaning the water inside isn't connected to
 * the water around the island. One cell is a square with side length 1.
 * The grid is rectangular, width and height don't exceed 100. Determine the
 * perimeter of the island.
 *
 * Example 1:
 * Input: grid = [[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]
 * Output: 16
 * Explanation: The perimeter is the 16 yellow stripes in the image above.
 *
 * Example 2:
 * Input: grid = [[1]]
 * Output: 4
 *
 * Example 3:
 * Input: grid = [[1,0]]
 * Output: 4
 *
 * Constraints:
 *     row == grid.length
 *     col == grid[i].length
 *     1 <= row, col <= 100
 *     grid[i][j] is 0 or 1.
 *     There is exactly one island in grid.
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
var islandPerimeter = function(grid) {
    function countBoundaries(row) {
        return sum(...zip([0].concat(row), row.concat([0]))
                      .map(([c1, c2]) => c1 != c2));
    }
    return sum(...grid.map(row => countBoundaries(row))) +        // horizontal
           sum(...zip(...grid).map(col => countBoundaries(col))); // vertical
};

function zip(...arrays) {
    let minLength = Math.min(...arrays.map(arr => arr.length));
    return [...Array(minLength)].map((_, i) => arrays.map(row => row[i]));
}

function sum(...iterable) {
    return iterable.reduce((acc, x) => acc + x);
}

var tests = [
    [[[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]], 16],
    [[[1]], 4],
    [[[1,0]], 4],
    [[[0,1]], 4],
    [[[0],[1]], 4],
]
for (let [grid, expected] of tests) {
    console.log(islandPerimeter(grid) == expected);
}
