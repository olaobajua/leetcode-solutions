"""
 * 1372. Longest ZigZag Path in a Binary Tree [Medium]
 * You are given the root of a binary tree.
 *
 * A ZigZag path for a binary tree is defined as follow:
 *     ∙ Choose any node in the binary tree and a direction (right or left).
 *     ∙ If the current direction is right, move to the right child of the
 *       current node; otherwise, move to the left child.
 *     ∙ Change the direction from right to left or from left to right.
 *     ∙ Repeat the second and third steps until you can't move in the tree.
 *
 * Zigzag length is defined as the number of nodes visited - 1. (A single node
 * has a length of 0).
 *
 * Return the longest ZigZag path contained in that tree.
 *
 * Example 1:
 *   1
 *     1
 *  1     1
 *      1   1
 *       1
 *        1
 * Input: root = [1,null,1,1,1,null,null,1,1,null,1,null,null,null,1,null,1]
 * Output: 3
 * Explanation: Longest ZigZag path in blue nodes (right -> left -> right).
 *
 * Example 2:
 *    1
 *  1   1
 *   1
 * 1   1
 *  1
 * Input: root = [1,1,1,null,1,null,null,1,1,null,1]
 * Output: 4
 * Explanation: Longest ZigZag path in blue nodes (left -> right -> left ->
 * right).
 *
 * Example 3:
 * Input: root = [1]
 * Output: 0
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 5 * 10⁴].
 *     ∙ 1 <= Node.val <= 100
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def longestZigZag(self, root: Optional[TreeNode]) -> int:
        def dfs(root, right, count):
            if not root:
                return count
            if right:
                return max(dfs(root.left, False, count + 1),
                           dfs(root.right, True, 0))
            else:
                return max(dfs(root.left, False, 0),
                           dfs(root.right, True, count + 1))

        return max(dfs(root.left, False, 0), dfs(root.right, True, 0))

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,None,1,1,1,None,None,1,1,None,1,None,None,None,1,None,1], 3),
        ([1,1,1,None,1,None,None,1,1,None,1], 4),
        ([1], 0),
    )
    for nums, expected in tests:
        print(Solution().longestZigZag(build_tree(nums)) == expected)
