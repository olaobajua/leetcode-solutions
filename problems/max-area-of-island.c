/**
 * 695. Max Area of Island (Medium)
 * You are given an m x n binary matrix grid. An island is a group of 1's
 * (representing land) connected 4-directionally (horizontal or vertical.) You
 * may assume all four edges of the grid are surrounded by water.
 *
 * The area of an island is the number of cells with a value 1 in the island.
 *
 * Return the maximum area of an island in grid. If there is no island,
 * return 0.
 *
 * Example 1:
 * Input: grid = [[0,0,1,0,0,0,0,1,0,0,0,0,0],
 *                [0,0,0,0,0,0,0,1,1,1,0,0,0],
 *                [0,1,1,0,1,0,0,0,0,0,0,0,0],
 *                [0,1,0,0,1,1,0,0,1,0,1,0,0],
 *                [0,1,0,0,1,1,0,0,1,1,1,0,0],
 *                [0,0,0,0,0,0,0,0,0,0,1,0,0],
 *                [0,0,0,0,0,0,0,1,1,1,0,0,0],
 *                [0,0,0,0,0,0,0,1,1,0,0,0,0]]
 * Output: 6
 * Explanation: The answer is not 11, because the island must be connected
 * 4-directionally.
 *
 * Example 2:
 * Input: grid = [[0,0,0,0,0,0,0,0]]
 * Output: 0
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 50
 *     ∙ grid[i][j] is either 0 or 1.
 */
int get_square(int r, int c, int m, int n, int **grid) {
    int ret = 0;
    if (grid[r][c] == 1) {
        ++ret;
        grid[r][c] = 2;
        if (c + 1 < n && grid[r][c+1] == 1)
            ret += get_square(r, c + 1, m, n, grid);
        if (0 <= c - 1 && grid[r][c-1] == 1)
            ret += get_square(r, c - 1, m, n, grid);
        if (r + 1 < m && grid[r+1][c] == 1)
            ret += get_square(r + 1, c, m, n, grid);
        if (0 <= r - 1 && grid[r-1][c] == 1)
            ret += get_square(r - 1, c, m, n, grid);
    }
    return ret;
}
int maxAreaOfIsland(int **grid, int gridSize, int *gridColSize) {
    int ret = 0;
    for (int r = 0; r < gridSize; ++r) {
        for (int c = 0; c < gridColSize[r]; ++c) {
            ret = fmax(ret, get_square(r, c, gridSize, gridColSize[0], grid));
        }
    }
    return ret;
}
