/**
 * 260. Single Number III (Medium)
 * Given an integer array nums, in which exactly two elements appear only once
 * and all the other elements appear exactly twice. Find the two elements that
 * appear only once. You can return the answer in any order.
 *
 * You must write an algorithm that runs in linear runtime complexity and uses
 * only constant extra space.
 *
 * Example 1:
 * Input: nums = [1,2,1,3,2,5]
 * Output: [3,5]
 * Explanation:  [5, 3] is also a valid answer.
 *
 * Example 2:
 * Input: nums = [-1,0]
 * Output: [-1,0]
 *
 * Example 3:
 * Input: nums = [0,1]
 * Output: [1,0]
 *
 * Constraints:
 *     ∙ 2 <= nums.length <= 3 * 10⁴
 *     ∙ -2³¹ <= nums[i] <= 2³¹ - 1
 *     ∙ Each integer in nums will appear twice, only two integers will appear
 *       once.
 */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var singleNumber = function(nums) {
    const xorNums = nums.reduce((acc, num) => acc ^= num, 0);
    const lo = xorNums & (xorNums - 1) ^ xorNums;
    let num1 = 0, num2 = 0;
    nums.forEach((n) => ((n & lo) !== 0) ? num1 ^= n : num2 ^= n);
    return [num1, num2];
};

const tests = [
    [[1,2,1,3,2,5], [3,5]],
    [[-1,0], [-1,0]],
    [[0,1], [1,0]],
    [[1,1,0,-2147483648], [0, -2147483648]],
];

for (let [nums, expected] of tests) {
    console.log(singleNumber(nums).every(x => expected.includes(x)));
}
