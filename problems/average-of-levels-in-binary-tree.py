"""
 * 637. Average of Levels in Binary Tree [Easy]
 * Given the root of a binary tree, return the average value of the nodes on
 * each level in the form of an array. Answers within 10⁻⁵ of the actual answer
 * will be accepted.
 * Example 1:
 * Input: root = [3,9,20,null,null,15,7]
 * Output: [3.00000,14.50000,11.00000]
 * Explanation: The average value of nodes on level 0 is 3, on level 1 is
 * 14.5, and on level 2 is 11.
 * Hence return [3, 14.5, 11].
 *
 * Example 2:
 * Input: root = [3,9,20,15,7]
 * Output: [3.00000,14.50000,11.00000]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ -2³¹ <= Node.val <= 2³¹ - 1
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def averageOfLevels(self, root: Optional[TreeNode]) -> List[float]:
        ret = []
        level = [root]
        while level:
            next_level = []
            s = 0
            for node in level:
                s += node.val
                if node.left:
                    next_level.append(node.left)
                if node.right:
                    next_level.append(node.right)
            ret.append(s / len(level))
            level = next_level
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([3,9,20,None,None,15,7], [3.00000,14.50000,11.00000]),
        ([3,9,20,15,7], [3.00000,14.50000,11.00000]),
    )
    for nums, expected in tests:
        print(Solution().averageOfLevels(build_tree(nums)) == expected)
