"""
 * 491. Non-decreasing Subsequences [Medium]
 * Given an integer array nums, return all the different possible
 * non-decreasing subsequences of the given array with at least two elements.
 * You may return the answer in any order.
 *
 * Example 1:
 * Input: nums = [4,6,7,7]
 * Output: [[4,6],[4,6,7],[4,6,7,7],[4,7],[4,7,7],[6,7],[6,7,7],[7,7]]
 *
 * Example 2:
 * Input: nums = [4,4,3,2,1]
 * Output: [[4,4]]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 15
 *     ∙ -100 <= nums[i] <= 100
"""
from typing import List

class Solution:
    def findSubsequences(self, nums: List[int]) -> List[List[int]]:
        n = len(nums)
        ret = set()
        for mask in range(1, 2**n):
            subseq = []
            for i, x in enumerate(nums):
                if mask & (1 << i):
                    if subseq and subseq[-1] > x:
                        break
                    subseq.append(x)
            else:
                if len(subseq) >= 2:
                    ret.add(tuple(subseq))
        return ret

if __name__ == "__main__":
    tests = (
        ([4,6,7,7], [[4,6],[4,6,7],[4,6,7,7],[4,7],[4,7,7],[6,7],[6,7,7],[7,7]]),
        ([4,4,3,2,1], [[4,4]]),
    )
    for nums, expected in tests:
        print(sorted(map(list, Solution().findSubsequences(nums))) == expected)
