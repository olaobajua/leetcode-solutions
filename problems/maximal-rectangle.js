/**
 * 85. Maximal Rectangle (Hard)
 * Given a rows x cols binary matrix filled with 0's and 1's, find the largest
 * rectangle containing only 1's and return its area.
 *
 * Example 1:
 * Input: matrix = [["1","0","1","0","0"],
 *                  ["1","0","1","1","1"],
 *                  ["1","1","1","1","1"],
 *                  ["1","0","0","1","0"]]
 * Output: 6
 *
 * Example 2:
 * Input: matrix = []
 * Output: 0
 *
 * Example 3:
 * Input: matrix = [["0"]]
 * Output: 0
 *
 * Example 4:
 * Input: matrix = [["1"]]
 * Output: 1
 *
 * Example 5:
 * Input: matrix = [["0","0"]]
 * Output: 0
 *
 * Constraints:
 *     rows == matrix.length
 *     cols == matrix[i].length
 *     0 <= row, cols <= 200
 *     matrix[i][j] is '0' or '1'.
 */

/**
 * @param {character[][]} matrix
 * @return {number}
 */
var maximalRectangle = function(matrix) {
    matrix = matrix.map(row => row.map(c => parseInt(c)));
    for (let col = matrix[0] ? matrix[0].length - 1 : -1; col >= 0; --col) {
        let acc = 0;
        for (let row = matrix.length - 1; row >= 0; --row) {
            acc = matrix[row][col] = (acc + matrix[row][col])*matrix[row][col];
        }
    }
    let maxRect = 0;
    matrix.forEach(row => {
        row.push(0);
        const stack = [-1];
        row.forEach((height, i) => {
            if (height >= row[stack[stack.length-1]]) {
                stack.push(i);
            }
            while (height < row[stack[stack.length-1]]) {
                const h = row[stack.pop()];
                const w = i - stack[stack.length-1] - 1;
                maxRect = Math.max(maxRect, h * w);
            }
            stack.push(i);
        });
    });
    return maxRect;
};

const tests = [
    [[["1","0","1","0","0"],
      ["1","0","1","1","1"],
      ["1","1","1","1","1"],
      ["1","0","0","1","0"]], 6],
    [[], 0],
    [[["0"]], 0],
    [[["1"]], 1],
    [[["0","0"]], 0],
];

for (const [matrix, expected] of tests) {
    console.log(maximalRectangle(matrix) == expected);
}
