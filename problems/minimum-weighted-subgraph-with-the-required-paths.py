"""
 * 2203. Minimum Weighted Subgraph With the Required Paths (Hard)
 * You are given an integer n denoting the number of nodes of a weighted
 * directed graph. The nodes are numbered from 0 to n - 1.
 *
 * You are also given a 2D integer array edges where
 * edges[i] = [fromᵢ, toᵢ, weightᵢ] denotes that there exists a directed edge
 * from fromᵢ to toᵢ with weight weightᵢ.
 *
 * Lastly, you are given three distinct integers src1, src2, and dest denoting
 * three distinct nodes of the graph.
 *
 * Return the minimum weight of a subgraph of the graph such that it is
 * possible to reach dest from both src1 and src2 via a set of edges of this
 * subgraph. In case such a subgraph does not exist, return -1.
 *
 * A subgraph is a graph whose vertices and edges are subsets of the original
 * graph. The weight of a subgraph is the sum of weights of its constituent
 * edges.
 *
 * Example 1:
 * Input: n = 6,
 *        edges = [[0,2,2],[0,5,6],[1,0,3],[1,4,5],[2,1,1],[2,3,3],[2,3,4],
 *                 [3,4,2],[4,5,1]],
 *        src1 = 0,
 *        src2 = 1,
 *        dest = 5
 * Output: 9
 * Explanation:
 * The above figure represents the input graph.
 * The blue edges represent one of the subgraphs that yield the optimal answer.
 * Note that the subgraph [[1,0,3],[0,5,6]] also yields the optimal answer.
 * It is not possible to get a subgraph with less weight satisfying all the
 * constraints.
 *
 * Example 2:
 * Input: n = 3, edges = [[0,1,1],[2,1,1]], src1 = 0, src2 = 1, dest = 2
 * Output: -1
 * Explanation:
 * The above figure represents the input graph.
 * It can be seen that there does not exist any path from node 1 to node 2,
 * hence there are no subgraphs satisfying all the constraints.
 *
 * Constraints:
 *     ∙ 3 <= n <= 10⁵
 *     ∙ 0 <= edges.length <= 10⁵
 *     ∙ edges[i].length == 3
 *     ∙ 0 <= fromᵢ, toᵢ, src1, src2, dest <= n - 1
 *     ∙ fromᵢ != toᵢ
 *     ∙ src1, src2, and dest are pairwise distinct.
 *     ∙ 1 <= weight[i] <= 10⁵
"""
from collections import defaultdict
from heapq import heappop, heappush
from math import inf
from typing import List

class Solution:
    def minimumWeight(self, n: int, edges: List[List[int]], src1: int, src2: int, dest: int) -> int:
        graph = defaultdict(list)
        back_graph = defaultdict(list)
        distances = {}
        back_distances = {}
        for s, t, w in edges:
            graph[s].append(t)
            back_graph[t].append(s)
            distances[(s, t)] = min(w, distances.get((s, t), inf))
            back_distances[(t, s)] = distances[(s, t)]
        dist_src1 = dijkstra(graph, distances, src1)
        dist_src2 = dijkstra(graph, distances, src2)
        dist_dest = dijkstra(back_graph, back_distances, dest)
        ret = min(dist_src1.get(i, inf) +
                  dist_src2.get(i, inf) +
                  dist_dest.get(i, inf)
                  for i in range(n))
        return ret if ret < inf else -1

def dijkstra(graph: dict, distances: dict, source) -> dict:
    """Dijkstra algorithm to find shortest paths."""
    shortest_paths = dict.fromkeys(graph, inf)
    shortest_paths[source] = 0
    to_visit = [(0, source)]
    while to_visit:
        parent_dist, root = heappop(to_visit)
        for neib in graph[root]:
            cur_dist = distances.get((root, neib), inf)
            if parent_dist + cur_dist < shortest_paths.get(neib, inf):
                shortest_paths[neib] = parent_dist + cur_dist
                heappush(to_visit, (parent_dist + cur_dist, neib))
    return shortest_paths

if __name__ == "__main__":
    tests = (
        (6, [[0,2,2],[0,5,6],[1,0,3],[1,4,5],[2,1,1],[2,3,3],[2,3,4],[3,4,2],[4,5,1]], 0, 1, 5, 9),
        (3, [[0,1,1],[2,1,1]], 0, 1, 2, -1),
        (5, [[4,2,20],[4,3,46],[0,1,15],[0,1,43],[0,1,32],[3,1,13]], 0, 4, 1, 74),
        (5, [[0,2,1],[0,3,1],[2,4,1],[3,4,1],[1,2,1],[1,3,10]], 0, 1, 4, 3),
    )
    for n, edges, src1, src2, dest, expected in tests:
        print(Solution().minimumWeight(n, edges, src1, src2, dest) == expected)
