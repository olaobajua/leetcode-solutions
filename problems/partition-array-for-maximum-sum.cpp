/**
 * 1043. Partition Array for Maximum Sum [Medium]
 * Given an integer array arr, partition the array into (contiguous) subarrays
 * of length at most k. After partitioning, each subarray has their values
 * changed to become the maximum value of that subarray.
 *
 * Return the largest sum of the given array after partitioning. Test cases
 * are generated so that the answer fits in a 32-bit integer.
 *
 * Example 1:
 * Input: arr = [1,15,7,9,2,5,10], k = 3
 * Output: 84
 * Explanation: arr becomes [15,15,15,9,10,10,10]
 *
 * Example 2:
 * Input: arr = [1,4,1,5,7,3,6,1,9,9,3], k = 4
 * Output: 83
 *
 * Example 3:
 * Input: arr = [1], k = 1
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 500
 *     ∙ 0 <= arr[i] <= 10⁹
 *     ∙ 1 <= k <= arr.length
 */
#include <vector>
#include <algorithm>

class Solution {
public:
    int maxSumAfterPartitioning(std::vector<int>& arr, int k) {
        int n = arr.size();
        std::vector<int> memo(n, -1);

        function<int(int)> dp = [&](int i) -> int {
            if (i == n) {
                return 0;
            }

            if (memo[i] > -1) {
                return memo[i];
            }

            memo[i] = 0;
            int maxElement = 0;
            for (int j = i; j < min(n, i + k); ++j) {
                maxElement = std::max(maxElement, arr[j]);
                memo[i] = std::max(memo[i], dp(j + 1) + maxElement * (j - i + 1));
            }

            return memo[i];
        };

        return dp(0);
    }
};
