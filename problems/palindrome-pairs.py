"""
 * 336. Palindrome Pairs [Hard]
 * Given a list of unique words, return all the pairs of the distinct indices
 * (i, j) in the given list, so that the concatenation of the two words
 * words[i] + words[j] is a palindrome.
 *
 * Example 1:
 * Input: words = ["abcd","dcba","lls","s","sssll"]
 * Output: [[0,1],[1,0],[3,2],[2,4]]
 * Explanation: The palindromes are ["dcbaabcd","abcddcba","slls","llssssll"]
 *
 * Example 2:
 * Input: words = ["bat","tab","cat"]
 * Output: [[0,1],[1,0]]
 * Explanation: The palindromes are ["battab","tabbat"]
 *
 * Example 3:
 * Input: words = ["a",""]
 * Output: [[0,1],[1,0]]
 *
 * Constraints:
 *     ∙ 1 <= words.length <= 5000
 *     ∙ 0 <= words[i].length <= 300
 *     ∙ words[i] consists of lower-case English letters.
"""
from typing import List

class Solution:
    def palindromePairs(self, words: List[str]) -> List[List[int]]:
        def dfs(prefs, chars):
            for c in prefs:
                if c == '#':
                    if chars == chars[::-1]:
                        for j in prefs.get('#', []):
                            if j != i:
                                ret.append([i, j])
                else:
                    dfs(prefs[c], chars + [c])
        prefixes = {}
        ret = []
        for i, word in enumerate(words):
            p = prefixes
            for c in reversed(word):
                p = p.setdefault(c, {})
            p.setdefault('#', []).append(i)
        for i, word in enumerate(words):
            p = prefixes
            for j, c in enumerate(word):
                for k in p.get('#', []):
                    if word[j:] == word[j:][::-1]:
                        ret.append([i, k])
                if (p := p.get(c)) is None:
                    break
            else:
                dfs(p, [])
        return ret

if __name__ == "__main__":
    tests = (
        (["abcd","dcba","lls","s","sssll"], [[0,1],[1,0],[3,2],[2,4]]),
        (["bat","tab","cat"], [[0,1],[1,0]]),
        (["a",""], [[0,1],[1,0]]),
    )
    for words, expected in tests:
        print(set(map(tuple, Solution().palindromePairs(words))) ==
              set(map(tuple, expected)))
