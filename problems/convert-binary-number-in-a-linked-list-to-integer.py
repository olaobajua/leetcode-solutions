"""
 * 1290. Convert Binary Number in a Linked List to Integer (Easy)
 * Given head which is a reference node to a singly-linked list. The value of
 * each node in the linked list is either 0 or 1. The linked list holds the
 * binary representation of a number.
 *
 * Return the decimal value of the number in the linked list.
 *
 * Example 1:
 * Input: head = [1,0,1]
 * Output: 5
 * Explanation: (101) in base 2 = (5) in base 10
 *
 * Example 2:
 * Input: head = [0]
 * Output: 0
 *
 * Example 3:
 * Input: head = [1]
 * Output: 1
 *
 * Example 4:
 * Input: head = [1,0,0,1,0,0,1,1,1,0,0,0,0,0,0]
 * Output: 18880
 *
 * Example 5:
 * Input: head = [0,0]
 * Output: 0
 *
 * Constraints:
 *     The Linked List is not empty.
 *     Number of nodes will not exceed 30.
 *     Each node's value is either 0 or 1.
"""
from typing import List

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def getDecimalValue(self, head: ListNode) -> int:
        ret = head.val
        while (head := head.next):
            ret = ret * 2 + head.val
        return ret

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

if __name__ == "__main__":
    tests = (
        ([1,0,1], 5),
        ([0], 0),
        ([1], 1),
        ([1,0,0,1,0,0,1,1,1,0,0,0,0,0,0], 18880),
        ([0,0], 0),
    )
    for nums, expected in tests:
        head = list_to_linked_list(nums)
        print(Solution().getDecimalValue(head) == expected)
