"""
 * 2583. Kth Largest Sum in a Binary Tree [Medium]
 * You are given the root of a binary tree and a positive integer k.
 *
 * The level sum in the tree is the sum of the values of the nodes that are on
 * the same level.
 *
 * Return the kᵗʰ largest level sum in the tree (not necessarily distinct). If
 * there are fewer than k levels in the tree, return -1.
 *
 * Note that two nodes are on the same level if they have the same distance
 * from the root.
 *
 * Example 1:
 * Input: root = [5,8,9,2,1,3,7,4,6], k = 2
 * Output: 13
 * Explanation: The level sums are the following:
 * - Level 1: 5.
 * - Level 2: 8 + 9 = 17.
 * - Level 3: 2 + 1 + 3 + 7 = 13.
 * - Level 4: 4 + 6 = 10.
 * The 2ⁿᵈ largest level sum is 13.
 *
 * Example 2:
 * Input: root = [1,2,null,3], k = 1
 * Output: 3
 * Explanation: The largest level sum is 3.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is n.
 *     ∙ 2 <= n <= 10⁵
 *     ∙ 1 <= Node.val <= 10⁶
 *     ∙ 1 <= k <= n
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def kthLargestLevelSum(self, root: Optional[TreeNode], k: int) -> int:
        sums = []
        level = [root]
        while level:
            next_level = []
            cur_sum = 0
            level_exists = False
            for node in level:
                if node:
                    level_exists = True
                    cur_sum += node.val
                    next_level.extend([node.left, node.right])
            if level_exists:
                sums.append(cur_sum)
            level = next_level
        return sorted(sums, reverse=True)[k-1] if len(sums) >= k else -1

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([5,8,9,2,1,3,7,4,6], 2, 13),
        ([1,2,None,3], 1, 3),
        ([5,8,9,2,1,3,7], 4, -1),
    )
    for nums, k, expected in tests:
        print(Solution().kthLargestLevelSum(build_tree(nums), k) == expected)
