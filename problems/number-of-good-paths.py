"""
 * 2421. Number of Good Paths [Hard]
 * There is a tree (i.e. a connected, undirected graph with no cycles)
 * consisting of n nodes numbered from 0 to n - 1 and exactly n - 1 edges.
 *
 * You are given a 0-indexed integer array vals of length n where vals[i]
 * denotes the value of the iᵗʰ node. You are also given a 2D integer array
 * edges where edges[i] = [aᵢ, bᵢ] denotes that there exists an undirected edge
 * connecting nodes aᵢ and bᵢ.
 *
 * A good path is a simple path that satisfies the following conditions:
 *     ∙ The starting node and the ending node have the same value.
 *     ∙ All nodes between the starting node and the ending node have values
 *       less than or equal to the starting node (i.e. the starting node's
 *       value should be the maximum value along the path).
 *
 * Return the number of distinct good paths.
 *
 * Note that a path and its reverse are counted as the same path. For example,
 * 0 -> 1 is considered to be the same as 1 -> 0. A single node is also
 * considered as a valid path.
 *
 * Example 1:
 *   0        1
 * 1   2    3   2
 *    3 4      1 3
 * Input: vals = [1,3,2,1,3], edges = [[0,1],[0,2],[2,3],[2,4]]
 * Output: 6
 * Explanation: There are 5 good paths consisting of a single node.
 * There is 1 additional good path: 1 -> 0 -> 2 -> 4.
 * (The reverse path 4 -> 2 -> 0 -> 1 is treated as the same as 1 -> 0 -> 2 ->
 * 4.)
 * Note that 0 -> 2 -> 3 is not a good path because vals[2] > vals[0].
 *
 * Example 2:
 *  0    1
 *  1    1
 *  2    2
 * 3 4  2 3
 * Input: vals = [1,1,2,2,3], edges = [[0,1],[1,2],[2,3],[2,4]]
 * Output: 7
 * Explanation: There are 5 good paths consisting of a single node.
 * There are 2 additional good paths: 0 -> 1 and 2 -> 3.
 *
 * Example 3:
 * Input: vals = [1], edges = []
 * Output: 1
 * Explanation: The tree consists of only one node, so there is one good path.
 *
 * Constraints:
 *     ∙ n == vals.length
 *     ∙ 1 <= n <= 3 * 10⁴
 *     ∙ 0 <= vals[i] <= 10⁵
 *     ∙ edges.length == n - 1
 *     ∙ edges[i].length == 2
 *     ∙ 0 <= aᵢ, bᵢ < n
 *     ∙ aᵢ != bᵢ
 *     ∙ edges represents a valid tree.
"""
from collections import Counter
from typing import List

class Solution:
    def numberOfGoodPaths(self, vals: List[int], edges: List[List[int]]) -> int:
        n = len(vals)
        ret = n
        dsu = DisjointSet(n)
        count = [Counter([v]) for v in vals]

        for max_val, a, b in sorted([max(vals[a], vals[b]), a, b] for a, b in edges):
            count_a = count[dsu.find(a)][max_val]
            count_b = count[dsu.find(b)][max_val]
            ret += count_b * count_a
            dsu.union(b, a)
            count[dsu.find(a)] = Counter({max_val: count_b + count_a})
        return ret

class DisjointSet:
    def __init__(self, n):
        self.parent = list(range(n))

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        if (px := self.find(x)) != (py := self.find(y)):
            self.parent[px] = py

if __name__ == "__main__":
    tests = (
        ([1,3,2,1,3], [[0,1],[0,2],[2,3],[2,4]], 6),
        ([1,1,2,2,3], [[0,1],[1,2],[2,3],[2,4]], 7),
        ([1], [], 1),
        ([2,5,5,1,5,2,3,5,1,5],
         [[0,1],[2,1],[3,2],[3,4],[3,5],[5,6],[1,7],[8,4],[9,7]], 20),
    )
    for vals, edges, expected in tests:
        print(Solution().numberOfGoodPaths(vals, edges) == expected)
