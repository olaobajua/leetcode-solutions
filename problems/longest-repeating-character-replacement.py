"""
 * 424. Longest Repeating Character Replacement (Medium)
 * You are given a string s and an integer k. You can choose any character of
 * the string and change it to any other uppercase English character. You can
 * perform this operation at most k times.
 *
 * Return the length of the longest substring containing the same letter you
 * can get after performing the above operations.
 *
 * Example 1:
 * Input: s = "ABAB", k = 2
 * Output: 4
 * Explanation: Replace the two 'A's with two 'B's or vice versa.
 *
 * Example 2:
 * Input: s = "AABABBA", k = 1
 * Output: 4
 * Explanation: Replace the one 'A' in the middle with 'B' and form "AABBBBA".
 * The substring "BBBB" has the longest repeating letters, which is 4.
 *
 * Constraints:
 *     1 <= s.length <= 10⁵
 *     s consists of only uppercase English letters.
 *     0 <= k <= s.length
"""
class Solution:
    def characterReplacement(self, s: str, k: int) -> int:
        letters = set(s)
        swaps = {l: {0: -1} for l in letters}
        cur_swaps = {l: 0 for l in letters}
        max_conseq = 0
        for i in range(len(s)):
            for l in letters:
                if s[i] != l:
                    cur_swaps[l] += 1
                    swaps[l][cur_swaps[l]] = i
            max_conseq = max(max_conseq,
                             *(i - swaps[l][max(0, cur_swaps[l] - k)]
                               for l in letters))
        return max_conseq

if __name__ == "__main__":
    tests = (
        ("ABAB", 2, 4),
        ("AABABBA", 1, 4),
    )
    for s, k, expected in tests:
        print(Solution().characterReplacement(s, k) == expected)
