"""
 * 1584. Min Cost to Connect All Points (Medium)
 * You are given an array points representing integer coordinates of some
 * points on a 2D-plane, where points[i] = [xᵢ, yᵢ].
 *
 * The cost of connecting two points [xᵢ, yᵢ] and [xⱼ, yⱼ] is the manhattan
 * distance between them: |xᵢ - xⱼ| + |yᵢ - yⱼ|, where |val| denotes the
 * absolute value of val.
 *
 * Return the minimum cost to make all points connected. All points are
 * connected if there is exactly one simple path between any two points.
 *
 * Example 1:
 * Input: points = [[0,0],[2,2],[3,10],[5,2],[7,0]]
 * Output: 20
 * Explanation:
 *
 * We can connect the points as shown above to get the minimum cost of 20.
 * Notice that there is a unique path between every pair of points.
 *
 * Example 2:
 * Input: points = [[3,12],[-2,5],[-4,1]]
 * Output: 18
 *
 * Constraints:
 *     ∙ 1 <= points.length <= 1000
 *     ∙ -10⁶ <= xᵢ, yᵢ <= 10⁶
 *     ∙ All pairs (xᵢ, yᵢ) are distinct.
"""
from typing import List

class Solution:
    def minCostConnectPoints(self, points: List[List[int]]) -> int:
        n = len(points)
        vertices = DisjointSet(n)
        distances = []
        for i in range(n):
            for j in range(i + 1, n):
                x1, y1 = points[i]
                x2, y2 = points[j]
                distances.append([abs(x1 - x2) + abs(y1 - y2), (i, j)])
        ret = 0
        for d, (i, j) in sorted(distances):
            if vertices.find(i) != vertices.find(j):
                vertices.union(i, j)
                ret += d
                n -= 1
            if n == 1:
                break
        return ret

class DisjointSet:
    """Optimized for add and union operations."""
    def __init__(self, n):
        self.parent = list(range(n))

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        if (px := self.find(x)) != (py := self.find(y)):
            self.parent[px] = py

if __name__ == "__main__":
    tests = (
        ([[0,0],[2,2],[3,10],[5,2],[7,0]], 20),
        ([[3,12],[-2,5],[-4,1]], 18),
    )
    for points, expected in tests:
        print(Solution().minCostConnectPoints(points) == expected)
