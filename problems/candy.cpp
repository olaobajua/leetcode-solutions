/**
 * 135. Candy (Hard)
 * There are n children standing in a line. Each child is assigned a rating
 * value given in the integer array ratings.
 *
 * You are giving candies to these children subjected to the following
 * requirements:
 *     ∙ Each child must have at least one candy.
 *     ∙ Children with a higher rating get more candies than their neighbors.
 *
 * Return the minimum number of candies you need to have to distribute the
 * candies to the children.
 *
 * Example 1:
 * Input: ratings = [1,0,2]
 * Output: 5
 * Explanation: You can allocate to the first, second and third child with
 * 2, 1, 2 candies respectively.
 *
 * Example 2:
 * Input: ratings = [1,2,2]
 * Output: 4
 * Explanation: You can allocate to the first, second and third child with
 * 1, 2, 1 candies respectively.
 * The third child gets 1 candy because it satisfies the above two conditions.
 *
 * Constraints:
 *     ∙ n == ratings.length
 *     ∙ 1 <= n <= 2 * 10⁴
 *     ∙ 0 <= ratings[i] <= 2 * 10⁴
 */
class Solution {
public:
    int candy(vector<int>& ratings) {
        vector<int> candies(ratings.size(), 1);
        vector <pair<int,int>> by_rating(ratings.size());
        for (int i = 0; i < ratings.size(); ++i) {
            by_rating[i].first = ratings[i];
            by_rating[i].second = i;
        }
        sort(by_rating.begin(), by_rating.end());
        for (auto&& [r, i] : by_rating) {
            if (i > 0 && r > ratings[i-1]) {
                candies[i] = max(candies[i], candies[i-1] + 1);
            }
            if (i < ratings.size() - 1 && r > ratings[i+1]) {
                candies[i] = max(candies[i], candies[i+1] + 1);
            }
        }
        return accumulate(candies.begin(), candies.end(), 0);
    }
};
