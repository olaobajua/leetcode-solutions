/**
 * 792. Number of Matching Subsequences [Medium]
 * Given a string s and an array of strings words, return the number of
 * words[i] that is a subsequence of s.
 *
 * A subsequence of a string is a new string generated from the original string
 * with some characters (can be none) deleted without changing the relative
 * order of the remaining characters.
 *     ∙ For example, "ace" is a subsequence of "abcde".
 *
 * Example 1:
 * Input: s = "abcde", words = ["a","bb","acd","ace"]
 * Output: 3
 * Explanation: There are three strings in words that are a subsequence of s:
 * "a", "acd", "ace".
 *
 * Example 2:
 * Input: s = "dsahjpjauf", words = ["ahjpjau","ja","ahbwzgqnuk","tnmlanowax"]
 * Output: 2
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 5 * 10⁴
 *     ∙ 1 <= words.length <= 5000
 *     ∙ 1 <= words[i].length <= 50
 *     ∙ s and words[i] consist of only lowercase English letters.
 */
class Solution {
    List<Integer>[] origChars = new List[128];
    public int numMatchingSubseq(String s, String[] words) {
        for(char c = 'a'; c <= 'z'; ++c)
            origChars[c] = new ArrayList<>();
        for (int i = 0; i < s.length(); ++i)
            origChars[s.charAt(i)].add(i);
        int ret = 0;
        for (String word : words)
            if (isSubsequence(word))
                ++ret;
        return ret;
    }
    private boolean isSubsequence(String word) {
        int curPos = 0;
        for (char c: word.toCharArray()) {
            int i = Collections.binarySearch(origChars[c], curPos);
            if (i < 0)
                i = -i - 1;
            if (i == origChars[c].size())
                return false;
            curPos = origChars[c].get(i) + 1;
        }
        return true;
    }
}
