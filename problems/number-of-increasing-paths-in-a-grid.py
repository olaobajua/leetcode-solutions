"""
 * 2328. Number of Increasing Paths in a Grid [Hard]
 * You are given an m x n integer matrix grid, where you can move from a cell
 * to any adjacent cell in all 4 directions.
 *
 * Return the number of strictly increasing paths in the grid such that you
 * can start from any cell and end at any cell. Since the answer may be very
 * large, return it modulo 10⁹ + 7.
 *
 * Two paths are considered different if they do not have exactly the same
 * sequence of visited cells.
 *
 * Example 1:
 * Input: grid = [[1,1],
 *                [3,4]]
 * Output: 8
 * Explanation: The strictly increasing paths are:
 * - Paths with length 1: [1], [1], [3], [4].
 * - Paths with length 2: [1 -> 3], [1 -> 4], [3 -> 4].
 * - Paths with length 3: [1 -> 3 -> 4].
 * The total number of paths is 4 + 3 + 1 = 8.
 *
 * Example 2:
 * Input: grid = [[1],
 *                [2]]
 * Output: 3
 * Explanation: The strictly increasing paths are:
 * - Paths with length 1: [1], [2].
 * - Paths with length 2: [1 -> 2].
 * The total number of paths is 2 + 1 = 3.
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 1000
 *     ∙ 1 <= m * n <= 10⁵
 *     ∙ 1 <= grid[i][j] <= 10⁵
"""
from itertools import product
from functools import cache
from typing import List

class Solution:
    def countPaths(self, grid: List[List[int]]) -> int:
        @cache
        def dp(r, c):
            ret = 0
            for nr, nc in (r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1):
                if 0 <= nr < m and 0 <= nc < n and grid[nr][nc] > grid[r][c]:
                    ret = (ret + 1 + dp(nr, nc)) % MOD
            return ret

        m = len(grid)
        n = len(grid[0])
        MOD = 1000000007
        ret = m * n
        for row, col in product(range(m), range(n)):
            ret = (ret + dp(row, col)) % MOD
        return ret

if __name__ == "__main__":
    tests = (
        ([[1,1],
          [3,4]], 8),
        ([[1],
          [2]], 3),
    )
    for grid, expected in tests:
        print(Solution().countPaths(grid) == expected)
