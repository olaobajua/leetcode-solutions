/**
 * 1631. Path With Minimum Effort (Medium)
 * You are a hiker preparing for an upcoming hike. You are given heights, a 2D
 * array of size rows x columns, where heights[row][col] represents the height
 * of cell (row, col). You are situated in the top-left cell, (0, 0), and you
 * hope to travel to the bottom-right cell, (rows-1, columns-1)
 * (i.e., 0-indexed). You can move up, down, left, or right, and you wish
 * to find a route that requires the minimum effort.
 *
 * A route's effort is the maximum absolute difference in heights between two
 * consecutive cells of the route.
 *
 * Return the minimum effort required to travel from the top-left cell to the
 * bottom-right cell.
 *
 * Example 1:
 * Input: heights = [[1,2,2],
 *                   [3,8,2],
 *                   [5,3,5]]
 * Output: 2
 * Explanation: The route of [1,3,5,3,5] has a maximum absolute difference of 2
 * in consecutive cells.
 * This is better than the route of [1,2,2,2,5], where the maximum absolute
 * difference is 3.
 *
 * Example 2:
 * Input: heights = [[1,2,3],
 *                   [3,8,4],
 *                   [5,3,5]]
 * Output: 1
 * Explanation: The route of [1,2,3,4,5] has a maximum absolute difference of 1
 * in consecutive cells, which is better than route [1,3,5,3,5].
 *
 * Example 3:
 * Input: heights = [[1,2,1,1,1],
 *                   [1,2,1,2,1],
 *                   [1,2,1,2,1],
 *                   [1,2,1,2,1],
 *                   [1,1,1,2,1]]
 * Output: 0
 * Explanation: This route does not require any effort.
 *
 * Constraints:
 *     ∙ rows == heights.length
 *     ∙ columns == heights[i].length
 *     ∙ 1 <= rows, columns <= 100
 *     ∙ 1 <= heights[i][j] <= 10⁶
 */

/**
 * @param {number[][]} heights
 * @return {number}
 */
var minimumEffortPath = function(heights) {
    function posToId(row, col) { return m*row + col; }
    const n = heights.length;
    const m = heights[0].length;
    const neighbours = new Map();;
    const distances = new Map();
    if (n == 1 && m == 1) { return 0; }
    for (let row = 0; row < n; ++row) {
        for (let col = 0; col < m; ++col) {
            for (const [dr, dc] of [[0, 1], [1, 0], [0, -1], [-1, 0]]) {
                const nr = row + dr, nc = col + dc;
                if (0 <= nr && nr < n && 0 <= nc && nc < m) {
                    const curId = posToId(row, col);
                    const newId = posToId(nr, nc);
                    neighbours.get(curId) ? neighbours.get(curId).push(newId)
                                          : neighbours.set(curId, [newId]);
                    const d = Math.abs(heights[row][col] - heights[nr][nc]);
                    distances.set([curId, newId].toString(), d);
                    distances.set([newId, curId].toString(), d);
                }
            }
        }
    }
    return dijkstra(neighbours, distances, 0).get(m*n-1);
};

/* Dijkstra algorithm to find shortest paths. */
function dijkstra(graph, distances, source) {
    const shortestPaths = new Map();
    [...graph.keys()].forEach(n => shortestPaths.set(n, Infinity));
    shortestPaths.set(source, 0);
    const toVisit = [[0, source]];
    while (toVisit.length) {
        const [parentDist, root] = toVisit.pop();
        for (const neib of graph.get(root)) {
            const curDist = distances.get([root, neib].toString());
            const dist = Math.max(parentDist, curDist);
            if (dist < shortestPaths.get(neib)) {
                shortestPaths.set(neib, dist);
                toVisit.splice(
                    bisectLeft(toVisit, [dist, neib], (a, b) => b[0] - a[0]),
                    0, [dist, neib]
                );
            }
        }
    }
    return shortestPaths;
}

function bisectLeft(a, x, cmp=(a, b) => a - b, lo=0, hi=a.length-1) {
    while (lo <= hi) {
        const mid = Math.floor((hi + lo) / 2);
        if (cmp(x, a[mid]) == 0) {
            return mid;
        } else if (cmp(x, a[mid]) > 0) {
            lo = mid + 1;
        } else {
            hi = mid - 1;
        }
    }
    return lo;
}

const tests = [
    [[[1,2,2],
      [3,8,2],
      [5,3,5]], 2],
    [[[1,2,3],
      [3,8,4],
      [5,3,5]], 1],
    [[[1,2,1,1,1],
      [1,2,1,2,1],
      [1,2,1,2,1],
      [1,2,1,2,1],
      [1,1,1,2,1]], 0],
    [[[10,8],
      [10,8],
      [1, 2],
      [10,3],
      [1, 3],
      [6, 3],
      [5, 2]], 6],
    [[[3]], 0],
    [[[1,10,6,7,9,10,4,9]], 9],
]
for (const [heights, expected] of tests) {
    console.log(minimumEffortPath(heights) === expected);
}
