"""
 * 661. Image Smoother [Easy]
 * An image smoother is a filter of the size 3 x 3 that can be applied to each
 * cell of an image by rounding down the average of the cell and the eight
 * surrounding cells (i.e., the average of the nine cells in the blue
 * smoother). If one or more of the surrounding cells of a cell is not present,
 * we do not consider it in the average (i.e., the average of the four cells in
 * the red smoother).
 *
 * Given an m x n integer matrix img representing the grayscale of an image,
 * return the image after applying the smoother on each cell of it.
 *
 * Example 1:
 * Input: img = [[1,1,1],
 *               [1,0,1],
 *               [1,1,1]]
 * Output: [[0,0,0],
 *          [0,0,0],
 *          [0,0,0]]
 * Explanation:
 * For the points (0,0), (0,2), (2,0), (2,2): floor(3/4) = floor(0.75) = 0
 * For the points (0,1), (1,0), (1,2), (2,1): floor(5/6) = floor(0.83333333) =
 * 0
 * For the point (1,1): floor(8/9) = floor(0.88888889) = 0
 *
 * Example 2:
 * Input: img = [[100,200,100],
 *               [200,50,200],
 *               [100,200,100]]
 * Output: [[137,141,137],
 *          [141,138,141],
 *          [137,141,137]]
 * Explanation:
 * For the points (0,0), (0,2), (2,0), (2,2): floor((100+200+200+50)/4) =
 * floor(137.5) = 137
 * For the points (0,1), (1,0), (1,2), (2,1):
 * floor((200+200+50+200+100+100)/6) = floor(141.666667) = 141
 * For the point (1,1): floor((50+200+200+200+200+100+100+100+100)/9) =
 * floor(138.888889) = 138
 *
 * Constraints:
 *     ∙ m == img.length
 *     ∙ n == img[i].length
 *     ∙ 1 <= m, n <= 200
 *     ∙ 0 <= img[i][j] <= 255
"""
from typing import List

class Solution:
    def imageSmoother(self, img: List[List[int]]) -> List[List[int]]:
        m = len(img)
        n = len(img[0])
        ret = [[0] * n for _ in range(m)]
        count = [[1] * n for _ in range(m)]

        img = [[0] * m] + [*zip(*img)] + [[0] * m]
        count = [[0] * m] + [*zip(*count)] + [[0] * m]
        m += 2
        n += 2
        img = [[0] * n] + [*zip(*img)] + [[0] * n]
        count = [[0] * n] + [*zip(*count)] + [[0] * n]

        for r in range(1, m - 1):
            for c in range(1, n - 1):
                s = 0
                s += img[r-1][c-1] + img[r-1][c] + img[r-1][c+1]
                s += img[r][c-1] + img[r][c] + img[r][c+1]
                s += img[r+1][c-1] + img[r+1][c] + img[r+1][c+1]

                k = 0
                k += count[r-1][c-1] + count[r-1][c] + count[r-1][c+1]
                k += count[r][c-1] + count[r][c] + count[r][c+1]
                k += count[r+1][c-1] + count[r+1][c] + count[r+1][c+1]

                ret[r-1][c-1] = s // k

        return ret

if __name__ == "__main__":
    tests = (
        ([[1,1,1],
          [1,0,1],
          [1,1,1]],
         [[0,0,0],
          [0,0,0],
          [0,0,0]]),
        ([[100,200,100],
          [200,50,200],
          [100,200,100]],
         [[137,141,137],
          [141,138,141],
          [137,141,137]]),
        ([[2,3,4],
          [5,6,7],
          [8,9,10],
          [11,12,13],
          [14,15,16]],
         [[4,4,5],
          [5,6,6],
          [8,9,9],
          [11,12,12],
          [13,13,14]]),
        ([[1]], [[1]]),
        ([[6,9,7]], [[7,7,8]]),
        ([[2,3]], [[2,2]]),
    )
    for img, expected in tests:
        print(Solution().imageSmoother(img) == expected)
