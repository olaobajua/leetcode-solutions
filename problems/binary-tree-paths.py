"""
 * 257. Binary Tree Paths [Easy]
 * Given the root of a binary tree, return all root-to-leaf paths in any
 * order.
 *
 * A leaf is a node with no children.
 *
 * Example 1:
 * Input: root = [1,2,3,null,5]
 * Output: ["1->2->5","1->3"]
 *
 * Example 2:
 * Input: root = [1]
 * Output: ["1"]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 100].
 *     ∙ -100 <= Node.val <= 100
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def binaryTreePaths(self, root: Optional[TreeNode]) -> List[str]:
        def dfs(node, path):
            path += (str(node.val),)
            if node.left:
                dfs(node.left, path)
            if node.right:
                dfs(node.right, path)
            if node.left is None and node.right is None:
                ret.append("->".join(path))
        ret = []
        dfs(root, ())
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3,None,5], ["1->2->5","1->3"]),
        ([1], ["1"]),
    )
    for nums, expected in tests:
        print(Solution().binaryTreePaths(build_tree(nums)) == expected)
