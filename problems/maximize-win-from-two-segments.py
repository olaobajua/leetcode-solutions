"""
 * 2555. Maximize Win From Two Segments [Medium]
 * There are some prizes on the X-axis. You are given an integer array
 * prizePositions that is sorted in non-decreasing order, where
 * prizePositions[i] is the position of the iᵗʰ prize. There could be different
 * prizes at the same position on the line. You are also given an integer k.
 *
 * You are allowed to select two segments with integer endpoints. The length
 * of each segment must be k. You will collect all prizes whose position falls
 * within at least one of the two selected segments (including the endpoints of
 * the segments). The two selected segments may intersect.
 *
 *     ∙ For example if k = 2, you can choose segments [1, 3] and [2, 4], and
 *       you will win any prize i that satisfies 1 <= prizePositions[i] <= 3 or
 *       2 <= prizePositions[i] <= 4.
 *
 * Return the maximum number of prizes you can win if you choose the two
 * segments optimally.
 *
 * Example 1:
 * Input: prizePositions = [1,1,2,2,3,3,5], k = 2
 * Output: 7
 * Explanation: In this example, you can win all 7 prizes by selecting two
 * segments [1, 3] and [3, 5].
 *
 * Example 2:
 * Input: prizePositions = [1,2,3,4], k = 0
 * Output: 2
 * Explanation: For this example, one choice for the segments is [3, 3] and
 * [4, 4], and you will be able to get 2 prizes.
 *
 * Constraints:
 *     ∙ 1 <= prizePositions.length <= 10⁵
 *     ∙ 1 <= prizePositions[i] <= 10⁹
 *     ∙ 0 <= k <= 10⁹
 *     ∙ prizePositions is sorted in non-decreasing order.
"""
from typing import List

class Solution:
    def maximizeWin(self, prizePositions: List[int], k: int) -> int:
        dp = [0] * (len(prizePositions) + 1)
        ret = left = 0
        for right in range(len(prizePositions)):
            while prizePositions[left] < prizePositions[right] - k:
                left += 1
            dp[right+1] = max(dp[right], right - left + 1)
            ret = max(ret, right - left + 1 + dp[left])
        return ret

if __name__ == "__main__":
    tests = (
        ([1,1,2,2,3,3,5], 2, 7),
        ([1,2,3,4], 0, 2),
        ([2616,2618,2620,2621,2626,2635,2657,2662,2662,2669,2671,2693,2702,2713,2714,2718,2730,2731,2750,2756,2772,2773,2775,2785,2795,2805,2811,2813,2816,2823,2824,2824,2826,2830,2833,2857,2885,2898,2910,2919,2928,2941,2942,2944,2965,2967,2970,2973,2974,2975,2977,3002,3007,3012,3042,3049,3078,3084,3089,3090,3094,3097,3114,3124,3125,3125,3144,3147,3148,3174,3197,3255,3262,3288,3291,3316,3320,3322,3331,3342,3378,3412,3412,3416,3420,3427,3428,3446,3452,3472,3479,3483,3488,3500,3516,3522,3531,3532,3540,3540,3544,3557,3570,3580,3592,3597,3597,3601,3615,3631,3640,3645,3673,3677,3681,3683,3685,3718,3738,3746,3758,3769,3797,3802,3815,3832,3839,3851,3864,3888,3889,3901,3902,3910,3913,3933,3940,3961,3974,3988,4003,4013,4019,4023,4026,4047,4060,4065,4072,4073,4082,4084,4109,4132,4139,4143,4145,4146,4155], 6641, 159),
    )
    for prizePositions, k, expected in tests:
        print(Solution().maximizeWin(prizePositions, k) == expected)
