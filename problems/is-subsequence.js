/**
 * 392. Is Subsequence (Easy)
 * Given two strings s and t, return true if s is a subsequence of t, or false
 * otherwise.
 *
 * A subsequence of a string is a new string that is formed from the original
 * string by deleting some (can be none) of the characters without disturbing
 * the relative positions of the remaining characters. (i.e., "ace" is a
 * subsequence of "abcde" while "aec" is not).
 *
 * Example 1:
 * Input: s = "abc", t = "ahbgdc"
 * Output: true
 *
 * Example 2:
 * Input: s = "axc", t = "ahbgdc"
 * Output: false
 *
 * Constraints:
 *     ∙ 0 <= s.length <= 100
 *     ∙ 0 <= t.length <= 10⁴
 *     ∙ s and t consist only of lowercase English letters.
 *
 * Follow up: Suppose there are lots of incoming s, say s₁, s₂, ..., sₖ where
 * k >= 10⁹, and you want to check one by one to see if t has its subsequence.
 * In this scenario, how would you change your code?
 */

/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isSubsequence = function(s, t) {
    const places = {};
    Array.from(t).forEach((symbol, i) => {
        places[symbol] ? places[symbol].push(i) : places[symbol] = [i];
    });

    let curIdx = 0;
    for (const symbol of s) {
        const idx = bisectLeft(places[symbol] || [], curIdx);
        if (idx >= (places[symbol] || []).length) {
            return false;
        }
        curIdx = places[symbol][idx] + 1;
    }
    return true;
};

function bisectLeft(a, x, cmp=(a, b) => a - b, lo=0, hi=a.length-1) {
    while (lo <= hi) {
        const mid = Math.floor((hi + lo) / 2);
        if (cmp(x, a[mid]) == 0) {
            return mid;
        } else if (cmp(x, a[mid]) > 0) {
            lo = mid + 1;
        } else {
            hi = mid - 1;
        }
    }
    return lo;
}

const tests = [
    ["abc", "ahbgdc", true],
    ["axc", "ahbgdc", false],
    ["acb", "ahbgdc", false],
    ["", "ahbgdc", true],
    ["aaaaaa", "bbaaaa", false],
];

for (const [s, t, expected] of tests) {
    console.log(isSubsequence(s, t) == expected);
}
