/**
 * 790. Domino and Tromino Tiling (Medium)
 * You have two types of tiles: a 2 x 1 domino shape and a tromino shape. You
 * may rotate these shapes.
 * ∙∙             ∙∙
 * domino tile    ∙
 *                tromino tile
 *
 * Given an integer n, return the number of ways to tile an 2 x n board. Since
 * the answer may be very large, return it modulo 109 + 7.
 *
 * In a tiling, every square must be covered by a tile. Two tilings are
 * different if and only if there are two 4-directionally adjacent cells on the
 * board such that exactly one of the tilings has both squares occupied by a
 * tile.
 *
 * Example 1:
 * 122 132 221
 * 133 132 331
 *
 * 112 122
 * 122 112
 *
 * Input: n = 3
 * Output: 5
 * Explanation: The five different ways are show above.
 *
 * Example 2:
 * Input: n = 1
 * Output: 1
 *
 * Constraints:
 *     1 <= n <= 1000
 */

/**
 * @param {number} n
 * @return {number}
 */
var numTilings = function(n) {
    let a = 0, b = 1, c = 1;
    for (let i = 0; i < n - 1; ++i) {
        [a, b, c] = [b, c, (2*c + a) % (1e9 + 7)];
    }
    return c;
};

const tests = [
    [1, 1],
    [2, 2],
    [3, 5],
    [4, 11],
    [5, 24],
    [6, 53],
    [7, 117],
    [8, 258],
    [9, 569],
    [10, 1255],
    [11, 2768],
    [12, 6105],
    [13, 13465],
    [14, 29698],
    [15, 65501],
    [16, 144467],
    [17, 318632],
    [18, 702765],
    [19, 1549997],
    [20, 3418626],
    [30, 312342182],
    [31, 539017241],
    [32, 300228551],
    [33, 912799284],
    [34, 364615795],
    [35, 29460134],
];

for (const [n, expected] of tests) {
    console.log(numTilings(n) == expected);
}
