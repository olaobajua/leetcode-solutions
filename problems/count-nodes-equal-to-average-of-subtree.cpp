/**
 * 2265. Count Nodes Equal to Average of Subtree [Medium]
 * Given the root of a binary tree, return the number of nodes where the value
 * of the node is equal to the average of the values in its subtree.
 *
 * Note:
 *     ∙ The average of n elements is the sum of the n elements divided by n
 *       and rounded down to the nearest integer.
 *     ∙ A subtree of root is a tree consisting of root and all of its
 *       descendants.
 *
 * Example 1:
 *    4
 *  8   5
 * 0 1  6
 * Input: root = [4,8,5,0,1,null,6]
 * Output: 5
 * Explanation:
 * For the node with value 4: The average of its subtree is (4 + 8 + 5 + 0 + 1
 * + 6) / 6 = 24 / 6 = 4.
 * For the node with value 5: The average of its subtree is (5 + 6) / 2 = 11 /
 * 2 = 5.
 * For the node with value 0: The average of its subtree is 0 / 1 = 0.
 * For the node with value 1: The average of its subtree is 1 / 1 = 1.
 * For the node with value 6: The average of its subtree is 6 / 1 = 6.
 *
 * Example 2:
 * Input: root = [1]
 * Output: 1
 * Explanation: For the node with value 1: The average of its subtree is 1 / 1
 * = 1.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 1000].
 *     ∙ 0 <= Node.val <= 1000
 */
#include <iostream>
#include <unordered_map>


// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    int averageOfSubtree(TreeNode* root) {
        ret = 0;
        dfs(root);
        return ret;
    }
private:
    int ret;

    void dfs(TreeNode* node) {
        if (node == nullptr) {
            return;
        }
        ret += node->val == subtreeSum(node) / countNodes(node);
        dfs(node->left);
        dfs(node->right);
    }

    std::unordered_map<TreeNode*, int> subtreeSumCache;
    int subtreeSum(TreeNode* node) {
        if (node == nullptr) {
            return 0;
        }
        if (subtreeSumCache.find(node) != subtreeSumCache.end()) {
            return subtreeSumCache[node];
        }
        int sum = node->val + subtreeSum(node->left) + subtreeSum(node->right);
        subtreeSumCache[node] = sum;
        return sum;
    }

    std::unordered_map<TreeNode*, int> countNodesCache;
    int countNodes(TreeNode* node) {
        if (node == nullptr) {
            return 0;
        }
        if (countNodesCache.find(node) != countNodesCache.end()) {
            return countNodesCache[node];
        }
        int count = 1 + countNodes(node->left) + countNodes(node->right);
        countNodesCache[node] = count;
        return count;
    }
};
