/**
 * 1930. Unique Length-3 Palindromic Subsequences [Medium]
 * Given a string s, return the number of unique palindromes of length three
 * that are a subsequence of s.
 *
 * Note that even if there are multiple ways to obtain the same subsequence,
 * it is still only counted once.
 *
 * A palindrome is a string that reads the same forwards and backwards.
 *
 * A subsequence of a string is a new string generated from the original
 * string with some characters (can be none) deleted without changing the
 * relative order of the remaining characters.
 *
 *     ∙ For example, "ace" is a subsequence of "abcde".
 *
 * Example 1:
 * Input: s = "aabca"
 * Output: 3
 * Explanation: The 3 palindromic subsequences of length 3 are:
 * - "aba" (subsequence of "aabca")
 * - "aaa" (subsequence of "aabca")
 * - "aca" (subsequence of "aabca")
 *
 * Example 2:
 * Input: s = "adc"
 * Output: 0
 * Explanation: There are no palindromic subsequences of length 3 in "adc".
 *
 * Example 3:
 * Input: s = "bbcbaba"
 * Output: 4
 * Explanation: The 4 palindromic subsequences of length 3 are:
 * - "bbb" (subsequence of "bbcbaba")
 * - "bcb" (subsequence of "bbcbaba")
 * - "bab" (subsequence of "bbcbaba")
 * - "aba" (subsequence of "bbcbaba")
 *
 * Constraints:
 *     ∙ 3 <= s.length <= 10⁵
 *     ∙ s consists of only lowercase English letters.
 */
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <string>

using namespace std;

class Solution {
public:
    int countPalindromicSubsequence(string s) {
        unordered_map<char, int> last;
        unordered_map<char, int> first;

        for (int i = 0; i < s.length(); ++i) {
            last[s[i]] = i;
            if (first.find(s[i]) == first.end()) {
                first[s[i]] = i;
            }
        }

        int ret = 0;
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            unordered_set<char> in_between;
            for (int i = first[ch] + 1; i < last[ch]; ++i) {
                in_between.insert(s[i]);
            }
            ret += in_between.size();
        }

        return ret;
    }
};
