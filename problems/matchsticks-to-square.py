"""
 * 473. Matchsticks to Square (Medium)
 * You are given an integer array matchsticks where matchsticks[i] is the
 * length of the iᵗʰ matchstick. You want to use all the matchsticks to make
 * one square. You should not break any stick, but you can link them up, and
 * each matchstick must be used exactly one time.
 *
 * Return true if you can make this square and false otherwise.
 *
 * Example 1:
 * Input: matchsticks = [1,1,2,2,2]
 * Output: true
 * Explanation: You can form a square with length 2, one side of the square
 * came two sticks with length 1.
 *
 * Example 2:
 * Input: matchsticks = [3,3,3,3,4]
 * Output: false
 * Explanation: You cannot find a way to form a square with all the
 * matchsticks.
 *
 * Constraints:
 *     ∙ 1 <= matchsticks.length <= 15
 *     ∙ 1 <= matchsticks[i] <= 10⁸
"""
from functools import cache
from typing import List

class Solution:
    def makesquare(self, matchsticks: List[int]) -> bool:
        @cache
        def dp(i, s1, s2, s3, s4):
            if i == n:
                return True
            for j in range(4):
                sides = [s1, s2, s3, s4]
                sides[j] += matchsticks[i]
                if sides[j] <= side and dp(i + 1, *sides):
                    return True
            return False

        matchsticks.sort(reverse=True)
        perimeter = sum(matchsticks)
        if perimeter % 4:
            return False
        side = perimeter // 4
        if perimeter - max(matchsticks) < 3 * side:
            return False
        n = len(matchsticks)
        return dp(0, 0, 0, 0, 0)

if __name__ == "__main__":
    tests = (
        ([1,1,2,2,2], True),
        ([3,3,3,3,4], False),
        ([5,5,5,5,4,4,4,4,3,3,3,3], True),
        ([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], True),
        ([13,11,1,8,6,7,8,8,6,7,8,9,8], True),
        ([4,4,4,4,8,8,8,8,1,1,1,1,4,4,8], True),
        ([5,5,5,5,16,4,4,4,4,4,3,3,3,3,4], False),
        ([10,1,1,1,1,1,1,1,1,1,1,1,1,1,1], False),
        ([100,100,100,100,100,100,100,100,4,100,2,2,100,100,100], False),
        ([5969561,8742425,2513572,3352059,9084275,2194427,1017540,2324577,6810719,8936380,7868365,2755770,9954463,9912280,4713511], False),
    )
    for matchsticks, expected in tests:
        print(Solution().makesquare(matchsticks) == expected, flush=True)
