/**
 * 941. Valid Mountain Array (Easy)
 * Given an array of integers arr, return true if and only if it is a valid
 * mountain array.
 *
 * Recall that arr is a mountain array if and only if:
 *     ▪ arr.length >= 3
 *     ▪ There exists some i with 0 < i < arr.length - 1 such that:
 *         ∙ arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
 *         ∙ arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
 *
 * Example 1:
 * Input: arr = [2,1]
 * Output: false
 *
 * Example 2:
 * Input: arr = [3,5,5]
 * Output: false
 *
 * Example 3:
 * Input: arr = [0,3,2,1]
 * Output: true
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 10⁴
 *     ∙ 0 <= arr[i] <= 10⁴
 */

/**
 * @param {number[]} arr
 * @return {boolean}
 */
var validMountainArray = function(arr) {
    const peak1 = arr.findIndex((m, i) => m <= arr[i-1]) - 1;
    const peak2 = arr.length - arr.reverse().findIndex((m, i) => m <= arr[i-1]);
    return peak1 == peak2 && 0 < peak1 && peak1 < arr.length - 1;
};

const tests = [
    [[2,1], false],
    [[3,5,5], false],
    [[0,3,2,1], true],
    [[9,8,7,6,5,4,3,2,1,0], false],
    [[0,1,2,4,2,1], true],
];

for (const [arr, expected] of tests) {
    console.log(validMountainArray(arr) == expected);
}
