"""
 * 1510. Stone Game IV (Hard)
 * Alice and Bob take turns playing a game, with Alice starting first.
 *
 * Initially, there are n stones in a pile. On each player's turn, that player
 * makes a move consisting of removing any non-zero square number of stones in
 * the pile.
 *
 * Also, if a player cannot make a move, he/she loses the game.
 *
 * Given a positive integer n, return true if and only if Alice wins the game
 * otherwise return false, assuming both players play optimally.
 *
 * Example 1:
 * Input: n = 1
 * Output: true
 * Explanation: Alice can remove 1 stone winning the game because Bob doesn't
 * have any moves.
 *
 * Example 2:
 * Input: n = 2
 * Output: false
 * Explanation: Alice can only remove 1 stone, after that Bob removes the last
 * one winning the game (2 -> 1 -> 0).
 *
 * Example 3:
 * Input: n = 4
 * Output: true
 * Explanation: n is already a perfect square, Alice can win with one move,
 * removing 4 stones (4 -> 0).
 *
 * Constraints:
 *     1 <= n <= 10⁵
"""
from functools import cache
from math import isqrt

class Solution:
    def winnerSquareGame(self, n: int) -> bool:
        @cache
        def i_can_win(n):
            return any(not i_can_win(n - m*m) for m in range(isqrt(n), 0, -1))

        # 1. The empty game (the game where there are no moves to be made) is a
        #    losing position.
        # 2. A position is a winning position if at least one of the positions
        #    that can be obtained from this position by a single move is a
        #    losing position.
        # 3. A position is a losing position if every position that can be
        #    obtained from this position by a single move is a winning
        #    position.
        # https://en.wikipedia.org/wiki/Zermelo%27s_theorem_(game_theory)
        return i_can_win(n)

if __name__ == "__main__":
    tests = (
        (1, True),  # 1 -> 0
        (2, False), # 1 -> 1 -> 0
        (4, True),  # 4 -> 0
        (5, False), # 1 -> 4 -> 0; 4 -> 1 -> 0
        (8, True),  # 1 -> 4 -> 1 -> 1 -> 1 -> 0; 1 -> 1 -> 4 -> 1 -> 1 -> 0
        (47, True),
        (9520, True),
    )
    for n, expected in tests:
        print(Solution().winnerSquareGame(n) == expected)
