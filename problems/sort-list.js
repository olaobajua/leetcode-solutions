/**
 * 148. Sort List (Medium)
 * Given the head of a linked list, return the list after sorting it in
 * ascending order.
 *
 * Example 1:
 * Input: head = [4,2,1,3]
 * Output: [1,2,3,4]
 *
 * Example 2:
 * Input: head = [-1,5,3,4,0]
 * Output: [-1,0,3,4,5]
 *
 * Example 3:
 * Input: head = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 5 * 10⁴].
 *     ∙ -10⁵ <= Node.val <= 10⁵
 *
 * Follow up: Can you sort the linked list in O(n logn) time and O(1) memory
 * (i.e. constant space)?
 */


// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var sortList = function(head) {
    if (head === null || head.next === null) {
        return head;
    }
    const mid = getMid(head);
    const left = sortList(head);
    const right = sortList(mid);
    return merge(left, right);
};

function merge(list1, list2) {
    const dummy = new ListNode();
    let tail = dummy;
    while (list1 != null && list2 != null) {
        if (list1.val < list2.val) {
            [tail.next, list1, tail] = [list1, list1.next, list1];
        } else {
            [tail.next, list2, tail] = [list2, list2.next, list2];
        }
    }
    tail.next = (list1 != null) ? list1 : list2;
    return dummy.next;
}

function getMid(head) {
    let prev = null;
    while (head != null && head.next != null) {
        prev = (prev == null) ? head : prev.next;
        head = head.next.next;
    }
    const mid = prev.next;
    prev.next = null;
    return mid;
}

function arrayToLinkedList(a) {
    return a && a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    [[4,2,1,3], [1,2,3,4]],
    [[-1,5,3,4,0], [-1,0,3,4,5]],
    [[], []],
];

for (const [nums, expected] of tests) {
    const ret = sortList(arrayToLinkedList(nums));
    console.log(!ret && !expected ||
                linkedListToArray(ret).toString() == expected.toString());
}
