"""
 * 1305. All Elements in Two Binary Search Trees (Medium)
 * Given two binary search trees root1 and root2, return a list containing all
 * the integers from both trees sorted in ascending order.
 *
 * Example 1:
 * Input: root1 = [2,1,4], root2 = [1,0,3]
 * Output: [0,1,1,2,3,4]
 *
 * Example 2:
 * Input: root1 = [1,null,8], root2 = [8,1]
 * Output: [1,1,8,8]
 *
 * Constraints:
 *     ∙ The number of nodes in each tree is in the range [0, 5000].
 *     ∙ -10⁵ <= Node.val <= 10⁵
"""
from typing import List

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def getAllElements(self, root1: TreeNode, root2: TreeNode) -> List[int]:
        return sorted([*tree_to_list(root1), *tree_to_list(root2)])

def tree_to_list(root):
    for n in (nodes := [root]):
        if n:
            yield n.val
            nodes.extend([n.left, n.right])

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([2,1,4], [1,0,3], [0,1,1,2,3,4]),
        ([1,None,8], [8,1], [1,1,8,8]),
    )
    for nums1, nums2, expected in tests:
        root1 = build_tree(nums1)
        root2 = build_tree(nums2)
        print(Solution().getAllElements(root1, root2) == expected)
