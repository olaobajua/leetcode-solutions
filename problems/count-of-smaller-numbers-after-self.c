/**
 * 315. Count of Smaller Numbers After Self [Hard]
 * You are given an integer array nums and you have to return a new counts
 * array. The counts array has the property where counts[i] is the number of
 * smaller elements to the right of nums[i].
 *
 * Example 1:
 * Input: nums = [5,2,6,1]
 * Output: [2,1,1,0]
 * Explanation:
 * To the right of 5 there are 2 smaller elements (2 and 1).
 * To the right of 2 there is only 1 smaller element (1).
 * To the right of 6 there is 1 smaller element (1).
 * To the right of 1 there is 0 smaller element.
 *
 * Example 2:
 * Input: nums = [-1]
 * Output: [0]
 *
 * Example 3:
 * Input: nums = [-1,-1]
 * Output: [0,0]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ -10⁴ <= nums[i] <= 10⁴
 */
#define BIT_LEN 20002
#define OFFSET 10001

int* countSmaller(int *nums, int numsSize, int *returnSize) {
    static int ret[100000];
    int bit[BIT_LEN] = {0};
    *returnSize = numsSize;
    for (int i = numsSize - 1; i >= 0; --i) {
        for (int j = nums[i] + OFFSET; j < BIT_LEN; j += j & -j)
            ++bit[j];
        ret[i] = 0;
        for (int j = nums[i] + OFFSET - 1; j; j -= j & -j)
            ret[i] += bit[j];
    };
    return ret;
}
