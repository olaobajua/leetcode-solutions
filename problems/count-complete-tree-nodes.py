"""
 * 222. Count Complete Tree Nodes (Medium)
 * Given the root of a complete binary tree, return the number of the nodes in
 * the tree.
 *
 * According to Wikipedia, every level, except possibly the last, is completely
 * filled in a complete binary tree, and all nodes in the last level are as far
 * left as possible. It can have between 1 and 2^h nodes inclusive at the last
 * level h.
 *
 * Design an algorithm that runs in less than O(n) time complexity.
 *
 * Example 1:
 * Input: root = [1,2,3,4,5,6]
 * Output: 6
 *
 * Example 2:
 * Input: root = []
 * Output: 0
 *
 * Example 3:
 * Input: root = [1]
 * Output: 1
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [0, 5 * 10⁴].
 *     0 <= Node.val <= 5 * 10⁴
 *     The tree is guaranteed to be complete.
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def countNodes(self, root: Optional[TreeNode]) -> int:
        if (h := height(root)) < 0:
            return 0
        if height(root.right) == h - 1:
            return (1 << h) + self.countNodes(root.right)
        else:
            return (1 << h - 1) + self.countNodes(root.left)

def height(root):
    return 1 + height(root.left) if root else -1

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5,6], 6),
        ([], 0),
        ([1], 1),
        ([1,2,3,4], 4),
    )
    for nums, expected in tests:
        print(Solution().countNodes(build_tree(nums)) == expected)
