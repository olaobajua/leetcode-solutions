/**
 * 1143. Longest Common Subsequence (Medium)
 * Given two strings text1 and text2, return the length of their longest common
 * subsequence. If there is no common subsequence, return 0.
 *
 * A subsequence of a string is a new string generated from the original string
 * with some characters (can be none) deleted without changing the relative
 * order of the remaining characters.
 *     ∙ For example, "ace" is a subsequence of "abcde".
 *
 * A common subsequence of two strings is a subsequence that is common to both
 * strings.
 *
 * Example 1:
 * Input: text1 = "abcde", text2 = "ace"
 * Output: 3
 * Explanation: The longest common subsequence is "ace" and its length is 3.
 *
 * Example 2:
 * Input: text1 = "abc", text2 = "abc"
 * Output: 3
 * Explanation: The longest common subsequence is "abc" and its length is 3.
 *
 * Example 3:
 * Input: text1 = "abc", text2 = "def"
 * Output: 0
 * Explanation: There is no such common subsequence, so the result is 0.
 *
 * Constraints:
 *     1 <= text1.length, text2.length <= 1000
 *     text1 and text2 consist of only lowercase English characters.
 */

/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */
var longestCommonSubsequence = function(text1, text2) {
    let len1 = text1.length;
    let len2 = text2.length;
    let longest_sub = [...Array(len1)].map(x => Array(len2).fill(0));
    longest_sub[-1] = new Array(len2).fill(0);
    longest_sub[-1][-1] = 0;
    for (let i = 0; i < len1; ++i) {
        longest_sub[i][-1] = 0;
    }
    for (let i = 0; i < len1; ++i) {
        for (let j = 0; j < len2; ++j) {
            if (text1[i] === text2[j]) {
                longest_sub[i][j] = longest_sub[i - 1][j - 1] + 1;
            } else {
                longest_sub[i][j] = Math.max(longest_sub[i - 1][j],
                                             longest_sub[i][j - 1]);
            }
        }
    }
    return longest_sub[len1 - 1][len2 - 1];
};

tests = [
    ["abcde", "ace", 3],
    ["abc", "abc", 3],
    ["abc", "def", 0],
    ["bl", "yby", 1],
];
for (let [text1, text2, expected] of tests) {
    console.log(longestCommonSubsequence(text1, text2) == expected);
}
