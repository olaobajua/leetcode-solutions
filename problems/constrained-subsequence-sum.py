"""
 * 1425. Constrained Subsequence Sum (Hard)
 * Given an integer array nums and an integer k, return the maximum sum of a
 * non-empty subsequence of that array such that for every two consecutive
 * integers in the subsequence, nums[i] and nums[j], where i < j,
 * the condition j - i <= k is satisfied.
 *
 * A subsequence of an array is obtained by deleting some number of elements
 * (can be zero) from the array, leaving the remaining elements in their
 * original order.
 *
 * Example 1:
 * Input: nums = [10,2,-10,5,20], k = 2
 * Output: 37
 * Explanation: The subsequence is [10, 2, 5, 20].
 *
 * Example 2:
 * Input: nums = [-1,-2,-3], k = 1
 * Output: -1
 * Explanation: The subsequence must be non-empty, so we choose the largest
 * number.
 *
 * Example 3:
 * Input: nums = [10,-2,-10,-5,20], k = 2
 * Output: 23
 * Explanation: The subsequence is [10, -2, -5, 20].
 *
 * Constraints:
 *     ∙ 1 <= k <= nums.length <= 10⁵
 *     ∙ -10⁴ <= nums[i] <= 10⁴
"""
from typing import List

class Solution:
    def constrainedSubsetSum(self, nums: List[int], k: int) -> int:
        queue = [0]
        for i in range(1, len(nums)):
            if queue[0] < i - k:
                queue.pop(0)
            nums[i] += max(0, nums[queue[0]])
            while queue and nums[queue[-1]] <= nums[i]:
                queue.pop()
            queue.append(i)
        return max(nums)

if __name__ == "__main__":
    tests = (
        ([10,2,-10,5,20], 2, 37),
        ([-1,-2,-3], 1, -1),
        ([10,-2,-10,-5,20], 2, 23),
        ([-5266,4019,7336,-3681,-5767], 2, 11355),
    )
    for nums, k, expected in tests:
        print(Solution().constrainedSubsetSum(nums, k) == expected)
