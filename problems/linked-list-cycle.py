"""
 * 141. Linked List Cycle (Easy)
 * Given head, the head of a linked list, determine if the linked list has a
 * cycle in it.
 *
 * There is a cycle in a linked list if there is some node in the list that can
 * be reached again by continuously following the next pointer. Internally, pos
 * is used to denote the index of the node that tail's next pointer is
 * connected to. Note that pos is not passed as a parameter.
 *
 * Return true if there is a cycle in the linked list. Otherwise, return false.
 *
 * Example 1:
 * Input: head = [3,2,0,-4], pos = 1
 * Output: true
 * Explanation: There is a cycle in the linked list, where the tail connects to
 * the 1ˢᵗ node (0-indexed).
 *
 * Example 2:
 * Input: head = [1,2], pos = 0
 * Output: true
 * Explanation: There is a cycle in the linked list, where the tail connects to
 * the 0ᵗʰ node.
 *
 * Example 3:
 * Input: head = [1], pos = -1
 * Output: false
 * Explanation: There is no cycle in the linked list.
 *
 * Constraints:
 *     ∙ The number of the nodes in the list is in the range [0, 10⁴].
 *     ∙ -10⁵ <= Node.val <= 10⁵
 *     ∙ pos is -1 or a valid index in the linked-list.
 *
 * Follow up: Can you solve it using O(1) (i.e. constant) memory?
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x, next=None):
        self.val = x
        self.next = next

class Solution:
    def hasCycle(self, head: Optional[ListNode]) -> bool:
        fast = slow = head
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
            if fast == slow:
                return True

        return False

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

if __name__ == "__main__":
    tests = (
        ([3,2,0,-4], 1, True),
        ([1,2], 0, True),
        ([1], -1, False),
    )
    for nums, pos, expected in tests:
        end = mid = root = list_to_linked_list(nums)
        while end and end.next:
            end = end.next
        if pos >= 0:
            for _ in range(pos):
                mid = mid.next
            end.next = mid
        print(bool(Solution().hasCycle(root)) == expected)
