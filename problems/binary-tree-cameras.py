"""
 * 968. Binary Tree Cameras (Hard)
 * You are given the root of a binary tree. We install cameras on the tree
 * nodes where each camera at a node can monitor its parent, itself, and its
 * immediate children.
 *
 * Return the minimum number of cameras needed to monitor all nodes of the
 * tree.
 *
 * Example 1:
 * Input: root = [0,0,null,0,0]
 * Output: 1
 * Explanation: One camera is enough to monitor all nodes if placed as shown.
 *
 * Example 2:
 * Input: root = [0,0,null,0,null,0,null,null,0]
 * Output: 2
 * Explanation: At least two cameras are needed to monitor all nodes of the
 * tree. The above image shows one of the valid configurations of camera
 * placement.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 1000].
 *     ∙ Node.val == 0
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def minCameraCover(self, root: Optional[TreeNode]) -> int:
        def dfs(node):
            if node :
                left = dfs(node.left)
                right = dfs(node.right)
                if left == 2 and right == 2:  # leaf
                    return 0
                if left == 0 or right == 0:  # leaf parent
                    nonlocal cams
                    cams += 1  # here we install 1 cam
                    return 1
                if left == 1 or right == 1: # covered by children node, new end
                    return 2
            return 2  # empty node, end

        cams = 0
        return (dfs(root) == 0) + cams

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([0,0,None,0,0], 1),
        ([0,0,None,0,None,0,None,None,0], 2),
        ([0], 1),
        ([0,0,None,None,0,0,None,None,0,0], 2),
        ([0,None,0,None,0,None,0], 2),
        ([0,None,0,None,0,0,0], 2),
        ([0,0,0,None,0,0,None,None,0], 2),
        ([0,0,None,None,0,0,0,None,None,None,0,None,0,None,0], 3),
        ([0,0,None,0,0,0,None,None,0,0,None,None,0,None,0], 3),
        ([0,None,0,None,0,None,0,None,0,None,0,0,0,None,0,None,0], 4),
        ([0,None,0,0,0,None,0,None,0,None,None,0,None,0,None,None,0,None,0,0], 4),
        ([0,0,None,0,None,None,0,None,0,0,0,None,0,None,0,None,None,None,0,0,0,None,None,0], 5),
    )
    for nums, expected in tests:
        root = build_tree(nums)
        print(Solution().minCameraCover(root) == expected)
