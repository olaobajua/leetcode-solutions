"""
 * 665. Non-decreasing Array (Medium)
 * Given an array nums with n integers, your task is to check if it could
 * become non-decreasing by modifying at most one element.
 *
 * We define an array is non-decreasing if nums[i] <= nums[i + 1] holds for
 * every i (0-based) such that (0 <= i <= n - 2).
 *
 * Example 1:
 * Input: nums = [4,2,3]
 * Output: true
 * Explanation: You could modify the first 4 to 1 to get a non-decreasing
 * array.
 *
 * Example 2:
 * Input: nums = [4,2,1]
 * Output: false
 * Explanation: You can't get a non-decreasing array by modify at most one
 * element.
 *
 * Constraints:
 *     ∙ n == nums.length
 *     ∙ 1 <= n <= 10⁴
 *     ∙ -10⁵ <= nums[i] <= 10⁵
"""
from typing import List

class Solution:
    def checkPossibility(self, nums: List[int]) -> bool:
        p = -1
        n = len(nums)
        for i in range(n - 1):
            if nums[i] > nums[i+1]:
                if p != -1:
                    return False
                p = i
        if p <= 0 or p == n - 2:
            return True;
        return nums[p-1] <= nums[p+1] or nums[p] <= nums[p+2]


if __name__ == "__main__":
    tests = (
        ([4,2,3], True),
        ([4,2,1], False),
        ([1,2,3,4,2,3], False),
        ([1], True),
        ([1,2,3], True),
        ([5,7,1,8], True),
        ([5,7,1], True),
        ([1,4,1,2], True),
    )
    for nums, expected in tests:
        print(Solution().checkPossibility(nums) == expected)
