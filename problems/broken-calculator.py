"""
 * 991. Broken Calculator (Medium)
 * There is a broken calculator that has the integer startValue on its display
 * initially. In one operation, you can:
 *     ∙ multiply the number on display by 2, or
 *     ∙ subtract 1 from the number on display.
 *
 * Given two integers startValue and target, return the minimum number of
 * operations needed to display target on the calculator.
 *
 * Example 1:
 * Input: startValue = 2, target = 3
 * Output: 2
 * Explanation: Use double operation and then decrement operation
 * {2 -> 4 -> 3}.
 *
 * Example 2:
 * Input: startValue = 5, target = 8
 * Output: 2
 * Explanation: Use decrement and then double {5 -> 4 -> 8}.
 *
 * Example 3:
 * Input: startValue = 3, target = 10
 * Output: 3
 * Explanation: Use double, decrement and double {3 -> 6 -> 5 -> 10}.
 *
 * Constraints:
 *     ∙ 1 <= x, y <= 10⁹
"""
class Solution:
    def brokenCalc(self, startValue: int, target: int) -> int:
        if startValue >= target:
            return startValue - target
        else:
            return 1 + target%2 + self.brokenCalc(startValue, (target + 1)//2)

if __name__ == "__main__":
    tests = (
        (2, 3, 2),
        (5, 8, 2),
        (3, 10, 3),
        (1024, 1, 1023),
        (1, 1, 0),
        (1, 1000000000, 39),
    )
    for startValue, target, expected in tests:
        print(Solution().brokenCalc(startValue, target) == expected)
