"""
 * 2009. Minimum Number of Operations to Make Array Continuous [Hard]
 * You are given an integer array nums. In one operation, you can replace any
 * element in nums with any integer.
 *
 * nums is considered continuous if both of the following conditions are
 * fulfilled:
 *     ∙ All elements in nums are unique.
 *     ∙ The difference between the maximum element and the minimum element in
 *       nums equals nums.length - 1.
 *
 * For example, nums = [4, 2, 5, 3] is continuous, but nums = [1, 2, 3, 5, 6]
 * is not continuous.
 *
 * Return the minimum number of operations to make nums continuous.
 *
 * Example 1:
 * Input: nums = [4,2,5,3]
 * Output: 0
 * Explanation: nums is already continuous.
 *
 * Example 2:
 * Input: nums = [1,2,3,5,6]
 * Output: 1
 * Explanation: One possible solution is to change the last element to 4.
 * The resulting array is [1,2,3,5,4], which is continuous.
 *
 * Example 3:
 * Input: nums = [1,10,100,1000]
 * Output: 3
 * Explanation: One possible solution is to:
 * - Change the second element to 2.
 * - Change the third element to 3.
 * - Change the fourth element to 4.
 * The resulting array is [1,2,3,4], which is continuous.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i] <= 10⁹
"""
from bisect import bisect_left
from math import inf
from typing import List

class Solution:
    def minOperations(self, nums: List[int]) -> int:
        n = len(nums)
        sorted_unique = sorted(set(nums))
        ret = inf
        for i, x in enumerate(sorted_unique):
            end = bisect_left(sorted_unique, x + n - 1)
            if end >= len(sorted_unique) or sorted_unique[end] != x + n - 1:
                end -= 1
            continuous = end - i
            ret = min(ret, n - continuous - 1)
        return ret

if __name__ == "__main__":
    tests = (
        ([4,2,5,3], 0),
        ([1,2,3,5,6], 1),
        ([1,10,100,1000], 3),
        ([8,5,9,9,8,4], 2),
    )
    for nums, expected in tests:
        print(Solution().minOperations(nums) == expected)
