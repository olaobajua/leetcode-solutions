/**
 * 1008. Construct Binary Search Tree from Preorder Traversal (Medium)
 * Given an array of integers preorder, which represents the preorder traversal
 * of a BST (i.e., binary search tree), construct the tree and return its root.
 *
 * It is guaranteed that there is always possible to find a binary search tree
 * with the given requirements for the given test cases.
 *
 * A binary search tree is a binary tree where for every node, any descendant
 * of Node.left has a value strictly less than Node.val, and any descendant of
 * Node.right has a value strictly greater than Node.val.
 *
 * A preorder traversal of a binary tree displays the value of the node first,
 * then traverses Node.left, then traverses Node.right.
 *
 * Example 1:
 * Input: preorder = [8,5,1,7,10,12]
 * Output: [8,5,10,1,7,null,12]
 *
 * Example 2:
 * Input: preorder = [1,3]
 * Output: [1,null,3]
 *
 * Constraints:
 *     1 <= preorder.length <= 100
 *     1 <= preorder[i] <= 10⁸
 *     All the values of preorder are unique.
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {number[]} preorder
 * @return {TreeNode}
 */
var bstFromPreorder = function(preorder) {
    let root = prev = new TreeNode(preorder.shift());
    let nodes = [prev];
    while (preorder.length) {
        let val = preorder.shift();
        if (val < prev.val) {
            prev.left = new TreeNode(val);
            prev = prev.left;
            nodes.push(prev);
        } else {
            while (nodes.length && nodes[nodes.length-1].val < val) {
                prev = nodes.pop();
            }
            prev.right = new TreeNode(val);
            prev = prev.right;
            nodes.push(prev);
        }
    }
    return root
};

function* tree_to_list(root) {
    let nodes = [root];
    for (let n of nodes) {
        if (n) {
            yield n.val;
            nodes.push(n.left, n.right);
        } else {
            yield null;
        }
    }
}

tests = [
    [[8,5,1,7,10,12], [8,5,10,1,7,null,12]],
    [[1,3], [1,null,3]],
];

for (let [preorder, expected] of tests) {
    let bst = [...tree_to_list(bstFromPreorder(preorder))];
    console.log(expected.every((n, i) => n === bst[i]));
}
