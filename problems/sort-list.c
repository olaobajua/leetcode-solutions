/**
 * 148. Sort List (Medium)
 * Given the head of a linked list, return the list after sorting it in
 * ascending order.
 *
 * Example 1:
 * Input: head = [4,2,1,3]
 * Output: [1,2,3,4]
 *
 * Example 2:
 * Input: head = [-1,5,3,4,0]
 * Output: [-1,0,3,4,5]
 *
 * Example 3:
 * Input: head = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 5 * 10⁴].
 *     ∙ -10⁵ <= Node.val <= 10⁵
 *
 * Follow up: Can you sort the linked list in O(n logn) time and O(1) memory
 * (i.e. constant space)?
 */

// Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode *next;
};

int compare(const int *a, const int *b) {
  return *a - *b;
}

struct ListNode* sortList(struct ListNode *head) {
    struct ListNode *cur = head;
    static int stack[50000];
    int n = 0;
    while (cur) {
        stack[n++] = cur->val;
        cur = cur->next;
    }
    qsort(stack, n, sizeof(int), compare);
    cur = head;
    for (int i = 0; i < n; ++i) {
        cur->val = stack[i];
        cur = cur->next;
    }
    return head;
}

int getCount(struct ListNode *head) {
    int ret = 0;
    while (head) {
        head = head->next;
        ++ret;
    }
    return ret;
}
