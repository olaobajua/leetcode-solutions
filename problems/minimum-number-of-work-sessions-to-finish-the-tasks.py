"""
 * 1986. Minimum Number of Work Sessions to Finish the Tasks (Medium)
 * There are n tasks assigned to you. The task times are represented as an
 * integer array tasks of length n, where the ith task takes tasks[i] hours to
 * finish. A work session is when you work for at most sessionTime consecutive
 * hours and then take a break.
 *
 * You should finish the given tasks in a way that satisfies the following
 * conditions:
 *     ∙ If you start a task in a work session, you must complete it in the
 *       same work session.
 *     ∙ You can start a new task immediately after finishing the previous one.
 *     ∙ You may complete the tasks in any order.
 *
 * Given tasks and sessionTime, return the minimum number of work sessions
 * needed to finish all the tasks following the conditions above.
 *
 * The tests are generated such that sessionTime is greater than or equal to
 * the maximum element in tasks[i].
 *
 * Example 1:
 * Input: tasks = [1,2,3], sessionTime = 3
 * Output: 2
 * Explanation: You can finish the tasks in two work sessions.
 * - First work session: finish the first and the second tasks
 *   in 1 + 2 = 3 hours.
 * - Second work session: finish the third task in 3 hours.
 *
 * Example 2:
 * Input: tasks = [3,1,3,1,1], sessionTime = 8
 * Output: 2
 * Explanation: You can finish the tasks in two work sessions.
 * - First work session: finish all the tasks except the last one
 *   in 3 + 1 + 3 + 1 = 8 hours.
 * - Second work session: finish the last task in 1 hour.
 *
 * Example 3:
 * Input: tasks = [1,2,3,4,5], sessionTime = 15
 * Output: 1
 * Explanation: You can finish all the tasks in one work session.
 *
 * Constraints:
 *     n == tasks.length
 *     1 <= n <= 14
 *     1 <= tasks[i] <= 10
 *     max(tasks[i]) <= sessionTime <= 15
"""
from functools import cache
from typing import List

class Solution:
    def minSessions(self, tasks: List[int], sessionTime: int) -> int:
        len_tasks = len(tasks)

        @cache
        def min_sessions(sessions, time_left, tasks):
            if tasks:
                ms = len_tasks
                for i, task in enumerate(tasks):
                    if time_left - task >= 0:
                        ms = min(ms, min_sessions(sessions, time_left - task,
                                                  tasks[:i] + tasks[i+1:]))
                    else:
                        ms = min(ms, min_sessions(sessions + 1, sessionTime,
                                                  tasks))
                        break
                return ms
            else:
                return sessions
        return min_sessions(1, sessionTime, tuple(sorted(tasks, reverse=True)))

if __name__ == "__main__":
    tests = (
        ([1,2,3], 3, 2),
        ([3,1,3,1,1], 8, 2),
        ([1,2,3,4,5], 15, 1),
        ([10,3,8,8,1,10], 13, 4),
        ([2,3,3,4,4,4,5,6,7,10], 12, 4),
        ([2,10,1,10,4,4,7,10,7,4,10,2], 15, 5),
        ([1,2,2,3,4,5,6,6,7,8,8,8], 15, 4),
    )
    from time import time
    start = time()
    for tasks, sessionTime, expected in tests:
        print(Solution().minSessions(tasks, sessionTime) == expected, flush=True)
    print(f"Time elapsed: {time() - start} sec")
