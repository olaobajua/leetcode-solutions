"""
 * 525. Contiguous Array (Medium)
 * Given a binary array nums, return the maximum length of a contiguous
 * subarray with an equal number of 0 and 1.
 *
 * Example 1:
 * Input: nums = [0,1]
 * Output: 2
 * Explanation: [0, 1] is the longest contiguous subarray with an equal number
 * of 0 and 1.
 *
 * Example 2:
 * Input: nums = [0,1,0]
 * Output: 2
 * Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal
 * number of 0 and 1.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ nums[i] is either 0 or 1.
"""
from typing import List

class Solution:
    def findMaxLength(self, nums: List[int]) -> int:
        diffs = {0:0}
        ret = ones = zeroes = 0
        for i, x in enumerate(nums, 1):
            if x == 0:
                zeroes += 1
            else:
                ones += 1
            diffs.setdefault(ones - zeroes, i)
            ret = max(ret, i - diffs.get(ones - zeroes, i))
        return ret

if __name__ == "__main__":
    tests = (
        ([0,1], 2),
        ([0,1,0], 2),
        ([0,0,1,0,0,0,1,1], 6),
        ([1,0,0,0,0,1,0,1,0,1,0,0,0,0], 6),
        ([1,1,1,1,1,1,1,1], 0),
    )
    for nums, expected in tests:
        print(Solution().findMaxLength(nums) == expected)
