"""
 * 827. Making A Large Island (Hard)
 * You are given an n x n binary matrix grid. You are allowed to change at most one 0 to be 1.
 * Return the size of the largest island in grid after applying this operation.
 * An island is a 4-directionally connected group of 1s.
 *
 * Example 1:
 * Input: grid = [[1,0],[0,1]]
 * Output: 3
 * Explanation: Change one 0 to 1 and connect two 1s, then we get an island with area = 3.
 *
 * Example 2:
 * Input: grid = [[1,1],[1,0]]
 * Output: 4
 * Explanation: Change the 0 to 1 and make the island bigger, only one island with area = 4.
 *
 * Example 3:
 * Input: grid = [[1,1],[1,1]]
 * Output: 4
 * Explanation: Can't change any 0 to 1, only one island with area = 4.
 *
 * Constraints:
 *     n == grid.length
 *     n == grid[i].length
 *     1 <= n <= 500
 *     grid[i][j] is either 0 or 1.
"""
from collections import Counter
from typing import List

def flood_fill_dfs(matrix, x, y, old, new):
    if matrix[x][y] == old:
        matrix[x][y] = new
        if x > 0:
            flood_fill_dfs(matrix, x - 1, y, old, new)
        if x < len(matrix) - 1:
            flood_fill_dfs(matrix, x + 1, y, old, new)
        if y > 0:
            flood_fill_dfs(matrix, x, y - 1, old, new)
        if y < len(matrix[x]) - 1:
            flood_fill_dfs(matrix, x, y + 1, old, new)

def new_area(matrix, areas, x, y):
    area = 1
    united = {0}
    if matrix[x][y] == 0:
        if x > 0 and matrix[x - 1][y] not in united:
            united.add(matrix[x - 1][y])
            area += areas[matrix[x - 1][y]]
        if x < len(matrix) - 1 and matrix[x + 1][y] not in united:
            united.add(matrix[x + 1][y])
            area += areas[matrix[x + 1][y]]
        if y > 0 and matrix[x][y - 1] not in united:
            united.add(matrix[x][y - 1])
            area += areas[matrix[x][y - 1]]
        if y < len(matrix[x]) - 1 and matrix[x][y + 1] not in united:
            united.add(matrix[x][y + 1])
            area += areas[matrix[x][y + 1]]
    return area

class Solution:
    def largestIsland(self, grid: List[List[int]]) -> int:
        isle_n = 2
        for x in range(len(grid)):
            for y in range(len(grid[x])):
                if grid[x][y] == 1:
                    flood_fill_dfs(grid, x, y, 1, isle_n)
                    isle_n += 1
        isles = Counter(item for row in grid for item in row if item)
        return max(max(isles.values(), default=0),
                   max(new_area(grid, isles, x, y)
                       for x in range(len(grid))
                       for y in range(len(grid[x]))))

if __name__ == "__main__":
    pass
