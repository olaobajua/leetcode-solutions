/**
 * 2146. K Highest Ranked Items Within a Price Range (Medium)
 * You are given a 0-indexed 2D integer array grid of size m x n that
 * represents a map of the items in a shop. The integers in the grid represent
 * the following:
 *     ∙ 0 represents a wall that you cannot pass through.
 *     ∙ 1 represents an empty cell that you can freely move to and from.
 *     ∙ All other positive integers represent the price of an item in that
 *       cell. You may also freely move to and from these item cells.
 *
 * It takes 1 step to travel between adjacent grid cells.
 *
 * You are also given integer arrays pricing and start where
 * pricing = [low, high] and start = [row, col] indicates that you start at the
 * position (row, col) and are interested only in items with a price in the
 * range of [low, high] (inclusive). You are further given an integer k.
 *
 * You are interested in the positions of the k highest-ranked items whose
 * prices are within the given price range. The rank is determined by the
 * first of these criteria that is different:
 *     ∙ Distance, defined as the length of the shortest path from the start
 *       (shorter distance has a higher rank).
 *     ∙ Price (lower price has a higher rank, but it must be in the price
 *       range).
 *     ∙ The row number (smaller row number has a higher rank).
 *     ∙ The column number (smaller column number has a higher rank).
 *
 * Return the k highest-ranked items within the price range sorted by their
 * rank (highest to lowest). If there are fewer than k reachable items within
 * the price range, return all of them.
 *
 * Example 1:
 * Input: grid = [[1,2,0,1],
 *                [1,3,0,1],
 *                [0,2,5,1]], pricing = [2,5], start = [0,0], k = 3
 * Output: [[0,1],[1,1],[2,1]]
 * Explanation: You start at (0,0).
 * With a price range of [2,5], we can take items from (0,1), (1,1), (2,1) and
 * (2,2).
 * The ranks of these items are:
 * - (0,1) with distance 1
 * - (1,1) with distance 2
 * - (2,1) with distance 3
 * - (2,2) with distance 4
 * Thus, the 3 highest ranked items in the price range are (0,1), (1,1), and
 * (2,1).
 *
 * Example 2:
 * Input: grid = [[1,2,0,1],
 *                [1,3,3,1],
 *                [0,2,5,1]], pricing = [2,3], start = [2,3], k = 2
 * Output: [[2,1],[1,2]]
 * Explanation: You start at (2,3).
 * With a price range of [2,3], we can take items from (0,1), (1,1), (1,2) and
 * (2,1).
 * The ranks of these items are:
 * - (2,1) with distance 2, price 2
 * - (1,2) with distance 2, price 3
 * - (1,1) with distance 3
 * - (0,1) with distance 4
 * Thus, the 2 highest ranked items in the price range are (2,1) and (1,2).
 *
 * Example 3:
 * Input: grid = [[1,1,1],
 *                [0,0,1],
 *                [2,3,4]], pricing = [2,3], start = [0,0], k = 3
 * Output: [[2,1],[2,0]]
 * Explanation: You start at (0,0).
 * With a price range of [2,3], we can take items from (2,0) and (2,1).
 * The ranks of these items are:
 * - (2,1) with distance 5
 * - (2,0) with distance 6
 * Thus, the 2 highest ranked items in the price range are (2,1) and (2,0).
 * Note that k = 3 but there are only 2 reachable items within the price range.
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 10⁵
 *     ∙ 1 <= m * n <= 10⁵
 *     ∙ 0 <= grid[i][j] <= 10⁵
 *     ∙ pricing.length == 2
 *     ∙ 2 <= low <= high <= 10⁵
 *     ∙ start.length == 2
 *     ∙ 0 <= row <= m - 1
 *     ∙ 0 <= col <= n - 1
 *     ∙ grid[row][col] > 0
 *     ∙ 1 <= k <= m * n
 */

/**
 * @param {number[][]} grid
 * @param {number[]} pricing
 * @param {number[]} start
 * @param {number} k
 * @return {number[][]}
 */
var highestRankedKItems = function(grid, pricing, start, k) {
    const m = grid.length;
    const n = grid[0].length;
    const [lo, hi] = pricing;
    const visited = [...Array(m)].map(_ => Array(n).fill(false));
    visited[start[0]][start[1]] = true;
    const toVisit = [[0, ...start]];  // distance, row, col
    const ret = [];
    for (const [distance, row, col] of toVisit) {
        if (lo <= grid[row][col] && grid[row][col] <= hi) {
            ret.push([distance, grid[row][col], row, col]);
        }
        for (const [r, c] of [[row,col+1],[row,col-1],[row-1,col],[row+1,col]]) {
            if (0 <= r && r < m && 0 <= c && c < n && grid[r][c] && !visited[r][c]) {
                visited[r][c] = true;
                toVisit.push([distance + 1, r, c]);
            }
        }
    }
    ret.sort(([distance1, price1, row1, col1], [distance2, price2, row2, col2]) => {
        return distance1 - distance2 || price1 - price2 || row1 - row2 || col1 - col2
    });
    return ret.slice(0, k).map(([distance, price, row, col]) => [row, col]);
};

const tests = [
    [[[1,2,0,1],
      [1,3,0,1],
      [0,2,5,1]], [2,5], [0,0], 3, [[0,1],[1,1],[2,1]]],
    [[[1,2,0,1],
      [1,3,3,1],
      [0,2,5,1]], [2,3], [2,3], 2, [[2,1],[1,2]]],
    [[[1,1,1],
      [0,0,1],
      [2,3,4]], [2,3], [0,0], 3, [[2,1],[2,0]]],
    [[[2,0,3],
      [2,0,3],
      [2,0,3]],
     [2,3], [0,0], 6, [[0,0],[1,0],[2,0]]],
    [[[255,366,223,492,122,154,755,163],
      [800,606,973,531,670,462,310,564],
      [340,  1,237,839,469,  1,258,703]],
     [690,842], [0,6], 4, [[0,6],[2,7],[2,3],[1,0]]],
    [[[  0,  0,  1,493,376,  1,  0,680,945,  0],
      [  1,495,  0,124,911,  1,  1,  0,  0,592],
      [333,122,667,871,  1, 45,804,  1,792,  0],
      [273,184,  1,817,773,787,  0, 60,  1,923],
      [557,  1,  1, 76,676,273,  1,  1,  1,729],
      [  1,765,834,698,159,  1, 77,385,  1,  0],
      [ 17,  0,  0, 98, 94,859,  0,844, 25,621],
      [  0,466,  1,259,483,  1,590,347,  1,  0]],
     [643,924], [4,7], 17, [[4,9],[6,7],[4,4],[3,5],[2,8],[2,6],[3,9],[3,4],[6,5],[5,3],[3,3],[5,2],[2,3],[1,4],[2,2],[5,1]]],
];

for (const [grid, pricing, start, k, expected] of tests) {
    console.log(JSON.stringify(highestRankedKItems(grid, pricing, start, k)) ==
                JSON.stringify(expected));
}
