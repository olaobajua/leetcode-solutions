"""
 * 2477. Minimum Fuel Cost to Report to the Capital [Medium]
 * There is a tree (i.e., a connected, undirected graph with no cycles)
 * structure country network consisting of n cities numbered from 0 to n - 1
 * and exactly n - 1 roads. The capital city is city 0. You are given a 2D
 * integer array roads where roads[i] = [aᵢ, bᵢ] denotes that there exists a
 * bidirectional road connecting cities aᵢ and bᵢ.
 *
 * There is a meeting for the representatives of each city. The meeting is in
 * the capital city.
 *
 * There is a car in each city. You are given an integer seats that indicates
 * the number of seats in each car.
 *
 * A representative can use the car in their city to travel or change the car
 * and ride with another representative. The cost of traveling between two
 * cities is one liter of fuel.
 *
 * Return the minimum number of liters of fuel to reach the capital city.
 *
 * Example 1:
 * Input: roads = [[0,1],[0,2],[0,3]], seats = 5
 * Output: 3
 * Explanation:
 * - Representative₁ goes directly to the capital with 1 liter of fuel.
 * - Representative₂ goes directly to the capital with 1 liter of fuel.
 * - Representative₃ goes directly to the capital with 1 liter of fuel.
 * It costs 3 liters of fuel at minimum.
 * It can be proven that 3 is the minimum number of liters of fuel needed.
 *
 * Example 2:
 * Input: roads = [[3,1],[3,2],[1,0],[0,4],[0,5],[4,6]], seats = 2
 * Output: 7
 * Explanation:
 * - Representative₂ goes directly to city 3 with 1 liter of fuel.
 * - Representative₂ and representative₃ go together to city 1 with 1 liter of
 * fuel.
 * - Representative₂ and representative₃ go together to the capital with 1
 * liter of fuel.
 * - Representative₁ goes directly to the capital with 1 liter of fuel.
 * - Representative₅ goes directly to the capital with 1 liter of fuel.
 * - Representative₆ goes directly to city 4 with 1 liter of fuel.
 * - Representative₄ and representative₆ go together to the capital with 1
 * liter of fuel.
 * It costs 7 liters of fuel at minimum.
 * It can be proven that 7 is the minimum number of liters of fuel needed.
 *
 * Example 3:
 * Input: roads = [], seats = 1
 * Output: 0
 * Explanation: No representatives need to travel to the capital city.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁵
 *     ∙ roads.length == n - 1
 *     ∙ roads[i].length == 2
 *     ∙ 0 <= aᵢ, bᵢ < n
 *     ∙ aᵢ != bᵢ
 *     ∙ roads represents a valid tree.
 *     ∙ 1 <= seats <= 10⁵
"""
from collections import defaultdict
from typing import List

class Solution:
    def minimumFuelCost(self, roads: List[List[int]], seats: int) -> int:
        def dfs(city, distance):
            nonlocal fuel
            passengers = 1
            for neib in cities.pop(city):
                if neib in cities:
                    passengers += (new_passengers := dfs(neib, distance + 1))
                    fuel += new_passengers > 0
            if city:
                fuel += distance * (passengers // seats)
                passengers %= seats
            return passengers

        cities = defaultdict(list, {0: []})
        for a, b in roads:
            cities[a].append(b)
            cities[b].append(a)
        fuel = 0
        dfs(0, 0)
        return fuel

if __name__ == "__main__":
    tests = (
        ([[0,1],[0,2],[0,3]], 5, 3),
        ([[3,1],[3,2],[1,0],[0,4],[0,5],[4,6]], 2, 7),
        ([], 1, 0),
        ([[0,1],[0,2],[1,3],[1,4]], 5, 4),
        ([[0,1],[2,1],[3,2],[4,2],[4,5],[6,0],[5,7],[8,4],[9,2]], 2, 16),
    )
    for roads, seats, expected in tests:
        print(Solution().minimumFuelCost(roads, seats) == expected)
