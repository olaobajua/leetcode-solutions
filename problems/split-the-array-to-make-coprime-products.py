"""
 * 2584. Split the Array to Make Coprime Products [Hard]
 * You are given a 0-indexed integer array nums of length n.
 *
 * A split at an index i where 0 <= i <= n - 2 is called valid if the product
 * of the first i + 1 elements and the product of the remaining elements are
 * coprime.
 *
 *     ∙ For example, if nums = [2, 3, 3], then a split at the index i = 0 is
 *       valid because 2 and 9 are coprime, while a split at the index i = 1 is
 *       not valid because 6 and 3 are not coprime. A split at the index i = 2
 *       is not valid because i == n - 1.
 *
 * Return the smallest index i at which the array can be split validly or -1
 * if there is no such split.
 *
 * Two values val1 and val2 are coprime if gcd(val1, val2) == 1 where
 * gcd(val1, val2) is the greatest common divisor of val1 and val2.
 *
 * Example 1:
 * Input: nums = [4,7,8,15,3,5]
 * Output: 2
 * Explanation: The table above shows the values of the product of the first i
 * + 1 elements, the remaining elements, and their gcd at each index i.
 * The only valid split is at index 2.
 *
 * Example 2:
 * Input: nums = [4,7,15,8,3,5]
 * Output: -1
 * Explanation: The table above shows the values of the product of the first i
 * + 1 elements, the remaining elements, and their gcd at each index i.
 * There is no valid split.
 *
 * Constraints:
 *     ∙ n == nums.length
 *     ∙ 1 <= n <= 10⁴
 *     ∙ 1 <= nums[i] <= 10⁶
"""
from collections import Counter
from functools import cache
from typing import List

class Solution:
    def findValidSplit(self, nums: List[int]) -> int:
        @cache
        def factorize(n):
            factors = []
            for i in range(2, int(n**0.5) + 1):
                while n % i == 0:
                    factors.append(i)
                    n //= i
            if n > 1:
                factors.append(n)
            return factors
        divisors_right = Counter()
        for x in nums:
            for d in factorize(x):
                divisors_right[d] += 1
        divisors_left = Counter()
        for i, x in enumerate(nums[:-1]):
            for d in factorize(x):
                divisors_right[d] -= 1
                divisors_left[d] += 1
                if divisors_right[d] == 0:
                    divisors_left.pop(d)
                    divisors_right.pop(d)
            if not divisors_left & divisors_right:
                return i
        return -1

if __name__ == "__main__":
    tests = (
        ([4,7,8,15,3,5], 2),
        ([4,7,15,8,3,5], -1),
        ([1,1,89], 0),
        ([3,15,5], -1),
    )
    for nums, expected in tests:
        print(Solution().findValidSplit(nums) == expected)
