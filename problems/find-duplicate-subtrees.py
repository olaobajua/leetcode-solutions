"""
 * 652. Find Duplicate Subtrees [Medium]
 * Given the root of a binary tree, return all duplicate subtrees.
 *
 * For each kind of duplicate subtrees, you only need to return the root node
 * of any one of them.
 *
 * Two trees are duplicate if they have the same structure with the same node
 * values.
 *
 * Example 1:
 *    1
 *  2   3
 * 4   2 4
 *    4
 * Input: root = [1,2,3,4,null,2,4,null,null,4]
 * Output: [[2,4],[4]]
 *
 * Example 2:
 *  2
 * 1 1
 * Input: root = [2,1,1]
 * Output: [[1]]
 *
 * Example 3:
 *    2
 *  2   2
 * 3   3
 * Input: root = [2,2,2,3,null,3,null]
 * Output: [[2,3],[3]]
 *
 * Constraints:
 *     ∙ The number of the nodes in the tree will be in the range [1, 5000]
 *     ∙ -200 <= Node.val <= 200
"""
from collections import defaultdict
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def findDuplicateSubtrees(self, root: Optional[TreeNode]) -> List[Optional[TreeNode]]:
        def get_id(root):
            if root:
                id = treeid[root.val, get_id(root.left), get_id(root.right)]
                trees[id].append(root)
                return id
        trees = defaultdict(list)
        treeid = defaultdict()
        treeid.default_factory = treeid.__len__
        get_id(root)
        return [roots[0] for roots in trees.values() if roots[1:]]

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,None,2,4,None,None,4], [[2,4],[4]]),
        ([2,1,1], [[1]]),
        ([2,2,2,3,None,3,None], [[2,3],[3]]),
        ([0,0,0,0,None,None,0,None,None,None,0], [[0]]),
    )
    for nums, expected in tests:
        ret = set()
        for node in Solution().findDuplicateSubtrees(build_tree(nums)):
            t = [*tree_to_list(node)]
            while t and t[-1] is None:
                t.pop()
            ret.add(tuple(t))
        print(ret == set(map(tuple, expected)))
