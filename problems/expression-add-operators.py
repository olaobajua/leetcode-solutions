"""
 * 282. Expression Add Operators (Hard)
 * Given a string num that contains only digits and an integer target, return
 * all possibilities to add the binary operators '+', '-', or '*' between the
 * digits of num so that the resultant expression evaluates to the target
 * value.
 *
 * Example 1:
 * Input: num = "123", target = 6
 * Output: ["1*2*3","1+2+3"]
 *
 * Example 2:
 * Input: num = "232", target = 8
 * Output: ["2*3+2","2+3*2"]
 *
 * Example 3:
 * Input: num = "105", target = 5
 * Output: ["1*0+5","10-5"]
 *
 * Example 4:
 * Input: num = "00", target = 0
 * Output: ["0*0","0+0","0-0"]
 *
 * Example 5:
 * Input: num = "3456237490", target = 9191
 * Output: []
 *
 * Constraints:
 *     1 <= num.length <= 10
 *     num consists of only digits.
 *     -2³¹ <= target <= 2³¹ - 1
"""
from typing import List

class Solution:
    def addOperators(self, num: str, target: int) -> List[str]:
        def find(expr, acc, prev, i):
            if i < n:
                for j in range(i + 1, n + 1 if num[i] != '0' else i + 2):
                    cur = int(num[i:j])
                    if i:
                        find(expr + ("+", str(cur)), acc + cur, cur, j)
                        find(expr + ("-", str(cur)), acc - cur, -cur, j)
                        find(expr + ("*", str(cur)), acc - prev + prev * cur,
                             prev * cur, j)
                    else:
                        find((str(cur),), acc + cur, cur, j)
            else:
                if acc == target:
                    ret.append(''.join(expr))

        n = len(num)
        ret = []
        find((), 0, 0, 0)
        return ret

if __name__ == "__main__":
    tests = (
        ("123", 6, ["1*2*3","1+2+3"]),
        ("232", 8, ["2*3+2","2+3*2"]),
        ("105", 5, ["1*0+5","10-5"]),
        ("00", 0, ["0*0","0+0","0-0"]),
        ("3456237490", 9191, []),
        ("2147483647", 2147483647, ["2147483647"]),
        ("000", 0, ["0*0*0","0*0+0","0*0-0","0+0*0","0+0+0","0+0-0","0-0*0","0-0+0","0-0-0"]),
    )
    for num, target, expected in tests:
        try:
            for expr in Solution().addOperators(num, target):
                expected.remove(expr)
            print(not expected)
        except ValueError:
            print(False)
