"""
 * 896. Monotonic Array [Easy]
 * An array is monotonic if it is either monotone increasing or monotone
 * decreasing.
 *
 * An array nums is monotone increasing if for all i <= j, nums[i] <= nums[j].
 * An array nums is monotone decreasing if for all i <= j, nums[i] >= nums[j].
 *
 * Given an integer array nums, return true if the given array is monotonic,
 * or false otherwise.
 *
 * Example 1:
 * Input: nums = [1,2,2,3]
 * Output: true
 *
 * Example 2:
 * Input: nums = [6,5,4,4]
 * Output: true
 *
 * Example 3:
 * Input: nums = [1,3,2]
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ -10⁵ <= nums[i] <= 10⁵
"""
from itertools import repeat
from math import copysign
from typing import List

class Solution:
    def isMonotonic(self, nums: List[int]) -> bool:
        diffs = []
        prev = nums[0]
        for x in nums:
            if x - prev:
                diffs.append(x - prev)
            prev = x
        return len(set(map(copysign, repeat(1), diffs))) <= 1

if __name__ == "__main__":
    tests = (
        ([1,2,2,3], True),
        ([6,5,4,4], True),
        ([1,3,2], False),
        ([1,1,1],True),
    )
    for nums, expected in tests:
        print(Solution().isMonotonic(nums) == expected)
