/**
 * 867. Transpose Matrix [Easy]
 * Given a 2D integer array matrix, return the transpose of matrix.
 *
 * The transpose of a matrix is the matrix flipped over its main diagonal,
 * switching the matrix's row and column indices.
 *
 * Example 1:
 * Input: matrix = [[1,2,3],
 *                  [4,5,6],
 *                  [7,8,9]]
 * Output: [[1,4,7],
 *          [2,5,8],
 *          [3,6,9]]
 *
 * Example 2:
 * Input: matrix = [[1,2,3],
 *                  [4,5,6]]
 * Output: [[1,4],
 *          [2,5],
 *          [3,6]]
 *
 * Constraints:
 *     ∙ m == matrix.length
 *     ∙ n == matrix[i].length
 *     ∙ 1 <= m, n <= 1000
 *     ∙ 1 <= m * n <= 10⁵
 *     ∙ -10⁹ <= matrix[i][j] <= 10⁹
 */
#include <vector>

class Solution {
public:
    vector<vector<int>> transpose(vector<vector<int>>& matrix) {
        int m = matrix.size();
        int n = matrix[0].size();

        std::vector<std::vector<int>> ret(n, std::vector<int>(m, 0));

        for (int r = 0; r < m; ++r) {
            for (int c = 0; c < n; ++c) {
                ret[c][r] = matrix[r][c];
            }
        }

        return ret;
    }
};
