/**
 * 1814. Count Nice Pairs in an Array [Medium]
 * You are given an array nums that consists of non-negative integers. Let us
 * define rev(x) as the reverse of the non-negative integer x. For example,
 * rev(123) = 321, and rev(120) = 21. A pair of indices (i, j) is nice if it
 * satisfies all of the following conditions:
 *     ∙ 0 <= i < j < nums.length
 *     ∙ nums[i] + rev(nums[j]) == nums[j] + rev(nums[i])
 *
 * Return the number of nice pairs of indices. Since that number can be too
 * large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: nums = [42,11,1,97]
 * Output: 2
 * Explanation: The two pairs are:
 *  - (0,3) : 42 + rev(97) = 42 + 79 = 121, 97 + rev(42) = 97 + 24 = 121.
 *  - (1,2) : 11 + rev(1) = 11 + 1 = 12, 1 + rev(11) = 1 + 11 = 12.
 *
 * Example 2:
 * Input: nums = [13,10,35,24,76]
 * Output: 4
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 0 <= nums[i] <= 10⁹
 */
#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

class Solution {
public:
    int countNicePairs(vector<int>& nums) {
        std::vector<int> revs;
        for (int x : nums) {
            int reversedNum = 0;
            int temp = x;
            while (temp > 0) {
                reversedNum = reversedNum * 10 + temp % 10;
                temp /= 10;
            }
            revs.push_back(reversedNum);
        }

        std::unordered_map<int, int> nice;
        for (size_t i = 0; i < nums.size(); ++i) {
            nice[nums[i] - revs[i]]++;
        }

        const int MOD = 1000000007;
        int ret = 0;
        for (const auto& pair : nice) {
            int count = pair.second;
            if (count > 1) {
                ret = (ret + nChoose2(count, MOD)) % MOD;
            }
        }

        return ret;
    }
private:
    int nChoose2(int n, int mod) {
        return static_cast<int>((static_cast<long long>(n) * (n - 1) / 2) % mod);
    }
};
