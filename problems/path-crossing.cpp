/**
 * 1496. Path Crossing [Easy]
 * Given a string path, where path[i] = 'N', 'S', 'E' or 'W', each
 * representing moving one unit north, south, east, or west, respectively. You
 * start at the origin (0, 0) on a 2D plane and walk on the path specified by
 * path.
 *
 * Return true if the path crosses itself at any point, that is, if at any
 * time you are on a location you have previously visited. Return false
 * otherwise.
 *
 * Example 1:
 * Input: path = "NES"
 * Output: false
 * Explanation: Notice that the path doesn't cross any point more than once.
 *
 * Example 2:
 * Input: path = "NESWW"
 * Output: true
 * Explanation: Notice that the path visits the origin twice.
 *
 * Constraints:
 *     ∙ 1 <= path.length <= 10⁴
 *     ∙ path[i] is either 'N', 'S', 'E', or 'W'.
 */
#include <iostream>
#include <unordered_set>
#include <utility>

class Solution {
public:
    bool isPathCrossing(string path) {
        std::unordered_map<char, std::pair<int, int>> delta = {
            {'N', {0, 1}},
            {'S', {0, -1}},
            {'E', {1, 0}},
            {'W', {-1, 0}},
        };
        int x = 0, y = 0;
        std::unordered_set<std::string> visited = {
            std::to_string(x) + ',' + std::to_string(y)
        };

        for (char move : path) {
            int dx = delta[move].first;
            int dy = delta[move].second;
            x += dx;
            y += dy;
            std::string pos = std::to_string(x) + ',' + std::to_string(y);

            if (visited.contains(pos)) {
                return true;
            }

            visited.insert(pos);
        }

        return false;
    }
};
