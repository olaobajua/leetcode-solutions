"""
 * 2080. Range Frequency Queries (Medium)
 * Design a data structure to find the frequency of a given value in a given
 * subarray.
 *
 * The frequency of a value in a subarray is the number of occurrences of that
 * value in the subarray.
 *
 * Implement the RangeFreqQuery class:
 *     ∙ RangeFreqQuery(int[] arr) Constructs an instance of the class with the
 *       given 0-indexed integer array arr.
 *     ∙ int query(int left, int right, int value) Returns the frequency of
 *       value in the subarray arr[left...right].
 *
 * A subarray is a contiguous sequence of elements within an array.
 * arr[left...right] denotes the subarray that contains the elements of nums
 * between indices left and right (inclusive).
 *
 * Example 1:
 * Input
 * ["RangeFreqQuery", "query", "query"]
 * [[[12, 33, 4, 56, 22, 2, 34, 33, 22, 12, 34, 56]], [1, 2, 4], [0, 11, 33]]
 * Output
 * [null, 1, 2]
 *
 * Explanation
 * RangeFreqQuery rangeFreqQuery =
 *     new RangeFreqQuery([12, 33, 4, 56, 22, 2, 34, 33, 22, 12, 34, 56]);
 * rangeFreqQuery.query(1, 2, 4);   // return 1. The value 4 occurs 1 time in
 *                                  // the subarray [33, 4]
 * rangeFreqQuery.query(0, 11, 33); // return 2. The value 33 occurs 2 times in
 *                                  // the whole array.
 *
 * Constraints:
 *     1 <= arr.length <= 10⁵
 *     1 <= arr[i], value <= 10⁴
 *     0 <= left <= right < arr.length
 *     At most 10⁵ calls will be made to query
"""
from bisect import bisect_left, bisect_right
from collections import defaultdict
from typing import List

class RangeFreqQuery:
    def __init__(self, arr: List[int]):
        self.values = defaultdict(list)
        for i, n in enumerate(arr):
            self.values[n].append(i)

    def query(self, left: int, right: int, value: int) -> int:
        l = bisect_left(self.values[value], left)
        r = bisect_right(self.values[value], right)
        return r - l

obj = RangeFreqQuery([12,33,4,56,22,2,34,33,22,12,34,56])
print(obj.query(1,2,4) == 1)
print(obj.query(0,11,33) == 2)
