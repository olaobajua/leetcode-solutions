"""
 * 169. Majority Element (Easy)
 * Given an array nums of size n, return the majority element.
 *
 * The majority element is the element that appears more than ⌊n / 2⌋ times.
 * You may assume that the majority element always exists in the array.
 *
 * Example 1:
 * Input: nums = [3,2,3]
 * Output: 3
 *
 * Example 2:
 * Input: nums = [2,2,1,1,1,2,2]
 * Output: 2
 *
 * Constraints:
 *     ∙ n == nums.length
 *     ∙ 1 <= n <= 5 * 10⁴
 *     ∙ -2³¹ <= nums[i] <= 2³¹ - 1
 *
 * Follow-up: Could you solve the problem in linear time and in O(1) space?
"""
from typing import List

class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        # Boyer–Moore majority vote algorithm
        count = 0
        candidate = None
        for x in nums:
            if count == 0:
                candidate = x
            count += 1 if x == candidate else -1
        return candidate

if __name__ == "__main__":
    tests = (
        ([3,2,3], 3),
        ([2,2,1,1,1,2,2], 2),
    )
    for nums, expected in tests:
        print(Solution().majorityElement(nums) == expected)
