/**
 * 1964. Find the Longest Valid Obstacle Course at Each Position (Hard)
 * You want to build some obstacle courses. You are given a 0-indexed integer
 * array obstacles of length n, where obstacles[i] describes the height of the
 * iᵗʰ obstacle.
 *
 * For every index i between 0 and n - 1 (inclusive), find the length of the
 * longest obstacle course in obstacles such that:
 *     ∙ You choose any number of obstacles between 0 and i inclusive.
 *     ∙ You must include the iᵗʰ obstacle in the course.
 *     ∙ You must put the chosen obstacles in the same order as they appear in
 *       obstacles.
 *     ∙ Every obstacle (except the first) is taller than or the same height as
 *       the obstacle immediately before it.
 *
 * Return an array ans of length n, where ans[i] is the length of the longest
 * obstacle course for index i as described above.
 *
 * Example 1:
 * Input: obstacles = [1,2,3,2]
 * Output: [1,2,3,3]
 * Explanation: The longest valid obstacle course at each position is:
 * - i = 0: [1], [1] has length 1.
 * - i = 1: [1,2], [1,2] has length 2.
 * - i = 2: [1,2,3], [1,2,3] has length 3.
 * - i = 3: [1,2,3,2], [1,2,2] has length 3.
 *
 * Example 2:
 * Input: obstacles = [2,2,1]
 * Output: [1,2,1]
 * Explanation: The longest valid obstacle course at each position is:
 * - i = 0: [2], [2] has length 1.
 * - i = 1: [2,2], [2,2] has length 2.
 * - i = 2: [2,2,1], [1] has length 1.
 *
 * Example 3:
 * Input: obstacles = [3,1,5,6,4,2]
 * Output: [1,1,2,3,2,2]
 * Explanation: The longest valid obstacle course at each position is:
 * - i = 0: [3], [3] has length 1.
 * - i = 1: [3,1], [1] has length 1.
 * - i = 2: [3,1,5], [3,5] has length 2. [1,5] is also valid.
 * - i = 3: [3,1,5,6], [3,5,6] has length 3. [1,5,6] is also valid.
 * - i = 4: [3,1,5,6,4], [3,4] has length 2. [1,4] is also valid.
 * - i = 5: [3,1,5,6,4,2], [1,2] has length 2.
 *
 * Constraints:
 *     n == obstacles.length
 *     1 <= n <= 10⁵
 *     1 <= obstacles[i] <= 10⁷
 */

/**
 * @param {number[]} obstacles
 * @return {number[]}
 */
var longestObstacleCourseAtEachPosition = function(obstacles) {
    const ret = [];
    const heights = [0];  // i - course len, heights[i] - largest height for it
    for (const o of obstacles) {
        const l = maxLenForHeight(heights, o) + 1;
        if (l >= heights.length) {
            heights.push(o);
        } else {
            heights[l] = Math.min(heights[l], o);
        }
        ret.push(l);
    }
    return ret;
};

function maxLenForHeight(heights, h) {
    let lo = 0;
    let hi = heights.length - 1;
    while (hi - lo > 1) {
        const mid = Math.floor((lo + hi) / 2);
        if (heights[mid] <= h) {
            lo = mid;
        } else {
            hi = mid;
        }
    }
    return heights[hi] > h ? lo : hi;
}

const tests = [
    [[1,2,3,2], [1,2,3,3]],
    [[2,2,1], [1,2,1]],
    [[3,1,5,6,4,2], [1,1,2,3,2,2]],
    [[5,2,5,4,1,1,1,5,3,1], [1,1,2,2,1,2,3,4,4,4]],
    [[5,1,5,5,1,3,4,5,1,4], [1,1,2,3,2,3,4,5,3,5]],
];

for (const [obstacles, expected] of tests) {
    console.log(longestObstacleCourseAtEachPosition(obstacles).toString() ==
                expected.toString());
}
