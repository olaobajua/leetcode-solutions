"""
 * 15. 3Sum (Medium)
 * Given an integer array nums, return all the triplets [nums[i], nums[j],
 * nums[k]] such that i != j, i != k, and j != k, and
 * nums[i] + nums[j] + nums[k] == 0.
 *
 * Notice that the solution set must not contain duplicate triplets.
 *
 * Example 1:
 * Input: nums = [-1,0,1,2,-1,-4]
 * Output: [[-1,-1,2],[-1,0,1]]
 *
 * Example 2:
 * Input: nums = []
 * Output: []
 *
 * Example 3:
 * Input: nums = [0]
 * Output: []
 *
 * Constraints:
 *     0 <= nums.length <= 3000
 *     -10⁵ <= nums[i] <= 10⁵
"""
from bisect import bisect_left
from collections import Counter
from typing import List

class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        nums_count = Counter(nums)
        nums_set = sorted(nums_count)
        ret = []
        # add triplets with 3 different numbers
        for i, n1 in enumerate(nums_set):
            two_sum = -n1
            start = bisect_left(nums_set, two_sum - nums_set[-1], i + 1)
            end = bisect_left(nums_set, two_sum / 2, start)
            for n2 in nums_set[start:end]:
                if two_sum - n2 in nums_count:
                    ret.append([n1, n2, two_sum - n2])
        # add triplets with 3 zeroes if exists
        if nums_count[0] >= 3:
            ret.append([0, 0, 0])
        del nums_count[0]
        # add triplets with 2 same numbers
        for n in nums_count:
            if nums_count[n] > 1 and -2*n in nums_count:
                ret.append([n, n, -2*n])
        return ret

if __name__ == "__main__":
    tests = (
        ([-1,0,1,2,-1,-4], [[-1,-1,2],[-1,0,1]]),
        ([], []),
        ([0], []),
        ([0,0], []),
    )
    for nums, expected in tests:
        try:
            for t in Solution().threeSum(nums):
                expected.remove(t)
            print(not expected)
        except ValueError:
            print(False)
