/**
 * 698. Partition to K Equal Sum Subsets (Medium)
 * Given an integer array nums and an integer k, return true if it is possible
 * to divide this array into k non-empty subsets whose sums are all equal.
 *
 * Example 1:
 * Input: nums = [4,3,2,3,5,2,1], k = 4
 * Output: true
 * Explanation: It's possible to divide it into 4 subsets (5), (1, 4), (2,3),
 * (2,3) with equal sums.
 *
 * Example 2:
 * Input: nums = [1,2,3,4], k = 3
 * Output: false
 *
 * Constraints:
 *     1 <= k <= nums.length <= 16
 *     1 <= nums[i] <= 10⁴
 *     The frequency of each element is in the range [1, 4].
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {boolean}
 */
var canPartitionKSubsets = function(nums, k) {
    function can_partition(groups, sumLeft, nums) {
        if (groups == k) {
            return nums.length === 0;
        } else if (nums.length) {
            for (let [i, num] of nums.entries()) {
                if (sumLeft > num) {
                    let numsLeft = nums.slice(0, i).concat(nums.slice(i + 1));
                    if (can_partition(groups, sumLeft - num, numsLeft)) {
                        return true;
                    }
                } else if (sumLeft == num) {
                    let numsLeft = nums.slice(0, i).concat(nums.slice(i + 1));
                    if (can_partition(groups + 1, groupSum, numsLeft)) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return groups < k;
        }
    }

    can_partition = cache(can_partition);
    let groupSum = nums.reduce((acc, num) => acc + num) / k;
    return (nums.length >= k && groupSum >= Math.max(...nums) &&
            can_partition(0, groupSum, nums));
};

function cache(fn) {
    const cache = {};
    return function(...args) {
        const argsKey = JSON.stringify(args);
        if (cache[argsKey] === undefined) {
            cache[argsKey] = fn.apply(this, args);
        }
        return cache[argsKey];
    };
}

var tests = [
    [[5, 3, 3, 3, 2, 2], 2, true],  // [[5, 2, 2], [3, 3, 3]]
    [[4,3,2,3,5,2,1], 4, true],
    [[1,2,3,4], 3, false],
    [[3522,181,521,515,304,123,2512,312,922,407,146,1932,4037,2646,3871,269], 5, true],
    [[815,625,3889,4471,60,494,944,1118,4623,497,771,679,1240,202,601,883], 3, true],
]
var start = new Date().getTime();
for (let [nums, k, expected] of tests) {
    console.log(canPartitionKSubsets(nums, k) == expected);
}
console.log(`Time elapsed: ${new Date().getTime() - start} msec`);
