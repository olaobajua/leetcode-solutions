"""
 * 152. Maximum Product Subarray (Medium)
 * Given an integer array nums, find a contiguous non-empty subarray within the
 * array that has the largest product, and return the product.
 *
 * It is guaranteed that the answer will fit in a 32-bit integer.
 *
 * A subarray is a contiguous subsequence of the array.
 *
 * Example 1:
 * Input: nums = [2,3,-2,4]
 * Output: 6
 * Explanation: [2,3] has the largest product 6.
 *
 * Example 2:
 * Input: nums = [-2,0,-1]
 * Output: 0
 * Explanation: The result cannot be 2, because [-2,-1] is not a subarray.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 2 * 10⁴
 *     ∙ -10 <= nums[i] <= 10
 *     ∙ The product of any prefix or suffix of nums is guaranteed to fit in a
 *       32-bit integer.
"""
from itertools import accumulate
from typing import List

class Solution:
    def maxProduct(self, nums: List[int]) -> int:
        return max([*accumulate(nums, lambda a, x: x * (a or 1))] +
                   [*accumulate(reversed(nums), lambda a, x: x * (a or 1))])

if __name__ == "__main__":
    tests = (
        ([2,3,-2,4], 6),
        ([-2,0,-1], 0),
        ([10,-1,0,-3,5,4], 20),
        ([10,-1,0,9,7,-3,5,4], 63),
        ([-2], -2),
        ([-3,-1,-1], 3),
        ([1,2,3,4,5,-1,-1,-1], 120),
        ([1,2,3,4,5,-1], 120),
    )
    for nums, expected in tests:
        print(Solution().maxProduct(nums) == expected)
