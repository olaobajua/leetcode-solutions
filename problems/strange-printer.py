"""
 * 664. Strange Printer [Hard]
 * There is a strange printer with the following two special properties:
 *     ∙ The printer can only print a sequence of the same character each
 *       time.
 *     ∙ At each turn, the printer can print new characters starting from and
 *       ending at any place and will cover the original existing characters.
 *
 * Given a string s, return the minimum number of turns the printer needed to
 * print it.
 *
 * Example 1:
 * Input: s = "aaabbb"
 * Output: 2
 * Explanation: Print "aaa" first and then print "bbb".
 *
 * Example 2:
 * Input: s = "aba"
 * Output: 2
 * Explanation: Print "aaa" first and then print "b" from the second place of
 * the string, which will cover the existing character 'a'.
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 100
 *     ∙ s consists of lowercase English letters.
"""
from functools import cache
import re

class Solution:
    def strangePrinter(self, s: str) -> int:
        @cache
        def dp(left, right):
            if left > right:
                return 0
            ret = dp(left + 1, right) + 1
            for i in range(left + 1, right + 1):
                if s[i] == s[left]:
                    ret = min(ret, dp(left, i - 1) + dp(i + 1, right))

            return ret

        s = ''.join(a for a, b in zip(s, s[1:] + "\0") if a != b)
        return dp(0, len(s) - 1)

if __name__ == "__main__":
    tests = (
        ("aaabbb", 2),
        ("aba", 2),
        ("abcabc", 5),
    )
    for s, expected in tests:
        print(Solution().strangePrinter(s) == expected)
