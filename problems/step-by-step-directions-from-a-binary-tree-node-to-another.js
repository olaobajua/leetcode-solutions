/**
 * 2096. Step-By-Step Directions From a Binary Tree Node to Another (Medium)
 * You are given the root of a binary tree with n nodes. Each node is uniquely
 * assigned a value from 1 to n. You are also given an integer startValue
 * representing the value of the start node s, and a different integer
 * destValue representing the value of the destination node t.
 *
 * Find the shortest path starting from node s and ending at node t. Generate
 * step-by-step directions of such path as a string consisting of only the
 * uppercase letters 'L', 'R', and 'U'. Each letter indicates a specific
 * direction:
 *     'L' means to go from a node to its left child node.
 *     'R' means to go from a node to its right child node.
 *     'U' means to go from a node to its parent node.
 *
 * Return the step-by-step directions of the shortest path from node s to
 * node t.
 *
 * Example 1:
 * Input: root = [5,1,2,3,null,6,4], startValue = 3, destValue = 6
 * Output: "UURL"
 * Explanation: The shortest path is: 3 → 1 → 5 → 2 → 6.
 *
 * Example 2:
 * Input: root = [2,1], startValue = 2, destValue = 1
 * Output: "L"
 * Explanation: The shortest path is: 2 → 1.
 *
 * Constraints:
 *     The number of nodes in the tree is n.
 *     2 <= n <= 10⁵
 *     1 <= Node.val <= n
 *     All the values in the tree are unique.
 *     1 <= startValue, destValue <= n
 *     startValue != destValue
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @param {number} startValue
 * @param {number} destValue
 * @return {string}
 */
var getDirections = function(root, startValue, destValue) {
    function findPath(root, val) {
        if (root) {
            if (root.val == val) {
                return [[root.val, 'N']];
            } else {
                let path;
                if (path = findPath(root.left, val)) {
                    path.push([root.val, 'L'])
                    return path;
                } else if (path = findPath(root.right, val)) {
                    path.push([root.val, 'R'])
                    return path;
                }
            }
        }
    }

    const spath = findPath(root, startValue).reverse();
    const dpath = findPath(root, destValue).reverse();
    while (spath.length > 1 && dpath.length > 1 && spath[1][0] == dpath[1][0]) {
        spath.shift();
        dpath.shift();
    }
    if (spath) {
        spath.pop();
    }
    if (dpath) {
        dpath.pop();
    }
    return 'U'.repeat(spath.length) + dpath.map(([_, d]) => d).join('');
};

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

const tests = [
    [[5,1,2,3,null,6,4], 3, 6, "UURL"],
    [[2,1], 2, 1, "L"],
];

for (const [nums, startValue, destValue, expected] of tests) {
    const root = buildTree(nums);
    console.log(getDirections(root, startValue, destValue) == expected);
}
