"""
 * 21. Merge Two Sorted Lists (Easy)
 * You are given the heads of two sorted linked lists list1 and list2.
 *
 * Merge the two lists in a one sorted list. The list should be made by
 * splicing together the nodes of the first two lists.
 *
 * Return the head of the merged linked list.
 *
 * Example 1:
 * Input: list1 = [1,2,4], list2 = [1,3,4]
 * Output: [1,1,2,3,4,4]
 *
 * Example 2:
 * Input: list1 = [], list2 = []
 * Output: []
 *
 * Example 3:
 * Input: list1 = [], list2 = [0]
 * Output: [0]
 *
 * Constraints:
 *     ∙ The number of nodes in both lists is in the range [0, 50].
 *     ∙ -100 <= Node.val <= 100
 *     ∙ Both list1 and list2 are sorted in non-decreasing order.
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        if list1 and list2:
            if list1.val > list2.val:
                list1, list2 = list2, list1
            list1.next = self.mergeTwoLists(list1.next, list2)
        return list1 or list2

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,4], [1,3,4], [1,1,2,3,4,4]),
        ([], [], []),
        ([], [0], [0]),
        ([-9,3], [5,7], [-9,3,5,7]),
    )
    for list1, list2, expected in tests:
        root1 = list_to_linked_list(list1)
        root2 = list_to_linked_list(list2)
        print(linked_list_to_list(Solution().mergeTwoLists(root1, root2)) ==
              expected)
