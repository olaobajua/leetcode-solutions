/**
 * 1658. Minimum Operations to Reduce X to Zero (Medium)
 * You are given an integer array nums and an integer x. In one operation, you
 * can either remove the leftmost or the rightmost element from the array nums
 * and subtract its value from x. Note that this modifies the array for future
 * operations.
 *
 * Return the minimum number of operations to reduce x to exactly 0 if it is
 * possible, otherwise, return -1.
 *
 * Example 1:
 * Input: nums = [1,1,4,2,3], x = 5
 * Output: 2
 * Explanation: The optimal solution is to remove the last two elements to
 * reduce x to zero.
 *
 * Example 2:
 * Input: nums = [5,6,7,8,9], x = 4
 * Output: -1
 *
 * Example 3:
 * Input: nums = [3,2,20,1,1,3], x = 10
 * Output: 5
 * Explanation: The optimal solution is to remove the last three elements and
 * the first two elements (5 operations in total) to reduce x to zero.
 *
 * Constraints:
 *     1 <= nums.length <= 10⁵
 *     1 <= nums[i] <= 10⁴
 *     1 <= x <= 10⁹
 */

/**
 * @param {number[]} nums
 * @param {number} x
 * @return {number}
 */
var minOperations = function(nums, x) {
    const right = new Map();
    let acc = 0;
    [...nums, 0].reverse().forEach((x, i) => {
        acc += x;
        right.set(acc, i);
    });
    nums = [0, ...nums];
    const n = nums.length;
    let ret = Infinity;
    for (let i = 0, acc = 0; i < n; ++i) {
        acc += nums[i];
        const fromRight = right.get(x - acc);
        if (fromRight !== undefined && i + fromRight < n) {
            ret = Math.min(ret, i + fromRight);
        }
    }
    return ret < Infinity ? ret : -1;
};

tests = [
    [[1,1,4,2,3], 5, 2],
    [[5,6,7,8,9], 4, -1],
    [[3,2,20,1,1,3], 10, 5],
    [[1,1], 3, -1],
    [[8828,9581,49,9818,9974,9869,9991,10000,10000,10000,9999,9993,9904,8819,1231,6309], 134365, 16],
];

for (const [nums, x, expected] of tests) {
    console.log(minOperations(nums, x) == expected);
}
