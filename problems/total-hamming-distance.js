/**
 * 477. Total Hamming Distance (Medium)
 * The Hamming distance between two integers is the number of positions at
 * which the corresponding bits are different.
 *
 * Given an integer array nums, return the sum of Hamming distances between all
 * the pairs of the integers in nums.
 *
 * Example 1:
 * Input: nums = [4,14,2]
 * Output: 6
 * Explanation: In binary representation, the 4 is 0100, 14 is 1110, and 2 is
 * 0010 (just showing the four bits relevant in this case).
 * The answer will be:
 * HammingDistance(4, 14) + HammingDistance(4, 2) + HammingDistance(14, 2) =
 * 2 + 2 + 2 = 6.
 *
 * Example 2:
 * Input: nums = [4,14,4]
 * Output: 4
 *
 * Constraints:
 *     1 <= nums.length <= 10⁴
 *     0 <= nums[i] <= 10⁹
 *     The answer for the given input will fit in a 32-bit integer.
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var totalHammingDistance = function(nums) {
    let bnums = nums.map(num => num.toString(2).padStart(32, 0));
    let n = nums.length;
    return Array.from(bnums[0]).map((_, i) => bnums.map(bnum => bnum[i]))
                               .map(bits => bits.filter(b => b == '1').length)
                               .map(ones => (n - ones) * ones)
                               .reduce((acc, x) => acc + x, 0);
};

tests = [
    [[4,14,2], 6],
    [[4,14,4], 4],
];
for (let [nums, expected] of tests) {
    console.log(totalHammingDistance(nums) == expected);
}
