"""
 * 215. Kth Largest Element in an Array [Medium]
 * Given an integer array nums and an integer k, return the kᵗʰ largest
 * element in the array.
 *
 * Note that it is the kᵗʰ largest element in the sorted order, not the kᵗʰ
 * distinct element.
 *
 * Can you solve it without sorting?
 *
 * Example 1:
 * Input: nums = [3,2,1,5,6,4], k = 2
 * Output: 5
 * Example 2:
 * Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
 * Output: 4
 *
 * Constraints:
 *     ∙ 1 <= k <= nums.length <= 10⁵
 *     ∙ -10⁴ <= nums[i] <= 10⁴
"""
from typing import List

class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        return sorted(nums)[-k]

if __name__ == "__main__":
    tests = (
        ([3,2,1,5,6,4], 2, 5),
        ([3,2,3,1,2,4,5,5,6], 4, 4),
    )
    for nums, k, expected in tests:
        print(Solution().findKthLargest(nums, k) == expected)
