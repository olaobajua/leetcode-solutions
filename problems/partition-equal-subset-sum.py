"""
 * 416. Partition Equal Subset Sum (Medium)
 * Given a non-empty array nums containing only positive integers, find if the
 * array can be partitioned into two subsets such that the sum of elements in
 * both subsets is equal.
 *
 * Example 1:
 * Input: nums = [1,5,11,5]
 * Output: true
 * Explanation: The array can be partitioned as [1, 5, 5] and [11].
 *
 * Example 2:
 * Input: nums = [1,2,3,5]
 * Output: false
 * Explanation: The array cannot be partitioned into equal sum subsets.
 *
 * Constraints:
 *     1 <= nums.length <= 200
 *     1 <= nums[i] <= 100
"""
from typing import List

class Solution:
    def canPartition(self, nums: List[int]) -> bool:
        if (S := sum(nums)) % 2:
            return False
        half_sum = S//2
        dp = [0] * (half_sum + 1)
        for i in range(len(nums)):
            for j in range(half_sum, nums[i]-1, -1):
                dp[j] = max(dp[j], dp[j - nums[i]] + nums[i])
                if (dp[j] == half_sum):
                    return True;
        return dp[half_sum] == half_sum

if __name__ == "__main__":
    tests = (
        ([1,5,11,5], True),
        ([1,2,3,5], False),
        ([100,4,100,100], False),
        ([23,13,11,7,6,5,5], True),
        ([100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,99,97], False),
    )
    for nums, expected in tests:
        print(Solution().canPartition(nums) == expected)
