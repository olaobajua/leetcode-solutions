"""
 * 703. Kth Largest Element in a Stream (Easy)
 * Design a class to find the kᵗʰ largest element in a stream. Note that it is
 * the kᵗʰ largest element in the sorted order, not the kᵗʰ distinct element.
 *
 * Implement KthLargest class:
 *     ∙ KthLargest(int k, int[] nums) Initializes the object with the integer
 *       k and the stream of integers nums.
 *     ∙ int add(int val) Appends the integer val to the stream and returns the
 *       element representing the kᵗʰ largest element in the stream.
 *
 * Example 1:
 * Input
 * ["KthLargest", "add", "add", "add", "add", "add"]
 * [[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
 * Output
 * [null, 4, 5, 5, 8, 8]
 *
 * Explanation
 * KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
 * kthLargest.add(3);   // return 4
 * kthLargest.add(5);   // return 5
 * kthLargest.add(10);  // return 5
 * kthLargest.add(9);   // return 8
 * kthLargest.add(4);   // return 8
 *
 * Constraints:
 *     ∙ 1 <= k <= 10⁴
 *     ∙ 0 <= nums.length <= 10⁴
 *     ∙ -10⁴ <= nums[i] <= 10⁴
 *     ∙ -10⁴ <= val <= 10⁴
 *     ∙ At most 10⁴ calls will be made to add.
 *     ∙ It is guaranteed that there will be at least k elements in the array
 *       when you search for the kᵗʰ element.
"""
import bisect
from typing import List

class KthLargest:
    def __init__(self, k: int, nums: List[int]):
        self.k = k
        self.nums = sorted(nums)

    def add(self, val: int) -> int:
        self.nums.insert(bisect.bisect(self.nums, val), val)
        return self.nums[-self.k]

obj = KthLargest(3, [4,5,8,2])
print(4 == obj.add(3))
print(5 == obj.add(5))
print(5 == obj.add(10))
print(8 == obj.add(9))
print(8 == obj.add(4))
