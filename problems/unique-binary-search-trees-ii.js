/**
 * 95. Unique Binary Search Trees II (Medium)
 * Given an integer n, return all the structurally unique BST's (binary search
 * trees), which has exactly n nodes of unique values from 1 to n. Return the
 * answer in any order.
 *
 * Example 1:
 * Input: n = 3
 * Output: [[1,null,2,null,3],[1,null,3,2],[2,1,3],[3,1,null,null,2],[3,2,null,1]]
 *
 * Example 2:
 * Input: n = 1
 * Output: [[1]]
 *
 * Constraints:
 *     1 <= n <= 8
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {number} n
 * @return {TreeNode[]}
 */
var generateTrees = function(n) {
    function generate(first, last) {
        let trees = [];
        for (let root = first; root <= last; ++root) {
            for (let left of generate(first, root-1)) {
                for (let right of generate(root+1, last)) {
                    trees.push(new TreeNode(root, left, right));
                }
            }
        }
        return trees.length ? trees : [null];
    }
    return generate(1, n);
};

function* treeToList(root) {
    let nodes = [root];
    for (let n of nodes) {
        yield n ? n.val : null;
        if (n) {
            nodes.push(n.left, n.right);
        }
    }
}

const tests = [
    [3, [[1,null,2,null,3],[1,null,3,2],[2,1,3],[3,1,null,null,2],[3,2,null,1]]],
    [1, [[1]]],
];

for (let [n, expected] of tests) {
    expected = expected.map(e => JSON.stringify(e));
    for (let tree of generateTrees(n)) {
        tree = [...treeToList(tree)];
        while (tree[tree.length-1] === null) {
            tree.pop();
        }
        tree = JSON.stringify(tree);
        if ((i = expected.indexOf(tree)) > -1) {
            expected.splice(i, 1);
        } else {
            console.log(false);
            break;
        }
    }
    console.log(expected.length == 0);
}
