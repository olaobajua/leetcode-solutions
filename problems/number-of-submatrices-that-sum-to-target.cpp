/**
 * 1074. Number of Submatrices That Sum to Target [Hard]
 * Given a matrix and a target, return the number of non-empty submatrices that
 * sum to target.
 *
 * A submatrix x1, y1, x2, y2 is the set of all cells matrix[x][y] with x1 <= x
 * <= x2 and y1 <= y <= y2.
 *
 * Two submatrices (x1, y1, x2, y2) and (x1', y1', x2', y2') are different if
 * they have some coordinate that is different: for example, if x1 != x1'.
 *
 * Example 1:
 * Input: matrix = [[0,1,0],[1,1,1],[0,1,0]], target = 0
 * Output: 4
 * Explanation: The four 1x1 submatrices that only contain 0.
 *
 * Example 2:
 * Input: matrix = [[1,-1],[-1,1]], target = 0
 * Output: 5
 * Explanation: The two 1x2 submatrices, plus the two 2x1 submatrices, plus the
 * 2x2 submatrix.
 *
 * Example 3:
 * Input: matrix = [[904]], target = 0
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= matrix.length <= 100
 *     ∙ 1 <= matrix[0].length <= 100
 *     ∙ -1000 <= matrix[i] <= 1000
 *     ∙ -10⁸ <= target <= 10⁸
 */
class Solution {
public:
    int numSubmatrixSumTarget(vector<vector<int>>& matrix, int target) {
        const int m = matrix.size();
        const int n = matrix[0].size();
        vector<vector<int>> s(m + 1, vector<int>(n + 1, 0));
        for (int r = 1; r <= m; ++r)
            for (int c = 1; c <= n; ++c)
                s[r][c] = s[r-1][c] + s[r][c-1] - s[r-1][c-1] + matrix[r-1][c-1];
        int ret = 0;
        for (int r1 = 0; r1 <= m; ++r1) {
            for (int r2 = r1 + 1; r2 <= m; ++r2) {
                unordered_map<int, int> col_sums;
                for (int c = 0; c <= n; ++c) {
                    int cur = s[r2][c] - s[r1][c];
                    if (col_sums.find(cur - target) != col_sums.end())
                        ret += col_sums[cur-target];
                    ++col_sums[cur];
                }
            }
        }
        return ret;
    }
};
