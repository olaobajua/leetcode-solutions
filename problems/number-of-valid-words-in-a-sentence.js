/**
 * 2047. Number of Valid Words in a Sentence (Easy)
 * A sentence consists of lowercase letters ('a' to 'z'), digits ('0' to '9'),
 * hyphens ('-'), punctuation marks ('!', '.', and ','), and spaces (' ') only.
 * Each sentence can be broken down into one or more tokens separated by one or
 * more spaces ' '.
 *
 * A token is a valid word if:
 *     ∙ It only contains lowercase letters, hyphens, and/or punctuation (no
 *       digits).
 *     ∙ There is at most one hyphen '-'. If present, it should be surrounded
 *       by lowercase characters ("a-b" is valid, but "-ab" and "ab-" are not
 *       valid).
 *     ∙ There is at most one punctuation mark. If present, it should be at the
 *       end of the token.
 *
 * Examples of valid words include "a-b.", "afad", "ba-c", "a!", and "!".
 *
 * Given a string sentence, return the number of valid words in sentence.
 *
 * Example 1:
 * Input: sentence = "cat and  dog"
 * Output: 3
 * Explanation: The valid words in the sentence are "cat", "and", and "dog".
 *
 * Example 2:
 * Input: sentence = "!this  1-s b8d!"
 * Output: 0
 * Explanation: There are no valid words in the sentence.
 * "!this" is invalid because it starts with a punctuation mark.
 * "1-s" and "b8d" are invalid because they contain digits.
 *
 * Example 3:
 * Input: sentence = "alice and  bob are playing stone-game10"
 * Output: 5
 * Explanation: The valid words in the sentence are "alice", "and", "bob",
 * "are", and "playing".
 * "stone-game10" is invalid because it contains digits.
 *
 * Example 4:
 * Input: sentence = "he bought 2 pencils, 3 erasers, and 1  pencil-sharpener."
 * Output: 6
 * Explanation: The valid words in the sentence are "he", "bought", "pencils,",
 * "erasers,", "and", and "pencil-sharpener.".
 *
 * Constraints:
 *     ∙ 1 <= sentence.length <= 1000
 *     ∙ sentence only contains lowercase English letters, digits, ' ', '-',
 *       '!', '.', and ','.
 *     ∙ There will be at least 1 token.
 */

/**
 * @param {string} sentence
 * @return {number}
 */
var countValidWords = function(sentence) {
    const space = /\s+/;
    const word = /^([a-z]+(-[a-z]+)?)?[!.,]?$/;
    return sentence.trim().split(/\s+/).filter(t => t.match(word)).length;
};

const tests = [
    ["cat and  dog", 3],
    ["!this  1-s b8d!", 0],
    ["alice and  bob are playing stone-game10", 5],
    ["he bought 2 pencils, 3 erasers, and 1  pencil-sharpener.", 6],
    ["!", 1],
    ["a-b-c", 0],
    [",ee3-jxp 6i! r  -  tk, h yh3h w-i10cws gl0   h ckd9drxyh !mr jii jaoj . na,b5a2v.!s2 pan e253yo87mvacrm ysw 7-e  7n.a!fr e6nxcxb axs  !.  ,  v2bz,p.t9iw8wu!  4q36au.zl8 39na dn rvdpfys   1qj48pi c    l6v -fqfd 3c  3 ytn,xm   !53y2a m 8fqq- 3 2ral f jhj v o  4!8  .3 p ,ijhq b2la89v5 xzax!e bjw  nq qwu! eod qqwnf 2sc mw0  ko r fi7p 0e9jc4!7bw,ki    ojf uo 7-jf1swt70! wr  3ahsb xs! v cb.h   ybb is4cx71  4r qmy  qi7rn r i0apj og z  tp545by!ct9h 8pugwy   ipyn.tfi6o 3 4 2f. l 1ex2 l9 a  5nn  s4m!xb2if 3  4dj4  7  7 4dxe g pu3 -nd1qb x x-ucx-7,455 ,cy  egdz  xvutn  p! n e,u  qgs  k48-gq7t52n wasqim8u -k yp 26z ux sgpwn5cm6 5m dfbkej pr g x t1ww10 s -d dh   10  -      -kt gb   1et !8 f!3w 8fe7czp hsxy7u6o -wu4hcbijze 27m5 6l 3vx 7oq! 1z8 7-,.t--   oat   -g2!.  o !ri72ox w7 p ko wi4kfx7vpd fq4 zffk eqvu6, xox57 f75vo1m  ln9  fw 07 d4 .  s3e ofwam1fd!e8n  p yenyky5p   09  q  pqs q1v2fdwi a4vm", 65],
    ["s -6x2 .2v hle826lk4cvc 58-w   t hqulb k1  sg6 8 2-tshzq t-kul.a r- tmc8 6q60xebttg sb9j t 79 ec,r5 n zw7dxk62q6mh y i4 6 sb1b!2 !h  e6 3zsj  a ,r5 , s ixceuso4 r 8o  53  3c9  .5 b3r  uxjg77252k2u, ue-oeo ,nn -e2 jtdcrpn6j ow1v3 f41vhw3 ts! -o 62xk cf f x 6l bk-2p-dq6 a8 dr  5zp m,,ibe,y-klgqj b73-  gz9,1  fa123 n22-6htq zcim,36. dknp,wroou339 xq   25wb. w,.fw bpft- - 8mha r sgk shshf, .z wnp8 p oi,y 7 ia 5  w y p-iocy3 s19v9 kvk9oe6  n n  7ppwtz e  goyzkk8k8b5ugn !26z- afv0t4 xt2j o,  u7 d!,0a q8ax3jyd dnz 2d h 4j jo! on4utd3n0, hz  ol  yh2 n7vg88v!4u!fhtksel3y xoi jy q83  4t5 ! xgn 3 5hq d bdvmd8 i c 7l ez04  a !e8oa55l8pj  z u  sxtr     5,,io1q65cml tv vbv lutwxaa6  q,8  n 30s   qu4w,r 82ywofe 9  6o4b!,  q,6 u6o frkj j,15 v!.    bphqk  f,g-gc4-u73 a  cx  . 1 b sw2robvz !! dpp lll pyz4hlyp4g 05 6  aikjgywpggj  r5wcx0n 7od  8q4bs y ng59af5h9  f1-i49g3rq4aotala.em ob9w ,8vjrq-88 t  x  84as9g  ua6mpo  mwc-  .g8doz9vfpc c. h5 70q8b bjl4xdvmwd23  m!z-   xiimsd-h6. 98ulv!6", 60],
    ["!g 3 !sy ", 0],
];
for (let [sentence, expected] of tests) {
    console.log(countValidWords(sentence) == expected);
}
