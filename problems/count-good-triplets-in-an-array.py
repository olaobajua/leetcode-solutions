"""
 * 2179. Count Good Triplets in an Array (Hard)
 * You are given two 0-indexed arrays nums1 and nums2 of length n, both of
 * which are permutations of [0, 1, ..., n - 1].
 *
 * A good triplet is a set of 3 distinct values which are present in increasing
 * order by position both in nums1 and nums2. In other words, if we consider
 * pos1ₓ as the index of the value x in nums1 and pos2ₓ as the index of the
 * value x in nums2, then a good triplet will be a set (x, y, u) where
 * 0 <= x, y, u <= n - 1, such that pos1ₓ < pos1ᵧ < pos1ᵤ and
 * pos2ₓ < pos2ᵧ < pos2ᵤ.
 *
 * Return the total number of good triplets.
 *
 * Example 1:
 * Input: nums1 = [2,0,1,3], nums2 = [0,1,2,3]
 * Output: 1
 * Explanation:
 * There are 4 triplets (x,y,u) such that pos1ₓ < pos1ᵧ < pos1ᵤ. They are
 * (2,0,1), (2,0,3), (2,1,3), and (0,1,3).
 * Out of those triplets, only the triplet (0,1,3) satisfies
 * pos2ₓ < pos2ᵧ < pos2ᵤ. Hence, there is only 1 good triplet.
 *
 * Example 2:
 * Input: nums1 = [4,0,1,3,2], nums2 = [4,1,0,2,3]
 * Output: 4
 * Explanation: The 4 good triplets are (4,0,3), (4,0,2), (4,1,3), and (4,1,2).
 *
 * Constraints:
 *     ∙ n == nums1.length == nums2.length
 *     ∙ 3 <= n <= 10⁵
 *     ∙ 0 <= nums1[i], nums2[i] <= n - 1
 *     ∙ nums1 and nums2 are permutations of [0, 1, ..., n - 1].
"""
from bisect import bisect_left, insort_left
from typing import List

class Solution:
    def goodTriplets(self, nums1: List[int], nums2: List[int]) -> int:
        pos2 = [0] * len(nums2)
        for i, x in enumerate(nums2):
            pos2[x] = i

        pos_in_2 = [pos2[nums1[0]]]
        before = [0]
        for x in nums1[1:]:
            insort_left(pos_in_2, pos2[x])
            before.append(bisect_left(pos_in_2, pos2[x]))

        pos_in_2 = [pos2[nums1[-1]]]
        after = [0]
        for x in reversed(nums1[:len(nums1)-1]):
            after.append(len(pos_in_2) - bisect_left(pos_in_2, pos2[x]))
            insort_left(pos_in_2, pos2[x])
        after.reverse()

        return sum(x * y for x, y in zip(before, after))

if __name__ == "__main__":
    tests = (
        ([2,0,1,3], [0,1,2,3], 1),
        ([4,0,1,3,2], [4,1,0,2,3], 4),
    )
    for nums1, nums2, expected in tests:
        print(Solution().goodTriplets(nums1, nums2) == expected, flush=True)
