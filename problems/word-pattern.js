/**
 * 290. Word Pattern (Easy)
 * Given a pattern and a string s, find if s follows the same pattern.
 *
 * Here follow means a full match, such that there is a bijection between a
 * letter in pattern and a non-empty word in s.
 *
 * Example 1:
 * Input: pattern = "abba", s = "dog cat cat dog"
 * Output: true
 *
 * Example 2:
 * Input: pattern = "abba", s = "dog cat cat fish"
 * Output: false
 *
 * Example 3:
 * Input: pattern = "aaaa", s = "dog cat cat dog"
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= pattern.length <= 300
 *     ∙ pattern contains only lower-case English letters.
 *     ∙ 1 <= s.length <= 3000
 *     ∙ s contains only lowercase English letters and spaces ' '.
 *     ∙ s does not contain any leading or trailing spaces.
 *     ∙ All the words in s are separated by a single space.
 */

/**
 * @param {string} pattern
 * @param {string} s
 * @return {boolean}
 */
var wordPattern = function(pattern, s) {
    s = s.split(' ');
    pattern = Array.from(pattern);
    return pattern.map(c => pattern.indexOf(c)).toString() ==
           s.map(w => s.indexOf(w)).toString();
};

const tests = [
    ["abba", "dog cat cat dog", true],
    ["abba", "dog cat cat fish", false],
    ["aaaa", "dog cat cat dog", false],
    ["abba", "dog dog dog dog", false],
    ["aaa", "aa aa aa aa", false],
    ["he", "unit", false],
];

for (const [pattern, s, expected] of tests) {
    console.log(wordPattern(pattern, s) == expected);
}
