"""
 * 102. Binary Tree Level Order Traversal (Medium)
 * Given the root of a binary tree, return the level order traversal of its
 * nodes' values. (i.e., from left to right, level by level).
 *
 * Example 1:
 * Input: root = [3,9,20,null,null,15,7]
 * Output: [[3],[9,20],[15,7]]
 *
 * Example 2:
 * Input: root = [1]
 * Output: [[1]]
 *
 * Example 3:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 2000].
 *     ∙ -1000 <= Node.val <= 1000
"""
from collections import deque
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def levelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        ret = []
        queue = deque([root] if root else [])
        while queue:
            level = []
            for _ in range(len(queue)):
                node = queue.popleft()
                level.append(node.val)
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)
            ret.append(level)
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([3,9,20,None,None,15,7], [[3],[9,20],[15,7]]),
        ([1], [[1]]),
        ([], []),
    )
    for nums, expected in tests:
        root = build_tree(nums)
        print(Solution().levelOrder(root) == expected)
