/*
 * 90. Subsets II (Medium)
 * Given an integer array nums that may contain duplicates, return all possible subsets (the power set).
 * The solution set must not contain duplicate subsets. Return the solution in any order.
 *
 * Example 1:
 * Input: nums = [1,2,2]
 * Output: [[],[1],[1,2],[1,2,2],[2],[2,2]]
 *
 * Example 2:
 * Input: nums = [0]
 * Output: [[],[0]]
 *
 * Constraints:
 *     1 <= nums.length <= 10
 *     -10 <= nums[i] <= 10
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct combo {
    int *array;
    int length;
    struct combo *next;
} Combo;

int compare(const void *a, const void *b);
int** combinations(int *arr, int arrSize, int r, int *returnSize);
long long countCombinations(int n, int k);
bool isEnd(int *indices, int len, int max);
void incIndices(int *indices, int len, int max);
long long factorial(int a);
int** subsetsWithDup(int *nums, int numsSize, int *returnSize, int **returnColumnSizes);

int main() {
    // const int numsInit[] = {1,2,2};
    // const int numsInit[] = {1,2,3};
    const int numsInit[] = {2,1,2,1,3};
    // const int numsInit[] = {0};
    // const int numsInit[] = {1,2,2};
    // const int numsInit[] = {4,4,4,1,4};
    // const int numsInit[] = {4,4,1,1,4};
    // const int numsInit[] = {1,1};
    const size_t numsSize = sizeof(numsInit) / sizeof(numsInit[0]);
    int *nums = (int*)malloc(numsSize * sizeof(int));
    memcpy(nums, numsInit, numsSize * sizeof(int));
    int subsetsCount = 0;
    int *columnSizes = NULL;
    int **subsets = subsetsWithDup(nums, numsSize, &subsetsCount, &columnSizes);
    printf("[[");
    for (int x = 0; x < subsetsCount; ++x) {
        printf("[");
        for (int y = 0; y < columnSizes[x]; ++y) {
            printf("%d,", subsets[x][y]);
        }
        printf("\b],");
        free(subsets[x]);
    }
    printf("\b]\n");
    free(subsets);
    free(columnSizes);
    return EXIT_SUCCESS;
}

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
int** subsetsWithDup(int *nums, int numsSize, int *returnSize, int **returnColumnSizes) {
    qsort(nums, numsSize, sizeof(int), compare);
    *returnSize = 0;
    Combo *first = NULL, *current = NULL;
    for (int i = 0; i < numsSize + 1; ++i) {
        int subsetsCount;
        int **subsets = combinations(nums, numsSize, i, &subsetsCount);
        for (int j = 0; j < subsetsCount; ++j) {
            bool unique = true;
            for (int k = j + 1; k < subsetsCount; ++k) {
                if (memcmp(subsets[j], subsets[k], i * sizeof(int)) == 0) {
                    unique = false;
                    break;
                }
            }
            if (unique) {
                (*returnSize)++;
                Combo *next = (Combo *)malloc(sizeof(Combo));
                next->length = i;
                next->array = subsets[j];
                next->next = NULL;
                if (current) {
                    current->next = next;
                    current = next;
                } else {
                    first = current = next;
                }
            } else {
                free(subsets[j]);
            }
        }
        free(subsets);
    }
    int **ret = (int**)malloc(*returnSize * sizeof(int*));
    *returnColumnSizes = (int*)malloc(*returnSize * sizeof(int));
    current = first;
    for (int i = 0; i < *returnSize; ++i) {
        (*returnColumnSizes)[i] = current->length;
        ret[i] = current->array;
        first = current;
        current = current->next;
        free(first);
    }
    return ret;
}

int compare(const void *a, const void *b) {
  return (*(int*)a - *(int*)b);
}

int** combinations(int *arr, int arrSize, int r, int *returnSize) {
    *returnSize = 0;
    if (r > arrSize) {
        return NULL;
    }
    int *indices = (int*)malloc(r * sizeof(int));
    *returnSize = countCombinations(arrSize, r);
    int **ret = (int**)malloc(*returnSize * sizeof(int*));
    int current = 0;
    ret[current] = (int*)malloc(r * sizeof(int));
    for (int j = 0; j < r; ++j) {
        indices[j] = j;
        ret[current][j] = arr[indices[j]];
    }
    while (!isEnd(indices, r, arrSize)) {
        incIndices(indices, r, arrSize);
        ret[++current] = (int*)malloc(r * sizeof(int));
        for (int j = 0; j < r; ++j) {
            ret[current][j] = arr[indices[j]];
        }
    }
    free(indices);
    return ret;
}

long long countCombinations(int n, int k) {
    return factorial(n) / (factorial(k) * factorial(n - k));
}

long long factorial(int a) {
    return 0 == a ? 1 : (a * factorial(a - 1));
}

bool isEnd(int *indices, int len, int max) {
    // scan indices right-to-left until finding one that is not at its maximum
    for (int i = len - 1; i >= 0; --i) {
        if (indices[i] != i + max - len) {
            return false;
        }
    }
    // if the indices are all at their maximum value then we're done
    return true;
}

void incIndices(int *indices, int len, int max) {
    // scan indices right-to-left until finding one that is not at its maximum
    int i;
    for (i = len - 1; i >= 0; --i) {
        if (indices[i] != i + max - len) {
            break;
        }
    }
    // increment the current index which we know is not at its maximum
    indices[i]++;
    // then move back to the right setting each index to its lowest
    // possible value (one higher than the index to its left - this maintains
    // the sort order invariant)
    for (int j = i + 1; j < len; ++j) {
        indices[j] = indices[j - 1] + 1;
    }
}
