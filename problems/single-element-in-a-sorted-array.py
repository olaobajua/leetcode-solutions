"""
 * 540. Single Element in a Sorted Array (Medium)
 * You are given a sorted array consisting of only integers where every element
 * appears exactly twice, except for one element which appears exactly once.
 *
 * Return the single element that appears only once.
 *
 * Your solution must run in O(log n) time and O(1) space.
 *
 * Example 1:
 * Input: nums = [1,1,2,3,3,4,4,8,8]
 * Output: 2
 *
 * Example 2:
 * Input: nums = [3,3,7,7,10,11,11]
 * Output: 10
 *
 * Constraints:
 *     1 <= nums.length <= 10⁵
 *     0 <= nums[i] <= 10⁵
"""
from bisect import bisect_left
from typing import List

class Solution:
    def singleNonDuplicate(self, nums: List[int]) -> int:
        class NonDupIsLess:
            def __getitem__(self, i):
                return nums[i] != nums[i^1]
        return nums[bisect_left(NonDupIsLess(), True, 0, len(nums) - 1)]

if __name__ == "__main__":
    tests = (
        ([1], 1),
        ([1,1,2], 2),
        ([1,2,2,3,3], 1),
        ([1,1,2,3,3], 2),
        ([1,1,2,3,3,4,4,8,8], 2),
        ([3,3,7,7,10,11,11], 10),
    )
    for nums, expected in tests:
        print(Solution().singleNonDuplicate(nums) == expected)
