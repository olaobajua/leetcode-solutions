"""
 * 695. Max Area of Island (Medium)
 * You are given an m x n binary matrix grid. An island is a group of 1's
 * (representing land) connected 4-directionally (horizontal or vertical.) You
 * may assume all four edges of the grid are surrounded by water.
 *
 * The area of an island is the number of cells with a value 1 in the island.
 *
 * Return the maximum area of an island in grid. If there is no island,
 * return 0.
 *
 * Example 1:
 * Input: grid = [[0,0,1,0,0,0,0,1,0,0,0,0,0],
 *                [0,0,0,0,0,0,0,1,1,1,0,0,0],
 *                [0,1,1,0,1,0,0,0,0,0,0,0,0],
 *                [0,1,0,0,1,1,0,0,1,0,1,0,0],
 *                [0,1,0,0,1,1,0,0,1,1,1,0,0],
 *                [0,0,0,0,0,0,0,0,0,0,1,0,0],
 *                [0,0,0,0,0,0,0,1,1,1,0,0,0],
 *                [0,0,0,0,0,0,0,1,1,0,0,0,0]]
 * Output: 6
 * Explanation: The answer is not 11, because the island must be connected
 * 4-directionally.
 *
 * Example 2:
 * Input: grid = [[0,0,0,0,0,0,0,0]]
 * Output: 0
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 50
 *     ∙ grid[i][j] is either 0 or 1.
"""
from typing import List

class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        def get_square(r, c):
            ret = 0
            if grid[r][c] == 1:
                ret += 1
                grid[r][c] = 2
                for nr, nc in ((r, c + 1), (r + 1, c), (r, c - 1), (r - 1, c)):
                    if 0 <= nr < m and 0 <= nc < n and grid[nr][nc] == 1:
                        ret += get_square(nr, nc)
            return ret

        m = len(grid)
        n = len(grid[0])
        return max(get_square(r, c) for r in range(m) for c in range(n))

if __name__ == "__main__":
    tests = (
        ([[0,0,1,0,0,0,0,1,0,0,0,0,0],
          [0,0,0,0,0,0,0,1,1,1,0,0,0],
          [0,1,1,0,1,0,0,0,0,0,0,0,0],
          [0,1,0,0,1,1,0,0,1,0,1,0,0],
          [0,1,0,0,1,1,0,0,1,1,1,0,0],
          [0,0,0,0,0,0,0,0,0,0,1,0,0],
          [0,0,0,0,0,0,0,1,1,1,0,0,0],
          [0,0,0,0,0,0,0,1,1,0,0,0,0]], 6),
        ([[0,0,0,0,0,0,0,0]], 0),
        ([[1]], 1),
        ([[1,1,0,0,0],
          [1,1,0,0,0],
          [0,0,0,1,1],
          [0,0,0,1,1]], 4),
        ([[0,1],
          [1,0]], 1),
    )
    for grid, expected in tests:
        print(Solution().maxAreaOfIsland(grid) == expected)
