/**
 * 2106. Maximum Fruits Harvested After at Most K Steps (Hard)
 * Fruits are available at some positions on an infinite x-axis. You are given
 * a 2D integer array fruits where fruits[i] = [positionᵢ, amountᵢ] depicts
 * amountᵢ fruits at the position positionᵢ. fruits is already sorted by
 * positionᵢ in ascending order, and each positionᵢ is unique.
 *
 * You are also given an integer startPos and an integer k. Initially, you are
 * at the position startPos. From any position, you can either walk to the left
 * or right. It takes one step to move one unit on the x-axis, and you can walk
 * at most k steps in total. For every position you reach, you harvest all the
 * fruits at that position, and the fruits will disappear from that position.
 *
 * Return the maximum total number of fruits you can harvest.
 *
 * Example 1:
 * Input: fruits = [[2,8],[6,3],[8,6]], startPos = 5, k = 4
 * Output: 9
 * Explanation:
 * The optimal way is to:
 * - Move right to position 6 and harvest 3 fruits
 * - Move right to position 8 and harvest 6 fruits
 * You moved 3 steps and harvested 3 + 6 = 9 fruits in total.
 *
 * Example 2:
 * Input: fruits = [[0,9],[4,1],[5,7],[6,2],[7,4],[10,9]], startPos = 5, k = 4
 * Output: 14
 * Explanation:
 * You can move at most k = 4 steps, so you cannot reach position 0 nor 10.
 * The optimal way is to:
 * - Harvest the 7 fruits at the starting position 5
 * - Move left to position 4 and harvest 1 fruit
 * - Move right to position 6 and harvest 2 fruits
 * - Move right to position 7 and harvest 4 fruits
 * You moved 1 + 3 = 4 steps and harvested 7 + 1 + 2 + 4 = 14 fruits in total.
 *
 * Example 3:
 * Input: fruits = [[0,3],[6,4],[8,5]], startPos = 3, k = 2
 * Output: 0
 * Explanation:
 * You can move at most k = 2 steps and cannot reach any position with fruits.
 *
 * Constraints:
 *     1 <= fruits.length <= 10⁵
 *     fruits[i].length == 2
 *     0 <= startPos, positionᵢ <= 2 * 10⁵
 *     positionᵢ₋₁ < positionᵢ for any i > 0 (0-indexed)
 *     1 <= amountᵢ <= 10⁴
 *     0 <= k <= 2 * 10⁵
 */

/**
 * @param {number[][]} fruits
 * @param {number} startPos
 * @param {number} k
 * @return {number}
 */
var maxTotalFruits = function(fruits, startPos, k) {
    fruits = Object.fromEntries(fruits);
    const left = Array(k + 1).fill(0);
    const right = Array(k + 1).fill(0);
    for (let i = 0; i < k; ++i) {
        right[i+1] = right[i] + (fruits[i+startPos+1] || 0);
        left[i+1] = left[i] + (fruits[startPos-i-1] || 0);
    }
    const startFruits = fruits[startPos] || 0;
    let ret = startFruits;
    for (let i = 1; i <= k; ++i) {
        const movesLeft = Math.max(0, k - 2*i);
        const goRight1st = right[i] + startFruits + left[movesLeft];
        const goLeft1st = left[i] + startFruits + right[movesLeft];
        ret = Math.max(ret, goLeft1st, goRight1st);
    }
    return ret;
}

const tests = [
    [[[2,8],[6,3],[8,6]], 5, 4, 9],
    [[[0,9],[4,1],[5,7],[6,2],[7,4],[10,9]], 5, 4, 14],
    [[[0,3],[6,4],[8,5]], 3, 2, 0],
    [[[200000,10000]], 200000, 0, 10000],
];

for (const [fruits, startPos, k, expected] of tests) {
    console.log(maxTotalFruits(fruits, startPos, k) == expected);
}
