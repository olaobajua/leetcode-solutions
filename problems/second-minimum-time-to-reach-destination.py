"""
 * 2045. Second Minimum Time to Reach Destination (Hard)
 * A city is represented as a bi-directional connected graph with n vertices
 * where each vertex is labeled from 1 to n (inclusive). The edges in the graph
 * are represented as a 2D integer array edges, where each edges[i] = [uᵢ, vᵢ]
 * denotes a bi-directional edge between vertex uᵢ and vertex vᵢ. Every vertex
 * pair is connected by at most one edge, and no vertex has an edge to itself.
 * The time taken to traverse any edge is time minutes.
 *
 * Each vertex has a traffic signal which changes its color from green to red
 * and vice versa every change minutes. All signals change at the same time.
 * You can enter a vertex at any time, but can leave a vertex only when the
 * signal is green. You cannot wait at a vertex if the signal is green.
 *
 * The second minimum value is defined as the smallest value strictly larger
 * than the minimum value.
 *     ∙ For example the second minimum value of [2, 3, 4] is 3, and the second
 *       minimum value of [2, 2, 4] is 4.
 *
 * Given n, edges, time, and change, return the second minimum time it will
 * take to go from vertex 1 to vertex n.
 *
 * Notes:
 *     ∙ You can go through any vertex any number of times, including 1 and n.
 *     ∙ You can assume that when the journey starts, all signals have just
 *       turned green.
 *
 * Example 1:
 * Input: n = 5, edges = [[1,2],[1,3],[1,4],[3,4],[4,5]], time = 3, change = 5
 * Output: 13
 * Explanation:
 * The figure on the left shows the given graph.
 * The blue path in the figure on the right is the minimum time path.
 * The time taken is:
 * - Start at 1, time elapsed=0
 * - 1 -> 4: 3 minutes, time elapsed=3
 * - 4 -> 5: 3 minutes, time elapsed=6
 * Hence the minimum time needed is 6 minutes.
 *
 * Path to get the second minimum time:
 * - Start at 1, time elapsed=0
 * - 1 -> 3: 3 minutes, time elapsed=3
 * - 3 -> 4: 3 minutes, time elapsed=6
 * - Wait at 4 for 4 minutes, time elapsed=10
 * - 4 -> 5: 3 minutes, time elapsed=13
 * Hence the second minimum time is 13 minutes.
 *
 * Example 2:
 * Input: n = 2, edges = [[1,2]], time = 3, change = 2
 * Output: 11
 * Explanation:
 * The minimum time path is 1 -> 2 with time = 3 minutes.
 * The second minimum time path is 1 -> 2 -> 1 -> 2 with time = 11 minutes.
 *
 * Constraints:
 *     ∙ 2 <= n <= 10⁴
 *     ∙ n - 1 <= edges.length <= min(2 * 10⁴, n * (n - 1) / 2)
 *     ∙ edges[i].length == 2
 *     ∙ 1 <= uᵢ, vᵢ <= n
 *     ∙ uᵢ != vᵢ
 *     ∙ There are no duplicate edges.
 *     ∙ Each vertex can be reached directly or indirectly from every other
 *       vertex.
 *     ∙ 1 <= time, change <= 10³
"""
from collections import defaultdict
from heapq import heappop, heappush
from typing import List

class Solution:
    def secondMinimum(self, n: int, edges: List[List[int]], time: int, change: int) -> int:
        def count_time(distance):
            total_time = 0
            time_left = change
            green = True
            for i in range(distance):
                if not green:  # wait green signal for time_left
                    total_time += time_left
                    time_left = change
                    green = True
                total_time += time
                time_left -= time
                while time_left <= 0:
                    time_left += change
                    green = not green
            return total_time

        neighbours = defaultdict(list)
        for n1, n2 in edges:
            neighbours[n1].append(n2)
            neighbours[n2].append(n1)

        return count_time(dijkstra_2nd_dist(neighbours, 1, n))

def dijkstra_2nd_dist(graph, source, destination):
    shortest_paths = defaultdict(set)
    shortest_paths[source].add(0)
    to_visit = [(0, source)]
    while to_visit:
        parent_dist, root = heappop(to_visit)
        for neib in graph[root]:
            if len(shortest_paths[neib]) < 2:
                shortest_paths[neib] |= {parent_dist + 1}
                heappush(to_visit, (parent_dist + 1, neib))
    return max(shortest_paths[destination])

if __name__ == "__main__":
    tests = (
        (5, [[1,2],[1,3],[1,4],[3,4],[4,5]], 3, 5, 13),
        (2, [[1,2]], 3, 2, 11),
        (19, [[1,2],[2,3],[1,4],[2,5],[2,6],[2,7],[7,8],[8,9],[7,10],[9,11],[11,12],[1,13],[3,14],[13,15],[14,16],[8,17],[4,18],[11,19],[17,11],[3,19],[19,7],[12,5],[8,1],[15,7],[19,6],[18,9],[6,8],[14,19],[13,18],[15,2],[13,12],[1,5],[16,18],[3,16],[6,1],[18,14],[12,1],[16,6],[13,11],[1,14],[16,13],[11,16],[4,15],[17,5],[5,9],[12,2],[4,10],[9,16],[17,9],[3,5],[10,2],[18,1],[15,18],[12,17],[10,6],[10,18],[19,12],[12,15],[19,13],[1,19],[9,14],[4,3],[17,13],[9,3],[17,10],[19,10],[5,4],[5,7],[14,17],[1,10],[4,11],[6,4],[5,10],[7,14],[8,14],[18,17],[15,10],[11,8],[14,11],[7,3],[5,18],[13,8],[4,12],[11,3],[5,15],[15,9],[8,10],[13,3],[17,1],[10,11],[15,11],[19,2],[1,3],[7,4],[18,11],[2,14],[9,1],[17,15],[7,13],[12,16],[12,8],[6,12],[9,6],[2,17],[15,6],[16,2],[12,7],[7,9],[8,4]], 850, 411, 1700),
    )
    for n, edges, time, change, expected in tests:
        print(Solution().secondMinimum(n, edges, time, change) == expected)
