"""
 * 2561. Rearranging Fruits [Hard]
 * You have two fruit baskets containing n fruits each. You are given two
 * 0-indexed integer arrays basket1 and basket2 representing the cost of fruit
 * in each basket. You want to make both baskets equal. To do so, you can use
 * the following operation as many times as you want:
 *     ∙ Chose two indices i and j, and swap the iᵗʰ fruit of basket1 with the
 *       jᵗʰ fruit of basket2.
 *     ∙ The cost of the swap is min(basket1[i],basket2[j]).
 *
 * Two baskets are considered equal if sorting them according to the fruit
 * cost makes them exactly the same baskets.
 *
 * Return the minimum cost to make both the baskets equal or -1 if impossible.
 *
 * Example 1:
 * Input: basket1 = [4,2,2,2], basket2 = [1,4,1,2]
 * Output: 1
 * Explanation: Swap index 1 of basket1 with index 0 of basket2, which has
 * cost 1. Now basket1 = [4,1,2,2] and basket2 = [2,4,1,2]. Rearranging both
 * the arrays makes them equal.
 *
 * Example 2:
 * Input: basket1 = [2,3,4,1], basket2 = [3,2,5,1]
 * Output: -1
 * Explanation: It can be shown that it is impossible to make both the baskets
 * equal.
 *
 * Constraints:
 *     ∙ basket1.length == bakste2.length
 *     ∙ 1 <= basket1.length <= 10⁵
 *     ∙ 1 <= basket1[i],basket2[i] <= 10⁹
"""
from collections import Counter
from typing import List

class Solution:
    def minCost(self, basket1: List[int], basket2: List[int]) -> int:
        count = Counter(basket1 + basket2)
        for c in count:
            if count[c] % 2:
                return -1
            count[c] >>= 1
        basket1_odd = list((Counter(basket1) - count).elements())
        basket2_odd = list((Counter(basket2) - count).elements())
        small = min(count)
        odd = sorted(basket1_odd + basket2_odd)
        return sum(min(small * 2, odd[i]) for i in range(len(basket1_odd)))

if __name__ == "__main__":
    tests = (
        ([4,2,2,2], [1,4,1,2], 1),
        ([2,3,4,1], [3,2,5,1], -1),
        ([4,4,4,4,3], [5,5,5,5,3], 8),
        ([84,80,43,8,80,88,43,14,100,88], [32,32,42,68,68,100,42,84,14,8], 48),
    )
    for basket1, basket2, expected in tests:
        print(Solution().minCost(basket1, basket2) == expected)
