"""
 * 404. Sum of Left Leaves (Easy)
 * Given the root of a binary tree, return the sum of all left leaves.
 *
 * Example 1:
 * Input: root = [3,9,20,null,null,15,7]
 * Output: 24
 * Explanation: There are two left leaves in the binary tree, with values 9 and
 * 15 respectively.
 *
 * Example 2:
 * Input: root = [1]
 * Output: 0
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [1, 1000].
 *     -1000 <= Node.val <= 1000
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def sumOfLeftLeaves(self, root: Optional[TreeNode]) -> int:
        def dfs(root, left):
            if not root:
                return 0
            if not root.left and not root.right and left:
                return root.val
            return dfs(root.left, True) + dfs(root.right, False)
        return dfs(root, False)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([3,9,20,None,None,15,7], 24),
        ([1], 0),
        ([1,2,3,4,5], 4),
    )
    for nums, expected in tests:
        print(Solution().sumOfLeftLeaves(build_tree(nums)) == expected)
