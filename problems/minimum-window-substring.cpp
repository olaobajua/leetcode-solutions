/**
 * 76. Minimum Window Substring [Hard]
 * Given two strings s and t of lengths m and n respectively, return the
 * minimum window substring of s such that every character in t (including
 * duplicates) is included in the window. If there is no such substring, return
 * the empty string "".
 *
 * The testcases will be generated such that the answer is unique.
 *
 * Example 1:
 * Input: s = "ADOBECODEBANC", t = "ABC"
 * Output: "BANC"
 * Explanation: The minimum window substring "BANC" includes 'A', 'B', and 'C'
 * from string t.
 *
 * Example 2:
 * Input: s = "a", t = "a"
 * Output: "a"
 * Explanation: The entire string s is the minimum window.
 *
 * Example 3:
 * Input: s = "a", t = "aa"
 * Output: ""
 * Explanation: Both 'a's from t must be included in the window.
 * Since the largest window of s only has one 'a', return empty string.
 *
 * Constraints:
 *     ∙ m == s.length
 *     ∙ n == t.length
 *     ∙ 1 <= m, n <= 10⁵
 *     ∙ s and t consist of uppercase and lowercase English letters.
 *
 * Follow up: Could you find an algorithm that runs in O(m + n) time?
 */
#include <string>
#include <vector>

class Solution {
public:
    string minWindow(string s, string t) {
        std::vector<int> count_t(123, 0);
        for (char c : t) {
            ++count_t[c];
        }
        std::vector<int> count_s(123, 0);
        int left = t.length();
        int start = 0, ret_start = 0, ret_end = s.length() + 1;

        for (int end = 0; end < s.length(); ++end) {
            char c = s[end];
            if (count_s[c] < count_t[c]) {
                --left;
            }
            ++count_s[c];

            while (left == 0) {
                if (ret_end - ret_start > end - start) {
                    ret_start = start;
                    ret_end = end;
                }
                c = s[start];
                --count_s[c];
                if (count_s[c] < count_t[c]) {
                    ++left;
                }
                ++start;
            }
        }

        if (ret_end - ret_start > s.length()) {
            return "";
        }
        return s.substr(ret_start, ret_end - ret_start + 1);
    }
};
