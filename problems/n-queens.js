/**
 * 51. N-Queens (Hard)
 * The n-queens puzzle is the problem of placing n queens on an n x n
 * chessboard such that no two queens attack each other.
 *
 * Given an integer n, return all distinct solutions to the n-queens puzzle.
 * You may return the answer in any order.
 *
 * Each solution contains a distinct board configuration of the n-queens'
 * placement, where 'Q' and '.' both indicate a queen and an empty space,
 * respectively.
 *
 * Example 1:
 * Input: n = 4
 * Output: [[".Q..",
 *           "...Q",
 *           "Q...",
 *           "..Q."],
 *          ["..Q.",
 *           "Q...",
 *           "...Q",
 *           ".Q.."]]
 * Explanation: There exist two distinct solutions to the 4-queens puzzle as
 * shown above
 *
 * Example 2:
 * Input: n = 1
 * Output: [["Q"]]
 *
 * Constraints:
 *     ∙ 1 <= n <= 9
 */

/**
 * @param {number} n
 * @return {string[][]}
 */
var solveNQueens = function(n) {
    function excludeAttacked(row, col, board) {
        const ret = board.map(row => row.slice());
        for (let i = row; i < n; ++i) {
            ret[i][col] = 1;
        }
        for (let i = col; i < n; ++i) {
            ret[row][i] = 1;
        }
        for (let i = Math.min(n - row, n - col) - 1; i >= 0; --i) {
            ret[row+i][col+i] = 1;
        }
        for (let i = Math.min(n - row, col + 1) - 1; i >= 0; --i) {
            ret[row+i][col-i] = 1;
        }
        ret[row][col] = 2;
        return ret;
    }

    function* dfs(row, col, board, queens) {
        if (queens == n) {
            yield board.map(row => row.map(c => c == 2 ? 'Q' : '.').join(''));
        } else {
            for (let i = row; i < n; ++i) {
                for (let j = 0; j < n; ++j) {
                    if (board[i][j] == 0) {
                        yield* dfs(i + 1, j, excludeAttacked(i, j, board),
                                   queens + 1);
                    }
                }
            }
        }
    }

    return [...dfs(0, 0, [...Array(n)].map(_ => Array(n).fill(0)), 0)];
};

const tests = [
    [4, [[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]],
    [1, [["Q"]]],
];

for (const [n, expected] of tests) {
    console.log(solveNQueens(n).toString() == expected.toString());
}
