/**
 * 1074. Number of Submatrices That Sum to Target [Hard]
 * Given a matrix and a target, return the number of non-empty submatrices that
 * sum to target.
 *
 * A submatrix x1, y1, x2, y2 is the set of all cells matrix[x][y] with x1 <= x
 * <= x2 and y1 <= y <= y2.
 *
 * Two submatrices (x1, y1, x2, y2) and (x1', y1', x2', y2') are different if
 * they have some coordinate that is different: for example, if x1 != x1'.
 *
 * Example 1:
 * Input: matrix = [[0,1,0],[1,1,1],[0,1,0]], target = 0
 * Output: 4
 * Explanation: The four 1x1 submatrices that only contain 0.
 *
 * Example 2:
 * Input: matrix = [[1,-1],[-1,1]], target = 0
 * Output: 5
 * Explanation: The two 1x2 submatrices, plus the two 2x1 submatrices, plus the
 * 2x2 submatrix.
 *
 * Example 3:
 * Input: matrix = [[904]], target = 0
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= matrix.length <= 100
 *     ∙ 1 <= matrix[0].length <= 100
 *     ∙ -1000 <= matrix[i] <= 1000
 *     ∙ -10⁸ <= target <= 10⁸
 */

/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {number}
 */
var numSubmatrixSumTarget = function(matrix, target) {
    const m = matrix.length;
    const n = matrix[0].length;
    const s = [...Array(m + 1)].map(_ => Array(n + 1).fill(0));
    for (let r = 1; r <= m; ++r)
        for (let c = 1; c <= n; ++c)
            s[r][c] = s[r-1][c] + s[r][c-1] - s[r-1][c-1] + matrix[r-1][c-1];
    let ret = 0;
    for (let r1 = 0; r1 <= m; ++r1) {
        for (let r2 = r1 + 1; r2 <= m; ++r2) {
            const colSums = new Map();
            for (let c = 0; c <= n; ++c) {
                const cur = s[r2][c] - s[r1][c];
                ret += colSums.get(cur - target) | 0;
                colSums.set(cur, (colSums.get(cur) | 0) + 1);
            }
        }
    }
    return ret;
};

const tests = [
    [[[0,1,0],[1,1,1],[0,1,0]], 0, 4],
    [[[1,-1],[-1,1]], 0, 5],
    [[[904]], 0, 0],
];
for (const [matrix, target, expected] of tests) {
    console.log(numSubmatrixSumTarget(matrix, target) === expected);
}
