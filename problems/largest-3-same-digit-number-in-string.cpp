/**
 * 2264. Largest 3-Same-Digit Number in String [Easy]
 * You are given a string num representing a large integer. An integer is good
 * if it meets the following conditions:
 *     ∙ It is a substring of num with length 3.
 *     ∙ It consists of only one unique digit.
 *
 * Return the maximum good integer as a string or an empty string "" if no
 * such integer exists.
 *
 * Note:
 *     ∙ A substring is a contiguous sequence of characters within a string.
 *     ∙ There may be leading zeroes in num or a good integer.
 *
 * Example 1:
 * Input: num = "6777133339"
 * Output: "777"
 * Explanation: There are two distinct good integers: "777" and "333".
 * "777" is the largest, so we return "777".
 *
 * Example 2:
 * Input: num = "2300019"
 * Output: "000"
 * Explanation: "000" is the only good integer.
 *
 * Example 3:
 * Input: num = "42352338"
 * Output: ""
 * Explanation: No substring of length 3 consists of only one unique digit.
 * Therefore, there are no good integers.
 *
 * Constraints:
 *     ∙ 3 <= num.length <= 1000
 *     ∙ num only consists of digits.
 */
#include <iostream>
#include <algorithm>
#include <string>

class Solution {
public:
    string largestGoodInteger(string num) {
        char m = '\0';
        char prev = '\0';
        int count = 0;

        for (char x : num) {
            if (x == prev) {
                ++count;
            } else {
                count = 1;
            }
            if (count >= 3) {
                m = std::max(m, x);
            }
            prev = x;
        }
        if (m == '\0') {
            return "";
        }

        std:string ret(3, m);
        return ret;
    }
};
