"""
 * 24. Swap Nodes in Pairs (Medium)
 * Given a linked list, swap every two adjacent nodes and return its head. You
 * must solve the problem without modifying the values in the list's nodes
 * (i.e., only nodes themselves may be changed.)
 *
 * Example 1:
 * Input: head = [1,2,3,4]
 * Output: [2,1,4,3]
 *
 * Example 2:
 * Input: head = []
 * Output: []
 *
 * Example 3:
 * Input: head = [1]
 * Output: [1]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 100].
 *     ∙ 0 <= Node.val <= 100
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def swapPairs(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if head and head.next:
            head.next.next, head.next, head = head, head.next.next, head.next
            head.next.next = self.swapPairs(head.next.next)
        return head

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3,4], [2,1,4,3]),
        ([], []),
        ([1], [1]),
    )
    for nums, expected in tests:
        ret = Solution().swapPairs(list_to_linked_list(nums))
        print(linked_list_to_list(ret) == expected)
