"""
 * 188. Best Time to Buy and Sell Stock IV [Hard]
 * You are given an integer array prices where prices[i] is the price of a
 * given stock on the iᵗʰ day, and an integer k.
 *
 * Find the maximum profit you can achieve. You may complete at most k
 * transactions.
 *
 * Note: You may not engage in multiple transactions simultaneously (i.e., you
 * must sell the stock before you buy again).
 *
 * Example 1:
 * Input: k = 2, prices = [2,4,1]
 * Output: 2
 * Explanation: Buy on day 1 (price = 2) and sell on day 2 (price = 4), profit
 * = 4-2 = 2.
 *
 * Example 2:
 * Input: k = 2, prices = [3,2,6,5,0,3]
 * Output: 7
 * Explanation: Buy on day 2 (price = 2) and sell on day 3 (price = 6), profit
 * = 6-2 = 4. Then buy on day 5 (price = 0) and sell on day 6 (price = 3),
 * profit = 3-0 = 3.
 *
 * Constraints:
 *     ∙ 0 <= k <= 100
 *     ∙ 0 <= prices.length <= 1000
 *     ∙ 0 <= prices[i] <= 1000
"""
from functools import cache
from typing import List

class Solution:
    def maxProfit(self, k: int, prices: List[int]) -> int:
        n = len(prices)
        dp = [[0] * n for _ in range(k + 1)]
        for i in range(1, k + 1):
            cur = prev = 0
            for j in range(1, n):
                profit = prices[j] - prices[j-1]
                cur = max(dp[i-1][j-1] + profit, dp[i-1][j-1], prev + profit)
                dp[i][j] = max(dp[i][j-1], cur)
                prev = cur
        return dp[-1][-1] if n else 0

if __name__ == "__main__":
    tests = (
        (2, [], 0),
        (2, [2,4,1], 2),
        (2, [3,2,6,5,0,3], 7),
        (1, [6,1,6,4,3,0,2], 5),
        (2, [3,3,5,0,0,3,1,4], 6),
        (4, [1,2,4,2,5,7,2,4,9,0], 15),
        (2, [1,2,4,2,5,7,2,4,9,0], 13),
        (7, [48,12,60,93,97,42,25,64,17,56,85,93,9,48,52,42,58,85,81,84,69,36,1,54,23,15,72,15,11,94], 469),
    )
    for k, prices, expected in tests:
        print(Solution().maxProfit(k, prices) == expected)
