/**
 * 5. Longest Palindromic Substring [Medium]
 * Given a string s, return the longest palindromic substring in s.
 *
 * Example 1:
 * Input: s = "babad"
 * Output: "bab"
 * Explanation: "aba" is also a valid answer.
 *
 * Example 2:
 * Input: s = "cbbd"
 * Output: "bb"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 1000
 *     ∙ s consist of only digits and English letters.
 */
#include <vector>
#include <string>

class Solution {
public:
    std::string longestPalindrome(std::string s) {
        if (s.empty()) {
            return "";
        }
        std::string ss = "^";
        for (char c : s) {
            ss += '|';
            ss += c;
        }
        ss += "|$";
        std::vector<int> radiuses = manacherOdd(ss);

        int maxRadius = *std::max_element(radiuses.begin(), radiuses.end());
        int center = std::distance(
            radiuses.begin(),
            std::find(radiuses.begin(), radiuses.end(), maxRadius)
        );

        int start = (center - maxRadius) / 2;
        int end = start + maxRadius;

        return s.substr(start, end - start);
    }

private:
    std::vector<int> manacherOdd(std::string s) {
        std::vector<int> radii(s.length(), 0);
        int center = 0;
        int right = 0;

        for (int i = 1; i < s.length() - 1; ++i) {
            int mirror = 2 * center - i;

            if (i < right) {
                radii[i] = std::min(right - i, radii[mirror]);
            }

            while (s[i + radii[i] + 1] == s[i - radii[i] - 1]) {
                radii[i]++;
            }

            if (i + radii[i] > right) {
                center = i;
                right = i + radii[i];
            }
        }

        return radii;
    }
};
