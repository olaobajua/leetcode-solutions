"""
 * 1759. Count Number of Homogenous Substrings [Medium]
 * Given a string s, return the number of homogenous substrings of s. Since
 * the answer may be too large, return it modulo 10⁹ + 7.
 *
 * A string is homogenous if all the characters of the string are the same.
 *
 * A substring is a contiguous sequence of characters within a string.
 *
 *
 * Example 1:
 * Input: s = "abbcccaa"
 * Output: 13
 * Explanation: The homogenous substrings are listed as below:
 * "a"   appears 3 times.
 * "aa"  appears 1 time.
 * "b"   appears 2 times.
 * "bb"  appears 1 time.
 * "c"   appears 3 times.
 * "cc"  appears 2 times.
 * "ccc" appears 1 time.
 * 3 + 1 + 2 + 1 + 3 + 2 + 1 = 13.
 *
 * Example 2:
 * Input: s = "xy"
 * Output: 2
 * Explanation: The homogenous substrings are "x" and "y".
 *
 * Example 3:
 * Input: s = "zzzzz"
 * Output: 15
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ s consists of lowercase letters.
"""
from collections import Counter

class Solution:
    def countHomogenous(self, s: str) -> int:
        MOD = 1000000007
        ret = 0
        prev = None
        cur_count = 0
        for c in s:
            if c == prev:
                cur_count += 1
            else:
                cur_count = 1
            ret = (ret + cur_count) % MOD
            prev = c

        return ret

if __name__ == "__main__":
    tests = (
        ("abbcccaa", 13),
        ("xy", 2),
        ("zzzzz", 15),
    )
    for s, expected in tests:
        print(Solution().countHomogenous(s) == expected)
