"""
 * 430. Flatten a Multilevel Doubly Linked List (Medium)
 * You are given a doubly linked list which in addition to the next and
 * previous pointers, it could have a child pointer, which may or may not point
 * to a separate doubly linked list. These child lists may have one or more
 * children of their own, and so on, to produce a multilevel data structure, as
 * shown in the example below.
 *
 * Flatten the list so that all the nodes appear in a single-level, doubly
 * linked list. You are given the head of the first level of the list.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
 * Output: [1,2,3,7,8,11,12,9,10,4,5,6]
 * Explanation:
 * The multilevel linked list in the input is as follows:
 *  1---2---3---4---5---6--NULL
 *          |
 *          7---8---9---10--NULL
 *              |
 *              11--12--NULL
 *
 * After flattening the multilevel linked list it becomes:
 *  1---2---3---7---8---11---12---9---10---4---5---6
 *
 * Example 2:
 * Input: head = [1,2,null,3]
 * Output: [1,3,2]
 * Explanation:
 * The input multilevel linked list is as follows:
 *
 *   1---2---NULL
 *   |
 *   3---NULL
 *
 * Example 3:
 * Input: head = []
 * Output: []
 *
 * How multilevel linked list is represented in test case:
 *
 * We use the multilevel linked list from Example 1 above:
 *  1---2---3---4---5---6--NULL
 *          |
 *          7---8---9---10--NULL
 *              |
 *              11--12--NULL
 *
 * The serialization of each level is as follows:
 * [1,2,3,4,5,6,null]
 * [7,8,9,10,null]
 * [11,12,null]
 *
 * To serialize all levels together we will add nulls in each level to signify
 * no node connects to the upper node of the previous level. The serialization
 * becomes:
 * [1,2,3,4,5,6,null]
 * [null,null,7,8,9,10,null]
 * [null,11,12,null]
 *
 * Merging the serialization of each level and removing trailing nulls we
 * obtain:
 * [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
 *
 * Constraints:
 *     The number of Nodes will not exceed 1000.
 *     1 <= Node.val <= 10⁵
"""
from typing import List

# Definition for a Node.
class Node:
    def __init__(self, val, prev, next, child):
        self.val = val
        self.prev = prev
        self.next = next
        self.child = child

class Solution:
    def flatten(self, head: 'Node') -> 'Node':
        if head:
            prev = root = Node(0, None, head, None)
            stack = [head]
            while stack:
                cur = stack.pop()
                prev.next = cur
                cur.prev = prev
                if cur.next:
                    stack.append(cur.next)
                if cur.child:
                    stack.append(cur.child)
                    cur.child = None
                prev = cur
            root = root.next
            root.prev = None
            return root

def list_to_doubly_linked_list(nums):
    root = Node(nums.pop(0), None, None, None) if nums else None
    cur = root
    nodes = [cur]
    insert = 1
    while nums:
        if (num := nums.pop(0)) is not None:
            if cur.next is None:
                cur.next = Node(num, cur, None, None)
                cur = cur.next
                nodes.insert(insert, cur)
                insert += 1
            else:
                cur.child = Node(num, cur, None, None)
                cur = cur.child
                nodes.insert(0, cur)
                insert = 1
        else:
            cur = nodes.pop(0)
    return root

def doubly_linked_list_to_list(root):
    ret = []
    nodes = [root]
    insert = 1
    while nodes:
        root = nodes.pop(0)
        while root:
            ret.append(root.val)
            nodes.insert(insert, root.child)
            insert += 1
            root = root.next
        insert = 0
        ret.append(None)

    while ret and ret[-1] is None:
        ret.pop()

    return ret

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5,6,None,None,None,7,8,9,10,None,None,11,12], [1,2,3,7,8,11,12,9,10,4,5,6]),
        ([1,2,None,3], [1,3,2]),
        ([], []),
    )
    for nums, expected in tests:
        root = list_to_doubly_linked_list(nums)
        print(doubly_linked_list_to_list(Solution().flatten(root)) == expected)
