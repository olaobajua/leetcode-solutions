/**
 * 387. First Unique Character in a String [Easy]
 * Given a string s, find the first non-repeating character in it and return
 * its index. If it does not exist, return -1.
 *
 * Example 1:
 * Input: s = "leetcode"
 * Output: 0
 * Example 2:
 * Input: s = "loveleetcode"
 * Output: 2
 * Example 3:
 * Input: s = "aabb"
 * Output: -1
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ s consists of only lowercase English letters.
 */
class Solution {
    public int firstUniqChar(String s) {
        int[] count = new int[123];
        for (int i = 0; i < s.length(); ++i)
            ++count[(int)s.charAt(i)];
        for (int i = 0; i < s.length(); ++i)
            if (count[(int)s.charAt(i)] == 1)
                return i;
        return -1;
    }
}
