/**
 * 745. Prefix and Suffix Search (Hard)
 * Design a special dictionary with some words that searchs the words in it by
 * a prefix and a suffix.
 *
 * Implement the WordFilter class:
 *     ∙ WordFilter(string[] words) Initializes the object with the words in
 *       the dictionary.
 *     ∙ f(string prefix, string suffix) Returns the index of the word in the
 *       dictionary, which has the prefix prefix and the suffix suffix. If
 *       there is more than one valid index, return the largest of them. If
 *       there is no such word in the dictionary, return -1.
 *
 * Example 1:
 * Input
 * ["WordFilter", "f"]
 * [[["apple"]], ["a", "e"]]
 * Output
 * [null, 0]
 *
 * Explanation
 * WordFilter wordFilter = new WordFilter(["apple"]);
 * wordFilter.f("a", "e"); // return 0, because the word at index 0 has
 * prefix = "a" and suffix = 'e".
 *
 * Constraints:
 *     ∙ 1 <= words.length <= 15000
 *     ∙ 1 <= words[i].length <= 10
 *     ∙ 1 <= prefix.length, suffix.length <= 10
 *     ∙ words[i], prefix and suffix consist of lower-case English letters only
 *     ∙ At most 15000 calls will be made to the function f
 */
#include <stdlib.h>
#include <stdio.h>

#define CHARS_LEN 27

typedef struct WordFilter {
    struct WordFilter *children[CHARS_LEN];
    int index;
} WordFilter; // TrieNode

WordFilter* wordFilterCreate(char** words, int wordsSize) {
    WordFilter *prefixes = (WordFilter*)calloc(1, sizeof(WordFilter));
    for (int i = 0; i < wordsSize; ++i) {
        for (int j = 0; words[i][j] != '\0'; ++j) {
            WordFilter *p = prefixes;
            char w[22] = "\0";
            sprintf(w, "%s%c%s", words[i] + j, '`', words[i]);
            for (int c = 0; w[c] != '\0'; ++c) {
                if (!p->children[w[c]-'`'])
                    p->children[w[c]-'`'] = (WordFilter*)calloc(
                            1, sizeof(WordFilter));
                p = p->children[w[c]-'`'];
                p->index = i;
            }
        }
    }
    return prefixes;
}

int wordFilterF(WordFilter *obj, char *prefix, char *suffix) {
    WordFilter *p = obj;
    char w[22] = "\0";
    sprintf(w, "%s%c%s", suffix, '`', prefix);
    for (int c = 0; w[c] != '\0'; ++c) {
        if (p->children[w[c]-'`'] == NULL) {
            return -1;
        }
        p = p->children[w[c]-'`'];
    }
    return p->index;
}

void wordFilterFree(WordFilter *obj) {
    if (obj) {
        for (int i = 0; i < CHARS_LEN; ++i) {
            wordFilterFree(obj->children[i]);
        }
        free(obj);
    }
}

int main() {
    char *words1[2] = {"apple", "banana"};
    WordFilter* obj = wordFilterCreate(words1, 2);
    printf("%d\n", 0 == wordFilterF(obj, "a", "e"));
    printf("%d\n", -1 == wordFilterF(obj, "b", "e"));
    printf("%d\n", 1 == wordFilterF(obj, "b", "a"));
    wordFilterFree(obj);
    char *words2[10] = {"cabaabaaaa", "ccbcababac", "bacaabccba", "bcbbcbacaa",
                        "abcaccbcaa", "accabaccaa", "cabcbbbcca", "ababccabcb",
                        "caccbbcbab","bccbacbcba"};
    obj = wordFilterCreate(words2, 10);
    printf("%d\n", 9 == wordFilterF(obj, "bccbacbcba", "a"));
    printf("%d\n", 4 == wordFilterF(obj, "ab", "abcaccbcaa"));
    printf("%d\n", 5 == wordFilterF(obj, "a", "aa"));
    printf("%d\n", 0 == wordFilterF(obj, "cabaaba", "abaaaa"));
    printf("%d\n", 8 == wordFilterF(obj, "cacc", "accbbcbab"));
    printf("%d\n", 1 == wordFilterF(obj, "ccbcab", "bac"));
    printf("%d\n", 2 == wordFilterF(obj, "bac", "cba"));
    printf("%d\n", 5 == wordFilterF(obj, "ac", "accabaccaa"));
    printf("%d\n", 3 == wordFilterF(obj, "bcbb", "aa"));
    printf("%d\n", 1 == wordFilterF(obj, "ccbca", "cbcababac"));
    wordFilterFree(obj);
}
