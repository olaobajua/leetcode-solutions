"""
 * 936. Stamping The Sequence [Hard]
 * You are given two strings stamp and target. Initially, there is a string s
 * of length target.length with all s[i] == '?'.
 *
 * In one turn, you can place stamp over s and replace every letter in the s
 * with the corresponding letter from stamp.
 *
 * For example, if stamp = "abc" and target = "abcba", then s is "?????"
 * initially. In one turn you can:
 * 	    ∙ place stamp at index 0 of s to obtain "abc??",
 * 	    ∙ place stamp at index 1 of s to obtain "?abc?", or
 * 	    ∙ place stamp at index 2 of s to obtain "??abc".
 *
 * Note that stamp must be fully contained in the boundaries of s in order to
 * stamp (i.e., you cannot place stamp at index 3 of s).
 *
 * We want to convert s to target using at most 10 * target.length turns.
 *
 * Return an array of the index of the left-most letter being stamped at each
 * turn. If we cannot obtain target from s within 10 * target.length turns,
 * return an empty array.
 *
 * Example 1:
 * Input: stamp = "abc", target = "ababc"
 * Output: [0,2]
 * Explanation: Initially s = "?????".
 * - Place stamp at index 0 to get "abc??".
 * - Place stamp at index 2 to get "ababc".
 * [1,0,2] would also be accepted as an answer, as well as some other answers.
 *
 * Example 2:
 * Input: stamp = "abca", target = "aabcaca"
 * Output: [3,0,1]
 * Explanation: Initially s = "???????".
 * - Place stamp at index 3 to get "???abca".
 * - Place stamp at index 0 to get "abcabca".
 * - Place stamp at index 1 to get "aabcaca".
 *
 * Constraints:
 *     ∙ 1 <= stamp.length <= target.length <= 1000
 *     ∙ stamp and target consist of lowercase English letters.
"""
from typing import List

class Solution:
    def movesToStamp(self, stamp: str, target: str) -> List[int]:
        m = len(stamp)
        target = list(target)
        prev = -1
        ret = []
        while len(ret) > prev:
            prev = len(ret)
            for i in range(len(target) - m + 1):
                if all(a in ('?', b) for a, b in zip(target[i:i+m], stamp)):
                    if set(target[i:i+m]) != {'?'}:
                        target[i:i+m] = ['?'] * m
                        ret.append(i)
        return ret[::-1] if set(target) == {'?'} else []

if __name__ == "__main__":
    tests = (
        ("abc", "ababc", [0,2]),
        ("abca", "aabcaca", [3,0,1]),
        ("by", "bbybyybyby", [5,7,4,2,0,8,6,3,1]),
        ("aye", "eyeye", []),
        ("cbcf", "ccbbf", []),
        ("de", "ddeddeddee", [5,2,8,6,3,0,7,4,1]),
        ("mda", "mdadddaaaa", []),
    )
    for stamp, target, expected in tests:
        if expected == []:
            print(Solution().movesToStamp(stamp, target) == expected)
        else:
            ret = [''] * len(target)
            m = len(stamp)
            for i in Solution().movesToStamp(stamp, target):
                for j in range(i, i + m):
                    ret[j] = stamp[j-i]
            print(''.join(ret) == target)
