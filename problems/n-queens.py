"""
 * 51. N-Queens (Hard)
 * The n-queens puzzle is the problem of placing n queens on an n x n
 * chessboard such that no two queens attack each other.
 *
 * Given an integer n, return all distinct solutions to the n-queens puzzle.
 * You may return the answer in any order.
 *
 * Each solution contains a distinct board configuration of the n-queens'
 * placement, where 'Q' and '.' both indicate a queen and an empty space,
 * respectively.
 *
 * Example 1:
 * Input: n = 4
 * Output: [[".Q..",
 *           "...Q",
 *           "Q...",
 *           "..Q."],
 *          ["..Q.",
 *           "Q...",
 *           "...Q",
 *           ".Q.."]]
 * Explanation: There exist two distinct solutions to the 4-queens puzzle as
 * shown above
 *
 * Example 2:
 * Input: n = 1
 * Output: [["Q"]]
 *
 * Constraints:
 *     ∙ 1 <= n <= 9
"""
from typing import List

class Solution:
    def solveNQueens(self, n: int) -> List[List[str]]:
        def dfs(queens, ld, rd):
            p = len(queens)
            if p == n:
                yield queens
            else:
                for q in range(n):
                    if q not in queens and p-q not in ld and p+q not in rd:
                        yield from dfs(queens + [q], ld + [p-q], rd + [p+q])
        return [['.' * i + "Q" + '.' * (n - i - 1) for i in queens]
                for queens in dfs([], [], [])]

if __name__ == "__main__":
    tests = (
        (4, [[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]),
        (1, [["Q"]]),
    )
    for n, expected in tests:
        print(set(map(tuple, Solution().solveNQueens(n))) ==
              set(map(tuple, expected)))
