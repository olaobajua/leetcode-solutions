"""
 * 101. Symmetric Tree [Easy]
 * Given the root of a binary tree, check whether it is a mirror of itself
 * (i.e., symmetric around its center).
 *
 * Example 1:
 *     1
 *  2     2
 * 3 4   4 3
 * Input: root = [1,2,2,3,4,4,3]
 * Output: true
 *
 * Example 2:
 *     1
 *  2     2
 *   3     3
 * Input: root = [1,2,2,null,3,null,3]
 * Output: false
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 1000].
 *     ∙ -100 <= Node.val <= 100
 *
 * Follow up: Could you solve it both recursively and iteratively?
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def isSymmetric(self, root: Optional[TreeNode]) -> bool:
        def check(left, right):
            if left is None and right is not None:
                return False
            if left is not None and right is None:
                return False
            if left is None and right is None:
                return True
            if left.val != right.val:
                return False
            return check(left.left, right.right) and check(left.right, right.left)
        return check(root.left, root.right)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,2,3,4,4,3], True),
        ([1,2,2,None,3,None,3], False),
    )
    for nums, expected in tests:
        print(Solution().isSymmetric(build_tree(nums)) == expected)
