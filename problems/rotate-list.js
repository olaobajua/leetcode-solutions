/**
 * 61. Rotate List (Medium)
 * Given the head of a linked list, rotate the list to the right by k places.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [4,5,1,2,3]
 *
 * Example 2:
 * Input: head = [0,1,2], k = 4
 * Output: [2,0,1]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 500].
 *     ∙ -100 <= Node.val <= 100
 *     ∙ 0 <= k <= 2 * 109
 */


// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
var rotateRight = function(head, k) {
    if (!head) {
        return head;
    }
    let end = head, n = 1;
    while (end && end.next) {
        end = end.next;
        ++n;
    }
    if ((k %= n) == 0) {
        return head;
    }
    let mid = head;
    for (let i = 0; i < n - k - 1; ++i) {
        mid = mid.next;
    }
    end.next = head;
    head = mid.next;
    mid.next = null;
    return head;
};

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    [[1,2,3,4,5], 2, [4,5,1,2,3]],
    [[1,2,3,4,5,6], 2, [5,6,1,2,3,4]],
    [[0,1,2], 4, [2,0,1]],
    [[], 1, []],
];
for (const [nums, k, expected] of tests) {
    const root = arrayToLinkedList(nums);
    console.log(linkedListToArray(rotateRight(root, k)).toString() ==
                expected.toString());
}
