/**
 * 1249. Minimum Remove to Make Valid Parentheses (Medium)
 * Given a string s of '(' , ')' and lowercase English characters.
 *
 * Your task is to remove the minimum number of parentheses ( '(' or ')', in
 * any positions ) so that the resulting parentheses string is valid and return
 * any valid string.
 *
 * Formally, a parentheses string is valid if and only if:
 *     ∙ It is the empty string, contains only lowercase characters, or
 *     ∙ It can be written as AB (A concatenated with B), where A and B are
 *       valid strings, or
 *     ∙ It can be written as (A), where A is a valid string.
 *
 * Example 1:
 * Input: s = "lee(t(c)o)de)"
 * Output: "lee(t(c)o)de"
 * Explanation: "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted.
 *
 * Example 2:
 * Input: s = "a)b(c)d"
 * Output: "ab(c)d"
 *
 * Example 3:
 * Input: s = "))(("
 * Output: ""
 * Explanation: An empty string is also valid.
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ s[i] is either'(' , ')', or lowercase English letter.
 */
char * minRemoveToMakeValid(char *s) {
    static int stack[100000];
    int n = 0;
    for (int i = 0; s[i] != '\0'; ++i) {
        if (s[i] == '(') {
            stack[n++] = i;
        } else if (s[i] == ')') {
            if (n) {
                --n;
            } else {
                s[i] = 1;
            }
        }
    };
    for (int i = 0; i < n; ++i) {
        s[stack[i]] = 1;
    }
    static char ret[100000];
    n = 0;
    for (int i = 0; s[i] != '\0'; ++i) {
        if (s[i] != 1) {
            ret[n++] = s[i];
        }
    }
    ret[n] = '\0';
    return ret;
}
