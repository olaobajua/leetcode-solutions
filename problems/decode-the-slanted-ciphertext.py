"""
 * 2075. Decode the Slanted Ciphertext (Medium)
 * A string originalText is encoded using a slanted transposition cipher to a
 * string encodedText with the help of a matrix having a fixed number of rows
 * rows.
 *
 * originalText is placed first in a top-left to bottom-right manner.
 *
 * All empty cells are filled with ' '. The number of columns is chosen such
 * that the rightmost column will not be empty after filling in originalText.
 *
 * encodedText is then formed by appending all characters of the matrix in a
 * row-wise fashion.
 *
 * For example, if originalText = "cipher" and rows = 3, then we encode it in
 * the following manner:
 * c h . .
 * . i e .
 * . . p r
 *
 * In the above example, encodedText = "ch ie pr".
 *
 * Given the encoded string encodedText and number of rows rows, return the
 * original string originalText.
 *
 * Note: originalText does not have any trailing spaces ' '. The test cases are
 * generated such that there is only one possible originalText.
 *
 * Example 1:
 * c h . .
 * . i e .
 * . . p r
 * Input: encodedText = "ch   ie   pr", rows = 3
 * Output: "cipher"
 * Explanation: This is the same example described in the problem description.
 *
 * Example 2:
 * i v e o . .
 * . . e e d .
 * . . l . t e
 * . . . o l c
 * Input: encodedText = "iveo    eed   l te   olc", rows = 4
 * Output: "i love leetcode"
 * Explanation: The figure above denotes the matrix that was used to encode
 * originalText.
 * The blue arrows show how we can find originalText from encodedText.
 *
 * Example 3:
 * c o d i n g
 * Input: encodedText = "coding", rows = 1
 * Output: "coding"
 * Explanation: Since there is only 1 row, both originalText and encodedText
 * are the same.
 *
 * Example 4:
 * . b .
 * . a c
 * Input: encodedText = " b  ac", rows = 2
 * Output: " abc"
 * Explanation: originalText cannot have trailing spaces, but it may be
 * preceded by one or more spaces.
 *
 * Constraints:
 *     ∙ 0 <= encodedText.length <= 10⁶
 *     ∙ encodedText consists of lowercase English letters and ' ' only.
 *     ∙ encodedText is a valid encoding of some originalText that does not
 *       have trailing spaces.
 *     ∙ 1 <= rows <= 1000
 *     ∙ The testcases are generated such that there is only one possible
 *       originalText.
"""
class Solution:
    def decodeCiphertext(self, encodedText: str, rows: int) -> str:
        cols = len(encodedText) // rows
        return ''.join(encodedText[r * cols + c + r] for c in range(cols)
                       for r in range(rows) if c + r < cols).rstrip()

if __name__ == "__main__":
    tests = (
        ("ch   ie   pr", 3, "cipher"),
        ("iveo    eed   l te   olc", 4, "i love leetcode"),
        ("coding", 1, "coding"),
        (" b  ac", 2, " abc"),
    )
    for encodedText, rows, expected in tests:
        print(Solution().decodeCiphertext(encodedText, rows) == expected)
