/**
 * 228. Summary Ranges (Easy)
 * You are given a sorted unique integer array nums.
 *
 * Return the smallest sorted list of ranges that cover all the numbers in the
 * array exactly. That is, each element of nums is covered by exactly one of
 * the ranges, and there is no integer x such that x is in one of the ranges
 * but not in nums.
 *
 * Each range [a,b] in the list should be output as:
 *     ∙ "a->b" if a != b
 *     ∙ "a" if a == b
 *
 * Example 1:
 * Input: nums = [0,1,2,4,5,7]
 * Output: ["0->2","4->5","7"]
 * Explanation: The ranges are:
 * [0,2] --> "0->2"
 * [4,5] --> "4->5"
 * [7,7] --> "7"
 *
 * Example 2:
 * Input: nums = [0,2,3,4,6,8,9]
 * Output: ["0","2->4","6","8->9"]
 * Explanation: The ranges are:
 * [0,0] --> "0"
 * [2,4] --> "2->4"
 * [6,6] --> "6"
 * [8,9] --> "8->9"
 *
 * Constraints:
 *     ∙ 0 <= nums.length <= 20
 *     ∙ -2³¹ <= nums[i] <= 2³¹ - 1
 *     ∙ All the values of nums are unique.
 *     ∙ nums is sorted in ascending order.
 */

/**
 * @param {number[]} nums
 * @return {string[]}
 */
var summaryRanges = function(nums) {
    const ret = [];
    let start = null, end = null;
    for (let i = 0; i < nums.length; ++i) {
        if (nums[i+1] === nums[i] + 1) {
            if (start === null) {
                start = nums[i];
            }
            end = nums[i+1];
        } else {
            if (start === null) {
                ret.push(nums[i].toString())
            } else {
                ret.push(`${start}->${end}`)
            }
            start = end = null;
        }
    }
    return ret;
};

const tests = [
    [[0,1,2,4,5,7], ["0->2","4->5","7"]],
    [[0,2,3,4,6,8,9], ["0","2->4","6","8->9"]],
    [[], []],
];
for (const [nums, expected] of tests) {
    console.log(summaryRanges(nums).toString() == expected.toString());
}
