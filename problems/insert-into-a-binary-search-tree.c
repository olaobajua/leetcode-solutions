/**
 * 701. Insert into a Binary Search Tree (Medium)
 * You are given the root node of a binary search tree (BST) and a value to
 * insert into the tree. Return the root node of the BST after the insertion.
 * It is guaranteed that the new value does not exist in the original BST.
 *
 * Notice that there may exist multiple valid ways for the insertion, as long
 * as the tree remains a BST after insertion. You can return any of them.
 *
 * Example 1:
 *     4              4
 *  2     7         2     7
 * 1 3             1 3   5
 * Input: root = [4,2,7,1,3], val = 5
 * Output: [4,2,7,1,3,5]
 * Explanation: Another accepted tree is:
 *     5
 *  2     7
 * 1 3
 *    4
 *
 * Example 2:
 * Input: root = [40,20,60,10,30,50,70], val = 25
 * Output: [40,20,60,10,30,50,70,null,null,25]
 *
 * Example 3:
 * Input: root = [4,2,7,1,3,null,null,null,null,null,null], val = 5
 * Output: [4,2,7,1,3,5]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree will be in the range [0, 10⁴].
 *     ∙ -10⁸ <= Node.val <= 10⁸
 *     ∙ All the values Node.val are unique.
 *     ∙ -10⁸ <= val <= 10⁸
 *     ∙ It's guaranteed that val does not exist in the original BST.
 */

// Definition for a binary tree node.
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

struct TreeNode * add_node(struct TreeNode *parent, struct TreeNode *child) {
    /* Creates sorted tree. */
    if (!parent)
        return child;
    if (child->val < parent->val)
        parent->left = add_node(parent->left, child);
    else
        parent->right = add_node(parent->right, child);
    return parent;
}

struct TreeNode* insertIntoBST(struct TreeNode* root, int val){
    static struct TreeNode node;
    node.val = val;
    node.left = NULL;
    node.right = NULL;
    return add_node(root, &node);
}
