/*
 * 279. Perfect Squares [Medium]
 * Given an integer n, return the least number of perfect square numbers that
 * sum to n.
 *
 * A perfect square is an integer that is the square of an integer; in other
 * words, it is the product of some integer with itself. For example, 1, 4, 9,
 * and 16 are perfect squares while 3 and 11 are not.
 *
 * Example 1:
 * Input: n = 12
 * Output: 3
 * Explanation: 12 = 4 + 4 + 4.
 *
 * Example 2:
 * Input: n = 13
 * Output: 2
 * Explanation: 13 = 4 + 9.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁴
 */
class Solution {
    public int numSquares(int n) {
        if (isSquare(n))
            return 1;
        List<Integer> nums = new ArrayList(Arrays.asList(n));
        int minSquares = 0;
        while (nums.size() > 0) {
            ++minSquares;
            for (int i = nums.size(); i > 0; --i) {
                int num = nums.remove(0);
                for (int j = (int)Math.sqrt(num) + 1; j > 0; --j) {
                    int nextNum = num - j*j;
                    if (isSquare(nextNum))
                        return minSquares + 1;
                    nums.add(nextNum);
                }
            }
        }
        return 4;
    }
    private boolean isSquare(int x) {
        return (double)Math.sqrt(x) % 1 == 0;
    }
}
