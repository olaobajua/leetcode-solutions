/**
 * 304. Range Sum Query 2D - Immutable (Medium)
 * Given a 2D matrix matrix, handle multiple queries of the following type:
 *     ∙ Calculate the sum of the elements of matrix inside the rectangle
 *       defined by its upper left corner (row1, col1) and lower right corner
 *       (row2, col2).
 *
 * Implement the NumMatrix class:
 *     ∙ NumMatrix(int[][] matrix) Initializes the object with the integer
 *       matrix matrix.
 *     ∙ int sumRegion(int row1, int col1, int row2, int col2) Returns the sum
 *       of the elements of matrix inside the rectangle defined by its upper
 *       left corner (row1, col1) and lower right corner (row2, col2).
 *
 * Example 1:
 * Input
 * ["NumMatrix", "sumRegion", "sumRegion", "sumRegion"]
 * [[[[3, 0, 1, 4, 2],
 *    [5, 6, 3, 2, 1],
 *    [1, 2, 0, 1, 5],
 *    [4, 1, 0, 1, 7],
 *    [1, 0, 3, 0, 5]]],
 *  [2, 1, 4, 3], [1, 1, 2, 2], [1, 2, 2, 4]]
 * Output
 * [null, 8, 11, 12]
 *
 * Explanation
 * NumMatrix numMatrix = new NumMatrix([[3, 0, 1, 4, 2],
 *                                      [5, 6, 3, 2, 1],
 *                                      [1, 2, 0, 1, 5],
 *                                      [4, 1, 0, 1, 7],
 *                                      [1, 0, 3, 0, 5]]);
 * numMatrix.sumRegion(2, 1, 4, 3); // return 8
 * numMatrix.sumRegion(1, 1, 2, 2); // return 11
 * numMatrix.sumRegion(1, 2, 2, 4); // return 12
 *
 * Constraints:
 *     ∙ m == matrix.length
 *     ∙ n == matrix[i].length
 *     ∙ 1 <= m, n <= 200
 *     ∙ -10⁵ <= matrix[i][j] <= 10⁵
 *     ∙ 0 <= row1 <= row2 < m
 *     ∙ 0 <= col1 <= col2 < n
 *     ∙ At most 10⁴ calls will be made to sumRegion.
 */
typedef struct {
    int **sum;
    int rowCount;
    int colCount;
} NumMatrix;

NumMatrix* numMatrixCreate(int** matrix, int matrixSize, int* matrixColSize) {
    NumMatrix *ret = (NumMatrix*)calloc(1, sizeof(NumMatrix));
    ret->rowCount = matrixSize;
    ret->colCount = matrixColSize[0];
    for (int r = 0; r < matrixSize; ++r) {
        for (int c = 0; c < matrixColSize[0]; ++c) {
            if (r > 0) {
                matrix[r][c] += matrix[r-1][c];
            }
            if (c > 0) {
                matrix[r][c] += matrix[r][c-1];
            }
            if (r > 0 && c > 0) {
                matrix[r][c] -= matrix[r-1][c-1];
            }
        }
    }
    ret->sum = matrix;
    return ret;
}

int numMatrixSumRegion(NumMatrix* obj, int row1, int col1, int row2, int col2) {
    int ret = obj->sum[row2][col2];
    if (row1 > 0) {
        ret -= obj->sum[row1-1][col2];
    }
    if (col1 > 0) {
        ret -= obj->sum[row2][col1-1];
    }
    if (row1 > 0 && col1 > 0) {
        ret += obj->sum[row1-1][col1-1];
    }
    return ret;
}

void numMatrixFree(NumMatrix* obj) {
    free(obj);
}
/**
 * Your NumMatrix struct will be instantiated and called as such:
 * NumMatrix* obj = numMatrixCreate(matrix, matrixSize, matrixColSize);
 * int param_1 = numMatrixSumRegion(obj, row1, col1, row2, col2);

 * numMatrixFree(obj);
*/
