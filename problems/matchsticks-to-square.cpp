/**
 * 473. Matchsticks to Square (Medium)
 * You are given an integer array matchsticks where matchsticks[i] is the
 * length of the iᵗʰ matchstick. You want to use all the matchsticks to make
 * one square. You should not break any stick, but you can link them up, and
 * each matchstick must be used exactly one time.
 *
 * Return true if you can make this square and false otherwise.
 *
 * Example 1:
 * Input: matchsticks = [1,1,2,2,2]
 * Output: true
 * Explanation: You can form a square with length 2, one side of the square
 * came two sticks with length 1.
 *
 * Example 2:
 * Input: matchsticks = [3,3,3,3,4]
 * Output: false
 * Explanation: You cannot find a way to form a square with all the
 * matchsticks.
 *
 * Constraints:
 *     ∙ 1 <= matchsticks.length <= 15
 *     ∙ 1 <= matchsticks[i] <= 10⁸
 */
class Solution {
private:
    vector<int> matchsticks;
    vector<int> stick_sum_cache;
    vector<int> match_sides;
    int FULL_MATCH;
public:
    bool makesquare(vector<int>& matchsticks) {
        int perimeter = accumulate(matchsticks.begin(), matchsticks.end(), 0);
        if (perimeter % 4) {
            return false;
        }
        int side = perimeter / 4;
        if (perimeter - *max_element(begin(matchsticks), end(matchsticks)) < 3 * side) {
            return false;
        }
        const int n = matchsticks.size();
        this->matchsticks = matchsticks;
        stick_sum_cache = vector<int>(1 << n, 0);
        FULL_MATCH = (1 << matchsticks.size()) - 1;
        for (int i = 0; i < (1 << n); ++i) {
            if (stick_sum(i) == side) {
                match_sides.push_back(i);
            }
        }
        return can_make_square(0, 0);
    }

    int stick_sum(int i) {
        if (stick_sum_cache[i] == 0) {
            if (i) {
                for (int j = 0; j < matchsticks.size(); ++j) {
                    if (i & (1 << j)) {
                        stick_sum_cache[i] = matchsticks[j] + \
                                             stick_sum(i ^ (1 << j));
                        return stick_sum_cache[i];
                    }
                }
            }
        }
        return stick_sum_cache[i];
    }

    bool can_make_square(int i, int sticks) {
        if (i < match_sides.size()) {
            if ((match_sides[i] & sticks) == 0) {
                if ((match_sides[i] | sticks) == FULL_MATCH) {
                    return true;
                }
                if (can_make_square(i + 1, match_sides[i] | sticks)) {
                    return true;
                }
                if (can_make_square(i + 1, sticks)) {
                    return true;
                }
            } else if (can_make_square(i + 1, sticks)) {
                return true;
            }
        }
        return false;
    }
};
