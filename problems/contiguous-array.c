/**
 * 525. Contiguous Array (Medium)
 * Given a binary array nums, return the maximum length of a contiguous
 * subarray with an equal number of 0 and 1.
 *
 * Example 1:
 * Input: nums = [0,1]
 * Output: 2
 * Explanation: [0, 1] is the longest contiguous subarray with an equal number
 * of 0 and 1.
 *
 * Example 2:
 * Input: nums = [0,1,0]
 * Output: 2
 * Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal
 * number of 0 and 1.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ nums[i] is either 0 or 1.
 */
int findMaxLength(int *nums, int numsSize) {
    int *diffs = calloc(2*numsSize + 1, sizeof(int));
    int ret = 0, ones = 0, zeroes = 0;
    for (int i = 0; i < numsSize; ++i) {
        nums[i] == 0 ? ++zeroes : ++ones;
        if (diffs[numsSize+ones-zeroes] == 0 && ones != zeroes) {
            diffs[numsSize+ones-zeroes] = i + 1;
        };
        ret = fmax(ret, i + 1 - diffs[numsSize+ones-zeroes]);
    };
    free(diffs);
    return ret;
}
