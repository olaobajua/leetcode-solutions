/**
 * 952. Largest Component Size by Common Factor (Hard)
 * You are given an integer array of unique positive integers nums. Consider
 * the following graph:
 *     ∙ There are nums.length nodes, labeled nums[0] to nums[nums.length - 1],
 *     ∙ There is an undirected edge between nums[i] and nums[j] if nums[i] and
 *       nums[j] share a common factor greater than 1.
 *
 * Return the size of the largest connected component in the graph.
 *
 * Example 1:
 * Input: nums = [4,6,15,35]
 * Output: 4
 *
 * Example 2:
 * Input: nums = [20,50,9,63]
 * Output: 2
 *
 * Example 3:
 * Input: nums = [2,3,6,7,4,12,21,39]
 * Output: 8
 *
 * Constraints:
 *     1 <= nums.length <= 2 * 10⁴
 *     1 <= nums[i] <= 10⁵
 *     All the values of nums are unique.
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var largestComponentSize = function(nums) {
    const groups = new DisjointSet();
    for (const n of nums) {
        groups.makeSet(n);
        for (const f of factors(n)) {
            groups.makeSet(f);
            groups.union(f, n);
        }
    }
    return Math.max(...new Counter(nums.map(n => groups.find(n))).values());
};

class DisjointSet {
    constructor(name) {
        Object.defineProperty(this, 'parent', { value: {} });
    }
    makeSet(x) {
        if (this.parent[x] === undefined) {
            this.parent[x] = x;
        }
    }
    find(x) {
        if (x != this.parent[x]) {
            this.parent[x] = this.find(this.parent[x]);
        }
        return this.parent[x];
    }
    union(x, y) {
        const px = this.find(x);
        const py = this.find(y);
        if (px !== undefined && py !== undefined && px != py) {
            this.parent[px] = py;
        }
    }
}

function factors(n) {
    const ret = new Set();
    for (let i = 2; i * i <= n; ++i) {
        if (n % i === 0) {
            ret.add(i);
            ret.add(n / i);
        }
    }
    return Array.from(ret);
}

class Counter extends Map {
    constructor(iterable) {
        super();
        [...iterable].forEach(x => this.set(x, this.get(x) + 1 || 1));
    }
}

const tests = [
    [[5, 25], 2],
    [[4,6,15,35], 4],
    [[20,50,9,63], 2],
    [[2,3,6,7,4,12,21,39], 8],
    [[1,73,51,84,21,55,92,93,85], 7],
    [[36314,20275,20056,86959,30023,8908,92707,34259,65423,65311,6548,73417,17285,88729,6057,54573,23909,12955,61298,6361,22911,23669,30187,37829,6988,47053,63847,77156,69911,75689,13573,33407,22084,21851,18377,99907,563,12249,467,27329,26001,25583,18703,30869,46051,32359,34641,52329,23531,39521,27699,19361,38281,39807,86998,25743,80296,61546,21050,61091,97423,63092,65138,25231,1097,66763,415,63961,94143,41221,6131,85941,90935,9293,97329,12703], 23],
];
for (const [nums, expected] of tests) {
    console.log(largestComponentSize(nums) == expected);
}
