/**
 * 895. Maximum Frequency Stack (Hard)
 * Design a stack-like data structure to push elements to the stack and pop the
 * most frequent element from the stack.
 *
 * Implement the FreqStack class:
 *     ▪ FreqStack() constructs an empty frequency stack.
 *     ▪ void push(int val) pushes an integer val onto the top of the stack.
 *     ▪ int pop() removes and returns the most frequent element in the stack.
 *         ∙ If there is a tie for the most frequent element, the element
 *           closest to the stack's top is removed and returned.
 *
 * Example 1:
 * Input
 * ["FreqStack", "push", "push", "push", "push", "push", "push", "pop", "pop",
    "pop", "pop"]
 * [[], [5], [7], [5], [7], [4], [5], [], [], [], []]
 * Output
 * [null, null, null, null, null, null, null, 5, 7, 5, 4]
 *
 * Explanation
 * FreqStack freqStack = new FreqStack();
 * freqStack.push(5); // The stack is [5]
 * freqStack.push(7); // The stack is [5,7]
 * freqStack.push(5); // The stack is [5,7,5]
 * freqStack.push(7); // The stack is [5,7,5,7]
 * freqStack.push(4); // The stack is [5,7,5,7,4]
 * freqStack.push(5); // The stack is [5,7,5,7,4,5]
 * freqStack.pop();   // return 5, as 5 is the most frequent.
 *                    // The stack becomes [5,7,5,7,4].
 * freqStack.pop();   // return 7, as 5 and 7 is the most frequent, but 7 is
 *                    // closest to the top. The stack becomes [5,7,5,4].
 * freqStack.pop();   // return 5, as 5 is the most frequent.
 *                    // The stack becomes [5,7,4].
 * freqStack.pop();   // return 4, as 4, 5 and 7 is the most frequent, but 4 is
 *                    // closest to the top. The stack becomes [5,7].
 *
 * Constraints:
 *     ∙ 0 <= val <= 10⁹
 *     ∙ At most 2 * 10⁴ calls will be made to push and pop.
 *     ∙ It is guaranteed that there will be at least one element in the stack
 *       before calling pop.
 */
var FreqStack = function() {
    this.counter = new Map();
    this.stack = [];
};

/**
 * @param {number} val
 * @return {void}
 */
FreqStack.prototype.push = function(val) {
    const count = (this.counter.get(val) || 0) + 1;
    this.counter.set(val, count);
    this.stack[count] ? this.stack[count].push(val) : this.stack[count] = [val];
};

/**
 * @return {number}
 */
FreqStack.prototype.pop = function() {
    const top = this.stack[this.stack.length-1];
    const val = top.pop();
    if (!top.length) {
        this.stack.pop();
    }
    this.counter.set(val, this.counter.get(val) - 1);
    return val;
};

let obj = new FreqStack();
obj.push(5);
obj.push(7);
obj.push(5);
obj.push(7);
obj.push(4);
obj.push(5);
console.log(obj.pop() === 5);
console.log(obj.pop() === 7);
console.log(obj.pop() === 5);
console.log(obj.pop() === 4);

obj = new FreqStack();
obj.push(4);
obj.push(0);
obj.push(9);
obj.push(3);
obj.push(4);
obj.push(2);
console.log(obj.pop() === 4);
obj.push(6);
console.log(obj.pop() === 6);
obj.push(1);
console.log(obj.pop() === 1);
obj.push(1);
console.log(obj.pop() === 1);
obj.push(4);
console.log(obj.pop() === 4);
console.log(obj.pop() === 2);
console.log(obj.pop() === 3);
console.log(obj.pop() === 9);
console.log(obj.pop() === 0);
console.log(obj.pop() === 4);
