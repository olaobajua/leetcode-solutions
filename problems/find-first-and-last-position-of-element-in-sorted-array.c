/**
 * 34. Find First and Last Position of Element in Sorted Array [Medium]
 * Given an array of integers nums sorted in non-decreasing order, find the
 * starting and ending position of a given target value.
 *
 * If target is not found in the array, return [-1, -1].
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 * Example 1:
 * Input: nums = [5,7,7,8,8,10], target = 8
 * Output: [3,4]
 * Example 2:
 * Input: nums = [5,7,7,8,8,10], target = 6
 * Output: [-1,-1]
 * Example 3:
 * Input: nums = [], target = 0
 * Output: [-1,-1]
 *
 * Constraints:
 *     ∙ 0 <= nums.length <= 10⁵
 *     ∙ -10⁹ <= nums[i] <= 10⁹
 *     ∙ nums is a non-decreasing array.
 *     ∙ -10⁹ <= target <= 10⁹
 */
int compare(const int *a, const int *b) {
    return *a - *b;
}

int bisectLeft(int *a, int aSize, int x,
               int (*comp)(const void *, const void *), int lo, int hi) {
    if (!aSize)
        return 0;
    while (lo + 1 < hi) {
        const int mid = (lo + hi) / 2;
        comp(&x, &a[mid]) <= 0 ? (hi = mid) : (lo = mid);
    }
    return a[lo] == x || hi < aSize && a[hi] > x ? lo : hi;
}

int bisectRight(int *a, int aSize, int x,
               int (*comp)(const void *, const void *), int lo, int hi) {
    if (!aSize)
        return 0;
    while (lo + 1 < hi) {
        const int mid = (lo + hi) / 2;
        comp(&x, &a[mid]) >= 0 ? (lo = mid) : (hi = mid);
    }
    return hi < aSize && a[hi] == x ? hi + 1 : a[lo] == x ? lo + 1 : lo;
}

int* searchRange(int *nums, int numsSize, int target, int *returnSize) {
    *returnSize = 2;
    static int ret[2];
    const int lo = bisectLeft(nums, numsSize, target, compare, 0, numsSize);
    const int hi = bisectRight(nums, numsSize, target, compare, 0, numsSize);
    if (lo == hi || lo == numsSize) {
        ret[0] = -1;
        ret[1] = -1;
    } else {
        ret[0] = lo;
        ret[1] = hi - 1;
    }
    return ret;
}
