/**
 * 23. Merge k Sorted Lists (Hard)
 * You are given an array of k linked-lists lists, each linked-list is sorted
 * in ascending order.
 *
 * Merge all the linked-lists into one sorted linked-list and return it.
 *
 * Example 1:
 * Input: lists = [[1,4,5],[1,3,4],[2,6]]
 * Output: [1,1,2,3,4,4,5,6]
 * Explanation: The linked-lists are:
 * [
 *   1->4->5,
 *   1->3->4,
 *   2->6
 * ]
 * merging them into one sorted list:
 * 1->1->2->3->4->4->5->6
 *
 * Example 2:
 * Input: lists = []
 * Output: []
 *
 * Example 3:
 * Input: lists = [[]]
 * Output: []
 *
 * Constraints:
 *     ∙ k == lists.length
 *     ∙ 0 <= k <= 10⁴
 *     ∙ 0 <= lists[i].length <= 500
 *     ∙ -10⁴ <= lists[i][j] <= 10⁴
 *     ∙ lists[i] is sorted in ascending order.
 *     ∙ The sum of lists[i].length won't exceed 10⁴.
 */

// Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode *mergeSortedLists(struct ListNode *a, struct ListNode *b) {
    if (!a) {
        return b;
    }
    if (!b) {
        return a;
    }
    if (a->val <= b->val) {
        a->next = mergeSortedLists(a->next, b);
        return a;
    } else {
        b->next = mergeSortedLists(a, b->next);
        return b;
    }
}

struct ListNode* mergeKLists(struct ListNode** lists, int listsSize) {
    if (listsSize == 0) {
        return NULL;
    }
    if (listsSize == 1) {
        return lists[0];
    }
    --listsSize;
    for (int i = 0; i < listsSize; ++i, --listsSize) {
        lists[i] = mergeSortedLists(lists[i], lists[listsSize]);
    }
    return mergeKLists(lists, listsSize + 1);
}
