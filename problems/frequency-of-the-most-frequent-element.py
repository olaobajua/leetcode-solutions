"""
 * 1838. Frequency of the Most Frequent Element [Medium]
 * The frequency of an element is the number of times it occurs in an array.
 *
 * You are given an integer array nums and an integer k. In one operation, you
 * can choose an index of nums and increment the element at that index by 1.
 *
 * Return the maximum possible frequency of an element after performing at
 * most k operations.
 *
 * Example 1:
 * Input: nums = [1,2,4], k = 5
 * Output: 3
 * Explanation: Increment the first element three times and the second element
 * two times to make nums = [4,4,4].
 * 4 has a frequency of 3.
 *
 * Example 2:
 * Input: nums = [1,4,8,13], k = 5
 * Output: 2
 * Explanation: There are multiple optimal solutions:
 * - Increment the first element three times to make nums = [4,4,8,13]. 4 has
 * a frequency of 2.
 * - Increment the second element four times to make nums = [1,8,8,13]. 8 has
 * a frequency of 2.
 * - Increment the third element five times to make nums = [1,4,13,13]. 13 has
 * a frequency of 2.
 *
 * Example 3:
 * Input: nums = [3,9,6], k = 2
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i] <= 10⁵
 *     ∙ 1 <= k <= 10⁵
"""
from collections import deque
from typing import List

class Solution:
    def maxFrequency(self, nums: List[int], k: int) -> int:
        ret = 0
        d = deque()
        inc = 0
        for x in sorted(nums):
            if d:
                inc += (x - d[-1]) * len(d)
            d.append(x)
            while inc > k:
                inc -= x - d.popleft()
            ret = max(ret, len(d))

        return ret

if __name__ == "__main__":
    tests = (
        ([1,2,4], 5, 3),
        ([1,4,8,13], 5, 2),
        ([3,9,6], 2, 1),
        ([9930,9923,9983,9997,9934,9952,9945,9914,9985,9982,9970,9932,9985,9902,9975,9990,9922,9990,9994,9937,9996,9964,9943,9963,9911,9925,9935,9945,9933,9916,9930,9938,10000,9916,9911,9959,9957,9907,9913,9916,9993,9930,9975,9924,9988,9923,9910,9925,9977,9981,9927,9930,9927,9925,9923,9904,9928,9928,9986,9903,9985,9954,9938,9911,9952,9974,9926,9920,9972,9983,9973,9917,9995,9973,9977,9947,9936,9975,9954,9932,9964,9972,9935,9946,9966], 3056, 73),
    )
    for nums, k, expected in tests:
        print(Solution().maxFrequency(nums, k) == expected)
