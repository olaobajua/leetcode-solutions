"""
 * 85. Maximal Rectangle (Hard)
 * Given a rows x cols binary matrix filled with 0's and 1's, find the largest
 * rectangle containing only 1's and return its area.
 *
 * Example 1:
 * Input: matrix = [["1","0","1","0","0"],
 *                  ["1","0","1","1","1"],
 *                  ["1","1","1","1","1"],
 *                  ["1","0","0","1","0"]]
 * Output: 6
 *
 * Example 2:
 * Input: matrix = []
 * Output: 0
 *
 * Example 3:
 * Input: matrix = [["0"]]
 * Output: 0
 *
 * Example 4:
 * Input: matrix = [["1"]]
 * Output: 1
 *
 * Example 5:
 * Input: matrix = [["0","0"]]
 * Output: 0
 *
 * Constraints:
 *     rows == matrix.length
 *     cols == matrix[i].length
 *     0 <= row, cols <= 200
 *     matrix[i][j] is '0' or '1'.
"""
from itertools import accumulate
from typing import List

class Solution:
    def maximalRectangle(self, matrix: List[List[str]]) -> int:
        matrix = [[*map(int, row)] for row in matrix]
        matrix = [*reversed([*zip(*[[*accumulate(row, lambda a, x: (a + x)*x)]
                                    for row in zip(*reversed(matrix))])])]
        max_rect = 0
        for row in matrix:
            row = row + (0,)
            stack = [-1]
            for i, height in enumerate(row):
                if height >= row[stack[-1]]:
                    stack.append(i)
                while height < row[stack[-1]]:
                    h = row[stack.pop()]
                    w = i - stack[-1] - 1
                    max_rect = max(max_rect, h * w)
                stack.append(i)
        return max_rect

if __name__ == "__main__":
    tests = (
        ([["1","0","1","0","0"],
          ["1","0","1","1","1"],
          ["1","1","1","1","1"],
          ["1","0","0","1","0"]], 6),
        ([], 0),
        ([["0"]], 0),
        ([["1"]], 1),
        ([["0","0"]], 0),
    )
    for matrix, expected in tests:
        print(Solution().maximalRectangle(matrix) == expected)
