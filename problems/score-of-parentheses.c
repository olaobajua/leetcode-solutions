/**
 * 856. Score of Parentheses (Medium)
 * Given a balanced parentheses string s, return the score of the string.
 *
 * The score of a balanced parentheses string is based on the following rule:
 *     ∙ "()" has score 1.
 *     ∙ AB has score A + B, where A and B are balanced parentheses strings.
 *     ∙ (A) has score 2 * A, where A is a balanced parentheses string.
 *
 * Example 1:
 * Input: s = "()"
 * Output: 1
 *
 * Example 2:
 * Input: s = "(())"
 * Output: 2
 *
 * Example 3:
 * Input: s = "()()"
 * Output: 2
 *
 * Constraints:
 *     ∙ 2 <= s.length <= 50
 *     ∙ s consists of only '(' and ')'.
 *     ∙ s is a balanced parentheses string.
 */
int helper(char *s, int start, int end) {
    int ret = 0, level = 0;
    for (int i = start; i <= end; ++i) {
        level += s[i] == '(' ? -1 : 1;
        if (level == 0) {
            if (i - start == 1) {
                ++ret;
            } else {
                ret += 2*helper(s, start + 1, i - 1);
            }
            start = i + 1;
        }
    }
    return ret;
}
int scoreOfParentheses(char *s) {
    return helper(s, 0, strlen(s) - 1);
}
