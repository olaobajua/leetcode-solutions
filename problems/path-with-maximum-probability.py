"""
 * 1514. Path with Maximum Probability [Medium]
 * You are given an undirected weighted graph of n nodes (0-indexed),
 * represented by an edge list where edges[i] = [a, b] is an undirected edge
 * connecting the nodes a and b with a probability of success of traversing
 * that edge succProb[i].
 *
 * Given two nodes start and end, find the path with the maximum probability
 * of success to go from start to end and return its success probability.
 *
 * If there is no path from start to end, return 0. Your answer will be
 * accepted if it differs from the correct answer by at most 1e-5.
 *
 * Example 1:
 * Input: n = 3, edges = [[0,1],[1,2],[0,2]], succProb = [0.5,0.5,0.2], start
 * = 0, end = 2
 * Output: 0.25000
 * Explanation: There are two paths from start to end, one having a
 * probability of success = 0.2 and the other has 0.5 * 0.5 = 0.25.
 *
 * Example 2:
 * Input: n = 3, edges = [[0,1],[1,2],[0,2]], succProb = [0.5,0.5,0.3], start
 * = 0, end = 2
 * Output: 0.30000
 *
 * Example 3:
 * Input: n = 3, edges = [[0,1]], succProb = [0.5], start = 0, end = 2
 * Output: 0.00000
 * Explanation: There is no path between 0 and 2.
 *
 * Constraints:
 *     ∙ 2 <= n <= 10⁴
 *     ∙ 0 <= start, end < n
 *     ∙ start != end
 *     ∙ 0 <= a, b < n
 *     ∙ a != b
 *     ∙ 0 <= succProb.length == edges.length <= 2*10⁴
 *     ∙ 0 <= succProb[i] <= 1
 *     ∙ There is at most one edge between every two nodes.
"""
from collections import defaultdict
from heapq import heappop, heappush
from math import inf
from typing import List

class Solution:
    def maxProbability(self, n: int, edges: List[List[int]], succProb: List[float], start: int, end: int) -> float:
        graph = defaultdict(list)
        distances = {}
        for (a, b), d in zip(edges, succProb):
            graph[a].append(b)
            graph[b].append(a)
            distances[(a, b)] = d
            distances[(b, a)] = d
        ret = dijkstra(graph, distances, start).get(end, 0)
        return -ret if ret < inf else 0

def dijkstra(graph: dict, distances: dict, source) -> dict:
    max_prob_paths = dict.fromkeys(graph, inf)
    max_prob_paths[source] = 0
    to_visit = [(-1, source)]
    while to_visit:
        parent_prob, root = heappop(to_visit)
        for neib in graph[root]:
            cur_prob = distances[(root, neib)]
            if parent_prob * cur_prob < max_prob_paths[neib]:
                max_prob_paths[neib] = parent_prob * cur_prob
                heappush(to_visit, (parent_prob * cur_prob, neib))
    return max_prob_paths

if __name__ == "__main__":
    tests = (
        (3, [[0,1],[1,2],[0,2]], [0.5,0.5,0.2], 0, 2, 0.25000),
        (3, [[0,1],[1,2],[0,2]], [0.5,0.5,0.3], 0, 2, 0.30000),
        (3, [[0,1]], [0.5], 0, 2, 0.00000),
        (10, [[0,3],[1,7],[1,2],[0,9]], [0.31,0.9,0.86,0.36], 2, 3, 0)
    )
    for n, edges, succProb, start, end, expected in tests:
        print(Solution().maxProbability(n, edges, succProb, start, end) == expected)
