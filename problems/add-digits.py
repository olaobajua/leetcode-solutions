"""
 * 258. Add Digits (Easy)
 * Given an integer num, repeatedly add all its digits until the result has
 * only one digit, and return it.
 *
 * Example 1:
 * Input: num = 38
 * Output: 2
 * Explanation: The process is
 * 38 --> 3 + 8 --> 11
 * 11 --> 1 + 1 --> 2
 * Since 2 has only one digit, return it.
 *
 * Example 2:
 * Input: num = 0
 * Output: 0
 *
 * Constraints:
 *     ∙ 0 <= num <= 2³¹ - 1
 *
 * Follow up: Could you do it without any loop/recursion in O(1) runtime?
"""
from typing import List

class Solution:
    def addDigits(self, num: int) -> int:
        # https://en.wikipedia.org/wiki/Digital_root#Congruence_formula
        return num if num == 0 else 1 + ((num - 1) % 9)

if __name__ == "__main__":
    tests = (
        (38, 2),
        (0, 0),
    )
    for num, expected in tests:
        print(Solution().addDigits(num) == expected)
