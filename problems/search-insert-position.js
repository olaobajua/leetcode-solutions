/**
 * 35. Search Insert Position (Easy)
 *
 * Given a sorted array of distinct integers and a target value, return the
 * index if the target is found. If not, return the index where it would be if
 * it were inserted in order.
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 * Example 1:
 * Input: nums = [1,3,5,6], target = 5
 * Output: 2
 *
 * Example 2:
 * Input: nums = [1,3,5,6], target = 2
 * Output: 1
 *
 * Example 3:
 * Input: nums = [1,3,5,6], target = 7
 * Output: 4
 *
 * Example 4:
 * Input: nums = [1,3,5,6], target = 0
 * Output: 0
 *
 * Example 5:
 * Input: nums = [1], target = 0
 * Output: 0
 *
 * Constraints:
 *     1 <= nums.length <= 10⁴
 *     -10⁴ <= nums[i] <= 10⁴
 *     nums contains distinct values sorted in ascending order.
 *     -10⁴ <= target <= 10⁴
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var searchInsert = function(nums, target) {
    return bisectLeft(nums, target);
};

function bisectLeft(a, x, lo=0, hi=a.length-1) {
    while (lo <= hi) {
        const mid = Math.floor((hi + lo) / 2);
        if (x == a[mid]) {
            return mid;
        } else if (x > a[mid]) {
            lo = mid + 1;
        } else {
            hi = mid - 1;
        }
    }
    return lo;
}

const tests = [
    [[1,3,5,6], 5, 2],
    [[1,3,5,6], 2, 1],
    [[1,3,5,6], 7, 4],
    [[1,3,5,6], 0, 0],
    [[1], 0, 0],
];
for (const [nums, target, expected] of tests) {
    console.log(searchInsert(nums, target) == expected);
}
