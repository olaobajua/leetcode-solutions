"""
 * 409. Longest Palindrome [Easy]
 * Given a string s which consists of lowercase or uppercase letters, return
 * the length of the longest palindrome that can be built with those letters.
 *
 * Letters are case sensitive, for example, "Aa" is not considered a
 * palindrome here.
 *
 * Example 1:
 * Input: s = "abccccdd"
 * Output: 7
 * Explanation: One longest palindrome that can be built is "dccaccd", whose
 * length is 7.
 *
 * Example 2:
 * Input: s = "a"
 * Output: 1
 * Explanation: The longest palindrome that can be built is "a", whose length
 * is 1.
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 2000
 *     ∙ s consists of lowercase and/or uppercase English letters only.
"""
from collections import Counter

class Solution:
    def longestPalindrome(self, s: str) -> int:
        return min(len(s), 2 * sum(c // 2 for c in Counter(s).values()) + 1)

if __name__ == "__main__":
    tests = (
        ("abccccdd", 7),
        ("a", 1),
        ("bb", 2),
    )
    for s, expected in tests:
        print(Solution().longestPalindrome(s) == expected)
