/**
 * 116. Populating Next Right Pointers in Each Node (Medium)
 * You are given a perfect binary tree where all leaves are on the same level,
 * and every parent has two children. The binary tree has the following
 * definition:
 *     struct Node {
 *       int val;
 *       Node *left;
 *       Node *right;
 *       Node *next;
 *     }
 *
 * Populate each next pointer to point to its next right node. If there is no
 * next right node, the next pointer should be set to NULL.
 *
 * Initially, all next pointers are set to NULL.
 *
 * Example 1:
 * Input: root = [1,2,3,4,5,6,7]
 * Output: [1,#,2,3,#,4,5,6,7,#]
 * Explanation: Given the above perfect binary tree (Figure A), your function
 * should populate each next pointer to point to its next right node, just like
 * in Figure B. The serialized output is in level order as connected by the
 * next pointers, with '#' signifying the end of each level.
 *
 * Example 2:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 2¹² - 1].
 *     ∙ -1000 <= Node.val <= 1000
 *
 * Follow-up:
 *     ∙ You may only use constant extra space.
 *     ∙ The recursive approach is fine. You may assume implicit stack space
 *       does not count as extra space for this problem.
 */

// Definition for a Node.
function Node(val, left, right, next) {
   this.val = val === undefined ? null : val;
   this.left = left === undefined ? null : left;
   this.right = right === undefined ? null : right;
   this.next = next === undefined ? null : next;
};

/**
 * @param {Node} root
 * @return {Node}
 */
var connect = function(root) {
    let cur = root;
    while (cur && cur.left) {
        const nxt = cur.left;
        while (cur) {
            cur.left.next = cur.right;
            cur.right.next = cur.next && cur.next.left;
            cur = cur.next;
        }
        cur = nxt;
    }
    return root;
};

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new Node(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new Node(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new Node(v);
            nodes.push(n.right);
        }
    }
}

function treeToList(root) {
    const ret = [];
    while (root) {
        const nxt = root.left;
        while (root) {
            ret.push(root.val)
            root = root.next;
        }
        ret.push('#');
        root = nxt;
    }
    return ret;
}

const tests = [
    [[1,2,3,4,5,6,7], [1,'#',2,3,'#',4,5,6,7,'#']],
    [[], []],
    [[0], [0, '#']],
];

for (const [nums, expected] of tests) {
    console.log(treeToList(connect(buildTree(nums))).toString() ==
                expected.toString());
}
