/**
 * 61. Rotate List (Medium)
 * Given the head of a linked list, rotate the list to the right by k places.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [4,5,1,2,3]
 *
 * Example 2:
 * Input: head = [0,1,2], k = 4
 * Output: [2,0,1]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 500].
 *     ∙ -100 <= Node.val <= 100
 *     ∙ 0 <= k <= 2 * 109
 */

// Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* rotateRight(struct ListNode *head, int k) {
    if (!head) {
        return head;
    }
    struct ListNode *end = head;
    int n = 1;
    while (end && end->next) {
        end = end->next;
        ++n;
    }
    if ((k %= n) == 0) {
        return head;
    }
    struct ListNode *mid = head;
    for (int i = 0; i < n - k - 1; ++i) {
        mid = mid->next;
    }
    end->next = head;
    head = mid->next;
    mid->next = NULL;
    return head;
}
