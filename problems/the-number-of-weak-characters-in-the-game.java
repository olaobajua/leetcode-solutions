/**
 * 1996. The Number of Weak Characters in the Game [Medium]
 * You are playing a game that contains multiple characters, and each of the
 * characters has two main properties: attack and defense. You are given a 2D
 * integer array properties where properties[i] = [attackᵢ, defenseᵢ]
 * represents the properties of the iᵗʰ character in the game.
 *
 * A character is said to be weak if any other character has both attack and
 * defense levels strictly greater than this character's attack and defense
 * levels. More formally, a character i is said to be weak if there exists
 * another character j where attackⱼ > attackᵢ and defenseⱼ > defenseᵢ.
 *
 * Return the number of weak characters.
 *
 * Example 1:
 * Input: properties = [[5,5],[6,3],[3,6]]
 * Output: 0
 * Explanation: No character has strictly greater attack and defense than the
 * other.
 *
 * Example 2:
 * Input: properties = [[2,2],[3,3]]
 * Output: 1
 * Explanation: The first character is weak because the second character has a
 * strictly greater attack and defense.
 *
 * Example 3:
 * Input: properties = [[1,5],[10,4],[4,3]]
 * Output: 1
 * Explanation: The third character is weak because the second character has a
 * strictly greater attack and defense.
 *
 * Constraints:
 *     ∙ 2 <= properties.length <= 10⁵
 *     ∙ properties[i].length == 2
 *     ∙ 1 <= attackᵢ, defenseᵢ <= 10⁵
 */
class Solution {
    public int numberOfWeakCharacters(int[][] properties) {
        int pa = 0, pd = 0, md = 0;
        int ret = 0;
        Arrays.sort(properties, (a, b) -> Integer.compare(a[0], b[0]) != 0
                                                ? Integer.compare(b[0], a[0])
                                                : Integer.compare(b[1], a[1]));
        for (int[] x : properties) {
            if (x[0] < pa)
                pd = md;
            if (x[1] < pd)
                ++ret;
            md = Math.max(x[1], md);
            pa = x[0];
        }
        return ret;
    }
}
