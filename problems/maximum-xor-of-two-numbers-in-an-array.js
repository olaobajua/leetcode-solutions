/**
 * 421. Maximum XOR of Two Numbers in an Array (Medium)
 * Given an integer array nums, return the maximum result of
 * nums[i] XOR nums[j], where 0 <= i <= j < n.
 *
 * Example 1:
 * Input: nums = [3,10,5,25,2,8]
 * Output: 28
 * Explanation: The maximum result is 5 XOR 25 = 28.
 *
 * Example 2:
 * Input: nums = [14,70,53,83,49,91,36,80,92,51,66,70]
 * Output: 127
 *
 * Constraints:
 *     1 <= nums.length <= 2 * 10⁵
 *     0 <= nums[i] <= 2³¹ - 1
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaximumXOR = function(nums) {
    let ret = 0, mask = 0;
    for (let i = 31; i >= 0; --i) {
        mask |= 1 << i;
        const prefixes = new Set(nums.map(x => x & mask));
        const start = ret | 1 << i;
        if (Array.from(prefixes).some(pref => prefixes.has(start ^ pref))) {
            ret = start;
        }
    }
    return ret;
};

const tests = [
    [[0], 0],
    [[3,10,5,25,2,8], 28],  // 25 ^ 5 = 11001 ^ 00101 = 11100
    [[14,70,53,83,49,91,36,80,92,51,66,70], 127],  // 36 ^ 91 = 0100100 ^ 1011011 = 1111111
    [[14,15,9,3,2], 13],  // 2 ^ 15 = 0010 ^ 1111 = 1101
    [[15,15,9,3,2], 13],  // 2 ^ 15 = 0010 ^ 1111 = 1101
];

for (const [nums, expected] of tests) {
    console.log(findMaximumXOR(nums) == expected);
}
