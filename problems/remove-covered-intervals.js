/**
 * 1288. Remove Covered Intervals (Medium)
 * Given an array intervals where intervals[i] = [lᵢ, rᵢ] represent the
 * interval [lᵢ, rᵢ), remove all intervals that are covered by another interval
 * in the list.
 *
 * The interval [a, b) is covered by the interval [c, d) if and only if
 * c <= a and b <= d.
 *
 * Return the number of remaining intervals.
 *
 * Example 1:
 * Input: intervals = [[1,4],[3,6],[2,8]]
 * Output: 2
 * Explanation: Interval [3,6] is covered by [2,8], therefore it is removed.
 *
 * Example 2:
 * Input: intervals = [[1,4],[2,3]]
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= intervals.length <= 1000
 *     ∙ intervals[i].length == 2
 *     ∙ 0 <= lᵢ <= rᵢ <= 10⁵
 *     ∙ All the given intervals are unique.
 */

/**
 * @param {number[][]} intervals
 * @return {number}
 */
var removeCoveredIntervals = function(intervals) {
    intervals.sort((a, b) => a[1] - b[1] || b[0] - a[0]);
    let i = 0;
    while (i < intervals.length - 1) {
        const [a, b] = intervals[i];
        const [c, d] = intervals[i+1];
        if (c <= a && b <= d) {
            intervals.splice(i, 1);
            i = Math.max(i - 1, 0);
        } else {
            ++i;
        }
    }
    return intervals.length;
};

const tests = [
    [[[1,4],[3,6],[2,8]], 2],
    [[[1,4],[2,3]], 1],
    [[[1,2],[1,4],[3,4]], 1],
    [[[1,4],[4,7],[3,6]], 3],
];

for (const [intervals, expected] of tests) {
    console.log(removeCoveredIntervals(intervals) == expected);
}
