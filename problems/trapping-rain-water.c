/*
 * 42. Trapping Rain Water (Hard)
 * Given n non-negative integers representing an elevation map where the width of each bar is 1,
 * compute how much water it can trap after raining.
 *
 * Example 1:
 * Input: height = [0,1,0,2,1,0,1,3,2,1,2,1]
 * Output: 6
 * Explanation: The above elevation map (black section) is represented by array [0,1,0,2,1,0,1,3,2,1,2,1].
 * In this case, 6 units of rain water (blue section) are being trapped.
 *
 * Example 2:
 * Input: height = [4,2,0,3,2,5]
 * Output: 9
 *
 * Constraints:
 *     n == height.length
 *     0 <= n <= 2 * 10⁴
 *     0 <= height[i] <= 10⁵
*/
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))

int trap(int* height, int heightSize){
    int *wheight = (int*)malloc(heightSize * sizeof(int));
    for (int i = 0, mx = 0; i < heightSize; ++i) {
        mx = max(mx, height[i]);
        wheight[i] = mx;
    }
    for (int i = heightSize - 1, mx = 0; i >= 0; --i) {
        mx = max(mx, height[i]);
        wheight[i] = min(wheight[i], mx);
    }
    int sum = 0;
    for (int i = 0; i < heightSize; ++i) {
        sum += wheight[i] - height[i];
    }
    free(wheight);
    return sum;
}
