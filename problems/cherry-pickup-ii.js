/**
 * 1463. Cherry Pickup II (Hard)
 * You are given a rows x cols matrix grid representing a field of cherries
 * where grid[i][j] represents the number of cherries that you can collect from
 * the (i, j) cell.
 *
 * You have two robots that can collect cherries for you:
 *     ∙ Robot #1 is located at the top-left corner (0, 0), and
 *     ∙ Robot #2 is located at the top-right corner (0, cols - 1).
 *
 * Return the maximum number of cherries collection using both robots by
 * following the rules below:
 *     ∙ From a cell (i, j), robots can move to cell (i + 1, j - 1),
 *       (i + 1, j), or (i + 1, j + 1).
 *     ∙ When any robot passes through a cell, It picks up all cherries, and
 *       the cell becomes an empty cell.
 *     ∙ When both robots stay in the same cell, only one takes the cherries.
 *     ∙ Both robots cannot move outside of the grid at any moment.
 *     ∙ Both robots should reach the bottom row in grid.
 *
 * Example 1:
 * Input: grid = [[3,1,1],
 *                [2,5,1],
 *                [1,5,5],
 *                [2,1,1]]
 * Output: 24
 * Explanation:
 * respectively.
 * Cherries taken by Robot #1, (3 + 2 + 5 + 2) = 12.
 * Cherries taken by Robot #2, (1 + 5 + 5 + 1) = 12.
 * Total of cherries: 12 + 12 = 24.
 *
 * Example 2:
 * Input: grid = [[1,0,0,0,0,0,1],
 *                [2,0,0,0,0,3,0],
 *                [2,0,9,0,0,0,0],
 *                [0,3,0,5,4,0,0],
 *                [1,0,2,3,0,0,6]]
 * Output: 28
 * Explanation:
 * respectively.
 * Cherries taken by Robot #1, (1 + 9 + 5 + 2) = 17.
 * Cherries taken by Robot #2, (1 + 3 + 4 + 3) = 11.
 * Total of cherries: 17 + 11 = 28.
 *
 * Constraints:
 *     ∙ rows == grid.length
 *     ∙ cols == grid[i].length
 *     ∙ 2 <= rows, cols <= 70
 *     ∙ 0 <= grid[i][j] <= 100
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
var cherryPickup = function(grid) {
    const R = grid.length;
    const C = grid[0].length;

    const dp = [...Array(R)].map(_ =>
               [...Array(C + 2)].map(_ =>
               Array(C + 2).fill(Number.MIN_SAFE_INTEGER)));
    dp[0][1][C] = grid[0][0] + grid[0][C-1];
    for (let r = 1; r < R; ++r) {
        for (let c1 = 1; c1 <= C; ++c1) {
            for (let c2 = 1; c2 <= C; ++c2) {
                let prev = Number.MIN_SAFE_INTEGER;
                for (const d1 of [-1, 0, 1]) {
                    for (const d2 of [-1, 0, 1]) {
                        prev = Math.max(prev, dp[r-1][c1+d1][c2+d2]);
                        dp[r][c1][c2] = ((grid[r][c1-1] + grid[r][c2-1]) /
                                         (1 + (c1 == c2)) + prev);
                    }
                }
            }
        }
    }
    return Math.max(...dp[R-1].flat());
};

const tests = [
    [[[3,1,1],
      [2,5,1],
      [1,5,5],
      [2,1,1]], 24],
    [[[1,0,0,0,0,0,1],
      [2,0,0,0,0,3,0],
      [2,0,9,0,0,0,0],
      [0,3,0,5,4,0,0],
      [1,0,2,3,0,0,6]], 28],
    [[[0,8,7,10,9,10,0,9,6],
      [8,7,10,8,7,4,9,6,10],
      [8,1,1,5,1,5,5,1,2],
      [9,4,10,8,8,1,9,5,0],
      [4,3,6,10,9,2,4,8,10],
      [7,3,2,8,3,3,5,9,8],
      [1,2,6,5,6,2,0,10,0]], 96],
];
for (const [grid, expected] of tests) {
    console.log(cherryPickup(grid) == expected);
}
