/**
 * 763. Partition Labels (Medium)
 * You are given a string s. We want to partition the string into as many parts
 * as possible so that each letter appears in at most one part.
 *
 * Note that the partition is done so that after concatenating all the parts in
 * order, the resultant string should be s.
 *
 * Return a list of integers representing the size of these parts.
 *
 * Example 1:
 * Input: s = "ababcbacadefegdehijhklij"
 * Output: [9,7,8]
 * Explanation:
 * The partition is "ababcbaca", "defegde", "hijhklij".
 * This is a partition so that each letter appears in at most one part.
 * A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it
 * splits s into less parts.
 *
 * Example 2:
 * Input: s = "eccbbbbdec"
 * Output: [10]
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 500
 *     ∙ s consists of lowercase English letters.
 */

/**
 * @param {string} s
 * @return {number[]}
 */
var partitionLabels = function(s) {
    const last = {};
    s = Array.from(s);
    s.forEach((char, i) => { last[char] = i })
    let start = -1;
    let end = last[s[0]];
    let ret = [];
    s.forEach((char, i) => {
        end = Math.max(end, last[char]);
        if (end === i) {
            ret.push(end - start);
            start = i;
        }
    });
    return ret;
};

const tests = [
    ["ababcbacadefegdehijhklij", [9,7,8]],
    ["eccbbbbdec", [10]],
    ["eaaaabaaec", [9,1]],
];

for (const [s, expected] of tests) {
    console.log(partitionLabels(s).toString() == expected.toString());
}
