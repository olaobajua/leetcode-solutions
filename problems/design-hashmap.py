"""
 * 706. Design HashMap [Easy]
 * Design a HashMap without using any built-in hash table libraries.
 *
 * Implement the MyHashMap class:
 *     ∙ MyHashMap() initializes the object with an empty map.
 *     ∙ void put(int key, int value) inserts a (key, value) pair into the
 *       HashMap. If the key already exists in the map, update the
 *       corresponding value.
 *     ∙ int get(int key) returns the value to which the specified key is
 *       mapped, or -1 if this map contains no mapping for the key.
 *     ∙ void remove(key) removes the key and its corresponding value if the
 *       map contains the mapping for the key.
 *
 * Example 1:
 * Input
 * ["MyHashMap", "put", "put", "get", "get", "put", "get", "remove", "get"]
 * [[], [1, 1], [2, 2], [1], [3], [2, 1], [2], [2], [2]]
 * Output
 * [null, null, null, 1, -1, null, 1, null, -1]
 *
 * Explanation
 * MyHashMap myHashMap = new MyHashMap();
 * myHashMap.put(1, 1); // The map is now [[1,1]]
 * myHashMap.put(2, 2); // The map is now [[1,1], [2,2]]
 * myHashMap.get(1);    // return 1, The map is now [[1,1], [2,2]]
 * myHashMap.get(3);    // return -1 (i.e., not found), The map is now [[1,1],
 * [2,2]]
 * myHashMap.put(2, 1); // The map is now [[1,1], [2,1]] (i.e., update the
 * existing value)
 * myHashMap.get(2);    // return 1, The map is now [[1,1], [2,1]]
 * myHashMap.remove(2); // remove the mapping for 2, The map is now [[1,1]]
 * myHashMap.get(2);    // return -1 (i.e., not found), The map is now [[1,1]]
 *
 * Constraints:
 *     ∙ 0 <= key, value <= 10⁶
 *     ∙ At most 10⁴ calls will be made to put, get, and remove.
"""
class MyHashMap:
    def __init__(self):
        self.__hashmap = [[] for _ in range(1 << 15)]

    def put(self, key: int, value: int) -> None:
        elements = self.__hashmap[self.eval_hash(key)]
        for i, (k, v) in enumerate(elements):
            if k == key:
                elements[i] = (key, value)
                break
        else:
            elements.append((key, value))

    def get(self, key: int) -> int:
        elements = self.__hashmap[self.eval_hash(key)]
        for k, v in elements:
            if k == key:
                return v
        return -1

    def remove(self, key: int) -> None:
        elements = self.__hashmap[self.eval_hash(key)]
        for i, (k, v) in enumerate(elements):
            if k == key:
                elements.pop(i)
                break

    def eval_hash(self, key):
        # https://en.wikipedia.org/wiki/Hash_function#Multiplicative_hashing
        # hₐ(K) = (a * K mod 2ʷ) / 2ʷ⁻ᵐ
        # K is our number (key), we want to hash.
        # a is some big odd number (sometimes good idea to use prime number)
        # m is length in bits of output we wan to have
        # w is size of machine word. Here we we can choose any w > m
        # Let's take K = key, a = 39916801, m = 15, w = 20

        return ((key*39916801) & (1 << 20) - 1) >> 5


# Your MyHashMap object will be instantiated and called as such:
obj = MyHashMap()
obj.put(1, 1)
obj.put(2, 2)
print(1 == obj.get(1))
print(-1 == obj.get(3))
obj.put(2, 1)
print(1 == obj.get(2))
obj.remove(2)
print(-1 == obj.get(2))
