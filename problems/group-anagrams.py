"""
 * 49. Group Anagrams [Medium]
 * Given an array of strings strs, group the anagrams together. You can return
 * the answer in any order.
 *
 * An Anagram is a word or phrase formed by rearranging the letters of a
 * different word or phrase, typically using all the original letters exactly
 * once.
 *
 * Example 1:
 * Input: strs = ["eat","tea","tan","ate","nat","bat"]
 * Output: [["bat"],["nat","tan"],["ate","eat","tea"]]
 * Example 2:
 * Input: strs = [""]
 * Output: [[""]]
 * Example 3:
 * Input: strs = ["a"]
 * Output: [["a"]]
 *
 * Constraints:
 *     ∙ 1 <= strs.length <= 10⁴
 *     ∙ 0 <= strs[i].length <= 100
 *     ∙ strs[i] consists of lowercase English letters.
"""
from collections import defaultdict
from typing import List

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        anagrams = defaultdict(list)
        for word in strs:
            anagrams["".join(sorted(word))].append(word)
        return anagrams.values()

if __name__ == "__main__":
    tests = (
        (["eat","tea","tan","ate","nat","bat"], [["bat"],["nat","tan"],["ate","eat","tea"]]),
        ([""], [[""]]),
        (["a"], [["a"]]),
        (["ddddddddddg","dgggggggggg"], [["dgggggggggg"],["ddddddddddg"]]),
    )
    for strs, expected in tests:
        print(set(map(frozenset, Solution().groupAnagrams(strs))) ==
              set(map(frozenset, expected)))
