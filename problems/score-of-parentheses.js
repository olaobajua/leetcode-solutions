/**
 * 856. Score of Parentheses (Medium)
 * Given a balanced parentheses string s, return the score of the string.
 *
 * The score of a balanced parentheses string is based on the following rule:
 *     ∙ "()" has score 1.
 *     ∙ AB has score A + B, where A and B are balanced parentheses strings.
 *     ∙ (A) has score 2 * A, where A is a balanced parentheses string.
 *
 * Example 1:
 * Input: s = "()"
 * Output: 1
 *
 * Example 2:
 * Input: s = "(())"
 * Output: 2
 *
 * Example 3:
 * Input: s = "()()"
 * Output: 2
 *
 * Constraints:
 *     ∙ 2 <= s.length <= 50
 *     ∙ s consists of only '(' and ')'.
 *     ∙ s is a balanced parentheses string.
 */

/**
 * @param {string} s
 * @return {number}
 */
var scoreOfParentheses = function(s) {
    let ret = 0, level = 0;
    Array.from(s).forEach((char, i) => {
        if (char == '(') {
            ++level;
        } else {
            --level;
            if (s[i-1] == '(') {
                ret += 1 << level;
            }
        }
    });
    return ret;
};

const tests = [
    ["()", 1],
    ["(())", 2],
    ["()()", 2],
    ["(()())", 4],
];

for (const [s, expected] of tests) {
    console.log(scoreOfParentheses(s) == expected);
}
