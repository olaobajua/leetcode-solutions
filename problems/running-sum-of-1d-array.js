/**
 * 1480. Running Sum of 1d Array (Easy)
 * Given an array nums. We define a running sum of an array as
 * runningSum[i] = sum(nums[0]…nums[i]).
 *
 * Return the running sum of nums.
 *
 * Example 1:
 * Input: nums = [1,2,3,4]
 * Output: [1,3,6,10]
 * Explanation: Running sum is obtained as follows: [1, 1+2, 1+2+3, 1+2+3+4].
 *
 * Example 2:
 * Input: nums = [1,1,1,1,1]
 * Output: [1,2,3,4,5]
 * Explanation: Running sum is obtained as follows:
 * [1, 1+1, 1+1+1, 1+1+1+1, 1+1+1+1+1].
 *
 * Example 3:
 * Input: nums = [3,1,2,10,1]
 * Output: [3,4,6,16,17]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 1000
 *     ∙ -10⁶ <= nums[i] <= 10⁶
 */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var runningSum = function(nums) {
    return nums.reduce((acc, x, i) => [...acc, acc[i] + x], [0]).slice(1);
};

const tests = [
    [[1,2,3,4], [1,3,6,10]],
    [[1,1,1,1,1], [1,2,3,4,5]],
    [[3,1,2,10,1], [3,4,6,16,17]],
];

for (const [nums, expected] of tests) {
    console.log(runningSum(nums).every((x, i) => x == expected[i]));
}
