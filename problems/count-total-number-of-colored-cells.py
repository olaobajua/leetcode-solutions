"""
 * 2579. Count Total Number of Colored Cells [Medium]
 * There exists an infinitely large two-dimensional grid of uncolored unit
 * cells. You are given a positive integer n, indicating that you must do the
 * following routine for n minutes:
 *     ∙ At the first minute, color any arbitrary unit cell blue.
 *     ∙ Every minute thereafter, color blue every uncolored cell that touches
 *       a blue cell.
 *
 * Below is a pictorial representation of the state of the grid after minutes
 * 1, 2, and 3.
 *
 * Return the number of colored cells at the end of n minutes.
 *
 * Example 1:
 * Input: n = 1
 * Output: 1
 * Explanation: After 1 minute, there is only 1 blue cell, so we return 1.
 *
 * Example 2:
 * Input: n = 2
 * Output: 5
 * Explanation: After 2 minutes, there are 4 colored cells on the boundary and
 * 1 in the center, so we return 5.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁵
"""
from functools import cache

class Solution:
    @cache
    def coloredCells(self, n: int) -> int:
        if n == 1:
            return 1
        return (n - 1) * 4 + self.coloredCells(n - 1)

if __name__ == "__main__":
    tests = (
        (1, 1),
        (2, 5),     # 4
        (3, 13),    # 8
        (4, 25),    # 12
        (5, 41),    # 16
        (6, 61),    # 20
        (7, 85),    # 24
        (8, 113),   # 28
        (9, 145),   # 32
        (10, 181),
        (15, 421),
        (20, 761),
        (100000, 0),
    )
    import sys
    sys.setrecursionlimit(100000)
    for n, expected in tests:
        print(Solution().coloredCells(n) == expected)
