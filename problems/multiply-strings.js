/**
 * 43. Multiply Strings (Medium)
 * Given two non-negative integers num1 and num2 represented as strings, return
 * the product of num1 and num2, also represented as a string.
 *
 * Note: You must not use any built-in BigInteger library or convert the inputs
 * to integer directly.
 *
 * Example 1:
 * Input: num1 = "2", num2 = "3"
 * Output: "6"
 *
 * Example 2:
 * Input: num1 = "123", num2 = "456"
 * Output: "56088"
 *
 * Constraints:
 *     ∙ 1 <= num1.length, num2.length <= 200
 *     ∙ num1 and num2 consist of digits only.
 *     ∙ Both num1 and num2 do not contain any leading zero, except the number
 *       0 itself.
 */

/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */
var multiply = function(num1, num2) {
    const ret = Array(num1.length  + num2.length).fill(0);
    num1 = Array.from(num1).map(n => parseInt(n));
    num2 = Array.from(num2).map(n => parseInt(n));
    for (let i = num1.length - 1; i >= 0; --i){
        for (let j = num2.length - 1; j >= 0; --j){
            const pos = i + j + 1;
            const product = num1[i] * num2[j] + ret[pos];
            ret[pos] = product % 10;
            ret[pos-1] += Math.floor(product / 10);
        };
    };
    while (ret[0] === 0) {
        ret.shift()
    }
    return ret.length ? ret.join('') : '0';
};

const tests = [
    ["2", "3", "6"],
    ["123", "456", "56088"],
    ["3", "456", "1368"],
    ["0", "0", "0"],
];

for (let [num1, num2, expected] of tests) {
    console.log(multiply(num1, num2) == expected);
}
