/**
 * 739. Daily Temperatures (Medium)
 * Given an array of integers temperatures represents the daily temperatures,
 * return an array answer such that answer[i] is the number of days you have to
 * wait after the ith day to get a warmer temperature. If there is no future
 * day for which this is possible, keep answer[i] == 0 instead.
 *
 * Example 1:
 * Input: temperatures = [73,74,75,71,69,72,76,73]
 * Output: [1,1,4,2,1,1,0,0]
 *
 * Example 2:
 * Input: temperatures = [30,40,50,60]
 * Output: [1,1,1,0]
 *
 * Example 3:
 * Input: temperatures = [30,60,90]
 * Output: [1,1,0]
 *
 * Constraints:
 *     1 <= temperatures.length <= 10⁵
 *     30 <= temperatures[i] <= 100
 */

/**
 * @param {number[]} temperatures
 * @return {number[]}
 */
var dailyTemperatures = function(temperatures) {
    let hottest = 0;
    const ret = Array(temperatures.length).fill(0);
    Array.from(temperatures.entries()).reverse().forEach(([curDay, temp]) => {
        if (temp >= hottest) {
            hottest = temp;
        } else {
            let days = 1;
            while (temperatures[curDay + days] <= temp) {
                days += ret[curDay + days];
            }
            ret[curDay] = days;
        }
    });
    return ret;
};

const tests = [
    [[73,74,75,71,69,72,76,73], [1,1,4,2,1,1,0,0]],
    [[30,40,50,60], [1,1,1,0]],
    [[30,60,90], [1,1,0]],
    [[73,71,72,71,74], [4,1,2,1,0]],
];

for (let [temperatures, expected] of tests) {
    console.log(dailyTemperatures(temperatures)
                .every((temp, i) => temp == expected[i]));
}
