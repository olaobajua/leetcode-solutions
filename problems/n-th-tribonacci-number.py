"""
 * 1137. N-th Tribonacci Number [Easy]
 * The Tribonacci sequence Tₙ is defined as follows:
 *
 * T₀ = 0, T₁ = 1, T₂ = 1, and Tₙ₊₃ = Tₙ + Tₙ₊₁ + Tₙ₊₂ for n >= 0.
 *
 * Given n, return the value of Tₙ.
 *
 *
 * Example 1:
 * Input: n = 4
 * Output: 4
 * Explanation:
 * T_3 = 0 + 1 + 1 = 2
 * T_4 = 1 + 1 + 2 = 4
 *
 * Example 2:
 * Input: n = 25
 * Output: 1389537
 *
 * Constraints:
 *     ∙ 0 <= n <= 37
 *     ∙ The answer is guaranteed to fit within a 32-bit integer,
 *       ie. answer <= 2³¹ - 1.
"""
from functools import cache

class Solution:
    @cache
    def tribonacci(self, n: int) -> int:
        if n <= 2:
            return int(n > 0)
        return sum(self.tribonacci(n - i) for i in range(1, 4))

if __name__ == "__main__":
    tests = (
        (4, 4),
        (25, 1389537),
    )
    for n, expected in tests:
        print(Solution().tribonacci(n) == expected)
