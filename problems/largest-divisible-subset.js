/**
 * 368. Largest Divisible Subset (Medium)
 * Given a set of distinct positive integers nums, return the largest subset
 * answer such that every pair (answer[i], answer[j]) of elements in this
 * subset satisfies:
 *     answer[i] % answer[j] == 0, or
 *     answer[j] % answer[i] == 0
 *
 * If there are multiple solutions, return any of them.
 *
 * Example 1:
 * Input: nums = [1,2,3]
 * Output: [1,2]
 * Explanation: [1,3] is also accepted.
 *
 * Example 2:
 * Input: nums = [1,2,4,8]
 * Output: [1,2,4,8]
 *
 * Constraints:
 *     1 <= nums.length <= 1000
 *     1 <= nums[i] <= 2 * 10⁹
 *     All the integers in nums are unique.
 */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var largestDivisibleSubset = function(nums) {
    nums.sort((a, b) => a - b);
    const ret = nums.map(n => [n]);
    nums.forEach((n, i) => {
        nums.slice(0, i).forEach((d, j) => {
            if (n % d == 0 && ret[i].length < ret[j].length + 1) {
                ret[i] = [...ret[j], n]}})});
    return ret.sort((a, b) => b.length - a.length)[0];
};

const tests = [
    [[1], [1]],
    [[1,2], [1,2]],
    [[1,2,3], [1,2]],
    [[1,2,3,9], [1,3,9]],
    [[1,2,4,8], [1,2,4,8]],
    [[2,3,6,8,9,12,27,81], [3,9,27,81]],
    [[5,9,18,54,108,540,90,180,360,720], [9,18,90,180,360,720]],
    [[1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576,2097152,4194304,8388608,16777216,33554432,67108864,134217728,268435456,536870912,1073741824], [1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576,2097152,4194304,8388608,16777216,33554432,67108864,134217728,268435456,536870912,1073741824]],
];

for (const [nums, expected] of tests) {
    test: {
        for (const n of largestDivisibleSubset(nums)) {
            let i = expected.indexOf(n);
            if (i > -1) {
                expected.splice(i, 1);
            } else {
                console.log(false);
                break test;
            }
        }
        console.log(expected.length == 0);
    }
}
