"""
 * 171. Excel Sheet Column Number (Easy)
 * Given a string columnTitle that represents the column title as appear in an
 * Excel sheet, return its corresponding column number.
 *
 * For example:
 * A -> 1
 * B -> 2
 * C -> 3
 * ...
 * Z -> 26
 * AA -> 27
 * AB -> 28
 * ...
 *
 * Example 1:
 * Input: columnTitle = "A"
 * Output: 1
 *
 * Example 2:
 * Input: columnTitle = "AB"
 * Output: 28
 *
 * Example 3:
 * Input: columnTitle = "ZY"
 * Output: 701
 *
 * Constraints:
 *     ∙ 1 <= columnTitle.length <= 7
 *     ∙ columnTitle consists only of uppercase English letters.
 *     ∙ columnTitle is in the range ["A", "FXSHRXW"].
"""
class Solution:
    def titleToNumber(self, col: str) -> int:
        if col == "":
            return 0
        return ord(col[-1]) - ord('A') + 1 + 26*self.titleToNumber(col[:-1])

if __name__ == "__main__":
    tests = (
        ("A", 1),
        ("AB", 28),
        ("ZY", 701),
        ("FXSHRXW", 2147483647),
    )
    for columnTitle, expected in tests:
        print(Solution().titleToNumber(columnTitle) == expected)
