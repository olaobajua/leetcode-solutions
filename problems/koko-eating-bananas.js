/**
 * 875. Koko Eating Bananas (Medium)
 * Koko loves to eat bananas. There are n piles of bananas, the iᵗʰ pile has
 * piles[i] bananas. The guards have gone and will come back in h hours.
 *
 * Koko can decide her bananas-per-hour eating speed of k. Each hour, she
 * chooses some pile of bananas and eats k bananas from that pile. If the pile
 * has less than k bananas, she eats all of them instead and will not eat any
 * more bananas during this hour.
 *
 * Koko likes to eat slowly but still wants to finish eating all the bananas
 * before the guards return.
 *
 * Return the minimum integer k such that she can eat all the bananas within h
 * hours.
 *
 * Example 1:
 * Input: piles = [3,6,7,11], h = 8
 * Output: 4
 *
 * Example 2:
 * Input: piles = [30,11,23,4,20], h = 5
 * Output: 30
 *
 * Example 3:
 * Input: piles = [30,11,23,4,20], h = 6
 * Output: 23
 *
 * Constraints:
 *     ∙ 1 <= piles.length <= 10⁴
 *     ∙ piles.length <= h <= 10⁹
 *     ∙ 1 <= piles[i] <= 10⁹
 */

/**
 * @param {number[]} piles
 * @param {number} h
 * @return {number}
 */
var minEatingSpeed = function(piles, h) {
    function canEatAllBananas(speed) {
        return sum(...piles.map(pile => Math.ceil(pile/speed))) <= h;
    }
    if (h === piles.length) {
        return Math.max(...piles);
    }
    let lo = Math.ceil(sum(...piles) / h);
    let hi = Math.max(...piles);
    let mid = 0;
    while (lo <= hi) {
        mid = Math.floor((lo + hi) / 2);
        if (canEatAllBananas(mid)) {
            hi = mid - 1;
        } else {
            lo = mid + 1;
        }
    }
    return lo;
}

function sum(...iterable) {
    return iterable.reduce((acc, x) => acc + x);
}

const tests = [
    [[3,6,7,11], 8, 4],
    [[30,11,23,4,20], 5, 30],
    [[30,11,23,4,20], 6, 23],
];

for (const [piles, h, expected] of tests) {
    console.log(minEatingSpeed(piles, h) == expected);
}
