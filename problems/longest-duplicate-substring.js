/**
 * 1044. Longest Duplicate Substring (Hard)
 * Given a string s, consider all duplicated substrings: (contiguous)
 * substrings of s that occur 2 or more times. The occurrences may overlap.
 *
 * Return any duplicated substring that has the longest possible length.
 * If s does not have a duplicated substring, the answer is "".
 *
 * Example 1:
 * Input: s = "banana"
 * Output: "ana"
 *
 * Example 2:
 * Input: s = "abcd"
 * Output: ""
 *
 * Constraints:
 *     2 <= s.length <= 3 * 10⁴
 *     s consists of lowercase English letters.
 */

/**
 * @param {string} s
 * @return {string}
 */
var longestDupSubstring = function(s) {
    let start = 0;
    let end = s.length;
    let ret = '';
    while (start <= end) {
        const middle = Math.floor((start + end) / 2);
        const cur = rabinKarpSet(s, middle);
        if (cur) {
            ret = cur;
            start = middle + 1;
        } else {
            end = middle - 1;
        }
    }
    return ret;
};

/**
 * The Rabin–Karp algorithm for multiple pattern search.
 * https://en.wikipedia.org/wiki/Rabin%E2%80%93Karp_algorithm
 */
function rabinKarpSet(s, m) {
    if (m == 0) {
        return '';
    }
    const Q = 2**31 - 1;
    const X = 256;
    let hashKey = 0;
    let mul = 1;

    for (let i = 0; i < m; ++i) {
        mul = (mul * X) % Q;
        hashKey = (hashKey * X + s[i].charCodeAt()) % Q;
    }
    const hashes = {[hashKey]: [[0, m]]};
    for (let i = 1; i <= s.length - m; ++i) {
        hashKey = hashKey * X - mul * s[i-1].charCodeAt() % Q + Q;
        hashKey = (hashKey + s[i+m-1].charCodeAt()) % Q;
        for (let [start, end] of hashes[hashKey] || []) {
            const sub = s.slice(i, i+m);
            if (s.slice(start, end) == sub) {
                return sub;
            }
        }
        (hashes[hashKey]) ? hashes[hashKey].push([i, i+m])
                          : hashes[hashKey] = [[i, i+m]];
    }
    return '';
}

const tests = [
    ["banana", "ana"],
    ["abcd", ""],
    ["abcdabc", "abc"],
    ["aa", "a"],
    ["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"],
];

const start = new Date().getTime();
for (let [s, expected] of tests) {
    console.log(longestDupSubstring(s) == expected);
}
console.log(`Time elapsed: ${new Date().getTime() - start} msec`);
