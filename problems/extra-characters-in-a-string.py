"""
 * 2707. Extra Characters in a String [Medium]
 * You are given a 0-indexed string s and a dictionary of words dictionary.
 * You have to break s into one or more non-overlapping substrings such that
 * each substring is present in dictionary. There may be some extra characters
 * in s which are not present in any of the substrings.
 *
 * Return the minimum number of extra characters left over if you break up s
 * optimally.
 *
 * Example 1:
 * Input: s = "leetscode", dictionary = ["leet","code","leetcode"]
 * Output: 1
 * Explanation: We can break s in two substrings: "leet" from index 0 to 3 and
 * "code" from index 5 to 8. There is only 1 unused character (at index 4), so
 * we return 1.
 *
 * Example 2:
 * Input: s = "sayhelloworld", dictionary = ["hello","world"]
 * Output: 3
 * Explanation: We can break s in two substrings: "hello" from index 3 to 7
 * and "world" from index 8 to 12. The characters at indices 0, 1, 2 are not
 * used in any substring and thus are considered as extra characters. Hence, we
 * return 3.
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 50
 *     ∙ 1 <= dictionary.length <= 50
 *     ∙ 1 <= dictionary[i].length <= 50
 *     ∙ dictionary[i] and s consists of only lowercase English letters
 *     ∙ dictionary contains distinct words
"""
from functools import cache
from typing import List

class Solution:
    def minExtraChar(self, s: str, dictionary: List[str]) -> int:
        def search(i):
            ret = []
            p = trie
            for j in range(i, n):
                char = s[j]
                if char not in p:
                    return ret
                p = p[char]
                if "\0" in p:
                    yield j

            return ret

        @cache
        def dp(i):
            if i == n:
                return 0

            ret = dp(i + 1) + 1
            for j in search(i):
                ret = min(ret, dp(j + 1))

            return ret

        trie = {}
        for word in dictionary:
            p = trie
            for char in word:
                p = p.setdefault(char, {})
            p["\0"] = word

        n = len(s)
        return dp(0)


if __name__ == "__main__":
    tests = (
        ("leetscode", ["leet","code","leetcode"], 1),
        ("sayhelloworld", ["hello","world"], 3),
        ("enknouowgowcipfipojlrpuowgoiogiiebfjiafwksaigjyd", ["gw","lq","yzqch","sah","giieb","kfqczw","qxqz","jb","ucxmpe","hpwr","y","vzlhe","i","kn","ip","iafwk","zl","dw","yhxeqi","egktb","xasq","f","c","vrllz","p","uowgo","pgxd","gnjgkm","rnug","sa","vfccq","j"], 10),
    )
    for s, dictionary, expected in tests:
        print(Solution().minExtraChar(s, dictionary) == expected)
