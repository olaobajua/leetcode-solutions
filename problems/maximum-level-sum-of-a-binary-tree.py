"""
 * 1161. Maximum Level Sum of a Binary Tree [Medium]
 * Given the root of a binary tree, the level of its root is 1, the level of
 * its children is 2, and so on.
 *
 * Return the smallest level x such that the sum of all the values of nodes at
 * level x is maximal.
 *
 * Example 1:
 *      1
 *   7     0
 * 7  -8
 * Input: root = [1,7,0,7,-8,null,null]
 * Output: 2
 * Explanation:
 * Level 1 sum = 1.
 * Level 2 sum = 7 + 0 = 7.
 * Level 3 sum = 7 + -8 = -1.
 * So we return the level with the maximum sum which is level 2.
 *
 * Example 2:
 *  989
 *    10250
 * 98693 -89388
 *          -32127
 * Input: root = [989,null,10250,98693,-89388,null,null,null,-32127]
 * Output: 2
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ -10⁵ <= Node.val <= 10⁵
"""
from math import inf
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def maxLevelSum(self, root: Optional[TreeNode]) -> int:
        level = [root]
        max_sum = -inf
        ret = cur_level = 1
        while level:
            next_level = []
            s = 0
            for node in level:
                s += node.val
                if node.left:
                    next_level.append(node.left)
                if node.right:
                    next_level.append(node.right)
            if s > max_sum:
                max_sum = s
                ret = cur_level
            cur_level += 1
            level = next_level
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,7,0,7,-8,None,None], 2),
        ([989,None,10250,98693,-89388,None,None,None,-32127], 2),
        ([-100,-200,-300,-20,-5,-10,None], 3),
    )
    for nums, expected in tests:
        print(Solution().maxLevelSum(build_tree(nums)) == expected)
