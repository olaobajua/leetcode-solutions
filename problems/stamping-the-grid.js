/**
 * 2132. Stamping the Grid (Hard)
 * You are given an m x n binary matrix grid where each cell is either 0
 * (empty) or 1 (occupied).
 *
 * You are then given stamps of size stampHeight x stampWidth. We want to fit
 * the stamps such that they follow the given restrictions and requirements:
 *     ∙ Cover all the empty cells.
 *     ∙ Do not cover any of the occupied cells.
 *     ∙ We can put as many stamps as we want.
 *     ∙ Stamps can overlap with each other.
 *     ∙ Stamps are not allowed to be rotated.
 *     ∙ Stamps must stay completely inside the grid.
 *
 * Return true if it is possible to fit the stamps while following the given
 * restrictions and requirements. Otherwise, return false.
 *
 * Example 1:
 * Input: grid = [[1,0,0,0],
 *                [1,0,0,0],
 *                [1,0,0,0],
 *                [1,0,0,0],
 *                [1,0,0,0]],
 *        stampHeight = 4, stampWidth = 3
 * Output: true
 * Explanation: We have two overlapping stamps that are able to cover all the
 * empty cells.
 *
 * Example 2:
 * Input: grid = [[1,0,0,0],
 *                [0,1,0,0],
 *                [0,0,1,0],
 *                [0,0,0,1]],
 *        stampHeight = 2, stampWidth = 2
 * Output: false
 * Explanation: There is no way to fit the stamps onto all the empty cells
 * without the stamps going outside the grid.
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[r].length
 *     ∙ 1 <= m, n <= 10⁵
 *     ∙ 1 <= m * n <= 2 * 10⁵
 *     ∙ grid[r][c] is either 0 or 1.
 *     ∙ 1 <= stampHeight, stampWidth <= 10⁵
 */

/**
 * @param {number[][]} grid
 * @param {number} stampHeight
 * @param {number} stampWidth
 * @return {boolean}
 */
var possibleToStamp = function(grid, stampHeight, stampWidth) {
    const m = grid.length;
    const n = grid[0].length;
    const cols = [...Array(m)].map(_ => Array(n).fill(0));
    const rows = [...Array(m)].map(_ => Array(n).fill(0));
    for (let r = 0; r < m; ++r) {
        for (let c = 0; c < n; ++c) {
            if (grid[r][c] == 0) {
                cols[r][c] = c > 0 ? cols[r][c-1] + 1 : 1;
                rows[r][c] = r > 0 ? rows[r-1][c] + 1 : 1;
                if (r > 0 && c > 0 && cols[r-1][c] < stampWidth &&
                        grid[r][c-1] == 0) {
                    rows[r][c] = Math.min(rows[r][c], rows[r][c-1]);
                }
            } else {
                if (r > 0 && 0 < rows[r-1][c] && rows[r-1][c] < stampHeight) {
                    return false;
                }
                if (c > 0 && 0 < cols[r][c-1] && cols[r][c-1] < stampWidth) {
                    return false;
                }
            }
        }
    }
    for (let c = 0; c < n; ++c) {
        if (0 < rows[m-1][c] && rows[m-1][c] < stampHeight) {
            return false;
        }
    }
    for (let r = 0; r < m; ++r) {
        if (0 < cols[r][n-1] && cols[r][n-1] < stampWidth) {
            return false;
        }
    }
    return true;
};

const tests = [
    [[[1,0],
      [0,0]], 2, 2, false],
    [[[1,0,0,0],
      [1,0,0,0],
      [1,0,0,0],
      [1,0,0,0],
      [1,0,0,0]], 4, 3, true],
    [[[1,0,0,0],
      [0,1,0,0],
      [0,0,1,0],
      [0,0,0,1]], 2, 2, false],
    [[[0,0,1,1],
      [0,0,1,1]], 2, 1, true],
    [[[1,1,1],
      [1,0,1],
      [1,1,1]], 1, 2, false],
    [[[1,1,1],
      [1,0,1],
      [1,1,1]], 2, 1, false],
    [[[0,0,0,0],
      [0,0,0,0],
      [0,0,0,0],
      [0,0,0,0]], 5, 5, false],
    [[[1,1,1],
      [1,1,1],
      [1,1,1],
      [1,1,1],
      [1,1,1],
      [1,1,1]], 9, 4, true],
    [[[0,0,0,0,0],
      [0,0,0,0,0],
      [0,0,1,0,0],
      [0,0,0,0,1],
      [0,0,0,1,1]], 2, 2, false],
    [[[0,0,0,0,0],
      [0,0,0,0,0],
      [0,0,1,0,0],
      [0,0,0,0,1],
      [0,0,0,0,1]], 2, 2, true],
    [[[0,0,0,0,0],
      [0,0,0,0,0],
      [0,0,0,0,0],
      [1,0,0,0,0],
      [1,1,0,0,0]], 2, 2, true],
];

for (const [grid, stampHeight, stampWidth, expected] of tests) {
    console.log(possibleToStamp(grid, stampHeight, stampWidth) == expected);
}
