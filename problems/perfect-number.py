"""
 * 507. Perfect Number [Easy]
 * A perfect number (https://en.wikipedia.org/wiki/Perfect_number) is a
 * positive integer that is equal to the sum of its positive divisors,
 * excluding the number itself. A divisor of an integer x is an integer that
 * can divide x evenly.
 *
 * Given an integer n, return true if n is a perfect number, otherwise return
 * false.
 *
 * Example 1:
 * Input: num = 28
 * Output: true
 * Explanation: 28 = 1 + 2 + 4 + 7 + 14
 * 1, 2, 4, 7, and 14 are all divisors of 28.
 *
 * Example 2:
 * Input: num = 7
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= num <= 10⁸
"""
from math import ceil, sqrt

class Solution:
    def checkPerfectNumber(self, num: int) -> bool:
        if num == 1:
            return False

        divisors = {1}
        for x in range(2, ceil(sqrt(num))):
            if num % x == 0:
                divisors.add(x)
                divisors.add(num // x)

        return num == sum(divisors)

if __name__ == "__main__":
    tests = (
        (28, True),
        (7, False),
        (6, True),
        (1, False),
    )
    for num, expected in tests:
        print(Solution().checkPerfectNumber(num) == expected)
