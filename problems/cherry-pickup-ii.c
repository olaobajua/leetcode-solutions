/**
 * 1463. Cherry Pickup II (Hard)
 * You are given a rows x cols matrix grid representing a field of cherries
 * where grid[i][j] represents the number of cherries that you can collect from
 * the (i, j) cell.
 *
 * You have two robots that can collect cherries for you:
 *     ∙ Robot #1 is located at the top-left corner (0, 0), and
 *     ∙ Robot #2 is located at the top-right corner (0, cols - 1).
 *
 * Return the maximum number of cherries collection using both robots by
 * following the rules below:
 *     ∙ From a cell (i, j), robots can move to cell (i + 1, j - 1),
 *       (i + 1, j), or (i + 1, j + 1).
 *     ∙ When any robot passes through a cell, It picks up all cherries, and
 *       the cell becomes an empty cell.
 *     ∙ When both robots stay in the same cell, only one takes the cherries.
 *     ∙ Both robots cannot move outside of the grid at any moment.
 *     ∙ Both robots should reach the bottom row in grid.
 *
 * Example 1:
 * Input: grid = [[3,1,1],
 *                [2,5,1],
 *                [1,5,5],
 *                [2,1,1]]
 * Output: 24
 * Explanation:
 * respectively.
 * Cherries taken by Robot #1, (3 + 2 + 5 + 2) = 12.
 * Cherries taken by Robot #2, (1 + 5 + 5 + 1) = 12.
 * Total of cherries: 12 + 12 = 24.
 *
 * Example 2:
 * Input: grid = [[1,0,0,0,0,0,1],
 *                [2,0,0,0,0,3,0],
 *                [2,0,9,0,0,0,0],
 *                [0,3,0,5,4,0,0],
 *                [1,0,2,3,0,0,6]]
 * Output: 28
 * Explanation:
 * respectively.
 * Cherries taken by Robot #1, (1 + 9 + 5 + 2) = 17.
 * Cherries taken by Robot #2, (1 + 3 + 4 + 3) = 11.
 * Total of cherries: 17 + 11 = 28.
 *
 * Constraints:
 *     ∙ rows == grid.length
 *     ∙ cols == grid[i].length
 *     ∙ 2 <= rows, cols <= 70
 *     ∙ 0 <= grid[i][j] <= 100
 */
int cherryPickup(int **grid, int gridSize, int *gridColSize) {
    const int R = gridSize, C = gridColSize[0];
    static int dp[72][72][72];
    for (int row = 0; row < R; ++row) {
        for (int col1 = 0; col1 <= C + 1; ++col1) {
            for (int col2 = 0; col2 <= C + 1; ++col2) {
                dp[row][col1][col2] = INT_MIN;
            }
        }
    }
    dp[0][1][C] = grid[0][0] + grid[0][C-1];
    for (int r = 1; r < R; ++r) {
        for (int c1 = 1; c1 <= C; ++c1) {
            for (int c2 = 1; c2 <= C; ++c2) {
                int prev = INT_MIN;
                for (int dc1 = -1; dc1 <= 1; ++dc1) {
                    for (int dc2 = -1; dc2 <= 1; ++dc2) {
                        prev = fmax(prev, dp[r-1][c1+dc1][c2+dc2]);
                        dp[r][c1][c2] = ((grid[r][c1-1] + grid[r][c2-1]) /
                                         (1 + (c1 == c2)) + prev);
                    }
                }
            }
        }
    }
    int ret = INT_MIN;
    for (int col1 = 0; col1 <= C; ++col1) {
        for (int col2 = 0; col2 <= C; ++col2) {
            ret = fmax(ret, dp[R-1][col1][col2]);
        }
    }
    return ret;
}
