/**
 * 154. Find Minimum in Rotated Sorted Array II (Hard)
 * Suppose an array of length n sorted in ascending order is rotated between 1
 * and n times. For example, the array nums = [0,1,4,4,5,6,7] might become:
 *     [4,5,6,7,0,1,4] if it was rotated 4 times.
 *     [0,1,4,4,5,6,7] if it was rotated 7 times.
 *
 * Notice that rotating an array [a[0], a[1], a[2], ..., a[n-1]] 1 time results
 * in the array [a[n-1], a[0], a[1], a[2], ..., a[n-2]].
 *
 * Given the sorted rotated array nums that may contain duplicates, return the
 * minimum element of this array.
 *
 * You must decrease the overall operation steps as much as possible.
 *
 * Example 1:
 * Input: nums = [1,3,5]
 * Output: 1
 *
 * Example 2:
 * Input: nums = [2,2,2,0,1]
 * Output: 0
 *
 * Constraints:
 *     n == nums.length
 *     1 <= n <= 5000
 *     -5000 <= nums[i] <= 5000
 *     nums is sorted and rotated between 1 and n times.
 *
 * Follow up: This problem is similar to Find Minimum in Rotated Sorted Array,
 * but nums may contain duplicates. Would this affect the runtime complexity?
 * How and why?
 */
#include <stdio.h>
#include <stdlib.h>

int findMin(int* nums, int numsSize) {
    if (nums[0] < nums[numsSize - 1]) {
        return nums[0];
    }
    for (int i = 1; i < numsSize; ++i) {
        if (nums[i] < nums[i - 1]) {
            return nums[i];
        }
    }
    return nums[numsSize - 1];
}

int main() {
    // int nums[] = {1,3,5}, expected = 1;
    int nums[] = {2,2,2,0,1}, expected = 0;
    // int nums[] = {1,2,2,2,0}, expected = 0;
    // int nums[] = {2,3,4,2,2}, expected = 2;
    // int nums[] = {10,1,10,10,10}, expected = 1;
    // int nums[] = {3,3,1,3}, expected = 1;
    // int nums[] = {3,4,5,1,2}, expected = 1;
    // int nums[] = {4,5,6,7,0,1,2}, expected = 0;
    // int nums[] = {11,13,15,17}, expected = 11;
    // int nums[] = {3,1,2}, expected = 1;
    // int nums[] = {5,1,2,3,4}, expected = 1;
    // int nums[] = {2,3,4,5,1}, expected = 1;
    const size_t numsSize = sizeof(nums) / sizeof(nums[0]);
    printf("%s\n", (findMin(nums, numsSize) == expected) ? "true" : "false");
    return EXIT_SUCCESS;
}
