"""
 * 95. Unique Binary Search Trees II [Medium]
 * Given an integer n, return all the structurally unique BST's (binary search
 * trees), which has exactly n nodes of unique values from 1 to n. Return the
 * answer in any order.
 *
 * Example 1:
 * 1    1     2     3    3
 *  2     3  1 3  1     2
 *   3   2         2   1
 *
 * Input: n = 3
 * Output:
 * [[1,null,2,null,3],[1,null,3,2],[2,1,3],[3,1,null,null,2],[3,2,null,1]]
 *
 * Example 2:
 * Input: n = 1
 * Output: [[1]]
 *
 * Constraints:
 *     ∙ 1 <= n <= 8
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def generateTrees(self, n: int) -> List[Optional[TreeNode]]:
        def dfs(lo, hi):
            if lo == hi:
                return [None]
            ret = []
            for i in range(lo, hi):
                for left in dfs(lo, i):
                    for right in dfs(i + 1, hi):
                        ret.append(TreeNode(i, left, right))
            return ret

        return dfs(1, n + 1)

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        (3, [[1,None,2,None,3],[1,None,3,2],[2,1,3],[3,1,None,None,2],[3,2,None,1]]),
        (1, [[1]]),
    )
    for n, expected in tests:
        ret = set()
        for root in Solution().generateTrees(n):
            nums = [*tree_to_list(root)]
            while nums and nums[-1] is None:
                nums.pop()
            ret.add(tuple(nums))

        print(ret == set(map(tuple, expected)))
