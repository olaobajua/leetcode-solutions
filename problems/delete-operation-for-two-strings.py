"""
 * 583. Delete Operation for Two Strings (Medium)
 * Given two strings word1 and word2, return the minimum number of steps
 * required to make word1 and word2 the same.
 *
 * In one step, you can delete exactly one character in either string.
 *
 * Example 1:
 * Input: word1 = "sea", word2 = "eat"
 * Output: 2
 * Explanation: You need one step to make "sea" to "ea" and another step to
 * make "eat" to "ea".
 *
 * Example 2:
 * Input: word1 = "leetcode", word2 = "etco"
 * Output: 4
 *
 * Constraints:
 *     ∙ 1 <= word1.length, word2.length <= 500
 *     ∙ word1 and word2 consist of only lowercase English letters.
"""
from functools import cache

class Solution:
    def minDistance(self, word1: str, word2: str) -> int:
        @cache
        def dp(i1, i2):
            if i1 == n1 or i2 == n2:
                return 0
            if (word1[i1] == word2[i2]):
                ret = 1 + dp(i1 + 1, i2 + 1)
            else:
                ret = max(dp(i1 + 1, i2), dp(i1, i2 + 1))
            return ret
        n1 = len(word1)
        n2 = len(word2)
        return n1 + n2 - 2*dp(0, 0)

if __name__ == "__main__":
    tests = (
        ("sea", "eat", 2),
        ("leetcode", "etco", 4),
        ("park", "spake", 3),
        ("leetcrode", "etaco", 6),
    )
    for word1, word2, expected in tests:
        print(Solution().minDistance(word1, word2) == expected)
