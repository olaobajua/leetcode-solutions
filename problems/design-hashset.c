/**
 * 705. Design HashSet (Easy)
 * Design a HashSet without using any built-in hash table libraries.
 *
 * Implement MyHashSet class:
 *     ∙ void add(key) Inserts the value key into the HashSet.
 *     ∙ bool contains(key) Returns whether the value key exists in the HashSet
 *       or not.
 *     ∙ void remove(key) Removes the value key in the HashSet. If key does not
 *       exist in the HashSet, do nothing.
 *
 * Example 1:
 * Input
 * ["MyHashSet", "add", "add", "contains", "contains", "add", "contains",
 *  "remove", "contains"]
 * [[], [1], [2], [1], [3], [2], [2], [2], [2]]
 * Output
 * [null, null, null, true, false, null, true, null, false]
 *
 * Explanation
 * MyHashSet myHashSet = new MyHashSet();
 * myHashSet.add(1);      // set = [1]
 * myHashSet.add(2);      // set = [1, 2]
 * myHashSet.contains(1); // return True
 * myHashSet.contains(3); // return False, (not found)
 * myHashSet.add(2);      // set = [1, 2]
 * myHashSet.contains(2); // return True
 * myHashSet.remove(2);   // set = [1]
 * myHashSet.contains(2); // return False, (already removed)
 *
 * Constraints:
 *     ∙ 0 <= key <= 10⁶
 *     ∙ At most 10⁴ calls will be made to add, remove, and contains.
 */
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define hash(key) (((long)(key) * 39916801) & ((1 << 20) - 1)) >> 5
#define SIZE 1 << 15

typedef struct MyHashSet {
    int key;
    struct MyHashSet *next;
} MyHashSet;

MyHashSet* myHashSetCreate() {
    return calloc(SIZE, sizeof(MyHashSet));
}

void myHashSetAdd(MyHashSet *obj, int key) {
    int index = hash(key);
    for (MyHashSet *cur = obj[index].next; cur; cur = cur->next) {
        if (cur->key == key) {
            return;
        }
    }
    MyHashSet *new_node = calloc(1, sizeof(MyHashSet));
    new_node->key = key;
    new_node->next = obj[index].next;
    obj[index].next = new_node;
}

void myHashSetRemove(MyHashSet* obj, int key) {
    for (MyHashSet **cur = &obj[hash(key)].next; *cur; cur = &(*cur)->next) {
        if ((*cur)->key == key) {
            MyHashSet *del = *cur;
            *cur = (*cur)->next;
            free(del);
            return;
        }
    }
}

bool myHashSetContains(MyHashSet* obj, int key) {
    for (MyHashSet *cur = obj[hash(key)].next; cur; cur = cur->next) {
        if (cur->key == key) {
            return true;
        }
    }
    return false;
}

void myHashSetFree(MyHashSet* obj) {
    for (int i = 0; i < SIZE; ++i) {
        MyHashSet *cur = obj[i].next;
        while (cur) {
            MyHashSet *del = cur;
            cur = cur->next;
            free(del);
        }
    }
    free(obj);
}

int main() {
    MyHashSet* obj = myHashSetCreate();
    myHashSetAdd(obj, 1);
    myHashSetAdd(obj, 2);
    printf("%s\n", true == myHashSetContains(obj, 1) ? "true" : "false");
    printf("%s\n", false == myHashSetContains(obj, 3) ? "true" : "false");
    myHashSetAdd(obj, 2);
    printf("%s\n", true == myHashSetContains(obj, 2) ? "true" : "false");
    myHashSetRemove(obj, 2);
    printf("%s\n", false == myHashSetContains(obj, 2) ? "true" : "false");
    myHashSetFree(obj);
}
