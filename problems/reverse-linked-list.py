"""
 * 206. Reverse Linked List (Easy)
 * Given the head of a singly linked list, reverse the list, and return the
 * reversed list.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5]
 * Output: [5,4,3,2,1]
 *
 * Example 2:
 * Input: head = [1,2]
 * Output: [2,1]
 *
 * Example 3:
 * Input: head = []
 * Output: []
 *
 * Constraints:
 *     The number of nodes in the list is the range [0, 5000].
 *     -5000 <= Node.val <= 5000
 *
 * Follow up: A linked list can be reversed either iteratively or recursively.
 * Could you implement both?
"""
from typing import Optional
# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        return reverse_recur(head)

def reverse_iter(head):
    prev = None
    cur = head
    while cur:
        prev = ListNode(cur.val, prev)
        cur = cur.next
    return prev

def reverse_recur(head, prev=None):
    return reverse_recur(head.next, ListNode(head.val, prev)) if head else prev

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5], [5,4,3,2,1]),
        ([1,2], [2,1]),
        ([], []),
    )
    for nums, expected in tests:
        head = list_to_linked_list(nums)
        print(linked_list_to_list(Solution().reverseList(head)) == expected)
