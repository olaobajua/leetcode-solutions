"""
 * 1048. Longest String Chain [Medium]
 * You are given an array of words where each word consists of lowercase
 * English letters.
 *
 * wordᵢ is a predecessor of wordⱼ if and only if we can insert exactly one
 * letter anywhere in wordᵢ without changing the order of the other characters
 * to make it equal to wordᵢ.
 *
 *     ∙ For example, "abc" is a predecessor of "abac", while "cba" is not a
 *       predecessor of "bcad".
 *
 * A word chain is a sequence of words [word₁, word₂, ..., wordₖ] with k >= 1,
 * where word₁ is a predecessor of word₂, word₂ is a predecessor of word₃, and
 * so on. A single word is trivially a word chain with k == 1.
 *
 * Return the length of the longest possible word chain with words chosen from
 * the given list of words.
 *
 * Example 1:
 * Input: words = ["a","b","ba","bca","bda","bdca"]
 * Output: 4
 * Explanation: One of the longest word chains is ["a","ba","bda","bdca"].
 *
 * Example 2:
 * Input: words = ["xbc","pcxbcf","xb","cxbc","pcxbc"]
 * Output: 5
 * Explanation: All the words can be put in a word chain ["xb", "xbc", "cxbc",
 * "pcxbc", "pcxbcf"].
 *
 * Example 3:
 * Input: words = ["abcd","dbqca"]
 * Output: 1
 * Explanation: The trivial word chain ["abcd"] is one of the longest word
 * chains.
 * ["abcd","dbqca"] is not a valid word chain because the ordering of the
 * letters is changed.
 *
 * Constraints:
 *     ∙ 1 <= words.length <= 1000
 *     ∙ 1 <= words[i].length <= 16
 *     ∙ words[i] only consists of lowercase English letters.
"""
from collections import defaultdict
from functools import cache
from typing import List

class Solution:
    def longestStrChain(self, words: List[str]) -> int:
        @cache
        def dp(word):
            ret = 1
            for predecessor in predecessors[word]:
                ret = max(ret, dp(predecessor) + 1)

            return ret

        words_set = set(words)
        predecessors = defaultdict(list)
        for word in words:
            for i in range(len(word)):
                predecessor = word[:i] + word[i+1:]
                if predecessor in words_set:
                    predecessors[word].append(predecessor)

        return max(dp(word) for word in words)

if __name__ == "__main__":
    tests = (
        (["a","b","ba","bca","bda","bdca"], 4),
        (["xbc","pcxbcf","xb","cxbc","pcxbc"], 5),
        (["abcd","dbqca"], 1),
        (["a","b","ab","bac"], 2),
        (["rigih","iid","idisd","rigihta","hfmeqzxki","idid","idisajd","rifgihta","hfmeqzki","rigi","idisjd","hfsmeqzxki","idisaojd","rigiht"], 6),
    )
    for words, expected in tests:
        print(Solution().longestStrChain(words) == expected)
