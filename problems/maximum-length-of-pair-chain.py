"""
 * 646. Maximum Length of Pair Chain [Medium]
 * You are given an array of n pairs pairs where pairs[i] = [leftᵢ, rightᵢ]
 * and leftᵢ < rightᵢ.
 *
 * A pair p2 = [c, d] follows a pair p1 = [a, b] if b < c. A chain of pairs
 * can be formed in this fashion.
 *
 * Return the length longest chain which can be formed.
 *
 * You do not need to use up all the given intervals. You can select pairs in
 * any order.
 *
 * Example 1:
 * Input: pairs = [[1,2],[2,3],[3,4]]
 * Output: 2
 * Explanation: The longest chain is [1,2] -> [3,4].
 *
 * Example 2:
 * Input: pairs = [[1,2],[7,8],[4,5]]
 * Output: 3
 * Explanation: The longest chain is [1,2] -> [4,5] -> [7,8].
 *
 * Constraints:
 *     ∙ n == pairs.length
 *     ∙ 1 <= n <= 1000
 *     ∙ -1000 <= leftᵢ < rightᵢ <= 1000
"""
from functools import cache
from operator import itemgetter
from typing import List

class Solution:
    def findLongestChain(self, pairs: List[List[int]]) -> int:
        @cache
        def dp(i):
            if i == n:
                return 0

            a, b = pairs[i]
            ret = 1
            for j in range(i + 1, n):
                c, d = pairs[j]
                if b < c:
                    ret = max(ret, dp(j) + 1)
            return ret

        n = len(pairs)
        pairs.sort(key=itemgetter(1))
        return dp(0)

if __name__ == "__main__":
    tests = (
        ([[1,2],[2,3],[3,4]], 2),
        ([[1,2],[7,8],[4,5]], 3),
        ([[-6,9],[1,6],[8,10],[-1,4],[-6,-2],[-9,8],[-5,3],[0,3]], 3),
    )
    for pairs, expected in tests:
        print(Solution().findLongestChain(pairs) == expected)
