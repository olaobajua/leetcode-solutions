"""
 * 98. Validate Binary Search Tree [Medium]
 * Given the root of a binary tree, determine if it is a valid binary search
 * tree (BST).
 *
 * A valid BST is defined as follows:
 *     ∙ The left subtree of a node contains only nodes with keys less than
 *       the node's key.
 *     ∙ The right subtree of a node contains only nodes with keys greater
 *       than the node's key.
 *     ∙ Both the left and right subtrees must also be binary search trees.
 *
 * Example 1:
 * Input: root = [2,1,3]
 * Output: true
 *
 * Example 2:
 * Input: root = [5,1,4,null,null,3,6]
 * Output: false
 * Explanation: The root node's value is 5 but its right child's value is 4.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ -2³¹ <= Node.val <= 2³¹ - 1
"""
from math import inf
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def isValidBST(self, root: Optional[TreeNode]) -> bool:
        def dfs(root, min_val, max_val):
            if root is None:
                return True;
            if not min_val < root.val < max_val:
                return False;
            return (dfs(root.left, min_val, root.val) and
                    dfs(root.right, root.val, max_val))
        return dfs(root, -inf, inf)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([2,1,3], True),
        ([5,1,4,None,None,3,6], False),
        ([5,2,10,1,7,8,11], False),
        ([2,2,2], False),
        ([3,None,30,10,None,None,15,None,45], False),
    )
    for nums, expected in tests:
        print(Solution().isValidBST(build_tree(nums)) == expected)
