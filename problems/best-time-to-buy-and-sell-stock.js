/**
 * 121. Best Time to Buy and Sell Stock (Easy)
 * You are given an array prices where prices[i] is the price of a given stock
 * on the ith day.
 *
 * You want to maximize your profit by choosing a single day to buy one stock
 * and choosing a different day in the future to sell that stock.
 *
 * Return the maximum profit you can achieve from this transaction. If you
 * cannot achieve any profit, return 0.
 *
 * Example 1:
 * Input: prices = [7,1,5,3,6,4]
 * Output: 5
 * Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6),
 * profit = 6-1 = 5.
 * Note that buying on day 2 and selling on day 1 is not allowed because you
 * must buy before you sell.
 *
 * Example 2:
 * Input: prices = [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transactions are done and the max profit = 0.
 *
 * Constraints:
 *     ∙ 1 <= prices.length <= 10⁵
 *     ∙ 0 <= prices[i] <= 10⁴
 */

/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
    // Kadane's Algorithm
    let maxProfit = 0, curProfit = 0;
    for (let i = 1; i < prices.length; ++i) {
        curProfit = Math.max(0, curProfit += prices[i] - prices[i-1]);
        maxProfit = Math.max(curProfit, maxProfit);
    }
    return maxProfit;
};

const tests = [
    [[7,1,5,3,6,4], 5],
    [[7,6,4,3,1], 0],
    [[7,2,4,1], 2],
];

for (const [prices, expected] of tests) {
    console.log(maxProfit(prices) == expected);
}
