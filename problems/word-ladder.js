/**
 * 127. Word Ladder (Hard)
 * A transformation sequence from word beginWord to word endWord using a
 * dictionary wordList is a sequence of words
 * beginWord -> s₁ -> s₂ -> ... -> sₖ such that:
 *     ∙ Every adjacent pair of words differs by a single letter.
 *     ∙ Every sᵢ for 1 <= i <= k is in wordList. Note that beginWord does not
 *       need to be in wordList.
 *     ∙ sₖ == endWord
 *
 * Given two words, beginWord and endWord, and a dictionary wordList, return
 * the number of words in the shortest transformation sequence from beginWord
 * to endWord, or 0 if no such sequence exists.
 *
 * Example 1:
 * Input: beginWord = "hit", endWord = "cog",
 *        wordList = ["hot","dot","dog","lot","log","cog"]
 * Output: 5
 * Explanation: One shortest transformation sequence is
 * "hit" -> "hot" -> "dot" -> "dog" -> cog", which is 5 words long.
 *
 * Example 2:
 * Input: beginWord = "hit", endWord = "cog",
 * wordList = ["hot","dot","dog","lot","log"]
 * Output: 0
 * Explanation: The endWord "cog" is not in wordList, therefore there is no
 * valid transformation sequence.
 *
 * Constraints:
 *     ∙ 1 <= beginWord.length <= 10
 *     ∙ endWord.length == beginWord.length
 *     ∙ 1 <= wordList.length <= 5000
 *     ∙ wordList[i].length == beginWord.length
 *     ∙ beginWord, endWord, and wordList[i] consist of lowercase English
 *       letters.
 *     ∙ beginWord != endWord
 *     ∙ All the words in wordList are unique.
 */

/**
 * @param {string} beginWord
 * @param {string} endWord
 * @param {string[]} wordList
 * @return {number}
 */
var ladderLength = function(beginWord, endWord, wordList) {
    const words = new Set(wordList);
    const toVisit = [[beginWord, 1]];
    for (const [word, steps] of toVisit) {
        if (word == endWord) {
            return steps;
        }
        for (let i = 0; i < word.length; ++i) {
            for (const char of 'abcdefghijklmnopqrstuvwxyz') {
                const w = word.slice(0, i) + char + word.slice(i+1);
                if (words.has(w)) {
                    toVisit.push([w, steps + 1]);
                    words.delete(w);
                }
            }
        }
    }
    return 0;
};

const tests = [
    ["hit", "cog", ["hot","dot","dog","lot","log","cog"], 5],
    ["hit", "cog", ["hot","dot","dog","lot","log"], 0],
    ["a", "c", ["a","b","c"], 2],
    ["hot", "dog", ["hot","dog"], 0],
];

for (const [beginWord, endWord, wordList, expected] of tests) {
    console.log(ladderLength(beginWord, endWord, wordList) == expected);
}
