"""
 * 1043. Partition Array for Maximum Sum [Medium]
 * Given an integer array arr, partition the array into (contiguous) subarrays
 * of length at most k. After partitioning, each subarray has their values
 * changed to become the maximum value of that subarray.
 *
 * Return the largest sum of the given array after partitioning. Test cases
 * are generated so that the answer fits in a 32-bit integer.
 *
 * Example 1:
 * Input: arr = [1,15,7,9,2,5,10], k = 3
 * Output: 84
 * Explanation: arr becomes [15,15,15,9,10,10,10]
 *
 * Example 2:
 * Input: arr = [1,4,1,5,7,3,6,1,9,9,3], k = 4
 * Output: 83
 *
 * Example 3:
 * Input: arr = [1], k = 1
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 500
 *     ∙ 0 <= arr[i] <= 10⁹
 *     ∙ 1 <= k <= arr.length
"""
from functools import cache
from typing import List

class Solution:
    def maxSumAfterPartitioning(self, arr: List[int], k: int) -> int:
        @cache
        def dp(i):
            if i == n:
                return 0

            ret = 0
            max_element = 0
            for j in range(i, min(n, i + k)):
                max_element = max(max_element, arr[j])
                ret = max(ret, dp(j + 1) + max_element * (j - i + 1))

            return ret

        n = len(arr)
        return dp(0)

if __name__ == "__main__":
    tests = (
        ([1,15,7,9,2,5,10], 3, 84),
        ([1,4,1,5,7,3,6,1,9,9,3], 4, 83),
        ([1], 1, 1),
        ([20779,436849,274670,543359,569973,280711,252931,424084,361618,430777,136519,749292,933277,477067,502755,695743,413274,168693,368216,677201,198089,927218,633399,427645,317246,403380,908594,854847,157024,719715,336407,933488,599856,948361,765131,335089,522119,403981,866323,519161,109154,349141,764950,558613,692211], 26, 42389649),
    )
    for arr, k, expected in tests:
        print(Solution().maxSumAfterPartitioning(arr, k) == expected)
