/**
 * 881. Boats to Save People (Medium)
 * You are given an array people where people[i] is the weight of the iᵗʰ
 * person, and an infinite number of boats where each boat can carry a maximum
 * weight of limit. Each boat carries at most two people at the same time,
 * provided the sum of the weight of those people is at most limit.
 *
 * Return the minimum number of boats to carry every given person.
 *
 * Example 1:
 * Input: people = [1,2], limit = 3
 * Output: 1
 * Explanation: 1 boat (1, 2)
 *
 * Example 2:
 * Input: people = [3,2,2,1], limit = 3
 * Output: 3
 * Explanation: 3 boats (1, 2), (2) and (3)
 *
 * Example 3:
 * Input: people = [3,5,3,4], limit = 5
 * Output: 4
 * Explanation: 4 boats (3), (3), (4), (5)
 *
 * Constraints:
 *     1 <= people.length <= 5 * 10⁴
 *     1 <= people[i] <= limit <= 3 * 10⁴
 */

/**
 * @param {number[]} people
 * @param {number} limit
 * @return {number}
 */
var numRescueBoats = function(people, limit) {
    people.sort((a, b) => a - b);
    let i = 0;
    let j = people.length - 1;
    let ret = 0;
    while (i <= j) {
        i += people[i] + people[j] <= limit;
        --j;
        ++ret;
    }
    return ret;
};

const tests = [
    [[1,2], 3, 1],
    [[3,2,2,1], 3, 3],
    [[3,5,3,4], 5, 4],
    [[2,4], 5, 2],
    [[2,2], 6, 1],
    [[4,3,6], 6, 3],
    [[3,1,7], 7, 2],
    [[3,2,3,2,2], 6, 3],
    [[1,8,4,9,7,1,5,9,3,4], 10, 6],
    [[44,10,29,12,49,41,23,5,17,26], 50, 6],
];

for (const [people, limit, expected] of tests) {
    console.log(numRescueBoats(people, limit) == expected);
}
