"""
 * 1493. Longest Subarray of 1's After Deleting One Element [Medium]
 * Given a binary array nums, you should delete one element from it.
 *
 * Return the size of the longest non-empty subarray containing only 1's in
 * the resulting array. Return 0 if there is no such subarray.
 *
 * Example 1:
 * Input: nums = [1,1,0,1]
 * Output: 3
 * Explanation: After deleting the number in position 2, [1,1,1] contains 3
 * numbers with value of 1's.
 *
 * Example 2:
 * Input: nums = [0,1,1,1,0,1,1,0,1]
 * Output: 5
 * Explanation: After deleting the number in position 4, [0,1,1,1,1,1,0,1]
 * longest subarray with value of 1's is [1,1,1,1,1].
 *
 * Example 3:
 * Input: nums = [1,1,1]
 * Output: 2
 * Explanation: You must delete one element.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ nums[i] is either 0 or 1.
"""
from typing import List

class Solution:
    def longestSubarray(self, nums: List[int]) -> int:
        subarrays = []
        prev = None
        for i, x in enumerate(nums):
            if x == 1:
                if prev == 1:
                    subarrays[-1][1] = i
                else:
                    subarrays.append([i, i])
            prev = x
        if len(subarrays) == 0:
            return 0
        if len(subarrays) == 1:
            start, end = subarrays[0]
            if end - start + 1 == len(nums):
                return len(nums) - 1
            return end - start + 1
        pstart, pend = subarrays[0]
        ret = pend - pstart + 1
        for start, end in subarrays[1:]:
            if start - pend == 2:
                ret = max(ret, end - pstart)
            else:
                ret = max(ret, end - start + 1)
            pstart, pend = start, end
        return ret

if __name__ == "__main__":
    tests = (
        ([1,1,0,1], 3),
        ([0,1,1,1,0,1,1,0,1], 5),
        ([1,1,1], 2),
        ([1,0,0,1,0], 1),
    )
    for nums, expected in tests:
        print(Solution().longestSubarray(nums) == expected)
