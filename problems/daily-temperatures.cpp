/**
 * 739. Daily Temperatures [Medium]
 * Given an array of integers temperatures represents the daily temperatures,
 * return an array answer such that answer[i] is the number of days you have to
 * wait after the iᵗʰ day to get a warmer temperature. If there is no future
 * day for which this is possible, keep answer[i] == 0 instead.
 *
 * Example 1:
 * Input: temperatures = [73,74,75,71,69,72,76,73]
 * Output: [1,1,4,2,1,1,0,0]
 * Example 2:
 * Input: temperatures = [30,40,50,60]
 * Output: [1,1,1,0]
 * Example 3:
 * Input: temperatures = [30,60,90]
 * Output: [1,1,0]
 *
 * Constraints:
 *     ∙ 1 <= temperatures.length <= 10⁵
 *     ∙ 30 <= temperatures[i] <= 100
 */
#include <vector>
#include <stack>

class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        std::vector<int> ret(temperatures.size(), 0);
        std::stack<std::pair<int, int>> stack;

        for (int cur_day = 0; cur_day < temperatures.size(); ++cur_day) {
            int temp = temperatures[cur_day];
            while (!stack.empty() && stack.top().first < temp) {
                auto [_, day] = stack.top();
                stack.pop();
                ret[day] = cur_day - day;
            }
            stack.push({temp, cur_day});
        }

        return ret;
    }
};
