"""
 * 144. Binary Tree Preorder Traversal [Easy]
 * Given the root of a binary tree, return the preorder traversal of its
 * nodes' values.
 *
 * Example 1:
 * Input: root = [1,null,2,3]
 * Output: [1,2,3]
 *
 * Example 2:
 * Input: root = []
 * Output: []
 *
 * Example 3:
 * Input: root = [1]
 * Output: [1]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 100].
 *     ∙ -100 <= Node.val <= 100
 *
 * Follow up: Recursive solution is trivial, could you do it iteratively?
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def preorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        ret = []
        stack = [root]
        while stack:
            to_visit = [stack.pop()]
            for node in to_visit:
                if node:
                    ret.append(node.val)
                    to_visit.append(node.left)
                    stack.append(node.right)
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,None,2,3], [1,2,3]),
        ([], []),
        ([1], [1]),
        ([1,4,3,2], [1,4,2,3]),
        ([2,1,3,None,4], [2,1,4,3]),
    )
    for nums, expected in tests:
        print(Solution().preorderTraversal(build_tree(nums)) == expected)
