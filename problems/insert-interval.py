"""
 * 57. Insert Interval [Medium]
 * You are given an array of non-overlapping intervals intervals where
 * intervals[i] = [startᵢ, endᵢ] represent the start and the end of the iᵗʰ
 * interval and intervals is sorted in ascending order by startᵢ. You are also
 * given an interval newInterval = [start, end] that represents the start and
 * end of another interval.
 *
 * Insert newInterval into intervals such that intervals is still sorted in
 * ascending order by startᵢ and intervals still does not have any overlapping
 * intervals (merge overlapping intervals if necessary).
 *
 * Return intervals after the insertion.
 *
 * Example 1:
 * Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
 * Output: [[1,5],[6,9]]
 *
 * Example 2:
 * Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
 * Output: [[1,2],[3,10],[12,16]]
 * Explanation: Because the new interval [4,8] overlaps with
 * [3,5],[6,7],[8,10].
 *
 * Constraints:
 *     ∙ 0 <= intervals.length <= 10⁴
 *     ∙ intervals[i].length == 2
 *     ∙ 0 <= startᵢ <= endᵢ <= 10⁵
 *     ∙ intervals is sorted by startᵢ in ascending order.
 *     ∙ newInterval.length == 2
 *     ∙ 0 <= start <= end <= 10⁵
"""
from math import inf
from typing import List

class Solution:
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        ret = []
        new_start, new_end = newInterval
        for start, end in intervals:
            if end < new_start:
                ret.append([start, end])
            elif new_end < start:
                ret.append([new_start, new_end])
                ret.append([start, end])
                new_start = inf
            else:
                new_start = min(new_start, start)
                new_end = max(new_end, end)
        if new_start < inf:
            ret.append([new_start, new_end])

        return ret

if __name__ == "__main__":
    tests = (
        ([[1,3],[6,9]], [2,5], [[1,5],[6,9]]),
        ([[1,2],[3,5],[6,7],[8,10],[12,16]], [4,8], [[1,2],[3,10],[12,16]]),
        ([], [5,7], [[5,7]]),
        ([[1,5]], [6,8], [[1,5],[6,8]]),
        ([[1,5]], [2,3], [[1,5]]),
        ([[2,7],[8,8],[10,10],[12,13],[16,19]], [9,17], [[2,7],[8,8],[9,19]]),
    )
    for intervals, newInterval, expected in tests:
        print(Solution().insert(intervals, newInterval) == expected)
