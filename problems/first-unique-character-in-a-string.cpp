/**
 * 387. First Unique Character in a String [Easy]
 * Given a string s, find the first non-repeating character in it and return
 * its index. If it does not exist, return -1.
 *
 * Example 1:
 * Input: s = "leetcode"
 * Output: 0
 * Example 2:
 * Input: s = "loveleetcode"
 * Output: 2
 * Example 3:
 * Input: s = "aabb"
 * Output: -1
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ s consists of only lowercase English letters.
 */
#include <vector>

class Solution {
public:
    int firstUniqChar(std::string s) {
        std::vector<int> count(26, 0);
        for (char c : s) {
            ++count[c-'a'];
        }
        for (int i = 0; i < s.length(); ++i) {
            char c = s[i];
            if (count[c-'a'] == 1) {
                return i;
            }
        }

        return -1;
    }
};
