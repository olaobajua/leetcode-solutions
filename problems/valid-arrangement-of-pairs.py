"""
 * 2097. Valid Arrangement of Pairs (Hard)
 * You are given a 0-indexed 2D integer array pairs where
 * pairs[i] = [startᵢ, endᵢ]. An arrangement of pairs is valid if for every
 * index i where 1 <= i < pairs.length, we have endᵢ-1 == startᵢ.
 *
 * Return any valid arrangement of pairs.
 *
 * Note: The inputs will be generated such that there exists a valid
 * arrangement of pairs.
 *
 * Example 1:
 * Input: pairs = [[5,1],[4,5],[11,9],[9,4]]
 * Output: [[11,9],[9,4],[4,5],[5,1]]
 * Explanation:
 * This is a valid arrangement since endᵢ-1 always equals startᵢ.
 * end0 = 9 == 9 = start1
 * end1 = 4 == 4 = start2
 * end2 = 5 == 5 = start3
 *
 * Example 2:
 * Input: pairs = [[1,3],[3,2],[2,1]]
 * Output: [[1,3],[3,2],[2,1]]
 * Explanation:
 * This is a valid arrangement since endᵢ-1 always equals startᵢ.
 * end0 = 3 == 3 = start1
 * end1 = 2 == 2 = start2
 * The arrangements [[2,1],[1,3],[3,2]] and [[3,2],[2,1],[1,3]] are also valid.
 *
 * Example 3:
 * Input: pairs = [[1,2],[1,3],[2,1]]
 * Output: [[1,2],[2,1],[1,3]]
 * Explanation:
 * This is a valid arrangement since endᵢ-1 always equals startᵢ.
 * end0 = 2 == 2 = start1
 * end1 = 1 == 1 = start2
 *
 * Constraints:
 *     1 <= pairs.length <= 10⁵
 *     pairs[i].length == 2
 *     0 <= startᵢ, endᵢ <= 10⁹
 *     startᵢ != endᵢ
 *     No two pairs are exactly the same.
 *     There exists a valid arrangement of pairs.
"""
from collections import defaultdict
from typing import List

class Solution:
    def validArrangement(self, pairs: List[List[int]]) -> List[List[int]]:
        by1st = defaultdict(set)
        by2nd = defaultdict(set)
        for x1, x2 in pairs:
            by1st[x1].add(x2)
            by2nd[x2].add(x1)
        for node in by1st | by2nd:
            start = node
            if len(by1st[node]) - len(by2nd[node]) == 1:
                break
        path = [*reversed(hierholzer(by1st, start))]
        return [[n1, n2] for n1, n2 in zip(path, path[1:])]

def hierholzer(graph: dict, start: int) -> list:
    """Find an Eulerian path of the graph."""
    circuit = []
    stack = [start]
    while stack:
        while graph[stack[-1]]:
            stack.append(graph[stack[-1]].pop())
        circuit.append(stack.pop())
    return circuit

if __name__ == "__main__":
    tests = (
        [[5,1],[4,5],[11,9],[9,4]],
        [[1,3],[3,2],[2,1]],
        [[1,2],[1,3],[2,1]],
        [[17,18],[18,10],[10,18]],
        [[8,5],[8,7],[0,8],[0,5],[7,0],[5,0],[0,7],[8,0],[7,8]],
        [[5,3],[2,3],[0,1],[1,4],[0,5],[3,2],[4,3],[3,0]],
    )
    for pairs in tests:
        path = Solution().validArrangement(pairs)
        print(set(map(tuple, path)) == set(map(tuple, pairs)) and
              all(p1[1] == p2[0] for p1, p2 in zip(path, path[1:])))
