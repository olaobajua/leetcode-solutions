"""
 * 1424. Diagonal Traverse II [Medium]
 * Given a 2D integer array nums, return all elements of nums in diagonal
 * order as shown in the below images.
 *
 * Example 1:
 * Input: nums = [[1,2,3],
 *                [4,5,6],
 *                [7,8,9]]
 * Output: [1,4,2,7,5,3,8,6,9]
 *
 * Example 2:
 * Input: nums = [[1,2,3,4,5],
 *                [6,7],
 *                [8],
 *                [9,10,11],
 *                [12,13,14,15,16]]
 * Output: [1,6,2,8,7,3,9,4,12,10,5,13,11,14,15,16]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i].length <= 10⁵
 *     ∙ 1 <= sum(nums[i].length) <= 10⁵
 *     ∙ 1 <= nums[i][j] <= 10⁵
"""
from typing import List

class Solution:
    def findDiagonalOrder(self, nums: List[List[int]]) -> List[int]:
        indices = []
        for i, row in enumerate(nums):
            for j, x in enumerate(row):
                indices.append((i + j, j, x))

        return [x for s, c, x in sorted(indices)]

if __name__ == "__main__":
    tests = (
        ([[1,2,3],[4,5,6],[7,8,9]], [1,4,2,7,5,3,8,6,9]),
        ([[1,2,3,4,5],[6,7],[8],[9,10,11],[12,13,14,15,16]],
         [1,6,2,8,7,3,9,4,12,10,5,13,11,14,15,16]),
    )
    for nums, expected in tests:
        print(Solution().findDiagonalOrder(nums) == expected)
