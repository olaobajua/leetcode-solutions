"""
 * 1095. Find in Mountain Array [Hard]
 * (This problem is an interactive problem.)
 *
 * You may recall that an array arr is a mountain array if and only if:
 *     ∙ arr.length >= 3
 *     ∙ There exists some i with 0 < i < arr.length - 1 such that:
 *
 * 	    ∙ arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
 * 	    ∙ arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
 *
 * Given a mountain array mountainArr, return the minimum index such that
 * mountainArr.get(index) == target. If such an index does not exist, return
 * -1.
 *
 * You cannot access the mountain array directly. You may only access the
 * array using a MountainArray interface:
 *     ∙ MountainArray.get(k) returns the element of the array at index k
 *       (0-indexed).
 *     ∙ MountainArray.length() returns the length of the array.
 *
 * Submissions making more than 100 calls to MountainArray.get will be judged
 * Wrong Answer. Also, any solutions that attempt to circumvent the judge will
 * result in disqualification.
 *
 * Example 1:
 * Input: array = [1,2,3,4,5,3,1], target = 3
 * Output: 2
 * Explanation: 3 exists in the array, at index=2 and index=5. Return the
 * minimum index, which is 2.
 *
 * Example 2:
 * Input: array = [0,1,2,4,2,1], target = 3
 * Output: -1
 * Explanation: 3 does not exist in the array, so we return -1.
 *
 * Constraints:
 *     ∙ 3 <= mountain_arr.length() <= 10⁴
 *     ∙ 0 <= target <= 10⁹
 *     ∙ 0 <= mountain_arr.get(index) <= 10⁹
"""
from bisect import bisect_left
from functools import cache
"""
This is MountainArray's API interface.
You should not implement it, or speculate about its implementation
"""
class MountainArray:
    def __init__(self, nums):
        self.__nums = nums

    def get(self, index: int) -> int:
        return self.__nums[index]

    def length(self) -> int:
        return len(self.__nums)

class Solution:
    def findInMountainArray(self, target: int, mountain_arr: 'MountainArray') -> int:
        @cache
        class CachedArray:
            def __init__(self, array):
                self.__arr = array

            @cache
            def __getitem__(self, i):
                return mountain_arr.get(i)

        elements = CachedArray(mountain_arr)
        n = mountain_arr.length()
        lo = 0
        hi = n
        while lo < hi:
            peak = (lo + hi) // 2
            if elements[peak-1] < elements[peak] > elements[peak+1]:
                break
            elif elements[peak-1] < elements[peak] < elements[peak+1]:
                lo = peak + 1
            else:
                hi = peak

        i = bisect_left(elements, target, 0, peak)
        if elements[i] == target:
            return i

        lo = peak
        hi = n
        while lo < hi:
            mid = (lo + hi) // 2
            if elements[mid] > target:
                lo = mid + 1
            elif elements[mid] < target:
                hi = mid - 1
            else:
                return mid

        return lo if lo < n and elements[lo] == target else -1

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5,3,1], 3, 2),
        ([0,1,2,4,2,1], 3, -1),
        ([0,5,3,1], 1, 3),
        ([1,5,2], 0, -1),
        ([0,5,3,1], 0, 0),
    )
    for mountain_arr, target, expected in tests:
        ma = MountainArray(mountain_arr)
        print(Solution().findInMountainArray(target, ma) == expected)
