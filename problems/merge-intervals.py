"""
 * 56. Merge Intervals (Medium)
 * Given an array of intervals where intervals[i] = [startᵢ, endᵢ], merge all
 * overlapping intervals, and return an array of the non-overlapping intervals
 * that cover all the intervals in the input.
 *
 * Example 1:
 * Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
 * Output: [[1,6],[8,10],[15,18]]
 * Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
 *
 * Example 2:
 * Input: intervals = [[1,4],[4,5]]
 * Output: [[1,5]]
 * Explanation: Intervals [1,4] and [4,5] are considered overlapping.
 *
 * Constraints:
 *     1 <= intervals.length <= 10⁴
 *     intervals[i].length == 2
 *     0 <= startᵢ <= endᵢ <= 10⁴
"""
from typing import List

class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        ret = [min(intervals)]
        for cur in sorted(intervals):
            if cur[0] <= ret[-1][1]:
                ret[-1][1] = max(ret[-1][1], cur[1])
            else:
                ret.append(cur)
        return ret

if __name__ == "__main__":
    tests = (
        ([[1,3],[2,6],[8,10],[15,18]], [[1,6],[8,10],[15,18]]),
        ([[1,4],[4,5]], [[1,5]]),
        ([[1,4],[2,3]], [[1,4]]),
    )
    for intervals, expected in tests:
        print(Solution().merge(intervals) == expected)
