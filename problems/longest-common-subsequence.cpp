/**
 * 1143. Longest Common Subsequence [Medium]
 * Given two strings text1 and text2, return the length of their longest
 * common subsequence. If there is no common subsequence, return 0.
 *
 * A subsequence of a string is a new string generated from the original
 * string with some characters (can be none) deleted without changing the
 * relative order of the remaining characters.
 *     ∙ For example, "ace" is a subsequence of "abcde".
 *
 * A common subsequence of two strings is a subsequence that is common to both
 * strings.
 *
 * Example 1:
 * Input: text1 = "abcde", text2 = "ace"
 * Output: 3
 * Explanation: The longest common subsequence is "ace" and its length is 3.
 *
 * Example 2:
 * Input: text1 = "abc", text2 = "abc"
 * Output: 3
 * Explanation: The longest common subsequence is "abc" and its length is 3.
 *
 * Example 3:
 * Input: text1 = "abc", text2 = "def"
 * Output: 0
 * Explanation: There is no such common subsequence, so the result is 0.
 *
 * Constraints:
 *     ∙ 1 <= text1.length, text2.length <= 1000
 *     ∙ text1 and text2 consist of only lowercase English characters.
 */
#include <iostream>
#include <vector>
#include <algorithm>

class Solution {
public:
    int longestCommonSubsequence(std::string text1, std::string text2) {
        indices1 = std::vector<vector<int>>(26, std::vector<int>());
        for (int i = 0; i < text1.size(); ++i) {
            indices1[text1[i]-'a'].push_back(i);
        }

        n1 = text1.size();
        n2 = text2.size();
        cache = std::vector<vector<int>>(n1, std::vector<int>(n2, -1));
        this->text2 = text2;

        return dp(0, 0);
    }
private:
    std::vector<vector<int>> indices1;
    std::vector<vector<int>> cache;
    std::string text2;
    int n1, n2;


    int dp(int i1, int i2) {
        if (i1 >= n1 || i2 >= n2) {
            return 0;
        }

        if (cache[i1][i2] > -1) {
            return cache[i1][i2];
        }

        int ret = 0;
        char c = text2[i2];
        auto it = std::lower_bound(
            indices1[c-'a'].begin(), indices1[c-'a'].end(), i1
        );
        int j = it - indices1[c-'a'].begin();

        if (j < indices1[c-'a'].size()) {
            ret = dp(indices1[c-'a'][j] + 1, i2 + 1) + 1;
        }

        ret = std::max(ret, dp(i1, i2 + 1));
        cache[i1][i2] = ret;

        return ret;
    }
};
