"""
 * 1980. Find Unique Binary String [Medium]
 * Given an array of strings nums containing n unique binary strings each of
 * length n, return a binary string of length n that does not appear in nums.
 * If there are multiple answers, you may return any of them.
 *
 * Example 1:
 * Input: nums = ["01","10"]
 * Output: "11"
 * Explanation: "11" does not appear in nums. "00" would also be correct.
 *
 * Example 2:
 * Input: nums = ["00","01"]
 * Output: "11"
 * Explanation: "11" does not appear in nums. "10" would also be correct.
 *
 * Example 3:
 * Input: nums = ["111","011","001"]
 * Output: "101"
 * Explanation: "101" does not appear in nums. "000", "010", "100", and "110"
 * would also be correct.
 *
 * Constraints:
 *     ∙ n == nums.length
 *     ∙ 1 <= n <= 16
 *     ∙ nums[i].length == n
 *     ∙ nums[i] is either '0' or '1'.
 *     ∙ All the strings of nums are unique.
"""
from typing import List

class Solution:
    def findDifferentBinaryString(self, nums: List[str]) -> str:
        nums = set(nums)
        n = len(nums)
        for x in range(2**n):
            s = f"{x:0{n}b}"
            if s not in nums:
                return s


if __name__ == "__main__":
    tests = (
        (["01","10"], None),
        (["00","01"], None),
        (["111","011","001"], None),
    )
    for nums, expected in tests:
        ret = Solution().findDifferentBinaryString(nums)
        if isinstance(ret, str) and len(ret) == len(nums):
            if set(ret).issubset({"1", "0"}):
                print(ret not in nums)
                continue

        print(False)
