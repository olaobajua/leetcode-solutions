"""
2611. Mice and Cheese [Medium]
There are two mice and n different types of cheese, each type of cheese
should be eaten by exactly one mouse.

A point of the cheese with index i (0-indexed) is:
    ∙ reward1[i] if the first mouse eats it.
    ∙ reward2[i] if the second mouse eats it.

You are given a positive integer array reward1, a positive integer array
reward2, and a non-negative integer k.

Return the maximum points the mice can achieve if the first mouse eats
exactly k types of cheese.

Example 1:
Input: reward1 = [1,1,3,4], reward2 = [4,4,1,1], k = 2
Output: 15
Explanation: In this example, the first mouse eats the 2ⁿᵈ (0-indexed) and
the 3ʳᵈ types of cheese, and the second mouse eats the 0ᵗʰ and the 1ˢᵗ types
of cheese.
The total points are 4 + 4 + 3 + 4 = 15.
It can be proven that 15 is the maximum total points that the mice can
achieve.

Example 2:
Input: reward1 = [1,1], reward2 = [1,1], k = 2
Output: 2
Explanation: In this example, the first mouse eats the 0ᵗʰ (0-indexed) and
1ˢᵗ types of cheese, and the second mouse does not eat any cheese.
The total points are 1 + 1 = 2.
It can be proven that 2 is the maximum total points that the mice can
achieve.

Constraints:
    ∙ 1 <= n == reward1.length == reward2.length <= 10⁵
    ∙ 1 <= reward1[i], reward2[i] <= 1000
    ∙ 0 <= k <= n
"""
from operator import itemgetter, sub
from typing import List

class Solution:
    def miceAndCheese(self, reward1: List[int], reward2: List[int], k: int) -> int:
        diff = sorted(enumerate(map(sub, reward2, reward1)), key=itemgetter(1))
        ret = 0
        for i, _ in diff[:k]:
            ret += reward1[i]
            reward2[i] = 0
        return ret + sum(reward2)

if __name__ == "__main__":
    tests = (
        ([1,1,3,4], [4,4,1,1], 2, 15),
        ([1,1], [1,1], 2, 2),
        ([3,3], [3,5], 1, 8),
        ([1,4,4,6,4], [6,5,3,6,1], 1, 24),
        ([1,2,1,2,1,2], [2,1,1,2,2,1], 0, 9),
    )
    for reward1, reward2, k, expected in tests:
        print(Solution().miceAndCheese(reward1, reward2, k) == expected)
