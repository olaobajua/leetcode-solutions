/**
 * 1041. Robot Bounded In Circle (Medium)
 * On an infinite plane, a robot initially stands at (0, 0) and faces north.
 * The robot can receive one of three instructions:
 *     ∙ "G": go straight 1 unit;
 *     ∙ "L": turn 90 degrees to the left;
 *     ∙ "R": turn 90 degrees to the right.
 *
 * The robot performs the instructions given in order, and repeats them forever.
 *
 * Return true if and only if there exists a circle in the plane such that the
 * robot never leaves the circle.
 *
 * Example 1:
 * Input: instructions = "GGLLGG"
 * Output: true
 * Explanation: The robot moves from (0,0) to (0,2), turns 180 degrees, and
 * then returns to (0,0). When repeating these instructions, the robot remains
 * in the circle of radius 2 centered at the origin.
 *
 * Example 2:
 * Input: instructions = "GG"
 * Output: false
 * Explanation: The robot moves north indefinitely.
 *
 * Example 3:
 * Input: instructions = "GL"
 * Output: true
 * Explanation: The robot moves from (0, 0) -> (0, 1) -> (-1, 1) -> (-1, 0) ->
 * (0, 0) -> ...
 *
 * Constraints:
 *     ∙ 1 <= instructions.length <= 100
 *     ∙ instructions[i] is 'G', 'L' or, 'R'.
 */

/**
 * @param {string} instructions
 * @return {boolean}
 */
var isRobotBounded = function(instructions) {
    function move() {
        [x, y] = position;
        [dx, dy] = DIRECTIONS[direction];
        x += dx;
        y += dy;
        position = [x, y];
    }

    function rotate(d) {
        if (d == 'L') {
            switch (direction) {
                case 'N': direction = 'W'; break;
                case 'W': direction = 'S'; break;
                case 'S': direction = 'E'; break;
                case 'E': direction = 'N'; break;
            }
        } else {
            switch (direction) {
                case 'N': direction = 'E'; break;
                case 'E': direction = 'S'; break;
                case 'S': direction = 'W'; break;
                case 'W': direction = 'N'; break;
            }
        }
    }

    DIRECTIONS = {'N': [0, 1],
                  'W': [-1, 0],
                  'S': [0, -1],
                  'E': [1, 0]};
    position = [0, 0];
    direction = 'N';
    for (let j = 0; j < 4; ++j) {
        for (const i of instructions) {
            if (i == 'G') {
                move();
            } else {
                rotate(i);
            }
        }
        if (position[0] == 0 && position[1] == 0) {
            return true;
        }
    }
    return false;
};

const tests = [
    ["GGLLGG", true],
    ["GG", false],
    ["GL", true],
    ["GLGLGGLGL", false],
];

for (const [instructions, expected] of tests) {
    console.log(isRobotBounded(instructions) == expected);
}
