/**
 * 867. Transpose Matrix (Easy)
 * Given a 2D integer array matrix, return the transpose of matrix.
 *
 * The transpose of a matrix is the matrix flipped over its main diagonal,
 * switching the matrix's row and column indices.
 *
 * Example 1:
 * Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
 * Output: [[1,4,7],[2,5,8],[3,6,9]]
 *
 * Example 2:
 * Input: matrix = [[1,2,3],[4,5,6]]
 * Output: [[1,4],[2,5],[3,6]]
 *
 * Constraints:
 *     ∙ m == matrix.length
 *     ∙ n == matrix[i].length
 *     ∙ 1 <= m, n <= 1000
 *     ∙ 1 <= m * n <= 10⁵
 *     ∙ -10⁹ <= matrix[i][j] <= 10⁹
 */

/**
 * @param {number[][]} matrix
 * @return {number[][]}
 */
var transpose = function(matrix) {
    return [...zip(...matrix)]
};

function zip(...arrays) {
    let minLength = Math.min(...arrays.map(arr => arr.length));
    return [...Array(minLength)].map((_, i) => arrays.map(row => row[i]));
}

const tests = [
    [[[1,2,3],[4,5,6],[7,8,9]], [[1,4,7],[2,5,8],[3,6,9]]],
    [[[1,2,3],[4,5,6]], [[1,4],[2,5],[3,6]]],
];

for (const [matrix, expected] of tests) {
    console.log(transpose(matrix).every((r, i) => r.toString() ==
                expected[i].toString()));
}
