"""
 * 123. Best Time to Buy and Sell Stock III (Hard)
 * You are given an array prices where prices[i] is the price of a given stock
 * on the iᵗʰ day.
 *
 * Find the maximum profit you can achieve. You may complete at most two
 * transactions.
 *
 * Note: You may not engage in multiple transactions simultaneously
 * (i.e., you must sell the stock before you buy again).
 *
 * Example 1:
 * Input: prices = [3,3,5,0,0,3,1,4]
 * Output: 6
 * Explanation: Buy on day 4 (price = 0) and sell on day 6 (price = 3),
 * profit = 3-0 = 3.
 * Then buy on day 7 (price = 1) and sell on day 8 (price = 4),
 * profit = 4-1 = 3.
 *
 * Example 2:
 * Input: prices = [1,2,3,4,5]
 * Output: 4
 * Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5),
 * profit = 5-1 = 4.
 * Note that you cannot buy on day 1, buy on day 2 and sell them later,
 * as you are engaging multiple transactions at the same time.
 * You must sell before buying again.
 *
 * Example 3:
 * Input: prices = [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transaction is done, i.e. max profit = 0.
 *
 * Example 4:
 * Input: prices = [1]
 * Output: 0
 *
 * Constraints:
 *     1 <= prices.length <= 10⁵
 *     0 <= prices[i] <= 10⁵
"""
from typing import List

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        min_price = prices[0]
        max_before = [0] * len(prices)
        for i, price in enumerate(prices):
            min_price = min(min_price, price)
            max_before[i] = max(max_before[i-1], price - min_price)

        max_price = prices[-1]
        max_after = [0] * len(prices)
        for i, price in enumerate(reversed(prices)):
            max_price = max(max_price, price)
            max_after[i] = max(max_after[i-1], max_price - price)

        return max(ma + mb for mb, ma in zip(max_before, reversed(max_after)))

if __name__ == "__main__":
    tests = (
        ([3,3,5,0,0,3,1,4], 6),
        ([1,2,3,4,5], 4),
        ([7,6,4,3,1], 0),
        ([1], 0),
        ([14,9,10,12,4,8,1,16], 19),
        ([1,2,4,2,5,7,2,4,9,0], 13),
        ([4,0,1,0,0,0,6,1,4], 9),
        ([8,3,6,2,8,8,8,4,2,0,7,2,9,4,9], 15),
    )
    for prices, expected in tests:
        print(Solution().maxProfit(prices) == expected)
