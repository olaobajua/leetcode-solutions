/**
 * 461. Hamming Distance (Easy)
 * The Hamming distance between two integers is the number of positions at
 * which the corresponding bits are different.
 *
 * Given two integers x and y, return the Hamming distance between them.
 *
 * Example 1:
 * Input: x = 1, y = 4
 * Output: 2
 * Explanation:
 * 1   (0 0 0 1)
 * 4   (0 1 0 0)
 *        ↑   ↑
 * The above arrows point to positions where the corresponding bits are
 * different.
 *
 * Example 2:
 * Input: x = 3, y = 1
 * Output: 1
 *
 * Constraints:
 *     0 <= x, y <= 2³¹ - 1
 */

/**
 * @param {number} x
 * @param {number} y
 * @return {number}
 */
var hammingDistance = function(x, y) {
    let z = x ^ y;
    let counter = z & 1
    while (z >>= 1) {
        counter += z & 1;
    }
    return counter;
};

const tests = [
    [1, 4, 2],
    [3, 1, 1],
    [93, 73, 2],
];

for (const [x, y, expected] of tests) {
    console.log(hammingDistance(x, y) == expected);
}
