"""
 * 1721. Swapping Nodes in a Linked List (Medium)
 * You are given the head of a linked list, and an integer k.
 *
 * Return the head of the linked list after swapping the values of the kth node
 * from the beginning and the kth node from the end (the list is 1-indexed).
 *
 * Example 1:
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [1,4,3,2,5]
 *
 * Example 2:
 * Input: head = [7,9,6,6,7,8,3,0,9,5], k = 5
 * Output: [7,9,6,6,8,7,3,0,9,5]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is n.
 *     ∙ 1 <= k <= n <= 10⁵
 *     ∙ 0 <= Node.val <= 100
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def swapNodes(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        n = 1
        cur = head
        while (cur := cur.next):
            n += 1
        first = last = head
        for _ in range(k - 1):
            first = first.next
        for _ in range(n - k):
            last = last.next
        first.val, last.val = last.val, first.val
        return head

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5], 2, [1,4,3,2,5]),
        ([7,9,6,6,7,8,3,0,9,5], 5, [7,9,6,6,8,7,3,0,9,5]),
    )
    for nums, k, expected in tests:
        ret = Solution().swapNodes(list_to_linked_list(nums), k)
        print(linked_list_to_list(ret) == expected)
