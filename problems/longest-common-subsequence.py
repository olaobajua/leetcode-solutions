"""
 * 1143. Longest Common Subsequence [Medium]
 * Given two strings text1 and text2, return the length of their longest
 * common subsequence. If there is no common subsequence, return 0.
 *
 * A subsequence of a string is a new string generated from the original
 * string with some characters (can be none) deleted without changing the
 * relative order of the remaining characters.
 *     ∙ For example, "ace" is a subsequence of "abcde".
 *
 * A common subsequence of two strings is a subsequence that is common to both
 * strings.
 *
 * Example 1:
 * Input: text1 = "abcde", text2 = "ace"
 * Output: 3
 * Explanation: The longest common subsequence is "ace" and its length is 3.
 *
 * Example 2:
 * Input: text1 = "abc", text2 = "abc"
 * Output: 3
 * Explanation: The longest common subsequence is "abc" and its length is 3.
 *
 * Example 3:
 * Input: text1 = "abc", text2 = "def"
 * Output: 0
 * Explanation: There is no such common subsequence, so the result is 0.
 *
 * Constraints:
 *     ∙ 1 <= text1.length, text2.length <= 1000
 *     ∙ text1 and text2 consist of only lowercase English characters.
"""
from bisect import bisect_left
from collections import defaultdict
from functools import cache

class Solution:
    def longestCommonSubsequence(self, text1: str, text2: str) -> int:
        @cache
        def dp(i1, i2):
            if i1 >= n1 or i2 >= n2:
                return 0
            ret = 0
            c = text2[i2]
            j = bisect_left(indices1[c], i1)
            if j < len(indices1[c]):
                ret = dp(indices1[c][j] + 1, i2 + 1) + 1
            return max(ret, dp(i1, i2 + 1))

        indices1 = defaultdict(list)
        for i, c in enumerate(text1):
            indices1[c].append(i)
        n1 = len(text1)
        n2 = len(text2)
        return dp(0, 0)

if __name__ == "__main__":
    tests = (
        ("abcde", "ace", 3),
        ("abc", "abc", 3),
        ("abc", "def", 0),
        ("bl", "yby", 1),
        ("oxcpqrsvwf", "shmtulqrypy", 2),
    )
    for text1, text2, expected in tests:
        print(Solution().longestCommonSubsequence(text1, text2) == expected)
