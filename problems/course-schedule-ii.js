/**
 * 210. Course Schedule II (Medium)
 * There are a total of numCourses courses you have to take, labeled from 0 to
 * numCourses - 1. You are given an array prerequisites where
 * prerequisites[i] = [aᵢ, bᵢ] indicates that you must take course bᵢ first if
 * you want to take course aᵢ.
 *     ∙ For example, the pair [0, 1], indicates that to take course 0 you have
 *       to first take course 1.
 *
 * Return the ordering of courses you should take to finish all courses. If
 * there are many valid answers, return any of them. If it is impossible to
 * finish all courses, return an empty array.
 *
 * Example 1:
 * Input: numCourses = 2, prerequisites = [[1,0]]
 * Output: [0,1]
 * Explanation: There are a total of 2 courses to take. To take course 1 you
 * should have finished course 0. So the correct course order is [0,1].
 *
 * Example 2:
 * Input: numCourses = 4, prerequisites = [[1,0],[2,0],[3,1],[3,2]]
 * Output: [0,2,1,3]
 * Explanation: There are a total of 4 courses to take. To take course 3 you
 * should have finished both courses 1 and 2. Both courses 1 and 2 should be
 * taken after you finished course 0.
 * So one correct course order is [0,1,2,3]. Another correct ordering is
 * [0,2,1,3].
 *
 * Example 3:
 * Input: numCourses = 1, prerequisites = []
 * Output: [0]
 *
 * Constraints:
 *     1 <= numCourses <= 2000
 *     0 <= prerequisites.length <= numCourses * (numCourses - 1)
 *     prerequisites[i].length == 2
 *     0 <= aᵢ, bᵢ < numCourses
 *     aᵢ != bᵢ
 *     All the pairs [aᵢ, bᵢ] are distinct.
 */

/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {number[]}
 */
var findOrder = function(numCourses, prerequisites) {
    const dependsOn = {};
    for (const [c1, c2] of prerequisites) {
        dependsOn[c1] ? dependsOn[c1].add(c2) : dependsOn[c1] = new Set([c2]);
    }

    const visited = Array(numCourses).fill(false);
    const ret = [];
    for (let i = 0; i < numCourses; ++i) {
        if (!visited[i]) {
            if (!topologicalSortDfs(dependsOn, i, visited, ret)) {
                return [];
            }
        }
    }

    return ret;
};

function topologicalSortDfs(graph, cur, visited, ret) {
    // https://en.wikipedia.org/wiki/Topological_sorting#Depth-first_search
    if (visited[cur] == 'temporary') {
        return false;   // directed cycle graph, can't be topologically sorted
    } else if (!visited[cur]) {
        visited[cur] = 'temporary';
        for (const course of graph[cur] || []) {
            if (!topologicalSortDfs(graph, course, visited, ret)) {
                return false;
            }
        }
        visited[cur] = 'permanent';
        ret.push(cur);
    }
    return true;
}

const tests = [
    [2, [[1,0]], [0,1]],
    [4, [[1,0],[2,0],[3,1],[3,2]], [0,2,1,3]],
    [1, [], [0]],
    [2, [[0,1],[1,0]], []],
];
for (const [numCourses, prerequisites, expected] of tests) {
    test: {
        const r = findOrder(numCourses, prerequisites);
        if (r.toString() == expected.toString()) {
            console.log(true);
        } else if (r.slice().sort().toString() !=
                [...Array(numCourses)].map((x, i) => i).toString()) {
            console.log(false);
        } else {
            for (const [c1, c2] of prerequisites) {
                if (r.indexOf(c1) < r.indexOf(c2)) {
                    console.log(false);
                    break test;
                }
            }
            console.log(true);
        }
    }
}
