"""
 * 127. Word Ladder (Hard)
 * A transformation sequence from word beginWord to word endWord using a
 * dictionary wordList is a sequence of words
 * beginWord -> s₁ -> s₂ -> ... -> sₖ such that:
 *     ∙ Every adjacent pair of words differs by a single letter.
 *     ∙ Every sᵢ for 1 <= i <= k is in wordList. Note that beginWord does not
 *       need to be in wordList.
 *     ∙ sₖ == endWord
 *
 * Given two words, beginWord and endWord, and a dictionary wordList, return
 * the number of words in the shortest transformation sequence from beginWord
 * to endWord, or 0 if no such sequence exists.
 *
 * Example 1:
 * Input: beginWord = "hit", endWord = "cog",
 *        wordList = ["hot","dot","dog","lot","log","cog"]
 * Output: 5
 * Explanation: One shortest transformation sequence is
 * "hit" -> "hot" -> "dot" -> "dog" -> cog", which is 5 words long.
 *
 * Example 2:
 * Input: beginWord = "hit", endWord = "cog",
 * wordList = ["hot","dot","dog","lot","log"]
 * Output: 0
 * Explanation: The endWord "cog" is not in wordList, therefore there is no
 * valid transformation sequence.
 *
 * Constraints:
 *     ∙ 1 <= beginWord.length <= 10
 *     ∙ endWord.length == beginWord.length
 *     ∙ 1 <= wordList.length <= 5000
 *     ∙ wordList[i].length == beginWord.length
 *     ∙ beginWord, endWord, and wordList[i] consist of lowercase English
 *       letters.
 *     ∙ beginWord != endWord
 *     ∙ All the words in wordList are unique.
"""
from typing import List

class Solution:
    def ladderLength(self, beginWord: str, endWord: str, wordList: List[str]) -> int:
        words = set(wordList)
        to_visit = [[beginWord, 1]]
        for word, steps in to_visit:
            if word == endWord:
                return steps
            for i in range(len(word)):
                for char in "abcdefghijklmnopqrstuvwxyz":
                    if (w := word[:i] + char + word[i+1:]) in words:
                        to_visit.append([w, steps + 1])
                        words.remove(w)
        return 0

if __name__ == "__main__":
    tests = (
        ("hit", "cog", ["hot","dot","dog","lot","log","cog"], 5),
        ("hit", "cog", ["hot","dot","dog","lot","log"], 0),
        ("a", "c", ["a","b","c"], 2),
        ("hot", "dog", ["hot","dog"], 0),
    )
    for beginWord, endWord, wordList, expected in tests:
        print(Solution().ladderLength(beginWord, endWord, wordList) == expected)
