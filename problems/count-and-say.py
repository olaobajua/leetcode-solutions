"""
 * 38. Count and Say [Medium]
 * The count-and-say sequence is a sequence of digit strings defined by the
 * recursive formula:
 *     ∙ countAndSay(1) = "1"
 *     ∙ countAndSay(n) is the way you would "say" the digit string from
 *       countAndSay(n-1), which is then converted into a different digit
 *       string.
 *
 * To determine how you "say" a digit string, split it into the minimal number
 * of substrings such that each substring contains exactly one unique digit.
 * Then for each substring, say the number of digits, then say the digit.
 * Finally, concatenate every said digit.
 *
 * For example, the saying and conversion for digit string "3322251":
 * Given a positive integer n, return the nᵗʰ term of the count-and-say
 * sequence.
 *
 * Example 1:
 * Input: n = 1
 * Output: "1"
 * Explanation: This is the base case.
 *
 * Example 2:
 * Input: n = 4
 * Output: "1211"
 * Explanation:
 * countAndSay(1) = "1"
 * countAndSay(2) = say "1" = one 1 = "11"
 * countAndSay(3) = say "11" = two 1's = "21"
 * countAndSay(4) = say "21" = one 2 + one 1 = "12" + "11" = "1211"
 *
 * Constraints:
 *     ∙ 1 <= n <= 30
"""
from itertools import groupby

class Solution:
    def countAndSay(self, n: int) -> str:
        if n == 1:
            return "1"
        return ''.join(f"{len([*g])}{k}" for k, g in groupby(self.countAndSay(n - 1)))

if __name__ == "__main__":
    tests = (
        (1, "1"),
        (2, "11"),
        (3, "21"),
        (4, "1211"),
        (5, "111221"),
        (6, "312211"),
        (7, "13112221"),
        (8, "1113213211"),
        (9, "31131211131221"),
        (10,"13211311123113112211"),
        (11,"11131221133112132113212221"),
        (12,"3113112221232112111312211312113211"),
        (13,"1321132132111213122112311311222113111221131221"),
        (14,"11131221131211131231121113112221121321132132211331222113112211"),
        (15,"311311222113111231131112132112311321322112111312211312111322212311322113212221"),
    )
    for n, expected in tests:
        print(Solution().countAndSay(n) == expected)
