/**
 * 931. Minimum Falling Path Sum [Medium]
 * Given an n x n array of integers matrix, return the minimum sum of any
 * falling path through matrix.
 *
 * A falling path starts at any element in the first row and chooses the
 * element in the next row that is either directly below or diagonally
 * left/right. Specifically, the next element from position (row, col) will be
 * (row + 1, col - 1), (row + 1, col), or (row + 1, col + 1).
 *
 * Example 1:
 * Input: matrix = [[2,1,3],
 *                  [6,5,4],
 *                  [7,8,9]]
 * Output: 13
 * Explanation: There are two falling paths with a minimum sum as shown.
 *
 * Example 2:
 * Input: matrix = [[-19,57],
 *                  [-40,-5]]
 * Output: -59
 * Explanation: The falling path with a minimum sum is shown.
 *
 * Constraints:
 *     ∙ n == matrix.length == matrix[i].length
 *     ∙ 1 <= n <= 100
 *     ∙ -100 <= matrix[i][j] <= 100
 */
#include <vector>
#include <algorithm>
#include <vector>

class Solution {
public:
    int minFallingPathSum(std::vector<std::vector<int>>& matrix) {
        n = matrix.size();
        memo = std::vector(n, std::vector(n, INT_MAX));
        this->matrix = &matrix;

        int ret = INT_MAX;
        for (int i = 0; i < n; ++i) {
            ret = std::min(ret, dfs(0, i));
        }

        return ret;
    }

private:
    std::vector<std::vector<int>> memo;
    std::vector<std::vector<int>> *matrix;
    int n;

    int dfs(int r, int c) {
        if (r == n) {
            return 0;
        }

        if (memo[r][c] < INT_MAX) {
            return memo[r][c];
        }

        int cur = (*matrix)[r][c];
        int ret = dfs(r + 1, c) + cur;
        if (c > 0) {
            ret = std::min(ret, dfs(r + 1, c - 1) + cur);
        }
        if (c < n - 1) {
            ret = std::min(ret, dfs(r + 1, c + 1) + cur);
        }

        memo[r][c] = ret;
        return ret;
    };
};
