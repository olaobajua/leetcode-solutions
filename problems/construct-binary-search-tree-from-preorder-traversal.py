"""
 * 1008. Construct Binary Search Tree from Preorder Traversal (Medium)
 * Given an array of integers preorder, which represents the preorder traversal
 * of a BST (i.e., binary search tree), construct the tree and return its root.
 *
 * It is guaranteed that there is always possible to find a binary search tree
 * with the given requirements for the given test cases.
 *
 * A binary search tree is a binary tree where for every node, any descendant
 * of Node.left has a value strictly less than Node.val, and any descendant of
 * Node.right has a value strictly greater than Node.val.
 *
 * A preorder traversal of a binary tree displays the value of the node first,
 * then traverses Node.left, then traverses Node.right.
 *
 * Example 1:
 * Input: preorder = [8,5,1,7,10,12]
 * Output: [8,5,10,1,7,null,12]
 *
 * Example 2:
 * Input: preorder = [1,3]
 * Output: [1,null,3]
 *
 * Constraints:
 *     1 <= preorder.length <= 100
 *     1 <= preorder[i] <= 10⁸
 *     All the values of preorder are unique.
"""
from math import inf
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def bstFromPreorder(self, preorder: List[int]) -> Optional[TreeNode]:
        root = prev = TreeNode(preorder.pop(0))
        nodes = [prev]
        while preorder:
            val = preorder.pop(0)
            if val < prev.val:
                prev.left = TreeNode(val)
                prev = prev.left
                nodes.append(prev)
            else:
                while nodes and nodes[-1].val < val:
                    prev = nodes.pop()
                prev.right = TreeNode(val)
                prev = prev.right
                nodes.append(prev)
        return root

def tree_to_list(root):
    nodes = [root]
    for n in nodes:
        if n:
            yield n.val
            nodes.extend([n.left, n.right])

if __name__ == "__main__":
    tests = (
        ([8,5,1,7,10,12], [8,5,10,1,7,None,12]),
        ([1,3], [1,None,3]),
    )
    for preorder, expected in tests:
        print([*tree_to_list(Solution().bstFromPreorder(preorder))] ==
              [*filter(None, expected)])

