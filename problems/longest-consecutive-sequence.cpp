/**
 * 128. Longest Consecutive Sequence (Medium)
 * Given an unsorted array of integers nums, return the length of the longest
 * consecutive elements sequence.
 *
 * You must write an algorithm that runs in O(n) time.
 *
 * Example 1:
 * Input: nums = [100,4,200,1,3,2]
 * Output: 4
 * Explanation: The longest consecutive elements sequence is [1, 2, 3, 4].
 * Therefore its length is 4.
 *
 * Example 2:
 * Input: nums = [0,3,7,2,5,8,4,6,0,1]
 * Output: 9
 *
 * Constraints:
 *     ∙ 0 <= nums.length <= 10⁵
 *     ∙ -10⁹ <= nums[i] <= 10⁹
 */
class Solution {
private:
    unordered_set<int> nums;
    unordered_map<int, int> cache;
public:
    int longestConsecutive(vector<int>& nums) {
        if (!nums.size()) {
            return 0;
        }
        this->nums = unordered_set(nums.begin(), nums.end());
        this->cache = unordered_map<int, int>(this->nums.size());
        int ret = 1;
        for (int i = 0; i < nums.size(); ++i) {
            ret = max(ret, dp(nums[i]));
        }
        return ret;
    }
    int dp(int x) {
        if (cache.find(x) == cache.end()) {
            cache[x] = 1;
            if (nums.find(x + 1) != nums.end()) {
                cache[x] += dp(x + 1);
            }
        }
        return cache[x];
    }
};
