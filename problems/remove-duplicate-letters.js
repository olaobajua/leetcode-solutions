/**
 * 316. Remove Duplicate Letters (Medium)
 * Given a string s, remove duplicate letters so that every letter appears once
 * and only once. You must make sure your result is the smallest in
 * lexicographical order among all possible results.
 *
 * Example 1:
 * Input: s = "bcabc"
 * Output: "abc"
 *
 * Example 2:
 * Input: s = "cbacdcbc"
 * Output: "acdb"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁴
 *     ∙ s consists of lowercase English letters.
 *
 * Note: This question is the same as 1081:
 * https://leetcode.com/problems/smallest-subsequence-of-distinct-characters/
 */

/**
 * @param {string} s
 * @return {string}
 */
var removeDuplicateLetters = function(s) {
    const lastOcc = {};
    s = Array.from(s);
    s.forEach((char, i) => {
        lastOcc[char] = i;
    });
    const stack = ['!'];
    const visited = new Set();
    for (const [i, l] of s.entries()) {
        if (visited.has(l)) {
            continue;
        }
        while (l < stack[stack.length-1] && lastOcc[stack[stack.length-1]] > i) {
            visited.delete(stack.pop());
        }
        stack.push(l);
        visited.add(l);
    }
    return stack.slice(1).join('');
};

const tests = [
    ["bcabc", "abc"],
    ["cbacdcbc", "acdb"],
    ["leetcode", "letcod"],
];

for (const [s, expected] of tests) {
    console.log(removeDuplicateLetters(s) == expected);
}
