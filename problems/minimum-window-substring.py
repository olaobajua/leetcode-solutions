"""
 * 76. Minimum Window Substring [Hard]
 * Given two strings s and t of lengths m and n respectively, return the
 * minimum window substring of s such that every character in t (including
 * duplicates) is included in the window. If there is no such substring, return
 * the empty string "".
 *
 * The testcases will be generated such that the answer is unique.
 *
 * Example 1:
 * Input: s = "ADOBECODEBANC", t = "ABC"
 * Output: "BANC"
 * Explanation: The minimum window substring "BANC" includes 'A', 'B', and 'C'
 * from string t.
 *
 * Example 2:
 * Input: s = "a", t = "a"
 * Output: "a"
 * Explanation: The entire string s is the minimum window.
 *
 * Example 3:
 * Input: s = "a", t = "aa"
 * Output: ""
 * Explanation: Both 'a's from t must be included in the window.
 * Since the largest window of s only has one 'a', return empty string.
 *
 * Constraints:
 *     ∙ m == s.length
 *     ∙ n == t.length
 *     ∙ 1 <= m, n <= 10⁵
 *     ∙ s and t consist of uppercase and lowercase English letters.
 *
 * Follow up: Could you find an algorithm that runs in O(m + n) time?
"""
from collections import Counter

class Solution:
    def minWindow(self, s: str, t: str) -> str:
        count_t = Counter(t)
        count_s = Counter()
        left = len(t)
        start = 0
        ret_start = 0
        ret_end = len(s) + 1

        for end, c in enumerate(s):
            if count_s[c] < count_t[c]:
                left -= 1
            count_s[c] += 1

            while left == 0:
                if ret_end - ret_start > end - start:
                    ret_start = start
                    ret_end = end
                c = s[start]
                count_s[c] -= 1
                if count_s[c] < count_t[c]:
                    left += 1
                start += 1

        return "" if ret_end - ret_start > len(s) else s[ret_start:ret_end+1]

if __name__ == "__main__":
    tests = (
        ("ADOBECODEBANC", "ABC", "BANC"),
        ("a", "a", "a"),
        ("a", "aa", ""),
    )
    for s, t, expected in tests:
        print(Solution().minWindow(s, t) == expected)
