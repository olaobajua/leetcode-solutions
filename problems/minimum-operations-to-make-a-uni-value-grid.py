"""
 * 2033. Minimum Operations to Make a Uni-Value Grid (Medium)
 * You are given a 2D integer grid of size m x n and an integer x. In one
 * operation, you can add x to or subtract x from any element in the grid.
 *
 * A uni-value grid is a grid where all the elements of it are equal.
 *
 * Return the minimum number of operations to make the grid uni-value.
 * If it is not possible, return -1.
 *
 * Example 1:
 * Input: grid = [[2,4],[6,8]], x = 2
 * Output: 4
 * Explanation: We can make every element equal to 4 by doing the following: 
 * - Add x to 2 once.
 * - Subtract x from 6 once.
 * - Subtract x from 8 twice.
 * A total of 4 operations were used.
 *
 * Example 2:
 * Input: grid = [[1,5],[2,3]], x = 1
 * Output: 5
 * Explanation: We can make every element equal to 3.
 *
 * Example 3:
 * Input: grid = [[1,2],[3,4]], x = 2
 * Output: -1
 * Explanation: It is impossible to make every element equal.
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m, n <= 10⁵
 *     1 <= m * n <= 10⁵
 *     1 <= x, grid[i][j] <= 10⁴
"""
from itertools import chain
from typing import List

class Solution:
    def minOperations(self, grid: List[List[int]], x: int) -> int:
        grid = sorted(chain(*grid))
        mid = grid[len(grid) // 2]
        if any(abs(mid - n) % x for n in grid):
            return -1
        return sum(abs(mid - n) // x for n in grid)

if __name__ == "__main__":
    tests = (
        ([[2,4],[6,8]], 2, 4),
        ([[1,5],[2,3]], 1, 5),
        ([[1,2],[3,4]], 2, -1),
        ([[980,476,644,56],[644,140,812,308],[812,812,896,560],[728,476,56,812]], 84, 45),
        ([[596,904,960,232,120,932,176],[372,792,288,848,960,960,764],[652,92,904,120,680,904,120],[372,960,92,680,876,624,904],[176,652,64,344,316,764,316],[820,624,848,596,960,960,372],[708,120,456,92,484,932,540]], 28, 473),
    )
    for grid, x, expected in tests:
        print(Solution().minOperations(grid, x) == expected)
