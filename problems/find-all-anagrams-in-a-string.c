/**
 * 438. Find All Anagrams in a String (Medium)
 * Given two strings s and p, return an array of all the start indices of p's
 * anagrams in s. You may return the answer in any order.
 *
 * An Anagram is a word or phrase formed by rearranging the letters of a
 * different word or phrase, typically using all the original letters exactly
 * once.
 *
 * Example 1:
 * Input: s = "cbaebabacd", p = "abc"
 * Output: [0,6]
 * Explanation:
 * The substring with start index = 0 is "cba", which is an anagram of "abc".
 * The substring with start index = 6 is "bac", which is an anagram of "abc".
 *
 * Example 2:
 * Input: s = "abab", p = "ab"
 * Output: [0,1,2]
 * Explanation:
 * The substring with start index = 0 is "ab", which is an anagram of "ab".
 * The substring with start index = 1 is "ba", which is an anagram of "ab".
 * The substring with start index = 2 is "ab", which is an anagram of "ab".
 *
 * Constraints:
 *     ∙ 1 <= s.length, p.length <= 3 * 10⁴
 *     ∙ s and p consist of lowercase English letters.
 */
int* findAnagrams(char *s, char *p, int *returnSize) {
    static int ret[30000];
    int sc[26] = {0};
    int pc[26] = {0};
    int m = strlen(p), n = strlen(s);
    for (int i = 0; i < m; ++i) {
        ++pc[p[i]-'a'];
    }
    for (int i = 0; i < fmin(m, n); ++i) {
        ++sc[s[i]-'a'];
    }
    *returnSize = 0;
    if (memcmp(sc, pc, 26 * sizeof(int)) == 0) {
        ret[(*returnSize)++] = 0;
    }
    for (int i = 0; i < n - m; ++i) {
        --sc[s[i]-'a'];
        ++sc[s[i+m]-'a'];
        if (memcmp(sc, pc, 26 * sizeof(int)) == 0) {
            ret[(*returnSize)++] = i + 1;
        }
    }
    return ret;
}
