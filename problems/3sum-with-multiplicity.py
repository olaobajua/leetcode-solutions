"""
 * 923. 3Sum With Multiplicity (Medium)
 * Given an integer array arr, and an integer target, return the number of
 * tuples i, j, k such that i < j < k and arr[i] + arr[j] + arr[k] == target.
 *
 * As the answer can be very large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: arr = [1,1,2,2,3,3,4,4,5,5], target = 8
 * Output: 20
 * Explanation:
 * Enumerating by the values (arr[i], arr[j], arr[k]):
 * (1, 2, 5) occurs 8 times;
 * (1, 3, 4) occurs 8 times;
 * (2, 2, 4) occurs 2 times;
 * (2, 3, 3) occurs 2 times.
 *
 * Example 2:
 * Input: arr = [1,1,2,2,2,2], target = 5
 * Output: 12
 * Explanation:
 * arr[i] = 1, arr[j] = arr[k] = 2 occurs 12 times:
 * We choose one 1 from [1,1] in 2 ways,
 * and two 2s from [2,2,2,2] in 6 ways.
 *
 * Constraints:
 *     ∙ 3 <= arr.length <= 3000
 *     ∙ 0 <= arr[i] <= 100
 *     ∙ 0 <= target <= 300
"""
from collections import Counter
from itertools import combinations_with_replacement
from typing import List

class Solution:
    def threeSumMulti(self, arr: List[int], target: int) -> int:
        c = Counter(arr)
        ret = 0
        mod = int(1e9 + 7)
        for x, y in combinations_with_replacement(c, 2):
            z = target - x - y
            if x == y == z:
                ret = (ret + c[x] * (c[x] - 1) * (c[x] - 2) // 6) % mod
            elif x == y != z:
                ret = (ret + c[x] * (c[x] - 1) // 2 * c[z]) % mod
            elif z > x and z > y:
                ret = (ret + c[x] * c[y] * c[z]) % mod
        return ret

if __name__ == "__main__":
    tests = (
        ([1,1,2,2,3,3,4,4,5,5], 8, 20),
        ([1,1,2,2,2,2], 5, 12),
    )
    for arr, target, expected in tests:
        print(Solution().threeSumMulti(arr, target) == expected)
