"""
 * 115. Distinct Subsequences (Hard)
 * Given two strings s and t, return the number of distinct subsequences of s
 * which equals t.
 *
 * A string's subsequence is a new string formed from the original string by
 * deleting some (can be none) of the characters without disturbing the
 * remaining characters' relative positions. (i.e., "ACE" is a subsequence of
 * "ABCDE" while "AEC" is not).
 *
 * It is guaranteed the answer fits on a 32-bit signed integer.
 *
 * Example 1:
 * Input: s = "rabbbit", t = "rabbit"
 * Output: 3
 * Explanation:
 * As shown below, there are 3 ways you can generate "rabbit" from S.
 * rabbbit
 * rabbbit
 * rabbbit
 *
 * Example 2:
 * Input: s = "babgbag", t = "bag"
 * Output: 5
 * Explanation:
 * As shown below, there are 5 ways you can generate "bag" from S.
 * babgbag
 * babgbag
 * babgbag
 * babgbag
 * babgbag
 *
 * Constraints:
 *     1 <= s.length, t.length <= 1000
 *     s and t consist of English letters.
"""
class Solution:
    def numDistinct(self, s: str, t: str) -> int:
        n = len(s)
        m = len(t)
        subs = [[0] * m + [1] for _ in range(n + 1)]
        for i in range(n):
            for j in range(m):
                subs[i][j] = subs[i-1][j] + subs[i-1][j-1] * (s[i] == t[j])

        print([*zip(*subs)])
        return subs[n-1][m-1]

if __name__ == "__main__":
    tests = (
        ("aabb", "ac", 0),  # 1 2 2 2 0
                            # 0 0 0 0 0
                            # 1 1 1 1 1

        ("aabb", "ab", 4),  # 1 2 2 2 0
                            # 0 0 2 4 0
                            # 1 1 1 1 1

        ("aabb", "abb", 2), # 1 2 2 2 0
                            # 0 0 2 4 0
                            # 0 0 0 2 0
                            # 1 1 1 1 1

        ("aabb", "b", 2),   # 0 0 1 2 0
                            # 1 1 1 1 1
        ("aaabb", "abb", 3),
        ("aabbb", "abb", 6),
        ("aabbbb", "abb", 12),
        ("aaabba", "abba", 3),
        ("b", "b", 1),
        ("ddd", "dd", 3),
        ("eee", "eee", 1),
        ("aabb", "abb", 2),
        ("rabbbit", "rabbit", 3),
        ("raabbbit", "rabbit", 6),
        ("raaabbbit", "rabbit", 9),
        ("raaabbbbit", "rabbit", 18),
        ("rabbbbit", "rabbit", 6),
        ("rabbbbbit", "rabbit", 10),
        ("rabbbbbbit", "rabbit", 15),
        ("rabbbbbbbit", "rabbit", 21),
        ("rabbbbbbbit", "rabbbit", 35),
        ("babgbag", "bag", 5),
    )
    for s, t, expected in tests:
        print(Solution().numDistinct(s, t) == expected)
