"""
 * 2385. Amount of Time for Binary Tree to Be Infected [Medium]
 * You are given the root of a binary tree with unique values, and an integer
 * start. At minute 0, an infection starts from the node with value start.
 *
 * Each minute, a node becomes infected if:
 *     ∙ The node is currently uninfected.
 *     ∙ The node is adjacent to an infected node.
 *
 * Return the number of minutes needed for the entire tree to be infected.
 *
 * Example 1:
 *      1
 *   5     3
 *    4  10 6
 *   9 2
 * Input: root = [1,5,3,null,4,10,6,9,2], start = 3
 * Output: 4
 * Explanation: The following nodes are infected during:
 * - Minute 0: Node 3
 * - Minute 1: Nodes 1, 10 and 6
 * - Minute 2: Node 5
 * - Minute 3: Node 4
 * - Minute 4: Nodes 9 and 2
 * It takes 4 minutes for the whole tree to be infected so we return 4.
 *
 * Example 2:
 * Input: root = [1], start = 1
 * Output: 0
 * Explanation: At minute 0, the only node in the tree is infected so we
 * return 0.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁵].
 *     ∙ 1 <= Node.val <= 10⁵
 *     ∙ Each node has a unique value.
 *     ∙ A node with a value of start exists in the tree.
"""
from collections import defaultdict
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def amountOfTime(self, root: Optional[TreeNode], start: int) -> int:
        graph = defaultdict(list)
        to_visit = [root]
        for node in to_visit:
            if node.left:
                to_visit.append(node.left)
                graph[node.left.val].append(node.val)
                graph[node.val].append(node.left.val)
            if node.right:
                to_visit.append(node.right)
                graph[node.right.val].append(node.val)
                graph[node.val].append(node.right.val)

        seen = {start}
        to_visit = [(start, 0)]
        ret = 0
        for node, steps in to_visit:
            ret = max(ret, steps)
            for n in graph[node]:
                if n not in seen:
                    to_visit.append([n, steps + 1])
                    seen.add(n)

        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,5,3,None,4,10,6,9,2], 3, 4),
        ([1], 1, 0),
    )
    for nums, start, expected in tests:
        print(Solution().amountOfTime(build_tree(nums), start) == expected)
