/**
 * 1286. Iterator for Combination (Medium)
 * Design the CombinationIterator class:
 *     ∙ CombinationIterator(string characters, int combinationLength)
 *       Initializes the object with a string characters of sorted distinct
 *       lowercase English letters and a number combinationLength as arguments.
 *     ∙ next() Returns the next combination of length combinationLength in
 *       lexicographical order.
 *     ∙ hasNext() Returns true if and only if there exists a next combination.
 *
 * Example 1:
 * Input
 * ["CombinationIterator", "next", "hasNext", "next", "hasNext", "next",
 *  "hasNext"]
 * [["abc", 2], [], [], [], [], [], []]
 * Output
 * [null, "ab", true, "ac", true, "bc", false]
 *
 * Explanation
 * CombinationIterator itr = new CombinationIterator("abc", 2);
 * itr.next();    // return "ab"
 * itr.hasNext(); // return True
 * itr.next();    // return "ac"
 * itr.hasNext(); // return True
 * itr.next();    // return "bc"
 * itr.hasNext(); // return False
 *
 * Constraints:
 *     1 <= combinationLength <= characters.length <= 15
 *     All the characters of characters are unique.
 *     At most 10⁴ calls will be made to next and hasNext.
 *     It's guaranteed that all calls of the function next are valid.
 */

/**
 * @param {string} characters
 * @param {number} combinationLength
 */
var CombinationIterator = function(characters, combinationLength) {
    this.__chars = combinations(characters, combinationLength);
    this.__next = null;
};

/**
 * @return {string}
 */
CombinationIterator.prototype.next = function() {
    const n = this.__next;
    this.__next = null;
    return (n || this.__chars.next()['value']).join('');
};

/**
 * @return {boolean}
 */
CombinationIterator.prototype.hasNext = function() {
    if (this.__next === null) {
        const { value, done } = this.__chars.next();
        this.__next = done ? null : value;
    }
    return this.__next !== null;
};

function* combinations(iter, r=iter.length, prefix='') {
    if (prefix.length < r) {
        for (let i = 0; i < iter.length; ++i) {
            yield* combinations(iter.slice(i + 1), r, [...prefix, iter[i]]);
        }
    } else {
        yield prefix;
    }
}


var obj = new CombinationIterator('abc', 2);
console.log(obj.next() == 'ab');
console.log(obj.hasNext() == true);
console.log(obj.next() == 'ac');
console.log(obj.hasNext() == true);
console.log(obj.next() == 'bc');
console.log(obj.hasNext() == false);

var obj = new CombinationIterator('chp', 1);
console.log(obj.hasNext() == true);
console.log(obj.next() == 'c');
console.log(obj.hasNext() == true);
console.log(obj.hasNext() == true);
console.log(obj.next() == 'h');
console.log(obj.next() == 'p');
console.log(obj.hasNext() == false);
console.log(obj.hasNext() == false);
console.log(obj.hasNext() == false);
console.log(obj.hasNext() == false);
