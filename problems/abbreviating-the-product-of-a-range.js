/**
 * 2117. Abbreviating the Product of a Range (Hard)
 * You are given two positive integers left and right with left <= right.
 * Calculate the product of all integers in the inclusive range [left, right].
 *
 * Since the product may be very large, you will abbreviate it following these
 * steps:
 *     ∙ Count all trailing zeros in the product and remove them. Let us denote
 *       this count as C.
 *     ∙ For example, there are 3 trailing zeros in 1000, and there are 0
 *       trailing zeros in 546.
 *     ∙ Denote the remaining number of digits in the product as d. If d > 10,
 *       then express the product as <pre>...<suf> where <pre> denotes the
 *       first 5 digits of the product, and <suf> denotes the last 5 digits of
 *       the product after removing all trailing zeros. If d <= 10, we keep it
 *       unchanged.
 *     ∙ For example, we express 1234567654321 as 12345...54321, but 1234567 is
 *       represented as 1234567.
 *     ∙ Finally, represent the product as a string "<pre>...<suf>eC".
 *     ∙ For example, 12345678987600000 will be represented as
 *       "12345...89876e5".
 *
 * Return a string denoting the abbreviated product of all integers in the
 * inclusive range [left, right].
 *
 * Example 1:
 * Input: left = 1, right = 4
 * Output: "24e0"
 * Explanation:
 * The product is 1 × 2 × 3 × 4 = 24.
 * There are no trailing zeros, so 24 remains the same. The abbreviation will
 * end with "e0".
 * Since the number of digits is 2, which is less than 10, we do not have to
 * abbreviate it further.
 * Thus, the final representation is "24e0".
 *
 * Example 2:
 * Input: left = 2, right = 11
 * Output: "399168e2"
 * Explanation:
 * The product is 39916800.
 * There are 2 trailing zeros, which we remove to get 399168. The abbreviation
 * will end with "e2".
 * The number of digits after removing the trailing zeros is 6, so we do not
 * abbreviate it further.
 * Hence, the abbreviated product is "399168e2".
 *
 * Example 3:
 * Input: left = 999998, right = 1000000
 * Output: "99999...00002e6"
 * Explanation:
 * The above diagram shows how we abbreviate the product to "99999...00002e6".
 * - It has 6 trailing zeros. The abbreviation will end with "e6".
 * - The first 5 digits are 99999.
 * - The last 5 digits after removing trailing zeros is 00002.
 *
 * Constraints:
 *     1 <= left <= right <= 10⁶
 */

/**
 * @param {number} left
 * @param {number} right
 * @return {string}
 */
var abbreviateProduct = function(left, right) {
    let first = 0;
    let last = 1;
    let count2 = 0;
    let count5 = 0;
    for (let i = left; i <= right; ++i) {
        let cur = i;
        first += Math.log10(cur);
        while (cur % 2 === 0) {
            cur /= 2;
            count2 += 1;
        }
        while (cur % 5 === 0) {
            cur /= 5;
            count5 += 1;
        }
        last = last * cur % 100000;
    }
    const zeroes = Math.min(count2, count5);
    if (first - zeroes <= 10) {
        return `${Math.round(10**(first - zeroes))}e${zeroes}`;
    } else {
        last = BigInt(last) * 2n**BigInt(Math.abs(count2 - count5)) % 100000n;
        last = last.toString().padStart(5, '0');
        return `${Math.floor(10**(4 + first % 1))}...${last}e${zeroes}`;
    }};

const tests = [
    [15, 15, "15e0"],
    [1, 4, "24e0"],
    [2, 11, "399168e2"],
    [999998, 1000000, "99999...00002e6"],
    [268, 64981, "11319...72736e16176"],
    [123, 85810, "88960...34912e21422"],
    [432, 61630, "13218...80608e15298"],
    [900553, 900563, "31595...16864e2"],
    [371, 375, "7219856259e3"],
];

for (const [left, right, expected] of tests) {
    console.log(abbreviateProduct(left, right) == expected);
}
