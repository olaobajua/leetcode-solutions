"""
 * 1288. Remove Covered Intervals (Medium)
 * Given an array intervals where intervals[i] = [lᵢ, rᵢ] represent the
 * interval [lᵢ, rᵢ), remove all intervals that are covered by another interval
 * in the list.
 *
 * The interval [a, b) is covered by the interval [c, d) if and only if
 * c <= a and b <= d.
 *
 * Return the number of remaining intervals.
 *
 * Example 1:
 * Input: intervals = [[1,4],[3,6],[2,8]]
 * Output: 2
 * Explanation: Interval [3,6] is covered by [2,8], therefore it is removed.
 *
 * Example 2:
 * Input: intervals = [[1,4],[2,3]]
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= intervals.length <= 1000
 *     ∙ intervals[i].length == 2
 *     ∙ 0 <= lᵢ <= rᵢ <= 10⁵
 *     ∙ All the given intervals are unique.
"""
from typing import List

class Solution:
    def removeCoveredIntervals(self, intervals: List[List[int]]) -> int:
        ret = right = 0
        intervals.sort(key=lambda a: (a[0], -a[1]))
        for a, b in intervals:
            ret += right < b
            right = max(right, b)
        return ret

if __name__ == "__main__":
    tests = (
        ([[1,4],[3,6],[2,8]], 2),
        ([[1,4],[2,3]], 1),
        ([[1,2],[1,4],[3,4]], 1),
        ([[1,4],[4,7],[3,6]], 3),
    )
    for intervals, expected in tests:
        print(Solution().removeCoveredIntervals(intervals) == expected)
