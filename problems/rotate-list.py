"""
 * 61. Rotate List (Medium)
 * Given the head of a linked list, rotate the list to the right by k places.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [4,5,1,2,3]
 *
 * Example 2:
 * Input: head = [0,1,2], k = 4
 * Output: [2,0,1]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 500].
 *     ∙ -100 <= Node.val <= 100
 *     ∙ 0 <= k <= 2 * 109
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def rotateRight(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        if not head:
            return head
        end = head
        n = 1
        while end and end.next:
            end = end.next
            n += 1
        if (k := k%n) == 0:
            return head
        mid = head
        for _ in range(n - k - 1):
            mid = mid.next
        end.next = head
        head = mid.next
        mid.next = None
        return head

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5], 2, [4,5,1,2,3]),
        ([1,2,3,4,5,6], 2, [5,6,1,2,3,4]),
        ([0,1,2], 4, [2,0,1]),
        ([], 1, []),
    )
    for nums, k, expected in tests:
        root = list_to_linked_list(nums)
        print(linked_list_to_list(Solution().rotateRight(root, k)) == expected)
