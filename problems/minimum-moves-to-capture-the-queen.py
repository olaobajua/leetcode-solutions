"""
 * 10036. Minimum Moves to Capture The Queen [Medium]
 * There is a 1-indexed 8 x 8 chessboard containing 3 pieces.
 *
 * You are given 6 integers a, b, c, d, e, and f where:
 *     ∙ (a, b) denotes the position of the white rook.
 *     ∙ (c, d) denotes the position of the white bishop.
 *     ∙ (e, f) denotes the position of the black queen.
 *
 * Given that you can only move the white pieces, return the minimum number of
 * moves required to capture the black queen.
 *
 * Note that:
 *     ∙ Rooks can move any number of squares either vertically or
 *       horizontally, but cannot jump over other pieces.
 *     ∙ Bishops can move any number of squares diagonally, but cannot jump
 *       over other pieces.
 *     ∙ A rook or a bishop can capture the queen if it is located in a square
 *       that they can move to.
 *     ∙ The queen does not move.
 *
 * Example 1:
 * Input: a = 1, b = 1, c = 8, d = 8, e = 2, f = 3
 * Output: 2
 * Explanation: We can capture the black queen in two moves by moving the
 * white rook to (1, 3) then to (2, 3).
 * It is impossible to capture the black queen in less than two moves since it
 * is not being attacked by any of the pieces at the beginning.
 *
 * Example 2:
 * Input: a = 5, b = 3, c = 3, d = 4, e = 5, f = 2
 * Output: 1
 * Explanation: We can capture the black queen in a single move by doing one
 * of the following:
 * - Move the white rook to (5, 2).
 * - Move the white bishop to (5, 2).
 *
 * Constraints:
 *     ∙ 1 <= a, b, c, d, e, f <= 8
 *     ∙ No two pieces are on the same square.
"""
from math import copysign

class Solution:
    def minMovesToCaptureTheQueen(self, a: int, b: int, c: int, d: int, e: int, f: int) -> int:
        ret = 2
        if a == e or b == f:
            if a == c == e and (b < d < f or b > d > f):
                pass
            elif b == d == f and (a < c < e or a > c > e):
                pass
            else:
                ret = 1
        if abs(e - c) == abs(f - d):
            dx = copysign(1, e - c)
            dy = copysign(1, f - d)
            while c != e and d != f:
                c += dx
                d += dy
                if a == c and b == d:
                    break
            else:
                ret = 1

        return ret

if __name__ == "__main__":
    tests = (
        (1, 1, 8, 8, 2, 3, 2),
        (5, 3, 3, 4, 5, 2, 1),
        (4, 3, 3, 4, 5, 2, 2),
        (1, 1, 1, 4, 1, 8, 2),
        (5, 8, 8, 8, 1, 8, 1),
        (8, 4, 7, 7, 4, 4, 1),
        (3, 7, 1, 5, 3, 2, 1),
        (4, 5, 7, 8, 2, 3, 2),
        (4, 3, 3, 4, 2, 5, 1),
        (2, 7, 4, 7, 3, 8, 1),
    )
    for a, b, c, d, e, f, expected in tests:
        print(Solution().minMovesToCaptureTheQueen(a, b, c, d, e, f) == expected)
