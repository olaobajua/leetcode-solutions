"""
 * 103. Binary Tree Zigzag Level Order Traversal [Medium]
 * Given the root of a binary tree, return the zigzag level order traversal of
 * its nodes' values. (i.e., from left to right, then right to left for the
 * next level and alternate between).
 *
 * Example 1:
 *    3
 * 9    20
 *     15 7
 * Input: root = [3,9,20,null,null,15,7]
 * Output: [[3],[20,9],[15,7]]
 *
 * Example 2:
 * Input: root = [1]
 * Output: [[1]]
 *
 * Example 3:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 2000].
 *     ∙ -100 <= Node.val <= 100
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def zigzagLevelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        ret = []
        reverse = False
        level = [root]
        while level:
            next_level = []
            cur = []
            for node in level:
                if node:
                    cur.append(node.val)
                    next_level.append(node.left)
                    next_level.append(node.right)
            level = next_level
            if cur:
                ret.append(list(reversed(cur)) if reverse else cur)
            reverse = not reverse
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([3,9,20,None,None,15,7], [[3],[20,9],[15,7]]),
        ([1], [[1]]),
        ([], []),
    )
    for nums, expected in tests:
        print(Solution().zigzagLevelOrder(build_tree(nums)) == expected)
