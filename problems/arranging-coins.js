/**
 * 441. Arranging Coins (Easy)
 * You have n coins and you want to build a staircase with these coins. The
 * staircase consists of k rows where the ith row has exactly i coins. The last
 * row of the staircase may be incomplete.
 *
 * Given the integer n, return the number of complete rows of the staircase you
 * will build.
 *
 * Example 1:
 * Input: n = 5
 * Output: 2
 * Explanation: Because the 3rd row is incomplete, we return 2.
 *
 * Example 2:
 * Input: n = 8
 * Output: 3
 * Explanation: Because the 4th row is incomplete, we return 3.
 *
 * Constraints:
 *     1 <= n <= 2³¹ - 1
 */

/**
 * @param {number} n
 * @return {number}
 */
var arrangeCoins = function(n) {
    // the sum of first n members of arithmetic progression can be found as
    // S = k(2a₁ + d(k - 1)) / 2
    // dk² + (2a₁ - d)k - 2S = 0
    // in our case S = n, a₁ = 1, d = 1, and we have to find k = ?
    // k² + k - 2n = 0
    // to solve quadratic equation
    // ax² + bx + c = 0; where in our case a = 1, b = 1, c = -2n and x = k
    // we need to find it's discriminant
    // D = b² - 4ac
    // D = 1 + 8n
    // then
    // x = (-b + sqrt(D)) / (2a)
    // x = (-1 + sqrt(8n + 1))/2
    return Math.floor(((8*n + 1)**0.5 - 1) / 2);
};

const tests = [
    [5, 2],
    [8, 3],
    [1, 1],
];
for (let [n, expected] of tests) {
    console.log(arrangeCoins(n) == expected);
}
