/**
 * 20. Valid Parentheses (Easy)
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and
 * ']', determine if the input string is valid.
 *
 * An input string is valid if:
 *     ∙ Open brackets must be closed by the same type of brackets.
 *     ∙ Open brackets must be closed in the correct order.
 *
 * Example 1:
 * Input: s = "()"
 * Output: true
 *
 * Example 2:
 * Input: s = "()[]{}"
 * Output: true
 *
 * Example 3:
 * Input: s = "(]"
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁴
 *     ∙ s consists of parentheses only '()[]{}'.
 */

/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
    const PAIRS = {'(': ')', '[': ']', '{': '}'};
    const stack = [];
    for (const char of Array.from(s)) {
        if (char in PAIRS) {
            stack.push(PAIRS[char]);
        } else if (!stack.length || char != stack[stack.length-1]) {
            return false;
        } else {
            stack.pop();
        }
    }
    return !stack.length;
};

const tests = [
    ["()", true],
    ["()[]{}", true],
    ["(]", false],
    ["{[]}", true],
    ["[", false],
    ["]", false],
];

for (const [s, expected] of tests) {
    console.log(isValid(s) == expected);
}
