"""
 * 316. Remove Duplicate Letters (Medium)
 * Given a string s, remove duplicate letters so that every letter appears once
 * and only once. You must make sure your result is the smallest in
 * lexicographical order among all possible results.
 *
 * Example 1:
 * Input: s = "bcabc"
 * Output: "abc"
 *
 * Example 2:
 * Input: s = "cbacdcbc"
 * Output: "acdb"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁴
 *     ∙ s consists of lowercase English letters.
 *
 * Note: This question is the same as 1081:
 * https://leetcode.com/problems/smallest-subsequence-of-distinct-characters/
"""
class Solution:
    def removeDuplicateLetters(self, s: str) -> str:
        for c in sorted(set(s)):
            suffix = s[s.index(c):]
            if set(suffix) == set(s):
                return c + self.removeDuplicateLetters(suffix.replace(c, ''))
        return ''

if __name__ == "__main__":
    tests = (
        ("bcabc", "abc"),
        ("cbacdcbc", "acdb"),
        ("leetcode", "letcod"),
    )
    for s, expected in tests:
        print(Solution().removeDuplicateLetters(s) == expected)
