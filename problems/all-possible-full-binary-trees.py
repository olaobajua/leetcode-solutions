"""
 * 894. All Possible Full Binary Trees [Medium]
 * Given an integer n, return a list of all possible full binary trees with n
 * nodes. Each node of each tree in the answer must have Node.val == 0.
 *
 * Each element of the answer is the root node of one possible tree. You may
 * return the final list of trees in any order.
 *
 * A full binary tree is a binary tree where each node has exactly 0 or 2
 * children.
 *
 * Example 1:
 * Input: n = 7
 *    0           0           0            0            0
 * 0     0     0     0     0     0      0     0      0     0
 *     0   0       0   0  0 0   0 0   0   0        0   0
 *        0 0     0 0                    0 0      0 0
 *
 * Output: [[0,0,0,null,null,0,0,null,null,0,0],
 *          [0,0,0,null,null,0,0,0,0],
 *          [0,0,0,0,0,0,0],
 *          [0,0,0,0,0,null,null,null,null,0,0],
 *          [0,0,0,0,0,null,null,0,0]]
 *
 * Example 2:
 *  0
 * 0 0
 *
 * Input: n = 3
 * Output: [[0,0,0]]
 *
 * Constraints:
 *     ∙ 1 <= n <= 20
"""
from functools import cache
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    @cache
    def allPossibleFBT(self, n: int) -> List[Optional[TreeNode]]:
        if n == 0:
            return []

        if n == 1:
            return [TreeNode()]

        root = TreeNode()
        ret = []
        for i in range(n):
            left_subtrees = self.allPossibleFBT(i)
            right_subtrees = self.allPossibleFBT(n - i - 1)
            for left in left_subtrees:
                for right in right_subtrees:
                    ret.append(TreeNode(left=left, right=right))
        
        return ret

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        (7, [[0,0,0,None,None,0,0,None,None,0,0],
             [0,0,0,None,None,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,None,None,None,None,0,0],
             [0,0,0,0,0,None,None,0,0]]),
        (3, [[0,0,0]]),
    )
    for n, expected in tests:
        ret = set()
        count = 0
        for tree in Solution().allPossibleFBT(n):
            tree = [*tree_to_list(tree)]
            while tree and tree[-1] is None:
                tree.pop()
            ret.add(tuple(tree))
            count += 1

        if count == len(expected):
            print(ret == set(map(tuple, expected)))
        else:
            print(False)
