"""
 * 994. Rotting Oranges (Medium)
 * You are given an m x n grid where each cell can have one of three values:
 *     0 representing an empty cell,
 *     1 representing a fresh orange, or
 *     2 representing a rotten orange.
 *
 * Every minute, any fresh orange that is 4-directionally adjacent to a rotten
 * orange becomes rotten.
 *
 * Return the minimum number of minutes that must elapse until no cell has a
 * fresh orange. If this is impossible, return -1.
 *
 * Example 1:
 * Input: grid = [[2,1,1],[1,1,0],[0,1,1]]
 * Output: 4
 *
 * Example 2:
 * Input: grid = [[2,1,1],[0,1,1],[1,0,1]]
 * Output: -1
 * Explanation: The orange in the bottom left corner (row 2, column 0) is never
 * rotten, because rotting only happens 4-directionally.
 *
 * Example 3:
 * Input: grid = [[0,2]]
 * Output: 0
 * Explanation: Since there are already no fresh oranges at minute 0, the
 * answer is just 0.
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m, n <= 10
 *     grid[i][j] is 0, 1, or 2.
"""
from itertools import chain
from typing import List

class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:
        rotten = [(r, c) for r, row in enumerate(grid)
                  for c, cell in enumerate(row) if cell == 2]
        n = len(grid)
        m = len(grid[0])
        mins = -1
        while rotten:
            for _ in range(len(rotten)):
                r, c = rotten.pop(0)
                for nr, nc in (r+1, c), (r, c+1), (r-1, c), (r, c-1):
                    if 0 <= nr < n and 0 <= nc < m and grid[nr][nc] == 1:
                        rotten.append((nr, nc))
                        grid[nr][nc] = 2
            mins += 1
        return -1 if any(cell == 1 for cell in chain(*grid)) else max(0, mins)

if __name__ == "__main__":
    tests = (
        ([[2,1,1],[1,1,0],[0,1,1]], 4),
        ([[2,1,1],[0,1,1],[1,0,1]], -1),
        ([[0,2]], 0),
        ([[0]], 0),
        ([[2,2],[1,1],[0,0],[2,0]], 1),
    )
    for grid, expected in tests:
        print(Solution().orangesRotting(grid) == expected)
