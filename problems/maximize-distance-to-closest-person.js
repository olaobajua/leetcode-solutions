/**
 * 849. Maximize Distance to Closest Person (Medium)
 * You are given an array representing a row of seats where seats[i] = 1
 * represents a person sitting in the ith seat, and seats[i] = 0 represents
 * that the ith seat is empty (0-indexed).
 *
 * There is at least one empty seat, and at least one person sitting.
 *
 * Alex wants to sit in the seat such that the distance between him and the
 * closest person to him is maximized.
 *
 * Return that maximum distance to the closest person.
 *
 * Example 1:
 * Input: seats = [1,0,0,0,1,0,1]
 * Output: 2
 * Explanation:
 * If Alex sits in the second open seat (i.e. seats[2]), then the closest
 * person has distance 2.
 * If Alex sits in any other open seat, the closest person has distance 1.
 * Thus, the maximum distance to the closest person is 2.
 *
 * Example 2:
 * Input: seats = [1,0,0,0]
 * Output: 3
 * Explanation:
 * If Alex sits in the last seat (i.e. seats[3]), the closest person is 3 seats
 * away. This is the maximum distance possible, so the answer is 3.
 *
 * Example 3:
 * Input: seats = [0,1]
 * Output: 1
 *
 * Constraints:
 *     ∙ 2 <= seats.length <= 2 * 10⁴
 *     ∙ seats[i] is 0 or 1.
 *     ∙ At least one seat is empty.
 *     ∙ At least one seat is occupied.
 */

/**
 * @param {number[]} seats
 * @return {number}
 */
var maxDistToClosest = function(seats) {
    let distance = Math.max(seats.indexOf(1) - 0,
                            seats.length - seats.lastIndexOf(1) - 1);
    let maxFreeSeats = 0, freeSeats = 0;
    for (seat of seats) {
        if (seat == 1) {
            maxFreeSeats = Math.max(maxFreeSeats, freeSeats);
            freeSeats = 0;
        } else {
            ++freeSeats;
        }
    }
    return Math.max(distance, Math.floor((maxFreeSeats + 1)/2));
};

const tests = [
    [[1,0,0,0,1,0,1], 2],
    [[1,0,0,0], 3],
    [[0,1], 1],
    [[1,0,1], 1],
];

for (const [seats, expected] of tests) {
    console.log(maxDistToClosest(seats) == expected);
}
