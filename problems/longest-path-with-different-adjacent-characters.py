"""
 * 2246. Longest Path With Different Adjacent Characters [Hard]
 * You are given a tree (i.e. a connected, undirected graph that has no
 * cycles) rooted at node 0 consisting of n nodes numbered from 0 to n - 1. The
 * tree is represented by a 0-indexed array parent of size n, where parent[i]
 * is the parent of node i. Since node 0 is the root, parent[0] == -1.
 *
 * You are also given a string s of length n, where s[i] is the character
 * assigned to node i.
 *
 * Return the length of the longest path in the tree such that no pair of
 * adjacent nodes on the path have the same character assigned to them.
 *
 * Example 1:
 *     0        a
 *  1     2   b   a
 * 3 4    5  c b  e
 * Input: parent = [-1,0,0,1,1,2], s = "abacbe"
 * Output: 3
 * Explanation: The longest path where each two adjacent nodes have different
 * characters in the tree is the path: 0 -> 1 -> 3. The length of this path is
 * 3, so 3 is returned.
 * It can be proven that there is no longer path that satisfies the
 * conditions.
 *
 * Example 2:
 *   0      a
 * 1 2 3  a b c
 * Input: parent = [-1,0,0,0], s = "aabc"
 * Output: 3
 * Explanation: The longest path where each two adjacent nodes have different
 * characters is the path: 2 -> 0 -> 3. The length of this path is 3, so 3 is
 * returned.
 *
 * Constraints:
 *     ∙ n == parent.length == s.length
 *     ∙ 1 <= n <= 10⁵
 *     ∙ 0 <= parent[i] <= n - 1 for all i >= 1
 *     ∙ parent[0] == -1
 *     ∙ parent represents a valid tree.
 *     ∙ s consists of only lowercase English letters.
"""
from collections import defaultdict
from functools import cache
from typing import List

class Solution:
    def longestPath(self, parent: List[int], s: str) -> int:
        @cache
        def longest_subtree(i):
            ret = 1
            for child in graph[i]:
                if s[i] != s[child]:
                    ret = max(ret, longest_subtree(child) + 1)
            return ret

        def longest_path(i):
            ret = []
            for child in graph[i]:
                if s[i] != s[child]:
                    ret.append(longest_subtree(child))
            if len(ret) >= 2:
                return sum(sorted(ret)[-2:]) + 1
            elif len(ret) == 1:
                return ret[0] + 1
            else:
                return 1

        graph = defaultdict(set)
        for i, node in enumerate(parent):
            graph[node].add(i)
        graph.pop(-1)
        return max(longest_path(i) for i in range(len(s)))

if __name__ == "__main__":
    tests = (
        ([-1,0,0,1,1,2], "abacbe", 3),
        ([-1,0,0,0], "aabc", 3),
    )
    for parent, s, expected in tests:
        print(Solution().longestPath(parent, s) == expected)
