"""
 * 542. 01 Matrix [Medium]
 * Given an m x n binary matrix mat, return the distance of the nearest 0 for
 * each cell.
 *
 * The distance between two adjacent cells is 1.
 *
 * Example 1:
 * Input: mat = [[0,0,0],
 *               [0,1,0],
 *               [0,0,0]]
 * Output: [[0,0,0],
 *          [0,1,0],
 *          [0,0,0]]
 *
 * Example 2:
 * Input: mat = [[0,0,0],
 *               [0,1,0],
 *               [1,1,1]]
 * Output: [[0,0,0],
 *          [0,1,0],
 *          [1,2,1]]
 *
 * Constraints:
 *     ∙ m == mat.length
 *     ∙ n == mat[i].length
 *     ∙ 1 <= m, n <= 10⁴
 *     ∙ 1 <= m * n <= 10⁴
 *     ∙ mat[i][j] is either 0 or 1.
 *     ∙ There is at least one 0 in mat.
"""
from itertools import product
from math import inf
from typing import List

class Solution:
    def updateMatrix(self, mat: List[List[int]]) -> List[List[int]]:
        m = len(mat)
        n = len(mat[0])
        ret = [[inf] * n for _ in range(m)]

        for row, col in product(range(m), range(n)):
            if mat[row][col] == 0:
                ret[row][col] = 0
            else:
                if row > 0:
                    ret[row][col] = min(ret[row][col], ret[row-1][col] + 1)
                if col > 0:
                    ret[row][col] = min(ret[row][col], ret[row][col-1] + 1)

        for row, col in product(range(m - 1, -1, -1), range(n - 1, -1, -1)):
            if row < m - 1:
                ret[row][col] = min(ret[row][col], ret[row+1][col] + 1)
            if col < n - 1:
                ret[row][col] = min(ret[row][col], ret[row][col+1] + 1)

        return ret

if __name__ == "__main__":
    tests = (
        ([[0,0,0],
          [0,1,0],
          [0,0,0]],
         [[0,0,0],
          [0,1,0],
          [0,0,0]]),
        ([[0,0,0],
          [0,1,0],
          [1,1,1]],
         [[0,0,0],
          [0,1,0],
          [1,2,1]]),
        ([[0,1,0],
          [0,1,0],
          [0,1,0],
          [0,1,0],
          [0,1,0]],
         [[0,1,0],
          [0,1,0],
          [0,1,0],
          [0,1,0],
          [0,1,0]]),
        ([[0,1,0,1,1],
          [1,1,0,0,1],
          [0,0,0,1,0],
          [1,0,1,1,1],
          [1,0,0,0,1]], 
         [[0,1,0,1,2],
          [1,1,0,0,1],
          [0,0,0,1,0],
          [1,0,1,1,1],
          [1,0,0,0,1]]),
    )
    for mat, expected in tests:
        print(Solution().updateMatrix(mat) == expected)
