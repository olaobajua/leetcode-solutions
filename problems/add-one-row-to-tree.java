/**
 * 623. Add One Row to Tree [Medium]
 * Given the root of a binary tree and two integers val and depth, add a row
 * of nodes with value val at the given depth depth.
 *
 * Note that the root node is at depth 1.
 *
 * The adding rule is:
 *     ∙ Given the integer depth, for each not null tree node cur at the depth
 *       depth - 1, create two tree nodes with value val as cur's left subtree
 *       root and right subtree root.
 *     ∙ cur's original left subtree should be the left subtree of the new
 *       left subtree root.
 *     ∙ cur's original right subtree should be the right subtree of the new
 *       right subtree root.
 *     ∙ If depth == 1 that means there is no depth depth - 1 at all, then
 *       create a tree node with value val as the new root of the whole
 *       original tree, and the original tree is the new root's left subtree.
 *
 * Example 1:
 *     4              4
 *  2     6   ->   1     1
 * 3 1   5        2       6
 *               3 1     5
 * Input: root = [4,2,6,3,1,5], val = 1, depth = 2
 * Output: [4,1,1,2,null,null,6,3,1,5]
 *
 * Example 2:
 *     4              4
 *  2         ->   2
 * 3 1            1 1
 *               3   1
 * Input: root = [4,2,null,3,1], val = 1, depth = 3
 * Output: [4,2,null,1,1,3,null,null,1]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ The depth of the tree is in the range [1, 10⁴].
 *     ∙ -100 <= Node.val <= 100
 *     ∙ -10⁵ <= val <= 10⁵
 *     ∙ 1 <= depth <= the depth of tree + 1
 */

// Definition for a binary tree node.
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class Solution {
    public TreeNode addOneRow(TreeNode root, int val, int depth) {
        if (depth == 1)
            return new TreeNode(val, root, null);
        dfs(root, val, depth - 1);
        return root;
    }
    private void dfs(TreeNode node, int val, int depth) {
        if (node != null) {
            if (depth > 1) {
                dfs(node.left, val, depth - 1);
                dfs(node.right, val, depth - 1);
            } else {
                node.left = new TreeNode(val, node.left, null);
                node.right = new TreeNode(val, null, node.right);
            }
        }
    }
}
