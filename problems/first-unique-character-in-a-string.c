/**
 * 387. First Unique Character in a String [Easy]
 * Given a string s, find the first non-repeating character in it and return
 * its index. If it does not exist, return -1.
 *
 * Example 1:
 * Input: s = "leetcode"
 * Output: 0
 * Example 2:
 * Input: s = "loveleetcode"
 * Output: 2
 * Example 3:
 * Input: s = "aabb"
 * Output: -1
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ s consists of only lowercase English letters.
 */
int firstUniqChar(char *s) {
    int count[123] = {0};
    for (int i = 0; s[i] != '\0'; ++i)
        ++count[s[i]];
    for (int i = 0; s[i] != '\0'; ++i)
        if (count[s[i]] == 1)
            return i;
    return -1;
}
