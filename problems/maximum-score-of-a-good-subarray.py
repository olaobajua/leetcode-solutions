"""
 * 1793. Maximum Score of a Good Subarray [Hard]
 * You are given an array of integers nums (0-indexed) and an integer k.
 *
 * The score of a subarray (i, j) is defined as min(nums[i], nums[i+1], ...,
 * nums[j]) * (j - i + 1). A good subarray is a subarray where i <= k <= j.
 *
 * Return the maximum possible score of a good subarray.
 *
 * Example 1:
 * Input: nums = [1,4,3,7,4,5], k = 3
 * Output: 15
 * Explanation: The optimal subarray is (1, 5) with a score of min(4,3,7,4,5)
 * * (5-1+1) = 3 * 5 = 15.
 *
 * Example 2:
 * Input: nums = [5,5,4,5,4,1,1,1], k = 0
 * Output: 20
 * Explanation: The optimal subarray is (0, 4) with a score of min(5,5,4,5,4)
 * * (4-0+1) = 4 * 5 = 20.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i] <= 2 * 10⁴
 *     ∙ 0 <= k < nums.length
"""
from typing import List

class Solution:
    def maximumScore(self, nums: List[int], k: int) -> int:
        n = len(nums)
        left = right = k
        cur_min = ret = nums[k]
        while 0 <= left and right < n:
            nl = 0
            if left > 0:
                nl = nums[left-1]
            nr = 0
            if right < n - 1:
                nr = nums[right+1]
            if nl == nr == 0:
                break
            if nl > nr:
                left -= 1
                cur_min = min(cur_min, nums[left])
            else:
                right += 1
                cur_min = min(cur_min, nums[right])
            ret = max(ret, cur_min * (right - left + 1))

        return ret

if __name__ == "__main__":
    tests = (
        ([1,4,3,7,4,5], 3, 15),
        ([5,5,4,5,4,1,1,1], 0, 20),
        ([6569,9667,3148,7698,1622,2194,793,9041,1670,1872], 5, 9732),
        ([8182,1273,9847,6230,52,1467,6062,726,4852,4507,2460,2041,500,1025,5524], 8, 9014),
    )
    for nums, k, expected in tests:
        print(Solution().maximumScore(nums, k) == expected)
