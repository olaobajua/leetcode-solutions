"""
 * 673. Number of Longest Increasing Subsequence [Medium]
 * Given an integer array nums, return the number of longest increasing
 * subsequences.
 *
 * Notice that the sequence has to be strictly increasing.
 *
 * Example 1:
 * Input: nums = [1,3,5,4,7]
 * Output: 2
 * Explanation: The two longest increasing subsequences are [1, 3, 4, 7] and
 * [1, 3, 5, 7].
 *
 * Example 2:
 * Input: nums = [2,2,2,2,2]
 * Output: 5
 * Explanation: The length of the longest increasing subsequence is 1, and
 * there are 5 increasing subsequences of length 1, so output 5.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 2000
 *     ∙ -10⁶ <= nums[i] <= 10⁶
"""
from typing import List

class Solution:
    def findNumberOfLIS(self, nums: List[int]) -> int:
        n = len(nums)
        lengths = [1] * n
        counts = [1] * n
        max_len = 1

        for i in range(n):
            for j in range(i):
                if nums[i] > nums[j]:
                    if lengths[j] + 1 > lengths[i]:
                        lengths[i] = lengths[j] + 1
                        counts[i] = counts[j]
                    elif lengths[j] + 1 == lengths[i]:
                        counts[i] += counts[j]

            max_len = max(max_len, lengths[i])

        return sum(counts[i] for i in range(n) if lengths[i] == max_len)

if __name__ == "__main__":
    tests = (
        ([1,3,5,4,7], 2),
        ([2,2,2,2,2], 5),
    )
    for nums, expected in tests:
        print(Solution().findNumberOfLIS(nums) == expected)
