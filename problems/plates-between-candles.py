"""
 * 2055. Plates Between Candles (Medium)
 * There is a long table with a line of plates and candles arranged on top of
 * it. You are given a 0-indexed string s consisting of characters '*' and '|'
 * only, where a '*' represents a plate and a '|' represents a candle.
 *
 * You are also given a 0-indexed 2D integer array queries where
 * queries[i] = [leftᵢ, rightᵢ] denotes the substring s[leftᵢ...rightᵢ]
 * (inclusive). For each query, you need to find the number of plates between
 * candles that are in the substring. A plate is considered between candles if
 * there is at least one candle to its left and at least one candle to its
 * right in the substring.
 *     ∙ For example, s = "||**||**|*", and a query [3, 8] denotes the
 *       substring "*||**|". The number of plates between candles in this
 *       substring is 2, as each of the two plates has at least one candle in
 *       the substring to its left and right.
 *
 * Return an integer array answer where answer[i] is the answer to the ith
 * query.
 *
 * Example 1:
 * Input: s = "**|**|***|", queries = [[2,5],[5,9]]
 * Output: [2,3]
 * Explanation:
 * - queries[0] has two plates between candles.
 * - queries[1] has three plates between candles.
 *
 * Example 2:
 * Input: s = "***|**|*****|**||**|*",
 * queries = [[1,17],[4,5],[14,17],[5,11],[15,16]]
 * Output: [9,0,0,0,0]
 * Explanation:
 * - queries[0] has nine plates between candles.
 * - The other queries have zero plates between candles.
 *
 * Constraints:
 *     3 <= s.length <= 10⁵
 *     s consists of '*' and '|' characters.
 *     1 <= queries.length <= 10⁵
 *     queries[i].length == 2
 *     0 <= leftᵢ <= rightᵢ < s.length
"""
from typing import List

class Solution:
    def platesBetweenCandles(self, s: str, queries: List[List[int]]) -> List[int]:
        n = len(s)
        plates_before = [0] * n
        prev_candle = [0] * n
        next_candle = [0] * n
        for i, c in enumerate(s):
            plates_before[i] = plates_before[i-1] + (c == '*')
            prev_candle[i] = prev_candle[i-1] if c == '*' else i
            next_candle[-i-1] = next_candle[-i] if s[-i-1] == '*' else n-i-1
        return [max(0, plates_before[prev_candle[right]] -
                       plates_before[next_candle[left]])
                for left, right in queries]

if __name__ == "__main__":
    tests = (
        ("**|**|***|", [[2,5],[5,9]], [2,3]),
        ("***|**|*****|**||**|*", [[1,17],[4,5],[14,17],[5,11],[15,16]], [9,0,0,0,0]),
    )
    for s, queries, expected in tests:
        print(Solution().platesBetweenCandles(s, queries) == expected)
