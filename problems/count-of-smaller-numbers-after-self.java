/**
 * 315. Count of Smaller Numbers After Self [Hard]
 * You are given an integer array nums and you have to return a new counts
 * array. The counts array has the property where counts[i] is the number of
 * smaller elements to the right of nums[i].
 *
 * Example 1:
 * Input: nums = [5,2,6,1]
 * Output: [2,1,1,0]
 * Explanation:
 * To the right of 5 there are 2 smaller elements (2 and 1).
 * To the right of 2 there is only 1 smaller element (1).
 * To the right of 6 there is 1 smaller element (1).
 * To the right of 1 there is 0 smaller element.
 *
 * Example 2:
 * Input: nums = [-1]
 * Output: [0]
 *
 * Example 3:
 * Input: nums = [-1,-1]
 * Output: [0,0]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ -10⁴ <= nums[i] <= 10⁴
 */
class Solution {
    public List<Integer> countSmaller(int[] nums) {
        List<Integer> sortedNums = new ArrayList<Integer>();
        List<Integer> ret = new ArrayList<Integer>();
        for (int i = nums.length - 1; i >= 0; --i) {
            int index = Collections.binarySearch(sortedNums, nums[i]);
            while (index > 0 && nums[i] == sortedNums.get(index - 1))
                --index;
            if (index < 0)
                index = -index - 1;
            ret.add(index);
            sortedNums.add(index, nums[i]);
        }
        Collections.reverse(ret);
        return ret;
    }
}
