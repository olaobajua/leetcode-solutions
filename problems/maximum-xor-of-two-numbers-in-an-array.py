"""
 * 421. Maximum XOR of Two Numbers in an Array (Medium)
 * Given an integer array nums, return the maximum result of
 * nums[i] XOR nums[j], where 0 <= i <= j < n.
 *
 * Example 1:
 * Input: nums = [3,10,5,25,2,8]
 * Output: 28
 * Explanation: The maximum result is 5 XOR 25 = 28.
 *
 * Example 2:
 * Input: nums = [14,70,53,83,49,91,36,80,92,51,66,70]
 * Output: 127
 *
 * Constraints:
 *     1 <= nums.length <= 2 * 10⁵
 *     0 <= nums[i] <= 2³¹ - 1
"""
from typing import List

class Solution:
    def findMaximumXOR(self, nums: List[int]) -> int:
        ret = 0
        for i in range(31, -1, -1):
            ret <<= 1
            prefixes = {x >> i for x in nums}
            ret += any((ret | 1) ^ pref in prefixes for pref in prefixes)
        return ret

if __name__ == "__main__":
    tests = (
        ([0], 0),
        ([3,10,5,25,2,8], 28),  # 25 ^ 5 = 11001 ^ 00101 = 11100
        ([14,70,53,83,49,91,36,80,92,51,66,70], 127),  # 36 ^ 91 = 0100100 ^ 1011011 = 1111111
        ([14,15,9,3,2], 13),  # 2 ^ 15 = 0010 ^ 1111 = 1101
        ([15,15,9,3,2], 13),  # 2 ^ 15 = 0010 ^ 1111 = 1101
    )
    for nums, expected in tests:
        print(Solution().findMaximumXOR(nums) == expected)
