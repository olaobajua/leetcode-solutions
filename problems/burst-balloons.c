/**
 * 312. Burst Balloons (Hard)
 * You are given n balloons, indexed from 0 to n - 1. Each balloon is painted
 * with a number on it represented by an array nums. You are asked to burst all
 * the balloons.
 *
 * If you burst the ith balloon, you will get
 * nums[i - 1] * nums[i] * nums[i + 1] coins. If i - 1 or i + 1 goes out of
 * bounds of the array, then treat it as if there is a balloon with a 1 painted
 * on it.
 *
 * Return the maximum coins you can collect by bursting the balloons wisely.
 *
 * Example 1:
 * Input: nums = [3,1,5,8]
 * Output: 167
 * Explanation:
 * nums = [3,1,5,8] --> [3,5,8] --> [3,8] --> [8] --> []
 * coins =  3*1*5    +   3*5*8   +  1*3*8  + 1*8*1 = 167
 *
 * Example 2:
 * Input: nums = [1,5]
 * Output: 10
 *
 * Constraints:
 *     n == nums.length
 *     1 <= n <= 500
 *     0 <= nums[i] <= 100
 */
int maxCoins(int *nums, int numsSize) {
    static int balloons[502] = {1};
    static int dp[502][502] = {0};
    int n = 1;
    int same = 0;
    for (int i = 0; i < numsSize; ++i) {
        balloons[n] = nums[i];
        n += nums[i] > 0;
        same += nums[i] == nums[0];
    }
    if (numsSize > 1 && same == numsSize) {
        return pow(nums[0], 3) * (numsSize - 2) + pow(nums[0], 2) + nums[0];
    }
    balloons[n++] = 1;
    int score;
    for (int i = n - 2; i >= 0; --i) {
        for (int j = i + 2; j < n; ++j) {
            dp[i][j] = 0;
            score = balloons[i]*balloons[j];
            for (int k = i + 1; k < j; ++k) {
                dp[i][j] = fmax(dp[i][j],
                                score*balloons[k] + dp[i][k] + dp[k][j]);
            }
        }
    }
    return dp[0][n-1];
}
