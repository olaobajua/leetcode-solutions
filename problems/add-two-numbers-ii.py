"""
 * 445. Add Two Numbers II [Medium]
 * You are given two non-empty linked lists representing two non-negative
 * integers. The most significant digit comes first and each of their nodes
 * contains a single digit. Add the two numbers and return the sum as a linked
 * list.
 *
 * You may assume the two numbers do not contain any leading zero, except the
 * number 0 itself.
 *
 * Example 1:
 * Input: l1 = [7,2,4,3], l2 = [5,6,4]
 * Output: [7,8,0,7]
 *
 * Example 2:
 * Input: l1 = [2,4,3], l2 = [5,6,4]
 * Output: [8,0,7]
 *
 * Example 3:
 * Input: l1 = [0], l2 = [0]
 * Output: [0]
 *
 * Constraints:
 *     ∙ The number of nodes in each linked list is in the range [1, 100].
 *     ∙ 0 <= Node.val <= 9
 *     ∙ It is guaranteed that the list represents a number that does not have
 *       leading zeros.
 *
 * Follow up: Could you solve it without reversing the input lists?
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        prev = None
        l1_len = 0
        while l1 is not None:
            l1.prev = prev
            prev = l1
            l1 = l1.next
            l1_len += 1

        prev = None
        l2_len = 0
        while l2 is not None:
            l2.prev = prev
            prev = l2
            l2 = l2.next
            l2_len += 1

        if l2_len > l1_len:
            l1, l2 = l2, l1

        acc = 0
        while l1.prev and l2.prev:
            l1.val += l2.val + acc
            acc = l1.val // 10
            l1.val %= 10
            l1 = l1.prev
            l2 = l2.prev

        acc += l2.val
        while l1.prev
        l1.val += l2.val + acc
        acc = l1.val // 10
        l1.val %= 10
        if acc:
            if l1.prev is None:
                l1.prev = ListNode()
            l1 = l1.prev
            l1.val +=kk
            l1 = l1.prev

        return l1

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head: Optional[ListNode]) -> List[int]:
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([7,2,4,3], [5,6,4], [7,8,0,7]),
        ([2,4,3], [5,6,4], [8,0,7]),
        ([0], [0], [0]),
    )
    for l1, l2, expected in tests:
        l1 = list_to_linked_list(l1)
        l2 = list_to_linked_list(l2)
        print(linked_list_to_list(Solution().addTwoNumbers(l1, l2)) == expected)
