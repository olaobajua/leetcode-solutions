"""
 * 112. Path Sum [Easy]
 * Given the root of a binary tree and an integer targetSum, return true if
 * the tree has a root-to-leaf path such that adding up all the values along
 * the path equals targetSum.
 *
 * A leaf is a node with no children.
 *
 * Example 1:
 *     5
 *  4     8
 * 11   13 4
 * 7 2      1
 * Input: root = [5,4,8,11,null,13,4,7,2,null,null,null,1], targetSum = 22
 * Output: true
 *
 * Example 2:
 *  1
 * 2 3
 * Input: root = [1,2,3], targetSum = 5
 * Output: false
 * Explanation: There two root-to-leaf paths in the tree:
 * (1 --> 2): The sum is 3.
 * (1 --> 3): The sum is 4.
 * There is no root-to-leaf path with sum = 5.
 *
 * Example 3:
 * Input: root = [], targetSum = 0
 * Output: false
 * Explanation: Since the tree is empty, there are no root-to-leaf paths.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 5000].
 *     ∙ -1000 <= Node.val <= 1000
 *     ∙ -1000 <= targetSum <= 1000
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def hasPathSum(self, root: Optional[TreeNode], targetSum: int) -> bool:
        if root:
            targetSum -= root.val
            if root.left is None and root.right is None and targetSum == 0:
                return True
            if self.hasPathSum(root.left, targetSum):
                return True
            if self.hasPathSum(root.right, targetSum):
                return True
        return False

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([5,4,8,11,None,13,4,7,2,None,None,None,1], 22, True),
        ([1,2,3], 5, False),
        ([], 0, False),
        ([1,2], 1, False),
    )
    for nums, targetSum, expected in tests:
        print(Solution().hasPathSum(build_tree(nums), targetSum) == expected)
