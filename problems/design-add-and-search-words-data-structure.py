"""
 * 211. Design Add and Search Words Data Structure [Medium]
 * Design a data structure that supports adding new words and finding if a
 * string matches any previously added string.
 *
 * Implement the WordDictionary class:
 *     ∙ WordDictionary() Initializes the object.
 *     ∙ void addWord(word) Adds word to the data structure, it can be matched
 *       later.
 *     ∙ bool search(word) Returns true if there is any string in the data
 *       structure that matches word or false otherwise. word may contain dots
 *       '.' where dots can be matched with any letter.
 *
 * Example:
 * Input
 * ["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
 * [[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
 * Output
 * [null,null,null,null,false,true,true,true]
 *
 * Explanation
 * WordDictionary wordDictionary = new WordDictionary();
 * wordDictionary.addWord("bad");
 * wordDictionary.addWord("dad");
 * wordDictionary.addWord("mad");
 * wordDictionary.search("pad"); // return False
 * wordDictionary.search("bad"); // return True
 * wordDictionary.search(".ad"); // return True
 * wordDictionary.search("b.."); // return True
 *
 * Constraints:
 *     ∙ 1 <= word.length <= 25
 *     ∙ word in addWord consists of lowercase English letters.
 *     ∙ word in search consist of '.' or lowercase English letters.
 *     ∙ There will be at most 3 dots in word for search queries.
 *     ∙ At most 10⁴ calls will be made to addWord and search.
"""
from collections import defaultdict
from itertools import zip_longest

class WordDictionary:
    def __init__(self):
        self.words = defaultdict(set)

    def addWord(self, word: str) -> None:
        self.words[len(word)].add(word)

    def search(self, word: str) -> bool:
        return any(all(l1 in {'.', l2} for l1, l2 in zip_longest(word, w))
                   for w in self.words[len(word)])


obj = WordDictionary()
obj.addWord("bad")
obj.addWord("dad")
obj.addWord("mad")
print(False == obj.search("pad"))
print(True == obj.search("bad"))
print(True == obj.search(".ad"))
print(True == obj.search("b.."))

obj = WordDictionary()
obj.addWord("ran")
obj.addWord("rune")
obj.addWord("runner")
obj.addWord("runs")
obj.addWord("add")
obj.addWord("adds")
obj.addWord("adder")
obj.addWord("addee")
print(obj.search("r.n") == True)
print(obj.search("ru.n.e") == False)
print(obj.search("add") == True)
print(obj.search("add.") == True)
print(obj.search("adde.") == True)
print(obj.search(".an.") == False)
print(obj.search("...s") == True)
print(obj.search("....e.") == True)
print(obj.search(".......") == False)
print(obj.search("..n.r") == False)
