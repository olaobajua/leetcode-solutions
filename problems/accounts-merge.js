/**
 * 721. Accounts Merge (Medium)
 * Given a list of accounts where each element accounts[i] is a list of
 * strings, where the first element accounts[i][0] is a name, and the rest of
 * the elements are emails representing emails of the account.
 *
 * Now, we would like to merge these accounts. Two accounts definitely belong
 * to the same person if there is some common email to both accounts. Note that
 * even if two accounts have the same name, they may belong to different people
 * as people could have the same name. A person can have any number of accounts
 * initially, but all of their accounts definitely have the same name.
 *
 * After merging the accounts, return the accounts in the following format: the
 * first element of each account is the name, and the rest of the elements are
 * emails in sorted order. The accounts themselves can be returned in any
 * order.
 *
 * Example 1:
 * Input: accounts = [["John","johnsmith@mail.com","john_newyork@mail.com"],
 *                    ["John","johnsmith@mail.com","john00@mail.com"],
 *                    ["Mary","mary@mail.com"],
 *                    ["John","johnnybravo@mail.com"]]
 * Output:
 *   [["John","john00@mail.com","john_newyork@mail.com","johnsmith@mail.com"],
 *    ["Mary","mary@mail.com"],
 *    ["John","johnnybravo@mail.com"]]
 * Explanation:
 * The first and second John's are the same person as they have the common
 * email "johnsmith@mail.com".
 * The third John and Mary are different people as none of their email
 * addresses are used by other accounts.
 * We could return these lists in any order, for example the answer
 * [['Mary', 'mary@mail.com'],
 *  ['John', 'johnnybravo@mail.com'],
 *  ['John', 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com']]
 * would still be accepted.
 *
 * Example 2:
 * Input: accounts = [["Gabe","Gabe0@m.co","Gabe3@m.co","Gabe1@m.co"],
 *                    ["Kevin","Kevin3@m.co","Kevin5@m.co","Kevin0@m.co"],
 *                    ["Ethan","Ethan5@m.co","Ethan4@m.co","Ethan0@m.co"],
 *                    ["Hanzo","Hanzo3@m.co","Hanzo1@m.co","Hanzo0@m.co"],
 *                    ["Fern","Fern5@m.co","Fern1@m.co","Fern0@m.co"]]
 * Output: [["Ethan","Ethan0@m.co","Ethan4@m.co","Ethan5@m.co"],
 *          ["Gabe","Gabe0@m.co","Gabe1@m.co","Gabe3@m.co"],
 *          ["Hanzo","Hanzo0@m.co","Hanzo1@m.co","Hanzo3@m.co"],
 *          ["Kevin","Kevin0@m.co","Kevin3@m.co","Kevin5@m.co"],
 *          ["Fern","Fern0@m.co","Fern1@m.co","Fern5@m.co"]]
 *
 * Constraints:
 *     1 <= accounts.length <= 1000
 *     2 <= accounts[i].length <= 10
 *     1 <= accounts[i][j] <= 30
 *     accounts[i][0] consists of English letters.
 *     accounts[i][j] (for j > 0) is a valid email.
 */

/**
 * @param {string[][]} accounts
 * @return {string[][]}
 */
var accountsMerge = function(accounts) {
    const accountsByName = {};
    accounts.forEach((acc) => {
        accountsByName[acc[0]] ? accountsByName[acc[0]].push(acc)
                               : accountsByName[acc[0]] = [acc];
    });
    const ret = [];
    for (const [name, accs] of Object.entries(accountsByName)) {
        const emails = new DisjointSet();
        for (const acc of accs) {
            firstEmail = acc[1];
            emails.makeSet(firstEmail);
            for (const email of acc.slice(2)) {
                emails.makeSet(email);
                emails.union(email, firstEmail);
            }
        }
        const distinctAccs = Object.keys(emails.parent)
                             .filter(e => e == emails.parent[e]);
        for (const email of distinctAccs) {
            ret.push([name, ...emails.findSet(email).sort()]);
        }
    }
    return ret;
};

class DisjointSet {
    constructor(name) {
        Object.defineProperty(this, 'parent', { value: {} });
    }
    makeSet(x) {
        if (this.parent[x] === undefined) {
            this.parent[x] = x;
        }
    }
    find(x) {
        if (x != this.parent[x]) {
            this.parent[x] = this.find(this.parent[x]);
        }
        return this.parent[x];
    }
    findSet(x) {
        const xParent = this.find(x);
        return Object.keys(this.parent).filter(i => this.find(i) == xParent);
    }
    union(x, y) {
        const px = this.find(x);
        const py = this.find(y);
        if (px !== undefined && py !== undefined && px != py) {
            this.parent[px] = py;
        }
    }
}

const tests = [
    [[["John","johnsmith@mail.com","john_newyork@mail.com"],
      ["John","johnsmith@mail.com","john00@mail.com"],
      ["Mary","mary@mail.com"],
      ["John","johnnybravo@mail.com"]],
     [["John","john00@mail.com","john_newyork@mail.com","johnsmith@mail.com"],
      ["Mary","mary@mail.com"],
      ["John","johnnybravo@mail.com"]]],

    [[["Gabe","Gabe0@m.co","Gabe3@m.co","Gabe1@m.co"],
      ["Kevin","Kevin3@m.co","Kevin5@m.co","Kevin0@m.co"],
      ["Ethan","Ethan5@m.co","Ethan4@m.co","Ethan0@m.co"],
      ["Hanzo","Hanzo3@m.co","Hanzo1@m.co","Hanzo0@m.co"],
      ["Fern","Fern5@m.co","Fern1@m.co","Fern0@m.co"]],
     [["Ethan","Ethan0@m.co","Ethan4@m.co","Ethan5@m.co"],
      ["Gabe","Gabe0@m.co","Gabe1@m.co","Gabe3@m.co"],
      ["Hanzo","Hanzo0@m.co","Hanzo1@m.co","Hanzo3@m.co"],
      ["Kevin","Kevin0@m.co","Kevin3@m.co","Kevin5@m.co"],
      ["Fern","Fern0@m.co","Fern1@m.co","Fern5@m.co"]]],

    [[["Ethan","Ethan1@m.co","Ethan2@m.co","Ethan0@m.co"],
      ["David","David1@m.co","David2@m.co","David0@m.co"],
      ["Lily","Lily0@m.co","Lily0@m.co","Lily4@m.co"],
      ["Gabe","Gabe1@m.co","Gabe4@m.co","Gabe0@m.co"],
      ["Ethan","Ethan2@m.co","Ethan1@m.co","Ethan0@m.co"]],
     [["Gabe","Gabe0@m.co","Gabe1@m.co","Gabe4@m.co"],
      ["Ethan","Ethan0@m.co","Ethan1@m.co","Ethan2@m.co"],
      ["David","David0@m.co","David1@m.co","David2@m.co"],
      ["Lily","Lily0@m.co","Lily4@m.co"]]],
];

for (let [accounts, expected] of tests) {
    expected = expected.map(el => el.toString());
    test: {
        for (const account of accountsMerge(accounts)) {
            const i = expected.indexOf(account.toString());
            if (i > -1) {
                expected.splice(i, 1);
            } else {
                console.log(false);
                break test;
            }
        }
        console.log(expected.length == 0);
    }
}
