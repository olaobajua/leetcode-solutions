"""
 * 1044. Longest Duplicate Substring (Hard)
 * Given a string s, consider all duplicated substrings: (contiguous)
 * substrings of s that occur 2 or more times. The occurrences may overlap.
 *
 * Return any duplicated substring that has the longest possible length.
 * If s does not have a duplicated substring, the answer is "".
 *
 * Example 1:
 * Input: s = "banana"
 * Output: "ana"
 *
 * Example 2:
 * Input: s = "abcd"
 * Output: ""
 *
 * Constraints:
 *     2 <= s.length <= 3 * 10⁴
 *     s consists of lowercase English letters.
"""
from collections import defaultdict
from functools import reduce

class Solution:
    def longestDupSubstring(self, s: str) -> str:
        start = 0
        end = len(s)
        ret = ""
        while start <= end:
            middle = (start + end) // 2
            if (cur := rabin_karp_set(s, middle)):
                ret = cur
                start = middle + 1
            else:
                end = middle - 1
        return ret

def rabin_karp_set(s, m):
    """
    The Rabin–Karp algorithm for multiple pattern search.
    https://en.wikipedia.org/wiki/Rabin%E2%80%93Karp_algorithm
    """
    if m == 0:
        return ""
    Q = (1 << 31) - 1
    X = 256
    hash_key = reduce(lambda acc, c: (acc * X + ord(c)) % Q, s[:m], 0)
    mul = pow(X, m - 1, Q)
    hashes = defaultdict(list)
    hashes[hash_key].append((0, m))
    for i in range(1, len(s) - m + 1):
        hash_key = ((hash_key - ord(s[i-1]) * mul) * X + ord(s[i+m-1])) % Q
        for start, end in hashes[hash_key]:
            if (sub := s[i:i+m]) == s[start:end]:
                return sub;
        hashes[hash_key].append((i, i + m))
    return ""

if __name__ == "__main__":
    tests = (
        ("banana", "ana"),
        ("abcd", ""),
        ("abcdabc", "abc"),
        ("aa", "a"),
        ("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
    )
    for s, expected in tests:
        print(Solution().longestDupSubstring(s) == expected, flush=True)
