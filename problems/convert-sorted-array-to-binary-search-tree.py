"""
 * 108. Convert Sorted Array to Binary Search Tree [Easy]
 * Given an integer array nums where the elements are sorted in ascending
 * order, convert it to a height-balanced binary search tree.
 *
 * A height-balanced binary tree is a binary tree in which the depth of the
 * two subtrees of every node never differs by more than one.
 *
 * Example 1:
 * Input: nums = [-10,-3,0,5,9]
 * Output: [0,-3,9,-10,null,5]
 * Explanation: [0,-10,5,null,-3,null,9] is also accepted:
 * Example 2:
 * Input: nums = [1,3]
 * Output: [3,1]
 * Explanation: [1,null,3] and [3,1] are both height-balanced BSTs.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁴
 *     ∙ -10⁴ <= nums[i] <= 10⁴
 *     ∙ nums is sorted in a strictly increasing order.
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> Optional[TreeNode]:
        if not nums:
            return None
        median = len(nums) // 2
        return TreeNode(nums[median],
                        self.sortedArrayToBST(nums[:median]),
                        self.sortedArrayToBST(nums[median+1:]))

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([-10,-3,0,5,9], [0,-3,9,-10,None,5]),
        ([1,3], [3,1]),
    )
    for nums, expected in tests:
        ret = [*tree_to_list(Solution().sortedArrayToBST(nums))]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
