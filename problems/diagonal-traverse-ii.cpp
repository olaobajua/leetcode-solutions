/**
 * 1424. Diagonal Traverse II [Medium]
 * Given a 2D integer array nums, return all elements of nums in diagonal
 * order as shown in the below images.
 *
 * Example 1:
 * Input: nums = [[1,2,3],
 *                [4,5,6],
 *                [7,8,9]]
 * Output: [1,4,2,7,5,3,8,6,9]
 *
 * Example 2:
 * Input: nums = [[1,2,3,4,5],
 *                [6,7],
 *                [8],
 *                [9,10,11],
 *                [12,13,14,15,16]]
 * Output: [1,6,2,8,7,3,9,4,12,10,5,13,11,14,15,16]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i].length <= 10⁵
 *     ∙ 1 <= sum(nums[i].length) <= 10⁵
 *     ∙ 1 <= nums[i][j] <= 10⁵
 */
#include <vector>
#include <algorithm>

class Solution {
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& nums) {
        std::vector<std::tuple<int, int, int>> indices;
        for (int i = 0; i < nums.size(); ++i) {
            for (int j = 0; j < nums[i].size(); ++j) {
                indices.push_back(std::make_tuple(i + j, j, nums[i][j]));
            }
        }

        std::sort(indices.begin(), indices.end());

        std::vector<int> result;
        for (const auto& tuple : indices) {
            result.push_back(std::get<2>(tuple));
        }

        return result;
    }
};
