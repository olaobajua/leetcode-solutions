"""
 * 1339. Maximum Product of Splitted Binary Tree [Medium]
 * Given the root of a binary tree, split the binary tree into two subtrees by
 * removing one edge such that the product of the sums of the subtrees is
 * maximized.
 *
 * Return the maximum product of the sums of the two subtrees. Since the
 * answer may be too large, return it modulo 10⁹ + 7.
 *
 * Note that you need to maximize the answer before taking the mod and not
 * after taking it.
 *
 * Example 1:
 *     1
 *  2     3
 * 4 5   6
 * Input: root = [1,2,3,4,5,6]
 * Output: 110
 * Explanation: Remove the red edge and get 2 binary trees with sum 11 and 10.
 * Their product is 110 (11*10)
 *
 * Example 2:
 * 1
 *   2
 * 3   4
 *    5 6
 * Input: root = [1,null,2,3,4,null,null,5,6]
 * Output: 90
 * Explanation: Remove the red edge and get 2 binary trees with sum 15 and
 * 6.Their product is 90 (15*6)
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [2, 5 * 10⁴].
 *     ∙ 1 <= Node.val <= 10⁴
"""
from functools import cache
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def maxProduct(self, root: Optional[TreeNode]) -> int:
        @cache
        def subtree_sum(node):
            if node is None:
                return 0
            return node.val + subtree_sum(node.left) + subtree_sum(node.right)

        def dp(node):
            if node is None:
                return 0
            return max(subtree_sum(node) * (total - subtree_sum(node)),
                       dp(node.left), dp(node.right))

        total = subtree_sum(root)
        return dp(root) % 1000000007

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5,6], 110),
        ([1,None,2,3,4,None,None,5,6], 90),
    )
    for nums, expected in tests:
        print(Solution().maxProduct(build_tree(nums)) == expected)
