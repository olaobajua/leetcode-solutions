/**
 * 382. Linked List Random Node (Medium)
 * Given a singly linked list, return a random node's value from the linked
 * list. Each node must have the same probability of being chosen.
 *
 * Implement the Solution class:
 *     ∙ Solution(ListNode head) Initializes the object with the integer array
 *       nums.
 *     ∙ int getRandom() Chooses a node randomly from the list and returns its
 *       value. All the nodes of the list should be equally likely to be
 *       choosen.
 *
 * Example 1:
 * Input
 * ["Solution", "getRandom", "getRandom", "getRandom", "getRandom",
 *  "getRandom"]
 * [[[1, 2, 3]], [], [], [], [], []]
 * Output
 * [null, 1, 3, 2, 2, 3]
 *
 * Explanation
 * Solution solution = new Solution([1, 2, 3]);
 * solution.getRandom(); // return 1
 * solution.getRandom(); // return 3
 * solution.getRandom(); // return 2
 * solution.getRandom(); // return 2
 * solution.getRandom(); // return 3
 * // getRandom() should return either 1, 2, or 3 randomly. Each element should
 * have equal probability of returning.
 *
 * Constraints:
 *     ∙ The number of nodes in the linked list will be in the range [1, 10⁴].
 *     ∙ -10⁴ <= Node.val <= 10⁴
 *     ∙ At most 10⁴ calls will be made to getRandom.
 *
 * Follow up:
 *     ∙ What if the linked list is extremely large and its length is unknown
 *       to you?
 *     ∙ Could you solve this efficiently without using extra space?
 */

// Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode *next;
};

typedef struct {
    struct ListNode *head;
} Solution;

Solution* solutionCreate(struct ListNode *head) {
   Solution *ret = malloc(sizeof(Solution));
   ret->head = head;
   return ret;
}

int solutionGetRandom(Solution *obj) {
    // https://en.wikipedia.org/wiki/Reservoir_sampling
    int scope = 1, chosen = 0;
    struct ListNode *cur = obj->head;
    while (cur) {
        // decide whether to include the element in reservoir
        if ((double)rand() / (double)((unsigned)RAND_MAX + 1) < 1.0 / scope) {
            chosen = cur->val;
        }
        cur = cur->next;
        ++scope;
    }
    return chosen;
}

void solutionFree(Solution *obj) {
    free(obj);
}

/**
 * Your Solution struct will be instantiated and called as such:
 * Solution* obj = solutionCreate(head);
 * int param_1 = solutionGetRandom(obj);

 * solutionFree(obj);
*/
