/**
 * 2157. Groups of Strings (Hard)
 * You are given a 0-indexed array of strings words. Each string consists of
 * lowercase English letters only. No letter occurs more than once in any
 * string of words.
 *
 * Two strings s1 and s2 are said to be connected if the set of letters of s2
 * can be obtained from the set of letters of s1 by any one of the following
 * operations:
 *     ∙ Adding exactly one letter to the set of the letters of s1.
 *     ∙ Deleting exactly one letter from the set of the letters of s1.
 *     ∙ Replacing exactly one letter from the set of the letters of s1 with
 *       any letter, including itself.
 *
 * The array words can be divided into one or more non-intersecting groups.
 * A string belongs to a group if any one of the following is true:
 *     ∙ It is connected to at least one other string of the group.
 *     ∙ It is the only string present in the group.
 *
 * Note that the strings in words should be grouped in such a manner that a
 * string belonging to a group cannot be connected to a string present in any
 * other group. It can be proved that such an arrangement is always unique.
 *
 * Return an array ans of size 2 where:
 *     ∙ ans[0] is the total number of groups words can be divided into, and
 *     ∙ ans[1] is the size of the largest group.
 *
 * Example 1:
 * Input: words = ["a","b","ab","cde"]
 * Output: [2,3]
 * Explanation:
 * - words[0] can be used to obtain words[1] (by replacing 'a' with 'b'), and
 *   words[2] (by adding 'b'). So words[0] is connected to words[1] and
 *   words[2].
 * - words[1] can be used to obtain words[0] (by replacing 'b' with 'a'), and
 *   words[2] (by adding 'a'). So words[1] is connected to words[0] and
 *   words[2].
 * - words[2] can be used to obtain words[0] (by deleting 'b'), and words[1]
 *   (by deleting 'a'). So words[2] is connected to words[0] and words[1].
 * - words[3] is not connected to any string in words.
 * Thus, words can be divided into 2 groups ["a","b","ab"] and ["cde"].
 * The size of the largest group is 3.
 *
 * Example 2:
 * Input: words = ["a","ab","abc"]
 * Output: [1,3]
 * Explanation:
 * - words[0] is connected to words[1].
 * - words[1] is connected to words[0] and words[2].
 * - words[2] is connected to words[1].
 * Since all strings are connected to each other, they should be grouped
 * together.
 * Thus, the size of the largest group is 3.
 *
 * Constraints:
 *     ∙ 1 <= words.length <= 2 * 10⁴
 *     ∙ 1 <= words[i].length <= 26
 *     ∙ words[i] consists of lowercase English letters only.
 *     ∙ No letter occurs more than once in words[i].
 */

/**
 * @param {string[]} words
 * @return {number[]}
 */
var groupStrings = function(words) {
    const a = 'a'.charCodeAt(0);
    const charBitmask = (char) => 1 << (char.charCodeAt() - a);
    const groups = new DisjointSet(words.length);
    const lookup = new Map();
    words.forEach((word, i) => {
        let mask = [...word].reduce((m, char) =>  m + charBitmask(char), 0);
        for (let j = 0; j < 26; ++j) {
            if (mask & (1 << j)) {
                const removed = mask ^ (1 << j);
                if (lookup.has(removed)) {
                    groups.union(i, lookup.get(removed));
                }

                const replaced = removed | (1 << 31);
                if (lookup.has(replaced)) {
                    groups.union(i, lookup.get(replaced));
                } else {
                    lookup.set(replaced, i);
                }
            } else {
                const added = mask | (1 << j);
                if (lookup.has(added)) {
                    groups.union(i, lookup.get(added));
                }
            }
        }
        lookup.set(mask, i);
    });

    const connected = {};
    for (let i = 0; i < words.length; ++i) {
        const parent = groups.find(i);
        connected[parent] = (connected[parent] || 0) + 1;
    };

    return [Object.keys(connected).length,
            Math.max(...Object.values(connected))];
};

class DisjointSet {
    constructor(n) {
        Object.defineProperty(this, 'parent', {
            value: [...new Array(n)].map((_, i) => i)
        });
    }
    find(x) {
        if (x != this.parent[x]) {
            this.parent[x] = this.find(this.parent[x]);
        }
        return this.parent[x];
    }
    union(x, y) {
        const px = this.find(x);
        const py = this.find(y);
        if (px !== undefined && py !== undefined && px != py) {
            this.parent[px] = py;
        }
    }
}

const tests = [
    [["a","b","ab","cde"], [2,3]],
    [["a","ab","abc"], [1,3]],
    [["web","a","te","hsx","v","k","a","roh"], [5,4]],
];

for (const [words, expected] of tests) {
    console.log(groupStrings(words).toString() == expected.toString());
}
