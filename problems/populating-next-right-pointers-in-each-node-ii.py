"""
 * 117. Populating Next Right Pointers in Each Node II (Medium)
 * Given a binary tree
 *     struct Node {
 *       int val;
 *       Node *left;
 *       Node *right;
 *       Node *next;
 *     }
 *
 * Populate each next pointer to point to its next right node. If there is no
 * next right node, the next pointer should be set to NULL.
 *
 * Initially, all next pointers are set to NULL.
 *
 * Example 1:
 * Input: root = [1,2,3,4,5,null,7]
 * Output: [1,#,2,3,#,4,5,7,#]
 * Explanation: Given the above binary tree (Figure A), your function should
 * populate each next pointer to point to its next right node, just like in
 * Figure B. The serialized output is in level order as connected by the next
 * pointers, with '#' signifying the end of each level.
 *
 * Example 2:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 6000].
 *     ∙ -100 <= Node.val <= 100
 *
 * Follow-up:
 *     ∙ You may only use constant extra space.
 *     ∙ The recursive approach is fine. You may assume implicit stack space
 *       does not count as extra space for this problem.
"""
from typing import List

# Definition for a Node.
class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next

class Solution:
    def connect(self, root: 'Node') -> 'Node':
        node = root
        while node:
            curr = dummy = Node()
            while node:
                if node.left:
                    curr.next = node.left
                    curr = curr.next
                if node.right:
                    curr.next = node.right
                    curr = curr.next
                node = node.next
            node = dummy.next

        return root

def build_tree(vals):
    root = None
    try:
        root = Node(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = Node(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = Node(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    if not root:
        return []
    ret = []
    level = [root]
    while level:
        ret.extend([n.val if n else None for n in level])
        ret.append('#')
        level = [node for n in level if n for node in (n.left, n.right) if node]
    return ret

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5,None,7], [1,'#',2,3,'#',4,5,7,'#']),
        ([], []),
    )
    for nums, expected in tests:
        root = Solution().connect(build_tree(nums))
        tree_to_list(root)
        print(tree_to_list(root) == expected)
