"""
 * 130. Surrounded Regions (Medium)
 * Given an m x n matrix board containing 'X' and 'O', capture all regions that
 * are 4-directionally surrounded by 'X'.
 *
 * A region is captured by flipping all 'O's into 'X's in that surrounded
 * region.
 *
 * Example 1:
 * Input: board = [["X","X","X","X"],
 *                 ["X","O","O","X"],
 *                 ["X","X","O","X"],
 *                 ["X","O","X","X"]]
 * Output: [["X","X","X","X"],
 *          ["X","X","X","X"],
 *          ["X","X","X","X"],
 *          ["X","O","X","X"]]
 * Explanation: Surrounded regions should not be on the border, which means
 * that any 'O' on the border of the board are not flipped to 'X'. Any 'O' that
 * is not on the border and it is not connected to an 'O' on the border will be
 * flipped to 'X'. Two cells are connected if they are adjacent cells connected
 * horizontally or vertically.
 *
 * Example 2:
 * Input: board = [["X"]]
 * Output: [["X"]]
 *
 * Constraints:
 *     m == board.length
 *     n == board[i].length
 *     1 <= m, n <= 200
 *     board[i][j] is 'X' or 'O'.
"""
from typing import List

class Solution:
    def solve(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        n = len(board)
        m = len(board[0])
        keep = [pos for i in range(max(m,n))
                for pos in ((0, i), (n-1, i), (i, 0), (i, m-1))]
        while keep:  # flood_fill via BFS
            row, col = keep.pop()
            if 0 <= row < n and 0 <= col < m and board[row][col] == 'O':
                board[row][col] = 'B'
                keep += (row, col-1), (row, col+1), (row-1, col), (row+1, col)
        board[:] = [['XO'[cell == 'B'] for cell in row] for row in board]

if __name__ == "__main__":
    tests = (
        ([["X","X","X","X"],
          ["X","O","O","X"],
          ["X","X","O","X"],
          ["X","O","X","X"]],
         [["X","X","X","X"],
          ["X","X","X","X"],
          ["X","X","X","X"],
          ["X","O","X","X"]]),
        ([["X"]],
         [["X"]]),
        ([["X","O","X","O","X","O"],
          ["O","X","O","X","O","X"],
          ["X","O","X","O","X","O"],
          ["O","X","O","X","O","X"]],
         [["X","O","X","O","X","O"],
          ["O","X","X","X","X","X"],
          ["X","X","X","X","X","O"],
          ["O","X","O","X","O","X"]]),
    )
    for board, expected in tests:
        Solution().solve(board)
        print(board == expected)
