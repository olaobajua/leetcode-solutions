/**
 * 1291. Sequential Digits (Medium)
 * An integer has sequential digits if and only if each digit in the number is
 * one more than the previous digit.
 *
 * Return a sorted list of all the integers in the range [low, high] inclusive
 * that have sequential digits.
 *
 * Example 1:
 * Input: low = 100, high = 300
 * Output: [123,234]
 *
 * Example 2:
 * Input: low = 1000, high = 13000
 * Output: [1234,2345,3456,4567,5678,6789,12345]
 *
 * Constraints:
 *     ∙ 10 <= low <= high <= 10⁹
 */

/**
 * @param {number} low
 * @param {number} high
 * @return {number[]}
 */
var sequentialDigits = function(low, high) {
    const ret = [];
    for (let width = Math.floor(Math.log10(low)) + 1;
             width <= Math.floor(Math.log10(high)) + 1;
             ++width) {
        for (let start = 0; start < 10 - width; ++start) {
            let n = 0;
            for (let i = start; i < start + width; ++i) {
                n = n * 10 + i + 1;
            }
            if (low <= n && n <= high) {
                ret.push(n);
            }
        }
    }
    return ret;
};

const tests = [
    [100, 300, [123,234]],
    [123, 234, [123,234]],
    [1000, 13000, [1234,2345,3456,4567,5678,6789,12345]],
    [10, 1000000000, [12,23,34,45,56,67,78,89,123,234,345,456,567,678,789,1234,2345,3456,4567,5678,6789,12345,23456,34567,45678,56789,123456,234567,345678,456789,1234567,2345678,3456789,12345678,23456789,123456789]],
];

for (const [low, high, expected] of tests) {
    console.log(sequentialDigits(low, high).toString() == expected.toString());
}
