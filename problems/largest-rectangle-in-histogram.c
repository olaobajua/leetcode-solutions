/**
 * 84. Largest Rectangle in Histogram (Hard)
 * Given an array of integers heights representing the histogram's bar height
 * where the width of each bar is 1, return the area of the largest rectangle
 * in the histogram.
 *
 * Example 1:
 * Input: heights = [2,1,5,6,2,3]
 * Output: 10
 * Explanation: The above is a histogram where width of each bar is 1.
 * The largest rectangle is shown in the red area, which has an area = 10 units.
 *
 * Example 2:
 * Input: heights = [2,4]
 * Output: 4
 *
 * Constraints:
 *     1 <= heights.length <= 10⁵
 *     0 <= heights[i] <= 10⁴
 */
int largestRectangleArea(int *heights, int heightsSize) {
    static int stack[100000];
    int h, w, n = 0, maxRect = 0;
    for (int i = 0; i < heightsSize; ++i) {
        while (n && heights[i] < heights[stack[n-1]]) {
            h = heights[stack[--n]];
            w = n ? i - stack[n-1] - 1 : i;
            maxRect = fmax(maxRect, h * w);
        }
        stack[n++] = i;
    }
    while (n && 0 < heights[stack[n-1]]) {
        h = heights[stack[--n]];
        w = n ? heightsSize - stack[n-1] - 1 : heightsSize;
        maxRect = fmax(maxRect, h * w);
    }
    return maxRect;
}
