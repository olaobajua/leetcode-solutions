/**
 * 2115. Find All Possible Recipes from Given Supplies (Medium)
 * You have information about n different recipes. You are given a string array
 * recipes and a 2D string array ingredients. The iᵗʰ recipe has the name
 * recipes[i], and you can create it if you have all the needed ingredients
 * from ingredients[i]. Ingredients to a recipe may need to be created from
 * other recipes, i.e., ingredients[i] may contain a string that is in recipes.
 *
 * You are also given a string array supplies containing all the ingredients
 * that you initially have, and you have an infinite supply of all of them.
 *
 * Return a list of all the recipes that you can create. You may return the
 * answer in any order.
 *
 * Note that two recipes may contain each other in their ingredients.
 *
 * Example 1:
 * Input: recipes = ["bread"],
 *        ingredients = [["yeast","flour"]],
 *        supplies = ["yeast","flour","corn"]
 * Output: ["bread"]
 * Explanation:
 * We can create "bread" since we have the ingredients "yeast" and "flour".
 *
 * Example 2:
 * Input: recipes = ["bread","sandwich"],
 *        ingredients = [["yeast","flour"],["bread","meat"]],
 *        supplies = ["yeast","flour","meat"]
 * Output: ["bread","sandwich"]
 * Explanation:
 * We can create "bread" since we have the ingredients "yeast" and "flour".
 * We can create "sandwich" since we have the ingredient "meat" and can create
 * the ingredient "bread".
 *
 * Example 3:
 * Input: recipes = ["bread","sandwich","burger"],
 *                  ingredients = [["yeast","flour"],
 *                                 ["bread","meat"],
 *                                 ["sandwich","meat","bread"]],
 *                  supplies = ["yeast","flour","meat"]
 * Output: ["bread","sandwich","burger"]
 * Explanation:
 * We can create "bread" since we have the ingredients "yeast" and "flour".
 * We can create "sandwich" since we have the ingredient "meat" and can create
 * the ingredient "bread".
 * We can create "burger" since we have the ingredient "meat" and can create
 * the ingredients "bread" and "sandwich".
 *
 * Constraints:
 *     ∙ n == recipes.length == ingredients.length
 *     ∙ 1 <= n <= 100
 *     ∙ 1 <= ingredients[i].length, supplies.length <= 100
 *     ∙ 1 <= recipes[i].length, ingredients[i][j].length,
 *       supplies[k].length <= 10
 *     ∙ recipes[i], ingredients[i][j], and supplies[k] consist only of
 *       lowercase English letters.
 *     ∙ All the values of recipes and supplies combined are unique.
 *     ∙ Each ingredients[i] does not contain any duplicate values.
 */

/**
 * @param {string[]} recipes
 * @param {string[][]} ingredients
 * @param {string[]} supplies
 * @return {string[]}
 */
var findAllRecipes = function(recipes, ingredients, supplies) {
    const requires = {};
    const visited = {};
    const recipesIndices = {};
    for (let i = 0; i < recipes.length; ++i) {
        requires[recipes[i]] =
            new Set(ingredients[i].filter(ingred => recipes.includes(ingred)));
        visited[recipes[i]] = false;
        recipesIndices[recipes[i]] = i;
    }
    recipes = [];
    for (const recipe of Object.keys(requires)) {
        if (!visited[recipe]) {
            topologicalSortDfs(requires, recipe, visited, recipes);
        }
    }
    const ret = [];
    supplies = new Set(supplies);
    for (const recipe of recipes) {
        checkRecipe: {
            for (ingredient of ingredients[recipesIndices[recipe]]) {
                if (!supplies.has(ingredient)) {
                    break checkRecipe;
                }
            }
            ret.push(recipe);
            supplies.add(recipe);
        }
    }
    return ret;
};

function topologicalSortDfs(graph, cur, visited, ret) {
    // https://en.wikipedia.org/wiki/Topological_sorting#Depth-first_search
    if (visited[cur] == 'temporary') {
        return false;   // directed cycle graph, can't be topologically sorted
    } else if (!visited[cur]) {
        visited[cur] = 'temporary';
        for (const course of graph[cur] || []) {
            if (!topologicalSortDfs(graph, course, visited, ret)) {
                return false;
            }
        }
        visited[cur] = 'permanent';
        ret.push(cur);
    }
    return true;
}

const tests = [
    [["bread"],
     [["yeast","flour"]],
     ["yeast","flour","corn"],
     ["bread"]],
    [["bread","sandwich"],
     [["yeast","flour"],["bread","meat"]],
     ["yeast","flour","meat"],
     ["bread","sandwich"]],
    [["bread","sandwich","burger"],
     [["yeast","flour"],["bread","meat"],["sandwich","meat","bread"]],
     ["yeast","flour","meat"],
     ["bread","sandwich","burger"]],
    [["ju","fzjnm","x","e","zpmcz","h","q"],
     [["d"],["hveml","f","cpivl"],["cpivl","zpmcz","h","e","fzjnm","ju"],["cpivl","hveml","zpmcz","ju","h"],["h","fzjnm","e","q","x"],["d","hveml","cpivl","q","zpmcz","ju","e","x"],["f","hveml","cpivl"]],
     ["f","hveml","cpivl","d"],
     ['ju', 'fzjnm', 'q']],
    [["xevvq","izcad","p","we","bxgnm","vpio","i","hjvu","igi","anp","tokfq","z","kwdmb","g","qb","q","b","hthy"],
     [["wbjr"],["otr","fzr","g"],["fzr","wi","otr","xgp","wbjr","igi","b"],["fzr","xgp","wi","otr","tokfq","izcad","igi","xevvq","i","anp"],["wi","xgp","wbjr"],["wbjr","bxgnm","i","b","hjvu","izcad","igi","z","g"],["xgp","otr","wbjr"],["wbjr","otr"],["wbjr","otr","fzr","wi","xgp","hjvu","tokfq","z","kwdmb"],["xgp","wi","wbjr","bxgnm","izcad","p","xevvq"],["bxgnm"],["wi","fzr","otr","wbjr"],["wbjr","wi","fzr","xgp","otr","g","b","p"],["otr","fzr","xgp","wbjr"],["xgp","wbjr","q","vpio","tokfq","we"],["wbjr","wi","xgp","we"],["wbjr"],["wi"]],
     ["wi","otr","wbjr","fzr","xgp"],
     ["xevvq","izcad","bxgnm","i","hjvu","tokfq","z","g","b","hthy"]],
];

for (const [recipes, ingredients, supplies, expected] of tests) {
    console.log(
        findAllRecipes(recipes, ingredients, supplies).sort().toString() ==
        expected.sort().toString());
}
