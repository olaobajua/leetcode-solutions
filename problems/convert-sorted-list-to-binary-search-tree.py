"""
 * 109. Convert Sorted List to Binary Search Tree [Medium]
 * Given the head of a singly linked list where elements are sorted in
 * ascending order, convert it to a height-balanced binary search tree.
 *
 * Example 1:
 *      0
 *   -3   9
 * -10   5
 * Input: head = [-10,-3,0,5,9]
 * Output: [0,-3,9,-10,null,5]
 * Explanation: One possible answer is [0,-3,9,-10,null,5], which represents
 * the shown height balanced BST.
 *
 * Example 2:
 * Input: head = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in head is in the range [0, 2 * 10⁴].
 *     ∙ -10⁵ <= Node.val <= 10⁵
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def sortedListToBST(self, head: Optional[ListNode]) -> Optional[TreeNode]:
        def build_bst(nums):
            if nums:
                mid = len(nums) // 2
                return TreeNode(nums[mid],
                                build_bst(nums[:mid]),
                                build_bst(nums[mid+1:]))

        nums = []
        while head:
            nums.append(head.val)
            head = head.next
        return build_bst(nums)

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([-10,-3,0,5,9], [0,-3,9,-10,None,5]),
        ([], []),
    )
    for nums, expected in tests:
        head = list_to_linked_list(nums)
        ret = [*tree_to_list(Solution().sortedListToBST(head))]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
