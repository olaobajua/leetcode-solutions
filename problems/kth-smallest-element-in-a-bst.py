"""
* 230. Kth Smallest Element in a BST (Medium)
* Given the root of a binary search tree, and an integer k, return the kth
* smallest value (1-indexed) of all the values of the nodes in the tree.
*
* Example 1:
* Input: root = [3,1,4,null,2], k = 1
* Output: 1
*
* Example 2:
* Input: root = [5,3,6,2,4,null,null,1], k = 3
* Output: 3
*
* Constraints:
*     ∙ The number of nodes in the tree is n.
*     ∙ 1 <= k <= n <= 10⁴
*     ∙ 0 <= Node.val <= 10⁴
*
* Follow up: If the BST is modified often (i.e., we can do insert and delete
* operations) and you need to find the kth smallest frequently, how would you
* optimize?
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def kthSmallest(self, root: Optional[TreeNode], k: int) -> int:
        for val in self.traverse_inorder(root):
            if (k := k - 1) == 0:
                return val

    def traverse_inorder(self, root):
        if root is None:
            return
        for val in self.traverse_inorder(root.left):
            yield val
        yield root.val
        for val in self.traverse_inorder(root.right):
            yield val

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([3,1,4,None,2], 1, 1),
        ([5,3,6,2,4,None,None,1], 3, 3),
    )
    for nums, k, expected in tests:
        print(Solution().kthSmallest( build_tree(nums), k) == expected)
