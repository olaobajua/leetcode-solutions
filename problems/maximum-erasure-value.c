/**
 * 1695. Maximum Erasure Value (Medium)
 * You are given an array of positive integers nums and want to erase a subarray
 * containing unique elements. The score you get by erasing the subarray is
 * equal to the sum of its elements.
 *
 * Return the maximum score you can get by erasing exactly one subarray.
 *
 * An array b is called to be a subarray of a if it forms a contiguous
 * subsequence of a, that is, if it is equal to a[l],a[l+1],...,a[r] for some
 * (l,r).
 *
 * Example 1:
 * Input: nums = [4,2,4,5,6]
 * Output: 17
 * Explanation: The optimal subarray here is [2,4,5,6].
 *
 * Example 2:
 * Input: nums = [5,2,1,2,5,2,1,2,5]
 * Output: 8
 * Explanation: The optimal subarray here is [5,2,1] or [1,2,5].
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i] <= 10⁴
 */
int maximumUniqueSubarray(int *nums, int numsSize) {
    int max = 0;
    for (int i = 0; i < numsSize; ++i)
        if (nums[i] > max)
            max = nums[i];
    int *subarray = calloc(max + 1, sizeof(int));
    int ret = 0, left = 0, sub_sum = 0;
    for (int right = 0; right < numsSize; ++right) {
        while (subarray[nums[right]]) {
            subarray[nums[left]] = false;
            sub_sum -= nums[left++];
        }
        subarray[nums[right]] = true;
        sub_sum += nums[right];
        if (ret < sub_sum)
            ret = sub_sum;
    }
    free(subarray);
    return ret;
}
