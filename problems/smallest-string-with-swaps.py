"""
 * 1202. Smallest String With Swaps (Medium)
 * You are given a string s, and an array of pairs of indices in the string
 * pairs where pairs[i] = [a, b] indicates 2 indices(0-indexed) of the string.
 *
 * You can swap the characters at any pair of indices in the given pairs any
 * number of times.
 *
 * Return the lexicographically smallest string that s can be changed to after
 * using the swaps.
 *
 * Example 1:
 * Input: s = "dcab", pairs = [[0,3],[1,2]]
 * Output: "bacd"
 * Explaination:
 * Swap s[0] and s[3], s = "bcad"
 * Swap s[1] and s[2], s = "bacd"
 *
 * Example 2:
 * Input: s = "dcab", pairs = [[0,3],[1,2],[0,2]]
 * Output: "abcd"
 * Explaination:
 * Swap s[0] and s[3], s = "bcad"
 * Swap s[0] and s[2], s = "acbd"
 * Swap s[1] and s[2], s = "abcd"
 *
 * Example 3:
 * Input: s = "cba", pairs = [[0,1],[1,2]]
 * Output: "abc"
 * Explaination:
 * Swap s[0] and s[1], s = "bca"
 * Swap s[1] and s[2], s = "bac"
 * Swap s[0] and s[1], s = "abc"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ 0 <= pairs.length <= 10⁵
 *     ∙ 0 <= pairs[i][0], pairs[i][1] < s.length
 *     ∙ s only contains lower case English letters.
"""
from collections import defaultdict
from typing import List

class Solution:
    def smallestStringWithSwaps(self, s: str, pairs: List[List[int]]) -> str:
        n = len(s)
        linked = DisjointSet(n)
        for i, j in pairs:
            linked.union(i, j)
        subs = defaultdict(list)
        for g in range(n):
            subs[linked.find(g)].append(g)
        letters = [[s[i] for i in indices] for indices in subs.values()]
        s = list(s)
        for indices, chars in zip(subs.values(), letters):
            for i, c in zip(sorted(indices), sorted(chars)):
                s[i] = c
        return ''.join(s)

class DisjointSet:
    """Optimized for add and union operations."""
    def __init__(self, n):
        self.parent = list(range(n))

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        if (px := self.find(x)) != (py := self.find(y)):
            self.parent[px] = py

if __name__ == "__main__":
    tests = (
        ("dcab", [[0,3],[1,2]], "bacd"),
        ("dcab", [[0,3],[1,2],[0,2]], "abcd"),
        ("cba", [[0,1],[1,2]], "abc"),
    )
    for s, pairs, expected in tests:
        print(Solution().smallestStringWithSwaps(s, pairs) == expected)
