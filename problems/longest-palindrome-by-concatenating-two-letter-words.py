"""
 * 2131. Longest Palindrome by Concatenating Two Letter Words [Medium]
 * You are given an array of strings words. Each element of words consists of
 * two lowercase English letters.
 *
 * Create the longest possible palindrome by selecting some elements from
 * words and concatenating them in any order. Each element can be selected at
 * most once.
 *
 * Return the length of the longest palindrome that you can create. If it is
 * impossible to create any palindrome, return 0.
 *
 * A palindrome is a string that reads the same forward and backward.
 *
 * Example 1:
 * Input: words = ["lc","cl","gg"]
 * Output: 6
 * Explanation: One longest palindrome is "lc" + "gg" + "cl" = "lcggcl", of
 * length 6.
 * Note that "clgglc" is another longest palindrome that can be created.
 *
 * Example 2:
 * Input: words = ["ab","ty","yt","lc","cl","ab"]
 * Output: 8
 * Explanation: One longest palindrome is "ty" + "lc" + "cl" + "yt" =
 * "tylcclyt", of length 8.
 * Note that "lcyttycl" is another longest palindrome that can be created.
 *
 * Example 3:
 * Input: words = ["cc","ll","xx"]
 * Output: 2
 * Explanation: One longest palindrome is "cc", of length 2.
 * Note that "ll" is another longest palindrome that can be created, and so is
 * "xx".
 *
 * Constraints:
 *     ∙ 1 <= words.length <= 10⁵
 *     ∙ words[i].length == 2
 *     ∙ words[i] consists of lowercase English letters.
"""
from collections import Counter
from typing import List

class Solution:
    def longestPalindrome(self, words: List[str]) -> int:
        ret = 0
        count = Counter(words)
        center = False
        for a, b in list(count):
            if a == b:
                ret += 4 * (count[a+b] // 2)
                count[a+b] %= 2
                if count[a+b]:
                    center = True
            else:
                m = min(count[a+b], count[b+a])
                ret += 4 * m
                count[a+b] -= m
                count[b+a] -= m
        return ret + 2 * center

if __name__ == "__main__":
    tests = (
        (["lc","cl","gg"], 6),
        (["ab","ty","yt","lc","cl","ab"], 8),
        (["cc","ll","xx"], 2),
        (["gl"], 0),
        (["dd","aa","bb","dd","aa","dd","bb","dd","aa","cc","bb","cc","dd","cc"], 22),
        (["em","pe","mp","ee","pp","me","ep","em","em","me"], 14),
        (["ll","lb","bb","bx","xx","lx","xx","lx","ll","xb","bx","lb","bb","lb","bl","bb","bx","xl","lb","xx"], 26),
        (["mm","mm","yb","by","bb","bm","ym","mb","yb","by","mb","mb","bb","yb","by","bb","yb","my","mb","ym"], 30),
        (["bb","bb"], 4),
    )
    for words, expected in tests:
        print(Solution().longestPalindrome(words) == expected)
