"""
 * 260. Single Number III (Medium)
 * Given an integer array nums, in which exactly two elements appear only once
 * and all the other elements appear exactly twice. Find the two elements that
 * appear only once. You can return the answer in any order.
 *
 * You must write an algorithm that runs in linear runtime complexity and uses
 * only constant extra space.
 *
 * Example 1:
 * Input: nums = [1,2,1,3,2,5]
 * Output: [3,5]
 * Explanation:  [5, 3] is also a valid answer.
 *
 * Example 2:
 * Input: nums = [-1,0]
 * Output: [-1,0]
 *
 * Example 3:
 * Input: nums = [0,1]
 * Output: [1,0]
 *
 * Constraints:
 *     ∙ 2 <= nums.length <= 3 * 10⁴
 *     ∙ -2³¹ <= nums[i] <= 2³¹ - 1
 *     ∙ Each integer in nums will appear twice, only two integers will appear
 *       once.
"""
from functools import reduce
from operator import xor
from typing import List

class Solution:
    def singleNumber(self, nums: List[int]) -> List[int]:
        xor_nums = reduce(xor, nums)
        lo = xor_nums & (xor_nums - 1) ^ xor_nums
        num1 = reduce(xor, filter(lambda n: n & lo, nums))
        return num1, xor_nums ^ num1

if __name__ == "__main__":
    tests = (
        ([1,2,1,3,2,5], [3,5]),
        ([-1,0], [-1,0]),
        ([0,1], [1,0]),
        ([1,1,0,-2147483648], [0, -2147483648]),
    )
    for nums, expected in tests:
        print(set(Solution().singleNumber(nums)) == set(expected))
