"""
 * 219. Contains Duplicate II [Easy]
 * Given an integer array nums and an integer k, return true if there are two
 * distinct indices i and j in the array such that nums[i] == nums[j] and
 * abs(i - j) <= k.
 *
 * Example 1:
 * Input: nums = [1,2,3,1], k = 3
 * Output: true
 *
 * Example 2:
 * Input: nums = [1,0,1,1], k = 1
 * Output: true
 *
 * Example 3:
 * Input: nums = [1,2,3,1,2,3], k = 2
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ -10⁹ <= nums[i] <= 10⁹
 *     ∙ 0 <= k <= 10⁵
"""
from math import inf
from typing import List

class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        indices = {}
        for i, x in enumerate(nums):
            if i - indices.get(x, -inf) <= k:
                return True
            indices[x] = i
        return False

if __name__ == "__main__":
    tests = (
        ([1,2,3,1], 3, True),
        ([1,0,1,1], 1, True),
        ([1,2,3,1,2,3], 2, False),
    )
    for nums, k, expected in tests:
        print(Solution().containsNearbyDuplicate(nums, k) == expected)
