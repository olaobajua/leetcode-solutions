"""
 * 567. Permutation in String (Medium)
 * Given two strings s1 and s2, return true if s2 contains a permutation of s1,
 * or false otherwise.
 *
 * In other words, return true if one of s1's permutations is the substring of
 * s2.
 *
 * Example 1:
 * Input: s1 = "ab", s2 = "eidbaooo"
 * Output: true
 * Explanation: s2 contains one permutation of s1 ("ba").
 *
 * Example 2:
 * Input: s1 = "ab", s2 = "eidboaoo"
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= s1.length, s2.length <= 10⁴
 *     ∙ s1 and s2 consist of lowercase English letters.
"""
class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        if (n1 := len(s1)) > (n2 := len(s2)):
            return False
        a = ord('a')
        s1 = [ord(char) - a for char in s1]
        s2 = [ord(char) - a for char in s2]
        c1 = [0]*26
        c2 = [0]*26
        for i in range(n1):
            c1[s1[i]] += 1
            c2[s2[i]] += 1
        for i in range(n1, n2):
            if c1 == c2:
                return True
            c2[s2[i]] += 1
            c2[s2[i-n1]] -= 1
        return c1 == c2

if __name__ == "__main__":
    tests = (
        ("ab", "eidbaooo", True),
        ("ab", "eidboaoo", False),
        ("ab", "a", False),
    )
    for s1, s2, expected in tests:
        print(Solution().checkInclusion(s1, s2) == expected)
