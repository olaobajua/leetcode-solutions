"""
 * 606. Construct String from Binary Tree [Easy]
 * Given the root of a binary tree, construct a string consisting of
 * parenthesis and integers from a binary tree with the preorder traversal way,
 * and return it.
 *
 * Omit all the empty parenthesis pairs that do not affect the one-to-one
 * mapping relationship between the string and the original binary tree.
 *
 * Example 1:
 *   1
 *  2 3
 * 4
 * Input: root = [1,2,3,4]
 * Output: "1(2(4))(3)"
 * Explanation: Originally, it needs to be "1(2(4)())(3()())", but you need to
 * omit all the unnecessary empty parenthesis pairs. And it will be
 * "1(2(4))(3)"
 *
 * Example 2:
 *   1
 * 2   3
 *  4
 * Input: root = [1,2,3,null,4]
 * Output: "1(2()(4))(3)"
 * Explanation: Almost the same as the first example, except we cannot omit
 * the first parenthesis pair to break the one-to-one mapping relationship
 * between the input and the output.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ -1000 <= Node.val <= 1000
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def tree2str(self, root: Optional[TreeNode]) -> str:
        if root:
            ret = [str(root.val)]
            if root.right:
                ret.append(f"({self.tree2str(root.left)})")
                ret.append(f"({self.tree2str(root.right)})")
            elif root.left:
                ret.append(f"({self.tree2str(root.left)})")
            return ''.join(ret)
        return ""

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3,4], "1(2(4))(3)"),
        ([1,2,3,None,4], "1(2()(4))(3)"),
    )
    for root, expected in tests:
        print(Solution().tree2str(build_tree(root)) == expected)
