"""
 * 438. Find All Anagrams in a String (Medium)
 * Given two strings s and p, return an array of all the start indices of p's
 * anagrams in s. You may return the answer in any order.
 *
 * An Anagram is a word or phrase formed by rearranging the letters of a
 * different word or phrase, typically using all the original letters exactly
 * once.
 *
 * Example 1:
 * Input: s = "cbaebabacd", p = "abc"
 * Output: [0,6]
 * Explanation:
 * The substring with start index = 0 is "cba", which is an anagram of "abc".
 * The substring with start index = 6 is "bac", which is an anagram of "abc".
 *
 * Example 2:
 * Input: s = "abab", p = "ab"
 * Output: [0,1,2]
 * Explanation:
 * The substring with start index = 0 is "ab", which is an anagram of "ab".
 * The substring with start index = 1 is "ba", which is an anagram of "ab".
 * The substring with start index = 2 is "ab", which is an anagram of "ab".
 *
 * Constraints:
 *     ∙ 1 <= s.length, p.length <= 3 * 10⁴
 *     ∙ s and p consist of lowercase English letters.
"""
from collections import Counter
from typing import List

class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        pc = Counter(p)
        m = len(p)
        sc = Counter(s[:m])
        ret = []
        if sc == pc:
            ret.append(0)
        for i in range(len(s) - m):
            sc[s[i]] -= 1
            sc[s[i+m]] += 1
            if not pc - sc:
                ret.append(i + 1)
        return ret

if __name__ == "__main__":
    tests = (
        ("cbaebabacd", "abc", [0,6]),
        ("abab", "ab", [0,1,2]),
    )
    for s, p, expected in tests:
        print(Solution().findAnagrams(s, p) == expected)
