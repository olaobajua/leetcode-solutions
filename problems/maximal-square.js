/**
 * 221. Maximal Square (Medium)
 * Given an m x n binary matrix filled with 0's and 1's, find the largest
 * square containing only 1's and return its area.
 *
 * Example 1:
 * Input: matrix = [["1","0","1","0","0"],
 *                  ["1","0","1","1","1"],
 *                  ["1","1","1","1","1"],
 *                  ["1","0","0","1","0"]]
 * Output: 4
 *
 * Example 2:
 * Input: matrix = [["0","1"],
 *                  ["1","0"]]
 * Output: 1
 *
 * Example 3:
 * Input: matrix = [["0"]]
 * Output: 0
 *
 * Constraints:
 *     m == matrix.length
 *     n == matrix[i].length
 *     1 <= m, n <= 300
 *     matrix[i][j] is '0' or '1'.
 */

/**
 * @param {character[][]} matrix
 * @return {number}
 */
var maximalSquare = function(matrix) {
    const M = matrix.map(row => row.map(cell => parseInt(cell)));
    const R = M.length;
    const C = M[0].length;
    for (let r = 1; r < R; ++r) {
        for (let c = 1; c < C; ++c) {
            M[r][c] += Math.min(M[r-1][c], M[r][c-1], M[r-1][c-1]) * M[r][c];
        }
    }
    return Math.max(...M.flat())**2;
};

tests = [
    [[["1","0","1","0","0"],
      ["1","0","1","1","1"],
      ["1","1","1","1","1"],
      ["1","0","0","1","0"]], 4],
    [[["1","1","1","0","0"],
      ["1","0","1","1","1"],
      ["1","1","1","0","1"],
      ["1","0","0","1","0"]], 1],
    [[["0","1"],
      ["1","0"]], 1],
    [[["0"]], 0],
    [[["0"],["1"]], 1],
];

for (const [matrix, expected] of tests) {
    console.log(maximalSquare(matrix) == expected);
}
