"""
 * 997. Find the Town Judge [Easy]
 * In a town, there are n people labeled from 1 to n. There is a rumor that
 * one of these people is secretly the town judge.
 *
 * If the town judge exists, then:
 *     ∙ The town judge trusts nobody.
 *     ∙ Everybody (except for the town judge) trusts the town judge.
 *     ∙ There is exactly one person that satisfies properties 1 and 2.
 *
 * You are given an array trust where trust[i] = [aᵢ, bᵢ] representing that
 * the person labeled aᵢ trusts the person labeled bᵢ.
 *
 * Return the label of the town judge if the town judge exists and can be
 * identified, or return -1 otherwise.
 *
 * Example 1:
 * Input: n = 2, trust = [[1,2]]
 * Output: 2
 *
 * Example 2:
 * Input: n = 3, trust = [[1,3],[2,3]]
 * Output: 3
 *
 * Example 3:
 * Input: n = 3, trust = [[1,3],[2,3],[3,1]]
 * Output: -1
 *
 * Constraints:
 *     ∙ 1 <= n <= 1000
 *     ∙ 0 <= trust.length <= 10⁴
 *     ∙ trust[i].length == 2
 *     ∙ All the pairs of trust are unique.
 *     ∙ aᵢ != bᵢ
 *     ∙ 1 <= aᵢ, bᵢ <= n
"""
from typing import List

class Solution:
    def findJudge(self, n: int, trust: List[List[int]]) -> int:
        candidates = [-1] + [0] * n
        for person, trusts_to in trust:
            candidates[person] -= 1
            candidates[trusts_to] += 1
        for person, trusted in enumerate(candidates):
            if trusted == n - 1:
                return person
        return -1

if __name__ == "__main__":
    tests = (
        (2, [[1,2]], 2),
        (3, [[1,3],[2,3]], 3),
        (3, [[1,3],[2,3],[3,1]], -1),
        (4, [[1,3],[1,4],[2,3],[2,4],[4,3]], 3),
        (1, [], 1),
    )
    for n, trust, expected in tests:
        print(Solution().findJudge(n, trust) == expected)
