"""
 * 263. Ugly Number [Easy]
 * An ugly number is a positive integer whose prime factors are limited to 2,
 * 3, and 5.
 *
 * Given an integer n, return true if n is an ugly number.
 *
 * Example 1:
 * Input: n = 6
 * Output: true
 * Explanation: 6 = 2 × 3
 *
 * Example 2:
 * Input: n = 1
 * Output: true
 * Explanation: 1 has no prime factors, therefore all of its prime factors are
 * limited to 2, 3, and 5.
 *
 * Example 3:
 * Input: n = 14
 * Output: false
 * Explanation: 14 is not ugly since it includes the prime factor 7.
 *
 * Constraints:
 *     ∙ -2³¹ <= n <= 2³¹ - 1
"""
class Solution:
    def isUgly(self, n: int) -> bool:
        for i in 2, 3, 5:
            while n % i == 0 < n:
                n /= i
        return n == 1


if __name__ == "__main__":
    tests = (
        (6, True),
        (1, True),
        (14, False),
        (-2147483648, False),
        (0, False),
    )
    for n, expected in tests:
        print(Solution().isUgly(n) == expected)
