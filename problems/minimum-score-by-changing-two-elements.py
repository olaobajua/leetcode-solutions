"""
 * 2567. Minimum Score by Changing Two Elements [Medium]
 * You are given a 0-indexed integer array nums.
 *
 *     ∙ The low score of nums is the minimum value of |nums[i] - nums[j]|
 *       over all 0 <= i < j < nums.length.
 *     ∙ The high score of nums is the maximum value of |nums[i] - nums[j]|
 *       over all 0 <= i < j < nums.length.
 *     ∙ The score of nums is the sum of the high and low scores of nums.
 *
 * To minimize the score of nums, we can change the value of at most two
 * elements of nums.
 *
 * Return the minimum possible score after changing the value of at most two
 * elements of nums.
 *
 * Note that |x| denotes the absolute value of x.
 *
 * Example 1:
 * Input: nums = [1,4,3]
 * Output: 0
 * Explanation: Change value of nums[1] and nums[2] to 1 so that nums becomes
 * [1,1,1]. Now, the value of |nums[i] - nums[j]| is always equal to 0, so we
 * return 0 + 0 = 0.
 *
 * Example 2:
 * Input: nums = [1,4,7,8,5]
 * Output: 3
 * Explanation: Change nums[0] and nums[1] to be 6. Now nums becomes
 * [6,6,7,8,5].
 * Our low score is achieved when i = 0 and j = 1, in which case |nums[i] -
 * nums[j]| = |6 - 6| = 0.
 * Our high score is achieved when i = 3 and j = 4, in which case |nums[i] -
 * nums[j]| = |8 - 5| = 3.
 * The sum of our high and low score is 3, which we can prove to be minimal.
 *
 * Constraints:
 *     ∙ 3 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i] <= 10⁹
"""
from collections import Counter
from typing import List

class Solution:
    def minimizeSum(self, nums: List[int]) -> int:
        count = Counter(nums)
        unique_nums = sorted(count)
        if len(unique_nums) == 1:
            return 0
        if len(unique_nums) == 2:
            lo, hi = unique_nums
            if count[lo] <= 2 or count[hi] <= 2:
                return 0
            else:
                return hi - lo
        lo = unique_nums[0]
        hi = unique_nums[-1]
        if count[lo] == 1 and count[hi] == 1:
            ret = unique_nums[-2] - unique_nums[1]
            lo2 = unique_nums[1]
            hi2 = unique_nums[-2]
            if count[lo2] == 1:
                ret = min(ret, unique_nums[-1] - unique_nums[2])
            if count[hi2] == 1:
                ret = min(ret, unique_nums[-3] - unique_nums[0])
            return ret
        if count[lo] == 2 and count[hi] == 2:
            return min(unique_nums[-1] - unique_nums[1], unique_nums[-2] - unique_nums[0])
        if count[lo] == 1 and count[hi] == 2:
            ret = min(unique_nums[-1] - unique_nums[1], unique_nums[-2] - unique_nums[0])
            lo2 = unique_nums[1]
            if count[lo2] == 1:
                ret = min(ret, unique_nums[-1] - unique_nums[2])
            return ret
        if count[lo] == 2 and count[hi] == 1:
            ret = min(unique_nums[-1] - unique_nums[1], unique_nums[-2] - unique_nums[0])
            hi2 = unique_nums[1]
            if count[hi2] == 1:
                ret = min(ret, unique_nums[-3] - unique_nums[0])
            return ret
        return unique_nums[-1] - unique_nums[0]

if __name__ == "__main__":
    tests = (
        ([1,4,3], 0),
        ([1,4,7,8,5], 3),
        ([31,25,72,79,74,65], 14),
        ([59,27,9,81,33], 24),
        ([21,13,21,72,35,52,74], 39),
        ([1,1,1,1000000000,1000000000,1000000000], 999999999),
    )
    for nums, expected in tests:
        print(Solution().minimizeSum(nums) == expected)
