"""
 * 47. Permutations II (Medium)
 * Given a collection of numbers, nums, that might contain duplicates, return
 * all possible unique permutations in any order.
 *
 * Example 1:
 * Input: nums = [1,1,2]
 * Output:
 * [[1,1,2],
 *  [1,2,1],
 *  [2,1,1]]
 *
 * Example 2:
 * Input: nums = [1,2,3]
 * Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 8
 *     ∙ -10 <= nums[i] <= 10
"""
from itertools import permutations
from typing import List

class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        return set(permutations(nums))

if __name__ == "__main__":
    tests = (
        ([1,1,2], [[1,1,2],[1,2,1],[2,1,1]]),
        ([1,2,3], [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]),
    )
    for nums, expected in tests:
        print(set(map(tuple, Solution().permuteUnique(nums))) == set(map(tuple, expected)))
