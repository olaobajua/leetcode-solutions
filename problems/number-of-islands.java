/**
 * 200. Number of Islands [Medium]
 * Given an m x n 2D binary grid grid which represents a map of '1's (land)
 * and '0's (water), return the number of islands.
 *
 * An island is surrounded by water and is formed by connecting adjacent lands
 * horizontally or vertically. You may assume all four edges of the grid are
 * all surrounded by water.
 *
 * Example 1:
 * Input: grid = [
 *   ["1","1","1","1","0"],
 *   ["1","1","0","1","0"],
 *   ["1","1","0","0","0"],
 *   ["0","0","0","0","0"]
 * ]
 * Output: 1
 *
 * Example 2:
 * Input: grid = [
 *   ["1","1","0","0","0"],
 *   ["1","1","0","0","0"],
 *   ["0","0","1","0","0"],
 *   ["0","0","0","1","1"]
 * ]
 * Output: 3
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 300
 *     ∙ grid[i][j] is '0' or '1'.
 */
class Solution {
    public int numIslands(char[][] grid) {
        int ret = 0;
        for (int row = 0; row < grid.length; ++row)
            for (int col = 0; col < grid[row].length; ++col)
                if (grid[row][col] == '1') {
                    ++ret;
                    floodFill(grid, row, col, '1', '0');
                }
        return ret;
    }
    private void floodFill(char[][] matrix, int row, int col, char o, char n) {
        if (matrix[row][col] == o) {
            matrix[row][col] = n;
            if (row > 0)
                floodFill(matrix, row - 1, col, o, n);
            if (row < matrix.length - 1)
                floodFill(matrix, row + 1, col, o, n);
            if (col > 0)
                floodFill(matrix, row, col - 1, o, n);
            if (col < matrix[row].length - 1)
                floodFill(matrix, row, col + 1, o, n);
        }
    }
}
