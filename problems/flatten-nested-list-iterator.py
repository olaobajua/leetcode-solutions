"""
 * 341. Flatten Nested List Iterator (Medium)
 * You are given a nested list of integers nestedList. Each element is either
 * an integer or a list whose elements may also be integers or other lists.
 * Implement an iterator to flatten it.
 *
 * Implement the NestedIterator class:
 *     ∙ NestedIterator(List<NestedInteger> nestedList) Initializes
 *       the iterator with the nested list nestedList.
 *     ∙ int next() Returns the next integer in the nested list.
 *     ∙ boolean hasNext() Returns true if there are still some integers in
 *       the nested list and false otherwise.
 *
 * Your code will be tested with the following pseudocode:
 *       initialize iterator with nestedList
 *       res = []
 *       while iterator.hasNext()
 *           append iterator.next() to the end of res
 *       return res
 *
 * If res matches the expected flattened list, then your code will be judged
 * as correct.
 *
 * Example 1:
 * Input: nestedList = [[1,1],2,[1,1]]
 * Output: [1,1,2,1,1]
 * Explanation: By calling next repeatedly until hasNext returns false,
 * the order of elements returned by next should be: [1,1,2,1,1].
 *
 * Example 2:
 * Input: nestedList = [1,[4,[6]]]
 * Output: [1,4,6]
 * Explanation: By calling next repeatedly until hasNext returns false,
 * the order of elements returned by next should be: [1,4,6].
 *
 * Constraints:
 *     ∙ 1 <= nestedList.length <= 500
 *     ∙ The values of the integers in the nested list is in
 *       the range [-10⁶, 10⁶].
"""
from typing import Any, List, Union

class NestedInteger:
    def __init__(self, val: Union[int, List]) -> None:
        self._integer = None
        self._list = []
        if isinstance(val, int):
            self._integer = val
        else:
            self._list = [NestedInteger(x) for x in val]

    def __len__(self) -> int:
        return len(self._list)

    def __getitem__(self, i: int) -> Union[int, "NestedInteger"]:
        return self._list[i]

    def isInteger(self) -> bool:
        """
        @return True if this NestedInteger holds a single integer, rather than
        a nested list.
        """
        return self._integer is not None

    def getInteger(self) -> int:
        """
        @return the single integer that this NestedInteger holds, if it holds
        a single integer. Return None if this NestedInteger holds a nested list
        """
        return self._integer

    def getList(self) -> ["NestedInteger"]:
        """
        @return the nested list that this NestedInteger holds, if it holds
        a nested list. Return None if this NestedInteger holds a single integer
        """
        return self._list

class NestedIterator:
    def __init__(self, nestedList: [NestedInteger]):
        def flatten(arr):
            for x in arr:
                if x.isInteger():
                    yield x.getInteger()
                else:
                    yield from flatten(x.getList())

        self.__iter = flatten(nestedList)
        self.__next = next(self.__iter, None)

    def next(self) -> int:
        ret = self.__next
        self.__next = next(self.__iter, None)
        return ret

    def hasNext(self) -> bool:
        return self.__next is not None

if __name__ == "__main__":
    tests = (
        ([[1,1],2,[1,1]], [1,1,2,1,1]),
        ([1,[4,[6]]], [1,4,6]),
        ([[]], []),
        ([0], [0]),
    )
    for nums, expected in tests:
        i = NestedIterator(NestedInteger(nums))
        ret = []
        while i.hasNext():
            ret.append(i.next())
        print(ret == expected)
