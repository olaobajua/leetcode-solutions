/**
 * 740. Delete and Earn (Medium)
 * You are given an integer array nums. You want to maximize the number of
 * points you get by performing the following operation any number of times:
 *     ∙ Pick any nums[i] and delete it to earn nums[i] points. Afterwards, you
 * must delete every element equal to nums[i] - 1 and every element equal to
 * nums[i] + 1.
 *
 * Return the maximum number of points you can earn by applying the above
 * operation some number of times.
 *
 * Example 1:
 * Input: nums = [3,4,2]
 * Output: 6
 * Explanation: You can perform the following operations:
 * - Delete 4 to earn 4 points. Consequently, 3 is also deleted. nums = [2].
 * - Delete 2 to earn 2 points. nums = [].
 * You earn a total of 6 points.
 *
 * Example 2:
 * Input: nums = [2,2,3,3,3,4]
 * Output: 9
 * Explanation: You can perform the following operations:
 * - Delete a 3 to earn 3 points. All 2's and 4's are also deleted. nums = [3,3].
 * - Delete a 3 again to earn 3 points. nums = [3].
 * - Delete a 3 once more to earn 3 points. nums = [].
 * You earn a total of 9 points.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 2 * 10⁴
 *     ∙ 1 <= nums[i] <= 10⁴
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var deleteAndEarn = function(nums) {
    const n = Math.max(...nums) + 1;
    const sums = Array(n).fill(0);
    nums.forEach(x => sums[x] += x);
    const dp = Array(n).fill(0);
    for (let i = Math.min(...nums); i < n; ++i) {
        dp[i+1] = Math.max(sums[i] + dp[i-1], dp[i]);
    }
    return dp[dp.length-1];
};

const tests = [
    [[3,4,2], 6],
    [[2,2,3,3,3,4], 9],
    [[8,10,4,9,1,3,5,9,4,10], 37],
    [[4,10,10,8,1,4,10,9,7,6], 53],
];

for (const [nums, expected] of tests) {
    console.log(deleteAndEarn(nums) == expected);
}
