"""
 * 460. LFU Cache [Hard]
 * Design and implement a data structure for a Least Frequently Used (LFU)
 * (https://en.wikipedia.org/wiki/Least_frequently_used) cache.
 *
 * Implement the LFUCache class:
 *     ∙ LFUCache(int capacity) Initializes the object with the capacity of
 *       the data structure.
 *     ∙ int get(int key) Gets the value of the key if the key exists in the
 *       cache. Otherwise, returns -1.
 *     ∙ void put(int key, int value) Update the value of the key if present,
 *       or inserts the key if not already present. When the cache reaches its
 *       capacity, it should invalidate and remove the least frequently used
 *       key before inserting a new item. For this problem, when there is a tie
 *       (i.e., two or more keys with the same frequency), the least recently
 *       used key would be invalidated.
 *
 * To determine the least frequently used key, a use counter is maintained for
 * each key in the cache. The key with the smallest use counter is the least
 * frequently used key.
 *
 * When a key is first inserted into the cache, its use counter is set to 1
 * (due to the put operation). The use counter for a key in the cache is
 * incremented either a get or put operation is called on it.
 *
 * The functions get and put must each run in O(1) average time complexity.
 *
 * Example 1:
 * Input
 * ["LFUCache", "put", "put", "get", "put", "get", "get", "put", "get", "get",
 * "get"]
 * [[2], [1, 1], [2, 2], [1], [3, 3], [2], [3], [4, 4], [1], [3], [4]]
 * Output
 * [null, null, null, 1, null, -1, 3, null, -1, 3, 4]
 *
 * Explanation
 * // cnt(x) = the use counter for key x
 * // cache=[] will show the last used order for tiebreakers (leftmost element
 * is  most recent)
 * LFUCache lfu = new LFUCache(2);
 * lfu.put(1, 1);   // cache=[1,_], cnt(1)=1
 * lfu.put(2, 2);   // cache=[2,1], cnt(2)=1, cnt(1)=1
 * lfu.get(1);      // return 1
 *                  // cache=[1,2], cnt(2)=1, cnt(1)=2
 * lfu.put(3, 3);   // 2 is the LFU key because cnt(2)=1 is the smallest,
 * invalidate 2.
 *                  // cache=[3,1], cnt(3)=1, cnt(1)=2
 * lfu.get(2);      // return -1 (not found)
 * lfu.get(3);      // return 3
 *                  // cache=[3,1], cnt(3)=2, cnt(1)=2
 * lfu.put(4, 4);   // Both 1 and 3 have the same cnt, but 1 is LRU,
 * invalidate 1.
 *                  // cache=[4,3], cnt(4)=1, cnt(3)=2
 * lfu.get(1);      // return -1 (not found)
 * lfu.get(3);      // return 3
 *                  // cache=[3,4], cnt(4)=1, cnt(3)=3
 * lfu.get(4);      // return 4
 *                  // cache=[4,3], cnt(4)=2, cnt(3)=3
 *
 * Constraints:
 *     ∙ 0 <= capacity <= 10⁴
 *     ∙ 0 <= key <= 10⁵
 *     ∙ 0 <= value <= 10⁹
 *     ∙ At most 2 * 10⁵ calls will be made to get and put.
"""
class Node:
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.freq = 1
        self.prev = None
        self.next = None

class DoublyLinkedList:
    def __init__(self):
        self.head = Node(-1, -1)
        self.tail = Node(-1, -1)
        self.head.next = self.tail
        self.tail.prev = self.head
        self.size = 0

    def add_node(self, node):
        node.prev = self.tail.prev
        node.next = self.tail
        self.tail.prev.next = node
        self.tail.prev = node
        self.size += 1

    def remove_node(self, node):
        node.prev.next = node.next
        node.next.prev = node.prev
        self.size -= 1

class LFUCache:
    def __init__(self, capacity):
        self.capacity = capacity
        self.cache = {}
        self.freq_list = {}
        self.min_freq = 0

    def get(self, key):
        if key not in self.cache:
            return -1
        node = self.cache[key]
        self._update_freq(node)
        return node.value

    def put(self, key, value):
        if self.capacity == 0:
            return
        if key in self.cache:
            node = self.cache[key]
            node.value = value
            self._update_freq(node)
        else:
            if len(self.cache) == self.capacity:
                self._remove_lfu()
            node = Node(key, value)
            self.cache[key] = node
            if 1 not in self.freq_list:
                self.freq_list[1] = DoublyLinkedList()
            self.freq_list[1].add_node(node)
            self.min_freq = 1

    def _update_freq(self, node):
        freq = node.freq
        node_list = self.freq_list[freq]
        node_list.remove_node(node)
        if node_list.size == 0:
            del self.freq_list[freq]
            if freq == self.min_freq:
                self.min_freq += 1
        node.freq += 1
        if node.freq not in self.freq_list:
            self.freq_list[node.freq] = DoublyLinkedList()
        self.freq_list[node.freq].add_node(node)

    def _remove_lfu(self):
        node_list = self.freq_list[self.min_freq]
        node = node_list.head.next
        node_list.remove_node(node)
        del self.cache[node.key]
        if node_list.size == 0:
            del self.freq_list[self.min_freq]

obj = LFUCache(2)
obj.put(1, 1)
obj.put(2, 2)
print(1 == obj.get(1))
obj.put(3, 3)
print(-1 == obj.get(2))
print(3 == obj.get(3))
obj.put(4, 4)
print(-1 == obj.get(1))
print(3 == obj.get(3))
print(4 == obj.get(4))

print()
obj = LFUCache(3)
obj.put(2, 2)
obj.put(1, 1)
print(2 == obj.get(2))
print(1 == obj.get(1))
print(2 == obj.get(2))
obj.put(3, 3)
obj.put(4, 4)
print(-1 == obj.get(3))
print(2 == obj.get(2))
print(1 == obj.get(1))
print(4 == obj.get(4))

print()
obj = LFUCache(0)
obj.put(0, 0)
print(-1 == obj.get(0))

print()
obj = LFUCache(1)
obj.put(2, 1)
print(1 == obj.get(2))
