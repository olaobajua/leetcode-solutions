"""
 * 882. Reachable Nodes In Subdivided Graph (Hard)
 * You are given an undirected graph (the "original graph") with n nodes
 * labeled from 0 to n - 1. You decide to subdivide each edge in the graph into
 * a chain of nodes, with the number of new nodes varying between each edge.
 *
 * The graph is given as a 2D array of edges where edges[i] = [uᵢ, vᵢ, cntᵢ]
 * indicates that there is an edge between nodes uᵢ and vᵢ in the original
 * graph, and cntᵢ is the total number of new nodes that you will subdivide the
 * edge into. Note that cntᵢ == 0 means you will not subdivide the edge.
 *
 * To subdivide the edge [uᵢ, vᵢ], replace it with (cntᵢ + 1) new edges and
 * cntᵢ new nodes. The new nodes are x₁, x₂, ..., xcntᵢ, and the new edges are
 * [uᵢ, x₁], [x₁, x₂], [x₂, x₃], ..., [xcntᵢ₊₁, xcntᵢ], [xcntᵢ, vᵢ].
 *
 * In this new graph, you want to know how many nodes are reachable from the
 * node 0, where a node is reachable if the distance is maxMoves or less.
 *
 * Given the original graph and maxMoves, return the number of nodes that are
 * reachable from node 0 in the new graph.
 *
 * Example 1:
 * Input: edges = [[0,1,10],[0,2,1],[1,2,2]], maxMoves = 6, n = 3
 * Output: 13
 * Explanation: The edge subdivisions are shown in the image above.
 * The nodes that are reachable are highlighted in yellow.
 *
 * Example 2:
 * Input: edges = [[0,1,4],[1,2,6],[0,2,8],[1,3,1]], maxMoves = 10, n = 4
 * Output: 23
 *
 * Example 3:
 * Input: edges = [[1,2,4],[1,4,5],[1,3,1],[2,3,4],[3,4,5]], maxMoves = 17,
 * n = 5
 * Output: 1
 * Explanation: Node 0 is disconnected from the rest of the graph, so only node
 * 0 is reachable.
 *
 * Constraints:
 *     0 <= edges.length <= min(n * (n - 1) / 2, 10⁴)
 *     edges[i].length == 3
 *     0 <= uᵢ < vᵢ < n
 *     There are no multiple edges in the graph.
 *     0 <= cntᵢ <= 10⁴
 *     0 <= maxMoves <= 10⁹
 *     1 <= n <= 3000
"""
from collections import defaultdict
from heapq import heappop, heappush
from math import inf
from typing import List

class Solution:
    def reachableNodes(self, edges: List[List[int]], maxMoves: int, n: int) -> int:
        neighbours = defaultdict(set)
        distances = {}
        for n1, n2, new_between in edges:
            neighbours[n1].add(n2)
            neighbours[n2].add(n1)
            distances[frozenset((n1, n2))] = new_between + 1  # 1 for old
        if not neighbours[0]:
            return 1  # node 0 is disconnected from the rest of the graph

        shortest_paths = dijkstra(neighbours, distances, 0)
        counter = 0
        for n1, n2, new_between in edges:
            reachable_from_n1 = max(maxMoves - shortest_paths[n1], 0)
            reachable_from_n2 = max(maxMoves - shortest_paths[n2], 0)
            counter += min(reachable_from_n1 + reachable_from_n2, new_between)
        counter += sum(shortest_paths[node] <= maxMoves for node in neighbours)
        return counter

def dijkstra(graph: dict, distances: dict, source: int) -> dict:
    shortest_paths = dict.fromkeys(graph, inf)
    shortest_paths[source] = 0
    to_visit = [(0, source)]
    while to_visit:
        parent_dist, root = heappop(to_visit)
        for neib in graph[root]:
            cur_dist = distances[frozenset((root, neib))]
            if parent_dist + cur_dist < shortest_paths[neib]:
                shortest_paths[neib] = parent_dist + cur_dist
                heappush(to_visit, (parent_dist + cur_dist, neib))
    return shortest_paths

if __name__ == "__main__":
    tests = (
        ([[0,1,10],[0,2,1],[1,2,2]], 6, 3, 13),
        ([[0,1,4],[1,2,6],[0,2,8],[1,3,1]], 10, 4, 23),
        ([[1,2,4],[1,4,5],[1,3,1],[2,3,4],[3,4,5]], 17, 5, 1),
        ([[1,2,5],[0,3,3],[1,3,2],[2,3,4],[0,4,1]], 7, 5, 13),
    )
    for edges, maxMoves, n, expected in tests:
        print(Solution().reachableNodes(edges, maxMoves, n) == expected)
