"""
 * 1268. Search Suggestions System (Medium)
 * You are given an array of strings products and a string searchWord.
 *
 * Design a system that suggests at most three product names from products
 * after each character of searchWord is typed. Suggested products should have
 * common prefix with searchWord. If there are more than three products with a
 * common prefix return the three lexicographically minimums products.
 *
 * Return a list of lists of the suggested products after each character of
 * searchWord is typed.
 *
 * Example 1:
 * Input: products = ["mobile","mouse","moneypot","monitor","mousepad"],
 *        searchWord = "mouse"
 * Output: [["mobile","moneypot","monitor"],
 *          ["mobile","moneypot","monitor"],
 *          ["mouse","mousepad"],
 *          ["mouse","mousepad"],
 *          ["mouse","mousepad"]
 * ]
 * Explanation: products sorted lexicographically =
 * ["mobile","moneypot","monitor","mouse","mousepad"]
 * After typing m and mo all products match and we show user
 * ["mobile","moneypot","monitor"]
 * After typing mou, mous and mouse the system suggests ["mouse","mousepad"]
 *
 * Example 2:
 * Input: products = ["havana"], searchWord = "havana"
 * Output: [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
 *
 * Example 3:
 * Input: products = ["bags","baggage","banner","box","cloths"],
 *        searchWord = "bags"
 * Output: [["baggage","bags","banner"],
 *          ["baggage","bags","banner"],
 *          ["baggage","bags"],
 *          ["bags"]]
 *
 * Constraints:
 *     ∙ 1 <= products.length <= 1000
 *     ∙ 1 <= products[i].length <= 3000
 *     ∙ 1 <= sum(products[i].length) <= 2 * 10⁴
 *     ∙ All the strings of products are unique.
 *     ∙ products[i] consists of lowercase English letters.
 *     ∙ 1 <= searchWord.length <= 1000
 *     ∙ searchWord consists of lowercase English letters.
"""
from typing import List

class Solution:
    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:
        trie = Trie()
        for word in sorted(products):
            trie.insert(word)
        trie.start_search()
        return [trie.suggest(char) for char in searchWord]

class Trie:
    """Prefix Tree"""
    def __init__(self):
        self.prefixes = {}

    def insert(self, word: str) -> None:
        p = self.prefixes
        for l in word:
            p = p.setdefault(l, {})
            p.setdefault("words", [])
            if len(p["words"]) < 3:
                p["words"].append(word)

    def start_search(self):
        self.cur_prefix = self.prefixes

    def suggest(self, char: str) -> bool:
        if char not in self.cur_prefix:
            self.cur_prefix = {}
            return []
        self.cur_prefix = self.cur_prefix[char]
        return self.cur_prefix["words"]

if __name__ == "__main__":
    tests = (
        (
            ["mobile","mouse","moneypot","monitor","mousepad"],
            "mouse",
            [["mobile","moneypot","monitor"],
             ["mobile","moneypot","monitor"],
             ["mouse","mousepad"],
             ["mouse","mousepad"],
             ["mouse","mousepad"]]
        ),
        (
            ["havana"],
            "havana",
            [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
        ),
        (
            ["bags","baggage","banner","box","cloths"],
            "bags",
            [["baggage","bags","banner"],
             ["baggage","bags","banner"],
             ["baggage","bags"],
             ["bags"]]
        ),
    )
    for products, searchWord, expected in tests:
        print(Solution().suggestedProducts(products, searchWord) == expected)
