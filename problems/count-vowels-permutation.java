/**
 * 1220. Count Vowels Permutation [Hard]
 * Given an integer n, your task is to count how many strings of length n can
 * be formed under the following rules:
 *     ∙ Each character is a lower case vowel ('a', 'e', 'i', 'o', 'u')
 *     ∙ Each vowel 'a' may only be followed by an 'e'.
 *     ∙ Each vowel 'e' may only be followed by an 'a' or an 'i'.
 *     ∙ Each vowel 'i' may not be followed by another 'i'.
 *     ∙ Each vowel 'o' may only be followed by an 'i' or a 'u'.
 *     ∙ Each vowel 'u' may only be followed by an 'a'.
 *
 * Since the answer may be too large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 1
 * Output: 5
 * Explanation: All possible strings are: "a", "e", "i" , "o" and "u".
 *
 * Example 2:
 * Input: n = 2
 * Output: 10
 * Explanation: All possible strings are: "ae", "ea", "ei", "ia", "ie", "io",
 * "iu", "oi", "ou" and "ua".
 *
 * Example 3:
 *
 * Input: n = 5
 * Output: 68
 *
 * Constraints:
 *     ∙ 1 <= n <= 2 * 10⁴
 */
class Solution {
    public int countVowelPermutation(int n) {
        final int MOD = 1000000007;
        long[] charCount = {1, 1, 1, 1, 1};
        final int[][] nextChars = {{1}, {0, 2}, {0, 1, 3, 4}, {2, 4}, {0}};
        for (int i = 0; i < n - 1; ++i) {
            long[] nextCharCount = {0, 0, 0, 0, 0};
            for (int j = 0; j < 5; ++j) {
                for (int nextChar : nextChars[j]) {
                    nextCharCount[nextChar] += charCount[j];
                    nextCharCount[nextChar] %= MOD;
                }
            }
            charCount = nextCharCount;
        }
        return (int)(LongStream.of(charCount).sum() % MOD);
    }
}
