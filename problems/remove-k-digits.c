/**
 * 402. Remove K Digits (Medium)
 * Given string num representing a non-negative integer num, and an integer k,
 * return the smallest possible integer after removing k digits from num.
 *
 * Example 1:
 * Input: num = "1432219", k = 3
 * Output: "1219"
 * Explanation: Remove the three digits 4, 3, and 2 to form the new number 1219
 * which is the smallest.
 *
 * Example 2:
 * Input: num = "10200", k = 1
 * Output: "200"
 * Explanation: Remove the leading 1 and the number is 200. Note that the
 * output must not contain leading zeroes.
 *
 * Example 3:
 * Input: num = "10", k = 2
 * Output: "0"
 * Explanation: Remove all the digits from the number and it is left with
 * nothing which is 0.
 *
 * Constraints:
 *     ∙ 1 <= k <= num.length <= 10⁵
 *     ∙ num consists of only digits.
 *     ∙ num does not have any leading zeros except for the zero itself.
 */
char * removeKdigits(char *num, int k) {
    static char stack[100000];
    int n = 0;
    for (int j = 0; num[j] != '\0'; ++j) {
        while (k && n && stack[n-1] > num[j]) {
            --k;
            --n;
        }
        stack[n++] = num[j];
    }
    n -= k;
    stack[n] = '\0';
    int i = 0;
    while (stack[i] == '0') { ++i; }
    return n > i ? stack + i : "0";
}
