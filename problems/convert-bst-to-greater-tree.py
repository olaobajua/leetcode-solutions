"""
 * 538. Convert BST to Greater Tree (Medium)
 * Given the root of a Binary Search Tree (BST), convert it to a Greater Tree
 * such that every key of the original BST is changed to the original key plus
 * the sum of all keys greater than the original key in BST.
 *
 * As a reminder, a binary search tree is a tree that satisfies these
 * constraints:
 *     ∙ The left subtree of a node contains only nodes with keys less than the
 *       node's key.
 *     ∙ The right subtree of a node contains only nodes with keys greater than
 *       the node's key.
 *     ∙ Both the left and right subtrees must also be binary search trees.
 *
 * Example 1:
 * Input: root = [4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
 * Output: [30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]
 *
 * Example 2:
 * Input: root = [0,null,1]
 * Output: [1,null,1]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 10⁴].
 *     ∙ -10⁴ <= Node.val <= 10⁴
 *     ∙ All the values in the tree are unique.
 *     ∙ root is guaranteed to be a valid binary search tree.
 *
 * Note: This question is the same as 1038:
 * https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def __init__(self):
        self.acc = 0

    def convertBST(self, root):
        if root:
            self.convertBST(root.right)
            self.acc += root.val
            root.val = self.acc
            self.convertBST(root.left)
        return root

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([4,1,6,0,2,5,7,None,None,None,3,None,None,None,8],
         [30,36,21,36,35,26,15,None,None,None,33,None,None,None,8]),
        ([0,None,1], [1,None,1]),
    )
    for nums, expected in tests:
        ret = list(tree_to_list(Solution().convertBST(build_tree(nums))))
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
