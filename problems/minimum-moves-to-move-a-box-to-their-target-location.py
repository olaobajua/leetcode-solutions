"""
 * 1263. Minimum Moves to Move a Box to Their Target Location (Hard)
 * Storekeeper is a game in which the player pushes boxes around in a warehouse
 * trying to get them to target locations.
 *
 * The game is represented by a grid of size m x n, where each element is a
 * wall, floor, or a box.
 *
 * Your task is move the box 'B' to the target position 'T' under the following
 * rules:
 *     ∙ Player is represented by character 'S' and can move up, down, left,
 *       right in the grid if it is a floor (empy cell).
 *     ∙ Floor is represented by character '.' that means free cell to walk.
 *     ∙ Wall is represented by character '#' that means obstacle (impossible
 *       to walk there).
 *     ∙ There is only one box 'B' and one target cell 'T' in the grid.
 *     ∙ The box can be moved to an adjacent free cell by standing next to the
 *       box and then moving in the direction of the box. This is a push.
 *     ∙ The player cannot walk through the box.
 *
 * Return the minimum number of pushes to move the box to the target. If there
 * is no way to reach the target, return -1.
 *
 * Example 1:
 * Input: grid = [["#","#","#","#","#","#"],
 *                ["#","T","#","#","#","#"],
 *                ["#",".",".","B",".","#"],
 *                ["#",".","#","#",".","#"],
 *                ["#",".",".",".","S","#"],
 *                ["#","#","#","#","#","#"]]
 * Output: 3
 * Explanation: We return only the number of times the box is pushed.
 *
 * Example 2:
 * Input: grid = [["#","#","#","#","#","#"],
 *                ["#","T","#","#","#","#"],
 *                ["#",".",".","B",".","#"],
 *                ["#","#","#","#",".","#"],
 *                ["#",".",".",".","S","#"],
 *                ["#","#","#","#","#","#"]]
 * Output: -1
 *
 * Example 3:
 * Input: grid = [["#","#","#","#","#","#"],
 *                ["#","T",".",".","#","#"],
 *                ["#",".","#","B",".","#"],
 *                ["#",".",".",".",".","#"],
 *                ["#",".",".",".","S","#"],
 *                ["#","#","#","#","#","#"]]
 * Output: 5
 * Explanation:  push the box down, left, left, up and up.
 *
 * Example 4:
 * Input: grid = [["#","#","#","#","#","#","#"],
 *                ["#","S","#",".","B","T","#"],
 *                ["#","#","#","#","#","#","#"]]
 * Output: -1
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m <= 20
 *     1 <= n <= 20
 *     grid contains only characters '.', '#',  'S' , 'T', or 'B'.
 *     There is only one character 'S', 'B' and 'T' in the grid.
"""
from typing import List

FLOOR = '.'
PATH = '+'
WALL = '#'

class Solution:
    def minPushBox(self, grid: List[List[str]]) -> int:
        hero = location(grid, 'S')
        box = location(grid, 'B')
        target = location(grid, 'T')
        grid[hero[0]][hero[1]] = '.'
        grid[target[0]][target[1]] = '.'
        grid[box[0]][box[1]] = '.'
        initial_state = *hero, *box  # state = hero_r, hero_c, box_r, box_c
        visited = {initial_state}
        pushes_count = 0
        states = (initial_state,)
        while states:
            next_states = ()
            for s in states:
                hrow, hcol, brow, bcol = s
                if (brow, bcol) == target:
                    return pushes_count
                reachable = [row[:] for row in grid]
                reachable[brow][bcol] = 'B'
                flood_fill_dfs(reachable, hrow, hcol, FLOOR, PATH)
                for dr, dc in ((0, 1), (0, -1), (1, 0), (-1, 0)):
                    if (is_inside(grid, brow + dr, bcol + dc) and
                            reachable[brow + dr][bcol + dc] == PATH and
                            can_push(grid, brow + dr, bcol + dc, brow, bcol)):
                        ns = (brow, bcol, brow - dr, bcol - dc)
                        if ns not in visited:
                            next_states += (ns,)
                            visited.add(ns)
            pushes_count += 1
            states = next_states
        return -1

def location(grid, symbol):
    width = len(grid[0])
    grid = [cell for row in grid for cell in row]
    s = grid.index(symbol)
    return (s // width, s % width)

def flood_fill_dfs(matrix, row, col, old, new):
    if matrix[row][col] == old:
        matrix[row][col] = new
        if row > 0:
            flood_fill_dfs(matrix, row - 1, col, old, new)
        if row < len(matrix) - 1:
            flood_fill_dfs(matrix, row + 1, col, old, new)
        if col > 0:
            flood_fill_dfs(matrix, row, col - 1, old, new)
        if col < len(matrix[row]) - 1:
            flood_fill_dfs(matrix, row, col + 1, old, new)

def can_push(grid, hrow, hcol, brow, bcol):
    r = brow + brow - hrow
    c = bcol + bcol - hcol
    return is_inside(grid, r, c) and grid[r][c] != WALL

def is_inside(grid, row, col):
    return 0 <= row < len(grid) and 0 <= col < len(grid[0])

if __name__ == "__main__":
    tests = (
        ([["#","#","#","#","#","#"],
          ["#","T","#","#","#","#"],
          ["#",".",".","B",".","#"],
          ["#",".","#","#",".","#"],
          ["#",".",".",".","S","#"],
          ["#","#","#","#","#","#"]],
         3),
        ([["#","#","#","#","#","#"],
          ["#","T","#","#","#","#"],
          ["#",".",".","B",".","#"],
          ["#","#","#","#",".","#"],
          ["#",".",".",".","S","#"],
          ["#","#","#","#","#","#"]],
         -1),
        ([["#","#","#","#","#","#"],
          ["#","T",".",".","#","#"],
          ["#",".","#","B",".","#"],
          ["#",".",".",".",".","#"],
          ["#",".",".",".","S","#"],
          ["#","#","#","#","#","#"]],
         5),
        ([["#","#","#","#","#","#","#"],
          ["#","S","#",".","B","T","#"],
          ["#","#","#","#","#","#","#"]],
         -1),
        ([["#",".",".","#","#","#","#","#"],
          ["#",".",".","T","#",".",".","#"],
          ["#",".",".",".","#","B",".","#"],
          ["#",".",".",".",".",".",".","#"],
          ["#",".",".",".","#",".","S","#"],
          ["#",".",".","#","#","#","#","#"]],
         7),
        ([["#",".",".",".",".",".",".",".",".","."],
          [".",".",".",".",".","#",".",".",".","#"],
          ["#",".","#",".",".","T",".",".",".","."],
          [".","#",".",".",".",".",".",".",".","."],
          [".",".",".",".",".",".","#",".",".","."],
          [".",".",".","#","#","S",".","B",".","."],
          ["#",".",".",".",".",".",".","#",".","."],
          [".","#",".",".",".",".",".",".",".","."],
          [".",".",".",".",".",".",".",".",".","."],
          [".",".",".",".",".","#",".",".",".","."]],
         5),
        ([[".",".","#",".",".",".",".",".",".","."],
          [".","#",".","#","B","#",".","#",".","."],
          [".","#",".",".",".",".",".",".","T","."],
          ["#",".",".",".",".",".",".",".",".","."],
          [".",".",".",".",".",".",".",".",".","#"],
          [".",".",".",".",".",".",".",".","#","."],
          [".",".",".","#",".",".","#","#",".","."],
          [".",".",".",".","#",".",".","#",".","."],
          [".","#",".","S",".",".",".",".",".","."],
          ["#",".",".","#",".",".",".",".",".","#"]],
         5),
    )
    for grid, expected in tests:
        print(Solution().minPushBox(grid) == expected)
