"""
 * 1444. Number of Ways of Cutting a Pizza [Hard]
 * Given a rectangular pizza represented as a rows x cols matrix containing
 * the following characters: 'A' (an apple) and '.' (empty cell) and given the
 * integer k. You have to cut the pizza into k pieces using k-1 cuts.
 *
 * For each cut you choose the direction: vertical or horizontal, then you
 * choose a cut position at the cell boundary and cut the pizza into two
 * pieces. If you cut the pizza vertically, give the left part of the pizza to
 * a person. If you cut the pizza horizontally, give the upper part of the
 * pizza to a person. Give the last piece of pizza to the last person.
 *
 * Return the number of ways of cutting the pizza such that each piece
 * contains at least one apple. Since the answer can be a huge number, return
 * this modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: pizza = ["A..",
 *                 "AAA",
 *                 "..."],
 *        k = 3
 * Output: 3
 *
 * Example 2:
 * Input: pizza = ["A..",
 *                 "AA.",
 *                 "..."],
 *        k = 3
 * Output: 1
 *
 * Example 3:
 * Input: pizza = ["A..",
 *                 "A..",
 *                 "..."],
 *        k = 1
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= rows, cols <= 50
 *     ∙ rows == pizza.length
 *     ∙ cols == pizza[i].length
 *     ∙ 1 <= k <= 10
 *     ∙ pizza consists of characters 'A' and '.' only.
"""
from functools import cache
from typing import List

class Solution:
    def ways(self, pizza: List[str], k: int) -> int:
        @cache
        def dp(row, col, c):
            if apples[row][col] == 0:
                return 0
            if c == 0:
                return 1
            ret = 0
            for nr in range(row + 1, m):
                if apples[row][col] - apples[nr][col] > 0:
                    ret = (ret + dp(nr, col, c - 1)) % MOD
            for nc in range(col + 1, n):
                if apples[row][col] - apples[row][nc] > 0:
                    ret = (ret + dp(row, nc, c - 1)) % MOD
            return ret

        m = len(pizza)
        n = len(pizza[0])
        MOD = 1000000007

        apples = [[0] * (n + 1) for _ in range(m + 1)]
        for i in range(m - 1, -1, -1):
            for j in range(n - 1, -1, -1):
                apples[i][j] += apples[i+1][j] + apples[i][j+1]
                apples[i][j] += (pizza[i][j] == "A") - apples[i+1][j+1]

        return dp(0, 0, k - 1)


if __name__ == "__main__":
    tests = (
        (["A..",
          "AAA",
          "..."], 3, 3),
        (["A..",
          "AA.",
          "..."], 3, 1),
        (["A..",
          "A..",
          "..."], 1, 1),
    )
    for pizza, k, expected in tests:
        print(Solution().ways(pizza, k) == expected)
