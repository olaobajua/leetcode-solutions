"""
 * 1302. Deepest Leaves Sum (Medium)
 * Given the root of a binary tree, return the sum of values of its deepest
 * leaves.
 *
 * Example 1:
 * Input: root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
 * Output: 15
 *
 * Example 2:
 * Input: root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]
 * Output: 19
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ 1 <= Node.val <= 100
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def deepestLeavesSum(self, root: Optional[TreeNode]) -> int:
        cur = [root]
        while cur:
            prev = cur
            cur = [child for parent in cur
                   for child in [parent.left, parent.right] if child]
        return sum(node.val for node in prev)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5,None,6,7,None,None,None,None,8], 15),
        ([6,7,8,2,7,1,3,9,None,1,4,None,None,None,5], 19),
    )
    for nums, expected in tests:
        root = build_tree(nums)
        print(Solution().deepestLeavesSum(root) == expected)
