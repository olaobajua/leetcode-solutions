/**
 * 106. Construct Binary Tree from Inorder and Postorder Traversal (Medium)
 * Given two integer arrays inorder and postorder where inorder is the inorder traversal of a binary tree and postorder is the postorder traversal of the same tree, construct and return the binary tree.
 *
 * Example 1:
 * Input: inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
 * Output: [3,9,20,null,null,15,7]
 *
 * Example 2:
 * Input: inorder = [-1], postorder = [-1]
 * Output: [-1]
 *
 * Constraints:
 *     1 <= inorder.length <= 3000
 *     postorder.length == inorder.length
 *     -3000 <= inorder[i], postorder[i] <= 3000
 *     inorder and postorder consist of unique values.
 *     Each value of postorder also appears in inorder.
 *     inorder is guaranteed to be the inorder traversal of the tree.
 *     postorder is guaranteed to be the postorder traversal of the tree.
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {number[]} inorder
 * @param {number[]} postorder
 * @return {TreeNode}
 */
var buildTree = function(inorder, postorder) {
    function buildTree(ps, pe, is, ie) {
        if (pe - ps > 0) {
            const i = inorderPos[postorder[pe-1]];
            return new TreeNode(inorder[i],
                                buildTree(ps, ps + i - is, is, i),
                                buildTree(pe - ie + i, pe - 1, i + 1, ie));
        }
    }
    const inorderPos = Object.fromEntries(inorder.map((n, i) => [n, i]));
    return buildTree(0, postorder.length, 0, inorder.length);
};

function* treeToList(root) {
    let nodes = [root];
    for (let n of nodes) {
        yield n ? n.val : null;
        if (n) {
            nodes.push(n.left, n.right);
        }
    }
}

const tests = [
    [[9,3,15,20,7],
     [9,15,7,20,3],
     [3,9,20,null,null,15,7]],
    [[9,3,13,15,14,20,102,7,101],
     [9,13,14,15,102,101,7,20,3],
     [3,9,20,null,null,15,7,13,14,102,101]],
    [[-1], [-1], [-1]],
];
for (const [inorder, postorder, expected] of tests) {
    r = [...treeToList(buildTree(inorder, postorder))]
    while (r.length && r[r.length-1] === null) {
        r.pop();
    }
    console.log(expected.every((n, i) => n == r[i]));
}
