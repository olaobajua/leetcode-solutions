/**
 * 234. Palindrome Linked List [Easy]
 * Given the head of a singly linked list, return true if it is a palindrome.
 *
 * Example 1:
 * Input: head = [1,2,2,1]
 * Output: true
 *
 * Example 2:
 * Input: head = [1,2]
 * Output: false
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [1, 10⁵].
 *     ∙ 0 <= Node.val <= 9
 *
 * Follow up: Could you do it in O(n) time and O(1) space?
 */

// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {boolean}
 */
var isPalindrome = function(head) {
    let end = head;
    let n = 0;
    while (end.next) {
        ++n;
        [end.next.prev, end] = [end, end.next];
    }
    let i = 0;
    while (i < n) {
        if (head.val != end.val)
            return false;
        [head, end] = [head.next, end.prev];
        ++i;
        --n;
    }
    return true;
};

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

const tests = [
    [[1,2,2,1], true],
    [[1,2], false],
];

for (const [nums, expected] of tests) {
    console.log(isPalindrome(arrayToLinkedList(nums)) == expected)
}
