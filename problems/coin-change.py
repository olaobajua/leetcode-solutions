"""
 * 322. Coin Change (Medium)
 * You are given an integer array coins representing coins of different
 * denominations and an integer amount representing a total amount of money.
 *
 * Return the fewest number of coins that you need to make up that amount.
 * If that amount of money cannot be made up by any combination of the coins,
 * return -1.
 *
 * You may assume that you have an infinite number of each kind of coin.
 *
 * Example 1:
 * Input: coins = [1,2,5], amount = 11
 * Output: 3
 * Explanation: 11 = 5 + 5 + 1
 *
 * Example 2:
 * Input: coins = [2], amount = 3
 * Output: -1
 *
 * Example 3:
 * Input: coins = [1], amount = 0
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= coins.length <= 12
 *     ∙ 1 <= coins[i] <= 2³¹ - 1
 *     ∙ 0 <= amount <= 10⁴
"""
from functools import cache
from math import inf
from typing import List

class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:
        @cache
        def dfs(amount):
            if amount == 0:
                return 0
            if amount < 0:
                return inf
            return min(dfs(amount - coin) for coin in coins) + 1
        return count if (count := dfs(amount)) < inf else -1

if __name__ == "__main__":
    tests = (
        ([1,2,5], 11, 3),
        ([2], 3, -1),
        ([1], 0, 0),
        ([1,2,5], 100, 20),
    )
    for coins, amount, expected in tests:
        print(Solution().coinChange(coins, amount) == expected)
