"""
 * 581. Shortest Unsorted Continuous Subarray (Medium)
 * Given an integer array nums, you need to find one continuous subarray that
 * if you only sort this subarray in ascending order, then the whole array will
 * be sorted in ascending order.
 *
 * Return the shortest such subarray and output its length.
 *
 * Example 1:
 * Input: nums = [2,6,4,8,10,9,15]
 * Output: 5
 * Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make
 * the whole array sorted in ascending order.
 *
 * Example 2:
 * Input: nums = [1,2,3,4]
 * Output: 0
 *
 * Example 3:
 * Input: nums = [1]
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁴
 *     ∙ -10⁵ <= nums[i] <= 10⁵
 *
 * Follow up: Can you solve it in O(n) time complexity?
"""
from itertools import accumulate
from math import inf
from typing import List

class Solution:
    def findUnsortedSubarray(self, nums: List[int]) -> int:
        nums = [-inf] + nums + [inf]
        max_from_left = list(accumulate(nums, max))
        min_from_right = [*accumulate(reversed(nums), min)][::-1]
        lo = 0
        hi= len(nums) - 1
        while nums[hi-1] <= nums[hi] and max_from_left[hi-1] <= nums[hi - 1]:
            hi -= 1
        if hi == 0:
            return 0
        while nums[lo+1] >= nums[lo] and min_from_right[lo+1] >= nums[lo + 1]:
            lo += 1
        return hi - lo - 1

if __name__ == "__main__":
    tests = (
        ([2,6,4,8,10,9,15], 5),
        ([1,2,3,4], 0),
        ([1], 0),
        ([1,3,2,2,2], 4),
        ([2,3,3,2,4], 3),
        ([1,2,4,5,3], 3),
    )
    for nums, expected in tests:
        print(Solution().findUnsortedSubarray(nums) == expected)
