"""
 * 594. Longest Harmonious Subsequence [Easy]
 * We define a harmonious array as an array where the difference between its
 * maximum value and its minimum value is exactly 1.
 *
 * Given an integer array nums, return the length of its longest harmonious
 * subsequence among all its possible subsequences.
 *
 * A subsequence of array is a sequence that can be derived from the array by
 * deleting some or no elements without changing the order of the remaining
 * elements.
 *
 * Example 1:
 * Input: nums = [1,3,2,2,5,2,3,7]
 * Output: 5
 * Explanation: The longest harmonious subsequence is [3,2,2,2,3].
 *
 * Example 2:
 * Input: nums = [1,2,3,4]
 * Output: 2
 *
 * Example 3:
 * Input: nums = [1,1,1,1]
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 2 * 10⁴
 *     ∙ -10⁹ <= nums[i] <= 10⁹
"""
from collections import Counter
from typing import List

class Solution:
    def findLHS(self, nums: List[int]) -> int:
        prev = -1000000002
        prev_count = 0
        ret = 0
        for x, count in sorted(Counter(nums).items()):
            if x - prev == 1:
                ret = max(ret, count + prev_count)
            prev, prev_count = x, count

        return ret

if __name__ == "__main__":
    tests = (
        ([1,3,2,2,5,2,3,7], 5),
        ([1,2,3,4], 2),
        ([1,1,1,1], 0),
    )
    for nums, expected in tests:
        print(Solution().findLHS(nums) == expected)
