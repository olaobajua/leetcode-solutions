"""
 * 1187. Make Array Strictly Increasing [Hard]
 * Given two integer arrays arr1 and arr2, return the minimum number of
 * operations (possibly zero) needed to make arr1 strictly increasing.
 *
 * In one operation, you can choose two indices 0 <= i < arr1.length and 0 <=
 * j < arr2.length and do the assignment arr1[i] = arr2[j].
 *
 * If there is no way to make arr1 strictly increasing, return -1.
 *
 * Example 1:
 * Input: arr1 = [1,5,3,6,7], arr2 = [1,3,2,4]
 * Output: 1
 * Explanation: Replace 5 with 2, then arr1 = [1, 2, 3, 6, 7].
 *
 * Example 2:
 * Input: arr1 = [1,5,3,6,7], arr2 = [4,3,1]
 * Output: 2
 * Explanation: Replace 5 with 3 and then replace 3 with 4. arr1 = [1, 3, 4,
 * 6, 7].
 *
 * Example 3:
 * Input: arr1 = [1,5,3,6,7], arr2 = [1,6,3,3]
 * Output: -1
 * Explanation: You can't make arr1 strictly increasing.
 *
 * Constraints:
 *     ∙ 1 <= arr1.length, arr2.length <= 2000
 *     ∙ 0 <= arr1[i], arr2[i] <= 10⁹
"""
from bisect import bisect_right
from functools import cache
from math import inf
from typing import List

class Solution:
    def makeArrayIncreasing(self, arr1: List[int], arr2: List[int]) -> int:
        @cache
        def dp(i, prev):
            if i >= len(arr1):
                return 0

            ret = inf
            if prev < arr1[i]:
                ret = dp(i + 1, arr1[i])
            j = bisect_right(arr2, prev)
            if j < len(arr2):
                ret = min(ret, dp(i + 1, arr2[j]) + 1)
            return ret

        arr2.sort()
        if (ret := dp(0, -1)) < inf:
            return ret
        return -1

if __name__ == "__main__":
    tests = (
        ([1,5,3,6,7], [1,3,2,4], 1),
        ([1,5,3,6,7], [4,3,1], 2),
        ([1,5,3,6,7], [1,6,3,3], -1),
    )
    for arr1, arr2, expected in tests:
        print(Solution().makeArrayIncreasing(arr1, arr2) == expected)
