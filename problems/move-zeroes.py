"""
 * 283. Move Zeroes [Easy]
 * Given an integer array nums, move all 0's to the end of it while
 * maintaining the relative order of the non-zero elements.
 *
 * Note that you must do this in-place without making a copy of the array.
 *
 * Example 1:
 * Input: nums = [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 *
 * Example 2:
 * Input: nums = [0]
 * Output: [0]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁴
 *     ∙ -2³¹ <= nums[i] <= 2³¹ - 1
 *
 * Follow up: Could you minimize the total number of operations done?
"""
from typing import List

class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        left = 0
        for i in range(len(nums)):
            if nums[i] != 0:
                nums[left] = nums[i]
                left += 1
        for i in range(left, len(nums)):
            nums[i] = 0

if __name__ == "__main__":
    tests = (
        ([0,1,0,3,12], [1,3,12,0,0]),
        ([0], [0]),
    )
    for nums, expected in tests:
        Solution().moveZeroes(nums)
        print(nums == expected)
