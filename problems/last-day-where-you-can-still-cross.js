/**
 * 1970. Last Day Where You Can Still Cross (Hard)
 * There is a 1-based binary matrix where 0 represents land and 1 represents
 * water. You are given integers row and col representing the number of rows
 * and columns in the matrix, respectively.
 *
 * Initially on day 0, the entire matrix is land. However, each day a new cell
 * becomes flooded with water. You are given a 1-based 2D array cells, where
 * cells[i] = [ri, ci] represents that on the ith day, the cell on the rith row
 * and cith column (1-based coordinates) will be covered with water
 * (i.e., changed to 1).
 *
 * You want to find the last day that it is possible to walk from the top to
 * the bottom by only walking on land cells. You can start from any cell in the
 * top row and end at any cell in the bottom row. You can only travel in the
 * four cardinal directions (left, right, up, and down).
 *
 * Return the last day where it is possible to walk from the top to the bottom
 * by only walking on land cells.
 *
 * Example 1:
 * Input: row = 2, col = 2, cells = [[1,1],[2,1],[1,2],[2,2]]
 * Output: 2
 * Explanation: The above image depicts how the matrix changes each day
 * starting from day 0.
 * The last day where it is possible to cross from top to bottom is on day 2.
 *
 * Example 2:
 * Input: row = 2, col = 2, cells = [[1,1],[1,2],[2,1],[2,2]]
 * Output: 1
 * Explanation: The above image depicts how the matrix changes each day
 * starting from day 0.
 * The last day where it is possible to cross from top to bottom is on day 1.
 *
 * Example 3:
 * Input: row = 3, col = 3, cells = [[1,2],[2,1],[3,3],[2,2],[1,1],[1,3],[2,3],
                                     [3,2],[3,1]]
 * Output: 3
 * Explanation: The above image depicts how the matrix changes each day
 * starting from day 0.
 * The last day where it is possible to cross from top to bottom is on day 3.
 *
 * Constraints:
 *     2 <= row, col <= 2 * 10⁴
 *     4 <= row * col <= 2 * 10⁴
 *     cells.length == row * col
 *     1 <= ri <= row
 *     1 <= ci <= col
 *     All the values of cells are unique.
 */

/**
 * @param {number} row
 * @param {number} col
 * @param {number[][]} cells
 * @return {number}
 */
var latestDayToCross = function(row, col, cells) {
    function canCross(day) {
        const matrix = [...Array(row)].map(r => Array(col).fill(0));
        cells.slice(0, day).forEach(([r, c]) => {
            matrix[r-1][c-1] = 1;
        });
        const land = 0;
        const path = 2;
        for (let c = 0; c < col; ++c) {
            if (matrix[0][c] == land) {
                floodFillDfs(matrix, 0, c, land, path);
                if (matrix[row - 1].some(c => c == path)) {
                    return true;
                }
            }
        }
        return false;
    }
    let lo = 0;
    let hi = cells.length - 1;
    while (hi - lo > 1) {
        const mid = Math.floor((hi + lo) / 2);
        if (canCross(mid)) {
            lo = mid;
        } else {
            hi = mid;
        }
    }
    return lo;
};

function floodFillDfs(matrix, row, col, o, n) {
    if (matrix[row][col] == o) {
        matrix[row][col] = n;
        if (row > 0) {
            floodFillDfs(matrix, row - 1, col, o, n);
        }
        if (row < matrix.length - 1) {
            floodFillDfs(matrix, row + 1, col, o, n);
        }
        if (col > 0) {
            floodFillDfs(matrix, row, col - 1, o, n);
        }
        if (col < matrix[row].length - 1) {
            floodFillDfs(matrix, row, col + 1, o, n);
        }
    }
}


const tests = [
    [2, 2, [[1,1],[2,1],[1,2],[2,2]], 2],
    [2, 2, [[1,1],[1,2],[2,1],[2,2]], 1],
    [3, 3, [[1,2],[2,1],[3,3],[2,2],[1,1],[1,3],[2,3],[3,2],[3,1]], 3],
];
for (const [row, col, cells, expected] of tests) {
    console.log(latestDayToCross(row, col, cells) == expected);
}
