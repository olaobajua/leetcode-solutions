/**
 * 199. Binary Tree Right Side View (Medium)
 * Given the root of a binary tree, imagine yourself standing on the right side
 * of it, return the values of the nodes you can see ordered from top to
 * bottom.
 *
 * Example 1:
 * Input: root = [1,2,3,null,5,null,4]
 * Output: [1,3,4]
 *
 * Example 2:
 * Input: root = [1,null,3]
 * Output: [1,3]
 *
 * Example 3:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 100].
 *     ∙ -100 <= Node.val <= 100
 */
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

int* rightSideView(struct TreeNode *root, int *returnSize) {
    *returnSize = 0;
    static int ret[101];
    static struct TreeNode level[101];
    static struct TreeNode next_level[101];
    int level_size = 0;
    if (root)
        level[level_size++] = *root;
    while (level_size) {
        ret[(*returnSize)++] = level[0].val;
        int next_level_size = 0;
        for (int i = 0; i < level_size; ++i) {
            if (level[i].right)
                next_level[next_level_size++] = *level[i].right;
            if (level[i].left)
                next_level[next_level_size++] = *level[i].left;
        }
        memcpy(level, next_level, next_level_size * sizeof(struct TreeNode));
        level_size = next_level_size;
    }
    return ret;
}
