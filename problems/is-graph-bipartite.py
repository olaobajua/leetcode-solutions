"""
 * 785. Is Graph Bipartite? (Medium)
 * There is an undirected graph with n nodes, where each node is numbered
 * between 0 and n - 1. You are given a 2D array graph, where graph[u] is
 * an array of nodes that node u is adjacent to. More formally, for each v in
 * graph[u], there is an undirected edge between node u and node v. The graph
 * has the following properties:
 *     ∙ There are no self-edges (graph[u] does not contain u).
 *     ∙ There are no parallel edges (graph[u] does not contain duplicate
 *       values).
 *     ∙ If v is in graph[u], then u is in graph[v] (the graph is undirected).
 *     ∙ The graph may not be connected, meaning there may be two nodes u and v
 *       such that there is no path between them.
 *
 * A graph is bipartite if the nodes can be partitioned into two independent
 * sets A and B such that every edge in the graph connects a node in set A and
 * a node in set B.
 *
 * Return true if and only if it is bipartite.
 *
 * Example 1:
 * Input: graph = [[1,2,3],[0,2],[0,1,3],[0,2]]
 * Output: false
 * Explanation: There is no way to partition the nodes into two independent
 * sets such that every edge connects a node in one and a node in the other.
 *
 * Example 2:
 * Input: graph = [[1,3],[0,2],[1,3],[0,2]]
 * Output: true
 * Explanation: We can partition the nodes into two sets: {0, 2} and {1, 3}.
 *
 * Constraints:
 *     ∙ graph.length == n
 *     ∙ 1 <= n <= 100
 *     ∙ 0 <= graph[u].length < n
 *     ∙ 0 <= graph[u][i] <= n - 1
 *     ∙ graph[u] does not contain u.
 *     ∙ All the values of graph[u] are unique.
 *     ∙ If graph[u] contains v, then graph[v] contains u.
"""
from typing import List

class Solution:
    def isBipartite(self, graph: List[List[int]]) -> bool:
        color = {}
        def dfs(pos):
            for i in graph[pos]:
                if i in color:
                    if color[i] == color[pos]:
                        return False
                else:
                    color[i] = 1 - color[pos]
                    if not dfs(i):
                        return False
            return True
        for i in range(len(graph)):
            if i not in color:
                color[i] = 0
                if not dfs(i):
                    return False
        return True

if __name__ == "__main__":
    tests = (
        ([[1,2,3],[0,2],[0,1,3],[0,2]], False),
        ([[1,3],[0,2],[1,3],[0,2]], True),
        ([[1],[0,3],[3],[1,2]], True),
        ([[4,1],[2,0],[3,1],[4,2],[0,3]], False),
        ([[4],[],[4],[4],[0,2,3]], True),
        ([[1],[0],[4],[4],[2,3]], True),
        ([[3,4,6],[3,6],[3,6],[0,1,2,5],[0,7,8],[3],[0,1,2,7],[4,6],[4],[]], True),
        ([[],[2,4,6],[1,4,8,9],[7,8],[1,2,8,9],[6,9],[1,5,7,8,9],[3,6,9],[2,3,4,6,9],[2,4,5,6,7,8]], False),
        ([[],[10,44,62],[98],[59],[90],[],[31,59],[52,58],[],[53],[1,63],[51,71],[18,64],[24,26,45,95],[61,67,96],[],[40],[39,74,79],[12,21,72],[35,85],[86,88],[18,76],[71,80],[27,58,85],[13,26,87],[75,94],[13,24,68,77,82],[23],[56,96],[67],[56,73],[6],[41],[50,88,91,94],[],[19,72,92],[59],[49],[49,89],[17],[16],[32,84,86],[61,73,77],[94,98],[1,74],[13,57,90],[],[93],[],[37,38,54,68],[33],[11],[7,85],[9],[49],[61],[28,30,87,93],[45,69,77],[7,23,76],[3,6,36,62],[81],[14,42,55,62],[1,59,61],[10],[12,93],[],[96],[14,29,70,73],[26,49,71,76],[57,83],[67],[11,22,68,89],[18,35],[30,42,67],[17,44],[25],[21,58,68],[26,42,57,95],[],[17],[22,83],[60],[26,83,84,94],[69,80,82],[41,82],[19,23,52],[20,41],[24,56],[20,33],[38,71,99],[4,45],[33],[35],[47,56,64],[25,33,43,82],[13,77],[14,28,66],[],[2,43],[89]], False),
        ([[3],[3],[],[0,1],[6],[],[4],[9],[],[7]], True),
    )
    for graph, expected in tests:
        print(Solution().isBipartite(graph) == expected)
