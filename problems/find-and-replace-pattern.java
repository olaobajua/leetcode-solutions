/**
 * 890. Find and Replace Pattern [Medium]
 * Given a list of strings words and a string pattern, return a list of
 * words[i] that match pattern. You may return the answer in any order.
 *
 * A word matches the pattern if there exists a permutation of letters p so
 * that after replacing every letter x in the pattern with p(x), we get the
 * desired word.
 *
 * Recall that a permutation of letters is a bijection from letters to letters:
 * every letter maps to another letter, and no two letters map to the same
 * letter.
 *
 * Example 1:
 * Input: words = ["abc","deq","mee","aqq","dkd","ccc"], pattern = "abb"
 * Output: ["mee","aqq"]
 * Explanation: "mee" matches the pattern because there is a permutation {a ->
 * m, b -> e, ...}.
 * "ccc" does not match the pattern because {a -> c, b -> c, ...} is not a
 * permutation, since a and b map to the same letter.
 *
 * Example 2:
 * Input: words = ["a","b","c"], pattern = "a"
 * Output: ["a","b","c"]
 *
 * Constraints:
 *     ∙ 1 <= pattern.length <= 20
 *     ∙ 1 <= words.length <= 50
 *     ∙ words[i].length == pattern.length
 *     ∙ pattern and words[i] are lowercase English letters.
 */
class Solution {
    public List<String> findAndReplacePattern(String[] words, String pattern) {
        List<String> ret = new ArrayList<String>();
        for (String word : words) {
            nextWord: {
                int[] wordToPattern = new int[123];
                int[] patternToWord = new int[123];
                for (int i = 0; i < pattern.length(); ++i) {
                    int wc = (int)word.charAt(i);
                    int pc = (int)pattern.charAt(i);
                    if (wordToPattern[wc] == 0)
                        wordToPattern[wc] = pc;
                    if (patternToWord[pc] == 0)
                        patternToWord[pc] = wc;
                    if (wordToPattern[wc] != pc || patternToWord[pc] != wc)
                        break nextWord;
                }
                ret.add(word);
            }
        }
        return ret;
    }
}
