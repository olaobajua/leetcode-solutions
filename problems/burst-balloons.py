"""
 * 312. Burst Balloons (Hard)
 * You are given n balloons, indexed from 0 to n - 1. Each balloon is painted
 * with a number on it represented by an array nums. You are asked to burst all
 * the balloons.
 *
 * If you burst the ith balloon, you will get
 * nums[i - 1] * nums[i] * nums[i + 1] coins. If i - 1 or i + 1 goes out of
 * bounds of the array, then treat it as if there is a balloon with a 1 painted
 * on it.
 *
 * Return the maximum coins you can collect by bursting the balloons wisely.
 *
 * Example 1:
 * Input: nums = [3,1,5,8]
 * Output: 167
 * Explanation:
 * nums = [3,1,5,8] --> [3,5,8] --> [3,8] --> [8] --> []
 * coins =  3*1*5    +   3*5*8   +  1*3*8  + 1*8*1 = 167
 *
 * Example 2:
 * Input: nums = [1,5]
 * Output: 10
 *
 * Constraints:
 *     n == nums.length
 *     1 <= n <= 500
 *     0 <= nums[i] <= 100
"""
from functools import cache
from typing import List

class Solution:
    def maxCoins(self, nums: List[int]) -> int:
        @cache
        def max_coins(balloons):
            if len(balloons) > 2:
                return max(max_coins(balloons[:last_burst+1]) +
                           max_coins(balloons[last_burst:]) +
                           balloons[0] * balloons[last_burst] * balloons[-1]
                           for last_burst in range(1, len(balloons) - 1))
            else:
                return 0

        return max_coins(tuple([1] + [x for x in nums if x != 0] + [1]))

if __name__ == "__main__":
    tests = (
        ([3,1,5,8], 167),
        ([1,5], 10),
        ([9,76,64,21,97,60], 1086136),
        ([8,2,6,8,9,8,1,4,1,5,3,0,7,7,0,4,2,2,5,5], 3830),
        ([8,3,4,3,5,0,5,6,6,2,8,5,6,2,3,8,3,5,1,0,2], 3394),
        ([4,1,4,9,4,1,8,1,8,6,9,1,2,0,9,6,4,1,7,9,5,4,4,0], 5872),
        ([8,3,4,3,5,0,5,6,6,2,8,5,6,2,3,8,3,5,1,0,2,9,6], 4548),
    )
    from time import time
    start = time()
    for nums, expected in tests:
        print(Solution().maxCoins(nums) == expected)
    print(f"Time elapsed: {time() - start} sec")
