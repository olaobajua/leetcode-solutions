"""
 * 838. Push Dominoes [Medium]
 * There are n dominoes in a line, and we place each domino vertically
 * upright. In the beginning, we simultaneously push some of the dominoes
 * either to the left or to the right.
 *
 * After each second, each domino that is falling to the left pushes the
 * adjacent domino on the left. Similarly, the dominoes falling to the right
 * push their adjacent dominoes standing on the right.
 *
 * When a vertical domino has dominoes falling on it from both sides, it stays
 * still due to the balance of the forces.
 *
 * For the purposes of this question, we will consider that a falling domino
 * expends no additional force to a falling or already fallen domino.
 *
 * You are given a string dominoes representing the initial state where:
 *     ∙ dominoes[i] = 'L', if the iᵗʰ domino has been pushed to the left,
 *     ∙ dominoes[i] = 'R', if the iᵗʰ domino has been pushed to the right,
 *     ∙ dominoes[i] = '.', if the iᵗʰ domino has not been pushed.
 *
 * Return a string representing the final state.
 *
 * Example 1:
 * Input: dominoes = "RR.L"
 * Output: "RR.L"
 * Explanation: The first domino expends no additional force on the second
 * domino.
 *
 * Example 2:
 * Input: dominoes = ".L.R...LR..L.."
 * Output: "LL.RR.LLRRLL.."
 *
 * Constraints:
 *     ∙ n == dominoes.length
 *     ∙ 1 <= n <= 10⁵
 *     ∙ dominoes[i] is either 'L', 'R', or '.'.
"""
from collections import defaultdict

class Solution:
    def pushDominoes(self, dominoes: str) -> str:
        n = len(dominoes)
        dominoes = list(dominoes)
        bfs = [i for i, d in enumerate(dominoes) if d != '.']
        while bfs:
            next_state = defaultdict(list)
            next_bfs = []
            for i in bfs:
                if dominoes[i] == 'L':
                    if i > 0 and dominoes[i-1] == '.':
                        next_state[i-1].append('L')
                        next_bfs.append(i-1)
                if dominoes[i] == 'R':
                    if i < n - 1 and dominoes[i+1] == '.':
                        next_state[i+1].append('R')
                        next_bfs.append(i+1)
            for j in next_state:
                if len(next_state[j]) == 1:
                    dominoes[j] = next_state[j][0]
            bfs = next_bfs
        return ''.join(dominoes)

if __name__ == "__main__":
    tests = (
        ("RR.L", "RR.L"),
        (".L.R...LR..L..", "LL.RR.LLRRLL.."),
    )
    for dominoes, expected in tests:
        print(Solution().pushDominoes(dominoes) == expected)
