"""
 * 208. Implement Trie (Prefix Tree) [Medium]
 * A trie (https://en.wikipedia.org/wiki/Trie) (pronounced as "try") or prefix
 * tree is a tree data structure used to efficiently store and retrieve keys in
 * a dataset of strings. There are various applications of this data structure,
 * such as autocomplete and spellchecker.
 *
 * Implement the Trie class:
 *     ∙ Trie() Initializes the trie object.
 *     ∙ void insert(String word) Inserts the string word into the trie.
 *     ∙ boolean search(String word) Returns true if the string word is in the
 *       trie (i.e., was inserted before), and false otherwise.
 *     ∙ boolean startsWith(String prefix) Returns true if there is a
 *       previously inserted string word that has the prefix prefix, and false
 *       otherwise.
 *
 * Example 1:
 * Input
 * ["Trie", "insert", "search", "search", "startsWith", "insert", "search"]
 * [[], ["apple"], ["apple"], ["app"], ["app"], ["app"], ["app"]]
 * Output
 * [null, null, true, false, true, null, true]
 *
 * Explanation
 * Trie trie = new Trie();
 * trie.insert("apple");
 * trie.search("apple");   // return True
 * trie.search("app");     // return False
 * trie.startsWith("app"); // return True
 * trie.insert("app");
 * trie.search("app");     // return True
 *
 * Constraints:
 *     ∙ 1 <= word.length, prefix.length <= 2000
 *     ∙ word and prefix consist only of lowercase English letters.
 *     ∙ At most 3 * 10⁴ calls in total will be made to insert, search, and
 * startsWith.
"""
class Trie:
    def __init__(self):
        self.prefixes = {}

    def insert(self, word: str) -> None:
        p = self.prefixes
        for l in word:
            p = p.setdefault(l, {})
        p["\0"] = word

    def search(self, word: str) -> bool:
        p = self.prefixes
        for l in word:
            if l not in p:
                return False
            p = p[l]
        return "\0" in p

    def startsWith(self, prefix: str) -> bool:
        p = self.prefixes
        for l in prefix:
            if l not in p:
                return False
            p = p[l]
        return True

obj = Trie()
obj.insert("apple")
print(True == obj.search("apple"))
print(False == obj.search("app"))
print(True == obj.startsWith("app"))
obj.insert("app")
print(True == obj.search("app"))
