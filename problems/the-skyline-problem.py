"""
 * 218. The Skyline Problem [Hard]
 * A city's skyline is the outer contour of the silhouette formed by all the
 * buildings in that city when viewed from a distance. Given the locations and
 * heights of all the buildings, return the skyline formed by these buildings
 * collectively.
 *
 * The geometric information of each building is given in the array buildings
 * where buildings[i] = [leftᵢ, rightᵢ, heightᵢ]:
 *     ∙ leftᵢ is the x coordinate of the left edge of the iᵗʰ building.
 *     ∙ rightᵢ is the x coordinate of the right edge of the iᵗʰ building.
 *     ∙ heightᵢ is the height of the iᵗʰ building.
 *
 * You may assume all buildings are perfect rectangles grounded on an
 * absolutely flat surface at height 0.
 *
 * The skyline should be represented as a list of "key points" sorted by their
 * x-coordinate in the form [[x₁,y₁],[x₂,y₂],...]. Each key point is the left
 * endpoint of some horizontal segment in the skyline except the last point in
 * the list, which always has a y-coordinate 0 and is used to mark the
 * skyline's termination where the rightmost building ends. Any ground between
 * the leftmost and rightmost buildings should be part of the skyline's
 * contour.
 *
 * Note: There must be no consecutive horizontal lines of equal height in the
 * output skyline. For instance, [...,[2 3],[4 5],[7 5],[11 5],[12 7],...] is
 * not acceptable; the three lines of height 5 should be merged into one in the
 * final output as such: [...,[2 3],[4 5],[12 7],...]
 *
 * Example 1:
 * Input: buildings = [[2,9,10],[3,7,15],[5,12,12],[15,20,10],[19,24,8]]
 * Output: [[2,10],[3,15],[7,12],[12,0],[15,10],[20,8],[24,0]]
 * Explanation:
 * Figure A shows the buildings of the input.
 * Figure B shows the skyline formed by those buildings. The red points in
 * figure B represent the key points in the output list.
 *
 * Example 2:
 * Input: buildings = [[0,2,3],[2,5,3]]
 * Output: [[0,3],[5,0]]
 *
 * Constraints:
 *     ∙ 1 <= buildings.length <= 10⁴
 *     ∙ 0 <= leftᵢ < rightᵢ <= 2³¹ - 1
 *     ∙ 1 <= heightᵢ <= 2³¹ - 1
 *     ∙ buildings is sorted by leftᵢ in non-decreasing order.
"""
from heapq import heappop, heappush
from typing import List

class Solution:
    def getSkyline(self, buildings: List[List[int]]) -> List[List[int]]:
        ret = [[-1, 0]]
        heap = []
        i = 0
        n = len(buildings)
        for x in sorted({x for l, r, _ in buildings for x in (l, r)}):
            while i < n and buildings[i][0] <= x:
                heappush(heap, (-buildings[i][2], buildings[i][1]))
                i += 1

            while heap and heap[0][1] <= x:
                heappop(heap)

            h = -heap[0][0] if heap else 0
            if ret[-1][1] != h:
                ret.append([x, h])

        return ret[1:]

if __name__ == "__main__":
    tests = (
        ([[2,9,10],[3,7,15],[5,12,12],[15,20,10],[19,24,8]],
         [[2,10],[3,15],[7,12],[12,0],[15,10],[20,8],[24,0]]),
        ([[0,2,3],[2,5,3]], [[0,3],[5,0]]),
        ([[0,2147483647,2147483647]], [[0,2147483647],[2147483647,0]]),
    )
    for buildings, expected in tests:
        print(Solution().getSkyline(buildings) == expected)
