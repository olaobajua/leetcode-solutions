"""
 * 1662. Check If Two String Arrays are Equivalent [Easy]
 * Given two string arrays word1 and word2, return true if the two arrays
 * represent the same string, and false otherwise.
 *
 * A string is represented by an array if the array elements concatenated in
 * order forms the string.
 *
 * Example 1:
 * Input: word1 = ["ab", "c"], word2 = ["a", "bc"]
 * Output: true
 * Explanation:
 * word1 represents string "ab" + "c" -> "abc"
 * word2 represents string "a" + "bc" -> "abc"
 * The strings are the same, so return true.
 *
 * Example 2:
 * Input: word1 = ["a", "cb"], word2 = ["ab", "c"]
 * Output: false
 *
 * Example 3:
 * Input: word1  = ["abc", "d", "defg"], word2 = ["abcddefg"]
 * Output: true
 *
 * Constraints:
 *     ∙ 1 <= word1.length, word2.length <= 10³
 *     ∙ 1 <= word1[i].length, word2[i].length <= 10³
 *     ∙ 1 <= sum(word1[i].length), sum(word2[i].length) <= 10³
 *     ∙ word1[i] and word2[i] consist of lowercase letters.
"""
from typing import List

class Solution:
    def arrayStringsAreEqual(self, word1: List[str], word2: List[str]) -> bool:
        return "".join(word1) == "".join(word2)

if __name__ == "__main__":
    tests = (
        (["ab", "c"], ["a", "bc"], True),
        (["a", "cb"], ["ab", "c"], False),
        (["abc", "d", "defg"], ["abcddefg"], True),
    )
    for word1, word2, expected in tests:
        print(Solution().arrayStringsAreEqual(word1, word2) == expected)
