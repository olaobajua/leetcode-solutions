"""
 * 203. Remove Linked List Elements (Easy)
 * Given the head of a linked list and an integer val, remove all the nodes of
 * the linked list that has Node.val == val, and return the new head.
 *
 * Example 1:
 * Input: head = [1,2,6,3,4,5,6], val = 6
 * Output: [1,2,3,4,5]
 *
 * Example 2:
 * Input: head = [], val = 1
 * Output: []
 *
 * Example 3:
 * Input: head = [7,7,7,7], val = 7
 * Output: []
 *
 * Constraints:
 *     The number of nodes in the list is in the range [0, 10⁴].
 *     1 <= Node.val <= 50
 *     0 <= val <= 50
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def removeElements(self, head: Optional[ListNode], val: int) -> Optional[ListNode]:
        if head:
            head.next = self.removeElements(head.next, val)
            return head.next if head.val == val else head

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,6,3,4,5,6], 6, [1,2,3,4,5]),
        ([], 1, []),
        ([7,7,7,7], 7, []),
        ([1,2,2,1], 2, [1,1]),

    )
    for nums, val, expected in tests:
        print(linked_list_to_list(Solution().removeElements(
            list_to_linked_list(nums), val)) == expected)
