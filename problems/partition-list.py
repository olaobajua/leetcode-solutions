"""
 * 86. Partition List [Medium]
 * Given the head of a linked list and a value x, partition it such that all
 * nodes less than x come before nodes greater than or equal to x.
 *
 * You should preserve the original relative order of the nodes in each of the
 * two partitions.
 *
 * Example 1:
 * Input: head = [1,4,3,2,5,2], x = 3
 * Output: [1,2,2,4,3,5]
 *
 * Example 2:
 * Input: head = [2,1], x = 2
 * Output: [1,2]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 200].
 *     ∙ -100 <= Node.val <= 100
 *     ∙ -200 <= x <= 200
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def partition(self, head: Optional[ListNode], x: int) -> Optional[ListNode]:
        lt_head = lt = ListNode()
        ge_head = ge = ListNode()
        while head:
            if head.val < x:
                lt.next, lt = head, head
            else:
                ge.next, ge = head, head
            head = head.next
        lt.next = ge_head.next
        ge.next = None
        return lt_head.next

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head: Optional[ListNode]) -> List[int]:
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,4,3,2,5,2], 3, [1,2,2,4,3,5]),
        ([2,1], 2, [1,2]),
    )
    for nums, x, expected in tests:
        root = list_to_linked_list(nums)
        print(linked_list_to_list(Solution().partition(root, x)) == expected)
