"""
 * 2617. Minimum Number of Visited Cells in a Grid [Hard]
 * You are given a 0-indexed m x n integer matrix grid. Your initial position
 * is at the top-left cell (0, 0).
 *
 * Starting from the cell (i, j), you can move to one of the following cells:
 *     ∙ Cells (i, k) with j < k <= grid[i][j] + j (rightward movement), or
 *     ∙ Cells (k, j) with i < k <= grid[i][j] + i (downward movement).
 *
 * Return the minimum number of cells you need to visit to reach the
 * bottom-right cell (m - 1, n - 1). If there is no valid path, return -1.
 *
 * Example 1:
 * Input: grid = [[3,4,2,1],[4,2,3,1],[2,1,0,0],[2,4,0,0]]
 * Output: 4
 * Explanation: The image above shows one of the paths that visits exactly 4
 * cells.
 *
 * Example 2:
 * Input: grid = [[3,4,2,1],[4,2,1,1],[2,1,1,0],[3,4,1,0]]
 * Output: 3
 * Explanation: The image above shows one of the paths that visits exactly 3
 * cells.
 *
 * Example 3:
 * Input: grid = [[2,1,0],[1,0,0]]
 * Output: -1
 * Explanation: It can be proven that no path exists.
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 10⁵
 *     ∙ 1 <= m * n <= 10⁵
 *     ∙ 0 <= grid[i][j] < m * n
 *     ∙ grid[m - 1][n - 1] == 0
"""
from sortedcontainers import SortedList
from typing import List

class Solution:
    def minimumVisitedCells(self, grid: List[List[int]]) -> int:
        m = len(grid)
        n = len(grid[0])
        cols = [SortedList(range(n)) for _ in range(m)]
        rows = [SortedList(range(m)) for _ in range(n)]
        to_visit = [(0, 0, 1)]
        for row, col, steps in to_visit:
            if row == m - 1 and col == n - 1:
                return steps
            cur = grid[row][col]
            for k in list(cols[row].irange(col + 1, min(col + cur + 1, n) - 1)):
                to_visit.append((row, k, steps + 1))
                cols[row].remove(k)
                rows[k].remove(row)
            for k in list(rows[col].irange(row + 1, min(row + cur + 1, m) - 1)):
                to_visit.append((k, col, steps + 1))
                rows[col].remove(k)
                cols[k].remove(col)
        return -1

if __name__ == "__main__":
    tests = (
        ([[3,4,2,1],
          [4,2,3,1],
          [2,1,0,0],
          [2,4,0,0]], 4),
        ([[3,4,2,1],
          [4,2,1,1],
          [2,1,1,0],
          [3,4,1,0]], 3),
        ([[2,1,0],
          [1,0,0]], -1),
        ([[0,1,0]], -1),
        ([[17,13,17,2],
          [14,3,2,1],
          [6,1,1,5],
          [16,16,9,9],
          [8,1,2,0]], 3),
    )
    for grid, expected in tests:
        print(Solution().minimumVisitedCells(grid) == expected)
