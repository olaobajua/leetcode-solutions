"""
 * 2054. Two Best Non-Overlapping Events (Medium)
 * You are given a 0-indexed 2D integer array of events where
 * events[i] = [startTimeᵢ, endTimeᵢ, valueᵢ]. The ith event starts at
 * startTimeᵢ and ends at endTimeᵢ, and if you attend this event, you will
 * receive a value of valueᵢ. You can choose at most two non-overlapping events
 * to attend such that the sum of their values is maximized.
 *
 * Return this maximum sum.
 *
 * Note that the start time and end time is inclusive: that is, you cannot
 * attend two events where one of them starts and the other ends at the same
 * time. More specifically, if you attend an event with end time t, the next
 * event must start at or after t + 1.
 *
 * Example 1:
 * Input: events = [[1,3,2],[4,5,2],[2,4,3]]
 * Output: 4
 * Explanation: Choose the green events, 0 and 1 for a sum of 2 + 2 = 4.
 *
 * Example 2:
 * Input: events = [[1,3,2],[4,5,2],[1,5,5]]
 * Output: 5
 * Explanation: Choose event 2 for a sum of 5.
 *
 * Example 3:
 * Input: events = [[1,5,3],[1,5,1],[6,6,5]]
 * Output: 8
 * Explanation: Choose events 0 and 2 for a sum of 3 + 5 = 8.
 *
 * Constraints:
 *     2 <= events.length <= 10⁵
 *     events[i].length == 3
 *     1 <= startTimeᵢ <= endTimeᵢ <= 10⁹
 *     1 <= valueᵢ <= 10⁶
"""
from bisect import bisect_left
from operator import itemgetter
from typing import List

class Solution:
    def maxTwoEvents(self, events: List[List[int]]) -> int:
        sorted_by_end = sorted(events, key=itemgetter(1))
        max_before = [[0, 0]]  # [end, max value]
        max_of_two = 0
        for start, end, value in sorted_by_end:
            if value > max_before[-1][1]:
                max_before.append([end, value])
            max_end = bisect_left(max_before, [start]) - 1
            max_of_two = max(value + max_before[max_end][1], max_of_two)

        return max_of_two

if __name__ == "__main__":
    tests = (
        ([[1,3,2],[4,5,2],[2,4,3]], 4),
        ([[1,3,2],[4,5,2],[1,5,5]], 5),
        ([[1,5,3],[1,5,1],[6,6,5]], 8),
        ([[66,97,90],[98,98,68],[38,49,63],[91,100,42],[92,100,22],[1,77,50],[64,72,97]], 165),
    )
    for events, expected in tests:
        print(Solution().maxTwoEvents(events) == expected)
