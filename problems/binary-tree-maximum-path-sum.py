"""
 * 124. Binary Tree Maximum Path Sum [Hard]
 * A path in a binary tree is a sequence of nodes where each pair of adjacent
 * nodes in the sequence has an edge connecting them. A node can only appear in
 * the sequence at most once. Note that the path does not need to pass through
 * the root.
 *
 * The path sum of a path is the sum of the node's values in the path.
 *
 * Given the root of a binary tree, return the maximum path sum of any
 * non-empty path.
 *
 * Example 1:
 *  1
 * 2 3
 * Input: root = [1,2,3]
 * Output: 6
 * Explanation: The optimal path is 2 -> 1 -> 3 with a path sum of 2 + 1 + 3 =
 * 6.
 *
 * Example 2:
 *  -10
 * 9   20
 *    15 7
 * Input: root = [-10,9,20,null,null,15,7]
 * Output: 42
 * Explanation: The optimal path is 15 -> 20 -> 7 with a path sum of 15 + 20 +
 * 7 = 42.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 3 * 10⁴].
 *     ∙ -1000 <= Node.val <= 1000
"""
from functools import cache
from math import inf
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def maxPathSum(self, root: Optional[TreeNode]) -> int:
        def dfs(node):
            if node is None:
                return -inf
            return max(node.val,
                       node.val + max_half(node.left),
                       node.val + max_half(node.right),
                       node.val + max_half(node.left) + max_half(node.right),
                       dfs(node.left),
                       dfs(node.right))

        @cache
        def max_half(node):
            if node is None:
                return 0
            return max(node.val + max(max_half(node.left), max_half(node.right)),
                       node.val)

        return dfs(root)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3], 6),
        ([-10,9,20,None,None,15,7], 42),
        ([-3], -3),
        ([2,-1], 2),
        ([9,6,-3,None,None,-6,2,None,None,2,None,-6,-6,-6], 16),
    )
    for nums, expected in tests:
        print(Solution().maxPathSum(build_tree(nums)) == expected)
