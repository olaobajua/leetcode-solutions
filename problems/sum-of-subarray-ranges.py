"""
 * 2104. Sum of Subarray Ranges (Medium)
 * You are given an integer array nums. The range of a subarray of nums is the
 * difference between the largest and smallest element in the subarray.
 *
 * Return the sum of all subarray ranges of nums.
 *
 * A subarray is a contiguous non-empty sequence of elements within an array.
 *
 * Example 1:
 * Input: nums = [1,2,3]
 * Output: 4
 * Explanation: The 6 subarrays of nums are the following:
 * [1], range = largest - smallest = 1 - 1 = 0
 * [2], range = 2 - 2 = 0
 * [3], range = 3 - 3 = 0
 * [1,2], range = 2 - 1 = 1
 * [2,3], range = 3 - 2 = 1
 * [1,2,3], range = 3 - 1 = 2
 * So the sum of all ranges is 0 + 0 + 0 + 1 + 1 + 2 = 4.
 *
 * Example 2:
 * Input: nums = [1,3,3]
 * Output: 4
 * Explanation: The 6 subarrays of nums are the following:
 * [1], range = largest - smallest = 1 - 1 = 0
 * [3], range = 3 - 3 = 0
 * [3], range = 3 - 3 = 0
 * [1,3], range = 3 - 1 = 2
 * [3,3], range = 3 - 3 = 0
 * [1,3,3], range = 3 - 1 = 2
 * So the sum of all ranges is 0 + 0 + 0 + 2 + 0 + 2 = 4.
 *
 * Example 3:
 * Input: nums = [4,-2,-3,4,1]
 * Output: 59
 * Explanation: The sum of all subarray ranges of nums is 59.
 *
 * Constraints:
 *     1 <= nums.length <= 1000
 *     -10⁹ <= nums[i] <= 10⁹
"""
from math import inf
from typing import List

class Solution:
    def subArrayRanges(self, nums: List[int]) -> int:
        n = len(nums)
        s = 0
        for i in range(n):
            mn = inf
            mx = -inf
            for j in range(i, n):
                mn = min(mn, nums[j])
                mx = max(mx, nums[j])
                s += mx - mn
        return s

if __name__ == "__main__":
    tests = (
        ([1,2,3], 4),
        ([1,3,3], 4),
        ([4,-2,-3,4,1], 59),
    )
    for nums, expected in tests:
        print(Solution().subArrayRanges(nums) == expected)
