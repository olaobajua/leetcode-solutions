"""
 * 967. Numbers With Same Consecutive Differences [Medium]
 * Return all non-negative integers of length n such that the absolute
 * difference between every two consecutive digits is k.
 *
 * Note that every number in the answer must not have leading zeros. For
 * example, 01 has one leading zero and is invalid.
 *
 * You may return the answer in any order.
 *
 * Example 1:
 * Input: n = 3, k = 7
 * Output: [181,292,707,818,929]
 * Explanation: Note that 070 is not a valid number, because it has leading
 * zeroes.
 *
 * Example 2:
 * Input: n = 2, k = 1
 * Output: [10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]
 *
 * Constraints:
 *     ∙ 2 <= n <= 9
 *     ∙ 0 <= k <= 9
"""
from typing import List

class Solution:
    def numsSameConsecDiff(self, n: int, k: int) -> List[int]:
        def dfs(i):
            if i >= m:
                yield i
            else:
                last = i % 10
                if last + k < 10:
                    yield from dfs(i * 10 + last + k)
                if last - k >= 0:
                    yield from dfs(i * 10 + last - k)
        m = 10**(n - 1)
        return sorted({x for first in range(1, 10) for x in dfs(first)})

if __name__ == "__main__":
    tests = (
        (3, 7, [181,292,707,818,929]),
        (2, 1, [10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]),
        (2, 0, [11,22,33,44,55,66,77,88,99]),
    )
    for n, k, expected in tests:
        print(set(Solution().numsSameConsecDiff(n, k)) == set(expected))
