"""
 * 99. Recover Binary Search Tree (Medium)
 * You are given the root of a binary search tree (BST), where the values of
 * exactly two nodes of the tree were swapped by mistake. Recover the tree
 * without changing its structure.
 *
 * Example 1:
 * Input: root = [1,3,null,null,2]
 * Output: [3,1,null,null,2]
 * Explanation: 3 cannot be a left child of 1 because 3 > 1. Swapping 1 and 3
 * makes the BST valid.
 *
 * Example 2:
 * Input: root = [3,1,4,null,null,2]
 * Output: [2,1,4,null,null,3]
 * Explanation: 2 cannot be in the right subtree of 3 because 2 < 3. Swapping 2
 * and 3 makes the BST valid.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [2, 1000].
 *     ∙ -2³¹ <= Node.val <= 2³¹ - 1
 *
 * Follow up: A solution using O(n) space is pretty straight-forward. Could you
 * devise a constant O(1) space solution?
"""
from math import inf
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def recoverTree(self, root: Optional[TreeNode]) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        def traverse_inorder(root):
            if root is None:
                return
            for node in traverse_inorder(root.left):
                yield node
            yield root
            for node in traverse_inorder(root.right):
                yield node
        prev = TreeNode(-inf)
        stack = []
        for node in traverse_inorder(root):
            if node.val < prev.val:
                stack.extend([prev, node])
            prev = node
        stack[0].val, stack[-1].val = stack[-1].val, stack[0].val

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([1,3,None,None,2], [3,1,None,None,2]),
        ([3,1,4,None,None,2], [2,1,4,None,None,3]),
    )
    for nums, expected in tests:
        root = build_tree(nums)
        Solution().recoverTree(root)
        ret = [*tree_to_list(root)]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
