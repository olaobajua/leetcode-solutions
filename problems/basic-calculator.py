"""
 * 224. Basic Calculator [Hard]
 * Given a string s representing a valid expression, implement a basic
 * calculator to evaluate it, and return the result of the evaluation.
 *
 * Note: You are not allowed to use any built-in function which evaluates
 * strings as mathematical expressions, such as eval().
 *
 * Example 1:
 * Input: s = "1 + 1"
 * Output: 2
 *
 * Example 2:
 * Input: s = " 2-1 + 2 "
 * Output: 3
 *
 * Example 3:
 * Input: s = "(1+(4+5+2)-3)+(6+8)"
 * Output: 23
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 3 * 10⁵
 *     ∙ s consists of digits, '+', '-', '(', ')', and ' '.
 *     ∙ s represents a valid expression.
 *     ∙ '+' is not used as a unary operation (i.e., "+1" and "+(2 + 3)" is
 *       invalid).
 *     ∙ '-' could be used as a unary operation (i.e., "-1" and "-(2 + 3)" is
 *       valid).
 *     ∙ There will be no two consecutive operators in the input.
 *     ∙ Every number and running calculation will fit in a signed 32-bit
 *       integer.
"""
class Solution:
    def calculate(self, s: str) -> int:
        s = s.replace(" ", "").replace("(-" ,"(0-")
        stack = []
        for element in math_infix_to_rpn(s):
            if isinstance(element, int):
                stack.append(element)
            else:
                if element == "+":
                    stack.append(stack.pop() + stack.pop())
                elif element == "-":
                    stack.append(-stack.pop() + stack.pop())
        return stack.pop()

def math_infix_to_rpn(expr):
    """
    Transforms expr from infix notation to reverse polish notation.
    This is classical shunting-yard algorithm by Edsger Dijkstra.
    """
    PRECEDENCE = {
        '*': 2,
        '/': 2,
        '+': 1,
        '-': 1,
        '(': 0,
        ')': 0,
    }
    ret = [0, ' ']
    stack = []
    for c in expr:
        if '0' <= c <= '9':
            if isinstance(ret[-1], int):
                ret.append(10*ret.pop() + int(c))
            else:
                ret.append(int(c))
        else:
            if ret[-1] != ' ':
                ret.append(' ')
            if c in '*/+-':
                while stack and PRECEDENCE[c] <= PRECEDENCE[stack[-1]]:
                    ret.append(stack.pop())
                stack.append(c)
            elif c == '(':
                stack.append('(')
            elif c == ')':
                while (tmp := stack.pop()) != '(':
                    ret.append(tmp)
    while stack:
        ret.append(stack.pop())
    return tuple(element for element in ret if element != ' ')

if __name__ == "__main__":
    tests = (
        ("1 + 1", 2),
        (" 2-1 + 2 ", 3),
        ("(1+(4+5+2)-3)+(6+8)", 23),
        ("0", 0),
        ("-2+ 1", -1),
        ("1 + (-1)", 0),
        ("1-(-2)", 3),
        ("1-(1-2)", 2),
        ("1-(     -2)", 3),
    )
    for s, expected in tests:
        print(Solution().calculate(s) == expected)
