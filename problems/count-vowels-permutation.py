"""
 * 1220. Count Vowels Permutation [Hard]
 * Given an integer n, your task is to count how many strings of length n can
 * be formed under the following rules:
 *     ∙ Each character is a lower case vowel ('a', 'e', 'i', 'o', 'u')
 *     ∙ Each vowel 'a' may only be followed by an 'e'.
 *     ∙ Each vowel 'e' may only be followed by an 'a' or an 'i'.
 *     ∙ Each vowel 'i' may not be followed by another 'i'.
 *     ∙ Each vowel 'o' may only be followed by an 'i' or a 'u'.
 *     ∙ Each vowel 'u' may only be followed by an 'a'.
 *
 * Since the answer may be too large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 1
 * Output: 5
 * Explanation: All possible strings are: "a", "e", "i" , "o" and "u".
 *
 * Example 2:
 * Input: n = 2
 * Output: 10
 * Explanation: All possible strings are: "ae", "ea", "ei", "ia", "ie", "io",
 * "iu", "oi", "ou" and "ua".
 *
 * Example 3:
 *
 * Input: n = 5
 * Output: 68
 *
 * Constraints:
 *     ∙ 1 <= n <= 2 * 10⁴
"""
import numpy as np

class Solution:
    def countVowelPermutation(self, n: int) -> int:
        def power(next_chars, n):
            ret = np.eye(len(next_chars), dtype = int)
            while n:
                if n & 1:
                    ret = next_chars * ret % MOD
                next_chars = next_chars * next_chars % MOD
                n >>= 1
            return ret

        MOD = 1000000007
        next_chars = np.matrix([[0,1,0,0,0],
                                [1,0,1,0,0],
                                [1,1,0,1,1],
                                [0,0,1,0,1],
                                [1,0,0,0,0]])
        return power(next_chars, n - 1).sum() % MOD

if __name__ == "__main__":
    tests = (
        (1, 5),
        (2, 10),
        (3, 19),
        (4, 35),
        (5, 68),
        (6, 129),
        (7, 249),
        (8, 474),
        (9, 911),
        (10, 1739),
        (10000, 76428576),
        (20000, 759959057),
    )
    for n, expected in tests:
        print(Solution().countVowelPermutation(n) == expected)
