"""
 * 1751. Maximum Number of Events That Can Be Attended II [Hard]
 * You are given an array of events where events[i] = [startDayᵢ, endDayᵢ,
 * valueᵢ]. The iᵗʰ event starts at startDayᵢand ends at endDayᵢ, and if you
 * attend this event, you will receive a value of valueᵢ. You are also given an
 * integer k which represents the maximum number of events you can attend.
 *
 * You can only attend one event at a time. If you choose to attend an event,
 * you must attend the entire event. Note that the end day is inclusive: that
 * is, you cannot attend two events where one of them starts and the other ends
 * on the same day.
 *
 * Return the maximum sum of values that you can receive by attending events.
 *
 * Example 1:
 * Input: events = [[1,2,4],[3,4,3],[2,3,1]], k = 2
 * Output: 7
 * Explanation: Choose the green events, 0 and 1 (0-indexed) for a total value
 * of 4 + 3 = 7.
 *
 * Example 2:
 * Input: events = [[1,2,4],[3,4,3],[2,3,10]], k = 2
 * Output: 10
 * Explanation: Choose event 2 for a total value of 10.
 * Notice that you cannot attend any other event as they overlap, and that you
 * do not have to attend k events.
 *
 * Example 3:
 * Input: events = [[1,1,1],[2,2,2],[3,3,3],[4,4,4]], k = 3
 * Output: 9
 * Explanation: Although the events do not overlap, you can only attend 3
 * events. Pick the highest valued three.
 *
 * Constraints:
 *     ∙ 1 <= k <= events.length
 *     ∙ 1 <= k * events.length <= 10⁶
 *     ∙ 1 <= startDayᵢ <= endDayᵢ <= 10⁹
 *     ∙ 1 <= valueᵢ <= 10⁶
"""
from functools import cache
from typing import List

class Solution:
    def maxValue(self, events: List[List[int]], k: int) -> int:
        @cache
        def dp(i, can_attend):
            if i >= n or can_attend == 0:
                return 0

            _, cur_end, cur_value = events[i]
            j = i + 1
            while j < n:
                next_start = events[j][0]
                if next_start > cur_end:
                    break
                j += 1

            return max(dp(i + 1, can_attend), cur_value + dp(j, can_attend - 1))

        n = len(events)
        events.sort()

        return dp(0, k)

if __name__ == "__main__":
    tests = (
        ([[1,2,4],[3,4,3],[2,3,1]], 2, 7),
        ([[1,2,4],[3,4,3],[2,3,10]], 2, 10),
        ([[1,1,1],[2,2,2],[3,3,3],[4,4,4]], 3, 9),
    )
    for events, k, expected in tests:
        print(Solution().maxValue(events, k) == expected)
