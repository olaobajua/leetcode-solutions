/**
 * 260. Single Number III (Medium)
 * Given an integer array nums, in which exactly two elements appear only once
 * and all the other elements appear exactly twice. Find the two elements that
 * appear only once. You can return the answer in any order.
 *
 * You must write an algorithm that runs in linear runtime complexity and uses
 * only constant extra space.
 *
 * Example 1:
 * Input: nums = [1,2,1,3,2,5]
 * Output: [3,5]
 * Explanation:  [5, 3] is also a valid answer.
 *
 * Example 2:
 * Input: nums = [-1,0]
 * Output: [-1,0]
 *
 * Example 3:
 * Input: nums = [0,1]
 * Output: [1,0]
 *
 * Constraints:
 *     ∙ 2 <= nums.length <= 3 * 10⁴
 *     ∙ -2³¹ <= nums[i] <= 2³¹ - 1
 *     ∙ Each integer in nums will appear twice, only two integers will appear
 *       once.
 */
#include <stdio.h>
#include <stdlib.h>

int* singleNumber(int* nums, int numsSize, int* returnSize);

int main() {
    int nums[] = {1,2,1,3,2,5}, expected[] = {3,5};
    // int nums[] = {-1,0}, expected[] = {-1,0};
    // int nums[] = {0,1}, expected[] = {1,0};
    // int nums[] = {1,1,0,-2147483648}, expected[] = {0, -2147483648};
    const size_t LENGTH = sizeof(nums) / sizeof(nums[0]);

    int rs = 0;
    int *r = singleNumber(nums, LENGTH, &rs);
    if ((r[0] == expected[0] || r[0] == expected[1]) &&
            (r[1] == expected[0] || r[1] == expected[1])) {
        printf("true\n");
    } else {
        printf("true\n");
    }
    free(r);
    return EXIT_SUCCESS;
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* singleNumber(int* nums, int numsSize, int* returnSize) {
    int *ret = (int*)calloc(2, sizeof(int));
    *returnSize = 2;
    int xorNums = 0;
    for (int i = 0; i < numsSize; ++i) {
        xorNums ^= nums[i];
    }
    const int lo = (xorNums & ((long)xorNums - 1)) ^ xorNums;
    for (int i = 0; i < numsSize; ++i) {
        if (nums[i] & lo) {
            ret[0] ^= nums[i];
        };
    }
    ret[1] = xorNums ^ ret[0];
    return ret;
}
