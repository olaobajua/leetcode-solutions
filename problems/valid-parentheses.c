/**
 * 20. Valid Parentheses (Easy)
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and
 * ']', determine if the input string is valid.
 *
 * An input string is valid if:
 *     ∙ Open brackets must be closed by the same type of brackets.
 *     ∙ Open brackets must be closed in the correct order.
 *
 * Example 1:
 * Input: s = "()"
 * Output: true
 *
 * Example 2:
 * Input: s = "()[]{}"
 * Output: true
 *
 * Example 3:
 * Input: s = "(]"
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁴
 *     ∙ s consists of parentheses only '()[]{}'.
 */
bool isValid(char *s) {
    static char stack[10000];
    int n = 0;
    for (int i = 0; s[i] != '\0'; ++i) {
        switch (s[i]) {
            case '(': stack[n++] = ')'; break;
            case '[': stack[n++] = ']'; break;
            case '{': stack[n++] = '}'; break;
            default:
                if (!n || s[i] != stack[n-1]) {
                    return false;
                } else {
                    --n;
                }
        }
    }
    return !n;
}
