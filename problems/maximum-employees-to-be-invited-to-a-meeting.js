/**
 * 2127. Maximum Employees to Be Invited to a Meeting (Hard)
 * A company is organizing a meeting and has a list of n employees, waiting to
 * be invited. They have arranged for a large circular table, capable of
 * seating any number of employees.
 *
 * The employees are numbered from 0 to n - 1. Each employee has a favorite
 * person and they will attend the meeting only if they can sit next to their
 * favorite person at the table. The favorite person of an employee is not
 * themself.
 *
 * Given a 0-indexed integer array favorite, where favorite[i] denotes the
 * favorite person of the ith employee, return the maximum number of employees
 * that can be invited to the meeting.
 *
 * Example 1:
 * Input: favorite = [2,2,1,2]
 * Output: 3
 * Explanation:
 *      2 1
 *       0
 * The above figure shows how the company can invite employees 0, 1, and 2, and
 * seat them at the round table.
 * All employees cannot be invited because employee 2 cannot sit beside
 * employees 0, 1, and 3, simultaneously.
 * Note that the company can also invite employees 1, 2, and 3, and give them
 * their desired seats.
 * The maximum number of employees that can be invited to the meeting is 3.
 *
 * Example 2:
 * Input: favorite = [1,2,0]
 * Output: 3
 * Explanation:
 * Each employee is the favorite person of at least one other employee, and the
 * only way the company can invite them is if they invite every employee.
 * The seating arrangement will be the same as that in the figure given in
 * example 1:
 * - Employee 0 will sit between employees 2 and 1.
 * - Employee 1 will sit between employees 0 and 2.
 * - Employee 2 will sit between employees 1 and 0.
 * The maximum number of employees that can be invited to the meeting is 3.
 *
 * Example 3:
 * Input: favorite = [3,0,1,4,1]
 * Output: 4
 * Explanation:
 *        4
 *      3   1
 *        0
 * The above figure shows how the company will invite employees 0, 1, 3, and 4,
 * and seat them at the round table.
 * Employee 2 cannot be invited because the two spots next to their favorite
 * employee 0 are taken.
 * So the company leaves them out of the meeting.
 * The maximum number of employees that can be invited to the meeting is 4.
 *
 * Constraints:
 *     ∙ n == favorite.length
 *     ∙ 2 <= n <= 10⁵
 *     ∙ 0 <= favorite[i] <= n - 1
 *     ∙ favorite[i] != i
 */

/**
 * @param {number[]} favorite
 * @return {number}
 */
var maximumInvitations = function(favorite) {
    function height(r) {
        seen.add(r);
        return Math.max(0, ...(favoriteFor.get(r)||[]).map(c => height(c)+1));
    }

    const favoriteFor = new Map();
    const couples = [];
    for (const [i, f] of favorite.entries()) {
        if (favorite[f] == i) {
            couples.push(i);
        } else {
            favoriteFor.get(f) ? favoriteFor.get(f).push(i)
                               : favoriteFor.set(f, [i]);
        }
    }
    const seen = new Set();
    const couplesWithFriends = couples.length +
                               couples.reduce((a, p) => a + height(p), 0);
    let maxCircle = 0;
    for (let person = 0; person < favorite.length; ++person) {
        if (!seen.has(person)) {
            let nextPerson = favorite[person];
            const cur = new Set();
            while (!cur.has(nextPerson)) {
                cur.add(nextPerson);
                seen.add(nextPerson);
                nextPerson = favorite[nextPerson];
            }
            personInCircle = favorite[nextPerson];
            circumference = 1;
            while (personInCircle != nextPerson) {
                personInCircle = favorite[personInCircle];
                ++circumference;
            }
            maxCircle = Math.max(maxCircle, circumference);
        }
    }
    return Math.max(couplesWithFriends, maxCircle);
};

const tests = [
    [[2,2,1,2], 3],
    [[1,2,0], 3],
    [[3,0,1,4,1], 4],
    [[1,2,3,4,5,6,3,8,9,10,11,8], 4],
    [[1,0,0,2,1,4,7,8,9,6,7,10,8], 6],
    [[3,0,10,4,1,9,12,8,5,8,13,1,9,12], 4],
    [[21,12,1,7,5,6,20,2,14,15,12,14,2,20,0,17,23,12,17,13,11,13,3,11], 6],
];

for (const [favorite, expected] of tests) {
    console.log(maximumInvitations(favorite) == expected);
}
