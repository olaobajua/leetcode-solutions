"""
 * 2060. Check if an Original String Exists Given Two Encoded Strings (Hard)
 * An original string, consisting of lowercase English letters, can be encoded
 * by the following steps:
 *     ∙ Arbitrarily split it into a sequence of some number of non-empty
 *       substrings.
 *     ∙ Arbitrarily choose some elements (possibly none) of the sequence, and
 *       replace each with its length (as a numeric string).
 *     ∙ Concatenate the sequence as the encoded string.
 *
 * For example, one way to encode an original string "abcdefghijklmnop" might
 * be:
 *     ∙ Split it as a sequence: ["ab", "cdefghijklmn", "o", "p"].
 *     ∙ Choose the second and third elements to be replaced by their lengths,
 *       respectively. The sequence becomes ["ab", "12", "1", "p"].
 *     ∙ Concatenate the elements of the sequence to get the encoded string:
 *       "ab121p".
 *
 * Given two encoded strings s1 and s2, consisting of lowercase English letters
 * and digits 1-9 (inclusive), return true if there exists an original string
 * that could be encoded as both s1 and s2. Otherwise, return false.
 *
 * Note: The test cases are generated such that the number of consecutive
 * digits in s1 and s2 does not exceed 3.
 *
 * Example 1:
 * Input: s1 = "internationalization", s2 = "i18n"
 * Output: true
 * Explanation: It is possible that "internationalization" was the original
 * string.
 * - "internationalization"
 *   -> Split:       ["internationalization"]
 *   -> Do not replace any element
 *   -> Concatenate:  "internationalization", which is s1.
 * - "internationalization"
 *   -> Split:       ["i", "nternationalizatio", "n"]
 *   -> Replace:     ["i", "18",                 "n"]
 *   -> Concatenate:  "i18n", which is s2
 *
 * Example 2:
 * Input: s1 = "l123e", s2 = "44"
 * Output: true
 * Explanation: It is possible that "leetcode" was the original string.
 * - "leetcode"
 *   -> Split:      ["l", "e", "et", "cod", "e"]
 *   -> Replace:    ["l", "1", "2",  "3",   "e"]
 *   -> Concatenate: "l123e", which is s1.
 * - "leetcode"
 *   -> Split:      ["leet", "code"]
 *   -> Replace:    ["4",    "4"]
 *   -> Concatenate: "44", which is s2.
 *
 * Example 3:
 * Input: s1 = "a5b", s2 = "c5b"
 * Output: false
 * Explanation: It is impossible.
 * - The original string encoded as s1 must start with the letter 'a'.
 * - The original string encoded as s2 must start with the letter 'c'.
 *
 * Example 4:
 * Input: s1 = "112s", s2 = "g841"
 * Output: true
 * Explanation: It is possible that "gaaaaaaaaaaaas" was the original string
 * - "gaaaaaaaaaaaas"
 *   -> Split:      ["g", "aaaaaaaaaaaa", "s"]
 *   -> Replace:    ["1", "12",           "s"]
 *   -> Concatenate: "112s", which is s1.
 * - "gaaaaaaaaaaaas"
 *   -> Split:      ["g", "aaaaaaaa", "aaaa", "s"]
 *   -> Replace:    ["g", "8",        "4",    "1"]
 *   -> Concatenate: "g841", which is s2.
 *
 * Example 5:
 * Input: s1 = "ab", s2 = "a2"
 * Output: false
 * Explanation: It is impossible.
 * - The original string encoded as s1 has two letters.
 * - The original string encoded as s2 has three letters.
 *
 * Constraints:
 *     ∙ 1 <= s1.length, s2.length <= 40
 *     ∙ s1 and s2 consist of digits 1-9 (inclusive), and lowercase English
 *       letters only.
 *     ∙ The number of consecutive digits in s1 and s2 does not exceed 3.
"""
from functools import cache
from typing import List

class Solution:
    def possiblyEquals(self, s1: str, s2: str) -> bool:
        return cmp_seq(parse(s1), parse(s2))

def parse(s):
    ret = []
    letters = []
    digits = []
    for c in s:
        if c.isalpha():
            if digits:
                ret.append(parse_num(''.join(digits)))
                digits = []
            letters.append(c)
        else:
            if letters:
                ret.append((''.join(letters),))
                letters = []
            digits.append(c)
    if digits:
        ret.append(parse_num(''.join(digits)))
    if letters:
        ret.append((''.join(letters),))
    return tuple(ret)

def parse_num(s):
    variants = [int(s)]
    if len(s) == 2:
        variants.append(int(s[0]) + int(s[1]))
    if len(s) == 3:
        variants.append(int(s[0]) + int(s[1]) + int(s[2]))
        variants.append(int(s[:2]) + int(s[2]))
        variants.append(int(s[0]) + int(s[1:]))
    return tuple(variants)

@cache
def cmp_seq(seq1, seq2):
    try:
        vars1, *seq1 = seq1
        vars2, *seq2 = seq2
    except ValueError:
        return False
    for v1 in vars1:
        for v2 in vars2:
            nv1, nv2 = cmp(v1, v2)
            if nv1 and nv2:
                continue  # doesn't match
            elif nv1 and cmp_seq(((nv1,),) + tuple(seq1), tuple(seq2)):
                return True
            elif nv2 and cmp_seq(tuple(seq1), ((nv2,),) + tuple(seq2)):
                return True
            elif not nv1 and not nv2:
                if not seq1 and not seq2:
                    return True
                elif cmp_seq(tuple(seq1), tuple(seq2)):
                    return True
    return False

def cmp(a, b):
    if isinstance(a, int) and isinstance(b, int):
        l = min(a, b)
        return (a - l, b - l)
    elif isinstance(a, str) and isinstance(b, str):
        l = min(len(a), len(b))
        if a[:l] != b[:l]:
            return (a, b)
        return (a[l:], b[l:])
    elif isinstance(a, int) and isinstance(b, str):
        l = min(a, len(b))
        return(a - l, b[l:])
    elif isinstance(a, str) and isinstance(b, int):
        l = min(len(a), b)
        return(a[l:], b - l)

if __name__ == "__main__":
    tests = (
        ("a7a242a", "a2a14", True),
        ("158", "716", True),
        ("internationalization", "i18n", True),
        ("l123e", "44", True),
        ("a5b", "c5b", False),
        ("112s", "g841", True),
        ("ab", "a2", False),
        ("a433a", "316a", False),
        ("g71g15q581g59g67g3q599q56g87g", "44q4g31g7q636g699g383g44q453g", False),
        ("g141g9g94q592q62g78g779q61g779g28", "g2q4g795g87g828q321g532g195q11g77", False),
    )
    from time import time
    start = time()
    for s1, s2, expected in tests:
        print(Solution().possiblyEquals(s1, s2) == expected, flush=True)
    print(f"Time elapsed: {time() - start} sec")
