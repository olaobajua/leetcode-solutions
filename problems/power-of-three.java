/**
 * 326. Power of Three [Easy]
 * Given an integer n, return true if it is a power of three. Otherwise,
 * return false.
 *
 * An integer n is a power of three, if there exists an integer x such that
 * n == 3ˣ.
 *
 * Example 1:
 * Input: n = 27
 * Output: true
 *
 * Example 2:
 * Input: n = 0
 * Output: false
 *
 * Example 3:
 * Input: n = 9
 * Output: true
 *
 * Constraints:
 *     ∙ -2³¹ <= n <= 2³¹ - 1
 *
 * Follow up: Could you solve it without loops/recursion?
 */
class Solution {
    public boolean isPowerOfThree(int n) {
        return n == 1 ? true : n < 1 || n % 3 > 0 ? false : isPowerOfThree(n / 3);
    }
}
