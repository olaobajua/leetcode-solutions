/**
 * 543. Diameter of Binary Tree (Easy)
 * Given the root of a binary tree, return the length of the diameter of the
 * tree.
 *
 * The diameter of a binary tree is the length of the longest path between any
 * two nodes in a tree. This path may or may not pass through the root.
 *
 * The length of a path between two nodes is represented by the number of edges
 * between them.
 *
 * Example 1:
 * Input: root = [1,2,3,4,5]
 * Output: 3
 * Explanation: 3 is the length of the path [4,2,1,3] or [5,2,1,3].
 *
 * Example 2:
 * Input: root = [1,2]
 * Output: 1
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [1, 10⁴].
 *     -100 <= Node.val <= 100
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @return {number}
 */
var diameterOfBinaryTree = function(root) {
    return diameter(root) - 1;
};

function diameter(root) {
    if (root === null) {
        return 0
    }
    return Math.max(height(root.left) + height(root.right) + 1,
                    Math.max(diameter(root.left), diameter(root.right)));
}

function height(root) {
    if (root === null) {
        return 0;
    }
    return 1 + Math.max(height(root.left), height(root.right));
}

function build_tree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

tests = [
    [[1,2,3,4,5], 3],
    [[1,2], 1],
];

for (let [vals, expected] of tests) {
    let root = build_tree(vals);
    console.log(diameterOfBinaryTree(root) === expected);
}
