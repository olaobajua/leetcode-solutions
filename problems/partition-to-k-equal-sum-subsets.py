"""
 * 698. Partition to K Equal Sum Subsets (Medium)
 * Given an integer array nums and an integer k, return true if it is possible
 * to divide this array into k non-empty subsets whose sums are all equal.
 *
 * Example 1:
 * Input: nums = [4,3,2,3,5,2,1], k = 4
 * Output: true
 * Explanation: It's possible to divide it into 4 subsets (5), (1, 4), (2,3),
 * (2,3) with equal sums.
 *
 * Example 2:
 * Input: nums = [1,2,3,4], k = 3
 * Output: false
 *
 * Constraints:
 *     1 <= k <= nums.length <= 16
 *     1 <= nums[i] <= 10⁴
 *     The frequency of each element is in the range [1, 4].
"""
from functools import cache
from typing import List

class Solution:
    def canPartitionKSubsets(self, nums: List[int], k: int) -> bool:
        @cache
        def can_partition(groups, sum_left, nums):
            if groups == k and not nums:
                yield True
            if groups < k:
                for i, num in enumerate(nums):
                    if sum_left > num:
                        yield from can_partition(groups, sum_left - num,
                                                 nums[:i] + nums[i+1:])
                    elif sum_left == num:
                        yield from can_partition(groups + 1, group_sum,
                                                 nums[:i] + nums[i+1:])

        return (len(nums) >= k and
                (group_sum := sum(nums) / k) >= max(nums) and
                any(can_partition(0, group_sum, tuple(nums))))

if __name__ == "__main__":
    tests = (
        ([4,3,2,3,5,2,1], 4, True),
        ([1,2,3,4], 3, False),
        ([3522,181,521,515,304,123,2512,312,922,407,146,1932,4037,2646,3871,269], 5, True),
    )
    for nums, k, expected in tests:
        print(Solution().canPartitionKSubsets(nums, k) == expected)
