/**
 * 345. Reverse Vowels of a String [Easy]
 * Given a string s, reverse only all the vowels in the string and return it.
 *
 * The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both
 * lower and upper cases, more than once.
 *
 * Example 1:
 * Input: s = "hello"
 * Output: "holle"
 * Example 2:
 * Input: s = "leetcode"
 * Output: "leotcede"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 3 * 10⁵
 *     ∙ s consist of printable ASCII characters.
 */
class Solution {
    private final String VOWELS = "AEIOUaeiou";
    public String reverseVowels(String s) {
        StringBuilder vs = new StringBuilder(s.length());
        for (char c : s.toCharArray())
            if (VOWELS.indexOf(c) > -1)
                vs.append(c);
        vs.reverse();
        StringBuilder ret = new StringBuilder(s.length());
        int i = 0;
        for (char c : s.toCharArray())
            if (VOWELS.indexOf(c) > -1)
                ret.append(vs.charAt(i++));
            else
                ret.append(c);
        return ret.toString();
    }
}
