"""
 * 315. Count of Smaller Numbers After Self [Hard]
 * You are given an integer array nums and you have to return a new counts
 * array. The counts array has the property where counts[i] is the number of
 * smaller elements to the right of nums[i].
 *
 * Example 1:
 * Input: nums = [5,2,6,1]
 * Output: [2,1,1,0]
 * Explanation:
 * To the right of 5 there are 2 smaller elements (2 and 1).
 * To the right of 2 there is only 1 smaller element (1).
 * To the right of 6 there is 1 smaller element (1).
 * To the right of 1 there is 0 smaller element.
 *
 * Example 2:
 * Input: nums = [-1]
 * Output: [0]
 *
 * Example 3:
 * Input: nums = [-1,-1]
 * Output: [0,0]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ -10⁴ <= nums[i] <= 10⁴
"""
from typing import List
from sortedcontainers import SortedList

class Solution:
    def countSmaller(self, nums: List[int]) -> List[int]:
        snums = SortedList()
        return reversed([snums.add(x) or SortedList.bisect_left(snums, x)
                         for x in reversed(nums)])

if __name__ == "__main__":
    tests = (
        ([5,2,6,1], [2,1,1,0]),
        ([-1], [0]),
        ([-1,-1], [0, 0]),
        ([26,78,27,100,33,67,90,23,66,5,38,7,35,23,52,22,83,51,98,69,81,32,78,28,94,13,2,97,3,76,99,51,9,21,84,66,65,36,100,41],
         [10,27,10,35,12,22,28,8,19,2,12,2,9,6,12,5,17,9,19,12,14,6,12,5,12,3,0,10,0,7,8,4,0,0,4,3,2,0,1,0]),
    )
    for nums, expected in tests:
        print(Solution().countSmaller(nums) == expected)
