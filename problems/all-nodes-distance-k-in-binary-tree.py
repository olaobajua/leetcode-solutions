"""
 * 863. All Nodes Distance K in Binary Tree [Medium]
 * Given the root of a binary tree, the value of a target node target, and an
 * integer k, return an array of the values of all nodes that have a distance k
 * from the target node.
 *
 * You can return the answer in any order.
 *
 * Example 1:
 *       3
 *   5       1
 * 6   2    0 8
 *    7 4
 * Input: root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, k = 2
 * Output: [7,4,1]
 * Explanation: The nodes that are a distance 2 from the target node (with
 * value 5) have values 7, 4, and 1.
 *
 * Example 2:
 * Input: root = [1], target = 1, k = 3
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 500].
 *     ∙ 0 <= Node.val <= 500
 *     ∙ All the values Node.val are unique.
 *     ∙ target is the value of one of the nodes in the tree.
 *     ∙ 0 <= k <= 1000
"""
from collections import defaultdict
from typing import List

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def distanceK(self, root: TreeNode, target: TreeNode, k: int) -> List[int]:
        graph = defaultdict(list)
        to_visit = [root]
        for node in to_visit:
            if node.left is not None:
                to_visit.append(node.left)
                graph[node].append(node.left)
                graph[node.left].append(node)
            if node.right is not None:
                to_visit.append(node.right)
                graph[node].append(node.right)
                graph[node.right].append(node)

        ret = []
        to_visit = [(target, 0)]
        visited = {target}
        for node, distance in to_visit:
            if distance == k:
                ret.append(node.val)
            for child in graph[node]:
                if child not in visited:
                    visited.add(child)
                    to_visit.append((child, distance + 1))

        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([3,5,1,6,2,0,8,None,None,7,4], 5, 2, [7,4,1]),
        ([1], 1, 3, []),
    )
    for nums, target, k, expected in tests:
        root = build_tree(nums)
        to_visit = [root]
        for node in to_visit:
            if node.val == target:
                target = node
                break
            if node.left is not None:
                to_visit.append(node.left)
            if node.right is not None:
                to_visit.append(node.right)

        print(set(Solution().distanceK(root, target, k)) == set(expected))
