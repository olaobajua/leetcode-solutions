"""
 * 504. Base 7 [Easy]
 * Given an integer num, return a string of its base 7 representation.
 *
 * Example 1:
 * Input: num = 100
 * Output: "202"
 *
 * Example 2:
 * Input: num = -7
 * Output: "-10"
 *
 * Constraints:
 *     ∙ -10⁷ <= num <= 10⁷
"""
class Solution:
    def convertToBase7(self, num: int) -> str:
        if num == 0:
            return "0"

        negative = False
        if num < 0:
            num = -num
            negative = True

        ret = []
        while num:
            ret.append(str(num % 7))
            num //= 7

        ret = "".join(reversed(ret))
        return "-" + ret if negative else ret

if __name__ == "__main__":
    tests = (
        (100, "202"),
        (-7, "-10"),
        (0, "0"),
    )
    for num, expected in tests:
        print(Solution().convertToBase7(num) == expected)
