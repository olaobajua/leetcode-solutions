"""
 * 1489. Find Critical and Pseudo-Critical Edges in Minimum Spanning Tree
 * [Hard]
 * Given a weighted undirected connected graph with n vertices numbered from 0
 * to n - 1, and an array edges where edges[i] = [aᵢ, bᵢ, weightᵢ] represents a
 * bidirectional and weighted edge between nodes aᵢ and bᵢ. A minimum spanning
 * tree (MST) is a subset of the graph's edges that connects all vertices
 * without cycles and with the minimum possible total edge weight.
 *
 * Find all the critical and pseudo-critical edges in the given graph's
 * minimum spanning tree (MST). An MST edge whose deletion from the graph would
 * cause the MST weight to increase is called a critical edge. On the other
 * hand, a pseudo-critical edge is that which can appear in some MSTs but not
 * all.
 *
 * Note that you can return the indices of the edges in any order.
 *
 * Example 1:
 * Input: n = 5, edges =
 * [[0,1,1],[1,2,1],[2,3,2],[0,3,2],[0,4,3],[3,4,3],[1,4,6]]
 * Output: [[0,1],[2,3,4,5]]
 * Explanation: The figure above describes the graph.
 * The following figure shows all the possible MSTs:
 * Notice that the two edges 0 and 1 appear in all MSTs, therefore they are
 * critical edges, so we return them in the first list of the output.
 * The edges 2, 3, 4, and 5 are only part of some MSTs, therefore they are
 * considered pseudo-critical edges. We add them to the second list of the
 * output.
 *
 * Example 2:
 * Input: n = 4, edges = [[0,1,1],[1,2,1],[2,3,1],[0,3,1]]
 * Output: [[],[0,1,2,3]]
 * Explanation: We can observe that since all 4 edges have equal weight,
 * choosing any 3 edges from the given 4 will yield an MST. Therefore all 4
 * edges are pseudo-critical.
 *
 * Constraints:
 *     ∙ 2 <= n <= 100
 *     ∙ 1 <= edges.length <= min(200, n * (n - 1) / 2)
 *     ∙ edges[i].length == 3
 *     ∙ 0 <= aᵢ < bᵢ < n
 *     ∙ 1 <= weightᵢ <= 1000
 *     ∙ All pairs (aᵢ, bᵢ) are distinct.
"""
from math import inf
from operator import itemgetter
from typing import List

class Solution:
    def findCriticalAndPseudoCriticalEdges(self, n: int, edges: List[List[int]]) -> List[List[int]]:
        def kruskal(n, edges, banned_idx, include_idx):
            uf = DisjointSet(n)
            total_weight = 0
            if include_idx != -1:
                x, y, w, _ = edges[include_idx]
                uf.union(x, y)
                total_weight += w
                n -= 1

            for i, (x, y, weight, _) in enumerate(edges):
                if i != banned_idx and uf.union(x, y):
                    total_weight += weight
                    n -= 1

            return inf if n > 1 else total_weight

        edges = [(x, y, w, i) for i, (x, y, w) in enumerate(edges)]
        edges.sort(key=itemgetter(2))

        critical_edges = []
        mst_weight = kruskal(n, edges, -1, -1)
        for i in range(len(edges)):
            total_weight = kruskal(n, edges, i, -1)
            if total_weight > mst_weight:
                critical_edges.append(edges[i][3])

        pseudo_critical_edges = []
        for i in range(len(edges)):
            if edges[i][3] in critical_edges:
                continue
            total_weight = kruskal(n, edges, -1, i)
            if total_weight == mst_weight:
                pseudo_critical_edges.append(edges[i][3])

        return [critical_edges, pseudo_critical_edges]

class DisjointSet:
    def __init__(self, n):
        self.parent = list(range(n))
        self.rank = [1] * n

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def find_set(self, x):
        x_parent = self.find(x)
        return {i for i in self.parent if self.find(i) == x_parent}

    def union(self, x, y):
        if (px := self.find(x)) == (py := self.find(y)):
            return False

        if self.rank[px] < self.rank[py]:
            self.parent[px] = py
        elif self.rank[px] > self.rank[py]:
            self.parent[py] = px
        else:
            self.rank[px] += 1
            self.parent[py] = px
        return True

if __name__ == "__main__":
    tests = (
        (5, [[0,1,1],[1,2,1],[2,3,2],[0,3,2],[0,4,3],[3,4,3],[1,4,6]], [[0,1],[2,3,4,5]]),
        (4, [[0,1,1],[1,2,1],[2,3,1],[0,3,1]], [[],[0,1,2,3]]),
    )
    for n, edges, expected in tests:
        print(Solution().findCriticalAndPseudoCriticalEdges(n, edges) == expected)
