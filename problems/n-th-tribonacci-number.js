/**
 * 1137. N-th Tribonacci Number (Easy)
 * The Tribonacci sequence Tn is defined as follows: 
 *
 * T₀ = 0, T₁ = 1, T₂ = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.
 *
 * Given n, return the value of Tn.
 *
 * Example 1:
 * Input: n = 4
 * Output: 4
 * Explanation:
 * T_3 = 0 + 1 + 1 = 2
 * T_4 = 1 + 1 + 2 = 4
 *
 * Example 2:
 * Input: n = 25
 * Output: 1389537
 *
 * Constraints:
 *     ∙ 0 <= n <= 37
 *     ∙ The answer is guaranteed to fit within a 32-bit integer, ie.
 *       answer <= 2³¹ - 1.
 */

/**
 * @param {number} n
 * @return {number}
 */
var tribonacci = function(n) {
    let trib = [0, 1, 1];
    for (let i = 3; i <= n; ++i) {
        trib[i % 3] = trib[0] + trib[1] + trib[2];
    }
    return trib[n % 3];
};

let tests = [
    [4, 4],
    [25, 1389537],
]
for (let [n, expected] of tests) {
    console.log(tribonacci(n) == expected);
}

