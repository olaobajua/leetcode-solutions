"""
 * 23. Merge k Sorted Lists (Hard)
 * You are given an array of k linked-lists lists, each linked-list is sorted
 * in ascending order.
 *
 * Merge all the linked-lists into one sorted linked-list and return it.
 *
 * Example 1:
 * Input: lists = [[1,4,5],[1,3,4],[2,6]]
 * Output: [1,1,2,3,4,4,5,6]
 * Explanation: The linked-lists are:
 * [
 *   1->4->5,
 *   1->3->4,
 *   2->6
 * ]
 * merging them into one sorted list:
 * 1->1->2->3->4->4->5->6
 *
 * Example 2:
 * Input: lists = []
 * Output: []
 *
 * Example 3:
 * Input: lists = [[]]
 * Output: []
 *
 * Constraints:
 *     ∙ k == lists.length
 *     ∙ 0 <= k <= 10⁴
 *     ∙ 0 <= lists[i].length <= 500
 *     ∙ -10⁴ <= lists[i][j] <= 10⁴
 *     ∙ lists[i] is sorted in ascending order.
 *     ∙ The sum of lists[i].length won't exceed 10⁴.
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def mergeKLists(self, lists: List[Optional[ListNode]]) -> Optional[ListNode]:
        n = len(lists);
        if n == 0:
            return None
        if n == 1:
            return lists[0]
        i = 0
        n -= 1
        while i < n:
            lists[i] = mergeSortedLists(lists[i], lists[n])
            i += 1
            n -= 1
        return self.mergeKLists(lists[0:n+1]);

def mergeSortedLists(a, b):
    if not a:
        return b
    if not b:
        return a
    if a.val <= b.val:
        a.next = mergeSortedLists(a.next, b)
        return a
    else:
        b.next = mergeSortedLists(a, b.next)
        return b;

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([[1,4,5],[1,3,4],[2,6]], [1,1,2,3,4,4,5,6]),
        ([[1,4,5]], [1,4,5]),
        ([[1,2,3],[1,2]], [1,1,2,2,3]),
        ([], []),
        ([[]], []),
        ([[],[]], []),
    )
    for lists, expected in tests:
        lists = [list_to_linked_list(l) for l in lists]
        print(linked_list_to_list(Solution().mergeKLists(lists)) == expected)
