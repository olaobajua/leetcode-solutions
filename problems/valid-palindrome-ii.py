"""
 * 680. Valid Palindrome II (Easy)
 * Given a string s, return true if the s can be palindrome after deleting at
 * most one character from it.
 *
 * Example 1:
 * Input: s = "aba"
 * Output: true
 *
 * Example 2:
 * Input: s = "abca"
 * Output: true
 * Explanation: You could delete the character 'c'.
 *
 * Example 3:
 * Input: s = "abc"
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ s consists of lowercase English letters.
"""
class Solution:
    def validPalindrome(self, s: str) -> bool:
        n = len(s)
        i = 0
        while i < n / 2 and s[i] == s[n-i-1]:
            i += 1
        s = s[i:n-i]
        return s[1:] == s[1:][::-1] or s[:-1] == s[:-1][::-1]

if __name__ == "__main__":
    tests = (
        ("aba", True),
        ("abca", True),
        ("abc", False),
        ("deeee", True),
        ("ebcbbececabbacecbbcbe", True),
    )
    for s, expected in tests:
        print(Solution().validPalindrome(s) == expected)
