"""
 * 2471. Minimum Number of Operations to Sort a Binary Tree by Level [Medium]
 * You are given the root of a binary tree with unique values.
 *
 * In one operation, you can choose any two nodes at the same level and swap
 * their values.
 *
 * Return the minimum number of operations needed to make the values at each
 * level sorted in a strictly increasing order.
 *
 * The level of a node is the number of edges along the path between it and
 * the root node.
 *
 * Example 1:
 * Input: root = [1,4,3,7,6,8,5,null,null,null,null,9,null,10]
 * Output: 3
 * Explanation:
 * - Swap 4 and 3. The 2ⁿᵈ level becomes [3,4].
 * - Swap 7 and 5. The 3ʳᵈ level becomes [5,6,8,7].
 * - Swap 8 and 7. The 3ʳᵈ level becomes [5,6,7,8].
 * We used 3 operations so return 3.
 * It can be proven that 3 is the minimum number of operations needed.
 *
 * Example 2:
 * Input: root = [1,3,2,7,6,5,4]
 * Output: 3
 * Explanation:
 * - Swap 3 and 2. The 2ⁿᵈ level becomes [2,3].
 * - Swap 7 and 4. The 3ʳᵈ level becomes [4,6,5,7].
 * - Swap 6 and 5. The 3ʳᵈ level becomes [4,5,6,7].
 * We used 3 operations so return 3.
 * It can be proven that 3 is the minimum number of operations needed.
 *
 * Example 3:
 * Input: root = [1,2,3,4,5,6]
 * Output: 0
 * Explanation: Each level is already sorted in increasing order so return 0.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁵].
 *     ∙ 1 <= Node.val <= 10⁵
 *     ∙ All the values of the tree are unique.
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def minimumOperations(self, root: Optional[TreeNode]) -> int:
        ret = 0
        level = [root]
        while level:
            level = [n for node in level for n in (node.left, node.right) if n]
            vals = [node.val for node in level]
            indices = {val: i for i, val in enumerate(vals)}
            for i, (x, y) in enumerate(zip(sorted(vals), vals)):
                if x != y:
                    ret += 1
                    j = indices[x]
                    vals[i], vals[j] = x, y
                    indices[x], indices[y] = i, j
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,4,3,7,6,8,5,None,None,None,None,9,None,10], 3),
        ([1,3,2,7,6,5,4], 3),
        ([1,2,3,4,5,6], 0),
    )
    for root, expected in tests:
        print(Solution().minimumOperations(build_tree(root)) == expected)
