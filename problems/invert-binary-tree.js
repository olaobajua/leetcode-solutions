/**
 * 226. Invert Binary Tree (Easy)
 * Given the root of a binary tree, invert the tree, and return its root.
 *
 * Example 1:
 * Input: root = [4,2,7,1,3,6,9]
 * Output: [4,7,2,9,6,3,1]
 *
 * Example 2:
 * Input: root = [2,1,3]
 * Output: [2,3,1]
 *
 * Example 3:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [0, 100].
 *     -100 <= Node.val <= 100
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var invertTree = function(root) {
    if (root) {
        [root.left, root.right] = [root.right, root.left];
        invertTree(root.left);
        invertTree(root.right);
    }
    return root;
};

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

function* treeToList(root) {
    let nodes = [root];
    for (let n of nodes) {
        if (n) {
            yield n.val;
            nodes.push(n.left, n.right);
        }
    }
}

const tests = [
    [[4,2,7,1,3,6,9], [4,7,2,9,6,3,1]],
    [[2,1,3], [2,3,1]],
    [[], []],
];
for (let [nums, expected] of tests) {
    console.log([...treeToList(invertTree(buildTree(nums)))]
                .every((x, i) => x == expected[i]));
}
