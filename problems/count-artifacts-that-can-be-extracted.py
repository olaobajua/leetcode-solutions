"""
 * 2201. Count Artifacts That Can Be Extracted (Medium)
 * There is an n x n 0-indexed grid with some artifacts buried in it. You are
 * given the integer n and a 0-indexed 2D integer array artifacts describing
 * the positions of the rectangular artifacts where
 * artifacts[i] = [r1ᵢ, c1ᵢ, r2ᵢ, c2ᵢ] denotes that the iᵗʰ artifact is buried
 * in the subgrid where:
 *     ∙ (r1ᵢ, c1ᵢ) is the coordinate of the top-left cell of the iᵗʰ artifact
 *     ∙ (r2ᵢ, c2ᵢ) is the coordinate of the bottom-right cell of the iᵗʰ
 *       artifact.
 *
 * You will excavate some cells of the grid and remove all the mud from them.
 * If the cell has a part of an artifact buried underneath, it will be
 * uncovered. If all the parts of an artifact are uncovered, you can extract
 * it.
 *
 * Given a 0-indexed 2D integer array dig where dig[i] = [rᵢ, cᵢ] indicates
 * that you will excavate the cell (rᵢ, cᵢ), return the number of artifacts
 * that you can extract.
 *
 * The test cases are generated such that:
 *     ∙ No two artifacts overlap.
 *     ∙ Each artifact only covers at most 4 cells.
 *     ∙ The entries of dig are unique.
 *
 * Example 1:
 * Input: n = 2, artifacts = [[0,0,0,0],[0,1,1,1]], dig = [[0,0],[0,1]]
 * Output: 1
 * Explanation:
 * The different colors represent different artifacts. Excavated cells are
 * labeled with a 'D' in the grid.
 * There is 1 artifact that can be extracted, namely the red artifact.
 * The blue artifact has one part in cell (1,1) which remains uncovered, so we
 * cannot extract it.
 * Thus, we return 1.
 *
 * Example 2:
 * Input: n = 2, artifacts = [[0,0,0,0],[0,1,1,1]], dig = [[0,0],[0,1],[1,1]]
 * Output: 2
 * Explanation: Both the red and blue artifacts have all parts uncovered
 * (labeled with a 'D') and can be extracted, so we return 2.
 *
 * Constraints:
 *     ∙ 1 <= n <= 1000
 *     ∙ 1 <= artifacts.length, dig.length <= min(n2, 105)
 *     ∙ artifacts[i].length == 4
 *     ∙ dig[i].length == 2
 *     ∙ 0 <= r1ᵢ, c1ᵢ, r2ᵢ, c2ᵢ, rᵢ, cᵢ <= n - 1
 *     ∙ r1ᵢ <= r2ᵢ
 *     ∙ c1ᵢ <= c2ᵢ
 *     ∙ No two artifacts will overlap.
 *     ∙ The number of cells covered by an artifact is at most 4.
 *     ∙ The entries of dig are unique.
"""
from typing import List

class Solution:
    def digArtifacts(self, n: int, artifacts: List[List[int]], dig: List[List[int]]) -> int:
        grid = [[None]*n for _ in range(n)]
        artifact_parts = [0]*len(artifacts)
        for i, (row1, col1, row2, col2) in enumerate(artifacts):
            for row in range(row1, row2 + 1):
                for col in range(col1, col2 + 1):
                    grid[row][col] = i
                    artifact_parts[i] += 1
        for row, col in dig:
            if (a := grid[row][col]) is not None:
                artifact_parts[a] -= 1
        return sum(a == 0 for a in artifact_parts)

if __name__ == "__main__":
    tests = (
        (2, [[0,0,0,0],[0,1,1,1]], [[0,0],[0,1]], 1),
        (2, [[0,0,0,0],[0,1,1,1]], [[0,0],[0,1],[1,1]], 2),
        (9, [[4,5,5,5],[5,1,7,1],[1,8,3,8],[1,1,1,2],[0,5,3,5],[6,2,7,2],[7,5,7,6],[6,4,6,6],[2,7,5,7],[0,2,0,2],[7,7,8,8],[3,1,4,2],[2,3,3,3],[5,3,7,3],[8,4,8,4],[2,6,5,6],[8,1,8,2],[4,8,4,8],[1,0,4,0],[6,8,6,8],[1,3,1,4],[0,7,0,8],[0,3,0,4],[0,6,0,6]], [[0,3],[0,4],[8,5],[8,6],[8,7],[0,8],[8,8],[0,6],[1,1],[1,8],[2,0],[2,2],[2,3],[2,4],[2,5],[2,8],[3,2],[3,3],[3,4],[3,5],[3,6],[3,7],[4,0],[4,3],[4,4],[4,6],[4,7],[5,1],[5,2],[5,6],[5,8],[6,0],[6,2],[6,4],[6,5],[6,6],[7,0],[7,1],[7,4],[7,5],[7,7]], 4),
    )
    for n, artifacts, dig, expected in tests:
        print(Solution().digArtifacts(n, artifacts, dig) == expected)
