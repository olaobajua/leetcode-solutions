/**
 * 79. Word Search (Medium)
 * Given an m x n grid of characters board and a string word, return true if
 * word exists in the grid.
 *
 * The word can be constructed from letters of sequentially adjacent cells,
 * where adjacent cells are horizontally or vertically neighboring. The same
 * letter cell may not be used more than once.
 *
 * Example 1:
 * Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]],
 * word = "ABCCED"
 * Output: true
 *
 * Example 2:
 * Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]],
 * word = "SEE"
 * Output: true
 *
 * Example 3:
 * Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]],
 * word = "ABCB"
 * Output: false
 *
 * Constraints:
 *     m == board.length
 *     n = board[i].length
 *     1 <= m, n <= 6
 *     1 <= word.length <= 15
 *     board and word consists of only lowercase and uppercase English letters.
 *
 * Follow up: Could you use search pruning to make your solution faster with a
 * larger board?
 */

/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */
var exist = function(board, word) {
    function find_word(r, c, word) {
        if (board[r][c] == word[0]) {
            if (word.length == 1) {
                return true;
            }
            for (let [nr, nc] of [[r + 1, c], [r, c + 1], [r - 1, c], [r, c - 1]]) {
                if (0 <= nr && nr < m && 0 <= nc && nc < n) {
                    let letter;
                    [letter, board[r][c]] = [board[r][c], null];
                    if (find_word(nr, nc, word.slice(1))) {
                        board[r][c] = letter;
                        return true;
                    }
                    board[r][c] = letter;
                }
            }
        }
        return false;
    }

    let flattenBoard = board.flat();
    for (let letter of word) {
        const i = flattenBoard.indexOf(letter);
        if (i > -1) {
            flattenBoard.splice(i, 1);
        } else {
            return false;
        }
    }
    const m = board.length;
    const n = board[0].length;
    for (let i = 0; i < m; ++i) {
        for (let j = 0; j < n; ++j) {
            if (find_word(i, j, word)) {
                return true;
            }
        }
    }
    return false;
};

tests = [
    [[["A","B","C","E"],
      ["S","F","C","S"],
      ["A","D","E","E"]],
     "ABCCED", true],
    [[["A","B","C","E"],
      ["S","F","C","S"],
      ["A","D","E","E"]],
     "SEE", true],
    [[["A","B","C","E"],
      ["S","F","C","S"],
      ["A","D","E","E"]],
     "ABCB", false],
    [[["A","B","C","E"],
      ["S","F","E","S"],
      ["A","D","E","E"]],
     "ABCESEEEFS", true],
    [[["a"]], "b", false],
    [[["A","A","A","A","A","A"],
      ["A","A","A","A","A","A"],
      ["A","A","A","A","A","A"],
      ["A","A","A","A","A","A"],
      ["A","A","A","A","A","A"],
      ["A","A","A","A","A","A"]],
     "BAAAAAAAAAAAAAA", false],
]
for (let [board, word, expected] of tests) {
    console.log(exist(board, word) == expected);
}
