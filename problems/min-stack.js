/**
 * 155. Min Stack (Easy)
 * Design a stack that supports push, pop, top, and retrieving the minimum
 * element in constant time.
 *
 * Implement the MinStack class:
 *     MinStack() initializes the stack object.
 *     void push(int val) pushes the element val onto the stack.
 *     void pop() removes the element on the top of the stack.
 *     int top() gets the top element of the stack.
 *     int getMin() retrieves the minimum element in the stack.
 *
 * Example 1:
 * Input
 * ["MinStack","push","push","push","getMin","pop","top","getMin"]
 * [[],[-2],[0],[-3],[],[],[],[]]
 *
 * Output
 * [null,null,null,null,-3,null,0,-2]
 *
 * Explanation
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.getMin(); // return -3
 * minStack.pop();
 * minStack.top();    // return 0
 * minStack.getMin(); // return -2
 *
 * Constraints:
 *     ∙ -2³¹ <= val <= 2³¹ - 1
 *     ∙ Methods pop, top and getMin operations will always be called on
 *       non-empty stacks.
 *     ∙ At most 3 * 10⁴ calls will be made to push, pop, top, and getMin.
 */
var MinStack = function() {
    this.__arr = [];
    this.__min = [];
};

/**
 * @param {number} val
 * @return {void}
 */
MinStack.prototype.push = function(val) {
    this.__arr.push(val)
    if (!this.__min.length || val <= this.__min[this.__min.length-1]) {
        this.__min.push(val);
    }
};

/**
 * @return {void}
 */
MinStack.prototype.pop = function() {
    if (this.__min[this.__min.length-1] === this.__arr[this.__arr.length-1]) {
        this.__min.pop();
    }
    this.__arr.pop();
};

/**
 * @return {number}
 */
MinStack.prototype.top = function() {
    return this.__arr[this.__arr.length-1];
};

/**
 * @return {number}
 */
MinStack.prototype.getMin = function() {
    return this.__min[this.__min.length-1];
};

var obj = new MinStack();
obj.push(-2);
obj.push(0);
obj.push(-3);
console.log(obj.getMin() == -3);
obj.pop();
console.log(obj.top() == 0);
console.log(obj.getMin() == -2);
