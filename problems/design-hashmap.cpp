/**
 * 706. Design HashMap [Easy]
 * Design a HashMap without using any built-in hash table libraries.
 *
 * Implement the MyHashMap class:
 *     ∙ MyHashMap() initializes the object with an empty map.
 *     ∙ void put(int key, int value) inserts a (key, value) pair into the
 *       HashMap. If the key already exists in the map, update the
 *       corresponding value.
 *     ∙ int get(int key) returns the value to which the specified key is
 *       mapped, or -1 if this map contains no mapping for the key.
 *     ∙ void remove(key) removes the key and its corresponding value if the
 *       map contains the mapping for the key.
 *
 * Example 1:
 * Input
 * ["MyHashMap", "put", "put", "get", "get", "put", "get", "remove", "get"]
 * [[], [1, 1], [2, 2], [1], [3], [2, 1], [2], [2], [2]]
 * Output
 * [null, null, null, 1, -1, null, 1, null, -1]
 *
 * Explanation
 * MyHashMap myHashMap = new MyHashMap();
 * myHashMap.put(1, 1); // The map is now [[1,1]]
 * myHashMap.put(2, 2); // The map is now [[1,1], [2,2]]
 * myHashMap.get(1);    // return 1, The map is now [[1,1], [2,2]]
 * myHashMap.get(3);    // return -1 (i.e., not found), The map is now [[1,1],
 * [2,2]]
 * myHashMap.put(2, 1); // The map is now [[1,1], [2,1]] (i.e., update the
 * existing value)
 * myHashMap.get(2);    // return 1, The map is now [[1,1], [2,1]]
 * myHashMap.remove(2); // remove the mapping for 2, The map is now [[1,1]]
 * myHashMap.get(2);    // return -1 (i.e., not found), The map is now [[1,1]]
 *
 * Constraints:
 *     ∙ 0 <= key, value <= 10⁶
 *     ∙ At most 10⁴ calls will be made to put, get, and remove.
 */
#include <iostream>
#include <vector>

class MyHashMap {
private:
    std::vector<std::vector<std::pair<int, int>>> hashmap;

    int eval_hash(int key) {
        // https://en.wikipedia.org/wiki/Hash_function#Multiplicative_hashing
        // hₐ(K) = (a * K mod 2ʷ) / 2ʷ⁻ᵐ
        // K is our number (key), we want to hash.
        // a is some big odd number (sometimes good idea to use prime number)
        // m is length in bits of output we wan to have
        // w is size of machine word. Here we we can choose any w > m
        // Let's take K = key, a = 39916801, m = 15, w = 20

        return (((long)key * 39916801) & ((1 << 20) - 1)) >> 5;
    }

public:
    MyHashMap() {
        hashmap.resize(1 << 15);
    }

    void put(int key, int value) {
        std::vector<std::pair<int, int>>& elements = hashmap[eval_hash(key)];
        for (auto& entry : elements) {
            if (entry.first == key) {
                entry = std::make_pair(key, value);
                return;
            }
        }
        elements.push_back(std::make_pair(key, value));
    }

    int get(int key) {
        const std::vector<std::pair<int, int>>& elements = hashmap[eval_hash(key)];
        for (const auto& entry : elements) {
            if (entry.first == key) {
                return entry.second;
            }
        }
        return -1;
    }

    void remove(int key) {
        std::vector<std::pair<int, int>>& elements = hashmap[eval_hash(key)];
        for (auto it = elements.begin(); it != elements.end(); ++it) {
            if (it->first == key) {
                elements.erase(it);
                return;
            }
        }
    }
};


int main() {
    // Example usage
    MyHashMap myHashMap;
    myHashMap.put(1, 1);
    myHashMap.put(2, 2);
    std::cout << 1 == myHashMap.get(1) << std::endl;
    std::cout << -1 == myHashMap.get(3) << std::endl;
    myHashMap.put(2, 1);
    std::cout << 1 == myHashMap.get(2) << std::endl;
    myHashMap.remove(2);
    std::cout << -1 == myHashMap.get(2) << std::endl;

    return 0;
}

