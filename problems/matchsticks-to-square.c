/**
 * 473. Matchsticks to Square (Medium)
 * You are given an integer array matchsticks where matchsticks[i] is the
 * length of the iᵗʰ matchstick. You want to use all the matchsticks to make
 * one square. You should not break any stick, but you can link them up, and
 * each matchstick must be used exactly one time.
 *
 * Return true if you can make this square and false otherwise.
 *
 * Example 1:
 * Input: matchsticks = [1,1,2,2,2]
 * Output: true
 * Explanation: You can form a square with length 2, one side of the square
 * came two sticks with length 1.
 *
 * Example 2:
 * Input: matchsticks = [3,3,3,3,4]
 * Output: false
 * Explanation: You cannot find a way to form a square with all the
 * matchsticks.
 *
 * Constraints:
 *     ∙ 1 <= matchsticks.length <= 15
 *     ∙ 1 <= matchsticks[i] <= 10⁸
 */
int dfs(int mask, int matchsticks[], int cache[], int side, int n) {
    if (cache[mask] == -2) {
        cache[mask] = -1;
        if (mask == 0) {
            cache[mask] = 0;
        } else {
            for (int i = 0; i < n; ++i) {
                if (mask & 1 << i) {
                    int s = dfs(mask ^ 1 << i, matchsticks, cache, side, n);
                    if (s >= 0 && s + matchsticks[i] <= side) {
                        cache[mask] = (s + matchsticks[i]) % side;
                        break;
                    }
                }
            }
        }
    }
    return cache[mask];
}
bool makesquare(int *matchsticks, int matchsticksSize) {
    static int cache[1<<15];
    for (int i = 0; i < (1 << matchsticksSize); ++i) {
        cache[i] = -2;
    }
    int perimeter = 0, max_stick = 0;
    for (int i = 0; i < matchsticksSize; ++i) {
        perimeter += matchsticks[i];
        max_stick = fmax(max_stick, matchsticks[i]);
    }
    int side = perimeter / 4;
    if (perimeter % 4 || max_stick > side) {
        return false;
    }
    return dfs((1 << matchsticksSize) - 1,
               matchsticks, cache, side, matchsticksSize) == 0;
}
