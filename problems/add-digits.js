/**
 * 258. Add Digits (Easy)
 * Given an integer num, repeatedly add all its digits until the result has
 * only one digit, and return it.
 *
 * Example 1:
 * Input: num = 38
 * Output: 2
 * Explanation: The process is
 * 38 --> 3 + 8 --> 11
 * 11 --> 1 + 1 --> 2
 * Since 2 has only one digit, return it.
 *
 * Example 2:
 * Input: num = 0
 * Output: 0
 *
 * Constraints:
 *     ∙ 0 <= num <= 2³¹ - 1
 *
 * Follow up: Could you do it without any loop/recursion in O(1) runtime?
 */

/**
 * @param {number} num
 * @return {number}
 */
var addDigits = function(num) {
    // https://en.wikipedia.org/wiki/Digital_root#Congruence_formula
    return num < 10 ? num : num % 9 == 0 ? 9 : num % 9;
};

const tests = [
    [38, 2],
    [0, 0],
];

for (const [num, expected] of tests) {
    console.log(addDigits(num) == expected);
}
