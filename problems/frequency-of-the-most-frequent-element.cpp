/**
 * 1838. Frequency of the Most Frequent Element [Medium]
 * The frequency of an element is the number of times it occurs in an array.
 *
 * You are given an integer array nums and an integer k. In one operation, you
 * can choose an index of nums and increment the element at that index by 1.
 *
 * Return the maximum possible frequency of an element after performing at
 * most k operations.
 *
 * Example 1:
 * Input: nums = [1,2,4], k = 5
 * Output: 3
 * Explanation: Increment the first element three times and the second element
 * two times to make nums = [4,4,4].
 * 4 has a frequency of 3.
 *
 * Example 2:
 * Input: nums = [1,4,8,13], k = 5
 * Output: 2
 * Explanation: There are multiple optimal solutions:
 * - Increment the first element three times to make nums = [4,4,8,13]. 4 has
 * a frequency of 2.
 * - Increment the second element four times to make nums = [1,8,8,13]. 8 has
 * a frequency of 2.
 * - Increment the third element five times to make nums = [1,4,13,13]. 13 has
 * a frequency of 2.
 *
 * Example 3:
 * Input: nums = [3,9,6], k = 2
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i] <= 10⁵
 *     ∙ 1 <= k <= 10⁵
 */
#include <vector>
#include <deque>
#include <algorithm>

class Solution {
public:
    int maxFrequency(vector<int>& nums, int k) {
        int ret = 0;
        std::deque<int> d;
        int inc = 0;

        std::sort(nums.begin(), nums.end());

        for (int x : nums) {
            if (!d.empty()) {
                inc += (x - d.back()) * d.size();
            }
            d.push_back(x);

            while (inc > k) {
                inc -= x - d.front();
                d.pop_front();
            }

            ret = std::max(ret, static_cast<int>(d.size()));
        }

        return ret;
    }
};
