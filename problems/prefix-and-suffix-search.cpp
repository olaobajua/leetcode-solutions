/**
 * 745. Prefix and Suffix Search (Hard)
 * Design a special dictionary with some words that searchs the words in it by
 * a prefix and a suffix.
 *
 * Implement the WordFilter class:
 *     ∙ WordFilter(string[] words) Initializes the object with the words in
 *       the dictionary.
 *     ∙ f(string prefix, string suffix) Returns the index of the word in the
 *       dictionary, which has the prefix prefix and the suffix suffix. If
 *       there is more than one valid index, return the largest of them. If
 *       there is no such word in the dictionary, return -1.
 *
 * Example 1:
 * Input
 * ["WordFilter", "f"]
 * [[["apple"]], ["a", "e"]]
 * Output
 * [null, 0]
 *
 * Explanation
 * WordFilter wordFilter = new WordFilter(["apple"]);
 * wordFilter.f("a", "e"); // return 0, because the word at index 0 has
 * prefix = "a" and suffix = 'e".
 *
 * Constraints:
 *     ∙ 1 <= words.length <= 15000
 *     ∙ 1 <= words[i].length <= 10
 *     ∙ 1 <= prefix.length, suffix.length <= 10
 *     ∙ words[i], prefix and suffix consist of lower-case English letters only
 *     ∙ At most 15000 calls will be made to the function f
 */
using namespace std;
#include <iostream>
#include <vector>

class TrieNode{
public:
    vector<TrieNode*> children;
    int index;
    TrieNode() {
        children = vector<TrieNode*> (27, NULL);
        index = 0;
    }
};

class WordFilter {
private:
    TrieNode *prefixes;

public:
    WordFilter(vector<string> &words) {
        prefixes = new TrieNode();
        for (int i = 0; i < words.size(); ++i) {
            for (int j = 0; j < words[i].length(); ++j) {
                TrieNode *p = prefixes;
                for (char c : words[i].substr(j) + '`' + words[i]) {
                    if (!p->children[c-'`'])
                        p->children[c-'`'] = new TrieNode();
                    p = p->children[c-'`'];
                    p->index = i;
                }
            }
        }
    }

    int f(string prefix, string suffix) {
        TrieNode *p = prefixes;
        for (char c : suffix + '`' + prefix) {
            if (p->children[c-'`'] == NULL) {
                return -1;
            }
            p = p->children[c-'`'];
        }
        return p->index;
    }
};

int main() {
    vector<string> words = {"apple", "banana"};
    WordFilter* obj = new WordFilter(words);
    cout << (0 == obj->f("a","e")) << endl;
    cout << (-1 == obj->f("b","e")) << endl;
    cout << (1 == obj->f("b","a")) << endl;
}
