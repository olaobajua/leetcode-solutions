/**
 * 96. Unique Binary Search Trees (Medium)
 * Given an integer n, return the number of structurally unique BST's (binary
 * search trees) which has exactly n nodes of unique values from 1 to n.
 *
 * Example 1:
 * Input: n = 3
 * Output: 5
 *
 * Example 2:
 * Input: n = 1
 * Output: 1
 *
 * Constraints:
 *     1 <= n <= 19
 */

/**
 * @param {number} n
 * @return {number}
 */
var numTrees = function(n) {
    // f[5] = f[0]*f[4] + f[1]*f[3] + f[2]*f[2] + f[3]*f[1] + f[4]*f[0]
    // f[n] = f[0]*f[n-1] + f[1]*f[n-2] + ... + f[n-2]*f[1] + f[n-1]*f[0]
    let uniqueBsts = [1, 1];
    for (let i = 2; i <= n; ++i) {
        uniqueBsts.push(0);
        for (let j = 1; j <= i; ++j) {
            uniqueBsts[i] += uniqueBsts[j-1] * uniqueBsts[i-j];
        }
    }
    return uniqueBsts[n];
};

const tests = [
    [1, 1],
    [2, 2],
    [3, 5],
    [4, 14],
    [5, 42],
    [6, 132],
    [7, 429],
    [8, 1430],
    [9, 4862],
    [10, 16796],
    [11, 58786],
    [12, 208012],
    [13, 742900],
    [14, 2674440],
    [15, 9694845],
    [16, 35357670],
    [17, 129644790],
    [18, 477638700],
    [19, 1767263190],
];
for (let [n, expected] of tests) {
    console.log(numTrees(n) == expected);
}
