/**
 * 118. Pascal's Triangle [Easy]
 * Given an integer numRows, return the first numRows of Pascal's triangle.
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly
 * above it as shown:
 * Example 1:
 * Input: numRows = 5
 * Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
 * Example 2:
 * Input: numRows = 1
 * Output: [[1]]
 *
 * Constraints:
 *     ∙ 1 <= numRows <= 30
 */
class Solution {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ret = new ArrayList();
        List<Integer> level = new ArrayList();
        level.add(1);
        while (numRows-- > 0) {
            ret.add(level);
            List<Integer> next_level = new ArrayList();
            next_level.add(1);
            for (int i = 0; i < level.size() - 1; ++i)
                next_level.add(level.get(i) + level.get(i+1));
            next_level.add(1);
            level = next_level;
        }
        return ret;
    }
}
