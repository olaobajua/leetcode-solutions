"""
 * 1981. Minimize the Difference Between Target and Chosen Elements (Medium)
 * You are given an m x n integer matrix mat and an integer target.
 *
 * Choose one integer from each row in the matrix such that the absolute
 * difference between target and the sum of the chosen elements is minimized.
 *
 * Return the minimum absolute difference.
 *
 * The absolute difference between two numbers a and b is the absolute
 * value of a - b.
 *
 * Example 1:
 * Input: mat = [[1,2,3],[4,5,6],[7,8,9]], target = 13
 * Output: 0
 * Explanation: One possible choice is to:
 * - Choose 1 from the first row.
 * - Choose 5 from the second row.
 * - Choose 7 from the third row.
 * The sum of the chosen elements is 13, which equals the target, so the
 * absolute difference is 0.
 *
 * Example 2:
 * Input: mat = [[1],[2],[3]], target = 100
 * Output: 94
 * Explanation: The best possible choice is to:
 * - Choose 1 from the first row.
 * - Choose 2 from the second row.
 * - Choose 3 from the third row.
 * The sum of the chosen elements is 6, and the absolute difference is 94.
 *
 * Example 3:
 * Input: mat = [[1,2,9,8,7]], target = 6
 * Output: 1
 * Explanation: The best choice is to choose 7 from the first row.
 * The absolute difference is 1.
 *
 * Constraints:
 *     m == mat.length
 *     n == mat[i].length
 *     1 <= m, n <= 70
 *     1 <= mat[i][j] <= 70
 *     1 <= target <= 800
"""
from functools import cache
from math import inf
from typing import List

class Solution:
    def minimizeTheDifference(self, mat: List[List[int]], target: int) -> int:
        row_count = len(mat)
        mat = tuple(sorted(set(row)) for row in mat)

        @cache
        def min_difference(row, sum_acc):
            if row < row_count:
                min_diff = inf
                for col in range(len(mat[row])):
                    cur_diff = min_difference(row + 1, sum_acc + mat[row][col])
                    if cur_diff == 0:
                        return cur_diff
                    if cur_diff > min_diff:
                        return min_diff
                    min_diff = min(min_diff, cur_diff)
                return min_diff
            else:
                return abs(target - sum_acc)

        return min_difference(0, 0)

if __name__ == "__main__":
    tests = (
        ([[1,2,9,8,7]], 6, 1),
        ([[1],[2],[3]], 100, 94),
        ([[1,2,3],[4,5,6],[7,8,9]], 13, 0),
    )
    for mat, target, expected in tests:
        print(Solution().minimizeTheDifference(mat, target) == expected, flush=True)
