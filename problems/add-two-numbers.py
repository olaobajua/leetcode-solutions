"""
 * 2. Add Two Numbers (Medium)
 * You are given two non-empty linked lists representing two non-negative
 * integers. The digits are stored in reverse order, and each of their nodes
 * contains a single digit. Add the two numbers and return the sum as a linked
 * list.
 *
 * You may assume the two numbers do not contain any leading zero, except the
 * number 0 itself.
 *
 * Example 1:
 * Input: l1 = [2,4,3], l2 = [5,6,4]
 * Output: [7,0,8]
 * Explanation: 342 + 465 = 807.
 *
 * Example 2:
 * Input: l1 = [0], l2 = [0]
 * Output: [0]
 *
 * Example 3:
 * Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * Output: [8,9,9,9,0,0,0,1]
 *
 * Constraints:
 *     ∙ The number of nodes in each linked list is in the range [1, 100].
 *     ∙ 0 <= Node.val <= 9
 *     ∙ It is guaranteed that the list represents a number that does not have
 *       leading zeros.
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        def to_int(node):
            return node.val + 10 * to_int(node.next) if node else 0

        def to_list(n):
            node = ListNode(n % 10)
            if n > 9:
                node.next = to_list(n // 10)
            return node

        return to_list(to_int(l1) + to_int(l2))

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([2,4,3], [5,6,4], [7,0,8]),
        ([0], [0], [0]),
        ([9,9,9,9,9,9,9], [9,9,9,9], [8,9,9,9,0,0,0,1]),
        ([2,4,9], [5,6,4,9], [7,0,4,0,1]),
        ([5], [5], [0, 1]),
    )
    for nums1, nums2, expected in tests:
        l1 = list_to_linked_list(nums1)
        l2 = list_to_linked_list(nums2)
        print(linked_list_to_list(Solution().addTwoNumbers(l1, l2)) == expected)
