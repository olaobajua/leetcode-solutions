"""
 * 149. Max Points on a Line [Hard]
 * Given an array of points where points[i] = [xᵢ, yᵢ] represents a point on
 * the X-Y plane, return the maximum number of points that lie on the same
 * straight line.
 *
 * Example 1:
 * Input: points = [[1,1],[2,2],[3,3]]
 * Output: 3
 *
 * Example 2:
 * Input: points = [[1,1],[3,2],[5,3],[4,1],[2,3],[1,4]]
 * Output: 4
 *
 * Constraints:
 *     ∙ 1 <= points.length <= 300
 *     ∙ points[i].length == 2
 *     ∙ -10⁴ <= xᵢ, yᵢ <= 10⁴
 *     ∙ All the points are unique.
"""
from collections import defaultdict
from itertools import combinations
from typing import List

class Solution:
    def maxPoints(self, points: List[List[int]]) -> int:
        if len(points) == 1:
            return 1
        groups = defaultdict(set)
        for (x1, y1), (x2, y2) in combinations(sorted(points), 2):
            try:
                slope = (y2 - y1) / (x2 - x1)
                intercept = y1 - slope * x1
            except ZeroDivisionError:
                slope = "inf"
                intercept = x1
            groups[(slope, intercept)].add((x1, y1))
            groups[(slope, intercept)].add((x2, y2))
        return len(max(groups.values(), key=len))

if __name__ == "__main__":
    tests = (
        ([[1,1],[2,2],[3,3]], 3),
        ([[1,1],[3,2],[5,3],[4,1],[2,3],[1,4]], 4),
        ([[0,0]], 1),
    )
    for points, expected in tests:
        print(Solution().maxPoints(points) == expected)
