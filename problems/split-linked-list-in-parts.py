"""
 * 725. Split Linked List in Parts [Medium]
 * Given the head of a singly linked list and an integer k, split the linked
 * list into k consecutive linked list parts.
 *
 * The length of each part should be as equal as possible: no two parts should
 * have a size differing by more than one. This may lead to some parts being
 * null.
 *
 * The parts should be in the order of occurrence in the input list, and parts
 * occurring earlier should always have a size greater than or equal to parts
 * occurring later.
 *
 * Return an array of the k parts.
 *
 * Example 1:
 * Input: head = [1,2,3], k = 5
 * Output: [[1],[2],[3],[],[]]
 * Explanation:
 * The first element output[0] has output[0].val = 1, output[0].next = null.
 * The last element output[4] is null, but its string representation as a
 * ListNode is [].
 *
 * Example 2:
 * Input: head = [1,2,3,4,5,6,7,8,9,10], k = 3
 * Output: [[1,2,3,4],[5,6,7],[8,9,10]]
 * Explanation:
 * The input has been split into consecutive parts with size difference at
 * most 1, and earlier parts are a larger size than the later parts.
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 1000].
 *     ∙ 0 <= Node.val <= 1000
 *     ∙ 1 <= k <= 50
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def splitListToParts(self, head: Optional[ListNode], k: int) -> List[Optional[ListNode]]:
        cur = head
        n = 0
        while cur:
            n += 1
            cur = cur.next
        s = n // k
        rem = n % k

        ret = []
        cur_part = cur = head
        cur_size = 1
        while cur:
            if cur_size >= (s + 1 if rem else s):
                ret.append(cur_part)
                cur_part = cur.next
                cur.next = None
                cur = cur_part
                cur_size = 1
                if rem:
                    rem -= 1
            else:
                cur = cur.next
                cur_size += 1

        while len(ret) < k:
            ret.append(cur_part)
            cur_part = None
        return ret

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head: Optional[ListNode]) -> List[int]:
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3], 5, [[1],[2],[3],[],[]]),
        ([1,2,3,4,5,6,7,8,9,10], 3, [[1,2,3,4],[5,6,7],[8,9,10]]),
    )
    for nums, k, expected in tests:
        root = list_to_linked_list(nums)
        for a, b in zip(Solution().splitListToParts(root, k), expected):
            if linked_list_to_list(a) != b:
                print(False)
                break
        else:
            print(True)
