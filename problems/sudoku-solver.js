/**
 * 37. Sudoku Solver (Hard)
 * Write a program to solve a Sudoku puzzle by filling the empty cells.
 * A sudoku solution must satisfy all of the following rules:
 *     ∙ Each of the digits 1-9 must occur exactly once in each row.
 *     ∙ Each of the digits 1-9 must occur exactly once in each column.
 *     ∙ Each of the digits 1-9 must occur exactly once in each of the 9 3x3
 *       sub-boxes of the grid.
 *
 * The '.' character indicates empty cells.
 *
 * Example 1:
 * Input: board = [["5","3",".",".","7",".",".",".","."],
                   ["6",".",".","1","9","5",".",".","."],
                   [".","9","8",".",".",".",".","6","."],
                   ["8",".",".",".","6",".",".",".","3"],
                   ["4",".",".","8",".","3",".",".","1"],
                   ["7",".",".",".","2",".",".",".","6"],
                   [".","6",".",".",".",".","2","8","."],
                   [".",".",".","4","1","9",".",".","5"],
                   [".",".",".",".","8",".",".","7","9"]]
 * Output: [["5","3","4","6","7","8","9","1","2"],
            ["6","7","2","1","9","5","3","4","8"],
            ["1","9","8","3","4","2","5","6","7"],
            ["8","5","9","7","6","1","4","2","3"],
            ["4","2","6","8","5","3","7","9","1"],
            ["7","1","3","9","2","4","8","5","6"],
            ["9","6","1","5","3","7","2","8","4"],
            ["2","8","7","4","1","9","6","3","5"],
            ["3","4","5","2","8","6","1","7","9"]]
 * Explanation: The input board is shown above and the only valid solution is
 * shown below.
 *
 * Constraints:
 *     ∙ board.length == 9
 *     ∙ board[i].length == 9
 *     ∙ board[i][j] is a digit or '.'.
 *     ∙ It is guaranteed that the input board has only one solution.
 */

/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var solveSudoku = function(board) {
    global.boardSize = board.length;
    global.squareSize = boardSize**0.5;
    const solutions = solve(getOpportunities(board));
    for (let [row, col, value] of solutions.next().value) {
        board[row][col] = value;
    }
};

function* solve(opportunities, selected=[]) {
    opportunities.sort((a, b) => a[2].size - b[2].size);
    let curr, curc, curValues;
    [curr, curc, curValues] = opportunities[0];
    if (opportunities.length == 1) {
        yield selected.concat([[curr, curc, curValues.values().next().value]]);
    } else {
        for (let cv of curValues) {
            let newOpps = [];
            for (let [r, c, vals] of opportunities.slice(1)) {
                let no = isLinked(r, c, curr, curc) ? vals.sub(new Set(cv))
                                                    : vals;
                newOpps.push([r, c, no]);
            }
            cellsWithoutOpportunities = newOpps.filter(o => o[2].size == 0);
            if (cellsWithoutOpportunities.length == 0) {
                yield* solve(newOpps, selected.concat([[curr, curc, cv]]));
            }
        }
    }
}

function isLinked(row1, col1, row2, col2) {
    const squareSize = global.squareSize;
    return row1 == row2 ||
           col1 == col2 ||
           Math.floor(row1 / squareSize) == Math.floor(row2 / squareSize) &&
           Math.floor(col1 / squareSize) == Math.floor(col2 / squareSize);
}

function getOpportunities(board) {
    const boardSize = global.boardSize;
    const squareSize = global.squareSize;
    const rows = board.map(row => new Set(row));
    const cols = zip(...board).map(col => new Set(col));
    let sqrs = [];
    for (let row = 0; row < boardSize; row += squareSize) {
        for (let col = 0; col < boardSize; col += squareSize) {
            sqrs.push(new Set([...zip(...board.slice(row, row + squareSize))]
                              .slice(col, col + squareSize)
                              .flat()));
        }
    }
    const allVariants = new Set([...Array(boardSize)]
                                .map((_, i) => (i + 1).toString()));
    let opportunities = [];
    for (let row = 0; row < boardSize; ++row) {
        for (let col = 0; col < boardSize; ++col) {
            if (board[row][col] == ".") {
                let square = sqrs[squareSize * Math.floor(row / squareSize) +
                                  Math.floor(col / squareSize)];
                opportunities.push([row, col, allVariants.sub(rows[row])
                                                         .sub(cols[col])
                                                         .sub(square)]);
            }
        }
    }
    return opportunities;
}

function zip(...arrays) {
    let minLength = Math.min(...arrays.map(arr => arr.length));
    return [...Array(minLength)].map((_, i) => arrays.map(row => row[i]));
}

Set.prototype.sub = function(set) {
    return new Set([...this.keys()].filter(x => !set.has(x)));
}

const tests = [
    [[["5","3",".",".","7",".",".",".","."],
      ["6",".",".","1","9","5",".",".","."],
      [".","9","8",".",".",".",".","6","."],
      ["8",".",".",".","6",".",".",".","3"],
      ["4",".",".","8",".","3",".",".","1"],
      ["7",".",".",".","2",".",".",".","6"],
      [".","6",".",".",".",".","2","8","."],
      [".",".",".","4","1","9",".",".","5"],
      [".",".",".",".","8",".",".","7","9"]],
     [["5","3","4","6","7","8","9","1","2"],
      ["6","7","2","1","9","5","3","4","8"],
      ["1","9","8","3","4","2","5","6","7"],
      ["8","5","9","7","6","1","4","2","3"],
      ["4","2","6","8","5","3","7","9","1"],
      ["7","1","3","9","2","4","8","5","6"],
      ["9","6","1","5","3","7","2","8","4"],
      ["2","8","7","4","1","9","6","3","5"],
      ["3","4","5","2","8","6","1","7","9"]]],

    [[[".",".","9","7","4","8",".",".","."],
      ["7",".",".",".",".",".",".",".","."],
      [".","2",".","1",".","9",".",".","."],
      [".",".","7",".",".",".","2","4","."],
      [".","6","4",".","1",".","5","9","."],
      [".","9","8",".",".",".","3",".","."],
      [".",".",".","8",".","3",".","2","."],
      [".",".",".",".",".",".",".",".","6"],
      [".",".",".","2","7","5","9",".","."]],
     [['5','1','9','7','4','8','6','3','2'],
      ['7','8','3','6','5','2','4','1','9'],
      ['4','2','6','1','3','9','8','7','5'],
      ['3','5','7','9','8','6','2','4','1'],
      ['2','6','4','3','1','7','5','9','8'],
      ['1','9','8','5','2','4','3','6','7'],
      ['9','7','5','8','6','3','1','2','4'],
      ['8','3','2','4','9','1','7','5','6'],
      ['6','4','1','2','7','5','9','8','3']]],
];

for (let [board, expected] of tests) {
    solveSudoku(board)
    console.log(board.every((row, r) => row.every((cell, c) => cell == expected[r][c])));
}
