"""
 * 1319. Number of Operations to Make Network Connected [Medium]
 * There are n computers numbered from 0 to n - 1 connected by ethernet cables
 * connections forming a network where connections[i] = [aᵢ, bᵢ] represents a
 * connection between computers aᵢ and bᵢ. Any computer can reach any other
 * computer directly or indirectly through the network.
 *
 * You are given an initial computer network connections. You can extract
 * certain cables between two directly connected computers, and place them
 * between any pair of disconnected computers to make them directly connected.
 *
 * Return the minimum number of times you need to do this in order to make all
 * the computers connected. If it is not possible, return -1.
 *
 * Example 1:
 * Input: n = 4, connections = [[0,1],[0,2],[1,2]]
 * Output: 1
 * Explanation: Remove cable between computer 1 and 2 and place between
 * computers 1 and 3.
 *
 * Example 2:
 * Input: n = 6, connections = [[0,1],[0,2],[0,3],[1,2],[1,3]]
 * Output: 2
 *
 * Example 3:
 * Input: n = 6, connections = [[0,1],[0,2],[0,3],[1,2]]
 * Output: -1
 * Explanation: There are not enough cables.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁵
 *     ∙ 1 <= connections.length <= min(n * (n - 1) / 2, 10⁵)
 *     ∙ connections[i].length == 2
 *     ∙ 0 <= aᵢ, bᵢ < n
 *     ∙ aᵢ != bᵢ
 *     ∙ There are no repeated connections.
 *     ∙ No two computers are connected by more than one cable.
"""
from typing import List

class Solution:
    def makeConnected(self, n: int, connections: List[List[int]]) -> int:
        if len(connections) < n - 1:
            return -1
        groups = DisjointSet(n)
        for a, b in connections:
            groups.union(a, b)
        subnets = set()
        for i in range(n):
            subnets.add(groups.find(i))
        return len(subnets) - 1

class DisjointSet:
    def __init__(self, n):
        self.parent = list(range(n))

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        if (px := self.find(x)) != (py := self.find(y)):
            self.parent[px] = py

if __name__ == "__main__":
    tests = (
        (4, [[0,1],[0,2],[1,2]], 1),
        (6, [[0,1],[0,2],[0,3],[1,2],[1,3]], 2),
        (6, [[0,1],[0,2],[0,3],[1,2]], -1),
    )
    for n, connections, expected in tests:
        print(Solution().makeConnected(n, connections) == expected)
