"""
 * 328. Odd Even Linked List [Medium]
 * Given the head of a singly linked list, group all the nodes with odd
 * indices together followed by the nodes with even indices, and return the
 * reordered list.
 *
 * The first node is considered odd, and the second node is even, and so on.
 *
 * Note that the relative order inside both the even and odd groups should
 * remain as it was in the input.
 *
 * You must solve the problem in O(1) extra space complexity and O(n) time
 * complexity.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5]
 * Output: [1,3,5,2,4]
 *
 * Example 2:
 * Input: head = [2,1,3,5,6,4,7]
 * Output: [2,3,6,7,1,5,4]
 *
 * Constraints:
 *     ∙ The number of nodes in the linked list is in the range [0, 10⁴].
 *     ∙ -10⁶ <= Node.val <= 10⁶
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def oddEvenList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if not head or not head.next:
            return head
        odd = o = head
        even = e = head.next
        head = head.next.next
        while head:
            o.next, head, o = head, head.next, head
            if head:
                e.next, head, e = head, head.next, head
        o.next = even
        e.next = None
        return odd

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head: Optional[ListNode]) -> List[int]:
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5], [1,3,5,2,4]),
        ([2,1,3,5,6,4,7], [2,3,6,7,1,5,4]),
    )
    for nums, expected in tests:
        root = list_to_linked_list(nums)
        print(linked_list_to_list(Solution().oddEvenList(root)) == expected)
