/**
 * 1081. Smallest Subsequence of Distinct Characters (Medium)
 * Given a string s, return the lexicographically smallest subsequence of s
 * that contains all the distinct characters of s exactly once.
 *
 * Example 1:
 * Input: s = "bcabc"
 * Output: "abc"
 *
 * Example 2:
 * Input: s = "cbacdcbc"
 * Output: "acdb"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 1000
 *     ∙ s consists of lowercase English letters.
 *
 * Note: This question is the same as 316:
 * https://leetcode.com/problems/remove-duplicate-letters/
 */

/**
 * @param {string} s
 * @return {string}
 */
var smallestSubsequence = function(s) {
    const lastOcc = {};
    s = Array.from(s);
    s.forEach((char, i) => {
        lastOcc[char] = i;
    });
    const stack = ['!'];
    const visited = new Set();
    for (const [i, l] of s.entries()) {
        if (visited.has(l)) {
            continue;
        }
        while (l < stack[stack.length-1] && lastOcc[stack[stack.length-1]] > i) {
            visited.delete(stack.pop());
        }
        stack.push(l);
        visited.add(l);
    }
    return stack.slice(1).join('');
};

const tests = [
    ["bcabc", "abc"],
    ["cbacdcbc", "acdb"],
    ["leetcode", "letcod"],
];

for (const [s, expected] of tests) {
    console.log(smallestSubsequence(s) == expected);
}
