/**
 * 916. Word Subsets [Medium]
 * You are given two string arrays words1 and words2.
 *
 * A string b is a subset of string a if every letter in b occurs in a
 * including multiplicity.
 *     ∙ For example, "wrr" is a subset of "warrior" but is not a subset of
 *       "world".
 *
 * A string a from words1 is universal if for every string b in words2, b is a
 * subset of a.
 *
 * Return an array of all the universal strings in words1. You may return the
 * answer in any order.
 *
 * Example 1:
 * Input: words1 = ["amazon","apple","facebook","google","leetcode"], words2 =
 * ["e","o"]
 * Output: ["facebook","google","leetcode"]
 *
 * Example 2:
 * Input: words1 = ["amazon","apple","facebook","google","leetcode"], words2 =
 * ["l","e"]
 * Output: ["apple","google","leetcode"]
 *
 * Constraints:
 *     ∙ 1 <= words1.length, words2.length <= 10⁴
 *     ∙ 1 <= words1[i].length, words2[i].length <= 10
 *     ∙ words1[i] and words2[i] consist only of lowercase English letters.
 *     ∙ All the strings of words1 are unique.
 */
class Solution {
    public List<String> wordSubsets(String[] words1, String[] words2) {
        final int a = (int)'a';
        int[] counter = new int[26];
        for (String word : words2) {
            int[] wc = new int[26];
            for (char c : word.toCharArray())
                ++wc[(int)c-a];
            for (int i = 0; i < 26; ++i)
                counter[i] = Math.max(counter[i], wc[i]);
        }
        List<String> ret = new ArrayList<String>();
        for (String word : words1) {
            nextWord: {
                int[] c1 = new int[26];
                for (char c : word.toCharArray())
                    ++c1[(int)c-a];
                for (int i = 0; i < 26; ++i)
                    if (c1[i] < counter[i])
                        break nextWord;
                ret.add(word);
            }
        };
        return ret;
    }
}
