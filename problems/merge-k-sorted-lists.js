/**
 * 23. Merge k Sorted Lists (Hard)
 * You are given an array of k linked-lists lists, each linked-list is sorted
 * in ascending order.
 *
 * Merge all the linked-lists into one sorted linked-list and return it.
 *
 * Example 1:
 * Input: lists = [[1,4,5],[1,3,4],[2,6]]
 * Output: [1,1,2,3,4,4,5,6]
 * Explanation: The linked-lists are:
 * [
 *   1->4->5,
 *   1->3->4,
 *   2->6
 * ]
 * merging them into one sorted list:
 * 1->1->2->3->4->4->5->6
 *
 * Example 2:
 * Input: lists = []
 * Output: []
 *
 * Example 3:
 * Input: lists = [[]]
 * Output: []
 *
 * Constraints:
 *     ∙ k == lists.length
 *     ∙ 0 <= k <= 10⁴
 *     ∙ 0 <= lists[i].length <= 500
 *     ∙ -10⁴ <= lists[i][j] <= 10⁴
 *     ∙ lists[i] is sorted in ascending order.
 *     ∙ The sum of lists[i].length won't exceed 10⁴.
 */

// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 */
var mergeKLists = function(lists) {
    let n = lists.length - 1;
    if (n < 0) {
        return null;
    }
    if (n == 0) {
        return lists[0];
    }
    for (let i = 0; i < n; ++i, --n) {
        lists[i] = mergeSortedLists(lists[i], lists[n]);
    }
    return mergeKLists(lists.slice(0, n + 1));
};

function mergeSortedLists(a, b) {
    if (!a) {
        return b;
    }
    if (!b) {
        return a;
    }
    if (a.val <= b.val) {
        a.next = mergeSortedLists(a.next, b);
        return a;
    } else {
        b.next = mergeSortedLists(a, b.next);
        return b;
    }
}

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    [[[1,4,5],[1,3,4],[2,6]], [1,1,2,3,4,4,5,6]],
    [[[1,4,5]], [1,4,5]],
    [[[1,2,3],[1,2]], [1,1,2,2,3]],
    [[], []],
    [[[]], []],
    [[[],[]], []],
];

for (let [lists, expected] of tests) {
    lists = lists.map(l => arrayToLinkedList(l));
    console.log(linkedListToArray(mergeKLists(lists)).toString() ===
                expected.toString());
}
