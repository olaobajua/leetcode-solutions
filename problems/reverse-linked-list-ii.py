"""
 * 92. Reverse Linked List II [Medium]
 * Given the head of a singly linked list and two integers left and right where
 * left <= right, reverse the nodes of the list from position left to position
 * right, and return the reversed list.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5], left = 2, right = 4
 * Output: [1,4,3,2,5]
 *
 * Example 2:
 * Input: head = [5], left = 1, right = 1
 * Output: [5]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is n.
 *     ∙ 1 <= n <= 500
 *     ∙ -500 <= Node.val <= 500
 *     ∙ 1 <= left <= right <= n
 *
 * Follow up: Could you do it in one pass?
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def reverseBetween(self, head: Optional[ListNode], left: int, right: int) -> Optional[ListNode]:
        if left == right:
            return head
        prev = dummy = ListNode(0, head)
        for _ in range(left - 1):
            prev = prev.next
        cur = prev.next
        nxt = cur.next
        for _ in range(right - left):
            nxt.next, cur, nxt = cur, nxt, nxt.next
        prev.next.next = nxt
        prev.next = cur
        return dummy.next

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head: Optional[ListNode]) -> List[int]:
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5], 2, 4, [1,4,3,2,5]),
        ([5], 1, 1, [5]),
        ([3,5], 1, 2, [5,3]),
        ([3,5], 1, 1, [3, 5]),
    )
    for nums, left, right, expected in tests:
        root = list_to_linked_list(nums)
        ret = linked_list_to_list(Solution().reverseBetween(root, left, right))
        print(ret == expected)
