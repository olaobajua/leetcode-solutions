"""
 * 1269. Number of Ways to Stay in the Same Place After Some Steps [Hard]
 * You have a pointer at index 0 in an array of size arrLen. At each step, you
 * can move 1 position to the left, 1 position to the right in the array, or
 * stay in the same place (The pointer should not be placed outside the array
 * at any time).
 *
 * Given two integers steps and arrLen, return the number of ways such that
 * your pointer is still at index 0 after exactly steps steps. Since the answer
 * may be too large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: steps = 3, arrLen = 2
 * Output: 4
 * Explanation: There are 4 differents ways to stay at index 0 after 3 steps.
 * Right, Left, Stay
 * Stay, Right, Left
 * Right, Stay, Left
 * Stay, Stay, Stay
 *
 * Example 2:
 * Input: steps = 2, arrLen = 4
 * Output: 2
 * Explanation: There are 2 differents ways to stay at index 0 after 2 steps
 * Right, Left
 * Stay, Stay
 *
 * Example 3:
 * Input: steps = 4, arrLen = 2
 * Output: 8
 *
 * Constraints:
 *     ∙ 1 <= steps <= 500
 *     ∙ 1 <= arrLen <= 10⁶
"""
from functools import cache

class Solution:
    def numWays(self, steps: int, arrLen: int) -> int:
        @cache
        def dfs(step, pos):
            if step == 0:
                return int(pos == 0)
            if pos < 0 or pos > max_pos:
                return 0

            ret = dfs(step - 1, pos)  # stay
            if pos > 0:
                ret = (ret + dfs(step - 1, pos - 1)) % MOD  # left
            if pos < max_pos:
                ret = (ret + dfs(step - 1, pos + 1)) % MOD  # right

            return ret

        MOD = 1000000007
        max_pos = min(steps // 2, arrLen - 1)

        return dfs(steps, 0)

if __name__ == "__main__":
    tests = (
        (3, 2, 4),
        (2, 4, 2),
        (4, 2, 8),
    )
    for steps, arrLen, expected in tests:
        print(Solution().numWays(steps, arrLen) == expected)
