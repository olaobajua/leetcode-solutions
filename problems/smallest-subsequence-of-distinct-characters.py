"""
 * 1081. Smallest Subsequence of Distinct Characters (Medium)
 * Given a string s, return the lexicographically smallest subsequence of s
 * that contains all the distinct characters of s exactly once.
 *
 * Example 1:
 * Input: s = "bcabc"
 * Output: "abc"
 *
 * Example 2:
 * Input: s = "cbacdcbc"
 * Output: "acdb"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 1000
 *     ∙ s consists of lowercase English letters.
 *
 * Note: This question is the same as 316:
 * https://leetcode.com/problems/remove-duplicate-letters/
"""
class Solution:
    def smallestSubsequence(self, s: str) -> str:
        for c in sorted(set(s)):
            suffix = s[s.index(c):]
            if set(suffix) == set(s):
                return c + self.smallestSubsequence(suffix.replace(c, ''))
        return ''

if __name__ == "__main__":
    tests = (
        ("bcabc", "abc"),
        ("cbacdcbc", "acdb"),
        ("leetcode", "letcod"),
    )
    for s, expected in tests:
        print(Solution().smallestSubsequence(s) == expected)
