/**
 * 662. Maximum Width of Binary Tree (Medium)
 * Given the root of a binary tree, return the maximum width of the given tree.
 *
 * The maximum width of a tree is the maximum width among all levels.
 *
 * The width of one level is defined as the length between the end-nodes
 * (the leftmost and rightmost non-null nodes), where the null nodes between
 * the end-nodes are also counted into the length calculation.
 *
 * It is guaranteed that the answer will in the range of 32-bit signed integer.
 *
 * Example 1:
 *    1
 *  3   2
 * 5 3   9
 * Input: root = [1,3,2,5,3,null,9]
 * Output: 4
 * Explanation: The maximum width existing in the third level with the length 4
 * (5,3,null,9).
 *
 * Example 2:
 *    1
 *  3
 * 5 3
 * Input: root = [1,3,null,5,3]
 * Output: 2
 * Explanation: The maximum width existing in the third level with the length 2
 * (5,3).
 *
 * Example 3:
 *    1
 *  3   2
 * 5
 * Input: root = [1,3,2,5]
 * Output: 2
 * Explanation: The maximum width existing in the second level with the length
 * 2 (3,2).
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 3000].
 *     ∙ -100 <= Node.val <= 100
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @return {number}
 */
var widthOfBinaryTree = function(root) {
    function addPlaceholder(l, i) {
        if (l.length && typeof(l[l.length-1]) == 'number') {
            l[l.length-1] += i;
        } else {
            l.push(i);
        }
    }
    let level = [root];
    let maxWidth = 0;
    while (level.length) {
        maxWidth = Math.max(
            maxWidth,
            level.reduce((a, x) => a + (typeof(x) === 'number' ? x : 1), 0)
        );
        const nextLevel = [];
        for (const node of level) {
            if (typeof(node) === 'object') {
                if (node.left) {
                    nextLevel.push(node.left);
                } else {
                    addPlaceholder(nextLevel, 1);
                }
                if (node.right) {
                    nextLevel.push(node.right);
                } else {
                    addPlaceholder(nextLevel, 1);
                }
            } else {
                addPlaceholder(nextLevel, 2*node);
            }
        }
        level = nextLevel;
        if (level.length && typeof(level[level.length-1]) === 'number') {
            level.pop();
        }
        if (level && typeof(level[0]) === 'number') {
            level.shift();
        }
    }
    return maxWidth;
};

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

const tests = [
    [[1,3,2,5,3,null,9], 4],
    [[1,3,null,5,3], 2],
    [[1,3,2,5], 2],
    [[1,3,2,5,null,null,9,6,null,null,7], 8],
    [[1,1,1,1,1,1,1,null,null,null,1,null,null,null,null,2,2,2,2,2,2,2,null,2,null,null,2,null,2], 8],
    [[1,1,1,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,1,null,null,1,1,null,1,null,1,null,1,null,1,null], 2147483645],
];

for (const [nums, expected] of tests) {
    console.log(widthOfBinaryTree(buildTree(nums)) == expected);
}
