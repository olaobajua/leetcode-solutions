"""
 * 1512. Number of Good Pairs [Easy]
 * Given an array of integers nums, return the number of good pairs.
 *
 * A pair (i, j) is called good if nums[i] == nums[j] and i < j.
 *
 * Example 1:
 * Input: nums = [1,2,3,1,1,3]
 * Output: 4
 * Explanation: There are 4 good pairs (0,3), (0,4), (3,4), (2,5) 0-indexed.
 *
 * Example 2:
 * Input: nums = [1,1,1,1]
 * Output: 6
 * Explanation: Each pair in the array are good.
 *
 * Example 3:
 * Input: nums = [1,2,3]
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 100
 *     ∙ 1 <= nums[i] <= 100
"""
from collections import Counter
from math import comb
from typing import List

class Solution:
    def numIdenticalPairs(self, nums: List[int]) -> int:
        return sum(comb(c, 2) for c in Counter(nums).values())

if __name__ == "__main__":
    tests = (
        ([1,2,3,1,1,3], 4),
        ([1,1,1,1], 6),
        ([1,2,3], 0),
    )
    for nums, expected in tests:
        print(Solution().numIdenticalPairs(nums) == expected)
