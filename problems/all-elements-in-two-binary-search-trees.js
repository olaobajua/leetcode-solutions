/**
 * 1305. All Elements in Two Binary Search Trees (Medium)
 * Given two binary search trees root1 and root2, return a list containing all
 * the integers from both trees sorted in ascending order.
 *
 * Example 1:
 * Input: root1 = [2,1,4], root2 = [1,0,3]
 * Output: [0,1,1,2,3,4]
 *
 * Example 2:
 * Input: root1 = [1,null,8], root2 = [8,1]
 * Output: [1,1,8,8]
 *
 * Constraints:
 *     ∙ The number of nodes in each tree is in the range [0, 5000].
 *     ∙ -10⁵ <= Node.val <= 10⁵
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root1
 * @param {TreeNode} root2
 * @return {number[]}
 */
var getAllElements = function(root1, root2) {
    return [...treeToList(root1), ...treeToList(root2)].sort((a, b) => a - b);
};

function* treeToList(root) {
    let nodes = [root];
    for (let n of nodes) {
        if (n) {
            yield n.val;
            nodes.push(n.left, n.right);
        }
    }
}

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

const tests = [
    [[2,1,4], [1,0,3], [0,1,1,2,3,4]],
    [[1,null,8], [8,1], [1,1,8,8]],
    [[0,-10,10], [5,1,7,0,2], [-10,0,0,1,2,5,7,10]]
];

for (const [nums1, nums2, expected] of tests) {
    const root1 = buildTree(nums1)
    const root2 = buildTree(nums2)
    console.log(getAllElements(root1, root2).toString() == expected.toString());
}
