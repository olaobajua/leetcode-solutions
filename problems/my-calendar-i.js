/**
 * 729. My Calendar I [Medium]
 * You are implementing a program to use as your calendar. We can add a new
 * event if adding the event will not cause a double booking.
 *
 * A double booking happens when two events have some non-empty intersection
 * (i.e., some moment is common to both events.).
 *
 * The event can be represented as a pair of integers start and end that
 * represents a booking on the half-open interval [start, end), the range of
 * real numbers x such that start <= x < end.
 *
 * Implement the MyCalendar class:
 *     ∙ MyCalendar() Initializes the calendar object.
 *     ∙ boolean book(int start, int end) Returns true if the event can be
 *       added to the calendar successfully without causing a double booking.
 *       Otherwise, return false and do not add the event to the calendar.
 *
 * Example 1:
 * Input
 * ["MyCalendar", "book", "book", "book"]
 * [[], [10, 20], [15, 25], [20, 30]]
 * Output
 * [null, true, false, true]
 *
 * Explanation
 * MyCalendar myCalendar = new MyCalendar();
 * myCalendar.book(10, 20); // return True
 * myCalendar.book(15, 25); // return False, It can not be booked because time
 * 15 is already booked by another event.
 * myCalendar.book(20, 30); // return True, The event can be booked, as the
 * first event takes every time less than 20, but not including 20.
 *
 * Constraints:
 *     ∙ 0 <= start < end <= 10⁹
 *     ∙ At most 1000 calls will be made to book.
 */

var MyCalendar = function() {
    this.events = []
};

/**
 * @param {number} start
 * @param {number} end
 * @return {boolean}
 */
MyCalendar.prototype.book = function(start, end) {
    const i = bisectLeft(this.events, [start, end],
                          (a, b) => (a[0] - b[0]) || (a[1] - b[1]));
    if (i && this.events[i-1][1] > start)
        return false;
    if (i < this.events.length && this.events[i][0] < end)
        return false;
    this.events.splice(i, 0, [start, end]);
    return true;
};

function bisectLeft(a, x, cmp=(a, b) => a - b, lo=0, hi=a.length) {
    if (!a.length)
        return 0;
    while (lo + 1 < hi) {
        const mid = Math.floor((hi + lo) / 2);
        cmp(x, a[mid]) <= 0 ? hi = mid : lo = mid;
    }
    return cmp(a[lo], x) >= 0 ? lo : hi;
}


let obj = new MyCalendar();
console.log(true == obj.book(10, 20));
console.log(false == obj.book(15, 25));
console.log(true == obj.book(20, 30));

obj = new MyCalendar();
console.log(true == obj.book(37,50));
console.log(false == obj.book(33, 50));
console.log(true == obj.book(4, 17));
console.log(false == obj.book(35, 48));
console.log(false == obj.book(8, 25));

obj = new MyCalendar();
console.log(true == obj.book(47,50));
console.log(true == obj.book(33, 41));
console.log(false == obj.book(39, 45));
console.log(false == obj.book(33, 42));
console.log(true == obj.book(25, 32));
console.log(false == obj.book(26, 35));
console.log(true == obj.book(19, 25));
console.log(true == obj.book(3, 8));
console.log(true == obj.book(8, 13));
console.log(false == obj.book(18, 27));
