/**
 * 15. 3Sum (Medium)
 * Given an integer array nums, return all the triplets [nums[i], nums[j],
 * nums[k]] such that i != j, i != k, and j != k, and
 * nums[i] + nums[j] + nums[k] == 0.
 *
 * Notice that the solution set must not contain duplicate triplets.
 *
 * Example 1:
 * Input: nums = [-1,0,1,2,-1,-4]
 * Output: [[-1,-1,2],[-1,0,1]]
 *
 * Example 2:
 * Input: nums = []
 * Output: []
 *
 * Example 3:
 * Input: nums = [0]
 * Output: []
 *
 * Constraints:
 *     0 <= nums.length <= 3000
 *     -10⁵ <= nums[i] <= 10⁵
 */

/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var threeSum = function(nums) {
    const ret = [];
    nums.sort((a, b) => a - b);
    for (let n1 = 0; n1 < nums.length - 2; ++n1) {
        if (nums[n1] > 0) { break; }
        if (n1 > 0 && nums[n1] === nums[n1-1]) { continue; }  // skip dups

        let n2 = n1 + 1;
        let n3 = nums.length - 1;

        while (n2 < n3) {
            const sum = nums[n1] + nums[n2] + nums[n3];
            if (sum === 0) {
                ret.push([nums[n1], nums[n2], nums[n3]]);
                while (nums[n2] === nums[n2+1]) { ++n2; }  // skip dups
                while (nums[n3] === nums[n3-1]) { --n3; }  // skip dups
                ++n2;
                --n3
            } else if (sum < 0) {
                ++n2;
            } else {
                --n3;
            }
        }
    }
    return ret;
};

const tests = [
    [[-1,0,1,2,-1,-4], [[-1,-1,2],[-1,0,1]]],
    [[], []],
    [[0], []],
    [[0,0], []],
    [[-2,0,0,2,2], [[-2,0,2]]],
];

for (let [nums, expected] of tests) {
    expected = expected.map(el => el.toString());
    test: {
        for (let triplet of threeSum(nums)) {
            const i = expected.indexOf(triplet.toString());
            if (i > -1) {
                expected.splice(i, 1);
            } else {
                console.log(false);
                break test;
            }
        }
        console.log(expected.length == 0);
    }
}
