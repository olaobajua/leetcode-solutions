"""
 * 413. Arithmetic Slices (Medium)
 * An integer array is called arithmetic if it consists of at least three
 * elements and if the difference between any two consecutive elements is the
 * same.
 *     ∙ For example, [1,3,5,7,9], [7,7,7,7], and [3,-1,-5,-9] are arithmetic
 *       sequences.
 *
 * Given an integer array nums, return the number of arithmetic subarrays of
 * nums.
 *
 * A subarray is a contiguous subsequence of the array.
 *
 * Example 1:
 * Input: nums = [1,2,3,4]
 * Output: 3
 * Explanation: We have 3 arithmetic slices in nums: [1, 2, 3], [2, 3, 4] and
 * [1,2,3,4] itself.
 *
 * Example 2:
 * Input: nums = [1]
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 5000
 *     ∙ -1000 <= nums[i] <= 1000
"""
from itertools import groupby
from operator import sub
from typing import List

class Solution:
    def numberOfArithmeticSlices(self, nums: List[int]) -> int:
        return sum(((2 * sum(1 for _ in group) - 1)**2 - 1) // 8
                   for _, group in groupby(map(sub, nums, nums[1:])))

if __name__ == "__main__":
    tests = (
        ([1,2,3,4], 3),
        ([1], 0),
        ([1,2,3,8,9,10], 2),
    )
    for nums, expected in tests:
        print(Solution().numberOfArithmeticSlices(nums) == expected)
