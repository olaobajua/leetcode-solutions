"""
 * 850. Rectangle Area II (Hard)
 * We are given a list of (axis-aligned) rectangles. Each
 * rectangle[i] = [xᵢ₁, yᵢ₁, xᵢ₂, yᵢ₂] , where (xᵢ₁, yᵢ₁) are the coordinates
 * of the bottom-left corner, and (xᵢ₂, yᵢ₂) are the coordinates of the
 * top-right corner of the ith rectangle.
 *
 * Find the total area covered by all rectangles in the plane. Since the answer
 * may be too large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: rectangles = [[0,0,2,2],[1,0,2,3],[1,0,3,1]]
 * Output: 6
 * Explanation: As illustrated in the picture.
 *
 * Example 2:
 * Input: rectangles = [[0,0,1000000000,1000000000]]
 * Output: 49
 * Explanation: The answer is 10¹⁸ modulo (10⁹ + 7), which is
 * (10⁹)² = (-7)² = 49.
 *
 * Constraints:
 *     ∙ 1 <= rectangles.length <= 200
 *     ∙ rectanges[i].length = 4
 *     ∙ 0 <= rectangles[i][j] <= 10⁹
 *     ∙ The total area covered by all rectangles will never exceed 2⁶³ - 1 and
 *       thus will fit in a 64-bit signed integer.
"""
from operator import itemgetter
from typing import List

def get_rectangles_in_section(rectangles, x1, x2):
    ys = set()
    for rx1, ry1, rx2, ry2 in rectangles:
        if rx1 < x2 and rx2 > x1:
            ys.add((ry1, ry2))
        if rx1 >= x2:
            break
    return sorted(ys)

class Solution:
    def rectangleArea(self, rectangles: List[List[int]]) -> int:
        rectangles = sorted(rectangles, key=itemgetter(0, 2))
        xplots = sorted({x for x1, _, x2, _ in rectangles for x in (x1, x2)})
        area = 0
        for x1, x2 in zip(xplots, xplots[1:]):
            ys = get_rectangles_in_section(rectangles, x1, x2)
            ystart = yend = -1
            for y1, y2 in ys:
                if y1 <= yend:
                    if y2 > yend:
                        yend = y2
                else:
                    area += (yend - ystart) * (x2 - x1)
                    ystart = y1
                    yend = y2
            area += (yend - ystart) * (x2 - x1)
        return area % (10**9 + 7)

if __name__ == "__main__":
    tests = (
            ([[0,0,2,2],[1,0,2,3],[1,0,3,1]], 6),
            ([[0,0,1000000000,1000000000]], 49),
    )
    for rectangles, expected in tests:
        print(Solution().rectangleArea(rectangles) == expected)
