/**
 * 300. Longest Increasing Subsequence (Medium)
 * Given an integer array nums, return the length of the longest strictly
 * increasing subsequence.
 *
 * A subsequence is a sequence that can be derived from an array by deleting
 * some or no elements without changing the order of the remaining elements.
 * For example, [3,6,2,7] is a subsequence of the array [0,3,1,6,2,2,7].
 *
 * Example 1:
 * Input: nums = [10,9,2,5,3,7,101,18]
 * Output: 4
 * Explanation: The longest increasing subsequence is [2,3,7,101], therefore
 * the length is 4.
 *
 * Example 2:
 * Input: nums = [0,1,0,3,2,3]
 * Output: 4
 *
 * Example 3:
 * Input: nums = [7,7,7,7,7,7,7]
 * Output: 1
 *
 * Constraints:
 *     1 <= nums.length <= 2500
 *     -10⁴ <= nums[i] <= 10⁴
 *
 * Follow up: Can you come up with an algorithm that runs in O(n log(n)) time
 * complexity?
 */
class Solution {
    public int lengthOfLIS(int[] nums) {
        List<Integer> ret = new ArrayList(Arrays.asList(nums[0]));
        for (int x: nums) {
            if (x > ret.get(ret.size() - 1)) {
                ret.add(x);
            } else {
                ret.set(bisectLeft(ret, x, 0, ret.size()), x);
            }
        }
        return ret.size();
    }

    int bisectLeft(List<Integer> a, int x, int lo, int hi) {
        if (a.size() == 0)
            return 0;
        while (lo + 1 < hi) {
            int mid = (hi + lo) / 2;
            int t = x <= a.get(mid) ? (hi = mid) : (lo = mid);
        }
        return a.get(lo) >= x ? lo : hi;
    }
}
