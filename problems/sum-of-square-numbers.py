"""
 * 633. Sum of Square Numbers (Medium)
 * Given a non-negative integer c, decide whether there're two integers a and b
 * such that a² + b² = c.
 *
 * Example 1:
 * Input: c = 5
 * Output: true
 * Explanation: 1 * 1 + 2 * 2 = 5
 *
 * Example 2:
 * Input: c = 3
 * Output: false
 *
 * Example 3:
 * Input: c = 4
 * Output: true
 *
 * Example 4:
 * Input: c = 2
 * Output: true
 *
 * Example 5:
 * Input: c = 1
 * Output: true
 *
 * Constraints:
 *     0 <= c <= 2³¹ - 1
"""
from math import isqrt

class Solution:
    def judgeSquareSum(self, c: int) -> bool:
        a = 0
        b = isqrt(c)
        while a <= b:
            square_sum = a**2 + b**2
            if square_sum == c:
                return True
            elif square_sum > c:
                b -= 1
            elif square_sum < c:
                a += 1
        return False

if __name__ == "__main__":
    tests = (
        (5, True),
        (3, False),
        (4, True),
        (2, True),
        (1, True),
        (6, False),
        (8, True),
        (999999999, False),
    )
    for c, expected in tests:
        print(Solution().judgeSquareSum(c) == expected)
