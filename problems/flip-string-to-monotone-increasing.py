"""
 * 926. Flip String to Monotone Increasing [Medium]
 * A binary string is monotone increasing if it consists of some number of 0's
 * (possibly none), followed by some number of 1's (also possibly none).
 *
 * You are given a binary string s. You can flip s[i] changing it from 0 to 1
 * or from 1 to 0.
 *
 * Return the minimum number of flips to make s monotone increasing.
 *
 * Example 1:
 * Input: s = "00110"
 * Output: 1
 * Explanation: We flip the last digit to get 00111.
 *
 * Example 2:
 * Input: s = "010110"
 * Output: 2
 * Explanation: We flip to get 011111, or alternatively 000111.
 *
 * Example 3:
 * Input: s = "00011000"
 * Output: 2
 * Explanation: We flip to get 00000000.
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ s[i] is either '0' or '1'.
"""
from collections import Counter

class Solution:
    def minFlipsMonoIncr(self, s: str) -> int:
        ret = zero_flips = Counter(s)["0"]
        one_flips = 0
        for c in s:
            if c == "0":
                zero_flips -= 1
            else:
                one_flips += 1
            ret = min(ret, zero_flips + one_flips)
        return ret

if __name__ == "__main__":
    tests = (
        ("00110", 1),
        ("010110", 2),
        ("00011000", 2),
        ("111011100100100", 7),
        ("0110011010001010011100011", 10),
    )
    for s, expected in tests:
        print(Solution().minFlipsMonoIncr(s) == expected)
