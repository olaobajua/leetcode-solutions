/**
 * 567. Permutation in String (Medium)
 * Given two strings s1 and s2, return true if s2 contains a permutation of s1,
 * or false otherwise.
 *
 * In other words, return true if one of s1's permutations is the substring of
 * s2.
 *
 * Example 1:
 * Input: s1 = "ab", s2 = "eidbaooo"
 * Output: true
 * Explanation: s2 contains one permutation of s1 ("ba").
 *
 * Example 2:
 * Input: s1 = "ab", s2 = "eidboaoo"
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= s1.length, s2.length <= 10⁴
 *     ∙ s1 and s2 consist of lowercase English letters.
 */
bool cmp(int *c1, int *c2) {
    for (int i = 0; i < 26; ++i) {
        if (c1[i] != c2[i]) {
            return false;
        }
    }
    return true;
}

bool checkInclusion(char * s1, char * s2) {
    const int n1 = strlen(s1);
    const int n2 = strlen(s2);
    if (n1 > n2) { return false; }
    int c1[26] = {0};
    int c2[26] = {0};
    for (int i = 0; i < n1; ++i) {
        ++c1[s1[i]-'a'];
        ++c2[s2[i]-'a'];
    }
    for (int i = n1; i < n2; ++i) {
        if (cmp(c1, c2)) {
            return true;
        }
        ++c2[s2[i]-'a'];
        --c2[s2[i-n1]-'a'];
    }
    return cmp(c1, c2);
}
