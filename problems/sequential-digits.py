"""
 * 1291. Sequential Digits [Medium]
 * An integer has sequential digits if and only if each digit in the number is
 * one more than the previous digit.
 *
 * Return a sorted list of all the integers in the range [low, high] inclusive
 * that have sequential digits.
 *
 * Example 1:
 * Input: low = 100, high = 300
 * Output: [123,234]
 * Example 2:
 * Input: low = 1000, high = 13000
 * Output: [1234,2345,3456,4567,5678,6789,12345]
 *
 * Constraints:
 *     ∙ 10 <= low <= high <= 10⁹
"""
from typing import List

class Solution:
    def sequentialDigits(self, low: int, high: int) -> List[int]:
        ret = []
        for i in range(1, 10):
            cur = i
            while cur <= high and i < 10:
                if cur >= low:
                    ret.append(cur)
                i += 1
                cur = 10 * cur + i

        return sorted(ret)

if __name__ == "__main__":
    tests = (
        (100, 300, [123,234]),
        (1000, 13000, [1234,2345,3456,4567,5678,6789,12345]),
    )
    for low, high, expected in tests:
        print(Solution().sequentialDigits(low, high) == expected)
