/**
 * 24. Swap Nodes in Pairs (Medium)
 * Given a linked list, swap every two adjacent nodes and return its head. You
 * must solve the problem without modifying the values in the list's nodes
 * (i.e., only nodes themselves may be changed.)
 *
 * Example 1:
 * Input: head = [1,2,3,4]
 * Output: [2,1,4,3]
 *
 * Example 2:
 * Input: head = []
 * Output: []
 *
 * Example 3:
 * Input: head = [1]
 * Output: [1]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 100].
 *     ∙ 0 <= Node.val <= 100
 */

// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var swapPairs = function(head) {
    if (head && head.next) {
        [head.next.next, head.next, head] = [head, head.next.next, head.next];
        head.next.next = swapPairs(head.next.next);
    }
    return head;
};

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    [[1,2,3,4], [2,1,4,3]],
    [[], []],
    [[1], [1]],
];

for (const [nums, expected] of tests) {
    const ret = linkedListToArray(swapPairs(arrayToLinkedList(nums)));
    console.log(ret.toString() == expected.toString());
}
