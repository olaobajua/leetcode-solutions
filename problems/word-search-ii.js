/**
 * 212. Word Search II (Hard)
 * Given an m x n board of characters and a list of strings words, return all
 * words on the board.
 *
 * Each word must be constructed from letters of sequentially adjacent cells,
 * where adjacent cells are horizontally or vertically neighboring. The same
 * letter cell may not be used more than once in a word.
 *
 * Example 1:
 * Input: board = [["o","a","a","n"],
 *                 ["e","t","a","e"],
 *                 ["i","h","k","r"],
 *                 ["i","f","l","v"]], words = ["oath","pea","eat","rain"]
 * Output: ["eat","oath"]
 *
 * Example 2:
 * Input: board = [["a","b"],
 *                 ["c","d"]], words = ["abcb"]
 * Output: []
 *
 * Constraints:
 *     m == board.length
 *     n == board[i].length
 *     1 <= m, n <= 12
 *     board[i][j] is a lowercase English letter.
 *     1 <= words.length <= 3 * 10⁴
 *     1 <= words[i].length <= 10
 *     words[i] consists of lowercase English letters.
 *     All the strings of words are unique.
 */

/**
 * @param {character[][]} board
 * @param {string[]} words
 * @return {string[]}
 */
var findWords = function(board, words) {
    function* getWords(r, c, prefs, word) {
        let l = board[r][c];
        if (l in prefs && (!seen[[r, c, word]] || seen[[r, c, word]] < 2)) {
            seen[[r, c, word]] = seen[[r, c, word]] + 1 || 1;
            word += l;
            let curPrefs = prefs[l];
            if (end in curPrefs) {
                delete curPrefs[end];  // remove found words from prefix tree
                yield word;
            }
            if (Object.keys(curPrefs).length === 0) {
                delete prefs[l];  // remove found words from prefix tree
            }
            board[r][c] = null;
            for (let [nr, nc] of [[r+1, c], [r, c+1], [r-1, c], [r, c-1]]) {
                if (0 <= nr && nr < m && 0 <= nc && nc < n) {
                    yield* getWords(nr, nc, curPrefs, word);
                }
            }
            board[r][c] = l;
        }
    }

    const end = '\0';
    let prefixes = {};
    for (let word of words) {  // initialize Trie
        let current = prefixes;
        for (let l of word) {
            current[l] = current[l] || {};
            current = current[l];
        }
        current[end] = null;
    }

    const m = board.length;
    const n = board[0].length;
    let seen = {};
    let onBoard = [];
    for (let r = 0; r < m; ++r) {
        for (let c = 0; c < n; ++c) {
            onBoard.push(...getWords(r, c, prefixes, ''));
        }
    }
    return onBoard;
};

tests = [
    [[["a","b"],
      ["c","d"]],
      ["abcb"], []],
    [[["o","a","a","n"],
      ["e","t","a","e"],
      ["i","h","k","r"],
      ["i","f","l","v"]],
      ["oath","pea","ate","eat","rain"], ["oath","ate","eat"]],
    [[["o","a","a","n"],
      ["e","t","a","e"],
      ["i","h","k","r"],
      ["i","f","l","v"]],
      ["oath","pea","eat","rain"], ["eat","oath"]],
    [[["o","a","a","n"],
      ["e","t","a","e"],
      ["i","h","k","r"],
      ["i","f","l","v"]],
     ["oath","pea","eat","rain","hklf", "hf"], ["oath","eat","hklf","hf"]],
    [[["o","a","a","n","n"],
      ["e","t","o","e","e"],
      ["t","o","t","o","e"],
      ["i","h","o","r","r"],
      ["i","f","l","v","v"]],
      ["otoa", "otoe", "otol", "otot"], ["otoa", "otoe", "otol", "otot"]],
    [[["e","e","c","d","b","b","c","b","c","d","e"],
      ["c","e","e","a","d","d","e","c","c","c","b"],
      ["b","e","a","c","d","a","a","b","c","d","c"],
      ["e","d","e","d","c","c","e","b","d","e","e"],
      ["b","b","b","a","b","d","b","b","b","a","a"],
      ["e","e","b","e","c","c","a","b","e","e","c"],
      ["b","a","b","c","b","d","a","d","c","d","a"],
      ["d","b","a","e","a","c","e","a","d","e","c"]],
     ["aeceecbee"], ["aeceecbee"]],
];
for (let [board, words, expected] of tests) {
    test: {
        for (let word of findWords(board, words)) {
            let i = expected.indexOf(word);
            if (i > -1) {
                expected.splice(i, 1);
            } else {
                console.log(false);
                break test;
            }
        }
        console.log(expected.length == 0);
    }
}
