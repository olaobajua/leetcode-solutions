/**
 * 1293. Shortest Path in a Grid with Obstacles Elimination (Hard)
 * You are given an m x n integer matrix grid where each cell is either 0
 * (empty) or 1 (obstacle). You can move up, down, left, or right from and to
 * an empty cell in one step.
 *
 * Return the minimum number of steps to walk from the upper left corner (0, 0)
 * to the lower right corner (m - 1, n - 1) given that you can eliminate at
 * most k obstacles. If it is not possible to find such walk return -1.
 *
 * Example 1:
 * Input:
 * grid = [[0,0,0],
 *         [1,1,0],
 *         [0,0,0],
 *         [0,1,1],
 *         [0,0,0]],
 * k = 1
 * Output: 6
 * Explanation:
 * The shortest path without eliminating any obstacle is 10.
 * The shortest path with one obstacle elimination at position (3,2) is 6.
 * Such path is (0,0) -> (0,1) -> (0,2) -> (1,2) -> (2,2) -> (3,2) -> (4,2).
 *
 * Example 2:
 * Input:
 * grid = [[0,1,1],
 *         [1,1,1],
 *         [1,0,0]],
 * k = 1
 * Output: -1
 * Explanation:
 * We need to eliminate at least two obstacles to find such a walk.
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m, n <= 40
 *     1 <= k <= m * n
 *     grid[i][j] == 0 or 1
 *     grid[0][0] == grid[m - 1][n - 1] == 0
 */

/**
 * @param {number[][]} grid
 * @param {number} k
 * @return {number}
 */
var shortestPath = function(grid, k) {
    let row_count = grid.length;
    let col_count = grid[0].length;
    if (k >= row_count + col_count - 3) {
        return row_count + col_count - 2;
    }
    let visited = new Set();
    let to_visit = [[0, 0, k, 0]];
    for (let [r, c, remaining, steps] of to_visit) {
        if (visited.has([r, c, remaining].toString()) || remaining < 0) {
            continue;
        }
        if (r == row_count - 1 && c == col_count - 1) {
            return steps;
        }
        for (let [nr, nc] of [[r, c + 1], [r + 1, c], [r, c - 1], [r - 1, c]]) {
            if (0 <= nr && nr < row_count && 0 <= nc && nc < col_count) {
                to_visit.push([nr, nc, remaining - grid[nr][nc], steps + 1]);
            }
        }
        visited.add([r, c, remaining].toString());
    }
    return -1;
};

tests = [
    [[[0,0,0],
      [1,1,0],
      [0,0,0],
      [0,1,1],
      [0,0,0]], 1, 6],
    [[[0,1,1],
      [1,1,1],
      [1,0,0]], 1, -1],
    [[[0,0,0,0,0,0,0,0,0,0],
      [0,1,1,1,1,1,1,1,1,0],
      [0,1,0,0,0,0,0,0,0,0],
      [0,1,0,1,1,1,1,1,1,1],
      [0,1,0,0,0,0,0,0,0,0],
      [0,1,1,1,1,1,1,1,1,0],
      [0,1,0,0,0,0,0,0,0,0],
      [0,1,0,1,1,1,1,1,1,1],
      [0,1,0,1,1,1,1,0,0,0],
      [0,1,0,0,0,0,0,0,1,0],
      [0,1,1,1,1,1,1,0,1,0],
      [0,0,0,0,0,0,0,0,1,0]], 1, 20],
]
for (let [grid, k, expected] of tests) {
    console.log(shortestPath(grid, k) == expected);
}
