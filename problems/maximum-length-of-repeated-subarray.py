"""
 * 718. Maximum Length of Repeated Subarray [Medium]
 * Given two integer arrays nums1 and nums2, return the maximum length of a
 * subarray that appears in both arrays.
 *
 * Example 1:
 * Input: nums1 = [1,2,3,2,1], nums2 = [3,2,1,4,7]
 * Output: 3
 * Explanation: The repeated subarray with maximum length is [3,2,1].
 *
 * Example 2:
 * Input: nums1 = [0,0,0,0,0], nums2 = [0,0,0,0,0]
 * Output: 5
 *
 * Constraints:
 *     ∙ 1 <= nums1.length, nums2.length <= 1000
 *     ∙ 0 <= nums1[i], nums2[i] <= 100
"""
from typing import List

class Solution:
    def findLength(self, nums1: List[int], nums2: List[int]) -> int:
        nums2 = ''.join(map(chr, nums2))
        nums1 = ''.join(map(chr, nums1))
        ret = i = 0
        for j in range(1, len(nums1) + 1):
            if nums1[i:j] in nums2:
                ret = max(ret, j - i)
            else:
                i += 1
        return ret

if __name__ == "__main__":
    tests = (
        ([1,2,3,2,1], [3,2,1,4,7], 3),
        ([0,0,0,0,0], [0,0,0,0,0], 5),
    )
    for nums1, nums2, expected in tests:
        print(Solution().findLength(nums1, nums2) == expected)
