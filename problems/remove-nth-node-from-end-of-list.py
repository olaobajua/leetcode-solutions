"""
 * 19. Remove Nth Node From End of List [Medium]
 * Given the head of a linked list, remove the nᵗʰ node from the end of the
 * list and return its head.
 *
 * Example 1:
 * Input: head = [1,2,3,4,5], n = 2
 * Output: [1,2,3,5]
 *
 * Example 2:
 * Input: head = [1], n = 1
 * Output: []
 *
 * Example 3:
 * Input: head = [1,2], n = 1
 * Output: [1]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is sz.
 *     ∙ 1 <= sz <= 30
 *     ∙ 0 <= Node.val <= 100
 *     ∙ 1 <= n <= sz
 *
 * Follow up: Could you do this in one pass?
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        fast = slow = head
        for _ in range(n):
            fast = fast.next
        if not fast:
            return head.next
        while fast.next:
            fast = fast.next
            slow = slow.next
        slow.next = slow.next.next
        return head

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head: Optional[ListNode]) -> List[int]:
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5], 2, [1,2,3,5]),
        ([1], 1, []),
        ([1,2], 1, [1]),
    )
    for nums, n, expected in tests:
        ret = Solution().removeNthFromEnd(list_to_linked_list(nums), n)
        print(linked_list_to_list(ret) == expected)
