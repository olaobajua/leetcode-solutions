/**
 * 322. Coin Change (Medium)
 * You are given an integer array coins representing coins of different
 * denominations and an integer amount representing a total amount of money.
 *
 * Return the fewest number of coins that you need to make up that amount.
 * If that amount of money cannot be made up by any combination of the coins,
 * return -1.
 *
 * You may assume that you have an infinite number of each kind of coin.
 *
 * Example 1:
 * Input: coins = [1,2,5], amount = 11
 * Output: 3
 * Explanation: 11 = 5 + 5 + 1
 *
 * Example 2:
 * Input: coins = [2], amount = 3
 * Output: -1
 *
 * Example 3:
 * Input: coins = [1], amount = 0
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= coins.length <= 12
 *     ∙ 1 <= coins[i] <= 2³¹ - 1
 *     ∙ 0 <= amount <= 10⁴
 */

/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function(coins, amount) {
    need = Array(amount + 1).fill(amount + 1);
    need[0] = 0;
    for (const c of coins) {
        for (let a = c; a <= amount; ++a) {
            need[a] = Math.min(need[a], need[a-c] + 1);
        }
    }
    return need[amount] > amount ? -1 : need[amount];
};

const tests = [
    [[1,2,5], 11, 3],
    [[2], 3, -1],
    [[1], 0, 0],
    [[1,2,5], 100, 20],
];
for (const [coins, amount, expected] of tests) {
    console.log(coinChange(coins, amount) == expected);
}
