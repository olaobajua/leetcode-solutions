"""
 * 138. Copy List with Random Pointer (Medium)
 * A linked list of length n is given such that each node contains an
 * additional random pointer, which could point to any node in the list, or
 * null.
 *
 * Construct a deep copy of the list. The deep copy should consist of exactly n
 * brand new nodes, where each new node has its value set to the value of its
 * corresponding original node. Both the next and random pointer of the new
 * nodes should point to new nodes in the copied list such that the pointers in
 * the original list and copied list represent the same list state. None of the
 * pointers in the new list should point to nodes in the original list.
 *
 * For example, if there are two nodes X and Y in the original list, where
 * X.random --> Y, then for the corresponding two nodes x and y in the copied
 * list, x.random --> y.
 *
 * Return the head of the copied linked list.
 *
 * The linked list is represented in the input/output as a list of n nodes.
 * Each node is represented as a pair of [val, random_index] where:
 *     ∙ val: an integer representing Node.val
 *     ∙ random_index: the index of the node (range from 0 to n-1) that the
 *       random pointer points to, or null if it does not point to any node.
 *
 * Your code will only be given the head of the original linked list.
 *
 * Example 1:
 * Input: head = [[7,null],[13,0],[11,4],[10,2],[1,0]]
 * Output: [[7,null],[13,0],[11,4],[10,2],[1,0]]
 *
 * Example 2:
 * Input: head = [[1,1],[2,1]]
 * Output: [[1,1],[2,1]]
 *
 * Example 3:
 * Input: head = [[3,null],[3,0],[3,null]]
 * Output: [[3,null],[3,0],[3,null]]
 *
 * Constraints:
 *     ∙ 0 <= n <= 1000
 *     ∙ -10⁴ <= Node.val <= 10⁴
 *     ∙ Node.random is null or is pointing to some node in the linked list.
"""
from itertools import zip_longest
from typing import Optional

# Definition for a Node.
class Node:
    def __init__(self, x: int, next: 'Node'=None, random: 'Node'=None):
        self.val = int(x)
        self.next = next
        self.random = random

class Solution:
    def copyRandomList(self, head: Optional['Node']) -> Optional['Node']:
        return list_to_linked_list(linked_list_to_list(head))

def list_to_linked_list(nums):
    l = [Node(val) for val, _ in nums]
    for cur, nxt, (_, random) in zip_longest(l, l[1:], nums):
        cur.next = nxt
        if random is not None:
            cur.random = l[random]
    return l[0] if l else None

def linked_list_to_list(head):
    l = []
    while head:
        l.append(head)
        head = head.next
    return [[n.val, l.index(n.random) if n.random in l else None] for n in l]

if __name__ == "__main__":
    tests = (
        [[7,None],[13,0],[11,4],[10,2],[1,0]],
        [[1,1],[2,1]],
        [[3,None],[3,0],[3,None]],
        [],
    )
    for nums in tests:
        root = list_to_linked_list(nums)
        ret = Solution().copyRandomList(root)
        while root and ret:
            if root.val != ret.val:
                print(False)
                break
            if root.random == ret.random and root.random is not None:
                print(False)
                break
            root = root.next
            ret = ret.next
        else:
            print(root is None and ret is None)
