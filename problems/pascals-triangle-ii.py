"""
 * 119. Pascal's Triangle II [Easy]
 * Given an integer rowIndex, return the rowIndexᵗʰ (0-indexed) row of the
 * Pascal's triangle.
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly
 * above it as shown:
 * Example 1:
 *        1
 *       1 1
 *      1 2 1
 *     1 3 3 1
 *    1 4 6 4 1
 *  1 5 10 10 5 1
 * 1 6 15 20 15 6 1
 * Input: rowIndex = 3
 * Output: [1,3,3,1]
 * Example 2:
 * Input: rowIndex = 0
 * Output: [1]
 * Example 3:
 * Input: rowIndex = 1
 * Output: [1,1]
 *
 * Constraints:
 *     ∙ 0 <= rowIndex <= 33
 *
 * Follow up: Could you optimize your algorithm to use only O(rowIndex) extra
 * space?
"""
from functools import cache
from typing import List

class Solution:
    @cache
    def getRow(self, rowIndex: int) -> List[int]:
        if rowIndex == 0:
            return [1]
        prev = self.getRow(rowIndex - 1)
        ret = [1]
        for i in range(1, len(prev)):
            ret.append(prev[i-1] + prev[i])
        return ret + [1]

if __name__ == "__main__":
    tests = (
        (3, [1,3,3,1]),
        (0, [1]),
        (1, [1,1]),
    )
    for rowIndex, expected in tests:
        print(Solution().getRow(rowIndex) == expected)
