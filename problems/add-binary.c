/**
 * 67. Add Binary (Easy)
 * Given two binary strings a and b, return their sum as a binary string.
 *
 * Example 1:
 * Input: a = "11", b = "1"
 * Output: "100"
 *
 * Example 2:
 * Input: a = "1010", b = "1011"
 * Output: "10101"
 *
 * Constraints:
 *     ∙ 1 <= a.length, b.length <= 10⁴
 *     ∙ a and b consist only of '0' or '1' characters.
 *     ∙ Each string does not contain leading zeros except for the zero itself.
 */
char * addBinary(char *a, char *b) {
    static char ret[10001];
    int c = 0, i = strlen(a) - 1, j = strlen(b) - 1, k = fmax(i, j) + 2;
    ret[k--] = '\0';
    while (k >= 0) {
        c += (i >= 0) ? a[i--] - '0' : 0;
        c += (j >= 0) ? b[j--] - '0' : 0;
        ret[k--] = c % 2 + '0';
        c /= 2;
    }
    if (ret[0] == '0') {
       memmove(ret, ret + 1, strlen(ret) * sizeof(char));
    }
    return ret;
}
