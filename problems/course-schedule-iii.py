"""
 * 630. Course Schedule III (Hard)
 * There are n different online courses numbered from 1 to n. You are given an
 * array courses where courses[i] = [durationᵢ, lastDayᵢ] indicate that the iᵗʰ
 * course should be taken continuously for durationᵢ days and must be finished
 * before or on lastDayᵢ.
 *
 * You will start on the 1ˢᵗ day and you cannot take two or more courses
 * simultaneously.
 *
 * Return the maximum number of courses that you can take.
 *
 * Example 1:
 * Input: courses = [[100,200],[200,1300],[1000,1250],[2000,3200]]
 * Output: 3
 * Explanation:
 * There are totally 4 courses, but you can take 3 courses at most:
 * First, take the 1ˢᵗ course, it costs 100 days so you will finish it on the
 * 100ᵗʰ day, and ready to take the next course on the 101ˢᵗ day.
 * Second, take the 3ʳᵈ course, it costs 1000 days so you will finish it on the
 * 1100ᵗʰ day, and ready to take the next course on the 1101ˢᵗ day.
 * Third, take the 2ⁿᵈ course, it costs 200 days so you will finish it on the
 * 1300ᵗʰ day.
 * The 4ᵗʰ course cannot be taken now, since you will finish it on the 3300ᵗʰ
 * day, which exceeds the closed date.
 *
 * Example 2:
 * Input: courses = [[1,2]]
 * Output: 1
 *
 * Example 3:
 * Input: courses = [[3,2],[4,3]]
 * Output: 0
 *
 * Constraints:
 *     ∙ 1 <= courses.length <= 10⁴
 *     ∙ 1 <= durationᵢ, lastDayᵢ <= 10⁴
"""
from heapq import heappop, heappush
from typing import List

class Solution:
    def scheduleCourse(self, courses: List[List[int]]) -> int:
        heap = []
        day = 0
        for duration, end in sorted(courses, key=lambda x: x[1]):
            day += duration
            heappush(heap, -duration)
            if day > end:
                day += heappop(heap)
        return len(heap)

if __name__ == "__main__":
    tests = (
        ([[100,200],[200,1300],[1000,1250],[2000,3200]], 3),
        ([[1,2]], 1),
        ([[3,2],[4,3]], 0),
        ([[9,14],[7,12],[1,11],[4,7]], 3),
        ([[5,11],[3,5],[10,20],[4,20],[10,16]], 3),
        ([[5,15],[3,19],[6,7],[2,10],[5,16],[8,14],[10,11],[2,19]], 5),
    )
    for courses, expected in tests:
        print(Solution().scheduleCourse(courses) == expected)
