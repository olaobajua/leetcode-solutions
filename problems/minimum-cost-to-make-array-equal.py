"""
 * 2448. Minimum Cost to Make Array Equal [Hard]
 * You are given two 0-indexed arrays nums and cost consisting each of n
 * positive integers.
 *
 * You can do the following operation any number of times:
 *     ∙ Increase or decrease any element of the array nums by 1.
 *
 * The cost of doing one operation on the iᵗʰ element is cost[i].
 *
 * Return the minimum total cost such that all the elements of the array nums
 * become equal.
 *
 * Example 1:
 * Input: nums = [1,3,5,2], cost = [2,3,1,14]
 * Output: 8
 * Explanation: We can make all the elements equal to 2 in the following way:
 * - Increase the 0ᵗʰ element one time. The cost is 2.
 * - Decrease the 1<sup><span style="font-size: 10.8333px;">st</span></sup>
 * element one time. The cost is 3.
 * - Decrease the 2ⁿᵈ element three times. The cost is 1 + 1 + 1 = 3.
 * The total cost is 2 + 3 + 3 = 8.
 * It can be shown that we cannot make the array equal with a smaller cost.
 *
 * Example 2:
 * Input: nums = [2,2,2,2,2], cost = [4,2,8,1,3]
 * Output: 0
 * Explanation: All the elements are already equal, so no operations are
 * needed.
 *
 * Constraints:
 *     ∙ n == nums.length == cost.length
 *     ∙ 1 <= n <= 10⁵
 *     ∙ 1 <= nums[i], cost[i] <= 10⁶
"""
from typing import List

class Solution:
    def minCost(self, nums: List[int], cost: List[int]) -> int:
        def get_cost(target):
            return sum(abs(x - target) * c for x, c in zip(nums, cost))

        lo = min(nums)
        hi = max(nums)
        while lo < hi:
            mid = (lo + hi) // 2
            if get_cost(mid) < get_cost(mid + 1):
                hi = mid
            else:
                lo = mid + 1
        return get_cost(lo)

if __name__ == "__main__":
    tests = (
        ([1,3,5,2], [2,3,1,14], 8),
        ([2,2,2,2,2], [4,2,8,1,3], 0),
    )
    for nums, cost, expected in tests:
        print(Solution().minCost(nums, cost) == expected)
