/**
 * 661. Image Smoother [Easy]
 * An image smoother is a filter of the size 3 x 3 that can be applied to each
 * cell of an image by rounding down the average of the cell and the eight
 * surrounding cells (i.e., the average of the nine cells in the blue
 * smoother). If one or more of the surrounding cells of a cell is not present,
 * we do not consider it in the average (i.e., the average of the four cells in
 * the red smoother).
 *
 * Given an m x n integer matrix img representing the grayscale of an image,
 * return the image after applying the smoother on each cell of it.
 *
 * Example 1:
 * Input: img = [[1,1,1],
 *               [1,0,1],
 *               [1,1,1]]
 * Output: [[0,0,0],
 *          [0,0,0],
 *          [0,0,0]]
 * Explanation:
 * For the points (0,0), (0,2), (2,0), (2,2): floor(3/4) = floor(0.75) = 0
 * For the points (0,1), (1,0), (1,2), (2,1): floor(5/6) = floor(0.83333333) =
 * 0
 * For the point (1,1): floor(8/9) = floor(0.88888889) = 0
 *
 * Example 2:
 * Input: img = [[100,200,100],
 *               [200,50,200],
 *               [100,200,100]]
 * Output: [[137,141,137],
 *          [141,138,141],
 *          [137,141,137]]
 * Explanation:
 * For the points (0,0), (0,2), (2,0), (2,2): floor((100+200+200+50)/4) =
 * floor(137.5) = 137
 * For the points (0,1), (1,0), (1,2), (2,1):
 * floor((200+200+50+200+100+100)/6) = floor(141.666667) = 141
 * For the point (1,1): floor((50+200+200+200+200+100+100+100+100)/9) =
 * floor(138.888889) = 138
 *
 * Constraints:
 *     ∙ m == img.length
 *     ∙ n == img[i].length
 *     ∙ 1 <= m, n <= 200
 *     ∙ 0 <= img[i][j] <= 255
 */
#include <vector>

class Solution {
public:
    vector<vector<int>> imageSmoother(vector<vector<int>>& img) {
        int m = img.size();
        int n = img[0].size();
        std::vector<std::vector<int>> ret(m, std::vector<int>(n, 0));
        std::vector<std::vector<int>> count(m, std::vector<int>(n, 1));

        // pad the input and count matrices with zeros around the edges
        img.insert(img.begin(), std::vector<int>(n, 0));
        img.push_back(std::vector<int>(n, 0));
        count.insert(count.begin(), std::vector<int>(n, 0));
        count.push_back(std::vector<int>(n, 0));
        m += 2;

        for (int i = 0; i < m; ++i) {
            img[i].insert(img[i].begin(), 0);
            img[i].push_back(0);
            count[i].insert(count[i].begin(), 0);
            count[i].push_back(0);
        }
        n += 2;

        // Apply the image smoothing algorithm
        for (int r = 1; r < m - 1; ++r) {
            for (int c = 1; c < n - 1; ++c) {
                int s = 0;
                s += img[r-1][c-1] + img[r-1][c] + img[r-1][c+1];
                s += img[r][c-1] + img[r][c] + img[r][c+1];
                s += img[r+1][c-1] + img[r+1][c] + img[r+1][c+1];

                int k = 0;
                k += count[r-1][c-1] + count[r-1][c] + count[r-1][c+1];
                k += count[r][c-1] + count[r][c] + count[r][c+1];
                k += count[r+1][c-1] + count[r+1][c] + count[r+1][c+1];

                ret[r-1][c-1] = s / k;
            }
        }

        return ret;
    }
};
