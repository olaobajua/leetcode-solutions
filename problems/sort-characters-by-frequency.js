/**
 * 451. Sort Characters By Frequency (Medium)
 * Given a string s, sort it in decreasing order based on the frequency of the
 * characters. The frequency of a character is the number of times it appears
 * in the string.
 *
 * Return the sorted string. If there are multiple answers, return any of them.
 *
 * Example 1:
 * Input: s = "tree"
 * Output: "eert"
 * Explanation: 'e' appears twice while 'r' and 't' both appear once.
 * So 'e' must appear before both 'r' and 't'. Therefore "eetr" is also a valid
 * answer.
 *
 * Example 2:
 * Input: s = "cccaaa"
 * Output: "aaaccc"
 * Explanation: Both 'c' and 'a' appear three times, so both "cccaaa" and
 * "aaaccc" are valid answers.
 * Note that "cacaca" is incorrect, as the same characters must be together.
 *
 * Example 3:
 * Input: s = "Aabb"
 * Output: "bbAa"
 * Explanation: "bbaA" is also a valid answer, but "Aabb" is incorrect.
 * Note that 'A' and 'a' are treated as two different characters.
 *
 * Constraints:
 *     1 <= s.length <= 5 * 10⁵
 *     s consists of uppercase and lowercase English letters and digits.
 */

/**
 * @param {string} s
 * @return {string}
 */
var frequencySort = function(s) {
    return new Counter(s).mostCommon().map(([c, f]) => c.repeat(f)).join('');
};

class Counter extends Map {
    constructor(iterable) {
        super();
        [...iterable].forEach(x => this.set(x, this.get(x) + 1 || 1));
    }
    mostCommon() {
        return [...this.entries()].sort((a, b) => b[1] - a[1]);
    }
}

tests = [
    ["tree", ["eert", "eetr"]],
    ["cccaaa", ["aaaccc", "cccaaa"]],
    ["Aabb", ["bbaA", "bbAa"]],
    ["loveleetcode", ["eeeelloovtdc", "eeeeoollvtdc", "eeeelloovtcd"]],
]
for (let [s, expected] of tests) {
    console.log(expected.includes(frequencySort(s)));
}
