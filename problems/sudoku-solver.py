"""
 * 37. Sudoku Solver (Hard)
 * Write a program to solve a Sudoku puzzle by filling the empty cells.
 * A sudoku solution must satisfy all of the following rules:
 *     ∙ Each of the digits 1-9 must occur exactly once in each row.
 *     ∙ Each of the digits 1-9 must occur exactly once in each column.
 *     ∙ Each of the digits 1-9 must occur exactly once in each of the 9 3x3
 *       sub-boxes of the grid.
 *
 * The '.' character indicates empty cells.
 *
 * Example 1:
 * Input: board = [["5","3",".",".","7",".",".",".","."],
                   ["6",".",".","1","9","5",".",".","."],
                   [".","9","8",".",".",".",".","6","."],
                   ["8",".",".",".","6",".",".",".","3"],
                   ["4",".",".","8",".","3",".",".","1"],
                   ["7",".",".",".","2",".",".",".","6"],
                   [".","6",".",".",".",".","2","8","."],
                   [".",".",".","4","1","9",".",".","5"],
                   [".",".",".",".","8",".",".","7","9"]]
 * Output: [["5","3","4","6","7","8","9","1","2"],
            ["6","7","2","1","9","5","3","4","8"],
            ["1","9","8","3","4","2","5","6","7"],
            ["8","5","9","7","6","1","4","2","3"],
            ["4","2","6","8","5","3","7","9","1"],
            ["7","1","3","9","2","4","8","5","6"],
            ["9","6","1","5","3","7","2","8","4"],
            ["2","8","7","4","1","9","6","3","5"],
            ["3","4","5","2","8","6","1","7","9"]]
 * Explanation: The input board is shown above and the only valid solution is
 * shown below.
 *
 * Constraints:
 *     ∙ board.length == 9
 *     ∙ board[i].length == 9
 *     ∙ board[i][j] is a digit or '.'.
 *     ∙ It is guaranteed that the input board has only one solution.
"""
from itertools import chain
from typing import List

class Solution:
    def solveSudoku(self, board: List[List[str]]) -> None:
        """Do not return anything, modify board in-place instead."""
        for row, col, value in next(solve(get_opprotunities(board))):
            board[row][col] = value

def get_opprotunities(board):
    rows = [set(row) for row in board]
    cols = [set(col) for col in zip(*board)]
    sqrs = [set(chain(*[*zip(*board[row:row+3])][col:col+3]))
            for row in range(0, 9, 3) for col in range(0, 9, 3)]
    all_variants = frozenset("123456789")
    return [(r, c, all_variants - rows[r] - cols[c] - sqrs[3*(r//3)+c//3])
            for r in range(len(board)) for c in range(len(board[r]))
            if board[r][c] == "."]

def solve(opprotunities, selected=()):
    opprotunities = sorted(opprotunities, key=lambda x: len(x[2]))
    curr, curc, cur_values = opprotunities[0]
    if len(opprotunities) == 1:
        yield selected + ((curr, curc, tuple(cur_values)[0]),)
    else:
        for cv in cur_values:
            no = [(r, c, vals - {cv} if is_linked(r, c, curr, curc) else vals)
                  for r, c, vals in opprotunities[1:]]
            cells_without_opportunities = [o for o in no if len(o[2]) == 0]
            if not cells_without_opportunities:
                yield from solve(no, selected + ((curr, curc, cv),))

def is_linked(r1, c1, r2, c2):
    return r1 == r2 or c1 == c2 or r1//3 == r2//3 and c1//3 == c2//3

if __name__ == "__main__":
    tests = [
        dict(board=[["5","3",".",".","7",".",".",".","."],
                    ["6",".",".","1","9","5",".",".","."],
                    [".","9","8",".",".",".",".","6","."],
                    ["8",".",".",".","6",".",".",".","3"],
                    ["4",".",".","8",".","3",".",".","1"],
                    ["7",".",".",".","2",".",".",".","6"],
                    [".","6",".",".",".",".","2","8","."],
                    [".",".",".","4","1","9",".",".","5"],
                    [".",".",".",".","8",".",".","7","9"]],
             expected=[["5","3","4","6","7","8","9","1","2"],
                       ["6","7","2","1","9","5","3","4","8"],
                       ["1","9","8","3","4","2","5","6","7"],
                       ["8","5","9","7","6","1","4","2","3"],
                       ["4","2","6","8","5","3","7","9","1"],
                       ["7","1","3","9","2","4","8","5","6"],
                       ["9","6","1","5","3","7","2","8","4"],
                       ["2","8","7","4","1","9","6","3","5"],
                       ["3","4","5","2","8","6","1","7","9"]]),

        dict(board=[[".",".","9","7","4","8",".",".","."],
                    ["7",".",".",".",".",".",".",".","."],
                    [".","2",".","1",".","9",".",".","."],
                    [".",".","7",".",".",".","2","4","."],
                    [".","6","4",".","1",".","5","9","."],
                    [".","9","8",".",".",".","3",".","."],
                    [".",".",".","8",".","3",".","2","."],
                    [".",".",".",".",".",".",".",".","6"],
                    [".",".",".","2","7","5","9",".","."]],
             expected=[['5','1','9','7','4','8','6','3','2'],
                       ['7','8','3','6','5','2','4','1','9'],
                       ['4','2','6','1','3','9','8','7','5'],
                       ['3','5','7','9','8','6','2','4','1'],
                       ['2','6','4','3','1','7','5','9','8'],
                       ['1','9','8','5','2','4','3','6','7'],
                       ['9','7','5','8','6','3','1','2','4'],
                       ['8','3','2','4','9','1','7','5','6'],
                       ['6','4','1','2','7','5','9','8','3']]),
    ]
    for test in tests:
        Solution().solveSudoku(test["board"])
        print(test["board"] == test["expected"])
