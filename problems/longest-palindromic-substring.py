"""
 * 5. Longest Palindromic Substring [Medium]
 * Given a string s, return the longest palindromic substring in s.
 *
 * Example 1:
 * Input: s = "babad"
 * Output: "bab"
 * Explanation: "aba" is also a valid answer.
 *
 * Example 2:
 * Input: s = "cbbd"
 * Output: "bb"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 1000
 *     ∙ s consist of only digits and English letters.
"""
class Solution:
    def longestPalindrome(self, s: str) -> str:
        s = "|" + "|".join(list(s)) + "|"
        radiuses = manacher_odd(s)
        m = max(radiuses)
        i = radiuses.index(m)
        return s[i-m+1:i+m].replace("|", "")

def manacher_odd(s):
    """
    Return array of max palindrome radiuses for every point in s.
    """
    s = '^' + s + '$'
    len_s = len(s)
    radii = [0] * len_s
    l = 0
    r = 0
    for i in range(1, len_s - 1):
        radii[i] = max(0, min(r - i, radii[l + (r - i)]))
        while s[i - radii[i]] == s[i + radii[i]]:
            radii[i] += 1
        if i + radii[i] > r:
            l = i - radii[i]
            r = i + radii[i]
    return radii[1:-1]

if __name__ == "__main__":
    tests = (
        ("babad", "bab"),
        ("cbbd", "bb"),
    )
    for s, expected in tests:
        print(Solution().longestPalindrome(s) == expected)
