"""
 * 318. Maximum Product of Word Lengths (Medium)
 * Given a string array words, return the maximum value of
 * length(word[i]) * length(word[j]) where the two words do not share common
 * letters. If no such two words exist, return 0.
 *
 * Example 1:
 * Input: words = ["abcw","baz","foo","bar","xtfn","abcdef"]
 * Output: 16
 * Explanation: The two words can be "abcw", "xtfn".
 *
 * Example 2:
 * Input: words = ["a","ab","abc","d","cd","bcd","abcd"]
 * Output: 4
 * Explanation: The two words can be "ab", "cd".
 *
 * Example 3:
 * Input: words = ["a","aa","aaa","aaaa"]
 * Output: 0
 * Explanation: No such pair of words.
 *
 * Constraints:
 *     ∙ 2 <= words.length <= 1000
 *     ∙ 1 <= words[i].length <= 1000
 *     ∙ words[i] consists only of lowercase English letters.
"""
from functools import cache, reduce
from itertools import combinations
from operator import or_
from typing import List

class Solution:
    def maxProduct(self, words: List[str]) -> int:
        @cache
        def wordmask(word):
            return reduce(or_, map(lambda c: 1 << ord(c) - ord('a'), word))
        return max([len(w1) * len(w2)
                    for w1, w2 in combinations(words, 2)
                    if wordmask(w1) & wordmask(w2) == 0],
                   default=0)

if __name__ == "__main__":
    tests = (
        (["abcw","baz","foo","bar","xtfn","abcdef"], 16),
        (["a","ab","abc","d","cd","bcd","abcd"], 4),
        (["a","aa","aaa","aaaa"], 0),
    )
    for words, expected in tests:
        print(Solution().maxProduct(words) == expected)
