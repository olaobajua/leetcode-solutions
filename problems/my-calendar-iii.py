"""
 * 732. My Calendar III [Hard]
 * A k-booking happens when k events have some non-empty intersection (i.e.,
 * there is some time that is common to all k events.)
 *
 * You are given some events [start, end), after each given event, return an
 * integer k representing the maximum k-booking between all the previous
 * events.
 *
 * Implement the MyCalendarThree class:
 *     ∙ MyCalendarThree() Initializes the object.
 *     ∙ int book(int start, int end) Returns an integer k representing the
 *       largest integer such that there exists a k-booking in the calendar.
 *
 * Example 1:
 * Input
 * ["MyCalendarThree", "book", "book", "book", "book", "book", "book"]
 * [[], [10, 20], [50, 60], [10, 40], [5, 15], [5, 10], [25, 55]]
 * Output
 * [null, 1, 1, 2, 3, 3, 3]
 *
 * Explanation
 * MyCalendarThree myCalendarThree = new MyCalendarThree();
 * myCalendarThree.book(10, 20); // return 1, The first event can be booked
 * and is disjoint, so the maximum k-booking is a 1-booking.
 * myCalendarThree.book(50, 60); // return 1, The second event can be booked
 * and is disjoint, so the maximum k-booking is a 1-booking.
 * myCalendarThree.book(10, 40); // return 2, The third event [10, 40)
 * intersects the first event, and the maximum k-booking is a 2-booking.
 * myCalendarThree.book(5, 15); // return 3, The remaining events cause the
 * maximum K-booking to be only a 3-booking.
 * myCalendarThree.book(5, 10); // return 3
 * myCalendarThree.book(25, 55); // return 3
 *
 * Constraints:
 *     ∙ 0 <= start < end <= 10⁹
 *     ∙ At most 400 calls will be made to book.
"""
from collections import Counter

class MyCalendarThree:
    def __init__(self):
        self.max = Counter()
        self.add = Counter()

    def book(self, start, end):
        def modify(i=1, left=0, right=1000000000):
            # segment tree
            if end <= left or right <= start:
                return
            if start <= left < right <= end:
                self.max[i] += 1
                self.add[i] += 1
            else:
                mid = (left + right) // 2
                modify(2*i, left, mid)
                modify(2*i + 1, mid, right)
                self.max[i] = max(self.max[2*i], self.max[2*i+1]) + self.add[i]
        modify()
        return self.max[1]

obj = MyCalendarThree()
print(1 == obj.book(10, 20))
print(1 == obj.book(50, 60))
print(2 == obj.book(10, 40))
print(3 == obj.book(5, 15))
print(3 == obj.book(5, 10))
print(3 == obj.book(25, 55))
print()

obj = MyCalendarThree()
print(1 == obj.book(24, 40))
print(1 == obj.book(43, 50))
print(2 == obj.book(27, 43))
print(2 == obj.book(5, 21))
print(3 == obj.book(30, 40))
print(3 == obj.book(14, 29))
print(3 == obj.book(3, 19))
print(3 == obj.book(3, 14))
print(4 == obj.book(25, 39))
print(4 == obj.book(6, 19))
print()

obj = MyCalendarThree()
print(1 == obj.book(97,100))
print(1 == obj.book(51,65))
print(1 == obj.book(27,46))
print(2 == obj.book(90,100))
print(2 == obj.book(20,32))
print(3 == obj.book(15,28))
print(3 == obj.book(60,73))
print(3 == obj.book(77,91))
print(3 == obj.book(67,85))
print(3 == obj.book(58,72))
print(3 == obj.book(74,93))
print(4 == obj.book(73,83))
print(5 == obj.book(71,87))
print(5 == obj.book(97,100))
print(5 == obj.book(14,31))
print(5 == obj.book(26,37))
print(5 == obj.book(66,76))
print(5 == obj.book(52,67))
print(6 == obj.book(24,43))
print(6 == obj.book(6,23))
print(6 == obj.book(94,100))
print(6 == obj.book(33,44))
print(6 == obj.book(30,46))
print(6 == obj.book(6,20))
print(6 == obj.book(71,87))
print(6 == obj.book(49,59))
print(6 == obj.book(38,55))
print(6 == obj.book(4,17))
print(6 == obj.book(46,61))
print(7 == obj.book(13,31))  #
print(7 == obj.book(94,100))  #
print(7 == obj.book(47,65))  #
print(7 == obj.book(9,25))
print(8 == obj.book(4,20))
print(9 == obj.book(2,17))
print(9 == obj.book(28,42))
print(9 == obj.book(26,38))
print(9 == obj.book(72,83))
print(9 == obj.book(43,61))
print(10 == obj.book(18,35))  #
