/**
 * 1465. Maximum Area of a Piece of Cake After Horizontal and Vertical Cuts
 * (Medium)
 * You are given a rectangular cake of size h x w and two arrays of integers
 * horizontalCuts and verticalCuts where:
 *     ∙ horizontalCuts[i] is the distance from the top of the rectangular cake
 *       to the iᵗʰ horizontal cut and similarly, and
 *     ∙ verticalCuts[j] is the distance from the left of the rectangular cake
 *       to the jᵗʰ vertical cut.
 *
 * Return the maximum area of a piece of cake after you cut at each horizontal
 * and vertical position provided in the arrays horizontalCuts and
 * verticalCuts. Since the answer can be a large number, return this
 * modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: h = 5, w = 4, horizontalCuts = [1,2,4], verticalCuts = [1,3]
 * Output: 4
 *
 * Example 2:
 * Input: h = 5, w = 4, horizontalCuts = [3,1], verticalCuts = [1]
 * Output: 6
 *
 * Example 3:
 * Input: h = 5, w = 4, horizontalCuts = [3], verticalCuts = [3]
 * Output: 9
 *
 * Constraints:
 *     ∙ 2 <= h, w <= 10⁹
 *     ∙ 1 <= horizontalCuts.length <= min(h - 1, 10⁵)
 *     ∙ 1 <= verticalCuts.length <= min(w - 1, 10⁵)
 *     ∙ 1 <= horizontalCuts[i] < h
 *     ∙ 1 <= verticalCuts[i] < w
 *     ∙ All the elements in horizontalCuts are distinct.
 *     ∙ All the elements in verticalCuts are distinct.
 */
int compare(const int *a, const int *b) {
  return *a - *b;
}
int maxArea(int h, int w, int *horizontalCuts, int horizontalCutsSize, int *verticalCuts, int verticalCutsSize) {
    horizontalCutsSize += 2;
    verticalCutsSize += 2;
    horizontalCuts = realloc(horizontalCuts, horizontalCutsSize * sizeof(int));
    verticalCuts = realloc(verticalCuts, verticalCutsSize * sizeof(int));
    horizontalCuts[horizontalCutsSize-2] = 0;
    horizontalCuts[horizontalCutsSize-1] = h;
    verticalCuts[verticalCutsSize-2] = 0;
    verticalCuts[verticalCutsSize-1] = w;
    qsort(horizontalCuts, horizontalCutsSize, sizeof(int), compare);
    qsort(verticalCuts, verticalCutsSize, sizeof(int), compare);
    int max_horizontal_cut = 0, max_vertical_cut = 0;
    for (int i = 1; i < horizontalCutsSize; ++i) {
        max_horizontal_cut = fmax(horizontalCuts[i] - horizontalCuts[i-1],
                                  max_horizontal_cut);
    }
    for (int i = 1; i < verticalCutsSize; ++i) {
        max_vertical_cut = fmax(verticalCuts[i] - verticalCuts[i-1],
                                max_vertical_cut);
    }
    return (long)max_horizontal_cut * max_vertical_cut % 1000000007;
}
