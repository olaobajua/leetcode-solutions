/**
 * 368. Largest Divisible Subset [Medium]
 * Given a set of distinct positive integers nums, return the largest subset
 * answer such that every pair (answer[i], answer[j]) of elements in this
 * subset satisfies:
 *     ∙ answer[i] % answer[j] == 0, or
 *     ∙ answer[j] % answer[i] == 0
 *
 * If there are multiple solutions, return any of them.
 *
 * Example 1:
 * Input: nums = [1,2,3]
 * Output: [1,2]
 * Explanation: [1,3] is also accepted.
 *
 * Example 2:
 * Input: nums = [1,2,4,8]
 * Output: [1,2,4,8]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 1000
 *     ∙ 1 <= nums[i] <= 2 * 10⁹
 *     ∙ All the integers in nums are unique.
 */
#include <vector>
#include <unordered_map>
#include <algorithm>

class Solution {
public:
    vector<int> largestDivisibleSubset(vector<int>& nums) {
        std::unordered_map<int, std::unordered_set<int>> divs;

        sort(nums.begin(), nums.end());

        for (int n : nums) {
            divs[n] = {};
            for (auto& pair : divs) {
                int x = pair.first;
                if (n % x == 0 && divs[x].size() > divs[n].size()) {
                    divs[n] = divs[x];
                }
            }
            divs[n].insert(n);
        }

        auto ret = max_element(
            divs.begin(),
            divs.end(),
            [](const auto& a, const auto& b) {
                return a.second.size() < b.second.size();
            }
        );

        return vector<int>(ret->second.begin(), ret->second.end());
    }
};
