/**
 * 2092. Find All People With Secret (Hard)
 * You are given an integer n indicating there are n people numbered from 0 to
 * n - 1. You are also given a 0-indexed 2D integer array meetings where
 * meetings[i] = [xᵢ, yᵢ, timeᵢ] indicates that person xᵢ and person yᵢ have a
 * meeting at timeᵢ. A person may attend multiple meetings at the same time.
 * Finally, you are given an integer firstPerson.
 *
 * Person 0 has a secret and initially shares the secret with a person
 * firstPerson at time 0. This secret is then shared every time a meeting takes
 * place with a person that has the secret. More formally, for every meeting,
 * if a person xᵢ has the secret at timeᵢ, then they will share the secret with
 * person yᵢ, and vice versa.
 *
 * The secrets are shared instantaneously. That is, a person may receive the
 * secret and share it with people in other meetings within the same time
 * frame.
 *
 * Return a list of all the people that have the secret after all the meetings
 * have taken place. You may return the answer in any order.
 *
 * Example 1:
 * Input: n = 6, meetings = [[1,2,5],[2,3,8],[1,5,10]], firstPerson = 1
 * Output: [0,1,2,3,5]
 * Explanation:
 * At time 0, person 0 shares the secret with person 1.
 * At time 5, person 1 shares the secret with person 2.
 * At time 8, person 2 shares the secret with person 3.
 * At time 10, person 1 shares the secret with person 5.
 * Thus, people 0, 1, 2, 3, and 5 know the secret after all the meetings.
 *
 * Example 2:
 * Input: n = 4, meetings = [[3,1,3],[1,2,2],[0,3,3]], firstPerson = 3
 * Output: [0,1,3]
 * Explanation:
 * At time 0, person 0 shares the secret with person 3.
 * At time 2, neither person 1 nor person 2 know the secret.
 * At time 3, person 3 shares the secret with person 0 and person 1.
 * Thus, people 0, 1, and 3 know the secret after all the meetings.
 *
 * Example 3:
 * Input: n = 5, meetings = [[3,4,2],[1,2,1],[2,3,1]], firstPerson = 1
 * Output: [0,1,2,3,4]
 * Explanation:
 * At time 0, person 0 shares the secret with person 1.
 * At time 1, person 1 shares the secret with person 2, and person 2 shares the
 * secret with person 3.
 * Note that person 2 can share the secret at the same time as receiving it.
 * At time 2, person 3 shares the secret with person 4.
 * Thus, people 0, 1, 2, 3, and 4 know the secret after all the meetings.
 *
 * Example 4:
 * Input: n = 6, meetings = [[0,2,1],[1,3,1],[4,5,1]], firstPerson = 1
 * Output: [0,1,2,3]
 * Explanation:
 * At time 0, person 0 shares the secret with person 1.
 * At time 1, person 0 shares the secret with person 2, and person 1 shares the
 * secret with person 3.
 * Thus, people 0, 1, 2, and 3 know the secret after all the meetings.
 *
 * Constraints:
 *     2 <= n <= 10⁵
 *     1 <= meetings.length <= 10⁵
 *     meetings[i].length == 3
 *     0 <= xᵢ, yᵢ <= n - 1
 *     xᵢ != yᵢ
 *     1 <= timeᵢ <= 10⁵
 *     1 <= firstPerson <= n - 1
 */

/**
 * @param {number} n
 * @param {number[][]} meetings
 * @param {number} firstPerson
 * @return {number[]}
 */
var findAllPeople = function(n, meetings, firstPerson) {
    const knowSecret = new Set([0, firstPerson]);
    const meetingsByTime = {};
    for (const meeting of meetings) {
        const time = meeting[2];
        meetingsByTime[time] ? meetingsByTime[time].push(meeting)
                             : meetingsByTime[time] = [meeting];
    }
    const meetingsTime = Array.from(Object.keys(meetingsByTime));
    meetingsTime.sort((a, b) => a - b);
    for (const time of meetingsTime) {
        const meetsWith = new DisjointSet()
        const curHolders = new Set();
        for (const [person1, person2, _] of meetingsByTime[time]) {
            meetsWith.makeSet(person1);
            meetsWith.makeSet(person2);
            meetsWith.union(person2, person1);
            if (knowSecret.has(person1)) {
                curHolders.add(person1);
            }
            if (knowSecret.has(person2)) {
                curHolders.add(person2);
            }
        }
        for (const secretHolder of curHolders) {
            meetsWith.findSet(secretHolder).forEach(h => knowSecret.add(h));
        }
    }
    return Array.from(knowSecret);
};

/* Optimized for add and union operations. */
class DisjointSet {
    constructor(name) {
        Object.defineProperty(this, 'parent', { value: {} });
    }
    makeSet(x) {
        if (this.parent[x] === undefined) {
            this.parent[x] = x;
        }
    }
    find(x) {
        if (x != this.parent[x]) {
            this.parent[x] = this.find(this.parent[x]);
        }
        return this.parent[x];
    }
    findSet(x) {
        const xParent = this.find(x);
        return Object.keys(this.parent).map(i => parseInt(i))
                                       .filter(i => this.find(i) == xParent);
    }
    union(x, y) {
        const px = this.find(x);
        const py = this.find(y);
        if (px !== undefined && py !== undefined && px != py) {
            this.parent[px] = py;
        }
    }
}

const tests = [
    [6, [[1,2,5],[2,3,8],[1,5,10]], 1, [0,1,2,3,5]],
    [4, [[3,1,3],[1,2,2],[0,3,3]], 3, [0,1,3]],
    [5, [[3,4,2],[1,2,1],[2,3,1]], 1, [0,1,2,3,4]],
    [6, [[0,2,1],[1,3,1],[4,5,1]], 1, [0,1,2,3]],
    [12, [[10,8,6],[9,5,11],[0,5,18],[4,5,13],[11,6,17],[0,11,10],[10,11,7],[5,8,3],[7,6,16],[3,6,10],[3,11,1],[8,3,2],[5,0,7],[3,8,20],[11,0,20],[8,3,4],[1,9,4],[10,7,11],[8,10,18]], 9, [0,1,4,5,6,9,11]],
];

for (const [n, meetings, firstPerson, expected] of tests) {
    test: {
        for (const p of findAllPeople(n, meetings, firstPerson)) {
            const i = expected.indexOf(p);
            if (i > -1) {
                expected.splice(i, 1);
            } else {
                console.log(false);
                break test;
            }
        }
        console.log(expected.length == 0);
    }
}
