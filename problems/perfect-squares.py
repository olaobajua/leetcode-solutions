"""
 * 279. Perfect Squares [Medium]
 * Given an integer n, return the least number of perfect square numbers that
 * sum to n.
 *
 * A perfect square is an integer that is the square of an integer; in other
 * words, it is the product of some integer with itself. For example, 1, 4, 9,
 * and 16 are perfect squares while 3 and 11 are not.
 *
 * Example 1:
 * Input: n = 12
 * Output: 3
 * Explanation: 12 = 4 + 4 + 4.
 *
 * Example 2:
 * Input: n = 13
 * Output: 2
 * Explanation: 13 = 4 + 9.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁴
"""
class Solution:
    def numSquares(self, n: int) -> int:
        def is_square(x):
            return x**0.5 % 1 == 0

        # Lagrange's four-square theorem states that every natural number can
        # be represented as the sum of four integer squares.
        # https://en.wikipedia.org/wiki/Lagrange%27s_four-square_theorem
        if is_square(n):
            return 1
        # Legendre's three-square theorem states that a natural number can be
        # represented as the sum of three squares of integers n = x² + y² + z²
        # if and only if n is not of the form n = 4ᵃ(8b + 7) for nonnegative
        # integers a and b
        # https://en.wikipedia.org/wiki/Legendre%27s_three-square_theorem
        while (n & 3) == 0:
            n >>= 2
        if (n & 7) == 7:
            return 4
        for i in range(1, int(n**0.5) + 1):
            if is_square(n - i**2):
                return 2
        return 3

if __name__ == "__main__":
    tests = (
        (12, 3),
        (13, 2),
        (28, 4),
        (4869, 2),
        (8609, 2),
        (8610, 3),
        (8935, 4),
    )
    for n, expected in tests:
        print(Solution().numSquares(n) == expected)
