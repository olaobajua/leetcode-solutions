"""
 * 653. Two Sum IV - Input is a BST [Easy]
 * Given the root of a Binary Search Tree and a target number k, return true
 * if there exist two elements in the BST such that their sum is equal to the
 * given target.
 *
 * Example 1:
 * Input: root = [5,3,6,2,4,null,7], k = 9
 * Output: true
 *
 * Example 2:
 * Input: root = [5,3,6,2,4,null,7], k = 28
 * Output: false
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ -10⁴ <= Node.val <= 10⁴
 *     ∙ root is guaranteed to be a valid binary search tree.
 *     ∙ -10⁵ <= k <= 10⁵
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def findTarget(self, root: Optional[TreeNode], k: int) -> bool:
        def preorder(node):
            if node:
                if find(root, k - node.val) not in (None, node):
                    return True
                if preorder(node.left) or preorder(node.right):
                    return True
            return False

        def find(node, val):
            if node:
                if val > node.val:
                    return find(node.right, val)
                elif val < node.val:
                    return find(node.left, val)
                else:
                    return node
        return preorder(root)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([5,3,6,2,4,None,7], 9, True),
        ([5,3,6,2,4,None,7], 28, False),
        ([1], 2, False),
        ([2,1,3], 4, True),
    )
    for root, k, expected in tests:
        print(Solution().findTarget(build_tree(root), k) == expected)
