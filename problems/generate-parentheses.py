"""
 * 22. Generate Parentheses [Medium]
 * Given n pairs of parentheses, write a function to generate all combinations
 * of well-formed parentheses.
 *
 * Example 1:
 * Input: n = 3
 * Output: ["((()))","(()())","(())()","()(())","()()()"]
 * Example 2:
 * Input: n = 1
 * Output: ["()"]
 *
 * Constraints:
 *     ∙ 1 <= n <= 8
"""
from typing import List

class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        def backtrack(s='', left=0, right=0):
            if len(s) == 2 * n:
                ret.append(s)
                return
            if left < n:
                backtrack(s + '(', left + 1, right)
            if right < left:
                backtrack(s + ')', left, right + 1)

        ret = []
        backtrack()
        return ret


if __name__ == "__main__":
    tests = (
        (3, ["((()))","(()())","(())()","()(())","()()()"]),
        (1, ["()"]),
    )
    for n, expected in tests:
        print(Solution().generateParenthesis(n) == expected)
