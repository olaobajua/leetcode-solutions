/**
 * 1446. Consecutive Characters (Easy)
 * The power of the string is the maximum length of a non-empty substring that
 * contains only one unique character.
 *
 * Given a string s, return the power of s.
 *
 * Example 1:
 * Input: s = "leetcode"
 * Output: 2
 * Explanation: The substring "ee" is of length 2 with the character 'e' only.
 *
 * Example 2:
 * Input: s = "abbcccddddeeeeedcba"
 * Output: 5
 * Explanation: The substring "eeeee" is of length 5 with the character 'e'
 * only.
 *
 * Example 3:
 * Input: s = "triplepillooooow"
 * Output: 5
 *
 * Example 4:
 * Input: s = "hooraaaaaaaaaaay"
 * Output: 11
 *
 * Example 5:
 * Input: s = "tourist"
 * Output: 1
 *
 * Constraints:
 *     1 <= s.length <= 500
 *     s consists of only lowercase English letters.
 */

/**
 * @param {string} s
 * @return {number}
 */
var maxPower = function(s) {
    return 1 + Math.max(...Array.from(s.slice(1))
                           .map((l, i) => l == s[i])
                           .map((p => e => p = (p + e)*e)(0)), 0);
};

const tests = [
    ["leetcode", 2],
    ["abbcccddddeeeeedcba", 5],
    ["triplepillooooow", 5],
    ["hooraaaaaaaaaaay", 11],
    ["tourist", 1],
    ["j", 1],
];

for (const [s, expected] of tests) {
    console.log(maxPower(s) == expected);
}
