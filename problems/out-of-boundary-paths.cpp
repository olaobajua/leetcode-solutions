/**
 * 576. Out of Boundary Paths [Medium]
 * There is an m x n grid with a ball. The ball is initially at the position
 * [startRow, startColumn]. You are allowed to move the ball to one of the four
 * adjacent cells in the grid (possibly out of the grid crossing the grid
 * boundary). You can apply at most maxMove moves to the ball.
 *
 * Given the five integers m, n, maxMove, startRow, startColumn, return the
 * number of paths to move the ball out of the grid boundary. Since the answer
 * can be very large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: m = 2, n = 2, maxMove = 2, startRow = 0, startColumn = 0
 * Output: 6
 *
 * Example 2:
 * Input: m = 1, n = 3, maxMove = 3, startRow = 0, startColumn = 1
 * Output: 12
 *
 * Constraints:
 *     ∙ 1 <= m, n <= 50
 *     ∙ 0 <= maxMove <= 50
 *     ∙ 0 <= startRow < m
 *     ∙ 0 <= startColumn < n
 */
class Solution {
    static const int MOD = 1000000007;
    vector<vector<vector<int>>> cache;
    int m, n;
public:
    int findPaths(int m, int n, int maxMove, int startRow, int startColumn) {
        this->m = m;
        this->n = n;
        cache = vector<vector<vector<int>>>(
                m, vector<vector<int>>(n, vector<int>(maxMove + 1, -1)));
        return dfs(startRow, startColumn, maxMove);
    }
    int dfs(int r, int c, int moves_left) {
        if (cache[r][c][moves_left] == -1) {
            cache[r][c][moves_left] = 0;
            if (moves_left) {
                long ret = 0;
                ret += (c + 1 < n) ? dfs(r, c + 1, moves_left - 1) : 1;
                ret += (r + 1 < m) ? dfs(r + 1, c, moves_left - 1) : 1;
                ret += (0 <= c - 1) ? dfs(r, c - 1, moves_left - 1) : 1;
                ret += (0 <= r - 1) ? dfs(r - 1, c, moves_left - 1) : 1;
                cache[r][c][moves_left] = ret % MOD;
            }
        }
        return cache[r][c][moves_left];
    }
};
