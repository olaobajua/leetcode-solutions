"""
 * 530. Minimum Absolute Difference in BST [Easy]
 * Given the root of a Binary Search Tree (BST), return the minimum absolute
 * difference between the values of any two different nodes in the tree.
 *
 * Example 1:
 *    4
 *  2   6
 * 1 3
 * Input: root = [4,2,6,1,3]
 * Output: 1
 *
 * Example 2:
 *    1
 * 0    48
 *    12  49
 * Input: root = [1,0,48,null,null,12,49]
 * Output: 1
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [2, 10⁴].
 *     ∙ 0 <= Node.val <= 10⁵
 *
 * Note: This question is the same as 783:
 * https://leetcode.com/problems/minimum-distance-between-bst-nodes/
 * (https://leetcode.com/problems/minimum-distance-between-bst-nodes/)
"""
from math import inf
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def getMinimumDifference(self, root: Optional[TreeNode]) -> int:
        def inorder_traversal(node):
            if node.left:
                yield from inorder_traversal(node.left)
            yield node.val
            if node.right:
                yield from inorder_traversal(node.right)

        ret = inf
        prev = -inf
        for x in inorder_traversal(root):
            ret = min(ret, x - prev)
            prev = x
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([4,2,6,1,3], 1),
        ([1,0,48,None,None,12,49], 1),
        ([236,104,701,None,227,None,911], 9),
    )
    for nums, expected in tests:
        print(Solution().getMinimumDifference(build_tree(nums)) == expected)
