"""
 * 82. Remove Duplicates from Sorted List II (Medium)
 * Given the head of a sorted linked list, delete all nodes that have duplicate
 * numbers, leaving only distinct numbers from the original list. Return the
 * linked list sorted as well.
 *
 * Example 1:
 * Input: head = [1,2,3,3,4,4,5]
 * Output: [1,2,5]
 *
 * Example 2:
 * Input: head = [1,1,1,2,3]
 * Output: [2,3]
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 300].
 *     ∙ -100 <= Node.val <= 100
 *     ∙ The list is guaranteed to be sorted in ascending order.
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def deleteDuplicates(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if head and head.next and head.val != head.next.val:
            head.next = self.deleteDuplicates(head.next)
            return head
        if head and not head.next:
            return head
        while head and head.next and head.val == head.next.val:
            head = head.next
        return self.deleteDuplicates(head.next) if head else None

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,2,3,3,4,4,5], [1,2,5]),
        ([1,1,1,2,3], [2,3]),
        ([1,2,2], [1]),
    )
    for nums, expected in tests:
        root = list_to_linked_list(nums)
        print(linked_list_to_list(Solution().deleteDuplicates(root)) ==
              expected)
