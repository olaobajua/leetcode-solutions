"""
 * 864. Shortest Path to Get All Keys [Hard]
 * You are given an m x n grid grid where:
 *     ∙ '.' is an empty cell.
 *     ∙ '#' is a wall.
 *     ∙ '@' is the starting point.
 *     ∙ Lowercase letters represent keys.
 *     ∙ Uppercase letters represent locks.
 *
 * You start at the starting point and one move consists of walking one space
 * in one of the four cardinal directions. You cannot walk outside the grid, or
 * walk into a wall.
 *
 * If you walk over a key, you can pick it up and you cannot walk over a lock
 * unless you have its corresponding key.
 *
 * For some 1 <= k <= 6, there is exactly one lowercase and one uppercase
 * letter of the first k letters of the English alphabet in the grid. This
 * means that there is exactly one key for each lock, and one lock for each
 * key; and also that the letters used to represent the keys and locks were
 * chosen in the same order as the English alphabet.
 *
 * Return the lowest number of moves to acquire all keys. If it is impossible,
 * return -1.
 *
 * Example 1:
 * Input: grid = ["@.a..","###.#","b.A.B"]
 * Output: 8
 * Explanation: Note that the goal is to obtain all the keys not to open all
 * the locks.
 *
 * Example 2:
 * Input: grid = ["@..aA","..B#.","....b"]
 * Output: 6
 *
 * Example 3:
 * Input: grid = ["@Aa"]
 * Output: -1
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 30
 *     ∙ grid[i][j] is either an English letter, '.', '#', or '@'.
 *     ∙ The number of keys in the grid is in the range [1, 6].
 *     ∙ Each key in the grid is unique.
 *     ∙ Each key in the grid has a matching lock.
"""
from typing import List

class Solution:
    def shortestPathAllKeys(self, grid: List[str]) -> int:
        POSSIBLE_KEYS = set("abcdef")
        POSSIBLE_DOORS = set("ABCDEF")
        m = len(grid)
        n = len(grid[0])
        all_keys = set()
        all_doors = set()
        for row in range(m):
            for col in range(n):
                if grid[row][col] == "@":
                    start = [row, col]
                    grid[row] = grid[row].replace("@", ".")
                elif grid[row][col] in POSSIBLE_KEYS:
                    all_keys.add(grid[row][col])
                elif grid[row][col] in POSSIBLE_DOORS:
                    all_doors.add(grid[row][col])

        to_visit = [(*start, 0, frozenset())]
        visited = set()
        for r, c, steps, keys in to_visit:
            if keys == all_keys:
                return steps
            if (r, c, keys) not in visited:
                visited.add((r, c, keys))
                steps += 1
                for nr, nc in (r - 1, c), (r + 1, c), (r, c - 1), (r, c + 1):
                    if not (0 <= nr < m and 0 <= nc < n):
                        continue
                    next_cell = grid[nr][nc]
                    if next_cell == ".":
                        to_visit.append((nr, nc, steps, keys))
                    elif next_cell in all_doors and next_cell.lower() in keys:
                        to_visit.append((nr, nc, steps, keys))
                    elif next_cell in all_keys:
                        to_visit.append((nr, nc, steps, keys | {next_cell}))
        return -1

if __name__ == "__main__":
    tests = (
        (["@.a..",
          "###.#",
          "b.A.B"], 8),
        (["@..aA",
          "..B#.",
          "....b"], 6),
        (["@Aa"], -1),
    )
    for grid, expected in tests:
        print(Solution().shortestPathAllKeys(grid) == expected)
