/**
 * 229. Majority Element II [Medium]
 * Given an integer array of size n, find all elements that appear more than
 * ⌊ n/3 ⌋ times.
 *
 * Example 1:
 * Input: nums = [3,2,3]
 * Output: [3]
 *
 * Example 2:
 * Input: nums = [1]
 * Output: [1]
 *
 * Example 3:
 * Input: nums = [1,2]
 * Output: [1,2]
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 5 * 10⁴
 *     ∙ -10⁹ <= nums[i] <= 10⁹
 *
 * Follow up: Could you solve the problem in linear time and in O(1) space?
 */
#include <vector>
#include <unordered_map>

class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
        int n = nums.size();
        std::unordered_map<int, int> count;

        for (int num : nums) {
            count[num]++;
        }

        std::vector<int> result;
        for (const auto& entry : count) {
            if (entry.second > n / 3) {
                result.push_back(entry.first);
            }
        }

        return result;
    }
};
