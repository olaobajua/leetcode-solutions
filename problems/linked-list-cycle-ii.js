/**
 * 142. Linked List Cycle II (Medium)
 * Given the head of a linked list, return the node where the cycle begins.
 * If there is no cycle, return null.
 *
 * There is a cycle in a linked list if there is some node in the list that can
 * be reached again by continuously following the next pointer. Internally, pos
 * is used to denote the index of the node that tail's next pointer is
 * connected to (0-indexed). It is -1 if there is no cycle. Note that pos is
 * not passed as a parameter.
 *
 * Do not modify the linked list.
 *
 * Example 1:
 * Input: head = [3,2,0,-4], pos = 1
 * Output: tail connects to node index 1
 * Explanation: There is a cycle in the linked list, where tail connects to
 * the second node.
 *
 * Example 2:
 * Input: head = [1,2], pos = 0
 * Output: tail connects to node index 0
 * Explanation: There is a cycle in the linked list, where tail connects to
 * the first node.
 *
 * Example 3:
 * Input: head = [1], pos = -1
 * Output: no cycle
 * Explanation: There is no cycle in the linked list.
 *
 * Constraints:
 *     ∙ The number of the nodes in the list is in the range [0, 10⁴].
 *     ∙ -10⁵ <= Node.val <= 10⁵
 *     ∙ pos is -1 or a valid index in the linked-list.
 *
 * Follow up: Can you solve it using O(1) (i.e. constant) memory?
 */

// Definition for singly-linked list.
function ListNode(val) {
    this.val = val;
    this.next = null;
}

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var detectCycle = function(head) {
    let slow = head, fast = head;
    while (fast && fast.next) {
        fast = fast.next.next;
        slow = slow.next;
        if (slow == fast) {
            break;
        }
    }
    if (!fast || !fast.next) {
        return null;  // end of list reached, thus no cycle
    }
    // L1 - distance between head and cycle entry point
    // L2 - distance between entry point and meeting point
    // C - circumference
    // n - times fast pointer travelled around loop until encountering slow
    // total distance slow pointer traveled before encounter is L1 + L2
    // total distance fast pointer traveled before encounter is L1 + L2 + n*C
    // 2*(L1 + L2) = L1 + L2 + n*C
    // L1 + L2 = n*C
    // L1 = (n - 1)*C + (C - L2)
    // L1 = C - L2
    // so if fast pointer will start moving from meeting point and slow
    // pointer - from the head with same speed, fast pointer will finish cycle
    // exactly when slow pointer will reach entry point and they'll meet there
    slow = head;
    while (slow != fast) {
        slow = slow.next;
        fast = fast.next;
    }
    return slow;
};

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

const tests = [
    [[3,2,0,-4], 1],
    [[1,2], 0],
    [[1], -1],
];

for (const [nums, pos] of tests) {
    const head = arrayToLinkedList(nums);
    let expected = head, tail = head;
    if (pos == -1) {
        expected == null;
    } else {
        for (let i = 0; i < pos; ++i) {
            expected = expected.next;
        }
    }
    while (tail && tail.next) {
        tail = tail.next;
    }
    if (tail) {
        tail.next = expected;
    }
    console.log(detectCycle(head) == expected);
}
