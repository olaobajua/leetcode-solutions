"""
 * 1802. Maximum Value at a Given Index in a Bounded Array [Medium]
 * You are given three positive integers: n, index, and maxSum. You want to
 * construct an array nums (0-indexed) that satisfies the following conditions:
 *     ∙ nums.length == n
 *     ∙ nums[i] is a positive integer where 0 <= i < n.
 *     ∙ abs(nums[i] - nums[i+1]) <= 1 where 0 <= i < n-1.
 *     ∙ The sum of all the elements of nums does not exceed maxSum.
 *     ∙ nums[index] is maximized.
 *
 * Return nums[index] of the constructed array.
 *
 * Note that abs(x) equals x if x >= 0, and -x otherwise.
 *
 * Example 1:
 * Input: n = 4, index = 2,  maxSum = 6
 * Output: 2
 * Explanation: nums = [1,2,2,1] is one array that satisfies all the
 * conditions.
 * There are no arrays that satisfy all the conditions and have nums[2] == 3,
 * so 2 is the maximum nums[2].
 *
 * Example 2:
 * Input: n = 6, index = 1,  maxSum = 10
 * Output: 3
 *
 * Constraints:
 *     ∙ 1 <= n <= maxSum <= 10⁹
 *     ∙ 0 <= index < n
"""
class Solution:
    def maxValue(self, n: int, index: int, maxSum: int) -> int:
        def get_sum(x):
            ret = n

            # S = (a₁ + aₙ) * n / 2
            left = max(x - index, 0)
            left_count = x - left + 1
            ret += (left + x) * left_count // 2

            right = max(x - (n - index - 1), 0)
            right_count = x - right + 1
            ret += (right + x) * right_count // 2 - x

            return ret

        lo = 0
        hi = maxSum
        while lo < hi:
            mid = (lo + hi) // 2
            if get_sum(mid) > maxSum:
                hi = mid
            else:
                lo = mid + 1
        return lo

if __name__ == "__main__":
    tests = (
        (4, 2, 6, 2),
        (6, 1, 10, 3),
        (3, 2, 18, 7),
        (3, 0, 815094800, 271698267),
    )
    for n, index, maxSum, expected in tests:
        print(Solution().maxValue(n, index, maxSum) == expected)
