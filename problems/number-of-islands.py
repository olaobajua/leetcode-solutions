"""
 * 200. Number of Islands [Medium]
 * Given an m x n 2D binary grid grid which represents a map of '1's (land)
 * and '0's (water), return the number of islands.
 *
 * An island is surrounded by water and is formed by connecting adjacent lands
 * horizontally or vertically. You may assume all four edges of the grid are
 * all surrounded by water.
 *
 * Example 1:
 * Input: grid = [
 *   ["1","1","1","1","0"],
 *   ["1","1","0","1","0"],
 *   ["1","1","0","0","0"],
 *   ["0","0","0","0","0"]
 * ]
 * Output: 1
 *
 * Example 2:
 * Input: grid = [
 *   ["1","1","0","0","0"],
 *   ["1","1","0","0","0"],
 *   ["0","0","1","0","0"],
 *   ["0","0","0","1","1"]
 * ]
 * Output: 3
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 300
 *     ∙ grid[i][j] is '0' or '1'.
"""
from typing import List

class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        ret = 0
        for row in range(len(grid)):
            for col in range(len(grid[0])):
                if grid[row][col] == "1":
                    ret += 1
                    flood_fill_dfs(grid, row, col, "1", "0")
        return ret

def flood_fill_dfs(matrix, row, col, old, new):
    if matrix[row][col] == old:
        matrix[row][col] = new
        if row > 0:
            flood_fill_dfs(matrix, row - 1, col, old, new)
        if row < len(matrix) - 1:
            flood_fill_dfs(matrix, row + 1, col, old, new)
        if col > 0:
            flood_fill_dfs(matrix, row, col - 1, old, new)
        if col < len(matrix[row]) - 1:
            flood_fill_dfs(matrix, row, col + 1, old, new)

if __name__ == "__main__":
    tests = (
        ([["1","1","1","1","0"],
          ["1","1","0","1","0"],
          ["1","1","0","0","0"],
          ["0","0","0","0","0"]], 1),
        ([["1","1","0","0","0"],
          ["1","1","0","0","0"],
          ["0","0","1","0","0"],
          ["0","0","0","1","1"]], 3),
    )
    for grid, expected in tests:
        print(Solution().numIslands(grid) == expected)
