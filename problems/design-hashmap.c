/**
 * 706. Design HashMap (Easy)
 * Design a HashMap without using any built-in hash table libraries.
 *
 * Implement the MyHashMap class:
 *     ∙ MyHashMap() initializes the object with an empty map.
 *     ∙ void put(int key, int value) inserts a (key, value) pair into the
 *       HashMap. If the key already exists in the map, update the
 *       corresponding value.
 *     ∙ int get(int key) returns the value to which the specified key is
 *       mapped, or -1 if this map contains no mapping for the key.
 *     ∙ void remove(key) removes the key and its corresponding value if the
 *       map contains the mapping for the key.
 *
 * Example 1:
 * Input
 * ["MyHashMap", "put", "put", "get", "get", "put", "get", "remove", "get"]
 * [[], [1, 1], [2, 2], [1], [3], [2, 1], [2], [2], [2]]
 * Output
 * [null, null, null, 1, -1, null, 1, null, -1]
 *
 * Explanation
 * MyHashMap myHashMap = new MyHashMap();
 * myHashMap.put(1, 1); // The map is now [[1,1]]
 * myHashMap.put(2, 2); // The map is now [[1,1], [2,2]]
 * myHashMap.get(1);    // return 1, The map is now [[1,1], [2,2]]
 * myHashMap.get(3);    // return -1 (i.e., not found),
 *                      // The map is now [[1,1], [2,2]]
 * myHashMap.put(2, 1); // The map is now [[1,1], [2,1]]
 *                      // (i.e., update the existing value)
 * myHashMap.get(2);    // return 1, The map is now [[1,1], [2,1]]
 * myHashMap.remove(2); // remove the mapping for 2, The map is now [[1,1]]
 * myHashMap.get(2);    // return -1 (i.e., not found), The map is now [[1,1]]
 *
 * Constraints:
 *     ∙ 0 <= key, value <= 10⁶
 *     ∙ At most 10⁴ calls will be made to put, get, and remove.
 */
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define SIZE 1 << 15

typedef struct MyHashMap {
    int key;
    int value;
    struct MyHashMap *next;
} MyHashMap;

int hash(long key) {
    return ((key * 39916801) & ((1 << 20) - 1)) >> 5;
}

MyHashMap* myHashMapCreate() {
    return calloc(SIZE, sizeof(MyHashMap));
}

void myHashMapPut(MyHashMap* obj, int key, int value) {
    int index = hash(key);
    for (MyHashMap *cur = obj[index].next; cur; cur = cur->next) {
        if (cur->key == key) {
            cur->value = value;
            return;
        }
    }
    MyHashMap *new_node = calloc(1, sizeof(MyHashMap));
    new_node->key = key;
    new_node->value = value;
    new_node->next = obj[index].next;
    obj[index].next = new_node;
}

int myHashMapGet(MyHashMap* obj, int key) {
    for (MyHashMap *cur = obj[hash(key)].next; cur; cur = cur->next) {
        if (cur->key == key) {
            return cur->value;
        }
    }
    return -1;
}

void myHashMapRemove(MyHashMap* obj, int key) {
    for (MyHashMap **cur = &obj[hash(key)].next; *cur; cur = &(*cur)->next) {
        if ((*cur)->key == key) {
            MyHashMap *del = *cur;
            *cur = (*cur)->next;
            free(del);
            return;
        }
    }
}

void myHashMapFree(MyHashMap* obj) {
    for (int i = 0; i < SIZE; ++i) {
        MyHashMap *cur = obj[i].next;
        while (cur) {
            MyHashMap *del = cur;
            cur = cur->next;
            free(del);
        }
    }
    free(obj);
}

int main() {
    MyHashMap* obj = myHashMapCreate();
    myHashMapPut(obj, 1, 1);
    myHashMapPut(obj, 2, 2);
    printf("%s\n", 1 == myHashMapGet(obj, 1) ? "true" : "false");
    printf("%s\n", -1 == myHashMapGet(obj, 3) ? "true" : "false");
    myHashMapPut(obj, 2, 1);
    printf("%s\n", 1 == myHashMapGet(obj, 2) ? "true" : "false");
    myHashMapRemove(obj, 2);
    printf("%s\n", -1 == myHashMapGet(obj, 2) ? "true" : "false");
    myHashMapFree(obj);
}
