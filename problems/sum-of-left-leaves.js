/**
 * 404. Sum of Left Leaves (Easy)
 * Given the root of a binary tree, return the sum of all left leaves.
 *
 * Example 1:
 * Input: root = [3,9,20,null,null,15,7]
 * Output: 24
 * Explanation: There are two left leaves in the binary tree, with values 9 and
 * 15 respectively.
 *
 * Example 2:
 * Input: root = [1]
 * Output: 0
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [1, 1000].
 *     -1000 <= Node.val <= 1000
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @return {number}
 */
var sumOfLeftLeaves = function(root) {
    function dfs(root, left) {
        if (!root) {
            return 0;
        }
        if (!root.left && !root.right && left) {
            return root.val;
        }
        return dfs(root.left, true) + dfs(root.right, false);
    }
    return dfs(root, false);
};

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

const tests = [
    [[3,9,20,null,null,15,7], 24],
    [[1], 0],
    [[1,2,3,4,5], 4],
];

for (let [nums, expected] of tests) {
    console.log(sumOfLeftLeaves(buildTree(nums)) == expected);
}
