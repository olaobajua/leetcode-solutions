"""
 * 310. Minimum Height Trees (Medium)
 * A tree is an undirected graph in which any two vertices are connected by
 * exactly one path. In other words, any connected graph without simple cycles
 * is a tree.
 *
 * Given a tree of n nodes labelled from 0 to n - 1, and an array of n - 1
 * edges where edges[i] = [aᵢ, bᵢ] indicates that there is an undirected edge
 * between the two nodes aᵢ and bᵢ in the tree, you can choose any node of the
 * tree as the root. When you select a node x as the root, the result tree has
 * height h. Among all possible rooted trees, those with minimum height
 * (i.e. min(h))  are called minimum height trees (MHTs).
 *
 * Return a list of all MHTs' root labels. You can return the answer in any
 * order.
 *
 * The height of a rooted tree is the number of edges on the longest downward
 * path between the root and a leaf.
 *
 * Example 1:
 * Input: n = 4, edges = [[1,0],[1,2],[1,3]]
 * Output: [1]
 * Explanation: As shown, the height of the tree is 1 when the root is the node
 * with label 1 which is the only MHT.
 *
 * Example 2:
 * Input: n = 6, edges = [[3,0],[3,1],[3,2],[3,4],[5,4]]
 * Output: [3,4]
 *
 * Example 3:
 * Input: n = 1, edges = []
 * Output: [0]
 *
 * Example 4:
 * Input: n = 2, edges = [[0,1]]
 * Output: [0,1]
 *
 * Constraints:
 *     ∙ 1 <= n <= 2 * 10⁴
 *     ∙ edges.length == n - 1
 *     ∙ 0 <= aᵢ, bᵢ < n
 *     ∙ aᵢ != bᵢ
 *     ∙ All the pairs (aᵢ, bᵢ) are distinct.
 *     ∙ The given input is guaranteed to be a tree and there will be no
 *       repeated edges.
"""
from collections import defaultdict
from functools import cache
from typing import List

class Solution:
    def findMinHeightTrees(self, n: int, edges: List[List[int]]) -> List[int]:
        @cache
        def height(root, prev=None):
            return 1 + max([height(child, root) for child in neighbours[root]
                            if child != prev], default=0)

        neighbours = defaultdict(set)
        for n1, n2 in edges:
            neighbours[n1].add(n2)
            neighbours[n2].add(n1)
        ret = []
        heights = defaultdict(list)
        for i in range(n):
            heights[height(i)].append(i)
        return heights[min(heights)]

if __name__ == "__main__":
    tests = (
        (1, [], [0]),
        (2, [[0,1]], [0,1]),
        (3, [[0,1],[0,2]], [0]),
        (4, [[1,0],[1,2],[1,3]], [1]),
        (6, [[0,1],[0,2],[0,3],[3,4],[4,5]], [3]),
        (6, [[3,0],[3,1],[3,2],[3,4],[5,4]], [3,4]),
        (11, [[0,1],[0,2],[2,3],[0,4],[2,5],[5,6],[3,7],[6,8],[8,9],[9,10]], [5,6]),
    )
    for n, edges, expected in tests:
        print(Solution().findMinHeightTrees(n, edges) == expected)
