"""
 * 17. Letter Combinations of a Phone Number (Medium)
 * Given a string containing digits from 2-9 inclusive, return all possible
 * letter combinations that the number could represent. Return the answer in
 * any order.
 *
 * A mapping of digit to letters (just like on the telephone buttons) is given
 * below. Note that 1 does not map to any letters.
 *
 * Example 1:
 * Input: digits = "23"
 * Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]
 *
 * Example 2:
 * Input: digits = ""
 * Output: []
 *
 * Example 3:
 * Input: digits = "2"
 * Output: ["a","b","c"]
 *
 * Constraints:
 *     ∙ 0 <= digits.length <= 4
 *     ∙ digits[i] is a digit in the range ['2', '9'].
"""
from functools import reduce
from typing import List

class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        def append_letters(comboes, digit):
            return [combo + l for combo in comboes for l in LETTERS[digit]]
        if not digits:
            return []
        LETTERS = {
            '2': "abc",
            '3': "def",
            '4': "ghi",
            '5': "jkl",
            '6': "mno",
            '7': "pqrs",
            '8': "tuv",
            '9': "wxyz",
        }
        return reduce(append_letters, digits, [''])

if __name__ == "__main__":
    tests = (
        ("23", ["ad","ae","af","bd","be","bf","cd","ce","cf"]),
        ("", []),
        ("2", ["a","b","c"]),
    )
    for digits, expected in tests:
        print(Solution().letterCombinations(digits) == expected)
