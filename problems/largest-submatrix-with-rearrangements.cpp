/**
 * 1727. Largest Submatrix With Rearrangements [Medium]
 * You are given a binary matrix matrix of size m x n, and you are allowed to
 * rearrange the columns of the matrix in any order.
 *
 * Return the area of the largest submatrix within matrix where every element
 * of the submatrix is 1 after reordering the columns optimally.
 *
 * Example 1:
 * Input: matrix = [[0,0,1],
 *                  [1,1,1],
 *                  [1,0,1]]
 * Output: 4
 * Explanation: You can rearrange the columns as shown above.
 * The largest submatrix of 1s, in bold, has an area of 4.
 *
 * Example 2:
 * Input: matrix = [[1,0,1,0,1]]
 * Output: 3
 * Explanation: You can rearrange the columns as shown above.
 * The largest submatrix of 1s, in bold, has an area of 3.
 *
 * Example 3:
 * Input: matrix = [[1,1,0],
 *                  [1,0,1]]
 * Output: 2
 * Explanation: Notice that you must rearrange entire columns, and there is no
 * way to make a submatrix of 1s larger than an area of 2.
 *
 * Constraints:
 *     ∙ m == matrix.length
 *     ∙ n == matrix[i].length
 *     ∙ 1 <= m * n <= 10⁵
 *     ∙ matrix[i][j] is either 0 or 1.
 */
#include <vector>
#include <algorithm>

class Solution {
public:
    int largestSubmatrix(vector<vector<int>>& matrix) {
        int m = matrix.size();
        int n = matrix[0].size();

        std::vector<std::vector<int>> columns;
        for (int col = 0; col < n; ++col) {
            std::vector<int> cur_column;
            int count = 0;
            for (int row = 0; row < m; ++row) {
                (matrix[row][col] == 1) ? ++count : count = 0;
                cur_column.push_back(count);
            }
            columns.push_back(cur_column);
        }

        std::vector<std::vector<int>> rows;
        for (int i = 0; i < m; ++i) {
            std::vector<int> row;
            for (int j = 0; j < n; ++j) {
                row.push_back(columns[j][i]);
            }
            std::sort(row.begin(), row.end(), std::less<int>());
            rows.push_back(row);
        }

        int ret = 0;
        for (const auto& row : rows) {
            for (int i = 0; i < n; ++i) {
                ret = std::max(ret, row[i] * (n - i));
            }
        }

        return ret;
    }
};
