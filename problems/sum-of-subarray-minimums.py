"""
 * 907. Sum of Subarray Minimums [Medium]
 * Given an array of integers arr, find the sum of min(b), where b ranges over
 * every (contiguous) subarray of arr. Since the answer may be large, return
 * the answer modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: arr = [3,1,2,4]
 * Output: 17
 * Explanation:
 * Subarrays are [3], [1], [2], [4], [3,1], [1,2], [2,4], [3,1,2], [1,2,4],
 * [3,1,2,4].
 * Minimums are 3, 1, 2, 4, 1, 1, 2, 1, 1, 1.
 * Sum is 17.
 *
 * Example 2:
 * Input: arr = [11,81,94,43,3]
 * Output: 444
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 3 * 10⁴
 *     ∙ 1 <= arr[i] <= 3 * 10⁴
"""
from typing import List

class Solution:
    def sumSubarrayMins(self, arr: List[int]) -> int:
        MOD = 1000000007
        arr = [0] + arr
        dp = [0] * len(arr)
        indices = [0]
        for i, x in enumerate(arr):
            while arr[indices[-1]] > x:
                indices.pop()
            j = indices[-1]
            dp[i] = dp[j] + (i - j) * x
            indices.append(i)
        return sum(dp) % MOD

if __name__ == "__main__":
    tests = (
        ([3,1,2,4], 17),
        ([11,81,94,43,3], 444),
    )
    for arr, expected in tests:
        print(Solution().sumSubarrayMins(arr) == expected)
