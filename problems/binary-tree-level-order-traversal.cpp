/**
 * 102. Binary Tree Level Order Traversal (Medium)
 * Given the root of a binary tree, return the level order traversal of its
 * nodes' values. (i.e., from left to right, level by level).
 *
 * Example 1:
 * Input: root = [3,9,20,null,null,15,7]
 * Output: [[3],[9,20],[15,7]]
 *
 * Example 2:
 * Input: root = [1]
 * Output: [[1]]
 *
 * Example 3:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 2000].
 *     ∙ -1000 <= Node.val <= 1000
 */
// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode *root) {
        vector<vector<int>> ret;
        vector<TreeNode*> level{root};
        while (level.size()) {
            vector<TreeNode*> next_level;
            vector<int> vals;
            for (auto n : level) {
                if (n) {
                    next_level.push_back(n->left);
                    next_level.push_back(n->right);
                    vals.push_back(n->val);
                }
            }
            ret.push_back(vals);
            level = next_level;
        }
        ret.pop_back();
        return ret;
    }
};
