"""
 * 1254. Number of Closed Islands [Medium]
 * Given a 2D grid consists of 0s (land) and 1s (water).  An island is a
 * maximal 4-directionally connected group of 0s and a closed island is an
 * island totally (all left, top, right, bottom) surrounded by 1s.
 *
 * Return the number of closed islands.
 *
 * Example 1:
 * Input: grid = [[1,1,1,1,1,1,1,0],
 *                [1,0,0,0,0,1,1,0],
 *                [1,0,1,0,1,1,1,0],
 *                [1,0,0,0,0,1,0,1],
 *                [1,1,1,1,1,1,1,0]]
 * Output: 2
 * Explanation:
 * Islands in gray are closed because they are completely surrounded by water
 * (group of 1s).
 *
 * Example 2:
 * Input: grid = [[0,0,1,0,0],
 *                [0,1,0,1,0],
 *                [0,1,1,1,0]]
 * Output: 1
 *
 * Example 3:
 * Input: grid = [[1,1,1,1,1,1,1],
 *                [1,0,0,0,0,0,1],
 *                [1,0,1,1,1,0,1],
 *                [1,0,1,0,1,0,1],
 *                [1,0,1,1,1,0,1],
 *                [1,0,0,0,0,0,1],
 *                [1,1,1,1,1,1,1]]
 * Output: 2
 *
 * Constraints:
 *     ∙ 1 <= grid.length, grid[0].length <= 100
 *     ∙ 0 <= grid[i][j] <=1
"""
from typing import List

class Solution:
    def closedIsland(self, grid: List[List[int]]) -> int:
        def is_closed(r, c):
            grid[r][c] = 2
            ret = True
            for nr, nc in (r+1, c), (r-1, c), (r, c+1), (r, c-1):
                if nr < 0 or nr == m or nc < 0 or nc == n:
                    ret = False
                elif grid[nr][nc] == 0:
                    if not is_closed(nr, nc):
                        ret = False
            return ret

        m = len(grid)
        n = len(grid[0])
        return sum(is_closed(row, col) for row in range(m) for col in range(n)
                   if grid[row][col] == 0)

if __name__ == "__main__":
    tests = (
        ([[1,1,1,1,1,1,1,0],
          [1,0,0,0,0,1,1,0],
          [1,0,1,0,1,1,1,0],
          [1,0,0,0,0,1,0,1],
          [1,1,1,1,1,1,1,0]], 2),
        ([[0,0,1,0,0],
          [0,1,0,1,0],
          [0,1,1,1,0]], 1),
        ([[1,1,1,1,1,1,1],
          [1,0,0,0,0,0,1],
          [1,0,1,1,1,0,1],
          [1,0,1,0,1,0,1],
          [1,0,1,1,1,0,1],
          [1,0,0,0,0,0,1],
          [1,1,1,1,1,1,1]], 2),
        ([[0,0,1,1,0,1,0,0,1,0],
          [1,1,0,1,1,0,1,1,1,0],
          [1,0,1,1,1,0,0,1,1,0],
          [0,1,1,0,0,0,0,1,0,1],
          [0,0,0,0,0,0,1,1,1,0],
          [0,1,0,1,0,1,0,1,1,1],
          [1,0,1,0,1,1,0,0,0,1],
          [1,1,1,1,1,1,0,0,0,0],
          [1,1,1,0,0,1,0,1,0,1],
          [1,1,1,0,1,1,0,1,1,0]], 5),
    )
    for grid, expected in tests:
        print(Solution().closedIsland(grid) == expected)
