"""
 * 382. Linked List Random Node [Medium]
 * Given a singly linked list, return a random node's value from the linked
 * list. Each node must have the same probability of being chosen.
 *
 * Implement the Solution class:
 *     ∙ Solution(ListNode head) Initializes the object with the head of the
 *       singly-linked list head.
 *     ∙ int getRandom() Chooses a node randomly from the list and returns its
 *       value. All the nodes of the list should be equally likely to be
 *       chosen.
 *
 * Example 1:
 * Input
 * ["Solution", "getRandom", "getRandom", "getRandom", "getRandom",
 * "getRandom"]
 * [[[1, 2, 3]], [], [], [], [], []]
 * Output
 * [null, 1, 3, 2, 2, 3]
 *
 * Explanation
 * Solution solution = new Solution([1, 2, 3]);
 * solution.getRandom(); // return 1
 * solution.getRandom(); // return 3
 * solution.getRandom(); // return 2
 * solution.getRandom(); // return 2
 * solution.getRandom(); // return 3
 * // getRandom() should return either 1, 2, or 3 randomly. Each element
 * should have equal probability of returning.
 *
 * Constraints:
 *     ∙ The number of nodes in the linked list will be in the range [1, 10⁴].
 *     ∙ -10⁴ <= Node.val <= 10⁴
 *     ∙ At most 10⁴ calls will be made to getRandom.
 *
 * Follow up:
 *     ∙ What if the linked list is extremely large and its length is unknown
 *       to you?
 *     ∙ Could you solve this efficiently without using extra space?
"""
import random
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def __init__(self, head: Optional[ListNode]):
        self.head = head

    def getRandom(self) -> int:
        # https://en.wikipedia.org/wiki/Reservoir_sampling
        scope = 1
        chosen = 0
        cur = self.head
        while cur:
            # decide whether to include the element in reservoir
            if random.random() < 1 / scope:
                chosen = cur.val
            cur = cur.next
            scope += 1
        return chosen

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

obj = Solution(list_to_linked_list([1,2,3]))
print(obj.getRandom())
print(obj.getRandom())
print(obj.getRandom())
print(obj.getRandom())
print(obj.getRandom())
