"""
 * 342. Power of Four [Easy]
 * Given an integer n, return true if it is a power of four. Otherwise, return
 * false.
 *
 * An integer n is a power of four, if there exists an integer x such that n
 * == 4ˣ.
 *
 * Example 1:
 * Input: n = 16
 * Output: true
 * Example 2:
 * Input: n = 5
 * Output: false
 * Example 3:
 * Input: n = 1
 * Output: true
 *
 * Constraints:
 *     ∙ -2³¹ <= n <= 2³¹ - 1
 *
 * Follow up: Could you solve it without loops/recursion?
"""
class Solution:
    def isPowerOfFour(self, n: int) -> bool:
        return n > 0 and n.bit_count() == 1 and n.bit_length() & 1

if __name__ == "__main__":
    tests = (
        (16, True),
        (5, False),
        (1, True),
        (-2147483648, False),
    )
    for n, expected in tests:
        print(Solution().isPowerOfFour(n) == expected)
