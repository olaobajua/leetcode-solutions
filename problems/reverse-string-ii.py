"""
 * 541. Reverse String II [Easy]
 * Given a string s and an integer k, reverse the first k characters for every
 * 2k characters counting from the start of the string.
 *
 * If there are fewer than k characters left, reverse all of them. If there
 * are less than 2k but greater than or equal to k characters, then reverse the
 * first k characters and leave the other as original.
 *
 * Example 1:
 * Input: s = "abcdefg", k = 2
 * Output: "bacdfeg"
 * Example 2:
 * Input: s = "abcd", k = 2
 * Output: "bacd"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁴
 *     ∙ s consists of only lowercase English letters.
 *     ∙ 1 <= k <= 10⁴
"""
class Solution:
    def reverseStr(self, s: str, k: int) -> str:
        ret = []
        reverse = True
        for i in range(0, len(s), k):
            sample = s[i:i+k]
            ret.append(sample[::-1] if reverse else sample)
            reverse = not reverse
        return "".join(ret)

if __name__ == "__main__":
    tests = (
        ("abcdefg", 2, "bacdfeg"),
        ("abcd", 2, "bacd"),
    )
    for s, k, expected in tests:
        print(Solution().reverseStr(s, k) == expected)
