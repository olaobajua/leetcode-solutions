"""
 * 148. Sort List (Medium)
 * Given the head of a linked list, return the list after sorting it in
 * ascending order.
 *
 * Example 1:
 * Input: head = [4,2,1,3]
 * Output: [1,2,3,4]
 *
 * Example 2:
 * Input: head = [-1,5,3,4,0]
 * Output: [-1,0,3,4,5]
 *
 * Example 3:
 * Input: head = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [0, 5 * 10⁴].
 *     ∙ -10⁵ <= Node.val <= 10⁵
 *
 * Follow up: Can you sort the linked list in O(n logn) time and O(1) memory
 * (i.e. constant space)?
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def __init__(self):
        self.tail = ListNode()
        self.nextSubList = ListNode()

    def sortList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if head is None or head.next is None:
            return head
        dummy = ListNode()
        n = getCount(head)
        start = head
        size = 1
        while size < n:
            self.tail = dummy
            while start:
                if start.next is None:
                    self.tail.next = start
                    break
                mid = self.split(start, size)
                self.merge(start, mid)
                start = self.nextSubList
            start = dummy.next
            size *= 2
        return dummy.next

    def split(self, start, size):
        midPrev = start
        end = start.next
        index = 1
        while index < size and (midPrev.next or end.next):
            if end.next:
                end = end.next.next if end.next.next else end.next
            if midPrev.next:
                midPrev = midPrev.next
            index += 1
        mid = midPrev.next
        midPrev.next = None
        self.nextSubList = end.next
        end.next = None
        return mid;

    def merge(self, list1, list2):
        dummy = ListNode()
        newTail = dummy
        while list1 and list2:
            if list1.val < list2.val:
                newTail.next, list1, newTail = list1, list1.next, list1
            else:
                newTail.next, list2, newTail = list2, list2.next, list2
        newTail.next = list1 if list1 else list2
        # traverse till the end of merged list to get the newTail
        while newTail.next:
            newTail = newTail.next
        # link the old tail with the head of merged list
        self.tail.next = dummy.next
        # update the old tail to the new tail of merged list
        self.tail = newTail

def getCount(head):
    count = 0
    while head:
        head = head.next
        count += 1
    return count

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([4,2,1,3], [1,2,3,4]),
        ([-1,5,3,4,0], [-1,0,3,4,5]),
        ([1], [1]),
        ([], []),
    )
    for nums, expected in tests:
        ret = Solution().sortList(list_to_linked_list(nums))
        print(linked_list_to_list(ret) == expected)
