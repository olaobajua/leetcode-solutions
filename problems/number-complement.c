/**
 * 476. Number Complement (Easy)
 * The complement of an integer is the integer you get when you flip all the
 * 0's to 1's and all the 1's to 0's in its binary representation.
 *     ∙ For example, The integer 5 is "101" in binary and its complement is
 *       "010" which is the integer 2.
 *
 * Given an integer num, return its complement.
 *
 * Example 1:
 * Input: num = 5
 * Output: 2
 * Explanation: The binary representation of 5 is 101 (no leading zero bits),
 * and its complement is 010. So you need to output 2.
 *
 * Example 2:
 * Input: num = 1
 * Output: 0
 * Explanation: The binary representation of 1 is 1 (no leading zero bits), and
 * its complement is 0. So you need to output 0.
 *
 * Constraints:
 *     ∙ 1 <= num < 2³¹
 *
 * Note: This question is the same as 1009:
 * https://leetcode.com/problems/complement-of-base-10-integer/
 */
#include <stdio.h>
#include <stdlib.h>

int findComplement(int num) {
    unsigned mask = ~0;
    while (num & mask) {
        mask <<= 1;
    }
    return ~mask & ~num;
}

int main() {
    const int tests[] = {5, 1, 4};
    const int expected[] = {2, 0, 3};
    const size_t n = sizeof(tests) / sizeof(tests[0]);

    for (int i = 0; i < n; ++i) {
        printf(findComplement(tests[i]) == expected[i] ? "true\n" : "false\n");
    }

    return EXIT_SUCCESS;
}
