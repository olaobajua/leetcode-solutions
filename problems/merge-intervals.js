/**
 * 56. Merge Intervals (Medium)
 * Given an array of intervals where intervals[i] = [startᵢ, endᵢ], merge all
 * overlapping intervals, and return an array of the non-overlapping intervals
 * that cover all the intervals in the input.
 *
 * Example 1:
 * Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
 * Output: [[1,6],[8,10],[15,18]]
 * Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
 *
 * Example 2:
 * Input: intervals = [[1,4],[4,5]]
 * Output: [[1,5]]
 * Explanation: Intervals [1,4] and [4,5] are considered overlapping.
 *
 * Constraints:
 *     1 <= intervals.length <= 10⁴
 *     intervals[i].length == 2
 *     0 <= startᵢ <= endᵢ <= 10⁴
 */

/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
var merge = function(intervals) {
    intervals.sort((a, b) => b[0] - a[0]);
    const ret = [];
    let curStart, curEnd, nextStart, nextEnd;
    [curStart, curEnd] = intervals.pop();
    while (intervals.length) {
        [nextStart, nextEnd] = intervals.pop();
        if (nextStart > curEnd) {
            ret.push([curStart, curEnd]);
            [curStart, curEnd] = [nextStart, nextEnd];
        } else {
            curEnd = Math.max(curEnd, nextEnd);
        }
    }
    ret.push([curStart, curEnd]);
    return ret;
};

const tests = [
    [[[1,3],[2,6],[8,10],[15,18]], [[1,6],[8,10],[15,18]]],
    [[[1,4],[4,5]], [[1,5]]],
    [[[1,4],[2,3]], [[1,4]]],
];

for (const [intervals, expected] of tests) {
    console.log(merge(intervals).toString() == expected.toString());
}
