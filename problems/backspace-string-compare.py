"""
 * 844. Backspace String Compare (Easy)
 * Given two strings s and t, return true if they are equal when both are typed
 * into empty text editors. '#' means a backspace character.
 *
 * Note that after backspacing an empty text, the text will continue empty.
 *
 * Example 1:
 * Input: s = "ab#c", t = "ad#c"
 * Output: true
 * Explanation: Both s and t become "ac".
 *
 * Example 2:
 * Input: s = "ab##", t = "c#d#"
 * Output: true
 * Explanation: Both s and t become "".
 *
 * Example 3:
 * Input: s = "a#c", t = "b"
 * Output: false
 * Explanation: s becomes "c" while t becomes "b".
 *
 * Constraints:
 *     1 <= s.length, t.length <= 200
 *     s and t only contain lowercase letters and '#' characters.
 *
 * Follow up: Can you solve it in O(n) time and O(1) space?
"""
from itertools import zip_longest

class Solution:
    def backspaceCompare(self, s: str, t: str) -> bool:
        def iterate(s):
            skip = 0
            for x in reversed(s):
                if x == '#':
                    skip += 1
                elif skip:
                    skip -= 1
                else:
                    yield x

        return all(x == y for x, y in zip_longest(iterate(s), iterate(t)))

if __name__ == "__main__":
    tests = (
        ("ab#c", "ad#c", True),
        ("ab##", "c#d#", True),
        ("a#c", "b", False),
        ("xywrrmp", "xywrrmu#p", True),
        ("a##c", "#a#c", True),
    )
    for s, t, expected in tests:
        print(Solution().backspaceCompare(s, t) == expected)
