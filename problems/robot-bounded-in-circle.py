"""
 * 1041. Robot Bounded In Circle (Medium)
 * On an infinite plane, a robot initially stands at (0, 0) and faces north.
 * The robot can receive one of three instructions:
 *     ∙ "G": go straight 1 unit;
 *     ∙ "L": turn 90 degrees to the left;
 *     ∙ "R": turn 90 degrees to the right.
 *
 * The robot performs the instructions given in order, and repeats them forever.
 *
 * Return true if and only if there exists a circle in the plane such that the
 * robot never leaves the circle.
 *
 * Example 1:
 * Input: instructions = "GGLLGG"
 * Output: true
 * Explanation: The robot moves from (0,0) to (0,2), turns 180 degrees, and
 * then returns to (0,0). When repeating these instructions, the robot remains
 * in the circle of radius 2 centered at the origin.
 *
 * Example 2:
 * Input: instructions = "GG"
 * Output: false
 * Explanation: The robot moves north indefinitely.
 *
 * Example 3:
 * Input: instructions = "GL"
 * Output: true
 * Explanation: The robot moves from (0, 0) -> (0, 1) -> (-1, 1) -> (-1, 0) ->
 * (0, 0) -> ...
 *
 * Constraints:
 *     ∙ 1 <= instructions.length <= 100
 *     ∙ instructions[i] is 'G', 'L' or, 'R'.
"""
class Solution:
    def isRobotBounded(self, instructions: str) -> bool:
        x, y = 0, 0
        dx, dy = 0, 1
        for i in instructions:
            if i == 'R':
                dx, dy = dy, -dx
            if i == 'L':
                dx, dy = -dy, dx
            if i == 'G':
                x, y = x + dx, y + dy
        # if robot return to the origin, he is obvious in an circle.
        # if robot finishes with face NOT towards north, it will get back to
        # the initial status in another one or three sequences.
        return (x, y) == (0, 0) or (dx, dy) != (0, 1)

if __name__ == "__main__":
    tests = (
        ("GGLLGG", True),
        ("GG", False),
        ("GL", True),
        ("GLGLGGLGL", False),
    )
    for instructions, expected in tests:
        print(Solution().isRobotBounded(instructions) == expected)
