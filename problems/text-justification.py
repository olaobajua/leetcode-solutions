"""
 * 68. Text Justification [Hard]
 * Given an array of strings words and a width maxWidth, format the text such
 * that each line has exactly maxWidth characters and is fully (left and right)
 * justified.
 *
 * You should pack your words in a greedy approach; that is, pack as many
 * words as you can in each line. Pad extra spaces ' ' when necessary so that
 * each line has exactly maxWidth characters.
 *
 * Extra spaces between words should be distributed as evenly as possible. If
 * the number of spaces on a line does not divide evenly between words, the
 * empty slots on the left will be assigned more spaces than the slots on the
 * right.
 *
 * For the last line of text, it should be left-justified, and no extra space
 * is inserted between words.
 *
 * Note:
 *     ∙ A word is defined as a character sequence consisting of non-space
 *       characters only.
 *     ∙ Each word's length is guaranteed to be greater than 0 and not exceed
 *       maxWidth.
 *     ∙ The input array words contains at least one word.
 *
 * Example 1:
 * Input: words = ["This", "is", "an", "example", "of", "text",
 * "justification."], maxWidth = 16
 * Output:
 * [
 *    "This    is    an",
 *    "example  of text",
 *    "justification.  "
 * ]
 *
 * Example 2:
 * Input: words = ["What","must","be","acknowledgment","shall","be"], maxWidth
 * = 16
 * Output:
 * [
 *   "What   must   be",
 *   "acknowledgment  ",
 *   "shall be        "
 * ]
 * Explanation: Note that the last line is "shall be    " instead of "shall
 *  be", because the last line must be left-justified instead of
 * fully-justified.
 * Note that the second line is also left-justified because it contains only
 * one word.
 *
 * Example 3:
 * Input: words =
 * ["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"],
 * maxWidth = 20
 * Output:
 * [
 *   "Science  is  what we",
 *   "understand      well",
 *   "enough to explain to",
 *   "a  computer.  Art is",
 *   "everything  else  we",
 *   "do                  "
 * ]
 *
 * Constraints:
 *     ∙ 1 <= words.length <= 300
 *     ∙ 1 <= words[i].length <= 20
 *     ∙ words[i] consists of only English letters and symbols.
 *     ∙ 1 <= maxWidth <= 100
 *     ∙ words[i].length <= maxWidth
"""
from typing import List

class Solution:
    def fullJustify(self, words: List[str], maxWidth: int) -> List[str]:
        ret = []
        stack = []
        count = 0
        for word in words:
            if count + len(word) > maxWidth:
                count -= len(stack)
                spaces = maxWidth - count
                if len(stack) == 1:
                    ret.append(stack[0] + " " * spaces)
                else:
                    delimeter = " " * (spaces // (len(stack) - 1))
                    extra_spaces = spaces % (len(stack) - 1)
                    cur_line = []
                    for w in stack:
                        cur_line.append(w)
                        cur_line.append(delimeter)
                        if extra_spaces:
                            cur_line.append(" ")
                            extra_spaces -= 1
                    ret.append("".join(cur_line[:-1]))
                count = 0
                stack = []

            count += len(word) + 1
            stack.append(word)

        if stack:
            cur_line = " ".join(stack)
            ret.append(cur_line + " " * (maxWidth - len(cur_line)))
        return ret

if __name__ == "__main__":
    tests = (
        (["This", "is", "an", "example", "of", "text", "justification."], 16,
         [
             "This    is    an",
             "example  of text",
             "justification.  "
         ]),
        (["What","must","be","acknowledgment","shall","be"], 16,
         [
             "What   must   be",
             "acknowledgment  ",
             "shall be        "
         ]),
        (["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"], 20,
         [
             "Science  is  what we",
             "understand      well",
             "enough to explain to",
             "a  computer.  Art is",
             "everything  else  we",
             "do                  "
         ]),
    )
    for words, maxWidth, expected in tests:
        print(Solution().fullJustify(words, maxWidth) == expected)
