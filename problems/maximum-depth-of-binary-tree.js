/**
 * 104. Maximum Depth of Binary Tree (Easy)
 * Given the root of a binary tree, return its maximum depth.
 *
 * A binary tree's maximum depth is the number of nodes along the longest path
 * from the root node down to the farthest leaf node.
 *
 * Example 1:
 * Input: root = [3,9,20,null,null,15,7]
 * Output: 3
 *
 * Example 2:
 * Input: root = [1,null,2]
 * Output: 2
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 10⁴].
 *     ∙ -100 <= Node.val <= 100
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @return {number}
 */
var maxDepth = function(root) {
    return height(root);
};

function height(root) {
    if (root === null) {
        return 0;
    }
    return 1 + Math.max(height(root.left), height(root.right));
}

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

const tests = [
    [[3,9,20,null,null,15,7], 3],
    [[1,null,2], 2],
];

for (const [nums, expected] of tests) {
    console.log(maxDepth(buildTree(nums)) == expected);
}
