"""
 * 168. Excel Sheet Column Title [Easy]
 * Given an integer columnNumber, return its corresponding column title as it
 * appears in an Excel sheet.
 *
 * For example:
 * A -> 1
 * B -> 2
 * C -> 3
 * ...
 * Z -> 26
 * AA -> 27
 * AB -> 28
 * ...
 *
 * Example 1:
 * Input: columnNumber = 1
 * Output: "A"
 *
 * Example 2:
 * Input: columnNumber = 28
 * Output: "AB"
 *
 * Example 3:
 * Input: columnNumber = 701
 * Output: "ZY"
 *
 * Constraints:
 *     ∙ 1 <= columnNumber <= 2³¹ - 1
"""
class Solution:
    def convertToTitle(self, columnNumber: int) -> str:
        ret = []
        a = ord('A')
        while columnNumber:
            columnNumber -= 1
            ret.append(chr(a + columnNumber % 26))
            columnNumber //= 26

        return "".join(reversed(ret))

if __name__ == "__main__":
    tests = (
        (1, "A"),
        (28, "AB"),
        (701, "ZY"),
    )
    for columnNumber, expected in tests:
        print(Solution().convertToTitle(columnNumber) == expected)
