"""
 * 429. N-ary Tree Level Order Traversal [Medium]
 * Given an n-ary tree, return the level order traversal of its nodes' values.
 *
 * Nary-Tree input serialization is represented in their level order
 * traversal, each group of children is separated by the null value (See
 * examples).
 *
 * Example 1:
 * Input: root = [1,null,
 *                3,2,4,null,
 *                5,6]
 * Output: [[1],[3,2,4],[5,6]]
 *
 * Example 2:
 * Input: root = [1,null,
 *                2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,
 *                11,null,12,null,13,null,null,
 *                14]
 * Output: [[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]
 *
 * Constraints:
 *     ∙ The height of the n-ary tree is less than or equal to 1000
 *     ∙ The total number of nodes is between [0, 10⁴]
"""
from typing import List

# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children

class Solution:
    def levelOrder(self, root: 'Node') -> List[List[int]]:
        if root is None:
            return []
        ret = []
        level = [root]
        while level:
            next_level = []
            ret.append([])
            for node in level:
                if node:
                    ret[-1].append(node.val)
                    next_level.extend(node.children)
            level = next_level
        return ret

def build_nary_tree(nums):
    root = Node(nums[0], [])
    nodes = [root]
    for x in nums[2:]:
        if x is None:
            nodes.pop(0)
        else:
            node = Node(x, [])
            nodes[0].children.append(node)
            nodes.append(node)
    return root

if __name__ == "__main__":
    tests = (
        ([1,None,
          3,2,4,None,
          5,6],
         [[1],[3,2,4],[5,6]]),
        ([1,None,
          2,3,4,5,None,None,
          6,7,None,8,None,9,10,None,None,11,None,12,None,13,None,None,
          14],
         [[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]),
    )
    for nums, expected in tests:
        print(Solution().levelOrder(build_nary_tree(nums)) == expected)
