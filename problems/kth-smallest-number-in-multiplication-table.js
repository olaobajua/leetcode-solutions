/**
 * 668. Kth Smallest Number in Multiplication Table (Hard)
 * Nearly everyone has used the Multiplication Table. The multiplication table
 * of size m x n is an integer matrix mat where mat[i][j] == i * j (1-indexed).
 *
 * Given three integers m, n, and k, return the kth smallest element in the
 * m x n multiplication table.
 *
 * Example 1:
 * 1 2 3
 * 2 4 6
 * 3 6 9
 *
 * 1 2 2 3 3 4 6 6 9
 * Input: m = 3, n = 3, k = 5
 * Output: 3
 * Explanation: The 5th smallest number is 3.
 *
 * Example 2:
 * 1 2 3
 * 2 4 6
 *
 * 1 2 2 3 4 6
 * Input: m = 2, n = 3, k = 6
 * Output: 6
 * Explanation: The 6th smallest number is 6.
 *
 * Constraints:
 *     1 <= m, n <= 3 * 10⁴
 *     1 <= k <= m * n
 */

/**
 * @param {number} m
 * @param {number} n
 * @param {number} k
 * @return {number}
 */
var findKthNumber = function(m, n, k) {
    function countLe(x) {
        let counter = 0;
        for (let i = 1; i <= n; ++i) {
            counter += Math.min(m, Math.floor(x / i));
        }
        return counter;
    }

    [m, n] = [m, n].sort();
    let lo = 1;
    let hi = m * n;
    while (hi - lo > 1) {
        const mid = Math.floor((lo + hi) / 2);
        if (countLe(mid) >= k) {
            hi = mid;
        } else {
            lo = mid;
        }
    }
    return hi;
};

const tests = [
    [3, 3, 5, 3],
    [2, 3, 6, 6],
    [10, 10, 25, 10],
    [10, 10, 50, 24],
    [10, 10, 75, 45],
    [10, 10, 100, 100],
    [42, 34, 401, 126],
    [45, 12, 471, 312],
    [38, 40, 955, 437],
    [9895, 28405, 100787757, 31666344],
];
for (const [m, n, k, expected] of tests) {
    console.log(findKthNumber(m, n, k) == expected);
}
