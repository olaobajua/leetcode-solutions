"""
 * 118. Pascal's Triangle [Easy]
 * Given an integer numRows, return the first numRows of Pascal's triangle.
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly
 * above it as shown:
 * Example 1:
 * Input: numRows = 5
 * Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
 * Example 2:
 * Input: numRows = 1
 * Output: [[1]]
 *
 * Constraints:
 *     ∙ 1 <= numRows <= 30
"""
from typing import List

class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        ret = []
        level = [1]
        while numRows:
            ret.append(level)
            level = [0] + level + [0]
            level = [x + y for x, y in zip(level, level[1:])]
            numRows -= 1
        return ret

if __name__ == "__main__":
    tests = (
        (5, [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]),
        (1, [[1]]),
    )
    for numRows, expected in tests:
        print(Solution().generate(numRows) == expected)
