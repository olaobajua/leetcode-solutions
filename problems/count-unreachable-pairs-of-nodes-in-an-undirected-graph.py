"""
 * 2316. Count Unreachable Pairs of Nodes in an Undirected Graph [Medium]
 * You are given an integer n. There is an undirected graph with n nodes,
 * numbered from 0 to n - 1. You are given a 2D integer array edges where
 * edges[i] = [aᵢ, bᵢ] denotes that there exists an undirected edge connecting
 * nodes aᵢ and bᵢ.
 *
 * Return the number of pairs of different nodes that are unreachable from
 * each other.
 *
 * Example 1:
 * Input: n = 3, edges = [[0,1],[0,2],[1,2]]
 * Output: 0
 * Explanation: There are no pairs of nodes that are unreachable from each
 * other. Therefore, we return 0.
 *
 * Example 2:
 * Input: n = 7, edges = [[0,2],[0,5],[2,4],[1,6],[5,4]]
 * Output: 14
 * Explanation: There are 14 pairs of nodes that are unreachable from each
 * other:
 *
 * [[0,1],[0,3],[0,6],[1,2],[1,3],[1,4],[1,5],[2,3],[2,6],[3,4],[3,5],[3,6],[4,6],[5,6]].
 * Therefore, we return 14.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁵
 *     ∙ 0 <= edges.length <= 2 * 10⁵
 *     ∙ edges[i].length == 2
 *     ∙ 0 <= aᵢ, bᵢ < n
 *     ∙ aᵢ != bᵢ
 *     ∙ There are no repeated edges.
"""
from collections import Counter
from typing import List

class Solution:
    def countPairs(self, n: int, edges: List[List[int]]) -> int:
        groups = DisjointSet(n)
        for a, b in edges:
            groups.union(a, b)
        count = Counter()
        for i in range(n):
            count[groups.find(i)] += 1
        ret = 0
        for m in count.values():
            ret -= m * (m - 1) // 2
        return ret + n * (n - 1) // 2


class DisjointSet:
    def __init__(self, n):
        self.parent = list(range(n))

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        if (px := self.find(x)) != (py := self.find(y)):
            self.parent[px] = py

if __name__ == "__main__":
    tests = (
        (3, [[0,1],[0,2],[1,2]], 0),
        (7, [[0,2],[0,5],[2,4],[1,6],[5,4]], 14),
    )
    for n, edges, expected in tests:
        print(Solution().countPairs(n, edges) == expected)
