"""
 * 1632. Rank Transform of a Matrix (Hard)
 * Given an m x n matrix, return a new matrix answer where answer[row][col] is
 * the rank of matrix[row][col].
 *
 * The rank is an integer that represents how large an element is compared to
 * other elements. It is calculated using the following rules:
 *     The rank is an integer starting from 1.
 *     If two elements p and q are in the same row or column, then:
 *         If p < q then rank(p) < rank(q)
 *         If p == q then rank(p) == rank(q)
 *         If p > q then rank(p) > rank(q)
 *     The rank should be as small as possible.
 *
 * It is guaranteed that answer is unique under the given rules.
 *
 * Example 1:
 * Input: matrix = [[1,2],
 *                  [3,4]]
 * Output: [[1,2],
 *          [2,3]]
 * Explanation:
 * The rank of matrix[0][0] is 1 because it is the smallest integer in its row and column.
 * The rank of matrix[0][1] is 2 because matrix[0][1] > matrix[0][0] and matrix[0][0] is rank 1.
 * The rank of matrix[1][0] is 2 because matrix[1][0] > matrix[0][0] and matrix[0][0] is rank 1.
 * The rank of matrix[1][1] is 3 because matrix[1][1] > matrix[0][1],
 * matrix[1][1] > matrix[1][0], and both matrix[0][1] and matrix[1][0] are rank 2.
 *
 * Example 2:
 * Input: matrix = [[7,7],
 *                  [7,7]]
 * Output: [[1,1],
 *          [1,1]]
 *
 * Example 3:
 * Input: matrix = [[ 20,-21,14],
 *                  [-19,  4,19],
 *                  [ 22,-47,24],
 *                  [-19,  4,19]]
 * Output: [[4,2,3],
 *          [1,3,4],
 *          [5,1,6],
 *          [1,3,4]]
 *
 * Example 4:
 * Input: matrix = [[7,3,6],
 *                  [1,4,5],
 *                  [9,8,2]]
 * Output: [[5,1,4],
 *          [1,2,3],
 *          [6,3,1]]
 *
 * Constraints:
 *     m == matrix.length
 *     n == matrix[i].length
 *     1 <= m, n <= 500
 *     -10⁹ <= matrix[row][col] <= 10⁹
 *
"""
from typing import List
from itertools import groupby
from collections import defaultdict
from operator import itemgetter

class DisjointSet:
    def __init__(self):
        self.parent = {}

    def make_set(self, x):
        self.parent.setdefault(x, x)

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        px = self.find(x)
        py = self.find(y)
        if px != py:
            self.parent[px] = py

class Solution:
    def matrixRankTransform(self, matrix: List[List[int]]) -> List[List[int]]:
        # sort the cells by value and process them in increasing order
        cells = sorted((cell, row_i, col_i) for row_i, row in enumerate(matrix)
                       for col_i, cell in enumerate(row))
        row_count = len(matrix)
        col_count = len(matrix[0])
        ret = [[0] * col_count for _ in range(row_count)]
        ranks = [0] * (col_count + row_count)
        value = itemgetter(0)
        for _, equal_cells in groupby(cells, value):
            # handle the equal cells by treating them as components using a
            # union-find data structure
            dset = DisjointSet()
            equal_cells = tuple(equal_cells)
            for _, row_i, col_i in equal_cells:
                dset.make_set(row_i)
                dset.make_set(col_i + row_count)
                dset.union(row_i, col_i + row_count)

            equal_rank_cells = defaultdict(set)
            for i in dset.parent:
                equal_rank_cells[dset.find(i)].add(i)

            for group in equal_rank_cells.values():
                # the rank of a cell is the maximum rank in its row and column
                # plus one
                rank = max(ranks[cell_num] for cell_num in group)
                for cell_num in group:
                    ranks[cell_num] = rank + 1

            for _, row_i, col_i in equal_cells:
                ret[row_i][col_i] = ranks[row_i]
        return ret

if __name__ == "__main__":
    tests = (
        ([[1,2],
          [3,4]],
         [[1,2],
          [2,3]]),
        ([[7,7],
          [7,7]],
         [[1,1],
          [1,1]]),
        ([[ 20,-21,14],
          [-19,  4,19],
          [ 22,-47,24],
          [-19,  4,19]],
         [[4,2,3],
          [1,3,4],
          [5,1,6],
          [1,3,4]]),
        ([[7,3,6],
          [1,4,5],
          [9,8,2]],
         [[5,1,4],
          [1,2,3],
          [6,3,1]]),
        ([[-37,-50, -3, 44],
          [-37, 46, 13,-32],
          [ 47,-42, -3,-40],
          [-17,-22,-39, 24]],
         [[2,1,4,6],
          [2,6,5,4],
          [5,2,4,3],
          [4,3,1,5]]),
        ([[-37,-26,-47,-40,-13],
          [ 22,-11,-44, 47, -6],
          [-35,  8,-45, 34,-31],
          [-16, 23, -6,-43,-20],
          [ 47, 38,-27, -8, 43]],
         [[3,  4,1, 2, 7],
          [9,  5,3,10, 8],
          [4,  6,2, 7, 5],
          [7,  9,8, 1, 6],
          [12,10,4, 5,11]]),
        ([[-2,-35,-32,-5,-30,33,-12],
          [7,2,-43,4,-49,14,17],
          [4,23,-6,-15,-24,-17,6],
          [-47,20,39,-26,9,-44,39],
          [-50,-47,44,43,-22,33,-36],
          [-13,34,49,24,23,-2,-35],
          [-40,43,-22,-19,-4,23,-18]],
         [[10,3,4,9,5,15,8],
          [12,4,2,10,1,13,14],
          [11,13,9,8,6,7,12],
          [2,10,15,4,9,3,15],
          [1,2,17,16,7,15,3],
          [5,14,18,11,10,8,4],
          [3,15,5,6,8,14,7]]),
        ([[-23,20,-49,-30,-39,-28,-5,-14],
         [-19,4,-33,2,-47,28,43,-6],
         [-47,36,-49,6,17,-8,-21,-30],
         [-27,44,27,10,21,-8,3,14],
         [-19,12,-25,34,-27,-48,-37,14],
         [-47,40,23,46,-39,48,-41,18],
         [-27,-4,7,-10,9,36,43,2],
         [37,44,43,-38,29,-44,19,38]],
         [[7,13,1,5,4,6,9,8],
         [8,11,2,10,1,12,14,9],
         [2,14,1,11,13,7,5,3],
         [3,19,16,12,14,7,10,13],
         [8,12,6,14,5,1,4,13],
         [2,16,15,17,4,18,3,14],
         [3,7,11,6,12,13,14,10],
         [16,19,18,3,15,2,11,17]]),
    )
    for matrix, expected in tests:
        print(Solution().matrixRankTransform(matrix) == expected)
