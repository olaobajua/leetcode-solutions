/**
 * 980. Unique Paths III (Hard)
 * You are given an m x n integer array grid where grid[i][j] could be:
 *     1 representing the starting square. There is exactly one starting square.
 *     2 representing the ending square. There is exactly one ending square.
 *     0 representing empty squares we can walk over.
 *     -1 representing obstacles that we cannot walk over.
 *
 * Return the number of 4-directional walks from the starting square to the
 * ending square, that walk over every non-obstacle square exactly once.
 *
 * Example 1:
 * Input: grid = [[1,0,0,0],[0,0,0,0],[0,0,2,-1]]
 * Output: 2
 * Explanation: We have the following two paths:
 * 1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2)
 * 2. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2)
 *
 * Example 2:
 * Input: grid = [[1,0,0,0],[0,0,0,0],[0,0,0,2]]
 * Output: 4
 * Explanation: We have the following four paths:
 * 1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2),(2,3)
 * 2. (0,0),(0,1),(1,1),(1,0),(2,0),(2,1),(2,2),(1,2),(0,2),(0,3),(1,3),(2,3)
 * 3. (0,0),(1,0),(2,0),(2,1),(2,2),(1,2),(1,1),(0,1),(0,2),(0,3),(1,3),(2,3)
 * 4. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2),(2,3)
 *
 * Example 3:
 * Input: grid = [[0,1],[2,0]]
 * Output: 0
 * Explanation: There is no path that walks over every empty square exactly once.
 * Note that the starting and ending square can be anywhere in the grid.
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m, n <= 20
 *     1 <= m * n <= 20
 *     -1 <= grid[i][j] <= 2
 *     There is exactly one starting cell and one ending cell.
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
var uniquePathsIII = function(grid) {
    function countPaths(r, c, left) {
        if (left == 0) {
            return 0;
        }
        let count = 0;
        const cell = grid[r][c];
        grid[r][c] = NaN;
        for (let [nr, nc] of [[r+1, c], [r-1, c], [r, c+1], [r, c-1]]) {
            if (0 <= nr && nr < n && 0 <= nc && nc < m) {
                if (grid[nr][nc] == 0) {
                    count += countPaths(nr, nc, left - 1);
                } else if (grid[nr][nc] == 2 && left == 1) {
                    ++count;
                }
            }
        }
        grid[r][c] = cell;
        return count;
    }
    const n = grid.length;
    const m = grid[0].length;
    let empty = 0, start = [0, 0];
    for (let row = 0; row < n; ++row) {
        for (let col = 0; col < m; ++col) {
            if (0 == grid[row][col] || 2 == grid[row][col]) {
                empty += 1;
            } else if (1 == grid[row][col]) {
                start = [row, col];
            }
        }
    }
    return countPaths(...start, empty);
};

const tests = [
    [[[1,0,0, 0],
      [0,0,0, 0],
      [0,0,2,-1]], 2],
    [[[1,0,0,0],
      [0,0,0,0],
      [0,0,0,2]], 4],
    [[[0,1],
      [2,0]], 0],
];
for (let [grid, expected] of tests) {
    console.log(uniquePathsIII(grid) == expected);
}
