"""
 * 547. Number of Provinces [Medium]
 * There are n cities. Some of them are connected, while some are not. If city
 * a is connected directly with city b, and city b is connected directly with
 * city c, then city a is connected indirectly with city c.
 *
 * A province is a group of directly or indirectly connected cities and no
 * other cities outside of the group.
 *
 * You are given an n x n matrix isConnected where isConnected[i][j] = 1 if
 * the iᵗʰ city and the jᵗʰ city are directly connected, and isConnected[i][j]
 * = 0 otherwise.
 *
 * Return the total number of provinces.
 *
 * Example 1:
 * Input: isConnected = [[1,1,0],
 *                       [1,1,0],
 *                       [0,0,1]]
 * Output: 2
 *
 * Example 2:
 * Input: isConnected = [[1,0,0],
 *                       [0,1,0],
 *                       [0,0,1]]
 * Output: 3
 *
 * Constraints:
 *     ∙ 1 <= n <= 200
 *     ∙ n == isConnected.length
 *     ∙ n == isConnected[i].length
 *     ∙ isConnected[i][j] is 1 or 0.
 *     ∙ isConnected[i][i] == 1
 *     ∙ isConnected[i][j] == isConnected[j][i]
"""
from typing import List

class Solution:
    def findCircleNum(self, isConnected: List[List[int]]) -> int:
        n = len(isConnected)
        provinces = DisjointSet(n)
        for i in range(n):
            for j in range(i + 1, n):
                if isConnected[i][j]:
                    provinces.union(i, j)
        return len({provinces.find(i) for i in range(n)})

class DisjointSet:
    def __init__(self, n):
        self.parent = list(range(n))

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        if (px := self.find(x)) != (py := self.find(y)):
            self.parent[px] = py

if __name__ == "__main__":
    tests = (
        ([[1,1,0],
          [1,1,0],
          [0,0,1]], 2),
        ([[1,0,0],
          [0,1,0],
          [0,0,1]], 3),
    )
    for isConnected, expected in tests:
        print(Solution().findCircleNum(isConnected) == expected)
