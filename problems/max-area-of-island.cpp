/**
 * 695. Max Area of Island (Medium)
 * You are given an m x n binary matrix grid. An island is a group of 1's
 * (representing land) connected 4-directionally (horizontal or vertical.) You
 * may assume all four edges of the grid are surrounded by water.
 *
 * The area of an island is the number of cells with a value 1 in the island.
 *
 * Return the maximum area of an island in grid. If there is no island,
 * return 0.
 *
 * Example 1:
 * Input: grid = [[0,0,1,0,0,0,0,1,0,0,0,0,0],
 *                [0,0,0,0,0,0,0,1,1,1,0,0,0],
 *                [0,1,1,0,1,0,0,0,0,0,0,0,0],
 *                [0,1,0,0,1,1,0,0,1,0,1,0,0],
 *                [0,1,0,0,1,1,0,0,1,1,1,0,0],
 *                [0,0,0,0,0,0,0,0,0,0,1,0,0],
 *                [0,0,0,0,0,0,0,1,1,1,0,0,0],
 *                [0,0,0,0,0,0,0,1,1,0,0,0,0]]
 * Output: 6
 * Explanation: The answer is not 11, because the island must be connected
 * 4-directionally.
 *
 * Example 2:
 * Input: grid = [[0,0,0,0,0,0,0,0]]
 * Output: 0
 *
 * Constraints:
 *     ∙ m == grid.length
 *     ∙ n == grid[i].length
 *     ∙ 1 <= m, n <= 50
 *     ∙ grid[i][j] is either 0 or 1.
 */
class Solution {
    int m;
    int n;
    vector<vector<int>> grid;
public:
    int maxAreaOfIsland(vector<vector<int>>& grid) {
        int ret = 0;
        this->grid = grid;
        m = grid.size();
        n = grid[0].size();
        for (int r = 0; r < m; ++r) {
            for (int c = 0; c < n; ++c) {
                ret = max(ret, get_square(r, c));
            }
        }
        return ret;
    }
    int get_square(int r, int c) {
        int ret = 0;
        if (grid[r][c] == 1) {
            ++ret;
            grid[r][c] = 2;
            if (c + 1 < n && grid[r][c+1] == 1)
                ret += get_square(r, c + 1);
            if (0 <= c - 1 && grid[r][c-1] == 1)
                ret += get_square(r, c - 1);
            if (r + 1 < m && grid[r+1][c] == 1)
                ret += get_square(r + 1, c);
            if (0 <= r - 1 && grid[r-1][c] == 1)
                ret += get_square(r - 1, c);
        }
        return ret;
    }
};
