/**
 * 993. Cousins in Binary Tree (Easy)
 * Given the root of a binary tree with unique values and the values of two
 * different nodes of the tree x and y, return true if the nodes corresponding
 * to the values x and y in the tree are cousins, or false otherwise.
 *
 * Two nodes of a binary tree are cousins if they have the same depth with
 * different parents.
 *
 * Note that in a binary tree, the root node is at the depth 0, and children of
 * each depth k node are at the depth k + 1.
 *
 * Example 1:
 * Input: root = [1,2,3,4], x = 4, y = 3
 * Output: false
 *
 * Example 2:
 * Input: root = [1,2,3,null,4,null,5], x = 5, y = 4
 * Output: true
 *
 * Example 3:
 * Input: root = [1,2,3,null,4], x = 2, y = 3
 * Output: false
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [2, 100].
 *     1 <= Node.val <= 100
 *     Each node has a unique value.
 *     x != y
 *     x and y are exist in the tree.
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 */
var isCousins = function(root, x, y) {
    return nodeDepth(root, x) == nodeDepth(root, y) && !isSiblings(root, x, y);
};

function nodeDepth(root, val) {
    if (root) {
        if (root.val == val) { return 0; }
        return nodeDepth(root.left, val) + 1 || nodeDepth(root.right, val) + 1;
    }
}

function isSiblings(root, x, y) {
    if (root) {
        if (root.left && root.right && [x, y].includes(root.left.val) &&
                [x, y].includes(root.right.val)) {
            return true;
        }
        if (isSiblings(root.left, x, y) || isSiblings(root.right, x, y)) {
            return true;
        }
    }
    return false;
}

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

var tests = [
    [[1,2,3,4], 4, 3, false],
    [[1,2,3,null,4,null,5], 5, 4, true],
    [[1,2,3,null,4], 2, 3, false],
    [[1,2,3,null,null,4,5], 4, 5, false],
    [[1,2,null,null,3,null,4,5], 3, 4, false],
];
for (let [nums, x, y, expected] of tests) {
    console.log(isCousins(buildTree(nums), x, y) == expected);
}
