/**
 * 704. Binary Search (Easy)
 * Given an array of integers nums which is sorted in ascending order, and an
 * integer target, write a function to search target in nums. If target exists,
 * then return its index. Otherwise, return -1.
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 * Example 1:
 * Input: nums = [-1,0,3,5,9,12], target = 9
 * Output: 4
 * Explanation: 9 exists in nums and its index is 4
 *
 * Example 2:
 * Input: nums = [-1,0,3,5,9,12], target = 2
 * Output: -1
 * Explanation: 2 does not exist in nums so return -1
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁴
 *     ∙ -10⁴ < nums[i], target < 10⁴
 *     ∙ All the integers in nums are unique.
 *     ∙ nums is sorted in ascending order.
 */
int compare(const int *a, const int *b) {
  return *a - *b;
}
int bisectLeft(int *a, int aSize, int x,
               int (*comp)(const void *, const void *), int lo, int hi) {
    while (lo <= hi) {
        const int mid = (lo + hi) / 2;
        if (comp(&x, &a[mid]) == 0) {
            return mid;
        } else if (comp(&x, &a[mid]) > 0) {
            lo = mid + 1;
        } else {
            hi = mid - 1;
        }
    }
    return lo;
}
int search(int *nums, int numsSize, int target) {
    const int i = bisectLeft(nums, numsSize, target, compare, 0, numsSize - 1);
    return i < numsSize && nums[i] == target ? i : -1;
}
