"""
 * 137. Single Number II [Medium]
 * Given an integer array nums where every element appears three times except
 * for one, which appears exactly once. Find the single element and return it.
 *
 * You must implement a solution with a linear runtime complexity and use only
 * constant extra space.
 *
 * Example 1:
 * Input: nums = [2,2,3,2]
 * Output: 3
 * Example 2:
 * Input: nums = [0,1,0,1,0,1,99]
 * Output: 99
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 3 * 10⁴
 *     ∙ -2³¹ <= nums[i] <= 2³¹ - 1
 *     ∙ Each element in nums appears exactly three times except for one
 *       element which appears once.
"""
from typing import List

class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        ret = 0
        for i in range(32):
            if sum(x & (1 << i) > 0 for x in nums) % 3:
                ret |= 1 << i

        if ret >= 1 << 31:
            ret -= 1 << 32
            
        return ret

if __name__ == "__main__":
    tests = (
        ([2,2,3,2], 3),
        ([0,1,0,1,0,1,99], 99),
        ([-2,-2,1,1,4,1,4,4,-4,-2], -4),
    )
    for nums, expected in tests:
        print(Solution().singleNumber(nums) == expected)
