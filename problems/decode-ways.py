"""
 * 91. Decode Ways [Medium]
 * A message containing letters from A-Z can be encoded into numbers using the
 * following mapping:
 * 'A' -> "1"
 * 'B' -> "2"
 * ...
 * 'Z' -> "26"
 *
 * To decode an encoded message, all the digits must be grouped then mapped
 * back into letters using the reverse of the mapping above (there may be
 * multiple ways). For example, "11106" can be mapped into:
 *     ∙ "AAJF" with the grouping (1 1 10 6)
 *     ∙ "KJF" with the grouping (11 10 6)
 *
 * Note that the grouping (1 11 06) is invalid because "06" cannot be mapped
 * into 'F' since "6" is different from "06".
 *
 * Given a string s containing only digits, return the number of ways to
 * decode it.
 *
 * The test cases are generated so that the answer fits in a 32-bit integer.
 *
 * Example 1:
 * Input: s = "12"
 * Output: 2
 * Explanation: "12" could be decoded as "AB" (1 2) or "L" (12).
 *
 * Example 2:
 * Input: s = "226"
 * Output: 3
 * Explanation: "226" could be decoded as "BZ" (2 26), "VF" (22 6), or "BBF"
 * (2 2 6).
 *
 * Example 3:
 * Input: s = "06"
 * Output: 0
 * Explanation: "06" cannot be mapped to "F" because of the leading zero ("6"
 * is different from "06").
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 100
 *     ∙ s contains only digits and may contain leading zero(s).
"""
from functools import cache

class Solution:
    def numDecodings(self, s: str) -> int:
        @cache
        def dp(i):
            if i <= 0:
                return int(s[0] != '0')

            char = s[i]
            prev = s[i-1]
            ret = 0
            if char != '0':
                ret += dp(i - 1)
            elif prev == '0':
                return 0
            if 10 <= int(prev + char) <= 26:
                ret += dp(i - 2)
            return ret

        return dp(len(s) - 1)

if __name__ == "__main__":
    tests = (
        ("12", 2),
        ("226", 3),
        ("06", 0),
        ("0", 0),
        ("1", 1),
        ("11106", 2),  # 1 1 10 6, 11 10 6
        ("111", 3),  # 1 1 1, 11 1, 1 11
        ("1110", 2),  # 1 1 10, 11 10
        ("10", 1),
        ("1123", 5),  # 1 1 2 3, 11 2 3, 1 12 3, 1 1 23, 11 23
        ("2611055971756562", 4),
    )
    for s, expected in tests:
        print(Solution().numDecodings(s) == expected)
