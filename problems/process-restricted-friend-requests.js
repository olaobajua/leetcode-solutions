/**
 * 2076. Process Restricted Friend Requests (Hard)
 * You are given an integer n indicating the number of people in a network.
 * Each person is labeled from 0 to n - 1.
 *
 * You are also given a 0-indexed 2D integer array restrictions, where
 * restrictions[i] = [xᵢ, yᵢ] means that person xᵢ and person yᵢ cannot become
 * friends, either directly or indirectly through other people.
 *
 * Initially, no one is friends with each other. You are given a list of friend
 * requests as a 0-indexed 2D integer array requests, where
 * requests[j] = [uⱼ, vⱼ] is a friend request between person uⱼ and person vⱼ.
 *
 * A friend request is successful if uⱼ and vⱼ can be friends. Each friend
 * request is processed in the given order (i.e., requests[j] occurs before
 * requests[j + 1]), and upon a successful request, uⱼ and vⱼ become direct
 * friends for all future friend requests.
 *
 * Return a boolean array result, where each result[j] is true if the jth
 * friend request is successful or false if it is not.
 *
 * Note: If uⱼ and vⱼ are already direct friends, the request is still
 * successful.
 *
 * Example 1:
 * Input: n = 3, restrictions = [[0,1]], requests = [[0,2],[2,1]]
 * Output: [true,false]
 * Explanation:
 * Request 0: Person 0 and person 2 can be friends, so they become direct
 * friends.
 * Request 1: Person 2 and person 1 cannot be friends since person 0 and
 * person 1 would be indirect friends (1--2--0).
 *
 * Example 2:
 * Input: n = 3, restrictions = [[0,1]], requests = [[1,2],[0,2]]
 * Output: [true,false]
 * Explanation:
 * Request 0: Person 1 and person 2 can be friends, so they become direct
 * friends.
 * Request 1: Person 0 and person 2 cannot be friends since person 0 and
 * person 1 would be indirect friends (0--2--1).
 *
 * Example 3:
 * Input: n = 5, restrictions = [[0,1],[1,2],[2,3]],
 *        requests = [[0,4],[1,2],[3,1],[3,4]]
 * Output: [true,false,true,false]
 * Explanation:
 * Request 0: Person 0 and person 4 can be friends, so they become direct
 * friends.
 * Request 1: Person 1 and person 2 cannot be friends since they are directly
 * restricted.
 * Request 2: Person 3 and person 1 can be friends, so they become direct
 * friends.
 * Request 3: Person 3 and person 4 cannot be friends since person 0 and
 * person 1 would be indirect friends (0--4--3--1).
 *
 * Constraints:
 *     2 <= n <= 1000
 *     0 <= restrictions.length <= 1000
 *     restrictions[i].length == 2
 *     0 <= xᵢ, yᵢ <= n - 1
 *     xᵢ != yᵢ
 *     1 <= requests.length <= 1000
 *     requests[j].length == 2
 *     0 <= uⱼ, vⱼ <= n - 1
 *     uⱼ != vⱼ
 */

/**
 * @param {number} n
 * @param {number[][]} restrictions
 * @param {number[][]} requests
 * @return {boolean[]}
 */
var friendRequests = function(n, restrictions, requests) {
    const friends = new DisjointSet();
    for (let i = 0; i < n; ++i) {
        friends.makeSet(i);
    }
    const ret = [];
    processRequests:
    for (const [p1, p2] of requests) {
        const u = [friends.find(p1), friends.find(p2)];
        for (const [r1, r2] of restrictions) {
            if (u.includes(friends.find(r1)) && u.includes(friends.find(r2))) {
                ret.push(false);
                continue processRequests;
            }
        }
        friends.union(p1, p2);
        ret.push(true);
    }
    return ret;
};

class DisjointSet {
    constructor(name) {
        Object.defineProperty(this, 'parent', { value: {} });
    }
    makeSet(x) {
        this.parent[x] = this.parent[x] === undefined ? x : this.parent[x];
    }
    find(x) {
        if (x != this.parent[x]) {
            this.parent[x] = this.find(this.parent[x]);
        }
        return this.parent[x];
    }
    union(x, y) {
        const px = this.find(x);
        const py = this.find(y);
        if (px != py) {
            this.parent[px] = py;
        }
    }
}

const tests = [
    [3, [[0,1]], [[0,2],[2,1]], [true,false]],
    [3, [[0,1]], [[1,2],[0,2]], [true,false]],
    [5, [[0,1],[1,2],[2,3]], [[0,4],[1,2],[3,1],[3,4]], [true,false,true,false]],
];

for (const [n, restrictions, requests, expected] of tests) {
    console.log(friendRequests(n, restrictions, requests)
                .every((r, i) =>  r == expected[i]));
}
