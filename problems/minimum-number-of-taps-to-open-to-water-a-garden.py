"""
 * 1326. Minimum Number of Taps to Open to Water a Garden [Hard]
 * There is a one-dimensional garden on the x-axis. The garden starts at the
 * point 0 and ends at the point n. (i.e The length of the garden is n).
 *
 * There are n + 1 taps located at points [0, 1, ..., n] in the garden.
 *
 * Given an integer n and an integer array ranges of length n + 1 where
 * ranges[i] (0-indexed) means the i-th tap can water the area [i - ranges[i],
 * i + ranges[i]] if it was open.
 *
 * Return the minimum number of taps that should be open to water the whole
 * garden, If the garden cannot be watered return -1.
 *
 * Example 1:
 * Input: n = 5, ranges = [3,4,1,1,0,0]
 * Output: 1
 * Explanation: The tap at point 0 can cover the interval [-3,3]
 * The tap at point 1 can cover the interval [-3,5]
 * The tap at point 2 can cover the interval [1,3]
 * The tap at point 3 can cover the interval [2,4]
 * The tap at point 4 can cover the interval [4,4]
 * The tap at point 5 can cover the interval [5,5]
 * Opening Only the second tap will water the whole garden [0,5]
 *
 * Example 2:
 * Input: n = 3, ranges = [0,0,0,0]
 * Output: -1
 * Explanation: Even if you activate all the four taps you cannot water the
 * whole garden.
 *
 * Constraints:
 *     ∙ 1 <= n <= 10⁴
 *     ∙ ranges.length == n + 1
 *     ∙ 0 <= ranges[i] <= 100
"""
from typing import List

class Solution:
    def minTaps(self, n: int, ranges: List[int]) -> int:
        max_reach = [0] * (n + 1)
        for i, r in enumerate(ranges):
            left = max(0, i - r)
            right = min(n, i + r)
            max_reach[left] = max(max_reach[left], right)
        
        taps = 0
        current_end = 0
        farthest_reach = 0
        for i in range(n + 1):
            farthest_reach = max(farthest_reach, max_reach[i])
            if i == current_end:
                taps += 1
                current_end = farthest_reach
                if current_end >= n:
                    return taps
            if i > current_end:
                return -1
        
        return taps


if __name__ == "__main__":
    tests = (
        (5, [3,4,1,1,0,0], 1),
        (3, [0,0,0,0], -1),
        (7, [1,2,1,0,2,1,0,1], 3),
        (17, [0,3,3,2,2,4,2,1,5,1,0,1,2,3,0,3,1,1], 3),
    )
    for n, ranges, expected in tests:
        print(Solution().minTaps(n, ranges) == expected)
