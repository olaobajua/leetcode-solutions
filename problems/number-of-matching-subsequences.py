"""
 * 792. Number of Matching Subsequences [Medium]
 * Given a string s and an array of strings words, return the number of
 * words[i] that is a subsequence of s.
 *
 * A subsequence of a string is a new string generated from the original string
 * with some characters (can be none) deleted without changing the relative
 * order of the remaining characters.
 *     ∙ For example, "ace" is a subsequence of "abcde".
 *
 * Example 1:
 * Input: s = "abcde", words = ["a","bb","acd","ace"]
 * Output: 3
 * Explanation: There are three strings in words that are a subsequence of s:
 * "a", "acd", "ace".
 *
 * Example 2:
 * Input: s = "dsahjpjauf", words = ["ahjpjau","ja","ahbwzgqnuk","tnmlanowax"]
 * Output: 2
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 5 * 10⁴
 *     ∙ 1 <= words.length <= 5000
 *     ∙ 1 <= words[i].length <= 50
 *     ∙ s and words[i] consist of only lowercase English letters.
"""
from collections import defaultdict
from typing import List

class Solution:
    def numMatchingSubseq(self, s: str, words: List[str]) -> int:
        word_index = defaultdict(list)
        for word in words:
            word_index[word[0]].append(word[1:])
        ret = 0
        for char in s:
            for word in word_index.pop(char, []):
                if word:
                    word_index[word[0]].append(word[1:])
                else:
                    ret += 1
        return ret

if __name__ == "__main__":
    tests = (
        ("abcde", ["a","bb","acd","ace"], 3),
        ("dsahjpjauf", ["ahjpjau","ja","ahbwzgqnuk","tnmlanowax"], 2),
    )
    for s, words, expected in tests:
        print(Solution().numMatchingSubseq(s, words) == expected)
