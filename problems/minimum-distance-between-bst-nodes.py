"""
 * 783. Minimum Distance Between BST Nodes [Easy]
 * Given the root of a Binary Search Tree (BST), return the minimum difference
 * between the values of any two different nodes in the tree.
 *
 * Example 1:
 * Input: root = [4,2,6,1,3]
 * Output: 1
 *
 * Example 2:
 * Input: root = [1,0,48,null,null,12,49]
 * Output: 1
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [2, 100].
 *     ∙ 0 <= Node.val <= 10⁵
 *
 * Note: This question is the same as 530:
 * https://leetcode.com/problems/minimum-absolute-difference-in-bst/
 * (https://leetcode.com/problems/minimum-absolute-difference-in-bst/)
"""
from math import inf
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def minDiffInBST(self, root: Optional[TreeNode]) -> int:
        def dfs(node):
            if node:
                dfs(node.left)
                nonlocal ret, prev
                ret = min(ret, node.val - prev)
                prev = node.val
                dfs(node.right)
        prev = -inf
        ret = inf
        dfs(root)
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([4,2,6,1,3], 1),
        ([1,0,48,None,None,12,49], 1),
        ([90,69,None,49,89,None,52], 1),
    )
    for nums, expected in tests:
        print(Solution().minDiffInBST(build_tree(nums)) == expected)
