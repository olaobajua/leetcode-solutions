"""
 * 878. Nth Magical Number (Hard)
 * A positive integer is magical if it is divisible by either a or b.
 *
 * Given the three integers n, a, and b, return the nᵗʰ magical number. Since
 * the answer may be very large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 1, a = 2, b = 3
 * Output: 2
 *
 * Example 2:
 * Input: n = 4, a = 2, b = 3
 * Output: 6
 *
 * Example 3:
 * Input: n = 5, a = 2, b = 4
 * Output: 10
 *
 * Example 4:
 * Input: n = 3, a = 6, b = 4
 * Output: 8
 *
 * Constraints:
 *     1 <= n <= 10⁹
 *     2 <= a, b <= 4 * 10⁴
"""
from math import gcd
from typing import List

class Solution:
    def nthMagicalNumber(self, n: int, a: int, b: int) -> int:
        # n = x/a + x/b - x/lcm
        # n = (x*b*lcm + x*a*lcm - x*a*b) / (a*b*lcm)
        # n*a*b*lcm = (b*lcm + a*lcm - a*b)*x
        # x = n*a*b*lcm / (b*lcm + a*lcm - a*b)
        lcm = a * b // gcd(a, b)
        x = n*a*b*lcm // (b*lcm + a*lcm - a*b)
        for m in (x//a*a, x//b*b, x//a*a + a, x//b*b + b):
            if m//a + m//b - m//lcm == n:
                return m % (10**9 + 7)

if __name__ == "__main__":
    tests = (
        (1, 2, 3, 2),
        (4, 2, 3, 6),
        (2, 7, 3, 6),
        (5, 2, 4, 10),
        (3, 6, 4, 8),
        (3, 8, 3, 8),
        (9, 10, 2, 18),
        (7, 5, 8, 24),
        (859, 759, 30, 24900),
        (1000000000, 40000, 40000, 999720007),
    )
    for n, a, b, expected in tests:
        print(Solution().nthMagicalNumber(n, a, b) == expected)
