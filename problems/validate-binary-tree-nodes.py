"""
 * 1361. Validate Binary Tree Nodes [Medium]
 * You have n binary tree nodes numbered from 0 to n - 1 where node i has two
 * children leftChild[i] and rightChild[i], return true if and only if all the
 * given nodes form exactly one valid binary tree.
 *
 * If node i has no left child then leftChild[i] will equal -1, similarly for
 * the right child.
 *
 * Note that the nodes have no values and that we only use the node numbers in
 * this problem.
 *
 * Example 1:
 *   0
 * 1   2
 *    3
 * Input: n = 4, leftChild = [1,-1,3,-1], rightChild = [2,-1,-1,-1]
 * Output: true
 *
 * Example 2:
 *   0
 * 1   2
 *  3 3
 * Input: n = 4, leftChild = [1,-1,3,-1], rightChild = [2,3,-1,-1]
 * Output: false
 *
 * Example 3:
 *   0
 *  1
 * 0
 * Input: n = 2, leftChild = [1,0], rightChild = [-1,-1]
 * Output: false
 *
 * Constraints:
 *     ∙ n == leftChild.length == rightChild.length
 *     ∙ 1 <= n <= 10⁴
 *     ∙ -1 <= leftChild[i], rightChild[i] <= n - 1
"""
from typing import List

class Solution:
    def validateBinaryTreeNodes(self, n: int, leftChild: List[int], rightChild: List[int]) -> bool:
        parent = [None] * n
        for i, (left, right) in enumerate(zip(leftChild, rightChild)):
            if left != -1:
                if parent[left] is None:
                    parent[left] = i
                else:
                    return False
            if right != -1:
                if parent[right] is None:
                    parent[right] = i
                else:
                    return False
        root = None
        for i, node in enumerate(parent):
            if node is None:
                if root is None:
                    root = i
                else:
                    return False
        if root is None:
            return False
        to_visit = [root]
        seen = set()
        while to_visit:
            next_level = []
            for node in to_visit:
                if node in seen:
                    return False
                seen.add(node)
                if leftChild[node] > -1:
                    next_level.append(leftChild[node])
                if rightChild[node] > -1:
                    next_level.append(rightChild[node])
            to_visit = next_level
        return len(seen) == n

if __name__ == "__main__":
    tests = (
        (4, [1,-1,3,-1], [2,-1,-1,-1], True),
        (4, [1,-1,3,-1], [2,3,-1,-1], False),
        (2, [1,0], [-1,-1], False),
        (6, [1,-1,-1,4,-1,-1], [2,-1,-1,5,-1,-1], False),
        (4, [3,-1,1,-1], [-1,-1,0,-1], True),
        (4, [1,0,3,-1], [-1,-1,-1,-1], False),
    )
    for n, leftChild, rightChild, expected in tests:
        print(Solution().validateBinaryTreeNodes(n, leftChild, rightChild) ==
              expected)
