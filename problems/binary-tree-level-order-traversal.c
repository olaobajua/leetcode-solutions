/**
 * 102. Binary Tree Level Order Traversal (Medium)
 * Given the root of a binary tree, return the level order traversal of its
 * nodes' values. (i.e., from left to right, level by level).
 *
 * Example 1:
 * Input: root = [3,9,20,null,null,15,7]
 * Output: [[3],[9,20],[15,7]]
 *
 * Example 2:
 * Input: root = [1]
 * Output: [[1]]
 *
 * Example 3:
 * Input: root = []
 * Output: []
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [0, 2000].
 *     ∙ -1000 <= Node.val <= 1000
 */
// Definition for a binary tree node.
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

int** levelOrder(struct TreeNode *root, int *returnSize, int **returnColumnSizes) {
    int **ret = NULL;
    *returnColumnSizes = NULL;
    *returnSize = 0;
    static struct TreeNode* queue[2001];
    static int level[1024];
    int start = 0, end = 0;
    if (root) {
        queue[end++] = root;
    }
    while (end - start) {
        int level_size = 0;
        for (int i = end - start; i; --i) {
            struct TreeNode* node = queue[start++];
            level[level_size++] = node->val;
            if (node->left) {
                queue[end++] = node->left;
            }
            if (node->right) {
                queue[end++] = node->right;
            }
        }
        (*returnSize)++;
        *returnColumnSizes = (int*)realloc(*returnColumnSizes,
                                           (*returnSize) * sizeof(int*));

        (*returnColumnSizes)[(*returnSize)-1] = level_size;
        ret = realloc(ret, (*returnSize) * sizeof(int*));
        ret[(*returnSize)-1] = (int*)malloc(level_size * sizeof(int));
        memcpy(ret[(*returnSize)-1], level, level_size * sizeof(int));
    }
    return ret;
}
