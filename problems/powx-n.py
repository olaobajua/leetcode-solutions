"""
 * 50. Pow(x, n) [Medium]
 * Implement pow(x, n) (http://www.cplusplus.com/reference/valarray/pow/),
 * which calculates x raised to the power n (i.e., xⁿ).
 *
 * Example 1:
 * Input: x = 2.00000, n = 10
 * Output: 1024.00000
 *
 * Example 2:
 * Input: x = 2.10000, n = 3
 * Output: 9.26100
 *
 * Example 3:
 * Input: x = 2.00000, n = -2
 * Output: 0.25000
 * Explanation: 2⁻² = 1/2² = 1/4 = 0.25
 *
 * Constraints:
 *     ∙ -100.0 < x < 100.0
 *     ∙ -2³¹ <= n <= 2³¹-1
 *     ∙ n is an integer.
 *     ∙ Either x is not zero or n > 0.
 *     ∙ -10⁴ <= xⁿ <= 10⁴
"""
from functools import cache
from math import copysign, isclose

class Solution:
    @cache
    def myPow(self, x: float, n: int) -> float:
        if n == 0:
            return copysign(1, x)
        if n < 0:
            return 1 / self.myPow(x, -n)
        if n % 2:
            return x * self.myPow(x, n - 1)
        return self.myPow(x * x, n / 2)

if __name__ == "__main__":
    tests = (
        (2.00000, 10, 1024.00000),
        (2.10000, 3, 9.26100),
        (2.00000, -2, 0.25000),
        (0.00001, 2147483647, 0.00000),
    )
    for x, n, expected in tests:
        print(isclose(Solution().myPow(x, n), expected))
