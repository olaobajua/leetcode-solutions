/**
 * 1220. Count Vowels Permutation [Hard]
 * Given an integer n, your task is to count how many strings of length n can
 * be formed under the following rules:
 *     ∙ Each character is a lower case vowel ('a', 'e', 'i', 'o', 'u')
 *     ∙ Each vowel 'a' may only be followed by an 'e'.
 *     ∙ Each vowel 'e' may only be followed by an 'a' or an 'i'.
 *     ∙ Each vowel 'i' may not be followed by another 'i'.
 *     ∙ Each vowel 'o' may only be followed by an 'i' or a 'u'.
 *     ∙ Each vowel 'u' may only be followed by an 'a'.
 *
 * Since the answer may be too large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 1
 * Output: 5
 * Explanation: All possible strings are: "a", "e", "i" , "o" and "u".
 *
 * Example 2:
 * Input: n = 2
 * Output: 10
 * Explanation: All possible strings are: "ae", "ea", "ei", "ia", "ie", "io",
 * "iu", "oi", "ou" and "ua".
 *
 * Example 3:
 *
 * Input: n = 5
 * Output: 68
 *
 * Constraints:
 *     ∙ 1 <= n <= 2 * 10⁴
 */
#include <iostream>
#include <unordered_map>

class Solution {
public:
    int countVowelPermutation(int n) {
        int MOD = 1000000007;
        std::unordered_map<char, long> count = {
            {'a', 1}, {'e', 1}, {'i', 1}, {'o', 1}, {'u', 1}
        };
        for (int j = 1; j < n; ++j) {
            long a = (count['e'] + count['i'] + count['u']) % MOD;
            long e = (count['a'] + count['i']) % MOD;
            long i = (count['e'] + count['o']) % MOD;
            long o = count['i'] % MOD;
            long u = (count['i'] + count['o']) % MOD;
            
            count['a'] = a;
            count['e'] = e;
            count['i'] = i;
            count['o'] = o;
            count['u'] = u;
        }
        long ret = 0;
        for (const auto& pair : count) {
            ret = (ret + pair.second) % MOD;
        }
        return ret;
    }
};
