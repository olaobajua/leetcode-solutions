/**
 * 2147. Number of Ways to Divide a Long Corridor (Hard)
 * Along a long library corridor, there is a line of seats and decorative
 * plants. You are given a 0-indexed string corridor of length n consisting of
 * letters 'S' and 'P' where each 'S' represents a seat and each 'P' represents
 * a plant.
 *
 * One room divider has already been installed to the left of index 0, and
 * another to the right of index n - 1. Additional room dividers can be
 * installed. For each position between indices i - 1 and i (1 <= i <= n - 1),
 * at most one divider can be installed.
 *
 * Divide the corridor into non-overlapping sections, where each section has
 * exactly two seats with any number of plants. There may be multiple ways to
 * perform the division. Two ways are different if there is a position with a
 * room divider installed in the first way but not in the second way.
 *
 * Return the number of ways to divide the corridor. Since the answer may be
 * very large, return it modulo 10⁹ + 7. If there is no way, return 0.
 *
 * Example 1:
 * ║SS│PPSPS║
 * ║SSP│PSPS║
 * ║SSPP│SPS║
 * Input: corridor = "SSPPSPS"
 * Output: 3
 * Explanation: There are 3 different ways to divide the corridor.
 * Note that in each of the ways, each section has exactly two seats.
 *
 * Example 2:
 * ║PPSPSP║
 * Input: corridor = "PPSPSP"
 * Output: 1
 * Explanation: There is only 1 way to divide the corridor, by not installing
 * any additional dividers. Installing any would create some section that does
 * not have exactly two seats.
 *
 * Example 3:
 * ║S║
 * Input: corridor = "S"
 * Output: 0
 * Explanation: There is no way to divide the corridor because there will
 * always be a section that does not have exactly two seats.
 *
 * Constraints:
 *     ∙ n == corridor.length
 *     ∙ 1 <= n <= 10⁵
 *     ∙ corridor[i] is either 'S' or 'P'.
 */

/**
 * @param {string} corridor
 * @return {number}
 */
var numberOfWays = function(corridor) {
    const mod = 10**9 + 7;
    let a = 1, b = 0, c = 0;
    for (const char of corridor) {
        [a, b, c] = char == 'S' ? [0, (a + c) % mod, b] : [(a + c) % mod, b, c];
    }
    return c;
};

const tests = [
    ["SSPPSPS", 3],
    ["SSPPPPSS", 5],
    ["SSPPPPPPSS", 7],
    ["SSPPSSPPSS", 9],
    ["SSPPSSPPSSPPSS", 27],
    ["PPSPSP", 1],
    ["PPSSS", 0],
    ["S", 0],
    ["P", 0],
    ["PPPPPSPPSPPSPPPSPPPPSPPPPSPPPPSPPSPPPSPSPPPSPSPPPSPSPPPSPSPPPPSPPPPSPPPSPPSPPPPSPSPPPPSPSPPPPSPSPPPSPPSPPPPSPSPSS", 919999993],
];

for (const [corridor, expected] of tests) {
    console.log(numberOfWays(corridor) == expected);
}
