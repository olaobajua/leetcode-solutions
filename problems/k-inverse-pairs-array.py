"""
 * 629. K Inverse Pairs Array [Hard]
 * For an integer array nums, an inverse pair is a pair of integers [i, j]
 * where 0 <= i < j < nums.length and nums[i] > nums[j].
 *
 * Given two integers n and k, return the number of different arrays consist of
 * numbers from 1 to n such that there are exactly k inverse pairs. Since the
 * answer can be huge, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 3, k = 0
 * Output: 1
 * Explanation: Only the array [1,2,3] which consists of numbers from 1 to 3
 * has exactly 0 inverse pairs.
 *
 * Example 2:
 * Input: n = 3, k = 1
 * Output: 2
 * Explanation: The array [1,3,2] and [2,1,3] have exactly 1 inverse pair.
 *
 * Constraints:
 *     ∙ 1 <= n <= 1000
 *     ∙ 0 <= k <= 1000
"""
from functools import cache

class Solution:
    def kInversePairs(self, n: int, k: int) -> int:
        @cache
        def dp(n, k):
            if n == 0:
                return 0
            if k == 0:
                return 1
            return dp(n, k-1) + dp(n-1, k) - ((k-n) >= 0 and dp(n-1, k-n)) % M
        M = 1000000007
        return (dp(n, k) - (k > 0 and dp(n, k - 1))) % M

if __name__ == "__main__":
    tests = (
        (3, 0, 1),  # [1,2,3]
        (3, 1, 2),  # [1,3,2], [2,1,3]
        (3, 2, 2),  # [2,3,1], [3,1,2]
        (3, 3, 1),  # [3,2,1]
        (4, 0, 1),  # [1,2,3,4]
        (4, 1, 3),  # [1,2,4,3], [1,3,2,4], [2,1,3,4]
        (4, 2, 5),  # [1,3,4,2], [1,4,2,3], [2,1,4,3], [2,3,1,4], [3,1,2,4]
        (4, 3, 6),  # [1,4,3,2], [2,3,4,1], [2,4,1,3], [3,1,4,2], [3,2,1,4], [4,1,2,3]
        (4, 4, 5),  # [2,4,3,1], [3,2,4,1], [3,4,1,2], [4,1,3,2], [4,2,1,3]
        (4, 5, 3),  # [3,4,2,1], [4,2,3,1], [4,3,1,2]
        (4, 6, 1),  # [4,3,2,1]
        (4, 7, 0),
        (10, 1, 9),
        (10, 2, 44),
        (10, 3, 155),
        (1000, 1000, 663677020)
    )
    import sys
    sys.setrecursionlimit(10**5)
    for n, k, expected in tests:
        print(Solution().kInversePairs(n, k) == expected)
