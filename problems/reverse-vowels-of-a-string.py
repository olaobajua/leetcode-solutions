"""
 * 345. Reverse Vowels of a String [Easy]
 * Given a string s, reverse only all the vowels in the string and return it.
 *
 * The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both
 * lower and upper cases, more than once.
 *
 * Example 1:
 * Input: s = "hello"
 * Output: "holle"
 * Example 2:
 * Input: s = "leetcode"
 * Output: "leotcede"
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 3 * 10⁵
 *     ∙ s consist of printable ASCII characters.
"""
class Solution:
    def reverseVowels(self, s: str) -> str:
        VOWELS = set("AEIOUaeiou")
        vs = reversed([c for c in s if c in VOWELS])
        return "".join(next(vs) if c in VOWELS else c for c in s)

if __name__ == "__main__":
    tests = (
        ("hello", "holle"),
        ("leetcode", "leotcede"),
        ("aA", "Aa"),
    )
    for s, expected in tests:
        print(Solution().reverseVowels(s) == expected)
