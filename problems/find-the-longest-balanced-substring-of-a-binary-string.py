"""
 * 2609. Find the Longest Balanced Substring of a Binary String [Easy]
 * You are given a binary string s consisting only of zeroes and ones.
 *
 * A substring of s is considered balanced if all zeroes are before ones and
 * the number of zeroes is equal to the number of ones inside the substring.
 * Notice that the empty substring is considered a balanced substring.
 *
 * Return the length of the longest balanced substring of s.
 *
 * A substring is a contiguous sequence of characters within a string.
 *
 * Example 1:
 * Input: s = "01000111"
 * Output: 6
 * Explanation: The longest balanced substring is "000111", which has length
 * 6.
 *
 * Example 2:
 * Input: s = "00111"
 * Output: 4
 * Explanation: The longest balanced substring is "0011", which has length 4.
 *
 * Example 3:
 * Input: s = "111"
 * Output: 0
 * Explanation: There is no balanced substring except the empty substring, so
 * the answer is 0.
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 50
 *     ∙ '0' <= s[i] <= '1'
"""
class Solution:
    def findTheLongestBalancedSubstring(self, s: str) -> int:
        ret = zeros = ones = 0
        for char in s:
            if char == "0":
                if ones > 0:
                    ret = max(ret, min(zeros, ones) * 2)
                    ones = 0
                    zeros = 0
                zeros += 1
                ones = 0
            else:
                ones += 1
        ret = max(ret, min(zeros, ones) * 2)
        return ret

if __name__ == "__main__":
    tests = (
        ("01000111", 6),
        ("0011", 4),
        ("111", 0),
    )
    for s, expected in tests:
        print(Solution().findTheLongestBalancedSubstring(s) == expected)
