"""
 * 234. Palindrome Linked List [Easy]
 * Given the head of a singly linked list, return true if it is a palindrome.
 *
 * Example 1:
 * Input: head = [1,2,2,1]
 * Output: true
 *
 * Example 2:
 * Input: head = [1,2]
 * Output: false
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [1, 10⁵].
 *     ∙ 0 <= Node.val <= 9
 *
 * Follow up: Could you do it in O(n) time and O(1) space?
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def isPalindrome(self, head: Optional[ListNode]) -> bool:
        end = head
        n = 0
        while end.next:
            n += 1
            end.next.prev, end = end, end.next
        i = 0
        while i < n:
            if head.val != end.val:
                return False
            head, end = head.next, end.prev
            i += 1
            n -= 1
        return True

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

if __name__ == "__main__":
    tests = (
        ([1,2,2,1], True),
        ([1,2], False),
    )
    for nums, expected in tests:
        print(Solution().isPalindrome(list_to_linked_list(nums)) == expected)
