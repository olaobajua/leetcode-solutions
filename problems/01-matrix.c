/*
 * 542. 01 Matrix (Medium)
 * Given an m x n binary matrix mat, return the distance of the nearest 0 for each cell.
 * The distance between two adjacent cells is 1.
 *
 * Example 1:
 * Input: mat = [[0,0,0],[0,1,0],[0,0,0]]
 * Output: [[0,0,0],[0,1,0],[0,0,0]]
 *
 * Example 2:
 * Input: mat = [[0,0,0],[0,1,0],[1,1,1]]
 * Output: [[0,0,0],[0,1,0],[1,2,1]]
 *
 * Constraints:
 *     m == mat.length
 *     n == mat[i].length
 *     1 <= m, n <= 10⁴
 *     1 <= m * n <= 10⁴
 *     mat[i][j] is either 0 or 1.
 *     There is at least one 0 in mat.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int** updateMatrix(int** mat, int matSize, int* matColSize, int* returnSize, int** returnColumnSizes);

int main() {
    int m[10][10] = {{1,0,1,1,0,0,1,0,0,1},
                     {0,1,1,0,1,0,1,0,1,1},
                     {0,0,1,0,1,0,0,1,0,0},
                     {1,0,1,0,1,1,1,1,1,1},
                     {0,1,0,1,1,0,0,0,0,1},
                     {0,0,1,0,1,1,1,0,1,0},
                     {0,1,0,1,0,1,0,0,1,1},
                     {1,0,0,0,1,1,1,1,0,1},
                     {1,1,1,1,1,1,1,0,1,0},
                     {1,1,1,1,0,1,0,0,1,1}};
    const size_t width = sizeof(m) / sizeof(m[0]);
    const size_t height = sizeof(m[0]) / sizeof(m[0][0]);
    int **mat = (int**)malloc(width * sizeof(int*));
    int *mcs = (int*)malloc(width * sizeof(int));
    for (int x = 0; x < width; ++x) {
        mat[x] = (int*)malloc(height * sizeof(int));
        mcs[x] = height;
        memcpy(mat[x], m[x], height * sizeof(int));
    }
    int nWidth = 0;
    int *nHeights = NULL;
    int **nmatrix = updateMatrix(mat, width, mcs, &nWidth, &nHeights);
    for (int x = 0; x < nWidth; ++x) {
        for (int y = 0; y < nHeights[x]; ++y) {
            printf("%d ", nmatrix[x][y]);
        }
        printf("\b\n");
        free(nmatrix[x]);
    }
    free(nmatrix);
    free(nHeights);
    free(mat);
    return EXIT_SUCCESS;
}

#define min(a,b) ((a)<(b)?(a):(b))

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
int** updateMatrix(int** mat, int matSize, int* matColSize, int* returnSize, int** returnColumnSizes) {
    int h, w = matSize;
    *returnSize = w;
    int **ret = (int**)malloc(w * sizeof(int*));
    *returnColumnSizes = (int*)malloc(w * sizeof(int));
    for (int x = 0; x < w; ++x) {
        h = matColSize[x];
        (*returnColumnSizes)[x] = h;
        ret[x] = (int*)malloc(h * sizeof(int));
        for (int y = 0; y < h; ++y) {
            int left = (x > 0) ? ret[x - 1][y] + 1 : h + w;
            int top = (y > 0) ? ret[x][y - 1] + 1 : h + w;
            ret[x][y] = (mat[x][y]) ? min(left, top) : 0;
        }
    }
    for (int x = w - 1; x >= 0; --x) {
        for (int y = h - 1; y >= 0; --y) {
            int right = (x < w - 1) ? ret[x + 1][y] + 1 : h + w;
            int bottom = (y < h - 1) ? ret[x][y + 1] + 1 : h + w;
            ret[x][y] = (mat[x][y]) ? min(min(right, bottom), ret[x][y]) : 0;
        }
    }
    return ret;
}
