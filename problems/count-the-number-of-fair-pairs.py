"""
 * 2563. Count the Number of Fair Pairs [Medium]
 * Given a 0-indexed integer array nums of size n and two integers lower and
 * upper, return the number of fair pairs.
 *
 * A pair (i, j) is fair if:
 *     ∙ 0 <= i < j < n, and
 *     ∙ lower <= nums[i] + nums[j] <= upper
 *
 * Example 1:
 * Input: nums = [0,1,7,4,4,5], lower = 3, upper = 6
 * Output: 6
 * Explanation: There are 6 fair pairs: (0,3), (0,4), (0,5), (1,3), (1,4), and
 * (1,5).
 *
 * Example 2:
 * Input: nums = [1,7,9,2,5], lower = 11, upper = 11
 * Output: 1
 * Explanation: There is a single fair pair: (2,3).
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ nums.length == n
 *     ∙ -10⁹ <= nums[i] <= 10⁹
 *     ∙ -10⁹ <= lower <= upper <= 10⁹
"""
from bisect import bisect_left, bisect_right
from collections import defaultdict
from typing import List

class Solution:
    def countFairPairs(self, nums: List[int], lower: int, upper: int) -> int:
        nums = sorted(nums)
        ret = 0
        lo = hi = len(nums) - 1
        for i, x in enumerate(nums):
            lo = min(max(lo, i + 1), hi)
            while x + nums[lo] >= lower and lo > i:
                lo -= 1
            while x + nums[hi] > upper and hi > i:
                hi -= 1
            if hi < lo:
                break
            ret += hi - lo
        return ret

if __name__ == "__main__":
    tests = (
        ([0,1,7,4,4,5], 3, 6, 6),
        ([1,7,9,2,5], 11, 11, 1),
        ([1], 5, 10, 0),
        ([-2,-6,4,0,-1000000000,-1000000000,-1000000000,-1000000000], -15, 15, 6),
        ([0,0,0,0,0,0], 0, 0, 15),
    )
    for nums, lower, upper, expected in tests:
        print(Solution().countFairPairs(nums, lower, upper) == expected)
