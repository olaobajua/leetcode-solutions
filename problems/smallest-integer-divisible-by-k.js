/**
 * 1015. Smallest Integer Divisible by K (Medium)
 * Given a positive integer k, you need to find the length of the smallest
 * positive integer n such that n is divisible by k, and n only contains the
 * digit 1.
 *
 * Return the length of n. If there is no such n, return -1.
 *
 * Note: n may not fit in a 64-bit signed integer.
 *
 * Example 1:
 * Input: k = 1
 * Output: 1
 * Explanation: The smallest answer is n = 1, which has length 1.
 *
 * Example 2:
 * Input: k = 2
 * Output: -1
 * Explanation: There is no such positive integer n divisible by 2.
 *
 * Example 3:
 * Input: k = 3
 * Output: 3
 * Explanation: The smallest answer is n = 111, which has length 3.
 *
 * Constraints:
 *     1 <= k <= 10⁵
 */

/**
 * @param {number} k
 * @return {number}
 */
var smallestRepunitDivByK = function(k) {
    let i = 1, length = 1;
    const remainders = new Set();
    while (i % k && !remainders.has(i % k)) {
        remainders.add(i % k);
        // if x % k = y % k, then (x * a + b) % k = (y * a + b) % k
        // so (10 * i + 1) % k == (10 * (i % k) + 1) % k
        // https://en.wikipedia.org/wiki/Modular_arithmetic#Properties
        i = 10*(i % k) + 1;
        ++length
    }
    return i % k ? -1 : length;
};

const tests = [
    [1, 1],
    [2, -1],
    [3, 3],
    [19, 18],
];

for (const [k, expected] of tests) {
    console.log(smallestRepunitDivByK(k) == expected);
}
