"""
 * 522. Longest Uncommon Subsequence II (Medium)
 * Given an array of strings strs, return the length of the longest uncommon
 * subsequence between them. If the longest uncommon subsequence does not
 * exist, return -1.
 *
 * An uncommon subsequence between an array of strings is a string that is a
 * subsequence of one string but not the others.
 *
 * A subsequence of a string s is a string that can be obtained after deleting
 * any number of characters from s.
 *
 *     For example, "abc" is a subsequence of "aebdc" because you can delete
 *     the underlined characters in "aebdc" to get "abc". Other subsequences of
 *     "aebdc" include "aebdc", "aeb", and "" (empty string).
 *
 * Example 1:
 * Input: strs = ["aba","cdc","eae"]
 * Output: 3
 *
 * Example 2:
 * Input: strs = ["aaa","aaa","aa"]
 * Output: -1
 *
 * Constraints:
 *     1 <= strs.length <= 50
 *     1 <= strs[i].length <= 10
 *     strs[i] consists of lowercase English letters.
"""
from typing import List

def is_subsequence(sub, string):
    string = iter(string)  # we can only remove, not reorder letters
    return all(letter in string for letter in sub)

class Solution:
    def findLUSlength(self, strs: List[str]) -> int:
        def is_uncommon(s):
            return sum(is_subsequence(s, st) for st in strs) == 1  # for itself

        # maximum uncommon subsequence in string is string itself;
        # if string is common, then any subsequence in it is common too
        return max((len(s) for s in strs if is_uncommon(s)), default=-1)

if __name__ == "__main__":
    tests = (
        (["aba","cdc","eae"], 3),
        (["aaa","aaa","aa"], -1),
        (["aaa","acb"], 3),
        (["bca","acb"], 3),
    )
    for strs, expected in tests:
        print(Solution().findLUSlength(strs) == expected)

