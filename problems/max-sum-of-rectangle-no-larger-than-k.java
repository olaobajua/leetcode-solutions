/**
 * 363. Max Sum of Rectangle No Larger Than K [Hard]
 * Given an m x n matrix matrix and an integer k, return the max sum of a
 * rectangle in the matrix such that its sum is no larger than k.
 *
 * It is guaranteed that there will be a rectangle with a sum no larger than
 * k.
 *
 * Example 1:
 * Input: matrix = [[1,0,1],[0,-2,3]], k = 2
 * Output: 2
 * Explanation: Because the sum of the blue rectangle [[0, 1], [-2, 3]] is 2,
 * and 2 is the max number no larger than k (k = 2).
 *
 * Example 2:
 * Input: matrix = [[2,2,-1]], k = 3
 * Output: 3
 *
 * Constraints:
 *     ∙ m == matrix.length
 *     ∙ n == matrix[i].length
 *     ∙ 1 <= m, n <= 100
 *     ∙ -100 <= matrix[i][j] <= 100
 *     ∙ -10⁵ <= k <= 10⁵
 *
 * Follow up: What if the number of rows is much larger than the number of
 * columns?
 */
class Solution {
    public int maxSumSubmatrix(int[][] matrix, int k) {
        final int m = matrix.length;
        final int n = matrix[0].length;
        int s[][] = new int[m+1][n+1];
        int ret = -100000;
        for (int r = 1; r <= m; ++r) {
            for (int c = 1; c <= n; ++c) {
                s[r][c] = s[r][c-1] + s[r-1][c] - s[r-1][c-1] + matrix[r-1][c-1];
                for (int i = 0; i < r; ++i) {
                    for (int j = 0; j < c; ++j) {
                        final int rec = s[r][c] - s[r][j] - s[i][c] + s[i][j];
                        if (ret < rec && rec <= k)
                            ret = rec;
                    }
                }
            }
        }
        return ret;
    }
}
