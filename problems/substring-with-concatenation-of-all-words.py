"""
 * 30. Substring with Concatenation of All Words [Hard]
 * You are given a string s and an array of strings words of the same length.
 * Return all starting indices of substring(s) in s that is a concatenation of
 * each word in words exactly once, in any order, and without any intervening
 * characters.
 *
 * You can return the answer in any order.
 *
 * Example 1:
 * Input: s = "barfoothefoobarman", words = ["foo","bar"]
 * Output: [0,9]
 * Explanation: Substrings starting at index 0 and 9 are "barfoo" and "foobar"
 * respectively.
 * The output order does not matter, returning [9,0] is fine too.
 *
 * Example 2:
 * Input: s = "wordgoodgoodgoodbestword", words =
 * ["word","good","best","word"]
 * Output: []
 *
 * Example 3:
 * Input: s = "barfoofoobarthefoobarman", words = ["bar","foo","the"]
 * Output: [6,9,12]
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁴
 *     ∙ 1 <= words.length <= 5000
 *     ∙ 1 <= words[i].length <= 30
 *     ∙ s and words[i] consist of lowercase English letters.
"""
from collections import Counter
from typing import List

class Solution:
    def findSubstring(self, s: str, words: List[str]) -> List[int]:
        word_count = Counter(words)
        n = len(words)
        m = len(words[0])
        ret = [];
        for i in range(len(s) - n * m + 1):
            count = word_count.copy()
            for j in range(i, i + n * m, m):
                ss = s[j:j+m]
                if ss in count:
                    count[ss] -= 1
                    if count[ss] == 0:
                        count.pop(ss)
                    if not count:
                        ret.append(i)
                        break
                else:
                    break
        return ret

if __name__ == "__main__":
    tests = (
        ("barfoothefoobarman", ["foo","bar"], [0,9]),
        ("wordgoodgoodgoodbestword", ["word","good","best","word"], []),
        ("barfoofoobarthefoobarman", ["bar","foo","the"], [6,9,12]),
        ("wordgoodgoodgoodbestword", ["word","good","best","good"], [8]),
    )
    for s, words, expected in tests:
        print(Solution().findSubstring(s, words) == expected)
