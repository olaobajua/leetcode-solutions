/**
 * 1379. Find a Corresponding Node of a Binary Tree in a Clone of That
 * Tree (Medium)
 * Given two binary trees original and cloned and given a reference to a node
 * target in the original tree.
 *
 * The cloned tree is a copy of the original tree.
 *
 * Return a reference to the same node in the cloned tree.
 *
 * Note that you are not allowed to change any of the two trees or the target
 * node and the answer must be a reference to a node in the cloned tree.
 *
 * Example 1:
 * Input: tree = [7,4,3,null,null,6,19], target = 3
 * Output: 3
 * Explanation: In all examples the original and cloned trees are shown.
 * The target node is a green node from the original tree. The answer is
 * the yellow node from the cloned tree.
 *
 * Example 2:
 * Input: tree = [7], target =  7
 * Output: 7
 *
 * Example 3:
 * Input: tree = [8,null,6,null,5,null,4,null,3,null,2,null,1], target = 4
 * Output: 4
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ The values of the nodes of the tree are unique.
 *     ∙ target node is a node from the original tree and is not null.
 *
 * Follow up: Could you solve the problem if repeated values on the tree are
 * allowed?
 */

// Definition for a binary tree node.
function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}

/**
 * @param {TreeNode} original
 * @param {TreeNode} cloned
 * @param {TreeNode} target
 * @return {TreeNode}
 */
var getTargetCopy = function(original, cloned, target) {
    return [...findNode(cloned, target.val)].filter(node => node)[0];
};

function* findNode(root, val) {
    if (root === null) {
        yield root;
    } else if (root.val === val) {
        yield root;
    } else {
        yield* findNode(root.left, val);
        yield* findNode(root.right, val);
    }
}

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

const tests = [
    [[7,4,3,null,null,6,19], 3, 3],
    [[7], 7, 7],
    [[8,null,6,null,5,null,4,null,3,null,2,null,1], 4, 4],
];

for (const [nums, target, expected] of tests) {
    const original = buildTree(nums.slice());
    const cloned = buildTree(nums.slice());
    const tnode = [...findNode(original, target)].filter(node => node)[0];
    const ret = getTargetCopy(original, cloned, tnode);
    console.log(ret !== tnode && ret.val == expected);
}
