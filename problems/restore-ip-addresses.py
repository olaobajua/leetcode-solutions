"""
 * 93. Restore IP Addresses [Medium]
 * A valid IP address consists of exactly four integers separated by single
 * dots. Each integer is between 0 and 255 (inclusive) and cannot have leading
 * zeros.
 *     ∙ For example, "0.1.2.201" and "192.168.1.1" are valid IP addresses,
 *       but "0.011.255.245", "192.168.1.312" and "192.168@1.1" are invalid IP
 *       addresses.
 *
 * Given a string s containing only digits, return all possible valid IP
 * addresses that can be formed by inserting dots into s. You are not allowed
 * to reorder or remove any digits in s. You may return the valid IP addresses
 * in any order.
 *
 * Example 1:
 * Input: s = "25525511135"
 * Output: ["255.255.11.135","255.255.111.35"]
 *
 * Example 2:
 * Input: s = "0000"
 * Output: ["0.0.0.0"]
 *
 * Example 3:
 * Input: s = "101023"
 * Output: ["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 20
 *     ∙ s consists of digits only.
"""
from itertools import combinations
from typing import List

class Solution:
    def restoreIpAddresses(self, s: str) -> List[str]:
        n = len(s)
        ret = []
        for dots in combinations(range(1, n), 3):
            num = 0
            ip = []
            dots = [n] + list(reversed(dots))
            for i, char in enumerate(s):
                if i < dots[-1]:
                    if i and num == 0:
                        break
                    num = 10 * num + int(char)
                else:
                    ip.append(str(num))
                    num = int(char)
                    dots.pop()
                if num > 255:
                    break
            else:
                if num <= 255:
                    ret.append(".".join(ip + [str(num)]))
        return ret

if __name__ == "__main__":
    tests = (
        ("25525511135", ["255.255.11.135","255.255.111.35"]),
        ("0000", ["0.0.0.0"]),
        ("101023", ["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]),
    )
    for s, expected in tests:
        print(Solution().restoreIpAddresses(s) == expected)
