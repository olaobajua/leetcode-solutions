"""
 * 981. Time Based Key-Value Store [Medium]
 * Design a time-based key-value data structure that can store multiple values
 * for the same key at different time stamps and retrieve the key's value at a
 * certain timestamp.
 *
 * Implement the TimeMap class:
 *     ∙ TimeMap() Initializes the object of the data structure.
 *     ∙ void set(String key, String value, int timestamp) Stores the key key
 *       with the value value at the given time timestamp.
 *     ∙ String get(String key, int timestamp) Returns a value such that set
 *       was called previously, with timestamp_prev <= timestamp. If there are
 *       multiple such values, it returns the value associated with the largest
 *       timestamp_prev. If there are no values, it returns "".
 *
 * Example 1:
 * Input
 * ["TimeMap", "set", "get", "get", "set", "get", "get"]
 * [[], ["foo", "bar", 1], ["foo", 1], ["foo", 3], ["foo", "bar2", 4], ["foo",
 * 4], ["foo", 5]]
 * Output
 * [null, null, "bar", "bar", null, "bar2", "bar2"]
 *
 * Explanation
 * TimeMap timeMap = new TimeMap();
 * timeMap.set("foo", "bar", 1);  // store the key "foo" and value "bar" along
 * with timestamp = 1.
 * timeMap.get("foo", 1);         // return "bar"
 * timeMap.get("foo", 3);         // return "bar", since there is no value
 * corresponding to foo at timestamp 3 and timestamp 2, then the only value is
 * at timestamp 1 is "bar".
 * timeMap.set("foo", "bar2", 4); // store the key "foo" and value "bar2"
 * along with timestamp = 4.
 * timeMap.get("foo", 4);         // return "bar2"
 * timeMap.get("foo", 5);         // return "bar2"
 *
 * Constraints:
 *     ∙ 1 <= key.length, value.length <= 100
 *     ∙ key and value consist of lowercase English letters and digits.
 *     ∙ 1 <= timestamp <= 10⁷
 *     ∙ All the timestamps timestamp of set are strictly increasing.
 *     ∙ At most 2 * 10⁵ calls will be made to set and get.
"""
from collections import defaultdict
from sortedcontainers import SortedList

class TimeMap:
    def __init__(self):
        self.data = {}
        self.timestamps = defaultdict(SortedList)

    def set(self, key: str, value: str, timestamp: int) -> None:
        self.data[(key, timestamp)] = value
        self.timestamps[key].add(timestamp)

    def get(self, key: str, timestamp: int) -> str:
        i = self.timestamps[key].bisect_right(timestamp) - 1
        if i < 0:
            return ""
        return self.data[(key, self.timestamps[key][i])]

obj = TimeMap()
obj.set("foo", "bar", 1)
print("bar" == obj.get("foo", 1))
print("bar" == obj.get("foo", 3))
obj.set("foo", "bar2", 4)
print("bar2" == obj.get("foo", 4))
print("bar2" == obj.get("foo", 5))
print()

obj = TimeMap()
obj.set("love", "high", 10)
obj.set("love", "low", 20)
print("" == obj.get("love", 5))
print("high" == obj.get("love", 10))
print("high" == obj.get("love", 15))
print("low" == obj.get("love", 20))
print("low" == obj.get("love", 25))
