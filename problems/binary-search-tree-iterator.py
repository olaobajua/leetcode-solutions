"""
 * 173. Binary Search Tree Iterator (Medium)
 * Implement the BSTIterator class that represents an iterator over the
 * in-order traversal of a binary search tree (BST):
 *     ∙ BSTIterator(TreeNode root) Initializes an object of the BSTIterator
 *       class. The root of the BST is given as part of the constructor. The
 *       pointer should be initialized to a non-existent number smaller than
 *       any element in the BST.
 *     ∙ boolean hasNext() Returns true if there exists a number in the
 *       traversal to the right of the pointer, otherwise returns false.
 *     ∙ int next() Moves the pointer to the right, then returns the number at
 *       the pointer.
 *
 * Notice that by initializing the pointer to a non-existent smallest number,
 * the first call to next() will return the smallest element in the BST.
 *
 * You may assume that next() calls will always be valid. That is, there will
 * be at least a next number in the in-order traversal when next() is called.
 *
 * Example 1:
 * Input
 * ["BSTIterator", "next", "next", "hasNext", "next", "hasNext", "next",
 *  "hasNext", "next", "hasNext"]
 * [[[7, 3, 15, null, null, 9, 20]], [], [], [], [], [], [], [], [], []]
 * Output
 * [null, 3, 7, true, 9, true, 15, true, 20, false]
 *
 * Explanation
 * BSTIterator bSTIterator = new BSTIterator([7, 3, 15, null, null, 9, 20]);
 * bSTIterator.next();    // return 3
 * bSTIterator.next();    // return 7
 * bSTIterator.hasNext(); // return True
 * bSTIterator.next();    // return 9
 * bSTIterator.hasNext(); // return True
 * bSTIterator.next();    // return 15
 * bSTIterator.hasNext(); // return True
 * bSTIterator.next();    // return 20
 * bSTIterator.hasNext(); // return False
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁵].
 *     ∙ 0 <= Node.val <= 10⁶
 *     ∙ At most 10⁵ calls will be made to hasNext, and next.
 *
 * Follow up:
 *     ∙ Could you implement next() and hasNext() to run in average O(1) time
 *       and use O(h) memory, where h is the height of the tree?
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class BSTIterator:
    def __init__(self, root: Optional[TreeNode]):
        self.__iter = self.traverse_inorder(root)
        self.__next = None

    def next(self) -> int:
        if self.__next is None:
            return next(self.__iter).val
        else:
            ret = self.__next
            self.__next = None
            return ret

    def hasNext(self) -> bool:
        while self.__next is None:
            self.__next = next(self.__iter, TreeNode(False)).val
        return self.__next is not False

    def traverse_inorder(self, root):
        if root is None:
            return
        for node in self.traverse_inorder(root.left):
            yield node
        yield root
        for node in self.traverse_inorder(root.right):
            yield node

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None


# Your BSTIterator object will be instantiated and called as such:
root = build_tree([7,3,15,None,None,9,20])
#    7
# 3   15
#    9 20
obj = BSTIterator(root)
print(3 == obj.next())
print(7 == obj.next())
print(True == obj.hasNext())
print(9 == obj.next())
print(True == obj.hasNext())
print(15 == obj.next())
print(True == obj.hasNext())
print(20 == obj.next())
print(False == obj.hasNext())

["BSTIterator","next","next","hasNext","next","hasNext","next","hasNext","next","hasNext"]
[[],[],[],[],[],[],[],[],[],[]]
[None,3,7,True,9,True,15,True,20,False]

