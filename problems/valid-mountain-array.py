"""
 * 941. Valid Mountain Array (Easy)
 * Given an array of integers arr, return true if and only if it is a valid
 * mountain array.
 *
 * Recall that arr is a mountain array if and only if:
 *     ▪ arr.length >= 3
 *     ▪ There exists some i with 0 < i < arr.length - 1 such that:
 *         ∙ arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
 *         ∙ arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
 *
 * Example 1:
 * Input: arr = [2,1]
 * Output: false
 *
 * Example 2:
 * Input: arr = [3,5,5]
 * Output: false
 *
 * Example 3:
 * Input: arr = [0,3,2,1]
 * Output: true
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 10⁴
 *     ∙ 0 <= arr[i] <= 10⁴
"""
from typing import List

class Solution:
    def validMountainArray(self, arr: List[int]) -> bool:
        if len(arr) < 3:
            return False
        peak = prev = -1
        for i, m in enumerate(arr):
            if m <= prev:
                peak = i - 1
                break
            prev = m
        prev = arr[-1] - 1
        for i, m in reversed([*enumerate(arr)]):
            if m <= prev:
                return i + 1 == peak
            prev = m
        return 0 < peak < len(arr) - 1

if __name__ == "__main__":
    tests = (
        ([2,1], False),
        ([3,5,5], False),
        ([0,3,2,1], True),
        ([9,8,7,6,5,4,3,2,1,0], False),
        ([0,1,2,4,2,1], True),
    )
    for arr, expected in tests:
        print(Solution().validMountainArray(arr) == expected)
