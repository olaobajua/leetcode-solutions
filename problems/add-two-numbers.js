/**
 * 2. Add Two Numbers (Medium)
 * You are given two non-empty linked lists representing two non-negative
 * integers. The digits are stored in reverse order, and each of their nodes
 * contains a single digit. Add the two numbers and return the sum as a linked
 * list.
 *
 * You may assume the two numbers do not contain any leading zero, except the
 * number 0 itself.
 *
 * Example 1:
 * Input: l1 = [2,4,3], l2 = [5,6,4]
 * Output: [7,0,8]
 * Explanation: 342 + 465 = 807.
 *
 * Example 2:
 * Input: l1 = [0], l2 = [0]
 * Output: [0]
 *
 * Example 3:
 * Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * Output: [8,9,9,9,0,0,0,1]
 *
 * Constraints:
 *     ∙ The number of nodes in each linked list is in the range [1, 100].
 *     ∙ 0 <= Node.val <= 9
 *     ∙ It is guaranteed that the list represents a number that does not have
 *       leading zeros.
 */


// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2) {
    function helper(l1, l2, prev) {
        let carry = 0;
        if (prev) {
            carry = Math.floor(prev.val / 10);
            prev.val %= 10;
        }
        if (l1 && l2) {
            l1.val += l2.val + carry;
            l1.next = helper(l1.next, l2.next, l1);
            return l1;
        } else if (l1) {
            l1.val += carry;
            l1.next = helper(l1.next, l2, l1);
            return l1;
        } else if (l2) {
            l2.val += carry;
            l2.next = helper(l1, l2.next, l2);
            return l2;
        } else if (carry) {
            prev.next = new ListNode(carry);
            return prev.next;
        }
        return null;
    }
    return helper(l1, l2, null);
};

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    [[2,4,3], [5,6,4], [7,0,8]],
    [[0], [0], [0]],
    [[9,9,9,9,9,9,9], [9,9,9,9], [8,9,9,9,0,0,0,1]],
    [[2,4,9], [5,6,4,9], [7,0,4,0,1]],
    [[5], [5], [0, 1]],
];
for (const [nums1, nums2, expected] of tests) {
    const l1 = arrayToLinkedList(nums1)
    const l2 = arrayToLinkedList(nums2)
    console.log(linkedListToArray(addTwoNumbers(l1, l2)).toString() ==
                expected.toString());
}
