/**
 * 583. Delete Operation for Two Strings (Medium)
 * Given two strings word1 and word2, return the minimum number of steps
 * required to make word1 and word2 the same.
 *
 * In one step, you can delete exactly one character in either string.
 *
 * Example 1:
 * Input: word1 = "sea", word2 = "eat"
 * Output: 2
 * Explanation: You need one step to make "sea" to "ea" and another step to
 * make "eat" to "ea".
 *
 * Example 2:
 * Input: word1 = "leetcode", word2 = "etco"
 * Output: 4
 *
 * Constraints:
 *     ∙ 1 <= word1.length, word2.length <= 500
 *     ∙ word1 and word2 consist of only lowercase English letters.
 */
class Solution {
public:
    int minDistance(string word1, string word2) {
        const int n1 = word1.length();
        const int n2 = word2.length();
        vector<vector<int>> cache(n1 + 1, vector<int> (n2 + 1, 0));
        for (int i1 = n1 - 1; i1 >= 0; --i1) {
            for (int i2 = n2 - 1; i2 >= 0; --i2) {
                if (word1[i1] == word2[i2]) {
                    cache[i1][i2] = cache[i1+1][i2+1] + 1;
                } else {
                    cache[i1][i2] = max(cache[i1+1][i2], cache[i1][i2+1]);
                }
            }
        }
        return n1 + n2 - 2*cache[0][0];
    }
};
