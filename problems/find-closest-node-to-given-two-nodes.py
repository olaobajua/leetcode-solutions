"""
 * 2359. Find Closest Node to Given Two Nodes [Medium]
 * You are given a directed graph of n nodes numbered from 0 to n - 1, where
 * each node has at most one outgoing edge.
 *
 * The graph is represented with a given 0-indexed array edges of size n,
 * indicating that there is a directed edge from node i to node edges[i]. If
 * there is no outgoing edge from i, then edges[i] == -1.
 *
 * You are also given two integers node1 and node2.
 *
 * Return the index of the node that can be reached from both node1 and node2,
 * such that the maximum between the distance from node1 to that node, and from
 * node2 to that node is minimized. If there are multiple answers, return the
 * node with the smallest index, and if no possible answer exists, return -1.
 *
 * Note that edges may contain cycles.
 *
 * Example 1:
 * 0 1
 *  2
 *  3
 * Input: edges = [2,2,3,-1], node1 = 0, node2 = 1
 * Output: 2
 * Explanation: The distance from node 0 to node 2 is 1, and the distance from
 * node 1 to node 2 is 1.
 * The maximum of those two distances is 1. It can be proven that we cannot
 * get a node with a smaller maximum distance than 1, so we return node 2.
 *
 * Example 2:
 * 0
 * 1
 * 2
 * Input: edges = [1,2,-1], node1 = 0, node2 = 2
 * Output: 2
 * Explanation: The distance from node 0 to node 2 is 2, and the distance from
 * node 2 to itself is 0.
 * The maximum of those two distances is 2. It can be proven that we cannot
 * get a node with a smaller maximum distance than 2, so we return node 2.
 *
 * Constraints:
 *     ∙ n == edges.length
 *     ∙ 2 <= n <= 10⁵
 *     ∙ -1 <= edges[i] < n
 *     ∙ edges[i] != i
 *     ∙ 0 <= node1, node2 < n
"""
from typing import List

class Solution:
    def closestMeetingNode(self, edges: List[int], node1: int, node2: int) -> int:
        n = len(edges)
        node = node1
        indices1 = {}
        for i in range(n):
            if node == -1 or node in indices1:
                break
            indices1[node] = i
            node = edges[node]
        node = node2
        indices2 = {}
        for i in range(n):
            if node == -1 or node in indices2:
                break
            indices2[node] = i
            node = edges[node]
        ret = prev_distance = n
        for node in range(n):
            if node in indices1 and node in indices2:
                distance = max(indices1[node], indices2[node])
                if (distance, node) < (prev_distance, ret):
                    prev_distance = distance
                    ret = node
        return ret if ret < n else -1

if __name__ == "__main__":
    tests = (
        ([2,2,3,-1], 0, 1, 2),
        ([1,2,-1], 0, 2, 2),
        ([4,4,4,5,1,2,2], 1, 1, 1),
        ([2,0,0], 2, 0, 0),
        ([5,3,1,0,2,4,5], 3, 2, 3),
        ([-1,3,-1,1,1,3,3,-1], 0, 1, -1),
        ([4,4,8,-1,9,8,4,4,1,1], 5, 6, 1),
        ([13,42,48,40,37,28,9,1,39,7,43,20,35,44,25,3,17,11,8,49,10,34,18,12,2,23,16,24,18,30,27,46,15,22,0,38,28,19,26,29,14,-1,5,33,50,30,6,26,4,15,31], 25, 30, 25),
    )
    for edges, node1, node2, expected in tests:
        print(Solution().closestMeetingNode(edges, node1, node2) == expected)
