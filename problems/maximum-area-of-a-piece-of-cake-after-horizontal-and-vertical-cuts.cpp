/**
 * 1465. Maximum Area of a Piece of Cake After Horizontal and Vertical Cuts
 * (Medium)
 * You are given a rectangular cake of size h x w and two arrays of integers
 * horizontalCuts and verticalCuts where:
 *     ∙ horizontalCuts[i] is the distance from the top of the rectangular cake
 *       to the iᵗʰ horizontal cut and similarly, and
 *     ∙ verticalCuts[j] is the distance from the left of the rectangular cake
 *       to the jᵗʰ vertical cut.
 *
 * Return the maximum area of a piece of cake after you cut at each horizontal
 * and vertical position provided in the arrays horizontalCuts and
 * verticalCuts. Since the answer can be a large number, return this
 * modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: h = 5, w = 4, horizontalCuts = [1,2,4], verticalCuts = [1,3]
 * Output: 4
 *
 * Example 2:
 * Input: h = 5, w = 4, horizontalCuts = [3,1], verticalCuts = [1]
 * Output: 6
 *
 * Example 3:
 * Input: h = 5, w = 4, horizontalCuts = [3], verticalCuts = [3]
 * Output: 9
 *
 * Constraints:
 *     ∙ 2 <= h, w <= 10⁹
 *     ∙ 1 <= horizontalCuts.length <= min(h - 1, 10⁵)
 *     ∙ 1 <= verticalCuts.length <= min(w - 1, 10⁵)
 *     ∙ 1 <= horizontalCuts[i] < h
 *     ∙ 1 <= verticalCuts[i] < w
 *     ∙ All the elements in horizontalCuts are distinct.
 *     ∙ All the elements in verticalCuts are distinct.
 */
class Solution {
public:
    int maxArea(int h, int w, vector<int>& horizontalCuts, vector<int>& verticalCuts) {
        horizontalCuts.push_back(0);
        horizontalCuts.push_back(h);
        verticalCuts.push_back(0);
        verticalCuts.push_back(w);
        sort(horizontalCuts.begin(), horizontalCuts.end());
        sort(verticalCuts.begin(), verticalCuts.end());
        int max_horizontal_cut = 0, max_vertical_cut = 0;
        for (int i = 1; i < horizontalCuts.size(); ++i) {
            max_horizontal_cut = max(horizontalCuts[i] - horizontalCuts[i-1],
                                     max_horizontal_cut);
        }
        for (int i = 1; i < verticalCuts.size(); ++i) {
            max_vertical_cut = max(verticalCuts[i] - verticalCuts[i-1],
                                   max_vertical_cut);
        }
        return (long)max_horizontal_cut * max_vertical_cut % 1000000007;
    }
};
