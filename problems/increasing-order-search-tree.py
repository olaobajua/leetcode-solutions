"""
 * 897. Increasing Order Search Tree (Easy)
 * Given the root of a binary search tree, rearrange the tree in in-order so
 * that the leftmost node in the tree is now the root of the tree, and every
 * node has no left child and only one right child.
 *
 * Example 1:
 * Input: root = [5,3,6,2,4,null,8,1,null,null,null,7,9]
 * Output: [1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]
 *
 * Example 2:
 * Input: root = [5,1,7]
 * Output: [1,null,5,null,7]
 *
 * Constraints:
 *     The number of nodes in the given tree will be in the range [1, 100].
 *     0 <= Node.val <= 1000
"""
from typing import List

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def increasingBST(self, root: TreeNode, prev=None) -> TreeNode:
        if not root:
            return prev
        ret = self.increasingBST(root.left, root)
        root.left = None
        root.right = self.increasingBST(root.right, prev)
        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([5,3,6,2,4,None,8,1,None,None,None,7,9],
         [1,None,2,None,3,None,4,None,5,None,6,None,7,None,8,None,9]),
        ([5,1,7], [1,None,5,None,7]),
    )
    for nums, expected in tests:
        root = build_tree(nums)
        ret = [*tree_to_list(Solution().increasingBST(root))]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
