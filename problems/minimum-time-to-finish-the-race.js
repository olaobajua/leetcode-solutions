/**
 * 2188. Minimum Time to Finish the Race (Hard)
 * You are given a 0-indexed 2D integer array tires where tires[i] = [fᵢ, rᵢ]
 * indicates that the iᵗʰ tire can finish its xᵗʰ successive lap in
 * fᵢ * rᵢ⁽ˣ⁻¹⁾ seconds.
 *     ∙ For example, if fᵢ = 3 and rᵢ = 2, then the tire would finish its 1ˢᵗ
 *       lap in 3 seconds, its 2ⁿᵈ lap in 3 * 2 = 6 seconds, its 3ʳᵈ lap in
 *       3 * 2² = 12 seconds, etc.
 *
 * You are also given an integer changeTime and an integer numLaps.
 *
 * The race consists of numLaps laps and you may start the race with any tire.
 * You have an unlimited supply of each tire and after every lap, you may
 * change to any given tire (including the current tire type) if you wait
 * changeTime seconds.
 *
 * Return the minimum time to finish the race.
 *
 * Example 1:
 * Input: tires = [[2,3],[3,4]], changeTime = 5, numLaps = 4
 * Output: 21
 * Explanation:
 * Lap 1: Start with tire 0 and finish the lap in 2 seconds.
 * Lap 2: Continue with tire 0 and finish the lap in 2 * 3 = 6 seconds.
 * Lap 3: Change tires to a new tire 0 for 5 seconds and then finish the lap in
 * another 2 seconds.
 * Lap 4: Continue with tire 0 and finish the lap in 2 * 3 = 6 seconds.
 * Total time = 2 + 6 + 5 + 2 + 6 = 21 seconds.
 * The minimum time to complete the race is 21 seconds.
 *
 * Example 2:
 * Input: tires = [[1,10],[2,2],[3,4]], changeTime = 6, numLaps = 5
 * Output: 25
 * Explanation:
 * Lap 1: Start with tire 1 and finish the lap in 2 seconds.
 * Lap 2: Continue with tire 1 and finish the lap in 2 * 2 = 4 seconds.
 * Lap 3: Change tires to a new tire 1 for 6 seconds and then finish the lap in
 * another 2 seconds.
 * Lap 4: Continue with tire 1 and finish the lap in 2 * 2 = 4 seconds.
 * Lap 5: Change tires to tire 0 for 6 seconds then finish the lap in another 1 second.
 * Total time = 2 + 4 + 6 + 2 + 4 + 6 + 1 = 25 seconds.
 * The minimum time to complete the race is 25 seconds.
 *
 * Constraints:
 *     ∙ 1 <= tires.length <= 10⁵
 *     ∙ tires[i].length == 2
 *     ∙ 1 <= fᵢ, changeTime <= 10⁵
 *     ∙ 2 <= rᵢ <= 10⁵
 *     ∙ 1 <= numLaps <= 1000
 */

/**
 * @param {number[][]} tires
 * @param {number} changeTime
 * @param {number} numLaps
 * @return {number}
 */
var minimumFinishTime = function(tires, changeTime, numLaps) {
    const optimal = Array(numLaps + 1).fill(Number.MAX_SAFE_INTEGER);
    for (const [f, r] of tires) {
        let curCost = f, totalCost = f;
        for (let lap = 1; lap <= Math.min(16, numLaps); ++lap) {
            optimal[lap] = Math.min(optimal[lap], totalCost);
            curCost *= r;
            totalCost += curCost;
        }
    }
    for (let laps = 1; laps <= numLaps; ++laps) {
        for (let change = 0; change < laps - 1; ++change) {
            optimal[laps] = Math.min(optimal[laps],
                                     changeTime + optimal[change+1] +
                                     optimal[laps - (change + 1)]);
        }
    }
    return optimal[numLaps];
};

const tests = [
    [[[2,3],[3,4]], 5, 4, 21],
    [[[1,10],[2,2],[3,4]], 6, 5, 25],
    [[[36,5],[32,5],[88,8],[11,4],[52,2],[2,2],[90,5],[49,6],[68,9],[77,3],[42,7],[17,3],[73,7],[89,2],[92,9],[40,7],[71,8],[79,3],[55,6],[77,9],[14,3],[87,10],[4,2],[63,7],[79,8],[3,9],[44,2],[49,9],[91,3],[58,6],[62,3],[72,7],[97,6],[29,5],[88,9],[40,8],[36,4],[82,8],[53,8],[26,2],[26,6],[92,2],[46,2],[75,6],[85,2],[6,10],[12,4],[15,4]], 24, 27, 318],
    [[[3,4],[84,2],[63,8],[72,8],[82,7],[83,6],[23,2],[77,5],[51,10],[28,2],[47,9],[8,3],[48,3],[56,3],[8,10],[66,6],[92,9],[44,6],[23,5],[5,6],[86,9],[13,10],[91,3],[2,2],[8,4],[67,8],[63,6],[52,5],[42,10],[3,9],[66,5],[35,10],[63,6],[65,6],[22,8],[40,9],[43,4],[73,9],[81,5],[32,2],[30,5],[80,9],[50,4],[35,4],[52,7],[11,5],[7,8],[68,3],[54,8],[49,8]], 90, 87, 2526],
];
for (const [tires, changeTime, numLaps, expected] of tests) {
    console.log(minimumFinishTime(tires, changeTime, numLaps) == expected);
}
