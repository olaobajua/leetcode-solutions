"""
 * 1345. Jump Game IV (Hard)
 * Given an array of integers arr, you are initially positioned at the first
 * index of the array.
 *
 * In one step you can jump from index i to index:
 *     ∙ i + 1 where: i + 1 < arr.length.
 *     ∙ i - 1 where: i - 1 >= 0.
 *     ∙ j where: arr[i] == arr[j] and i != j.
 *
 * Return the minimum number of steps to reach the last index of the array.
 *
 * Notice that you can not jump outside of the array at any time.
 *
 * Example 1:
 * Input: arr = [100,-23,-23,404,100,23,23,23,3,404]
 * Output: 3
 * Explanation: You need three jumps from index 0 --> 4 --> 3 --> 9. Note that
 * index 9 is the last index of the array.
 *
 * Example 2:
 * Input: arr = [7]
 * Output: 0
 * Explanation: Start index is the last index. You do not need to jump.
 *
 * Example 3:
 * Input: arr = [7,6,9,6,9,6,9,7]
 * Output: 1
 * Explanation: You can jump directly from index 0 to index 7 which is last
 * index of the array.
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 5 * 10⁴
 *     ∙ -10⁸ <= arr[i] <= 10⁸
"""
from collections import defaultdict
from typing import List

class Solution:
    def minJumps(self, arr: List[int]) -> int:
        n = len(arr)
        by_value = defaultdict(list)
        for i, x in enumerate(arr):
            by_value[x].append(i)
        to_visit = [0]
        steps = 0
        arr.append(None)
        while True:
            next_visit = []
            for i in to_visit:
                if i == n - 1:
                    return steps
                num = arr[i]
                arr[i] = None  # mark visited
                if arr[i-1] != None:
                    next_visit.append(i - 1)
                if arr[i+1] != None:
                    next_visit.append(i + 1)
                for i in by_value[num]:
                    if arr[i] != None:
                        next_visit.append(i)
                by_value.pop(num)  # remove visited
            to_visit = next_visit
            steps += 1

if __name__ == "__main__":
    tests = (
        ([100,-23,-23,404,100,23,23,23,3,404], 3),
        ([7], 0),
        ([7,6,9,6,9,6,9,7], 1),
        ([6,1,9], 2),
    )
    for arr, expected in tests:
        print(Solution().minJumps(arr) == expected)
