/**
 * 387. First Unique Character in a String [Easy]
 * Given a string s, find the first non-repeating character in it and return
 * its index. If it does not exist, return -1.
 *
 * Example 1:
 * Input: s = "leetcode"
 * Output: 0
 * Example 2:
 * Input: s = "loveleetcode"
 * Output: 2
 * Example 3:
 * Input: s = "aabb"
 * Output: -1
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ s consists of only lowercase English letters.
 */

/**
 * @param {string} s
 * @return {number}
 */
var firstUniqChar = function(s) {
    count = Array(123).fill(0);
    for (let i = 0; i < s.length; ++i)
        ++count[s[i].charCodeAt()];
    for (let i = 0; i < s.length; ++i)
        if (count[s[i].charCodeAt()] == 1)
            return i;
    return -1;
};

const tests = [
    ["leetcode", 0],
    ["loveleetcode", 2],
    ["aabb", -1],
];
for (const [s, expected] of tests) {
    console.log(firstUniqChar(s) == expected);
}
