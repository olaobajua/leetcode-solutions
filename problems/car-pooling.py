"""
 * 1094. Car Pooling (Medium)
 * There is a car with capacity empty seats. The vehicle only drives east
 * (i.e., it cannot turn around and drive west).
 *
 * You are given the integer capacity and an array trips where
 * trip[i] = [numPassengersᵢ, fromᵢ, toᵢ] indicates that the iᵗʰ trip has
 * numPassengersᵢ passengers and the locations to pick them up and drop them
 * off are fromᵢ and toᵢ respectively. The locations are given as the number of
 * kilometers due east from the car's initial location.
 *
 * Return true if it is possible to pick up and drop off all passengers for all
 * the given trips, or false otherwise.
 *
 * Example 1:
 * Input: trips = [[2,1,5],[3,3,7]], capacity = 4
 * Output: false
 *
 * Example 2:
 * Input: trips = [[2,1,5],[3,3,7]], capacity = 5
 * Output: true
 *
 * Constraints:
 *     ∙ 1 <= trips.length <= 1000
 *     ∙ trips[i].length == 3
 *     ∙ 1 <= numPassengersᵢ <= 100
 *     ∙ 0 <= fromᵢ < toᵢ <= 1000
 *     ∙ 1 <= capacity <= 10⁵
"""
from typing import List

class Solution:
    def carPooling(self, trips: List[List[int]], capacity: int) -> bool:
        delta = []
        for passengers, origin, destination in trips:
            delta.append([origin, passengers])
            delta.append([destination, -passengers])
        for _, d in sorted(delta):
            capacity -= d
            if capacity < 0:
                return False
        return True

if __name__ == "__main__":
    tests = (
        ([[2,1,5],[3,3,7]], 4, False),
        ([[2,1,5],[3,3,7]], 5, True),
        ([[2,1,5],[3,5,7]], 3, True),
        ([[8,2,3],[4,1,3],[1,3,6],[8,4,6],[4,4,8]], 12, False),
        ([[9,3,4],[9,1,7],[4,2,4],[7,4,5]], 23, True),
    )
    for trips, capacity, expected in tests:
        print(Solution().carPooling(trips, capacity) == expected)
