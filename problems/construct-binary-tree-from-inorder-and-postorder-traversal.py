"""
 * 106. Construct Binary Tree from Inorder and Postorder Traversal [Medium]
 * Given two integer arrays inorder and postorder where inorder is the inorder
 * traversal of a binary tree and postorder is the postorder traversal of the
 * same tree, construct and return the binary tree.
 *
 * Example 1:
 *    3
 * 9    20
 *     15 7
 * Input: inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
 * Output: [3,9,20,null,null,15,7]
 *
 * Example 2:
 * Input: inorder = [-1], postorder = [-1]
 * Output: [-1]
 *
 * Constraints:
 *     ∙ 1 <= inorder.length <= 3000
 *     ∙ postorder.length == inorder.length
 *     ∙ -3000 <= inorder[i], postorder[i] <= 3000
 *     ∙ inorder and postorder consist of unique values.
 *     ∙ Each value of postorder also appears in inorder.
 *     ∙ inorder is guaranteed to be the inorder traversal of the tree.
 *     ∙ postorder is guaranteed to be the postorder traversal of the tree.
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def buildTree(self, inorder: List[int], postorder: List[int]) -> Optional[TreeNode]:
        if inorder and postorder:
            return TreeNode(inorder[(i:=inorder.index(postorder[-1]))],
                            self.buildTree(inorder[:i], postorder[:i]),
                            self.buildTree(inorder[i+1:], postorder[i:-1]))

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([9,3,15,20,7], [9,15,7,20,3], [3,9,20,None,None,15,7]),
        ([-1], [-1], [-1]),
    )
    for inorder, postorder, expected in tests:
        ret = [*tree_to_list(Solution().buildTree(inorder, postorder))]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
