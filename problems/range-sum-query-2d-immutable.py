"""
 * 304. Range Sum Query 2D - Immutable (Medium)
 * Given a 2D matrix matrix, handle multiple queries of the following type:
 *     ∙ Calculate the sum of the elements of matrix inside the rectangle
 *       defined by its upper left corner (row1, col1) and lower right corner
 *       (row2, col2).
 *
 * Implement the NumMatrix class:
 *     ∙ NumMatrix(int[][] matrix) Initializes the object with the integer
 *       matrix matrix.
 *     ∙ int sumRegion(int row1, int col1, int row2, int col2) Returns the sum
 *       of the elements of matrix inside the rectangle defined by its upper
 *       left corner (row1, col1) and lower right corner (row2, col2).
 *
 * Example 1:
 * Input
 * ["NumMatrix", "sumRegion", "sumRegion", "sumRegion"]
 * [[[[3, 0, 1, 4, 2],
 *    [5, 6, 3, 2, 1],
 *    [1, 2, 0, 1, 5],
 *    [4, 1, 0, 1, 7],
 *    [1, 0, 3, 0, 5]]],
 *  [2, 1, 4, 3], [1, 1, 2, 2], [1, 2, 2, 4]]
 * Output
 * [null, 8, 11, 12]
 *
 * Explanation
 * NumMatrix numMatrix = new NumMatrix([[3, 0, 1, 4, 2],
 *                                      [5, 6, 3, 2, 1],
 *                                      [1, 2, 0, 1, 5],
 *                                      [4, 1, 0, 1, 7],
 *                                      [1, 0, 3, 0, 5]]);
 * numMatrix.sumRegion(2, 1, 4, 3); // return 8
 * numMatrix.sumRegion(1, 1, 2, 2); // return 11
 * numMatrix.sumRegion(1, 2, 2, 4); // return 12
 *
 * Constraints:
 *     ∙ m == matrix.length
 *     ∙ n == matrix[i].length
 *     ∙ 1 <= m, n <= 200
 *     ∙ -10⁵ <= matrix[i][j] <= 10⁵
 *     ∙ 0 <= row1 <= row2 < m
 *     ∙ 0 <= col1 <= col2 < n
 *     ∙ At most 10⁴ calls will be made to sumRegion.
"""
from typing import List

class NumMatrix:
    def __init__(self, matrix: List[List[int]]):
        R = len(matrix)
        C = len(matrix[0])
        s = [[0]*(C + 1) for _ in range(R + 1)]
        for r in range(R):
            for c in range(C):
                s[r][c] = s[r][c-1] + s[r-1][c] + matrix[r][c] - s[r-1][c-1]
        self.sum = s

    def sumRegion(self, row1: int, col1: int, row2: int, col2: int) -> int:
        return (self.sum[row2][col2] + self.sum[row1-1][col1-1] -
                self.sum[row2][col1-1] - self.sum[row1-1][col2])

obj = NumMatrix([[3, 0, 1, 4, 2],
                 [5, 6, 3, 2, 1],
                 [1, 2, 0, 1, 5],
                 [4, 1, 0, 1, 7],
                 [1, 0, 3, 0, 5]])
# obj.sumRegion(row1, col1, row2, col2)
print(obj.sumRegion(2, 1, 4, 3) == 8)
print(obj.sumRegion(1, 1, 2, 2) == 11)
print(obj.sumRegion(1, 2, 2, 4) == 12)

obj = NumMatrix([[-1]])
print(obj.sumRegion(0, 0, 0, 0) == -1)
