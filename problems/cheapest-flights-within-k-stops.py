"""
 * 787. Cheapest Flights Within K Stops [Medium]
 * There are n cities connected by some number of flights. You are given an
 * array flights where flights[i] = [fromᵢ, toᵢ, priceᵢ] indicates that there
 * is a flight from city fromᵢ to city toᵢ with cost priceᵢ.
 *
 * You are also given three integers src, dst, and k, return the cheapest
 * price from src to dst with at most k stops. If there is no such route,
 * return -1.
 *
 * Example 1:
 * Input: n = 4,
 *        flights = [[0,1,100],[1,2,100],[2,0,100],[1,3,600],[2,3,200]],
 *        src = 0,
 *        dst = 3,
 *        k = 1
 * Output: 700
 *
 * Example 2:
 * Input: n = 3,
 *        flights = [[0,1,100],[1,2,100],[0,2,500]],
 *        src = 0,
 *        dst = 2,
 *        k = 1
 * Output: 200
 *
 * Example 3:
 * Input: n = 3,
 *        flights = [[0,1,100],[1,2,100],[0,2,500]],
 *        src = 0,
 *        dst = 2,
 *        k = 0
 * Output: 500
 *
 * Constraints:
 *     ∙ 1 <= n <= 100
 *     ∙ 0 <= flights.length <= (n * (n - 1) / 2)
 *     ∙ flights[i].length == 3
 *     ∙ 0 <= fromᵢ, toᵢ < n
 *     ∙ fromᵢ != toᵢ
 *     ∙ 1 <= priceᵢ <= 10⁴
 *     ∙ There will not be any multiple flights between two cities.
 *     ∙ 0 <= src, dst, k < n
 *     ∙ src != dst
"""
from functools import cache
from math import inf
from typing import List

class Solution:
    def findCheapestPrice(self, n: int, flights: List[List[int]], src: int, dst: int, k: int) -> int:
        @cache
        def dfs(i, left):
            if i == dst:
                return 0
            if left < 0 and i != dst:
                return inf
            ret = inf
            for ds, price in graph[i]:
                ret = min(ret, dfs(ds, left - 1) + price)
            return ret

        graph = [[] for _ in range(n)]
        for s, d, p in flights:
            graph[s].append((d, p))
        ret = dfs(src, k)
        return ret if ret < inf else -1

if __name__ == "__main__":
    tests = (
        (4, [[0,1,100],[1,2,100],[2,0,100],[1,3,600],[2,3,200]], 0, 3, 1, 700),
        (3, [[0,1,100],[1,2,100],[0,2,500]], 0, 2, 1, 200),
        (3, [[0,1,100],[1,2,100],[0,2,500]], 0, 2, 0, 500),
    )
    for n, flights, src, dst, k, expected in tests:
        print(Solution().findCheapestPrice(n, flights, src, dst, k) == expected)
