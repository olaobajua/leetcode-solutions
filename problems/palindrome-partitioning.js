/**
 * 131. Palindrome Partitioning (Medium)
 * Given a string s, partition s such that every substring of the partition is
 * a palindrome. Return all possible palindrome partitioning of s.
 *
 * A palindrome string is a string that reads the same backward as forward.
 *
 * Example 1:
 * Input: s = "aab"
 * Output: [["a","a","b"],["aa","b"]]
 *
 * Example 2:
 * Input: s = "a"
 * Output: [["a"]]
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 16
 *     ∙ s contains only lowercase English letters.
 */

/**
 * @param {string} s
 * @return {string[][]}
 */
var partition = function(s) {
    if (s.length) {
        const ret = [];
        for (let substrLen = 1; substrLen <= s.length; ++substrLen) {
            const cur = s.slice(0, substrLen);
            if (isPalindrome(cur)) {
                for (const pals of partition(s.slice(substrLen))) {
                    ret.push([cur, ...pals]);
                }
            }
        }
        return ret;
    } else {
        return [[]];
    }
};

function isPalindrome(str) {
    for (let i = Math.floor(str.length / 2) + 1; i >= 0; --i) {
        if (str[i] !== str[str.length - i - 1]) {
            return false;
        }
    }
    return true;
}

const tests = [
    ["a", [["a"]]],
    ["aab", [["a","a","b"],["aa","b"]]],
    ["aaa", [["a","a","a"],["a","aa"],["aa","a"],["aaa"]]],
    ["aabb", [["a","a","b","b"],["a","a","bb"],["aa","b","b"],["aa","bb"]]],
    ["aaab", [["a","a","a","b"],["a","aa","b"],["aa","a","b"],["aaa","b"]]],
];

for (const [s, expected] of tests) {
    console.log(partition(s).sort().toString() == expected.sort().toString());
}
