"""
 * 1420. Build Array Where You Can Find The Maximum Exactly K Comparisons
 * [Hard]
 * You are given three integers n, m and k. Consider the following algorithm
 * to find the maximum element of an array of positive integers:
 * You should build the array arr which has the following properties:
 *     ∙ arr has exactly n integers.
 *     ∙ 1 <= arr[i] <= m where (0 <= i < n).
 *     ∙ After applying the mentioned algorithm to arr, the value search_cost
 *       is equal to k.
 *
 * Return the number of ways to build the array arr under the mentioned
 * conditions. As the answer may grow large, the answer must be computed modulo
 * 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 2, m = 3, k = 1
 * Output: 6
 * Explanation: The possible arrays are [1, 1], [2, 1], [2, 2], [3, 1], [3, 2]
 * [3, 3]
 *
 * Example 2:
 * Input: n = 5, m = 2, k = 3
 * Output: 0
 * Explanation: There are no possible arrays that satisify the mentioned
 * conditions.
 *
 * Example 3:
 * Input: n = 9, m = 1, k = 1
 * Output: 1
 * Explanation: The only possible array is [1, 1, 1, 1, 1, 1, 1, 1, 1]
 *
 * Constraints:
 *     ∙ 1 <= n <= 50
 *     ∙ 1 <= m <= 100
 *     ∙ 0 <= k <= n
"""
from functools import cache

class Solution:
    def numOfArrays(self, n: int, m: int, k: int) -> int:
        @cache
        def dfs(i, max_val, k):
            if i == n:
                return 1 if k == 0 else 0

            if k < 0:
                return 0

            ret = 1
            for val in range(1, m + 1):
                ret += dfs(i + 1, max(max_val, val), k - (max_val < val))
                ret %= 1000000007

            return ret - 1

        return dfs(0, 0, k)

if __name__ == "__main__":
    tests = (
        (2, 3, 1, 6),
        (5, 2, 3, 0),
        (9, 1, 1, 1),
    )
    for n, m, k, expected in tests:
        print(Solution().numOfArrays(n, m, k) == expected)
