"""
 * 352. Data Stream as Disjoint Intervals [Hard]
 * Given a data stream input of non-negative integers a₁, a₂, ..., aₙ,
 * summarize the numbers seen so far as a list of disjoint intervals.
 *
 * Implement the SummaryRanges class:
 *     ∙ SummaryRanges() Initializes the object with an empty stream.
 *     ∙ void addNum(int value) Adds the integer value to the stream.
 *     ∙ int[][] getIntervals() Returns a summary of the integers in the
 *       stream currently as a list of disjoint intervals [startᵢ, endᵢ].
 *       The answer should be sorted by startᵢ.
 *
 * Example 1:
 * Input
 * ["SummaryRanges", "addNum", "getIntervals", "addNum", "getIntervals",
 * "addNum", "getIntervals", "addNum", "getIntervals", "addNum",
 * "getIntervals"]
 * [[], [1], [], [3], [], [7], [], [2], [], [6], []]
 * Output
 * [null, null, [[1, 1]], null, [[1, 1], [3, 3]], null, [[1, 1], [3, 3], [7,
 * 7]], null, [[1, 3], [7, 7]], null, [[1, 3], [6, 7]]]
 *
 * Explanation
 * SummaryRanges summaryRanges = new SummaryRanges();
 * summaryRanges.addNum(1);      // arr = [1]
 * summaryRanges.getIntervals(); // return [[1, 1]]
 * summaryRanges.addNum(3);      // arr = [1, 3]
 * summaryRanges.getIntervals(); // return [[1, 1], [3, 3]]
 * summaryRanges.addNum(7);      // arr = [1, 3, 7]
 * summaryRanges.getIntervals(); // return [[1, 1], [3, 3], [7, 7]]
 * summaryRanges.addNum(2);      // arr = [1, 2, 3, 7]
 * summaryRanges.getIntervals(); // return [[1, 3], [7, 7]]
 * summaryRanges.addNum(6);      // arr = [1, 2, 3, 6, 7]
 * summaryRanges.getIntervals(); // return [[1, 3], [6, 7]]
 *
 * Constraints:
 *     ∙ 0 <= value <= 10⁴
 *     ∙ At most 3 * 10⁴ calls will be made to addNum and getIntervals.
 *
 * Follow up: What if there are lots of merges and the number of disjoint
 * intervals is small compared to the size of the data stream?
"""
from sortedcontainers import SortedList
from typing import List

class SummaryRanges:
    def __init__(self):
        self.__intervals = SortedList()

    def addNum(self, value: int) -> None:
        modified_prev = modified_next = False
        i = self.__intervals.bisect_left([value, value])
        if i > 0:
            _, end = self.__intervals[i-1]
            if end + 1 == value:
                self.__intervals[i-1][1] = value
                modified_prev = True
            if end >= value:
                return
        if i < len(self.__intervals):
            start, _ = self.__intervals[i]
            if start - 1 == value:
                self.__intervals[i][0] = value
                modified_next = True
            if start == value:
                return
        if modified_prev and modified_next:
            _, end = self.__intervals.pop(i)
            self.__intervals[i-1][1] = end
        elif not modified_prev and not modified_next:
            self.__intervals.add([value, value])

    def getIntervals(self) -> List[List[int]]:
        return list(self.__intervals)

obj = SummaryRanges()
obj.addNum(1)
print([[1, 1]] == obj.getIntervals())
obj.addNum(3)
print([[1, 1], [3, 3]] == obj.getIntervals())
obj.addNum(7)
print([[1, 1], [3, 3], [7, 7]] == obj.getIntervals())
obj.addNum(2)
print([[1, 3], [7, 7]] == obj.getIntervals())
obj.addNum(6)
print([[1, 3], [6, 7]] == obj.getIntervals())

print()
obj = SummaryRanges()
obj.addNum(6)
print(obj.getIntervals() == [[6,6]])
obj.addNum(6)
print(obj.getIntervals() == [[6,6]])
obj.addNum(0)
print(obj.getIntervals() == [[0,0],[6,6]])
obj.addNum(4)
print(obj.getIntervals() == [[0,0],[4,4],[6,6]])
obj.addNum(8)
print(obj.getIntervals() == [[0,0],[4,4],[6,6],[8,8]])
obj.addNum(7)
print(obj.getIntervals() == [[0,0],[4,4],[6,8]])
obj.addNum(6)
print(obj.getIntervals() == [[0,0],[4,4],[6,8]])
obj.addNum(4)
print(obj.getIntervals() == [[0,0],[4,4],[6,8]])
obj.addNum(7)
print(obj.getIntervals() == [[0,0],[4,4],[6,8]])
obj.addNum(5)
print(obj.getIntervals() == [[0,0],[4,8]])
