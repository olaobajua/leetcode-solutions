/**
 * 2007. Find Original Array From Doubled Array [Medium]
 * An integer array original is transformed into a doubled array changed by
 * appending twice the value of every element in original, and then randomly
 * shuffling the resulting array.
 *
 * Given an array changed, return original if changed is a doubled array. If
 * changed is not a doubled array, return an empty array. The elements in
 * original may be returned in any order.
 *
 * Example 1:
 * Input: changed = [1,3,4,2,6,8]
 * Output: [1,3,4]
 * Explanation: One possible original array could be [1,3,4]:
 * - Twice the value of 1 is 1 * 2 = 2.
 * - Twice the value of 3 is 3 * 2 = 6.
 * - Twice the value of 4 is 4 * 2 = 8.
 * Other original arrays could be [4,3,1] or [3,1,4].
 *
 * Example 2:
 * Input: changed = [6,3,0,1]
 * Output: []
 * Explanation: changed is not a doubled array.
 *
 * Example 3:
 * Input: changed = [1]
 * Output: []
 * Explanation: changed is not a doubled array.
 *
 * Constraints:
 *     ∙ 1 <= changed.length <= 10⁵
 *     ∙ 0 <= changed[i] <= 10⁵
 */
class Solution {
    public int[] findOriginalArray(int[] changed) {
        int max = 0;
        for (int num : changed)
            max = Math.max(max, num);
        int[] count = new int[2*max+1];
        for (int x : changed)
            ++count[x];

        int[] ret = new int[changed.length/2];
        if (count[0] % 2 > 0)
            return new int[0];
        int n = count[0] / 2;
        for(int x = 1; x < max + 1; ++x) {
            count[2*x] -= count[x];
            if (count[2*x] < 0)
                return new int[0];
            while (count[x]-- > 0)
                ret[n++] = x;
        }
        return ret;
    }
}
