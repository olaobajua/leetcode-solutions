"""
 * 978. Longest Turbulent Subarray (Medium)
 * Given an integer array arr, return the length of a maximum size turbulent
 * subarray of arr.
 *
 * A subarray is turbulent if the comparison sign flips between each adjacent
 * pair of elements in the subarray.
 *
 * More formally, a subarray [arr[i], arr[i + 1], ..., arr[j]] of arr is said
 * to be turbulent if and only if:
 *     For i <= k < j:
 *         arr[k] > arr[k + 1] when k is odd, and
 *         arr[k] < arr[k + 1] when k is even.
 *     Or, for i <= k < j:
 *         arr[k] > arr[k + 1] when k is even, and
 *         arr[k] < arr[k + 1] when k is odd.
 *
 * Example 1:
 * Input: arr = [9,4,2,10,7,8,8,1,9]
 * Output: 5
 * Explanation: arr[1] > arr[2] < arr[3] > arr[4] < arr[5]
 *
 * Example 2:
 * Input: arr = [4,8,12,16]
 * Output: 2
 *
 * Example 3:
 * Input: arr = [100]
 * Output: 1
 *
 * Constraints:
 *     1 <= arr.length <= 4 * 10⁴
 *     0 <= arr[i] <= 10⁹
"""
from typing import List

class Solution:
    def maxTurbulenceSize(self, arr: List[int]) -> int:
        max_len = 1
        counter = 1
        prev_cmp = -2
        for a, b in zip(arr, arr[1:]):
            counter = counter + 1 if prev_cmp and prev_cmp == -cmp(a, b) else 1 + (a != b)
            max_len = max(max_len, counter)
            prev_cmp = cmp(a, b)
        return max_len

def cmp(a, b):
    return (a > b) - (a < b) 

if __name__ == "__main__":
    tests = (
        ([9,4,2,10,7,8,8,1,9], 5),
        ([4,8,12,16], 2),
        ([100], 1),
        ([9,9], 1),
        ([100,100,100], 1),
    )
    for arr, expected in tests:
        print(Solution().maxTurbulenceSize(arr) == expected)
