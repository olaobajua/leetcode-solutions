"""
 * 1129. Shortest Path with Alternating Colors [Medium]
 * You are given an integer n, the number of nodes in a directed graph where
 * the nodes are labeled from 0 to n - 1. Each edge is red or blue in this
 * graph, and there could be self-edges and parallel edges.
 *
 * You are given two arrays redEdges and blueEdges where:
 *     ∙ redEdges[i] = [aᵢ, bᵢ] indicates that there is a directed red edge
 *       from node aᵢ to node bᵢ in the graph, and
 *     ∙ blueEdges[j] = [uⱼ, vⱼ] indicates that there is a directed blue edge
 *       from node uⱼ to node vⱼ in the graph.
 *
 * Return an array answer of length n, where each answer[x] is the length of
 * the shortest path from node 0 to node x such that the edge colors alternate
 * along the path, or -1 if such a path does not exist.
 *
 * Example 1:
 * Input: n = 3, redEdges = [[0,1],[1,2]], blueEdges = []
 * Output: [0,1,-1]
 *
 * Example 2:
 * Input: n = 3, redEdges = [[0,1]], blueEdges = [[2,1]]
 * Output: [0,1,-1]
 *
 * Constraints:
 *     ∙ 1 <= n <= 100
 *     ∙ 0 <= redEdges.length, blueEdges.length <= 400
 *     ∙ redEdges[i].length == blueEdges[j].length == 2
 *     ∙ 0 <= aᵢ, bᵢ, uⱼ, vⱼ < n
"""
from collections import defaultdict
from typing import List

class Solution:
    def shortestAlternatingPaths(self, n: int, redEdges: List[List[int]], blueEdges: List[List[int]]) -> List[int]:
        ret = [-1] * n
        red_graph = defaultdict(list)
        for a, b in redEdges:
            red_graph[a].append(b)
        blue_graph = defaultdict(list)
        for a, b in blueEdges:
            blue_graph[a].append(b)

        to_visit = []
        if 0 in red_graph:
            to_visit.append((0, 0, True))
        if 0 in blue_graph:
            to_visit.append((0, 0, False))

        seen = set()
        for node, distance, is_red in to_visit:
            if ret[node] == -1:
                ret[node] = distance
            if (node, is_red) not in seen:
                seen.add((node, is_red))
                graph = red_graph if is_red else blue_graph
                for child in graph[node]:
                    to_visit.append((child, distance + 1, not is_red))
        ret[0] = 0
        return ret

if __name__ == "__main__":
    tests = (
        (3, [[0,1],[1,2]], [], [0,1,-1]),
        (3, [[0,1]], [[2,1]], [0,1,-1]),
        (3, [[1,0]], [[2,1]], [0,-1,-1]),
        (3, [[0,1],[0,2]], [[1,0]], [0,1,1]),
        (5, [[0,1],[1,2],[2,3],[3,4]], [[1,2],[2,3],[3,1]], [0,1,2,3,7]),
    )
    for n, redEdges, blueEdges, expected in tests:
        print(Solution().shortestAlternatingPaths(n, redEdges, blueEdges) == expected)
