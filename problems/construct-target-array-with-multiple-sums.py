"""
 * 1354. Construct Target Array With Multiple Sums (Hard)
 * You are given an array target of n integers. From a starting array arr
 * consisting of n 1's, you may perform the following procedure :
 *     ∙ let x be the sum of all elements currently in your array.
 *     ∙ choose index i, such that 0 <= i < n and set the value of arr at index
 *       i to x.
 *     ∙ You may repeat this procedure as many times as needed.
 *
 * Return true if it is possible to construct the target array from arr,
 * otherwise, return false.
 *
 * Example 1:
 * Input: target = [9,3,5]
 * Output: true
 * Explanation: Start with arr = [1, 1, 1]
 * [1, 1, 1], sum = 3 choose index 1
 * [1, 3, 1], sum = 5 choose index 2
 * [1, 3, 5], sum = 9 choose index 0
 * [9, 3, 5] Done
 *
 * Example 2:
 * Input: target = [1,1,1,2]
 * Output: false
 * Explanation: Impossible to create target array from [1,1,1,1].
 *
 * Example 3:
 * Input: target = [8,5]
 * Output: true
 *
 * Constraints:
 *     ∙ n == target.length
 *     ∙ 1 <= n <= 5 * 10⁴
 *     ∙ 1 <= target[i] <= 10⁹
"""
from heapq import heapify, heappop, heappush
from typing import List

class Solution:
    def isPossible(self, target: List[int]) -> bool:
        target = [-x for x in target]
        heapify(target)
        s = sum(target)
        x = -1
        while x < 0:
            x = heappop(target)
            if x == -1:
                return True
            s -= x
            if s == x or s == 0:
                return False
            if x // s:
                x = x % s if x % s else s
            else:
                x -= s
            s += x
            heappush(target, x)
        return False

if __name__ == "__main__":
    tests = (
        ([9,3,5], True),  #  [9,5,3] -> [1,5,3] -> [1,1,3] -> [1,1,1]
        ([1,1,1,2], False),
        ([8,5], True),  # [8,5] -> [3,5] -> [3,2] -> [1,2] -> [1,1]
        ([1,1,2], False),
        ([2], False),
        ([1,1000000000], True),
    )
    for target, expected in tests:
        print(Solution().isPossible(target) == expected)
