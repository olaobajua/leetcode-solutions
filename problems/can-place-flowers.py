"""
 * 605. Can Place Flowers (Easy)
 * You have a long flowerbed in which some of the plots are planted, and some
 * are not. However, flowers cannot be planted in adjacent plots.
 *
 * Given an integer array flowerbed containing 0's and 1's, where 0 means empty
 * and 1 means not empty, and an integer n, return if n new flowers can be
 * planted in the flowerbed without violating the no-adjacent-flowers rule.
 *
 * Example 1:
 * Input: flowerbed = [1,0,0,0,1], n = 1
 * Output: true
 *
 * Example 2:
 * Input: flowerbed = [1,0,0,0,1], n = 2
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= flowerbed.length <= 2 * 10⁴
 *     ∙ flowerbed[i] is 0 or 1.
 *     ∙ There are no two adjacent flowers in flowerbed.
 *     ∙ 0 <= n <= flowerbed.length
"""
from itertools import groupby
from typing import List

class Solution:
    def canPlaceFlowers(self, flowerbed: List[int], n: int) -> bool:
        return n <= sum((len([*group]) - 1) // 2
                        for place, group in groupby([0] + flowerbed + [0])
                        if place == 0)

if __name__ == "__main__":
    tests = (
        ([1,0,0,0,1], 1, True),
        ([1,0,0,0,1], 2, False),
        ([0,0,1,0,1], 1, True),
        ([1,0,0,0,1,0,0], 2, True),
        ([0,1,0], 1, False),
    )
    for flowerbed, n, expected in tests:
        print(Solution().canPlaceFlowers(flowerbed, n) == expected)
