/**
 * 222. Count Complete Tree Nodes (Medium)
 * Given the root of a complete binary tree, return the number of the nodes in
 * the tree.
 *
 * According to Wikipedia, every level, except possibly the last, is completely
 * filled in a complete binary tree, and all nodes in the last level are as far
 * left as possible. It can have between 1 and 2^h nodes inclusive at the last
 * level h.
 *
 * Design an algorithm that runs in less than O(n) time complexity.
 *
 * Example 1:
 * Input: root = [1,2,3,4,5,6]
 * Output: 6
 *
 * Example 2:
 * Input: root = []
 * Output: 0
 *
 * Example 3:
 * Input: root = [1]
 * Output: 1
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [0, 5 * 10⁴].
 *     0 <= Node.val <= 5 * 10⁴
 *     The tree is guaranteed to be complete.
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @return {number}
 */
var countNodes = function(root) {
    return [...treeToList(root)].length;
};

function* treeToList(root) {
    let nodes = [root];
    for (let n of nodes) {
        if (n) {
            yield n.val;
            nodes.push(n.left, n.right);
        }
    }
}

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

tests = [
    [[1,2,3,4,5,6], 6],
    [[], 0],
    [[1], 1],
];
for (let [nums, expected] of tests) {
    console.log(countNodes(buildTree(nums)) == expected);
}
