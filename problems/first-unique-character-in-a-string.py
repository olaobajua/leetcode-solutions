"""
 * 387. First Unique Character in a String [Easy]
 * Given a string s, find the first non-repeating character in it and return
 * its index. If it does not exist, return -1.
 *
 * Example 1:
 * Input: s = "leetcode"
 * Output: 0
 * Example 2:
 * Input: s = "loveleetcode"
 * Output: 2
 * Example 3:
 * Input: s = "aabb"
 * Output: -1
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 10⁵
 *     ∙ s consists of only lowercase English letters.
"""
from collections import Counter

class Solution:
    def firstUniqChar(self, s: str) -> int:
        count = Counter(s)
        for i, c in enumerate(s):
            if count[c] == 1:
                return i
        return -1

if __name__ == "__main__":
    tests = (
        ("leetcode", 0),
        ("loveleetcode", 2),
        ("aabb", -1),
    )
    for s, expected in tests:
        print(Solution().firstUniqChar(s) == expected)
