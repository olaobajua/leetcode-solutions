"""
 * 1146. Snapshot Array [Medium]
 * Implement a SnapshotArray that supports the following interface:
 *     ∙ SnapshotArray(int length) initializes an array-like data structure
 *       with the given length. Initially, each element equals 0.
 *     ∙ void set(index, val) sets the element at the given index to be equal
 *       to val.
 *     ∙ int snap() takes a snapshot of the array and returns the snap_id: the
 *       total number of times we called snap() minus 1.
 *     ∙ int get(index, snap_id) returns the value at the given index, at the
 *       time we took the snapshot with the given snap_id
 *
 * Example 1:
 * Input: ["SnapshotArray","set","snap","set","get"]
 * [[3],[0,5],[],[0,6],[0,0]]
 * Output: [null,null,0,null,5]
 * Explanation:
 * SnapshotArray snapshotArr = new SnapshotArray(3); // set the length to be 3
 * snapshotArr.set(0,5);  // Set array[0] = 5
 * snapshotArr.snap();  // Take a snapshot, return snap_id = 0
 * snapshotArr.set(0,6);
 * snapshotArr.get(0,0);  // Get the value of array[0] with snap_id = 0,
 * return 5
 *
 * Constraints:
 *     ∙ 1 <= length <= 5 * 10⁴
 *     ∙ 0 <= index < length
 *     ∙ 0 <= val <= 10⁹
 *     ∙ 0 <= snap_id < (the total number of times we call snap())
 *     ∙ At most 5 * 10⁴ calls will be made to set, snap, and get.
"""
from bisect import bisect_left

class SnapshotArray:
    def __init__(self, length: int):
        self.arr = [{0: 0} for _ in range(length)]
        self.snap_id = 0

    def set(self, index: int, val: int) -> None:
        self.arr[index][self.snap_id] = val

    def snap(self) -> int:
        self.snap_id += 1
        return self.snap_id - 1

    def get(self, index: int, snap_id: int) -> int:
        snap_ids = list(self.arr[index].keys())
        i = min(bisect_left(snap_ids, snap_id), len(snap_ids) - 1)
        if snap_id < snap_ids[i]:
            i -= 1
        return self.arr[index][snap_ids[i]]

obj = SnapshotArray(3)
obj.set(0, 5)
obj.snap()
obj.set(0, 6)
print(5 == obj.get(0, 0))

obj = SnapshotArray(4)
obj.snap()
obj.snap()
obj.get(3, 1)
obj.set(2, 4)
obj.snap()
obj.set(1, 4)

obj = SnapshotArray(3)
obj.set(1, 6)
obj.snap()
obj.snap()
obj.set(1, 19)
obj.set(0, 4)
print(0 == obj.get(2, 1))
print(0 == obj.get(2, 0))
print(0 == obj.get(0, 1))
