/**
 * 118. Pascal's Triangle [Easy]
 * Given an integer numRows, return the first numRows of Pascal's triangle.
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly
 * above it as shown:
 * Example 1:
 * Input: numRows = 5
 * Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
 * Example 2:
 * Input: numRows = 1
 * Output: [[1]]
 *
 * Constraints:
 *     ∙ 1 <= numRows <= 30
 */

/**
 * @param {number} numRows
 * @return {number[][]}
 */
var generate = function(numRows) {
    const ret = [];
    let level = [1];
    while (numRows--) {
        ret.push(level);
        level = [0, ...level, 0];
        level = level.map((x, i) => x + level[i+1]);
        level.pop();
    }
    return ret;
};

const tests = [
    [5, [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]],
    [1, [[1]]],
];

for (const [numRows, expected] of tests) {
    console.log(JSON.stringify(generate(numRows)) == JSON.stringify(expected));
}
