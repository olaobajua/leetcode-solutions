"""
 * 501. Find Mode in Binary Search Tree [Easy]
 * Given the root of a binary search tree (BST) with duplicates, return all
 * the mode(s) (https://en.wikipedia.org/wiki/Mode_(statistics)) (i.e., the
 * most frequently occurred element) in it.
 *
 * If the tree has more than one mode, return them in any order.
 *
 * Assume a BST is defined as follows:
 *     ∙ The left subtree of a node contains only nodes with keys less than or
 *       equal to the node's key.
 *     ∙ The right subtree of a node contains only nodes with keys greater
 *       than or equal to the node's key.
 *     ∙ Both the left and right subtrees must also be binary search trees.
 *
 * Example 1:
 *  1
 *    2
 *   2
 * Input: root = [1,null,2,2]
 * Output: [2]
 *
 * Example 2:
 * Input: root = [0]
 * Output: [0]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ -10⁵ <= Node.val <= 10⁵
 *
 * Follow up: Could you do that without using any extra space? (Assume that
 * the implicit stack space incurred due to recursion does not count).
"""
from collections import Counter
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def findMode(self, root: Optional[TreeNode]) -> List[int]:
        def dfs(root):
            if root:
                count[root.val] += 1
                dfs(root.left)
                dfs(root.right)

        count = Counter()
        dfs(root)
        m = max(count.values())
        return [k for k, v in count.items() if v == m]

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,None,2,2], [2]),
        ([0], [0]),
    )
    for nums, expected in tests:
        root = build_tree(nums)
        print(Solution().findMode(root) == expected)
