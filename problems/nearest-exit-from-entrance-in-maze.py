"""
 * 1926. Nearest Exit from Entrance in Maze [Medium]
 * You are given an m x n matrix maze (0-indexed) with empty cells
 * (represented as '.') and walls (represented as '+'). You are also given the
 * entrance of the maze, where entrance = [entranceᵣₒᵥᵥ, entrance꜀ₒₗ] denotes
 * the row and column of the cell you are initially standing at.
 *
 * In one step, you can move one cell up, down, left, or right. You cannot
 * step into a cell with a wall, and you cannot step outside the maze. Your
 * goal is to find the nearest exit from the entrance. An exit is defined as an
 * empty cell that is at the border of the maze. The entrance does not count as
 * an exit.
 *
 * Return the number of steps in the shortest path from the entrance to the
 * nearest exit, or -1 if no such path exists.
 *
 * Example 1:
 * Input: maze = [["+","+",".","+"],
 *                [".",".",".","+"],
 *                ["+","+","+","."]],
 * entrance = [1,2]
 * Output: 1
 * Explanation: There are 3 exits in this maze at [1,0], [0,2], and [2,3].
 * Initially, you are at the entrance cell [1,2].
 * - You can reach [1,0] by moving 2 steps left.
 * - You can reach [0,2] by moving 1 step up.
 * It is impossible to reach [2,3] from the entrance.
 * Thus, the nearest exit is [0,2], which is 1 step away.
 *
 * Example 2:
 * Input: maze = [["+","+","+"],
 *                [".",".","."],
 *                ["+","+","+"]],
 * entrance = [1,0]
 * Output: 2
 * Explanation: There is 1 exit in this maze at [1,2].
 * [1,0] does not count as an exit since it is the entrance cell.
 * Initially, you are at the entrance cell [1,0].
 * - You can reach [1,2] by moving 2 steps right.
 * Thus, the nearest exit is [1,2], which is 2 steps away.
 *
 * Example 3:
 * Input: maze = [[".","+"]], entrance = [0,0]
 * Output: -1
 * Explanation: There are no exits in this maze.
 *
 * Constraints:
 *     ∙ maze.length == m
 *     ∙ maze[i].length == n
 *     ∙ 1 <= m, n <= 100
 *     ∙ maze[i][j] is either '.' or '+'.
 *     ∙ entrance.length == 2
 *     ∙ 0 <= entranceᵣₒᵥᵥ < m
 *     ∙ 0 <= entrance꜀ₒₗ < n
 *     ∙ entrance will always be an empty cell.
"""
from typing import List

class Solution:
    def nearestExit(self, maze: List[List[str]], entrance: List[int]) -> int:
        erow, ecol = entrance
        maze[erow][ecol] = "s"
        m = len(maze)
        n = len(maze[0])
        visited = set((ecol, erow))
        for r, c, s in (to_visit := [(erow, ecol, 0)]):
            if (r in (0, m - 1) or c in (0, n - 1)) and maze[r][c] == ".":
                return s
            for nr, nc in (r+1, c), (r-1, c), (r, c+1), (r, c-1):
                if 0 <= nr < m and 0 <= nc < n and maze[nr][nc] != "+":
                    if (nr, nc) not in visited:
                        to_visit.append((nr, nc, s + 1))
                        visited.add((nr, nc))
        return -1

if __name__ == "__main__":
    tests = (
        ([["+","+",".","+"],
          [".",".",".","+"],
          ["+","+","+","."]],
         [1,2], 1),
        ([["+","+","+"],
          [".",".","."],
          ["+","+","+"]],
         [1,0], 2),
        ([[".","+"]],
         [0,0], -1),
        ([["+",".","+","+","+","+","+"],
          ["+",".","+",".",".",".","+"],
          ["+",".","+",".","+",".","+"],
          ["+",".",".",".","+",".","+"],
          ["+","+","+","+","+","+","."]],
        [0,1], -1),
    )
    for maze, entrance, expected in tests:
        print(Solution().nearestExit(maze, entrance) == expected)
