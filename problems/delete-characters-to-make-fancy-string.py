"""
 * 1957. Delete Characters to Make Fancy String (Easy)
 * A fancy string is a string where no three consecutive characters are equal.
 * Given a string s, delete the minimum possible number of characters from s
 * to make it fancy.
 * Return the final string after the deletion. It can be shown that the answer
 * will always be unique.
 *
 * Example 1:
 * Input: s = "leeetcode"
 * Output: "leetcode"
 * Explanation:
 * Remove an 'e' from the first group of 'e's to create "leetcode".
 * No three consecutive characters are equal, so return "leetcode".
 *
 * Example 2:
 * Input: s = "aaabaaaa"
 * Output: "aabaa"
 * Explanation:
 * Remove an 'a' from the first group of 'a's to create "aabaaaa".
 * Remove two 'a's from the second group of 'a's to create "aabaa".
 * No three consecutive characters are equal, so return "aabaa".
 *
 * Example 3:
 * Input: s = "aab"
 * Output: "aab"
 * Explanation: No three consecutive characters are equal, so return "aab".
 *
 * Constraints:
 *
 *     1 <= s.length <= 10⁵
 *     s consists only of lowercase English letters.
"""
class Solution:
    def makeFancyString(self, s: str) -> str:
        counter = 0
        prev = None
        substring = []
        for l in s:
            if l == prev:
                counter += 1
            else:
                counter = 0
            if counter < 2:
                substring.append(l)
            prev = l
        return ''.join(substring)

if __name__ == "__main__":
    tests = (
        ("leeetcode", "leetcode"),
        ("aaabaaaa", "aabaa"),
        ("aab", "aab"),
    )
    for s, expected in tests:
        print(Solution().makeFancyString(s) == expected)
