/**
 * 450. Delete Node in a BST (Medium)
 * Given a root node reference of a BST and a key, delete the node with the
 * given key in the BST. Return the root node reference (possibly updated) of
 * the BST.
 *
 * Basically, the deletion can be divided into two stages:
 *     Search for a node to remove.
 *     If the node is found, delete the node.
 *
 * Example 1:
 * Input: root = [5,3,6,2,4,null,7], key = 3
 * Output: [5,4,6,2,null,null,7]
 * Explanation: Given key to delete is 3. So we find the node with value 3 and
 * delete it.
 * One valid answer is [5,4,6,2,null,null,7], shown in the above BST.
 * Please notice that another valid answer is [5,2,6,null,4,null,7] and it's
 * also accepted.
 *
 * Example 2:
 * Input: root = [5,3,6,2,4,null,7], key = 0
 * Output: [5,3,6,2,4,null,7]
 * Explanation: The tree does not contain a node with value = 0.
 *
 * Example 3:
 * Input: root = [], key = 0
 * Output: []
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [0, 10⁴].
 *     -10⁵ <= Node.val <= 10⁵
 *     Each node has a unique value.
 *     root is a valid binary search tree.
 *     -10⁵ <= key <= 10⁵
 *
 * Follow up: Could you solve it with time complexity O(height of tree)?
 */

// Definition for a binary tree node.
function TreeNode(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

/**
 * @param {TreeNode} root
 * @param {number} key
 * @return {TreeNode}
 */
var deleteNode = function(root, key) {
    if (root !== null) {
        if (root.val == key) {
            if (root.left === root.right) {
                return null;
            } else if (root.left === null) {
                return root.right;
            } else if (root.right === null) {
                return root.left;
            } else {
                return addNode(root.right, root.left);
            }
        } else if (root.val < key) {
            root.right = deleteNode(root.right, key);
        } else {
            root.left = deleteNode(root.left, key);
        }
    }
    return root;
};

function addNode(root, child) {
    if (root === null) {
        return child;
    } else if (child.val < root.val) {
        root.left = addNode(root.left, child);
    } else {
        root.right = addNode(root.right, child);
    }
    return root;
}

function buildTree(vals) {
    let root = null;
    let v = vals.shift();
    if (v === undefined) { return root; }
    root = new TreeNode(v);
    let nodes = [root];
    for (let n of nodes) {
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.left = new TreeNode(v);
            nodes.push(n.left);
        }
        v = vals.shift()
        if (v === undefined) { return root; }
        if (v !== null) {
            n.right = new TreeNode(v);
            nodes.push(n.right);
        }
    }
}

function* treeToList(root) {
    let nodes = [root];
    for (let n of nodes) {
        yield n ? n.val : null;
        if (n) {
            nodes.push(n.left, n.right);
        }
    }
}

const tests = [
    [[5,3,6,2,4,null,7], 3, [5,4,6,2,null,null,7]],
    [[5,3,6,2,4,null,7], 0, [5,3,6,2,4,null,7]],
    [[], 0, []],
];

for (const [nums, key, expected] of tests) {
    const root = buildTree(nums);
    const ret = Array.from(treeToList(deleteNode(root, key)));
    while (ret && ret[ret.length-1] === null) {
        ret.pop();
    }
    console.log(ret.every((n, i) => n == expected[i]));
}
