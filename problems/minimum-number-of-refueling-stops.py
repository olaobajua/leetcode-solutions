"""
 * 871. Minimum Number of Refueling Stops [Hard]
 * A car travels from a starting position to a destination which is target
 * miles east of the starting position.
 *
 * There are gas stations along the way. The gas stations are represented as
 * an array stations where stations[i] = [positionᵢ, fuelᵢ] indicates that the
 * iᵗʰ gas station is positionᵢ miles east of the starting position and has
 * fuelᵢ liters of gas.
 *
 * The car starts with an infinite tank of gas, which initially has startFuel
 * liters of fuel in it. It uses one liter of gas per one mile that it drives.
 * When the car reaches a gas station, it may stop and refuel, transferring all
 * the gas from the station into the car.
 *
 * Return the minimum number of refueling stops the car must make in order to
 * reach its destination. If it cannot reach the destination, return -1.
 *
 * Note that if the car reaches a gas station with 0 fuel left, the car can
 * still refuel there. If the car reaches the destination with 0 fuel left, it
 * is still considered to have arrived.
 *
 * Example 1:
 * Input: target = 1, startFuel = 1, stations = []
 * Output: 0
 * Explanation: We can reach the target without refueling.
 *
 * Example 2:
 * Input: target = 100, startFuel = 1, stations = [[10,100]]
 * Output: -1
 * Explanation: We can not reach the target (or even the first gas station).
 *
 * Example 3:
 * Input: target = 100,
 *        startFuel = 10,
 *        stations = [[10,60],[20,30],[30,30],[60,40]]
 * Output: 2
 * Explanation: We start with 10 liters of fuel.
 * We drive to position 10, expending 10 liters of fuel. We refuel from 0
 * liters to 60 liters of gas.
 * Then, we drive from position 10 to position 60 (expending 50 liters of
 * fuel),
 * and refuel from 10 liters to 50 liters of gas.  We then drive to and reach
 * the target.
 * We made 2 refueling stops along the way, so we return 2.
 *
 * Constraints:
 *     ∙ 1 <= target, startFuel <= 10⁹
 *     ∙ 0 <= stations.length <= 500
 *     ∙ 0 <= positionᵢ <= positionᵢ₊₁ < target
 *     ∙ 1 <= fuelᵢ < 10⁹
"""
from sortedcontainers import SortedList
from typing import List

class Solution:
    def minRefuelStops(self, target: int, startFuel: int, stations: List[List[int]]) -> int:
        skipped = SortedList()
        refuels = cur_pos = 0
        for pos, fuel in stations + [[target, 0]]:
            while skipped and cur_pos + startFuel < pos:
                startFuel += skipped.pop()
                refuels += 1
            if cur_pos + startFuel < pos:
                return -1
            startFuel -= pos - cur_pos
            cur_pos = pos
            skipped.add(fuel)
        return refuels

if __name__ == "__main__":
    tests = (
        (1, 1, [], 0),
        (100, 1, [[10,100]], -1),
        (100, 10, [[10,60],[20,30],[30,30],[60,40]], 2),
        (100, 1, [], -1),
        (100, 50, [[25,25],[50,50]], 1),
        (1000000, 8663, [[31,195796],[42904,164171],[122849,139112],[172890,121724],[182747,90912],[194124,112994],[210182,101272],[257242,73097],[284733,108631],[369026,25791],[464270,14596],[470557,59420],[491647,192483],[516972,123213],[577532,184184],[596589,143624],[661564,154130],[705234,100816],[721453,122405],[727874,6021],[728786,19444],[742866,2995],[807420,87414],[922999,7675],[996060,32691]], 6),
    )
    for target, startFuel, stations, expected in tests:
        print(Solution().minRefuelStops(target, startFuel, stations) ==
              expected)
