/**
 * 84. Largest Rectangle in Histogram (Hard)
 * Given an array of integers heights representing the histogram's bar height
 * where the width of each bar is 1, return the area of the largest rectangle
 * in the histogram.
 *
 * Example 1:
 * Input: heights = [2,1,5,6,2,3]
 * Output: 10
 * Explanation: The above is a histogram where width of each bar is 1.
 * The largest rectangle is shown in the red area, which has an area = 10 units.
 *
 * Example 2:
 * Input: heights = [2,4]
 * Output: 4
 *
 * Constraints:
 *     1 <= heights.length <= 10⁵
 *     0 <= heights[i] <= 10⁴
 */

/**
 * @param {number[]} heights
 * @return {number}
 */
var largestRectangleArea = function(heights) {
    heights.push(0);
    let maxRect = 0;
    const stack = [];
    heights.forEach((height, i) => {
        while (stack.length && height < heights[stack[stack.length-1]]) {
            const h = heights[stack.pop()];
            const w = stack.length ? i - stack[stack.length-1] - 1 : i;
            maxRect = Math.max(maxRect, h * w);
        }
        stack.push(i);
    });
    return maxRect;
};

const tests = [
    [[2,1,5,6,2,3], 10],
    [[2,1,5,6,2,3,2,2], 12],
    [[2,4], 4],
    [[0], 0],
    [[1], 1],
];
for (const [heights, expected] of tests) {
    console.log(largestRectangleArea(heights) == expected);
}
