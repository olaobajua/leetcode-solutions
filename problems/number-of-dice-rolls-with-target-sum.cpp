/**
 * 1155. Number of Dice Rolls With Target Sum [Medium]
 * You have n dice, and each die has k faces numbered from 1 to k.
 *
 * Given three integers n, k, and target, return the number of possible ways
 * (out of the kⁿ total ways) to roll the dice, so the sum of the face-up
 * numbers equals target. Since the answer may be too large, return it modulo
 * 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 1, k = 6, target = 3
 * Output: 1
 * Explanation: You throw one die with 6 faces.
 * There is only one way to get a sum of 3.
 *
 * Example 2:
 * Input: n = 2, k = 6, target = 7
 * Output: 6
 * Explanation: You throw two dice, each with 6 faces.
 * There are 6 ways to get a sum of 7: 1+6, 2+5, 3+4, 4+3, 5+2, 6+1.
 *
 * Example 3:
 * Input: n = 30, k = 30, target = 500
 * Output: 222616187
 * Explanation: The answer must be returned modulo 10⁹ + 7.
 *
 * Constraints:
 *     ∙ 1 <= n, k <= 30
 *     ∙ 1 <= target <= 1000
 */
#include <iostream>
#include <unordered_map>

class Solution {
public:
    std::vector<std::vector<int>> memo{31, std::vector<int>(1000, -1)};

    int numRollsToTarget(int n, int k, int target) {
        return dp(n, target, k);
    }

    int dp(int dice_left, int target_left, int k) {
        if (dice_left <= 0) {
            return target_left == 0;
        }
        if (target_left <= 0) {
            return dice_left == 0;
        }

        if (memo[dice_left][target_left] > -1) {
            return memo[dice_left][target_left];
        }

        int ret = 0;
        for (int i = 1; i <= k; ++i) {
            ret += dp(dice_left - 1, target_left - i, k);
            ret %= 1000000007;
        }

        memo[dice_left][target_left] = ret;
        return ret;
    };
};
