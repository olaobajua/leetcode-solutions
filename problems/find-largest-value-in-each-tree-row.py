"""
 * 515. Find Largest Value in Each Tree Row [Medium]
 * Given the root of a binary tree, return an array of the largest value in
 * each row of the tree (0-indexed).
 *
 * Example 1:
 *    1
 *  3   2
 * 5 3   9
 * Input: root = [1,3,2,5,3,null,9]
 * Output: [1,3,9]
 *
 * Example 2:
 *  1
 * 2 3
 * Input: root = [1,2,3]
 * Output: [1,3]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree will be in the range [0, 10⁴].
 *     ∙ -2³¹ <= Node.val <= 2³¹ - 1
"""
from math import inf
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def largestValues(self, root: Optional[TreeNode]) -> List[int]:
        if root is None:
            return []
        ret = []
        level = [root]
        while level:
            next_level = []
            cur = -inf
            for node in level:
                cur = max(cur, node.val)
                if node.left:
                    next_level.append(node.left)
                if node.right:
                    next_level.append(node.right)
            ret.append(cur)
            level = next_level

        return ret

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,3,2,5,3,None,9], [1,3,9]),
        ([1,2,3], [1, 3]),
        ([], []),
    )
    for nums, expected in tests:
        root = build_tree(nums)
        print(Solution().largestValues(root) == expected)
