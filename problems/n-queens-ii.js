/**
 * 52. N-Queens II (Hard)
 * The n-queens puzzle is the problem of placing n queens on an n x n
 * chessboard such that no two queens attack each other.
 *
 * Given an integer n, return the number of distinct solutions to the n-queens
 * puzzle.
 *
 * Example 1:
 * . Q . .  . . Q .
 * . . . Q  Q . . .
 * Q . . .  . . . Q
 * . . Q .  . Q . .
 *
 * Input: n = 4
 * Output: 2
 * Explanation: There are two distinct solutions to the 4-queens puzzle
 * as shown.
 *
 * Example 2:
 * Input: n = 1
 * Output: 1
 *
 * Constraints:
 *     ∙ 1 <= n <= 9
 */

/**
 * @param {number} n
 * @return {number}
 */
var totalNQueens = function(n) {
    function dfs(i, cols, ld, rd) {
        if (i === n) { ++ret }
        for (let j = 0; j < n; ++j) {
            if (cols & 1<<j || ld & 1<<(i-j+n) || rd & 1<<(i+j)) {
                continue;
            }
            dfs(i + 1, cols ^ 1<<j, ld ^ 1<<(i-j+n), rd ^ 1<<(i+j));
        }
    }

    let ret = 0;
    dfs(0, 0, 0, 0);
    return ret;
};


const tests = [
    [4, 2],
    [1, 1],
];

for (const [n, expected] of tests) {
    console.log(totalNQueens(n) == expected);
}
