/**
 * 307. Range Sum Query - Mutable [Medium]
 * Given an integer array nums, handle multiple queries of the following types:
 *     ∙ Update the value of an element in nums.
 *     ∙ Calculate the sum of the elements of nums between indices left and
 *       right inclusive where left <= right.
 *
 * Implement the NumArray class:
 *     ∙ NumArray(int[] nums) Initializes the object with the integer array
 *       nums.
 *     ∙ void update(int index, int val) Updates the value of nums[index] to be
 *       val.
 *     ∙ int sumRange(int left, int right) Returns the sum of the elements of
 *       nums between indices left and right inclusive
 *       (i.e. nums[left] + nums[left + 1] + ... + nums[right]).
 *
 * Example 1:
 * Input
 * ["NumArray", "sumRange", "update", "sumRange"]
 * [[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]
 * Output
 * [null, 9, null, 8]
 *
 * Explanation
 * NumArray numArray = new NumArray([1, 3, 5]);
 * numArray.sumRange(0, 2); // return 1 + 3 + 5 = 9
 * numArray.update(1, 2);   // nums = [1, 2, 5]
 * numArray.sumRange(0, 2); // return 1 + 2 + 5 = 8
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 3 * 10⁴
 *     ∙ -100 <= nums[i] <= 100
 *     ∙ 0 <= index < nums.length
 *     ∙ -100 <= val <= 100
 *     ∙ 0 <= left <= right < nums.length
 *     ∙ At most 3 * 10⁴ calls will be made to update and sumRange.
 */

/**
 * @param {number[]} nums
 */
var NumArray = function(nums) {
    this.bit = new BIT(nums.length);
    nums.forEach((x, i) => this.bit.update(i + 1, x));
    this.nums = [0, ...nums];
};

/**
 * @param {number} index
 * @param {number} val
 * @return {void}
 */
NumArray.prototype.update = function(index, val) {
    this.bit.update(index + 1, val - this.nums[index+1]);
    this.nums[index+1] = val;
};

/**
 * @param {number} left
 * @param {number} right
 * @return {number}
 */
NumArray.prototype.sumRange = function(left, right) {
    return this.bit.query(right + 1) - this.bit.query(left);
};

/* Binary Indexed Tree implementation. */
class BIT {
    constructor(n) {
        this.sums = Array(n + 1).fill(0);
    }

    update(i, delta) {
        while (i < this.sums.length) {
            this.sums[i] += delta;
            i += i & -i;
        }
    }

    query(i) {
        let ret = 0;
        while (i > 0) {
            ret += this.sums[i];
            i -= i & -i;
        }
        return ret;
    }
}


let obj = new NumArray([1, 3, 5]);
console.log(9 == obj.sumRange(0, 2));
obj.update(1, 2);
console.log(8 == obj.sumRange(0, 2));

obj = new NumArray([-1]);
console.log(-1 == obj.sumRange(0, 0));
obj.update(0, 1);
console.log(1 == obj.sumRange(0, 0));
