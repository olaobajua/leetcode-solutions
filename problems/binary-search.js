/**
 * 704. Binary Search (Easy)
 * Given an array of integers nums which is sorted in ascending order, and an
 * integer target, write a function to search target in nums. If target exists,
 * then return its index. Otherwise, return -1.
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 * Example 1:
 * Input: nums = [-1,0,3,5,9,12], target = 9
 * Output: 4
 * Explanation: 9 exists in nums and its index is 4
 *
 * Example 2:
 * Input: nums = [-1,0,3,5,9,12], target = 2
 * Output: -1
 * Explanation: 2 does not exist in nums so return -1
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁴
 *     ∙ -10⁴ < nums[i], target < 10⁴
 *     ∙ All the integers in nums are unique.
 *     ∙ nums is sorted in ascending order.
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function(nums, target) {
    const i = bisectLeft(nums, target);
    return i < nums.length && nums[i] === target ? i : -1;
};

function bisectLeft(a, x, cmp=(a, b) => a - b, lo=0, hi=a.length-1) {
    while (lo <= hi) {
        const mid = Math.floor((hi + lo) / 2);
        if (cmp(x, a[mid]) == 0) {
            return mid;
        } else if (cmp(x, a[mid]) > 0) {
            lo = mid + 1;
        } else {
            hi = mid - 1;
        }
    }
    return lo;
}

const tests = [
    [[-1,0,3,5,9,12], 9, 4],
    [[-1,0,3,5,9,12], 2, -1],
    [[-1,0,3,5,9,12], 13, -1],
];

for (const [nums, target, expected] of tests) {
    console.log(search(nums, target) == expected);
}
