"""
 * 472. Concatenated Words [Hard]
 * Given an array of strings words (without duplicates), return all the
 * concatenated words in the given list of words.
 *
 * A concatenated word is defined as a string that is comprised entirely of at
 * least two shorter words in the given array.
 *
 * Example 1:
 * Input: words =
 * ["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"]
 * Output: ["catsdogcats","dogcatsdog","ratcatdogcat"]
 * Explanation: "catsdogcats" can be concatenated by "cats", "dog" and "cats";
 *
 * "dogcatsdog" can be concatenated by "dog", "cats" and "dog";
 * "ratcatdogcat" can be concatenated by "rat", "cat", "dog" and "cat".
 *
 * Example 2:
 * Input: words = ["cat","dog","catdog"]
 * Output: ["catdog"]
 *
 * Constraints:
 *     ∙ 1 <= words.length <= 10⁴
 *     ∙ 1 <= words[i].length <= 30
 *     ∙ words[i] consists of only lowercase English letters.
 *     ∙ All the strings of words are unique.
 *     ∙ 1 <= sum(words[i].length) <= 10⁵
"""
from functools import cache
from typing import List

class Solution:
    def findAllConcatenatedWordsInADict(self, words: List[str]) -> List[str]:
        @cache
        def dfs(word):
            for i in range(1, len(word)):
                if word[:i] in words and (word[i:] in words or dfs(word[i:])):
                    return True
            return False
        words = set(words)
        return [word for word in words if dfs(word)]

if __name__ == "__main__":
    tests = (
        (["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"],
         ["catsdogcats","dogcatsdog","ratcatdogcat"]),
        (["cat","dog","catdog"], ["catdog"]),
    )
    for words, expected in tests:
        print(set(Solution().findAllConcatenatedWordsInADict(words)) == set(expected))
