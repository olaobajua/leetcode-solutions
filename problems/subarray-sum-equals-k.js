/**
 * 560. Subarray Sum Equals K (Medium)
 * Given an array of integers nums and an integer k, return the total number of
 * continuous subarrays whose sum equals to k.
 *
 * Example 1:
 * Input: nums = [1,1,1], k = 2
 * Output: 2
 *
 * Example 2:
 * Input: nums = [1,2,3], k = 3
 * Output: 2
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 2 * 10⁴
 *     ∙ -1000 <= nums[i] <= 1000
 *     ∙ -10⁷ <= k <= 10⁷
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var subarraySum = function(nums, k) {
    const sums = new Map([[0, 1]]);
    let ret = 0, s = 0;
    nums.forEach((x, i) => {
        s += x;
        ret += sums.get(s-k) || 0;
        sums.set(s, (sums.get(s) || 0) + 1);
    });
    return ret;
};

const tests = [
    [[1,1,1], 2, 2],
    [[1,2,3], 3, 2],
    [[-1,-1,1], 0, 1],
];

for (const [nums, k, expected] of tests) {
    console.log(subarraySum(nums, k) == expected);
}
