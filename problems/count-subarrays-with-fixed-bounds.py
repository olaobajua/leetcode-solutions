"""
 * 2444. Count Subarrays With Fixed Bounds [Hard]
 * You are given an integer array nums and two integers minK and maxK.
 *
 * A fixed-bound subarray of nums is a subarray that satisfies the following
 * conditions:
 *     ∙ The minimum value in the subarray is equal to minK.
 *     ∙ The maximum value in the subarray is equal to maxK.
 *
 * Return the number of fixed-bound subarrays.
 *
 * A subarray is a contiguous part of an array.
 *
 * Example 1:
 * Input: nums = [1,3,5,2,7,5], minK = 1, maxK = 5
 * Output: 2
 * Explanation: The fixed-bound subarrays are [1,3,5] and [1,3,5,2].
 *
 * Example 2:
 * Input: nums = [1,1,1,1], minK = 1, maxK = 1
 * Output: 10
 * Explanation: Every subarray of nums is a fixed-bound subarray. There are 10
 * possible subarrays.
 *
 * Constraints:
 *     ∙ 2 <= nums.length <= 10⁵
 *     ∙ 1 <= nums[i], minK, maxK <= 10⁶
"""
from typing import List

class Solution:
    def countSubarrays(self, nums: List[int], minK: int, maxK: int) -> int:
        ret = 0
        min_idx = max_idx = out_idx = -1
        for i, x in enumerate(nums):
            if not minK <= x <= maxK:
                out_idx = i
            if x == minK:
                min_idx = i
            if x == maxK:
                max_idx = i
            ret += max(0, min(min_idx, max_idx) - out_idx)
        return ret

if __name__ == "__main__":
    tests = (
        ([1,3,5,2,7,5], 1, 5, 2),
        ([1,1,1,1], 1, 1, 10),
        ([35054,398719,945315,945315,820417,945315,35054,945315,171832,945315,35054,109750,790964,441974,552913], 35054, 945315, 81),
    )
    for nums, minK, maxK, expected in tests:
        print(Solution().countSubarrays(nums, minK, maxK) == expected)
