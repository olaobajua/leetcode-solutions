/**
 * 130. Surrounded Regions (Medium)
 * Given an m x n matrix board containing 'X' and 'O', capture all regions that
 * are 4-directionally surrounded by 'X'.
 *
 * A region is captured by flipping all 'O's into 'X's in that surrounded
 * region.
 *
 * Example 1:
 * Input: board = [["X","X","X","X"],
 *                 ["X","O","O","X"],
 *                 ["X","X","O","X"],
 *                 ["X","O","X","X"]]
 * Output: [["X","X","X","X"],
 *          ["X","X","X","X"],
 *          ["X","X","X","X"],
 *          ["X","O","X","X"]]
 * Explanation: Surrounded regions should not be on the border, which means
 * that any 'O' on the border of the board are not flipped to 'X'. Any 'O' that
 * is not on the border and it is not connected to an 'O' on the border will be
 * flipped to 'X'. Two cells are connected if they are adjacent cells connected
 * horizontally or vertically.
 *
 * Example 2:
 * Input: board = [["X"]]
 * Output: [["X"]]
 *
 * Constraints:
 *     m == board.length
 *     n == board[i].length
 *     1 <= m, n <= 200
 *     board[i][j] is 'X' or 'O'.
 */

/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var solve = function(board) {
    const n = board.length;
    const m = board[0].length;
    let borderRegions = [];
    for (let row = 0; row < n; ++row) {
        if (board[row][0] == 'O') {
            floodFillDfs(board, row, 0, 'O', 'B');
            borderRegions.push([row, 0]);
        }
        if (board[row][m-1] == 'O') {
            floodFillDfs(board, row, m - 1, 'O', 'B');
            borderRegions.push([row, m - 1]);
        }
    }
    for (let col = 0; col < m; ++col) {
        if (board[0][col] == 'O') {
            floodFillDfs(board, 0, col, 'O', 'B');
            borderRegions.push([0, col]);
        }
        if (board[n-1][col] == 'O') {
            floodFillDfs(board, n - 1, col, 'O', 'B');
            borderRegions.push([n - 1, col]);
        }
    }
    for (let row = 0; row < n; ++row) {
        for (let col = 0; col < m; ++col) {
            if (board[row][col] == 'O') {
                board[row][col] = 'X';
            }
        }
    }
    for (let [row, col] of borderRegions) {
        floodFillDfs(board, row, col, 'B', 'O');
    }
};

function floodFillDfs(matrix, row, col, o, n) {
    if (matrix[row][col] == o) {
        matrix[row][col] = n;
        if (row > 0) {
            floodFillDfs(matrix, row - 1, col, o, n);
        }
        if (row < matrix.length - 1) {
            floodFillDfs(matrix, row + 1, col, o, n);
        }
        if (col > 0) {
            floodFillDfs(matrix, row, col - 1, o, n);
        }
        if (col < matrix[row].length - 1) {
            floodFillDfs(matrix, row, col + 1, o, n);
        }
    }
}

const tests = [
    [[["X","X","X","X"],
      ["X","O","O","X"],
      ["X","X","O","X"],
      ["X","O","X","X"]],
     [["X","X","X","X"],
      ["X","X","X","X"],
      ["X","X","X","X"],
      ["X","O","X","X"]]],
    [[["X"]],
     [["X"]]],
    [[["X","O","X","O","X","O"],
      ["O","X","O","X","O","X"],
      ["X","O","X","O","X","O"],
      ["O","X","O","X","O","X"]],
     [["X","O","X","O","X","O"],
      ["O","X","X","X","X","X"],
      ["X","X","X","X","X","O"],
      ["O","X","O","X","O","X"]]],
]
for (let [board, expected] of tests) {
    solve(board);
    console.log(board.every((row, r) => row.every((cell, c) => cell == expected[r][c])));
}
