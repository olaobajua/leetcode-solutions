"""
 * 450. Delete Node in a BST (Medium)
 * Given a root node reference of a BST and a key, delete the node with the
 * given key in the BST. Return the root node reference (possibly updated) of
 * the BST.
 *
 * Basically, the deletion can be divided into two stages:
 *     Search for a node to remove.
 *     If the node is found, delete the node.
 *
 * Example 1:
 * Input: root = [5,3,6,2,4,null,7], key = 3
 * Output: [5,4,6,2,null,null,7]
 * Explanation: Given key to delete is 3. So we find the node with value 3 and
 * delete it.
 * One valid answer is [5,4,6,2,null,null,7], shown in the above BST.
 * Please notice that another valid answer is [5,2,6,null,4,null,7] and it's
 * also accepted.
 *
 * Example 2:
 * Input: root = [5,3,6,2,4,null,7], key = 0
 * Output: [5,3,6,2,4,null,7]
 * Explanation: The tree does not contain a node with value = 0.
 *
 * Example 3:
 * Input: root = [], key = 0
 * Output: []
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [0, 10⁴].
 *     -10⁵ <= Node.val <= 10⁵
 *     Each node has a unique value.
 *     root is a valid binary search tree.
 *     -10⁵ <= key <= 10⁵
 *
 * Follow up: Could you solve it with time complexity O(height of tree)?
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def deleteNode(self, root: Optional[TreeNode], key: int) -> Optional[TreeNode]:
        return del_node(root, key)

def del_node(node, key):
    if node is not None:
        if node.val == key:
            if node.left is node.right is None:
                return None;
            elif node.left is None:
                return node.right
            elif node.right is None:
                return node.left
            else:
                return add_node(node.right, node.left)
        elif node.val < key:
            node.right = del_node(node.right, key)
        else:
            node.left = del_node(node.left, key)
    return node

def add_node(node, child):
    if not node:
        return child;
    if child.val < node.val:
        node.left = add_node(node.left, child);
    else:
        node.right = add_node(node.right, child);
    return node;

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([5,3,6,2,4,None,7], 3, [5,4,6,2,None,None,7]),
        ([5,3,6,2,4,None,7], 0, [5,3,6,2,4,None,7]),
        ([], 0, []),
    )
    for nums, key, expected in tests:
        root = build_tree(nums)
        ret = [*tree_to_list(Solution().deleteNode(root, key))]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
