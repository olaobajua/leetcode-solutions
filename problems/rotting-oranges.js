/**
 * 994. Rotting Oranges (Medium)
 * You are given an m x n grid where each cell can have one of three values:
 *     0 representing an empty cell,
 *     1 representing a fresh orange, or
 *     2 representing a rotten orange.
 *
 * Every minute, any fresh orange that is 4-directionally adjacent to a rotten
 * orange becomes rotten.
 *
 * Return the minimum number of minutes that must elapse until no cell has a
 * fresh orange. If this is impossible, return -1.
 *
 * Example 1:
 * Input: grid = [[2,1,1],[1,1,0],[0,1,1]]
 * Output: 4
 *
 * Example 2:
 * Input: grid = [[2,1,1],[0,1,1],[1,0,1]]
 * Output: -1
 * Explanation: The orange in the bottom left corner (row 2, column 0) is never
 * rotten, because rotting only happens 4-directionally.
 *
 * Example 3:
 * Input: grid = [[0,2]]
 * Output: 0
 * Explanation: Since there are already no fresh oranges at minute 0, the
 * answer is just 0.
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m, n <= 10
 *     grid[i][j] is 0, 1, or 2.
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
var orangesRotting = function(grid) {
    let rotten = grid.map((row, r) => row.map((_, c) => [r, c]))
                     .flat()
                     .filter(([r, c]) => grid[r][c] == 2);
    const n = grid.length;
    const m = grid[0].length;
    let mins = -1;
    rotten.push([-1, -1]);  // next step border
    for (let [i, [r, c]] of rotten.entries()) {
        if (r < 0) {
            ++mins;
            if (i < rotten.length - 1) { rotten.push([-1, -1]) };
            continue;
        }
        for (let [nr, nc] of [[r+1, c], [r, c+1], [r-1, c], [r, c-1]]) {
            if (0 <= nr && nr < n && 0 <= nc && nc < m && grid[nr][nc] == 1) {
                grid[nr][nc] = 2;
                rotten.push([nr, nc]);
            }
        }
    }
    return (grid.flat().some(c => c == 1)) ? -1 : Math.max(0, mins);
};

const tests = [
    [[[2,1,1],[1,1,0],[0,1,1]], 4],
    [[[2,1,1],[0,1,1],[1,0,1]], -1],
    [[[0,2]], 0],
    [[[0]], 0],
    [[[2,2],[1,1],[0,0],[2,0]], 1],
];
for (const [grid, expected] of tests) {
    console.log(orangesRotting(grid) == expected);
}
