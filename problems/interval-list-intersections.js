/**
 * 986. Interval List Intersections (Medium)
 * You are given two lists of closed intervals, firstList and secondList, where
 * firstList[i] = [startᵢ, endᵢ] and secondList[j] = [startⱼ, endⱼ]. Each list
 * of intervals is pairwise disjoint and in sorted order.
 *
 * Return the intersection of these two interval lists.
 *
 * A closed interval [a, b] (with a <= b) denotes the set of real numbers x
 * with a <= x <= b.
 *
 * The intersection of two closed intervals is a set of real numbers that are
 * either empty or represented as a closed interval. For example, the
 * intersection of [1, 3] and [2, 4] is [2, 3].
 *
 * Example 1:
 * Input: firstList = [[0,2],[5,10],[13,23],[24,25]],
 *        secondList = [[1,5],[8,12],[15,24],[25,26]]
 * Output: [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]
 *
 * Example 2:
 * Input: firstList = [[1,3],[5,9]], secondList = []
 * Output: []
 *
 * Example 3:
 * Input: firstList = [], secondList = [[4,8],[10,12]]
 * Output: []
 *
 * Example 4:
 * Input: firstList = [[1,7]], secondList = [[3,10]]
 * Output: [[3,7]]
 *
 * Constraints:
 *     0 <= firstList.length, secondList.length <= 1000
 *     firstList.length + secondList.length >= 1
 *     0 <= startᵢ < endᵢ <= 10⁹
 *     endᵢ < startᵢ+1
 *     0 <= startⱼ < endⱼ <= 10⁹
 *     endⱼ < startⱼ+1
 */

/**
 * @param {number[][]} firstList
 * @param {number[][]} secondList
 * @return {number[][]}
 */
var intervalIntersection = function(firstList, secondList) {
    const ret = [];
    const I = firstList.length;
    const J = secondList.length;
    let i = 0, j = 0;
    let starti = 0, endi = -1, startj = 0, endj = -1;
    while (i < I && j < J) {
        [starti, endi] = firstList[i];
        [startj, endj] = secondList[j];
        const start = Math.max(starti, startj);
        const end = Math.min(endi, endj);
        if (end >= start) {
            ret.push([start, end]);
        }
        (endi < endj) ? ++i : ++j;
    }
    return ret;
};

const tests = [
    [[[0,2],[5,10],[13,23],[24,25]], [[1,5],[8,12],[15,24],[25,26]], [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]],
    [[[1,3],[5,9]], [], []],
    [[], [[4,8],[10,12]], []],
    [[[1,7]], [[3,10]], [[3,7]]],
    [[[8,15]], [[2,6],[8,10],[12,20]], [[8,10],[12,15]]],
];

for (let [firstList, secondList, expected] of tests) {
    expected = expected.map(pair => pair.toString());
    test: {
        for (const interval of intervalIntersection(firstList, secondList)) {
            const i = expected.indexOf(interval.toString());
            if (i > -1) {
                expected.splice(i, 1);
            } else {
                console.log(false);
                break test;
            }
        }
        console.log(expected.length == 0);
    }
}
