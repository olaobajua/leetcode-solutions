"""
 * 2095. Delete the Middle Node of a Linked List [Medium]
 * You are given the head of a linked list. Delete the middle node, and return
 * the head of the modified linked list.
 *
 * The middle node of a linked list of size n is the ⌊n / 2⌋ᵗʰ node from the
 * start using 0-based indexing, where ⌊x⌋ denotes the largest integer less
 * than or equal to x.
 *
 *     ∙ For n = 1, 2, 3, 4, and 5, the middle nodes are 0, 1, 1, 2, and 2,
 *       respectively.
 *
 * Example 1:
 * Input: head = [1,3,4,7,1,2,6]
 * Output: [1,3,4,1,2,6]
 * Explanation:
 * The above figure represents the given linked list. The indices of the nodes
 * are written below.
 * Since n = 7, node 3 with value 7 is the middle node, which is marked in
 * red.
 * We return the new list after removing this node.
 *
 * Example 2:
 * Input: head = [1,2,3,4]
 * Output: [1,2,4]
 * Explanation:
 * The above figure represents the given linked list.
 * For n = 4, node 2 with value 3 is the middle node, which is marked in red.
 *
 * Example 3:
 * Input: head = [2,1]
 * Output: [2]
 * Explanation:
 * The above figure represents the given linked list.
 * For n = 2, node 1 with value 1 is the middle node, which is marked in red.
 * Node 0 with value 2 is the only node remaining after removing node 1.
 *
 * Constraints:
 *     ∙ The number of nodes in the list is in the range [1, 10⁵].
 *     ∙ 1 <= Node.val <= 10⁵
"""
from typing import List, Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def deleteMiddle(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if head.next is None:
            return None
        slow = head
        fast = head.next
        while (fast := fast.next) and (fast := fast.next):
            slow = slow.next
        slow.next = slow.next.next
        return head

def list_to_linked_list(nums: List[int]) -> Optional[ListNode]:
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head: Optional[ListNode]) -> List[int]:
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1,3,4,7,1,2,6], [1,3,4,1,2,6]),
        ([1,2,3,4], [1,2,4]),
        ([2,1], [2]),
        ([1], []),
    )
    for nums, expected in tests:
        head = list_to_linked_list(nums)
        print(linked_list_to_list(Solution().deleteMiddle(head)) == expected)
