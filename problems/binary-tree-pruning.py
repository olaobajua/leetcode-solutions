"""
 * 814. Binary Tree Pruning [Medium]
 * Given the root of a binary tree, return the same tree where every subtree
 * (of the given tree) not containing a 1 has been removed.
 *
 * A subtree of a node node is node plus every node that is a descendant of
 * node.
 *
 * Example 1:
 *  1         1
 *    0         0
 *   0 1         1
 * Input: root = [1,null,0,0,1]
 * Output: [1,null,0,null,1]
 * Explanation:
 * Only the red nodes satisfy the property "every subtree not containing a 1".
 * The diagram on the right represents the answer.
 *
 * Example 2:
 *    1         1
 *  0   1         1
 * 0 0 0 1         1
 * Input: root = [1,0,1,0,0,0,1]
 * Output: [1,null,1,null,1]
 *
 * Example 3:
 *      1             1
 *   1     0       1     0
 *  1 1   0 1     1 1     1
 * 0
 * Input: root = [1,1,0,1,1,0,1,0]
 * Output: [1,1,0,1,1,null,1]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 200].
 *     ∙ Node.val is either 0 or 1.
"""
from functools import cache
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def pruneTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        def contains_one(n):
            if n is None:
                return False
            if not contains_one(n.left):
                n.left = None
            if not contains_one(n.right):
                n.right = None
            return n.val == 1 or contains_one(n.left) or contains_one(n.right)
        return root if contains_one(root) else None;

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([1,None,0,0,1], [1,None,0,None,1]),
        ([1,0,1,0,0,0,1], [1,None,1,None,1]),
        ([1,1,0,1,1,0,1,0], [1,1,0,1,1,None,1]),
    )
    for nums, expected in tests:
        ret = [*tree_to_list(Solution().pruneTree(build_tree(nums)))]
        while ret and ret[-1] is None:
            ret.pop()
        print(ret == expected)
