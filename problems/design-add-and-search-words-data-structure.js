/**
 * 211. Design Add and Search Words Data Structure (Medium)
 * Design a data structure that supports adding new words and finding if a
 * string matches any previously added string.
 *
 * Implement the WordDictionary class:
 *     ∙ WordDictionary() Initializes the object.
 *     ∙ void addWord(word) Adds word to the data structure, it can be matched
 *       later.
 *     ∙ bool search(word) Returns true if there is any string in the data
 *       structure that matches word or false otherwise. word may contain
 *       dots '.' where dots can be matched with any letter.
 *
 * Example:
 * Input
 * ["WordDictionary","addWord","addWord","addWord","search","search","search",
 *  "search"]
 * [[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
 * Output
 * [null,null,null,null,false,true,true,true]
 *
 * Explanation
 * WordDictionary wordDictionary = new WordDictionary();
 * wordDictionary.addWord("bad");
 * wordDictionary.addWord("dad");
 * wordDictionary.addWord("mad");
 * wordDictionary.search("pad"); // return False
 * wordDictionary.search("bad"); // return True
 * wordDictionary.search(".ad"); // return True
 * wordDictionary.search("b.."); // return True
 *
 * Constraints:
 *     ∙ 1 <= word.length <= 500
 *     ∙ word in addWord consists lower-case English letters.
 *     ∙ word in search consist of  '.' or lower-case English letters.
 *     ∙ At most 50000 calls will be made to addWord and search.
 */

var WordDictionary = function() {
    Object.defineProperty(this, 'prefixes', { value: {} });
};

/**
 * @param {string} word
 * @return {void}
 */
WordDictionary.prototype.addWord = function(word) {
    let p = this.prefixes;
    for (let l of word) {
        if (!(l in p)) {
            p[l] = {};
        }
        p = p[l];
    }
    p['\0'] = {};
};

/**
 * @param {string} word
 * @return {boolean}
 */
WordDictionary.prototype.search = function(word) {
    let prefixes = [this.prefixes];
    for (const l of word) {
        const nxt = [];
        for (const p of prefixes) {
            if (l == '.') {
                nxt.push(...Object.values(p));
            } else if (p[l] !== undefined) {
                nxt.push(p[l]);
            }
        }
        prefixes = nxt;
    }
    return prefixes.some(p => p['\0'] !== undefined);
};

/**
 * Your WordDictionary object will be instantiated and called as such:
 * var obj = new WordDictionary()
 * obj.addWord(word)
 * var param_2 = obj.search(word)
 */

let obj = new WordDictionary();
obj.addWord("bad");
obj.addWord("dad");
obj.addWord("mad");
console.log(obj.search("pad") == false);
console.log(obj.search("bad") == true);
console.log(obj.search(".ad") == true);
console.log(obj.search("b..") == true);

obj = new WordDictionary();
obj.addWord("ran");
obj.addWord("rune");
obj.addWord("runner");
obj.addWord("runs");
obj.addWord("add");
obj.addWord("adds");
obj.addWord("adder");
obj.addWord("addee");
console.log(obj.search("r.n") == true);
console.log(obj.search("ru.n.e") == false);
console.log(obj.search("add") == true);
console.log(obj.search("add.") == true);
console.log(obj.search("adde.") == true);
console.log(obj.search(".an.") == false);
console.log(obj.search("...s") == true);
console.log(obj.search("....e.") == true);
console.log(obj.search(".......") == false);
console.log(obj.search("..n.r") == false);
