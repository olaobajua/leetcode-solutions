"""
 * 212. Word Search II [Hard]
 * Given an m x n board of characters and a list of strings words, return all
 * words on the board.
 *
 * Each word must be constructed from letters of sequentially adjacent cells,
 * where adjacent cells are horizontally or vertically neighboring. The same
 * letter cell may not be used more than once in a word.
 *
 * Example 1:
 * Input: board = [["o","a","a","n"],
 *                 ["e","t","a","e"],
 *                 ["i","h","k","r"],
 *                 ["i","f","l","v"]],
 * words = ["oath","pea","eat","rain"]
 * Output: ["eat","oath"]
 *
 * Example 2:
 * Input: board = [["a","b"],
 *                 ["c","d"]],
 * words = ["abcb"]
 * Output: []
 *
 * Constraints:
 *     ∙ m == board.length
 *     ∙ n == board[i].length
 *     ∙ 1 <= m, n <= 12
 *     ∙ board[i][j] is a lowercase English letter.
 *     ∙ 1 <= words.length <= 3 * 10⁴
 *     ∙ 1 <= words[i].length <= 10
 *     ∙ words[i] consists of lowercase English letters.
 *     ∙ All the strings of words are unique.
"""
from typing import List

class Solution:
    def findWords(self, board: List[List[str]], words: List[str]) -> List[str]:
        def dfs(r, c, t):
            if "\0" in t:
                ret.append(t.pop("\0"))
            char, board[r][c] = board[r][c], "#"
            for nr, nc in (r+1, c), (r-1, c), (r, c+1), (r, c-1):
                if 0 <= nr < m and 0 <= nc < n and board[nr][nc] in t:
                    nt = t[board[nr][nc]]
                    dfs(nr, nc, nt)
                    if not nt:
                        t.pop(board[nr][nc])
            board[r][c] = char
        trie = {}
        for word in words:
            p = trie
            for c in word:
                p = p.setdefault(c, {})
            p["\0"] = word
        m = len(board)
        n = len(board[0])
        ret = []
        for row in range(m):
            for col in range(n):
                dfs(row, col, trie.get(board[row][col], {}))
        return ret

if __name__ == "__main__":
    tests = (
        ([["o","a","a","n"],
          ["e","t","a","e"],
          ["i","h","k","r"],
          ["i","f","l","v"]],
         ["oath","pea","eat","rain"], ["eat","oath"]),
        ([["a","b"],
          ["c","d"]],
         ["abcb"], []),
        ([["a"]], ["a"], ["a"]),
         ([["b","a","b","a"],
           ["b","b","b","a"],
           ["b","b","b","a"],
           ["b","a","a","a"],
           ["a","a","a","a"],
           ["a","a","a","a"]],
          ["aabaaaaab"], ["aabaaaaab"]),
    )
    for board, words, expected in tests:
        print(set(Solution().findWords(board, words)) == set(expected))
