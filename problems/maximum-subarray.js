/**
 * 53. Maximum Subarray (Easy)
 * Given an integer array nums, find the contiguous subarray (containing at
 * least one number) which has the largest sum and return its sum.
 *
 * A subarray is a contiguous part of an array.
 *
 * Example 1:
 * Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
 * Output: 6
 * Explanation: [4,-1,2,1] has the largest sum = 6.
 *
 * Example 2:
 * Input: nums = [1]
 * Output: 1
 *
 * Example 3:
 * Input: nums = [5,4,-1,7,8]
 * Output: 23
 *
 * Constraints:
 *     1 <= nums.length <= 10⁵
 *     -10⁴ <= nums[i] <= 10⁴
 *
 * Follow up: If you have figured out the O(n) solution, try coding another
 * solution using the divide and conquer approach, which is more subtle.
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function(nums) {
    let sum = 0, min = nums[0];
    for (const n of nums) {
        min = Math.max(min, sum += n);
        sum = Math.max(sum, 0);
    }
    return min;
};

const tests = [
    [[-2,1,-3,4,-1,2,1,-5,4], 6],
    [[1], 1],
    [[5,4,-1,7,8], 23],
    [[-1], -1],
    [[-2,-1], -1],
];

for (const [nums, expected] of tests) {
    console.log(maxSubArray(nums) == expected);
}
