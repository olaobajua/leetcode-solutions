"""
 * 523. Continuous Subarray Sum [Medium]
 * Given an integer array nums and an integer k, return true if nums has a
 * continuous subarray of size at least two whose elements sum up to a multiple
 * of k, or false otherwise.
 *
 * An integer x is a multiple of k if there exists an integer n such that x =
 * n * k. 0 is always a multiple of k.
 *
 * Example 1:
 * Input: nums = [23,2,4,6,7], k = 6
 * Output: true
 * Explanation: [2, 4] is a continuous subarray of size 2 whose elements sum
 * up to 6.
 *
 * Example 2:
 * Input: nums = [23,2,6,4,7], k = 6
 * Output: true
 * Explanation: [23, 2, 6, 4, 7] is an continuous subarray of size 5 whose
 * elements sum up to 42.
 * 42 is a multiple of 6 because 42 = 7 * 6 and 7 is an integer.
 *
 * Example 3:
 * Input: nums = [23,2,6,4,7], k = 13
 * Output: false
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ 0 <= nums[i] <= 10⁹
 *     ∙ 0 <= sum(nums[i]) <= 2³¹ - 1
 *     ∙ 1 <= k <= 2³¹ - 1
"""
from typing import List

class Solution:
    def checkSubarraySum(self, nums: List[int], k: int) -> bool:
        acc = prev = 0
        seen = set()
        for x in nums:
            acc += x;
            if (mod := acc % k) in seen:
                return True
            seen.add(prev)
            prev = mod
        return False

if __name__ == "__main__":
    tests = (
        ([23,2,4,6,7], 6, True),
        ([23,2,6,4,7], 6, True),
        ([23,2,6,4,7], 13, False),
        ([23,2,4,6,6], 7, True),
        ([5,0,0,0], 3, True),
        ([0], 1, False),
        ([1,0], 2, False),
        ([1,2,12], 6, False),
        ([1,2,3], 5, True),
    )
    for nums, k, expected in tests:
        print(Solution().checkSubarraySum(nums, k) == expected)
