"""
 * 958. Check Completeness of a Binary Tree [Medium]
 * Given the root of a binary tree, determine if it is a complete binary tree.
 *
 * In a complete binary tree
 * (http://en.wikipedia.org/wiki/Binary_tree#Types_of_binary_trees), every
 * level, except possibly the last, is completely filled, and all nodes in the
 * last level are as far left as possible. It can have between 1 and 2ʰ nodes
 * inclusive at the last level h.
 *
 * Example 1:
 * Input: root = [1,2,3,4,5,6]
 * Output: true
 * Explanation: Every level before the last is full (ie. levels with
 * node-values {1} and {2, 3}), and all nodes in the last level ({4, 5, 6}) are
 * as far left as possible.
 *
 * Example 2:
 * Input: root = [1,2,3,4,5,null,7]
 * Output: false
 * Explanation: The node with value 7 isn't as far left as possible.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 100].
 *     ∙ 1 <= Node.val <= 1000
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def isCompleteTree(self, root: Optional[TreeNode]) -> bool:
        to_visit = [root]
        seen_empty = False
        for node in to_visit:
            if node:
                if seen_empty:
                    return False
                to_visit.append(node.left)
                to_visit.append(node.right)
            else:
                seen_empty = True
        return True

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5,6], True),
        ([1,2,3,4,5,None,7], False),
    )
    for nums, expected in tests:
        print(Solution().isCompleteTree(build_tree(nums)) == expected)
