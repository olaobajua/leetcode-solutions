"""
 * 572. Subtree of Another Tree [Easy]
 * Given the roots of two binary trees root and subRoot, return true if there
 * is a subtree of root with the same structure and node values of subRoot and
 * false otherwise.
 *
 * A subtree of a binary tree tree is a tree that consists of a node in tree
 * and all of this node's descendants. The tree tree could also be considered
 * as a subtree of itself.
 *
 * Example 1:
 * Input: root = [3,4,5,1,2], subRoot = [4,1,2]
 * Output: true
 *
 * Example 2:
 * Input: root = [3,4,5,1,2,null,null,null,null,0], subRoot = [4,1,2]
 * Output: false
 *
 * Constraints:
 *     ∙ The number of nodes in the root tree is in the range [1, 2000].
 *     ∙ The number of nodes in the subRoot tree is in the range [1, 1000].
 *     ∙ -10⁴ <= root.val <= 10⁴
 *     ∙ -10⁴ <= subRoot.val <= 10⁴
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def isSubtree(self, root: Optional[TreeNode], subRoot: Optional[TreeNode]) -> bool:
        def dfs(node):
            if node:
                if node.val == subRoot.val and compare(node, subRoot):
                    return True
                if dfs(node.left) or dfs(node.right):
                    return True
            return False

        def compare(node1, node2):
            if node1 is None and node2 is not None:
                return False
            if node1 is not None and node2 is None:
                return False
            if node1 and node2:
                if node1.val != node2.val:
                    return False
                if compare(node1.left, node2.left) is False:
                    return False
                if compare(node1.right, node2.right) is False:
                    return False
            return True

        return dfs(root)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([3,4,5,1,2], [4,1,2], True),
        ([3,4,5,1,2,None,None,None,None,0], [4,1,2], False),
    )
    for nums, nums2, expected in tests:
        root = build_tree(nums)
        subRoot = build_tree(nums2)
        print(Solution().isSubtree(root, subRoot) == expected)
