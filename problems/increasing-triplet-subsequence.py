"""
 * 334. Increasing Triplet Subsequence [Medium]
 * Given an integer array nums, return true if there exists a triple of
 * indices (i, j, k) such that i < j < k and nums[i] < nums[j] < nums[k]. If no
 * such indices exists, return false.
 *
 * Example 1:
 * Input: nums = [1,2,3,4,5]
 * Output: true
 * Explanation: Any triplet where i < j < k is valid.
 *
 * Example 2:
 * Input: nums = [5,4,3,2,1]
 * Output: false
 * Explanation: No triplet exists.
 *
 * Example 3:
 * Input: nums = [2,1,5,0,4,6]
 * Output: true
 * Explanation: The triplet (3, 4, 5) is valid because nums[3] == 0 < nums[4]
 * == 4 < nums[5] == 6.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 5 * 10⁵
 *     ∙ -2³¹ <= nums[i] <= 2³¹ - 1
 *
 * Follow up: Could you implement a solution that runs in O(n) time complexity
 * and O(1) space complexity?
"""
from math import inf
from typing import List

class Solution:
    def increasingTriplet(self, nums: List[int]) -> bool:
        if len(nums) < 3:
            return False
        a = b = inf
        for x in nums:
            if a >= x:
                a = x
            elif b >= x:
                b = x
            else:
                return True
        return False

if __name__ == "__main__":
    tests = (
        ([1,2,3,4,5], True),
        ([5,4,3,2,1], False),
        ([2,1,5,0,4,6], True),
        ([0,4,2,1,0,-1,-3], False),
        ([20,100,10,12,5,13], True),
    )
    for nums, expected in tests:
        print(Solution().increasingTriplet(nums) == expected)
