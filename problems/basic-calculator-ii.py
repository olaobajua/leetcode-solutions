"""
 * 227. Basic Calculator II (Medium)
 * Given a string s which represents an expression, evaluate this expression
 * and return its value.
 *
 * The integer division should truncate toward zero.
 *
 * You may assume that the given expression is always valid. All intermediate
 * results will be in the range of [-2³¹, 2³¹ - 1].
 *
 * Note: You are not allowed to use any built-in function which evaluates
 * strings as mathematical expressions, such as eval().
 *
 * Example 1:
 * Input: s = "3+2*2"
 * Output: 7
 *
 * Example 2:
 * Input: s = " 3/2 "
 * Output: 1
 *
 * Example 3:
 * Input: s = " 3+5 / 2 "
 * Output: 5
 *
 * Constraints:
 *     ∙ 1 <= s.length <= 3 * 10⁵
 *     ∙ s consists of integers and operators ('+', '-', '*', '/') separated by
 *       some number of spaces.
 *     ∙ s represents a valid expression.
 *     ∙ All the integers in the expression are non-negative integers in the
 *       range [0, 2³¹ - 1].
 *     ∙ The answer is guaranteed to fit in a 32-bit integer.
"""
class Solution:
    def calculate(self, s: str) -> int:
        stack = []
        for element in math_infix_to_rpn(s):
            if isinstance(element, int):
                stack.append(element)
            else:
                if element == '+':
                    stack.append(stack.pop() + stack.pop())
                elif element == '-':
                    stack.append(-stack.pop() + stack.pop())
                elif element == '/':
                    prev = stack.pop()
                    stack.append(stack.pop() // prev)
                elif element == '*':
                    stack.append(stack.pop() * stack.pop())
        return stack.pop()

def math_infix_to_rpn(expr):
    """
    Transforms expr from infix notation to reverse polish notation.
    This is classical shunting-yard algorithm by Edsger Dijkstra.
    """
    PRECEDENCE = {
        '*': 2,
        '/': 2,
        '+': 1,
        '-': 1,
        '(': 0,
        ')': 0,
    }
    ret = [0, ' ']
    stack = []
    for c in expr:
        if '0' <= c <= '9':
            if isinstance(ret[-1], int):
                ret.append(10*ret.pop() + int(c))
            else:
                ret.append(int(c))
        else:
            if ret[-1] != ' ':
                ret.append(' ')
            if c in '*/+-':
                while stack and PRECEDENCE[c] <= PRECEDENCE[stack[-1]]:
                    ret.append(stack.pop())
                stack.append(c)
            elif c == '(':
                stack.append('(')
            elif c == ')':
                while (tmp := stack.pop()) != '(':
                    ret.append(tmp)
    while stack:
        ret.append(stack.pop())
    return tuple(element for element in ret if element != ' ')

if __name__ == "__main__":
    tests = (
        ("3+2*2", 7),
        (" 3/2 ", 1),
        (" 3+5 / 2 ", 5),
        ("0", 0),
    )
    for s, expected in tests:
        print(Solution().calculate(s) == expected)
