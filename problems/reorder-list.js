/**
 * 143. Reorder List (Medium)
 * You are given the head of a singly linked-list. The list can be represented
 * as:
 * L0 → L1 → … → Ln - 1 → Ln
 *
 * Reorder the list to be on the following form:
 * L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
 *
 * You may not modify the values in the list's nodes. Only nodes themselves may
 * be changed.
 *
 * Example 1:
 * Input: head = [1,2,3,4]
 * Output: [1,4,2,3]
 *
 * Example 2:
 * Input: head = [1,2,3,4,5]
 * Output: [1,5,2,4,3]
 *
 * Constraints:
 *     The number of nodes in the list is in the range [1, 5 * 10⁴].
 *     1 <= Node.val <= 1000
 */

// Definition for singly-linked list.
function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function(head) {
    let end = head, mid = head;
    while (end.next && (end = end.next.next)) {
        mid = mid.next;
    }
    [mid.next, mid] = [null, reverseRecur(mid.next)];
    while (mid) {
        [mid.next, head.next, head, mid] = [head.next, mid, head.next, mid.next];
    }
};

function reverseRecur(head, prev=null) {
    return head ? reverseRecur(head.next, new ListNode(head.val, prev)) : prev;
}

function arrayToLinkedList(a) {
    return a.length ? new ListNode(a.shift(), arrayToLinkedList(a)) : null;
}

function linkedListToArray(head) {
    let ret = [];
    while (head) {
        ret.push(head.val);
        head = head.next;
    }
    return ret;
}

const tests = [
    [[1,2,3,4], [1,4,2,3]],
    [[1,2,3,4,5], [1,5,2,4,3]],
];

for (const [nums, expected] of tests) {
    const root = arrayToLinkedList(nums);
    reorderList(root);
    console.log(linkedListToArray(root).toString() == expected.toString());
}
