"""
 * 437. Path Sum III (Medium)
 * Given the root of a binary tree and an integer targetSum, return the number
 * of paths where the sum of the values along the path equals targetSum.
 *
 * The path does not need to start or end at the root or a leaf, but it must go
 * downwards (i.e., traveling only from parent nodes to child nodes).
 *
 * Example 1:
 * Input: root = [10,5,-3,3,2,null,11,3,-2,null,1], targetSum = 8
 * Output: 3
 * Explanation: The paths that sum to 8 are shown.
 *
 * Example 2:
 * Input: root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
 * Output: 3
 *
 * Constraints:
 *     The number of nodes in the tree is in the range [0, 1000].
 *     -10⁹ <= Node.val <= 10⁹
 *     -1000 <= targetSum <= 1000
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def pathSum(self, root: Optional[TreeNode], targetSum: int) -> int:
        def count_paths(root, sums=[]):
            if root is None:
                return 0
            sums = [s + root.val for s in sums + [0]]
            return (sum(s == targetSum for s in sums) +
                    count_paths(root.left, sums) +
                    count_paths(root.right, sums))
        return count_paths(root)

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([10,5,-3,3,2,None,11,3,-2,None,1], 8, 3),
        ([5,4,8,11,None,13,4,7,2,None,None,5,1], 22, 3),
    )
    for nums, targetSum, expected in tests:
        root = build_tree(nums)
        print(Solution().pathSum(root, targetSum) == expected)
