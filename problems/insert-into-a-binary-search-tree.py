"""
 * 701. Insert into a Binary Search Tree (Medium)
 * You are given the root node of a binary search tree (BST) and a value to
 * insert into the tree. Return the root node of the BST after the insertion.
 * It is guaranteed that the new value does not exist in the original BST.
 *
 * Notice that there may exist multiple valid ways for the insertion, as long
 * as the tree remains a BST after insertion. You can return any of them.
 *
 * Example 1:
 *     4              4
 *  2     7         2     7
 * 1 3             1 3   5
 * Input: root = [4,2,7,1,3], val = 5
 * Output: [4,2,7,1,3,5]
 * Explanation: Another accepted tree is:
 *     5
 *  2     7
 * 1 3
 *    4
 *
 * Example 2:
 * Input: root = [40,20,60,10,30,50,70], val = 25
 * Output: [40,20,60,10,30,50,70,null,null,25]
 *
 * Example 3:
 * Input: root = [4,2,7,1,3,null,null,null,null,null,null], val = 5
 * Output: [4,2,7,1,3,5]
 *
 * Constraints:
 *     ∙ The number of nodes in the tree will be in the range [0, 10⁴].
 *     ∙ -10⁸ <= Node.val <= 10⁸
 *     ∙ All the values Node.val are unique.
 *     ∙ -10⁸ <= val <= 10⁸
 *     ∙ It's guaranteed that val does not exist in the original BST.
"""
from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def insertIntoBST(self, root: Optional[TreeNode], val: int) -> Optional[TreeNode]:
        return add_node(root, TreeNode(val))

def add_node(parent, child):
    if not parent:
        return child;
    if child.val < parent.val:
        parent.left = add_node(parent.left, child);
    else:
        parent.right = add_node(parent.right, child);
    return parent;

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

def tree_to_list(root):
    for n in (nodes := [root]):
        nodes.extend(n and [n.left, n.right] or [])
        yield n.val if n else None

if __name__ == "__main__":
    tests = (
        ([4,2,7,1,3], 5, [4,2,7,1,3,5]),
        ([40,20,60,10,30,50,70], 25, [40,20,60,10,30,50,70,None,None,25]),
        ([4,2,7,1,3,None,None,None,None,None,None], 5, [4,2,7,1,3,5]),
    )
    for nums, val, expected in tests:
        while nums and nums[-1] is None:
            nums.pop()
        tree = [*tree_to_list(Solution().insertIntoBST(build_tree(nums), val))]
        while tree and tree[-1] is None:
            tree.pop()
        print(tree == expected)
