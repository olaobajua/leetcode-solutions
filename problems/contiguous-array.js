/**
 * 525. Contiguous Array (Medium)
 * Given a binary array nums, return the maximum length of a contiguous
 * subarray with an equal number of 0 and 1.
 *
 * Example 1:
 * Input: nums = [0,1]
 * Output: 2
 * Explanation: [0, 1] is the longest contiguous subarray with an equal number
 * of 0 and 1.
 *
 * Example 2:
 * Input: nums = [0,1,0]
 * Output: 2
 * Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal
 * number of 0 and 1.
 *
 * Constraints:
 *     ∙ 1 <= nums.length <= 10⁵
 *     ∙ nums[i] is either 0 or 1.
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaxLength = function(nums) {
    const diffs = new Map([[0,0]]);
    let ret = 0, ones = 0, zeroes = 0;
    nums.forEach((x, i) => {
        x == 0 ? ++zeroes : ++ones;
        if (diffs.get(ones - zeroes) === undefined) {
            diffs.set(ones - zeroes, i + 1);
        };
        ret = Math.max(ret, i + 1 - diffs.get(ones - zeroes));
    });
    return ret;
};

const tests = [
    [[0,1], 2],
    [[0,1,0], 2],
    [[0,0,1,0,0,0,1,1], 6],
    [[1,0,0,0,0,1,0,1,0,1,0,0,0,0], 6],
    [[1,1,1,1,1,1,1,1], 0],
];

for (const [nums, expected] of tests) {
    console.log(findMaxLength(nums) == expected);
}
