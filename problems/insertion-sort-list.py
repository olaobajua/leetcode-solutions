"""
 * 147. Insertion Sort List (Medium)
 * Given the head of a singly linked list, sort the list using insertion sort,
 * and return the sorted list's head.
 *
 * The steps of the insertion sort algorithm:
 *     ∙ Insertion sort iterates, consuming one input element each repetition
 *       and growing a sorted output list.
 *     ∙ At each iteration, insertion sort removes one element from the input
 *       data, finds the location it belongs within the sorted list and inserts
 *       it there.
 *     ∙ It repeats until no input elements remain.
 *
 * The following is a graphical example of the insertion sort algorithm. The
 * partially sorted list (black) initially contains only the first element in
 * the list. One element (red) is removed from the input data and inserted
 * in-place into the sorted list with each iteration.
 *
 * Example 1:
 * Input: head = [4,2,1,3]
 * Output: [1,2,3,4]
 *
 * Example 2:
 * Input: head = [-1,5,3,4,0]
 * Output: [-1,0,3,4,5]
 *
 * Constraints:
 *     The number of nodes in the list is in the range [1, 5000].
 *     -5000 <= Node.val <= 5000
"""
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def insertionSortList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        start = ListNode()
        while head:
            bookmark = head.next
            prev, nxt = start, start.next
            while nxt and nxt.val < head.val:
                prev, nxt = nxt, nxt.next
            prev.next, head.next, head = head, nxt, bookmark
        return start.next

def list_to_linked_list(nums):
    return ListNode(nums[0], list_to_linked_list(nums[1:])) if nums else None

def linked_list_to_list(head):
    return [head.val] + linked_list_to_list(head.next) if head else []

if __name__ == "__main__":
    tests = (
        ([1], [1]),
        ([1,2,3,4], [1,2,3,4]),
        ([7,6,5,4,3,2,1], [1,2,3,4,5,6,7]),
        ([4,2,1,3], [1,2,3,4]),
        ([-1,5,3,4,0], [-1,0,3,4,5]),
        ([4,19,14,5,-3,1,8,5,11,15], [-3,1,4,5,5,8,11,14,15,19])
    )
    for nums, expected in tests:
        root = list_to_linked_list(nums)
        print(linked_list_to_list(Solution().insertionSortList(root)) ==
              expected)
