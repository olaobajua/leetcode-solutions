/**
 * 916. Word Subsets [Medium]
 * You are given two string arrays words1 and words2.
 *
 * A string b is a subset of string a if every letter in b occurs in a
 * including multiplicity.
 *     ∙ For example, "wrr" is a subset of "warrior" but is not a subset of
 *       "world".
 *
 * A string a from words1 is universal if for every string b in words2, b is a
 * subset of a.
 *
 * Return an array of all the universal strings in words1. You may return the
 * answer in any order.
 *
 * Example 1:
 * Input: words1 = ["amazon","apple","facebook","google","leetcode"], words2 =
 * ["e","o"]
 * Output: ["facebook","google","leetcode"]
 *
 * Example 2:
 * Input: words1 = ["amazon","apple","facebook","google","leetcode"], words2 =
 * ["l","e"]
 * Output: ["apple","google","leetcode"]
 *
 * Constraints:
 *     ∙ 1 <= words1.length, words2.length <= 10⁴
 *     ∙ 1 <= words1[i].length, words2[i].length <= 10
 *     ∙ words1[i] and words2[i] consist only of lowercase English letters.
 *     ∙ All the strings of words1 are unique.
 */

/**
 * @param {string[]} words1
 * @param {string[]} words2
 * @return {string[]}
 */
var wordSubsets = function(words1, words2) {
    const counter = new Counter([]);
    words2.forEach(word => new Counter(word).forEach((v, i) => {
        counter[i] = Math.max(counter[i], v);
    }));
    return words1.filter(w => new Counter(w).every((v, i) => counter[i] <= v));
};

class Counter extends Array {
    constructor(iterable) {
        super(26).fill(0);
        const a = 'a'.charCodeAt();
        [...iterable].forEach(x => ++this[x.charCodeAt()-a]);
    }
}

const tests = [
    [["amazon","apple","facebook","google","leetcode"], ["e","o"],
     ["facebook","google","leetcode"]],
    [["amazon","apple","facebook","google","leetcode"], ["l","e"],
     ["apple","google","leetcode"]],
];

for (const [words1, words2, expected] of tests) {
    console.log(wordSubsets(words1, words2).toString() == expected.toString());
}
