/**
 * 923. 3Sum With Multiplicity (Medium)
 * Given an integer array arr, and an integer target, return the number of
 * tuples i, j, k such that i < j < k and arr[i] + arr[j] + arr[k] == target.
 *
 * As the answer can be very large, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: arr = [1,1,2,2,3,3,4,4,5,5], target = 8
 * Output: 20
 * Explanation:
 * Enumerating by the values (arr[i], arr[j], arr[k]):
 * (1, 2, 5) occurs 8 times;
 * (1, 3, 4) occurs 8 times;
 * (2, 2, 4) occurs 2 times;
 * (2, 3, 3) occurs 2 times.
 *
 * Example 2:
 * Input: arr = [1,1,2,2,2,2], target = 5
 * Output: 12
 * Explanation:
 * arr[i] = 1, arr[j] = arr[k] = 2 occurs 12 times:
 * We choose one 1 from [1,1] in 2 ways,
 * and two 2s from [2,2,2,2] in 6 ways.
 *
 * Constraints:
 *     ∙ 3 <= arr.length <= 3000
 *     ∙ 0 <= arr[i] <= 100
 *     ∙ 0 <= target <= 300
 */

/**
 * @param {number[]} arr
 * @param {number} target
 * @return {number}
 */
var threeSumMulti = function(arr, target) {
    const n = arr.length;
    const MOD = 1e9 + 7;
    const indices = {};
    arr.forEach((x, i) => {
        indices[x] ? indices[x].push(i) : indices[x] = [i];
    });
    let ret = 0;
    arr.forEach((x, i) => {
        const twoSum = target - x;
        for (let j = i + 1; j < n; ++j) {
            const zs = indices[twoSum - arr[j]] || [];
            ret = (ret + zs.length - bisectRight(zs, j)) % MOD
        }
    });
    return ret;
};

function bisectRight(a, x, cmp=(a, b) => a - b, lo=0, hi=a.length-1) {
    while (lo <= hi) {
        const mid = Math.floor((hi + lo) / 2);
        if (cmp(x, a[mid]) >= 0) {
            lo = mid + 1;
        } else {
            hi = mid - 1;
        }
    }
    return lo;
}

tests = [
    [[1,1,2,2,3,3,4,4,5,5], 8, 20],
    [[1,1,2,2,2,2], 5, 12],
];
for (const [arr, target, expected] of tests) {
    console.log(threeSumMulti(arr, target) == expected);
}
