"""
 * 1232. Check If It Is a Straight Line [Easy]
 * You are given an array coordinates, coordinates[i] = [x, y], where [x, y]
 * represents the coordinate of a point. Check if these points make a straight
 * line in the XY plane.
 *
 * Example 1:
 * Input: coordinates = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]]
 * Output: true
 *
 * Example 2:
 * Input: coordinates = [[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]]
 * Output: false
 *
 * Constraints:
 *     ∙ 2 <= coordinates.length <= 1000
 *     ∙ coordinates[i].length == 2
 *     ∙ -10⁴ <= coordinates[i][0], coordinates[i][1] <= 10⁴
 *     ∙ coordinates contains no duplicate point.
"""
from typing import List

class Solution:
    def checkStraightLine(self, coordinates: List[List[int]]) -> bool:
        x1, y1 = coordinates[0]
        x2, y2 = coordinates[1]
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1

        if x1 == x2:
            return all(x == x1 for x, _ in coordinates)

        slope = (y2 - y1) / (x2 - x1)
        intercept = y1 - slope*x1
        for x, y in coordinates:
            if y != intercept + slope * x:
                return False
        return True

if __name__ == "__main__":
    tests = (
        ([[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]], True),
        ([[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]], False),
        ([[0,0],[0,1],[0,-1]], True),
    )
    for coordinates, expected in tests:
        print(Solution().checkStraightLine(coordinates) == expected)
