/**
 * 890. Find and Replace Pattern [Medium]
 * Given a list of strings words and a string pattern, return a list of
 * words[i] that match pattern. You may return the answer in any order.
 *
 * A word matches the pattern if there exists a permutation of letters p so
 * that after replacing every letter x in the pattern with p(x), we get the
 * desired word.
 *
 * Recall that a permutation of letters is a bijection from letters to letters:
 * every letter maps to another letter, and no two letters map to the same
 * letter.
 *
 * Example 1:
 * Input: words = ["abc","deq","mee","aqq","dkd","ccc"], pattern = "abb"
 * Output: ["mee","aqq"]
 * Explanation: "mee" matches the pattern because there is a permutation {a ->
 * m, b -> e, ...}.
 * "ccc" does not match the pattern because {a -> c, b -> c, ...} is not a
 * permutation, since a and b map to the same letter.
 *
 * Example 2:
 * Input: words = ["a","b","c"], pattern = "a"
 * Output: ["a","b","c"]
 *
 * Constraints:
 *     ∙ 1 <= pattern.length <= 20
 *     ∙ 1 <= words.length <= 50
 *     ∙ words[i].length == pattern.length
 *     ∙ pattern and words[i] are lowercase English letters.
 */

/**
 * @param {string[]} words
 * @param {string} pattern
 * @return {string[]}
 */
var findAndReplacePattern = function(words, pattern) {
    const ret = [];
    const a = 'a'.charCodeAt();
    const n = pattern.length;
    for (const word of words) {
        next_word: {
            const word_to_pattern = Array(26);
            const pattern_to_word = Array(26);
            for (let i = 0; i < n; ++i) {
                const wc = word[i].charCodeAt() - a;
                const pc = pattern[i].charCodeAt() - a;
                if (word_to_pattern[wc] === undefined)
                    word_to_pattern[wc] = pc;
                if (pattern_to_word[pc] === undefined)
                    pattern_to_word[pc] = wc;
                if (word_to_pattern[wc] != pc || pattern_to_word[pc] != wc)
                    break next_word;
            }
            ret.push(word);
        }
    }
    return ret;
};

const tests = [
    [["abc","deq","mee","aqq","dkd","ccc"], "abb", ["mee","aqq"]],
    [["a","b","c"], "a", ["a","b","c"]],
];

for (const [words, pattern, expected] of tests) {
    console.log(findAndReplacePattern(words, pattern).toString() ==
                expected.toString());
}
