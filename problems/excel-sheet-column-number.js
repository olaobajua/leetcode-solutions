/**
 * 171. Excel Sheet Column Number (Easy)
 * Given a string columnTitle that represents the column title as appear in an
 * Excel sheet, return its corresponding column number.
 *
 * For example:
 * A -> 1
 * B -> 2
 * C -> 3
 * ...
 * Z -> 26
 * AA -> 27
 * AB -> 28
 * ...
 *
 * Example 1:
 * Input: columnTitle = "A"
 * Output: 1
 *
 * Example 2:
 * Input: columnTitle = "AB"
 * Output: 28
 *
 * Example 3:
 * Input: columnTitle = "ZY"
 * Output: 701
 *
 * Constraints:
 *     ∙ 1 <= columnTitle.length <= 7
 *     ∙ columnTitle consists only of uppercase English letters.
 *     ∙ columnTitle is in the range ["A", "FXSHRXW"].
 */

/**
 * @param {string} columnTitle
 * @return {number}
 */
var titleToNumber = function(columnTitle) {
    const A = 'A'.charCodeAt();
    let ret = 0;
    for (let i = 0; i < columnTitle.length; ++i) {
        ret = 26*ret + columnTitle.charCodeAt(i) - A + 1;
    }
    return ret;
};

const tests = [
    ["A", 1],
    ["AB", 28],
    ["ZY", 701],
    ["FXSHRXW", 2147483647],
];

for (const [columnTitle, expected] of tests) {
    console.log(titleToNumber(columnTitle) == expected);
}
