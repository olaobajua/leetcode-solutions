/**
 * 55. Jump Game (Medium)
 * You are given an integer array nums. You are initially positioned at the
 * array's first index, and each element in the array represents your maximum
 * jump length at that position.
 *
 * Return true if you can reach the last index, or false otherwise.
 *
 * Example 1:
 * Input: nums = [2,3,1,1,4]
 * Output: true
 * Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
 *
 * Example 2:
 * Input: nums = [3,2,1,0,4]
 * Output: false
 * Explanation: You will always arrive at index 3 no matter what. Its maximum
 * jump length is 0, which makes it impossible to reach the last index.
 *
 * Constraints:
 *     1 <= nums.length <= 10⁴
 *     0 <= nums[i] <= 10⁵
 */

/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canJump = function(nums) {
    let can_reach = 0;
    for (let [i, max_jump] of nums.entries()) {
        if (i > can_reach) {
            return false;
        }
        can_reach = Math.max(can_reach, i + max_jump);
    }
    return true;
};

tests = [
    [[2,3,1,1,4], true],
    [[3,2,1,0,4], false],
    [[0], true],
    [[1], true],
];
for (let [nums, expected] of tests) {
    console.log(canJump(nums) == expected);
}
