"""
 * 872. Leaf-Similar Trees [Easy]
 * Consider all the leaves of a binary tree, from left to right order, the
 * values of those leaves form a leaf value sequence.
 *
 * For example, in the given tree above, the leaf value sequence is
 * (6, 7, 4, 9, 8).
 *
 * Two binary trees are considered leaf-similar if their leaf value sequence
 * is the same.
 *
 * Return true if and only if the two given trees with head nodes root1 and
 * root2 are leaf-similar.
 *
 * Example 1:
 *       3                  3
 *   5       1         5         1
 * 6   2   9   8     6   7     4   2
 *    7 4                         9 8
 * Input: root1 = [3,5,1,6,2,9,8,null,null,7,4], root2 =
 * [3,5,1,6,7,4,2,null,null,null,null,null,null,9,8]
 * Output: true
 *
 * Example 2:
 *  1     1
 * 2 3   3 2
 * Input: root1 = [1,2,3], root2 = [1,3,2]
 * Output: false
 *
 * Constraints:
 *     ∙ The number of nodes in each tree will be in the range [1, 200].
 *     ∙ Both of the given trees will have values in the range [0, 200].
"""
from typing import List, Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def leafSimilar(self, root1: Optional[TreeNode], root2: Optional[TreeNode]) -> bool:
        def get_leaf_nodes(node):
            if node.left is None and node.right is None:
                yield node.val
            if node.left is not None:
                yield from get_leaf_nodes(node.left)
            if node.right is not None:
                yield from get_leaf_nodes(node.right)

        return [*get_leaf_nodes(root1)] == [*get_leaf_nodes(root2)]

def build_tree(vals):
    root = None
    try:
        root = TreeNode(vals.pop(0))
        nodes = [root]
        for n in nodes:
            if (v := vals.pop(0)) is not None:
                n.left = TreeNode(v)
                nodes.append(n.left)
            if (v := vals.pop(0)) is not None:
                n.right = TreeNode(v)
                nodes.append(n.right)
    except IndexError:
        return root

if __name__ == "__main__":
    tests = (
        ([3,5,1,6,2,9,8,None,None,7,4], [3,5,1,6,7,4,2,None,None,None,None,None,None,9,8], True),
        ([1,2,3], [1,3,2], False),
    )
    for nums1, nums2, expected in tests:
        root1 = build_tree(nums1)
        root2 = build_tree(nums2)
        print(Solution().leafSimilar(root1, root2) == expected)
