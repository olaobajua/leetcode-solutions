/**
 * 968. Binary Tree Cameras (Hard)
 * You are given the root of a binary tree. We install cameras on the tree
 * nodes where each camera at a node can monitor its parent, itself, and its
 * immediate children.
 *
 * Return the minimum number of cameras needed to monitor all nodes of the
 * tree.
 *
 * Example 1:
 * Input: root = [0,0,null,0,0]
 * Output: 1
 * Explanation: One camera is enough to monitor all nodes if placed as shown.
 *
 * Example 2:
 * Input: root = [0,0,null,0,null,0,null,null,0]
 * Output: 2
 * Explanation: At least two cameras are needed to monitor all nodes of the
 * tree. The above image shows one of the valid configurations of camera
 * placement.
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 1000].
 *     ∙ Node.val == 0
 */

// Definition for a binary tree node.
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

int dfs(struct TreeNode *node, int *cams) {
    if (node) {
        int left = dfs(node->left, cams);
        int right = dfs(node->right, cams);
        if (left == 2 && right == 2) {  // leaf
            return 0;
        }
        if (left == 0 || right == 0) {  // leaf parent
            ++*cams;  // here we install 1 cam
            return 1;
        }
        if (left == 1 || right == 1) {  // covered by children node, new end
            return 2;
        }
    }
    return 2;  // empty node, end
}

int minCameraCover(struct TreeNode *root) {
    int ret = 0;
    return (dfs(root, &ret) == 0) + ret;
}
