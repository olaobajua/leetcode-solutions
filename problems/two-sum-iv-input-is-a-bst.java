/**
 * 653. Two Sum IV - Input is a BST [Easy]
 * Given the root of a Binary Search Tree and a target number k, return true
 * if there exist two elements in the BST such that their sum is equal to the
 * given target.
 *
 * Example 1:
 * Input: root = [5,3,6,2,4,null,7], k = 9
 * Output: true
 *
 * Example 2:
 * Input: root = [5,3,6,2,4,null,7], k = 28
 * Output: false
 *
 * Constraints:
 *     ∙ The number of nodes in the tree is in the range [1, 10⁴].
 *     ∙ -10⁴ <= Node.val <= 10⁴
 *     ∙ root is guaranteed to be a valid binary search tree.
 *     ∙ -10⁵ <= k <= 10⁵
 */

// Definition for a binary tree node.
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class Solution {
    TreeNode root;
    int k;
    public boolean findTarget(TreeNode root, int k) {
        this.root = root;
        this.k = k;
        return preorder(root);
    }
    private boolean preorder(TreeNode node) {
        if (node != null) {
            TreeNode n = find(root, k - node.val);
            if (n != null && n != node)
                return true;
            if (preorder(node.left) || preorder(node.right))
                return true;
        }
        return false;
    }

    private TreeNode find(TreeNode node, int val) {
        if (node != null)
            if (val > node.val)
                return find(node.right, val);
            else if (val < node.val)
                return find(node.left, val);
            else
                return node;
        return null;
    }
}
