/**
 * 208. Implement Trie (Prefix Tree) (Medium)
 * A trie (pronounced as "try") or prefix tree is a tree data structure used to
 * efficiently store and retrieve keys in a dataset of strings. There are
 * various applications of this data structure, such as autocomplete and
 * spellchecker.
 *
 * Implement the Trie class:
 *     ∙ Trie() Initializes the trie object.
 *     ∙ void insert(String word) Inserts the string word into the trie.
 *     ∙ boolean search(String word) Returns true if the string word is in
 *       the trie (i.e., was inserted before), and false otherwise.
 *     ∙ boolean startsWith(String prefix) Returns true if there is a
 *       previously inserted string word that has the prefix prefix, and false
 *       otherwise.
 *
 * Example 1:
 * Input
 * ["Trie", "insert", "search", "search", "startsWith", "insert", "search"]
 * [[], ["apple"], ["apple"], ["app"], ["app"], ["app"], ["app"]]
 * Output
 * [null, null, true, false, true, null, true]
 *
 * Explanation
 * Trie trie = new Trie();
 * trie.insert("apple");
 * trie.search("apple");   // return True
 * trie.search("app");     // return False
 * trie.startsWith("app"); // return True
 * trie.insert("app");
 * trie.search("app");     // return True
 *
 * Constraints:
 *     ∙ 1 <= word.length, prefix.length <= 2000
 *     ∙ word and prefix consist only of lowercase English letters.
 *     ∙ At most 3 * 10⁴ calls in total will be made to insert, search, and
 *       startsWith.
 */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define TRIE_CHARS_COUNT 26
#define TRIE_FIRST_CHAR 'a'

typedef struct Trie {
    struct Trie *children[TRIE_CHARS_COUNT];
    bool end;
} Trie;

Trie* trieCreate() {
    return (Trie*)calloc(1, sizeof(Trie));
}

void trieInsert(Trie *obj, char *word) {
    for (int i = 0; word[i] != '\0'; ++i) {
        if (!obj->children[word[i]-TRIE_FIRST_CHAR])
            obj->children[word[i]-TRIE_FIRST_CHAR] =
                (Trie*)calloc(1, sizeof(Trie));
        obj = obj->children[word[i]-TRIE_FIRST_CHAR];
    }
    obj->end = true;
}

bool trieSearch(Trie *obj, char *word) {
    for (int i = 0; word[i] != '\0'; ++i) {
        if (obj->children[word[i]-TRIE_FIRST_CHAR] == NULL) {
            return false;
        }
        obj = obj->children[word[i]-TRIE_FIRST_CHAR];
    }
    return obj->end;
}

bool trieStartsWith(Trie *obj, char *prefix) {
    for (int i = 0; prefix[i] != '\0'; ++i) {
        if (obj->children[prefix[i]-TRIE_FIRST_CHAR] == NULL) {
            return false;
        }
        obj = obj->children[prefix[i]-TRIE_FIRST_CHAR];
    }
    return true;
}

void trieFree(Trie* obj) {
    if (obj) {
        for (int i = 0; i < TRIE_CHARS_COUNT; ++i) {
            trieFree(obj->children[i]);
        }
        free(obj);
    }
}

int main() {
    Trie *obj = trieCreate();
    trieInsert(obj, "apple");
    printf("%d\n", true == trieSearch(obj, "apple"));
    printf("%d\n", false == trieSearch(obj, "app"));
    printf("%d\n", true == trieStartsWith(obj, "app"));
    trieInsert(obj, "app");
    printf("%d\n", true == trieSearch(obj, "app"));
    trieFree(obj);
}
