/**
 * 629. K Inverse Pairs Array [Hard]
 * For an integer array nums, an inverse pair is a pair of integers [i, j]
 * where 0 <= i < j < nums.length and nums[i] > nums[j].
 *
 * Given two integers n and k, return the number of different arrays consist of
 * numbers from 1 to n such that there are exactly k inverse pairs. Since the
 * answer can be huge, return it modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: n = 3, k = 0
 * Output: 1
 * Explanation: Only the array [1,2,3] which consists of numbers from 1 to 3
 * has exactly 0 inverse pairs.
 *
 * Example 2:
 * Input: n = 3, k = 1
 * Output: 2
 * Explanation: The array [1,3,2] and [2,1,3] have exactly 1 inverse pair.
 *
 * Constraints:
 *     ∙ 1 <= n <= 1000
 *     ∙ 0 <= k <= 1000
 */

const memo = [...Array(1001)].map(_ => Array(1001));
/**
 * @param {number} n
 * @param {number} k
 * @return {number}
 */
var kInversePairs = function(n, k) {
    if (n === 0)
        return 0;
    if (k === 0)
        return 1;
    if (memo[n][k] === undefined) {
        memo[n][k] = 0;
        for (let i = 0; i < Math.min(n, k + 1); ++i) {
            memo[n][k] += kInversePairs(n - 1, k - i);
            memo[n][k] %= 1000000007;
        }
    }
    return memo[n][k];
};

const tests = [
    [3, 0, 1],  // [1,2,3]
    [3, 1, 2],  // [1,3,2], [2,1,3]
    [3, 2, 2],  // [2,3,1], [3,1,2]
    [3, 3, 1],  // [3,2,1]
    [4, 0, 1],  // [1,2,3,4]
    [4, 1, 3],  // [1,2,4,3], [1,3,2,4], [2,1,3,4]
    [4, 2, 5],  // [1,3,4,2], [1,4,2,3], [2,1,4,3], [2,3,1,4], [3,1,2,4]
    [4, 3, 6],  // [1,4,3,2], [2,3,4,1], [2,4,1,3], [3,1,4,2], [3,2,1,4], [4,1,2,3]
    [4, 4, 5],  // [2,4,3,1], [3,2,4,1], [3,4,1,2], [4,1,3,2], [4,2,1,3]
    [4, 5, 3],  // [3,4,2,1], [4,2,3,1], [4,3,1,2]
    [4, 6, 1],  // [4,3,2,1]
    [4, 7, 0],
    [10, 1, 9],
    [10, 2, 44],
    [10, 3, 155],
];

for (const [n, k, expected] of tests) {
    console.log(kInversePairs(n, k) === expected);
}
