/**
 * 2120. Execution of All Suffix Instructions Staying in a Grid (Medium)
 * There is an n x n grid, with the top-left cell at (0, 0) and the
 * bottom-right cell at (n - 1, n - 1). You are given the integer n and an
 * integer array startPos where startPos = [startrow, startcol] indicates that
 * a robot is initially at cell (startrow, startcol).
 *
 * You are also given a 0-indexed string s of length m where s[i] is the iᵗʰ
 * instruction for the robot: 'L' (move left), 'R' (move right), 'U' (move up),
 * and 'D' (move down).
 *
 * The robot can begin executing from any iᵗʰ instruction in s. It executes the
 * instructions one by one towards the end of s but it stops if either of these
 * conditions is met:
 *     ∙ The next instruction will move the robot off the grid.
 *     ∙ There are no more instructions left to execute.
 *
 * Return an array answer of length m where answer[i] is the number of
 * instructions the robot can execute if the robot begins executing from the
 * iᵗʰ instruction in s.
 *
 * Example 1:
 * Input: n = 3, startPos = [0,1], s = "RRDDLU"
 * Output: [1,5,4,3,1,0]
 * Explanation: Starting from startPos and beginning execution from the iᵗʰ
 * instruction:
 * - 0th: "RRDDLU". Only one instruction "R" can be executed before it moves
 *   off the grid.
 * - 1st:  "RDDLU". All five instructions can be executed while it stays in the
 *   grid and ends at (1, 1).
 * - 2nd:   "DDLU". All four instructions can be executed while it stays in the
 *   grid and ends at (1, 0).
 * - 3rd:    "DLU". All three instructions can be executed while it stays in
 *   the grid and ends at (0, 0).
 * - 4th:     "LU". Only one instruction "L" can be executed before it moves
 *   off the grid.
 * - 5th:      "U". If moving up, it would move off the grid.
 *
 * Example 2:
 * Input: n = 2, startPos = [1,1], s = "LURD"
 * Output: [4,1,0,0]
 * Explanation:
 * - 0th: "LURD".
 * - 1st:  "URD".
 * - 2nd:   "RD".
 * - 3rd:    "D".
 *
 * Example 3:
 * Input: n = 1, startPos = [0,0], s = "LRUD"
 * Output: [0,0,0,0]
 * Explanation: No matter which instruction the robot begins execution from, it
 * would move off the grid.
 *
 * Constraints:
 *     ∙ m == s.length
 *     ∙ 1 <= n, m <= 500
 *     ∙ startPos.length == 2
 *     ∙ 0 <= startrow, startcol < n
 *     ∙ s consists of 'L', 'R', 'U', and 'D'.
 */

/**
 * @param {number} n
 * @param {number[]} startPos
 * @param {string} s
 * @return {number[]}
 */
var executeInstructions = function(n, startPos, s) {
    const DIRECTION = {'U': [-1, 0],
                       'D': [1, 0],
                       'R': [0, 1],
                       'L': [0, -1]};
    s = Array.from(s).map(d => DIRECTION[d]);
    const ret = []
    let r, c;
    for (let i = 0; i < s.length; ++i) {
        [r, c] = startPos;
        startInstruction: {
            let j, dx, dy;
            for ([j, [dy, dx]] of s.slice(i).entries()) {
                r += dy;
                c += dx;
                if (c < 0 || c >= n || r < 0 || r >= n) {
                    ret.push(j);
                    break startInstruction;
                }
            }
            ret.push(j + 1);
        }
    }
    return ret;};

const tests = [
    [3, [0,1], "RRDDLU", [1,5,4,3,1,0]],
    [2, [1,1], "LURD", [4,1,0,0]],
    [1, [0,0], "LRUD", [0,0,0,0]],
];

for (const [n, startPos, s, expected] of tests) {
    console.log(executeInstructions(n, startPos, s).toString() ==
                expected.toString());
}
