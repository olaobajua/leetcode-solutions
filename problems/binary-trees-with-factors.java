/**
 * 823. Binary Trees With Factors [Medium]
 * Given an array of unique integers, arr, where each integer arr[i] is
 * strictly greater than 1.
 *
 * We make a binary tree using these integers, and each number may be used for
 * any number of times. Each non-leaf node's value should be equal to the
 * product of the values of its children.
 *
 * Return the number of binary trees we can make. The answer may be too large
 * so return the answer modulo 10⁹ + 7.
 *
 * Example 1:
 * Input: arr = [2,4]
 * Output: 3
 * Explanation: We can make these trees: [2], [4], [4, 2, 2]
 *
 * Example 2:
 * Input: arr = [2,4,5,10]
 * Output: 7
 * Explanation: We can make these trees: [2], [4], [5], [10], [4, 2, 2], [10,
 * 2, 5], [10, 5, 2].
 *
 * Constraints:
 *     ∙ 1 <= arr.length <= 1000
 *     ∙ 2 <= arr[i] <= 10⁹
 *     ∙ All the values of arr are unique.
 */
class Solution {
    final int MOD = 1000000007;
    Map<Integer, Long> cache = new HashMap();
    Set<Integer> a = new HashSet<>();

    public int numFactoredBinaryTrees(int[] arr) {
        for (int x : arr)
            a.add(x);
        long ret = 0;
        for (int x : arr) {
            ret += dp(x);
            ret %= MOD;
        }
        return (int)ret;
    }
    long dp(int x) {
        if (cache.get(x) == null) {
            long ret = 1;
            for (int y : a) {
                int z = x / y;
                if (x % y == 0 && a.contains(z)) {
                    ret += dp(y) * dp(z);
                    ret %= MOD;
                }
            }
            cache.put(x, ret);
        }
        return cache.get(x);
    }
}
