/**
 * 867. Transpose Matrix (Easy)
 * Given a 2D integer array matrix, return the transpose of matrix.
 *
 * The transpose of a matrix is the matrix flipped over its main diagonal,
 * switching the matrix's row and column indices.
 *
 * Example 1:
 * Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
 * Output: [[1,4,7],[2,5,8],[3,6,9]]
 *
 * Example 2:
 * Input: matrix = [[1,2,3],[4,5,6]]
 * Output: [[1,4],[2,5],[3,6]]
 *
 * Constraints:
 *     ∙ m == matrix.length
 *     ∙ n == matrix[i].length
 *     ∙ 1 <= m, n <= 1000
 *     ∙ 1 <= m * n <= 10⁵
 *     ∙ -10⁹ <= matrix[i][j] <= 10⁹
 */

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
int** transpose(int **matrix, int matrixSize, int *matrixColSize, int *returnSize, int **returnColumnSizes) {
    *returnSize = matrixColSize[0];
    *returnColumnSizes = (int*)calloc(matrixColSize[0], sizeof(int));
    int **ret = (int**)calloc(matrixColSize[0], sizeof(int*));
    for (int i = 0; i < matrixColSize[0]; ++i) {
        ret[i] = (int*)calloc(matrixSize, sizeof(int));
        (*returnColumnSizes)[i] = matrixSize;
    }
    for (int i = 0; i < matrixColSize[0]; ++i) {
        for (int j = 0; j < matrixSize; ++j) {
            ret[i][j] = matrix[j][i];
        }
    }
    return ret;
}
